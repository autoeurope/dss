program reportstudy;

uses
  Forms,
  rbStudy in 'rbStudy.pas' {frmDssAdhocTool},
  ReportToolGlobals in 'ReportToolGlobals.pas',
  newreportwizard in 'newreportwizard.pas' {frmNewReportWizard},
  aeDynamicParams in 'aeDynamicParams.pas' {frmDynamicParams},
  Dictionary in 'Dictionary.pas' {frmDictionary},
  aeHeaders in 'aeHeaders.pas' {frmHeader},
  rbStudyDM in 'rbStudyDM.pas' {dmRbStudy: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmRbStudy, dmRbStudy);
  Application.CreateForm(TfrmDssAdhocTool, frmDssAdhocTool);
  Application.CreateForm(TfrmHeader, frmHeader);
  Application.Run;
end.
