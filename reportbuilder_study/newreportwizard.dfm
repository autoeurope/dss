object frmNewReportWizard: TfrmNewReportWizard
  Left = 0
  Top = 0
  Caption = 'New Report Wizard'
  ClientHeight = 348
  ClientWidth = 435
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBottom: TPanel
    Left = 0
    Top = 307
    Width = 435
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 274
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Next'
      TabOrder = 0
    end
    object btnClose: TButton
      Left = 355
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 1
      OnClick = btnCloseClick
    end
  end
  object PanelBody: TPanel
    Left = 0
    Top = 0
    Width = 435
    Height = 307
    Align = alClient
    Caption = 'PanelBody'
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 80
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 433
      Height = 305
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'General'
        object Label3: TLabel
          Left = 0
          Top = 45
          Width = 57
          Height = 13
          Caption = 'Description:'
        end
        object Label4: TLabel
          Left = 3
          Top = 178
          Width = 48
          Height = 13
          Caption = 'Dictionary'
        end
        object LabeledEditReportName: TLabeledEdit
          Left = 0
          Top = 16
          Width = 381
          Height = 21
          EditLabel.Width = 67
          EditLabel.Height = 13
          EditLabel.Caption = 'Report Name:'
          TabOrder = 0
        end
        object TntRichEditDescription: TTntRichEdit
          Left = 0
          Top = 64
          Width = 381
          Height = 105
          Lines.Strings = (
            '')
          TabOrder = 1
        end
        object Shared: TCheckBox
          Left = 0
          Top = 217
          Width = 97
          Height = 17
          Caption = 'Shared'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object btnSaveTemplate: TButton
          Left = 339
          Top = 251
          Width = 83
          Height = 25
          Caption = 'Save Template'
          TabOrder = 3
          OnClick = btnSaveTemplateClick
        end
        object cbDictionary: TComboBox
          Left = 128
          Top = 175
          Width = 253
          Height = 21
          ItemHeight = 13
          TabOrder = 4
          Items.Strings = (
            'Default Dictionary')
        end
        object cbDateStamp: TCheckBox
          Left = 0
          Top = 240
          Width = 97
          Height = 17
          Caption = 'Add Date Stamp'
          TabOrder = 5
        end
        object cbPageNum: TCheckBox
          Left = 0
          Top = 259
          Width = 137
          Height = 17
          Caption = 'Add Page Numbers'
          TabOrder = 6
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Custom SQL'
        ImageIndex = 1
        object TntRichEditSQL: TTntRichEdit
          Left = 0
          Top = 0
          Width = 425
          Height = 277
          Align = alClient
          Lines.Strings = (
            '')
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Parameters'
        ImageIndex = 2
        object Panelpbottom: TPanel
          Left = 0
          Top = 121
          Width = 425
          Height = 156
          Align = alClient
          Caption = 'Panelb'
          TabOrder = 0
          object ListViewParam: TListView
            Left = 1
            Top = 1
            Width = 423
            Height = 154
            Align = alClient
            Columns = <
              item
                Caption = 'Name'
                Width = 175
              end
              item
                Caption = 'Type'
                Width = 125
              end
              item
                Caption = 'Default Value'
                Width = 110
              end>
            TabOrder = 0
            ViewStyle = vsReport
          end
        end
        object PanelPTop: TPanel
          Left = 0
          Top = 0
          Width = 425
          Height = 121
          Align = alTop
          TabOrder = 1
          DesignSize = (
            425
            121)
          object Label2: TLabel
            Left = 0
            Top = 48
            Width = 81
            Height = 13
            Caption = 'Parameter Type:'
          end
          object LabeledEditPNmae: TLabeledEdit
            Left = 0
            Top = 24
            Width = 417
            Height = 21
            EditLabel.Width = 137
            EditLabel.Height = 13
            EditLabel.Caption = 'Parmeter Name (no spaces):'
            TabOrder = 0
          end
          object ComboBox1: TComboBox
            Left = 0
            Top = 64
            Width = 214
            Height = 21
            ItemHeight = 13
            TabOrder = 1
            Text = 'String'
            Items.Strings = (
              'String'
              'Integer'
              'Boolean'
              'Date'
              'Date/Time'
              'Currency'
              'Double'
              'Extended')
          end
          object LabeledEditDefaultValue: TLabeledEdit
            Left = 220
            Top = 64
            Width = 197
            Height = 21
            EditLabel.Width = 68
            EditLabel.Height = 13
            EditLabel.Caption = 'Default Value:'
            TabOrder = 2
          end
          object Button3: TButton
            Left = 342
            Top = 90
            Width = 75
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = 'Add'
            TabOrder = 3
          end
        end
      end
    end
  end
end
