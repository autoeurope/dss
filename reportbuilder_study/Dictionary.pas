unit Dictionary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls;

type
  TfrmDictionary = class(TForm)
    PanelBottom: TPanel;
    PanelBody: TPanel;
    btnSelect: TButton;
    btnCancel: TButton;
    ListView1: TListView;
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDictionary: TfrmDictionary;

implementation

{$R *.dfm}

procedure TfrmDictionary.btnCancelClick(Sender: TObject);
begin
  self.Close;
end;

end.
