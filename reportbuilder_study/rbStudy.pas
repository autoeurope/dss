unit rbStudy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IfxCustomDataSet, IfxQuery, IfxConnection, ppDB, ppDBPipe,
  ppRelatv, ppProd, ppClass, ppReport, ppComm, ppEndUsr, StdCtrls, ppBands,
  ppCache, aecommonfunc_II,apicryptunit2, ppDsgnDB, ExtCtrls, VirtualTrees,
  Menus,
  ReportToolGlobals, ComCtrls, TntComCtrls,newreportwizard, ppViewr, ppCtrls,
  ppPrnabl, ppParameter,
  aeDynamicParams, ppVar, rbStudyDM;

type
  TfrmDssAdhocTool = class(TForm)
    ppDesigner1: TppDesigner;
    ppReport1: TppReport;
    ppDBPipeline1: TppDBPipeline;
    DataSource1: TDataSource;
    IfxConnection1: TIfxConnection;
    IfxQuery1: TIfxQuery;
    ppDataDictionary1: TppDataDictionary;
    PanelBottom: TPanel;
    PanelLeft: TPanel;
    SplitterVSTBody: TSplitter;
    PanelBody: TPanel;
    VirtualStringTreeMain: TVirtualStringTree;
    btnNewReport: TButton;
    btnClose: TButton;
    btnRun: TButton;
    btnEdit: TButton;
    btnDeleteSelected: TButton;
    MainMenu1: TMainMenu;
    mnuFile: TMenuItem;
    mnuClose: TMenuItem;
    mnuHelp: TMenuItem;
    mnuAbout: TMenuItem;
    PanelBodyTop: TPanel;
    PanelbodyClient: TPanel;
    LabeledEditFileLocation: TLabeledEdit;
    LabeledEditFileName: TLabeledEdit;
    PageControlText: TPageControl;
    TabSheetDescription: TTabSheet;
    TabSheetSQL: TTabSheet;
    TabSheet1: TTabSheet;
    TntRichEditDescription: TTntRichEdit;
    TntRichEditSQL: TTntRichEdit;
    LabeledEditReportOwner: TLabeledEdit;
    LabeledEditCreateDate: TLabeledEdit;
    LabeledEditlastModifiedDate: TLabeledEdit;
    cbShared: TCheckBox;
    cbCustomQuery: TCheckBox;
    cbFileLocked: TCheckBox;
    cbIsDeleted: TCheckBox;
    btnEditLayout: TButton;
    ppParameterList1: TppParameterList;
    TntRichEditParmList: TTntRichEdit;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    procedure btnEditLayoutClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VirtualStringTreeMainGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure btnCloseClick(Sender: TObject);
    procedure mnuCloseClick(Sender: TObject);
    procedure VirtualStringTreeMainFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure btnEditClick(Sender: TObject);
    procedure btnNewReportClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure btnDeleteSelectedClick(Sender: TObject);
    procedure mnuAboutClick(Sender: TObject);
  private
  fbolAreWeNew, fbolInEditState: boolean;
   fNodeD, fNewNodeD: pTreeData;
   fstrSystem: string;
  procedure LoadTree();
  Procedure PointToRecord;
  procedure ClearFields();
  procedure SetEditState();
  procedure WhatColor();
  procedure WhatButtons();
  procedure LoadParameters(NodeD: pTreeData);
  //procedure FetchReports();
    { Private declarations }
  public
  property BaseSystem: string read fstrSystem write fstrSystem;
    { Public declarations }
  end;

var
  frmDssAdhocTool: TfrmDssAdhocTool;

implementation

{$R *.dfm}
procedure TfrmDssAdhocTool.LoadParameters(NodeD: pTreeData);
var
  I: Integer;
begin

    SetLength(NodeD^.mParmList, 3);
      //NodeD^.mParmList
    for I := 0 to 2 do
      begin
       //new(NodeD^.mParmList);
         case I of

          0:
            begin
              NodeD^.mParmList[I].mParmId := I;
              NodeD^.mParmList[I].mParmName := 'Parm' + inttostr(I);
              NodeD^.mParmList[I].mParmType := 'string';
              NodeD^.mParmList[I].mParmDefaultValue := '';
              NodeD^.mParmList[I].mParmDescription := 'Name of Mailbox';
              NodeD^.mParmList[I].mParmIsDelete := false;
              NodeD^.mParmList[I].mParmCreateDate := now;
            end;
          1:
            begin
              NodeD^.mParmList[I].mParmID := I;
              NodeD^.mParmList[I].mParmName := 'Parm' + inttostr(I);
              NodeD^.mParmList[I].mParmType := 'integer';
              NodeD^.mParmList[I].mParmDefaultValue := '0';
              NodeD^.mParmList[I].mParmDescription := 'Mailbox Count';
              NodeD^.mParmList[I].mParmIsDelete := false;
              NodeD^.mParmList[I].mParmCreateDate := now;
            end;
          2:
            begin
              NodeD^.mParmList[I].mParmID := I;
              NodeD^.mParmList[I].mParmName := 'Parm' + inttostr(I);
              NodeD^.mParmList[I].mParmType := 'Date/Time';
              NodeD^.mParmList[I].mParmDefaultValue := datetimetostr(now);
              NodeD^.mParmList[I].mParmDescription := 'Mailbox Last Modified';
              NodeD^.mParmList[I].mParmIsDelete := false;
              NodeD^.mParmList[I].mParmCreateDate := now;
            end;
          end;
      end;
end;

procedure TfrmDssAdhocTool.WhatButtons();
begin

 if not fbolInEditState then
  begin
    btnEdit.Caption := 'Edit Selected';
    btnDeleteSelected.Caption := 'Delete Selected';
    btnRun.Enabled := true;
    btnNewReport.Enabled := true;
    self.caption := 'Adhoc Reporting Tool';
  end
  else
  begin
    btnEdit.Caption := 'Save Selected';
    btnDeleteSelected.Caption := 'Cancel Edit';
    btnRun.Enabled := false;
    btnNewReport.Enabled := false;
    self.caption := 'Edit Mode Adhoc Reporting Tool';
  end;


end;

procedure TfrmDssAdhocTool.WhatColor();
begin
  if not fbolInEditState then
  begin
    TntRichEditSQL.Color := cl3DLight;
    TntRichEditParmList.Color := cl3DLight;
    TntRichEditDescription.Color := cl3DLight;
    LabeledEditReportOwner.Color := cl3DLight;
    LabeledEditCreateDate.Color := cl3DLight;
    LabeledEditlastModifiedDate.Color := cl3DLight;
    LabeledEditFileLocation.Color := cl3DLight;
    LabeledEditFileName.Color := cl3DLight;
    cbShared.Enabled := false;
    cbIsDeleted.Enabled := false;
    cbFileLocked.Enabled := false;
    cbCustomQuery.Enabled := false;
  end
  else
  begin
    TntRichEditSQL.Color := clWhite;
    TntRichEditParmList.Color := clWhite;
    TntRichEditDescription.Color := clWhite;
    LabeledEditReportOwner.Color := clWhite;
    LabeledEditCreateDate.Color := clWhite;
    LabeledEditlastModifiedDate.Color := clWhite;
    LabeledEditFileLocation.Color := clWhite;
    LabeledEditFileName.Color := clWhite;
    cbShared.Enabled := true;
    cbIsDeleted.Enabled := true;
    cbFileLocked.Enabled := true;
    cbCustomQuery.Enabled := true;
  end;
end;

procedure TfrmDssAdhocTool.SetEditState();
begin

//switch edit state
fbolInEditState := not fbolInEditState;



//set the state colors
WhatColor();
//set the state buttons
WhatButtons();


//if fbolInEditState then not fbolInEditState;
//showmessage(booltostr(fbolInEditState));
end;

procedure TfrmDssAdhocTool.FormCreate(Sender: TObject);
begin
//set the system we need to use. hard coded until I need to make production ready
fstrSystem := 'TESTGLOBAL';
//default to old
fbolAreWeNew := false;
//not in edit state
fbolInEditState := false;
//lets grab all the data we are going to need to run and or edit a report
LoadTree();
//change color based on system
WhatColor();
//what buttons need to say what and when they need to say it.
WhatButtons();
end;

procedure TfrmDssAdhocTool.ClearFields();
 begin
  try
    //I am doing this so often I needed to refactor it to its own method
    //that's much better.
    TntRichEditSQL.Clear;
    TntRichEditParmList.Clear;
    TntRichEditDescription.Clear;
    LabeledEditReportOwner.Clear;
    LabeledEditCreateDate.Clear;
    LabeledEditlastModifiedDate.Clear;
    LabeledEditFileLocation.Clear;
    LabeledEditFileName.Clear;
    cbShared.Checked := false;
    cbIsDeleted.Checked := false;
    cbFileLocked.Checked := false;
    cbCustomQuery.Checked := false;

  Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;

Procedure TfrmDssAdhocTool.PointToRecord;
Var
  Node: PVirtualNode;
  NodeD: pTreeData;
  i: integer;
begin
  Try
    Node := VirtualStringTreeMain.GetFirstSelected();
    If Node = Nil then
      Exit;
    ClearFields();

    NodeD := VirtualStringTreeMain.GetNodeData(Node);


    LabeledEditReportOwner.Text   := NodeD.mOwner;
    LabeledEditCreateDate.Text := FormatDateTime('ddddd', NodeD.mCreateDate);
    TntRichEditDescription.Text := NodeD^.mDescription;
    LabeledEditlastModifiedDate.Text := FormatDateTime('ddddd', NodeD.mLastmodified);
    LabeledEditFileLocation.Text := NodeD^.mLocation;
    LabeledEditFileName.Text := NodeD^.mFileName;
    cbShared.Checked := NodeD^.mShared;
    cbIsDeleted.Checked := NodeD^.mIsDelete;
    cbFileLocked.Checked := NodeD^.mIsLocked;
    cbCustomQuery.Checked := NodeD^.mIsCustomQuery;
    TntRichEditSQL.Text := NodeD^.mSQLText;
     TntRichEditParmList.Clear;
    if NodeD^.mHasParams then
    begin
      for I := 0 to high(NodeD^.mParmList) do
      begin
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmName);
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmType);
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmDescription);
        TntRichEditParmList.Lines.Add('-------------------------------------------');
      end;
    end;



  Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;

//procedure FetchReports();
//begin
//end;


procedure TfrmDssAdhocTool.LoadTree();
var
NodeD: pTreeData;
Node: PVirtualNode;
strResult: string;
intIsShared, intIsDeleted, intIsCustomSQL, intIsLocked: integer;
begin
  try
  intIsShared := 0;
  intIsDeleted := 0;
  intIsCustomSQL := 0;
  intIsLocked := 0;


  //lets add this to the database.
  //
   //size the node data container to the size of my record
   VirtualStringTreeMain.NodeDataSize := SizeOf( TTreeData );
   //grab the templates
   //dmContext.FetchAllTemplates();
   strResult := dmRbStudy.FetchAllReports();


  if trim(strResult) <> '' then
  begin
    showmessage(strResult);
  end;

  While Not dmRbStudy.qryMain.Eof do
  begin
    //addnode
    Node := VirtualStringTreeMain.AddChild(Nil);
    //initilize node
    NodeD := VirtualStringTreeMain.GetNodeData(Node);
    NodeD^.mReportID := dmRbStudy.qryMain.FieldByName('reportid').AsInteger;
    NodeD^.mReportName := dmRbStudy.qryMain.FieldByName('reportname').AsString;
    NodeD^.mLocation := dmRbStudy.qryMain.FieldByName('filelocation').AsString;
    NodeD^.mFileName := dmRbStudy.qryMain.FieldByName('filename').AsString;
    NodeD^.mOwner := dmRbStudy.qryMain.FieldByName('Owner').AsString;
    NodeD^.mSQLText := dmRbStudy.qryMain.FieldByName('sqltext').AsString;
    NodeD^.mDescription := dmRbStudy.qryMain.FieldByName('description').AsString;

    intIsShared := dmRbStudy.qryMain.FieldByName('description').AsInteger;
    intIsDeleted := dmRbStudy.qryMain.FieldByName('description').AsInteger;
    intIsCustomSQL := dmRbStudy.qryMain.FieldByName('description').AsInteger;
    intIsLocked := dmRbStudy.qryMain.FieldByName('description').AsInteger;

    if intIsShared = 0 then
    begin
      NodeD^.mShared := FALSE;
    end
    else
    begin
      NodeD^.mShared := true;
    end;

    if intIsDeleted = 0 then
    begin
      NodeD^.mIsDelete := FALSE;
    end
    else
    begin
      NodeD^.mIsDelete := true;
    end;

    if intIsCustomSQL = 0 then
    begin
      NodeD^.mIsCustomQuery := FALSE;
    end
    else
    begin
      NodeD^.mIsCustomQuery := true;
    end;

    if intIsLocked = 0 then
    begin
      NodeD^.mIsLocked := FALSE;
    end
    else
    begin
      NodeD^.mIsLocked := true;
    end;

    NodeD^.mCreateDate := dmRbStudy.qryMain.FieldByName('createdate').AsDateTime;
    NodeD^.mLastmodified := dmRbStudy.qryMain.FieldByName('lastmodified').AsDateTime;
    NodeD^.mHasParams := false;
    VirtualStringTreeMain.InvalidateNode(Node);
    dmRbStudy.qryMain.Next;
  end;

        //While Not dmContext.IfxQuery1.Eof do
        //begin
        //addnode
        Node := VirtualStringTreeMain.AddChild(Nil);
        //initilize node
        NodeD := VirtualStringTreeMain.GetNodeData(Node);
        //build node data
        NodeD^.mReportID := 17643;
        NodeD^.mReportName := 'Cost per booking';
        NodeD^.mLocation := 'C:\playground\reports' + '\';
        NodeD^.mFileName := 'test' + '.rtm';
        NodeD^.mOwner := 'JRODIMON';
        NodeD^.mSQLText := 'Select * from imc_mailaccounts';
        NodeD^.mDescription := 'Show me all items in the table regardless';
        NodeD^.mShared := true;
        NodeD^.mIsDelete := false;
        //unicode is utf8 encoded so lets decode to view.
        NodeD^.mIsCustomQuery := True;
        NodeD^.mIsLocked := False;
        NodeD^.mCreateDate := now;
        NodeD^.mLastmodified := now;
        NodeD^.mHasParams := true;
        VirtualStringTreeMain.InvalidateNode(Node);


        if NodeD^.mHasParams then
        begin
         LoadParameters(NodeD);
        end;


        Node := VirtualStringTreeMain.AddChild(Nil);
        //initilize node
        NodeD := VirtualStringTreeMain.GetNodeData(Node);
        //build node data
        NodeD^.mReportID := 17644;
        NodeD^.mReportName := 'Grab ID';
        NodeD^.mLocation := 'C:\playground\reports' + '\';
        NodeD^.mFileName := 'wid' + '.rtm';
        NodeD^.mOwner := 'JRODIMON';
        NodeD^.mSQLText := 'Select * from imc_resolvestatuses';
        NodeD^.mDescription := 'Show me all Id''s';
        NodeD^.mShared := true;
        NodeD^.mIsDelete := false;
        //unicode is utf8 encoded so lets decode to view.
        NodeD^.mIsCustomQuery := True;
        NodeD^.mIsLocked := False;
        NodeD^.mCreateDate := now;
        NodeD^.mLastmodified := now;
        NodeD^.mHasParams := false;
        if NodeD^.mHasParams then
        begin
         LoadParameters(NodeD);
        end;
        VirtualStringTreeMain.InvalidateNode(Node);
        //dmContext.IfxQuery1.Next;
   //end;

 Except on e: exception do
    begin
     showmessage('Error: Module-->LoadTree: ' + e.Message);
    // RAISE;
    End;
   end;
end;

procedure TfrmDssAdhocTool.mnuAboutClick(Sender: TObject);
begin
//
end;

procedure TfrmDssAdhocTool.mnuCloseClick(Sender: TObject);
begin
  btnCloseClick(Sender);
end;

procedure TfrmDssAdhocTool.VirtualStringTreeMainFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
begin
try
   If Node = Nil then
      Exit;
     VirtualStringTreeMain.Selected[Node] := True;
      PointToRecord;
      fbolAreWeNew := false;
     ///lets get the meta and populate this
 Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;

procedure TfrmDssAdhocTool.VirtualStringTreeMainGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
Var
  NodeD: pTreeData;
begin
  Try
   // If Column = 1 then
      //Begin
        //add the correct image currently we have only one.
        NodeD := VirtualStringTreeMain.GetNodeData(Node);
        CellText := NodeD^.mReportName;
      //end;

  Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;
procedure TfrmDssAdhocTool.btnCloseClick(Sender: TObject);
begin
self.Close;
end;

procedure TfrmDssAdhocTool.btnDeleteSelectedClick(Sender: TObject);
begin

  //we need to flip the state only if in edit mode
  if  btnDeleteSelected.Caption = 'Cancel Edit' then
  begin
    SetEditState();
  end;
end;

procedure TfrmDssAdhocTool.btnEditClick(Sender: TObject);
begin
  //flip state in or out of edit mode
  SetEditState;
end;

procedure TfrmDssAdhocTool.btnNewReportClick(Sender: TObject);
begin
 try
    try
              Application.CreateForm(TfrmNewReportWizard, frmNewReportWizard);
              frmNewReportWizard.NodeData := fNewNodeD;
              frmNewReportWizard.Showmodal;
            finally
              try
                freeandnil(frmNewReportWizard);

              except;
              end;
    end

  Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;

procedure TfrmDssAdhocTool.btnRunClick(Sender: TObject);
Var
  Node: PVirtualNode;
  NodeD: pTreeData;
  begin
  Try
    try
    Node := VirtualStringTreeMain.GetFirstSelected();
    If Node = Nil then
      Exit;



      NodeD := VirtualStringTreeMain.GetNodeData(Node);

      if NodeD^.mHasParams then
      begin
        Application.CreateForm(TfrmDynamicParams, frmDynamicParams);
        frmDynamicParams.NodeD := NodeD;
        frmDynamicParams.Showmodal;
        //showmessage('Has parameters');
      end
      else
      begin
       showmessage('no parameters');
      end;
      
      Set_IDAC_Connection(IfxConnection1, 'POPTART', '', true);
      IfxConnection1.KeepConnection := false;
      IfxConnection1.Open;
      IfxQuery1.SQL.Clear;
      IfxQuery1.SQL.text :=  NodeD^.mSQLText;
      IfxQuery1.Open;


      ppreport1.Template.FileName := NodeD^.mLocation + NodeD^.mFileName;
      ppReport1.Template.LoadFromFile;
      //ppreport1.TextFileName  := NodeD^.mLocation + NodeD^.mFileName;
      //ppreport1.DataPipelineDataPipeline := nil;
      ppreport1.DataPipeline := ppDBPipeline1;
     // ppDBPipeline1.Open;
      ppreport1.Print;

    Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
finally
    if NodeD^.mHasParams then
    begin
      FreeAndNil(frmDynamicParams);
    end;
  end;
end;

procedure TfrmDssAdhocTool.btnEditLayoutClick(Sender: TObject);
Var
  Node: PVirtualNode;
  NodeD: pTreeData;
  begin
  Try
    Node := VirtualStringTreeMain.GetFirstSelected();
    If Node = Nil then
      Exit;

      NodeD := VirtualStringTreeMain.GetNodeData(Node);
      Set_IDAC_Connection(IfxConnection1, 'POPTART', '', true);
      IfxConnection1.KeepConnection := false;
      IfxConnection1.Open;
      IfxQuery1.SQL.Clear;

        {'C:\playground\reports' + '\';
        NodeD^.mFileName := 'test' + '.rtm';}

      IfxQuery1.SQL.text := NodeD^.mSQLText;//'Select * from imc_resolvestatuses';
      IfxQuery1.Open;
      ppreport1.DataPipeline := ppDBPipeline1;
      //ppreport1.Template.FileName := 'C:\playground\reports\test.rtm';
      ppreport1.Template.FileName := NodeD^.mLocation + NodeD^.mFileName;
      ppReport1.Template.LoadFromFile;
      ppDBPipeline1.Open;

      //ppDesigner1.;
      ppDesigner1.Show;

Except on E:Exception do
    Begin
      ShowMessage(e.Message);
    End;
  End;
end;

end.
