unit rbStudyDM;

interface

uses
  SysUtils, Classes, DB, IfxCustomDataSet, IfxQuery, IfxConnection,
  aecommonfunc_II, ReportToolGlobals;

type
  TdmRbStudy = class(TDataModule)
    conn: TIfxConnection;
    qryMain: TIfxQuery;
  private
    { Private declarations }
  public
    //select
    function FetchAllReports(): string; overload;
    function FetchAllReports(Netlogon: string): string; overload;
    //delete
    function DeleteReport(id: integer): string; overload;
    function DeleteReport(Netlogon: string): string; overload;
    //create
    function AddNewReport(data: TTreeData): string;
    //update
    function UpdateReport(data: TTreeData): string;

    { Public declarations }
  end;

var
  dmRbStudy: TdmRbStudy;

implementation

uses
rbStudy;

function TdmRbStudy.UpdateReport(data: TTreeData): string;
var
strSQL: string;
begin
  try

  Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'UPDATE '
    + 'aeadhocreport '
    + 'SET '
    + 'reportname = :reportname, '
    + 'filelocation = :filelocation, '
    + 'filename = :filename, '
    + 'Owner = :Owner, '
    + 'sqltext = :sqltext, '
    + 'description = :description, '
    + 'isshared = :isshared, '
    + 'isdeleted = :isdeleted, '
    + 'iscustomsql = :iscustomsql, '
    + 'islocked = :islocked, '
    + 'lastmodified = Current';


    qryMain.SQL.Text := trim(strSQL);
    qryMain.ParamByName('reportname').asString := data.mReportName;
    qryMain.ParamByName('filelocation').asString := data.mLocation;
    qryMain.ParamByName('filename').asString := data.mFileName;
    qryMain.ParamByName('Owner').asString := data.mOwner;
    qryMain.ParamByName('sqltext').asString := data.mSQLText;
    qryMain.ParamByName('description').asString := data.mDescription;

    if data.mShared then
    begin
      qryMain.ParamByName('isshared').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('isshared').asInteger := 0;
    end;

     if data.mIsCustomQuery then
    begin
      qryMain.ParamByName('iscustomsql').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('iscustomsql').asInteger := 0;
    end;

    if data.mIsLocked then
    begin
      qryMain.ParamByName('islocked').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('islocked').asInteger := 0;
    end;

    if data.mIsDelete then
    begin
      qryMain.ParamByName('isdeleted').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('isdeleted').asInteger := 0;
    end;


    qryMain.ExecSQL;
//
//
  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;

function TdmRbStudy.AddNewReport(data: TTreeData): string;
var
strSQL: string;
begin
  try

  Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'INSERT INTO '
    + 'aeadhocreport '
    + '( '
    + 'reportname, '
    + 'filelocation, '
    + 'filename, '
    + 'Owner, '
    + 'sqltext, '
    + 'description, '
    + 'isshared, '
    + 'isdeleted, '
    + 'iscustomsql, '
    + 'islocked, '
    + 'lastmodified, '
    + 'createdate '
    + ') '
    + 'VALUES '
    + '( '
    + ':reportname, '
    + ':filelocation, '
    + ':filename, '
    + ':Owner, '
    + ':sqltext, '
    + ':description, '
    + ':isshared, '
    + '0, '
    + ':iscustomsql, '
    + '0, '
    + 'current, '
    + 'current '
    + ') ';

    qryMain.SQL.Text := trim(strSQL);
    qryMain.ParamByName('reportname').asString := data.mReportName;
    qryMain.ParamByName('filelocation').asString := data.mLocation;
    qryMain.ParamByName('filename').asString := data.mFileName;
    qryMain.ParamByName('Owner').asString := data.mOwner;
    qryMain.ParamByName('sqltext').asString := data.mSQLText;
    qryMain.ParamByName('description').asString := data.mDescription;

    if data.mShared then
    begin
      qryMain.ParamByName('isshared').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('isshared').asInteger := 0;
    end;

     if data.mIsCustomQuery then
    begin
      qryMain.ParamByName('iscustomsql').asInteger := 1;
    end
    else
    begin
      qryMain.ParamByName('iscustomsql').asInteger := 0;
    end;


    qryMain.ExecSQL;
//
//
  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;

function TdmRbStudy.DeleteReport(id: integer): string;
var
strSQL: string;
begin
  try

  Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'Delete '
    + 'FROM '
    + 'aeadhocreport '
    + 'WHERE '
    + 'reportid = :id';

    qryMain.SQL.Text := trim(strSQL);
    qryMain.ParamByName('id').asInteger := id;
    qryMain.ExecSQL;
//
//
  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;

function TdmRbStudy.DeleteReport(Netlogon: string): string;
var
strSQL: string;
begin
  try

 Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'Delete '
    + 'FROM '
    + 'aeadhocreport '
    + 'WHERE '
    + 'Owner = :netlogon';

    qryMain.SQL.Text := trim(strSQL);
    qryMain.ParamByName('netlogon').asString := Netlogon;
    qryMain.ExecSQL;
//
  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;


function TdmRbStudy.FetchAllReports(): string;
var
strSQL: string;
begin
  try

  Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'SELECT '
    + 'reportid, '
    + 'reportname, '
    + 'filelocation, '
    + 'filename, '
    + 'Owner, '
    + 'sqltext, '
    + 'description, '
    + 'isshared, '
    + 'isdeleted, '
    + 'iscustomsql, '
    + 'islocked, '
    + 'lastmodified, '
    + 'createdate '
    + 'FROM '
    + 'aeadhocreport '
    + 'ORDER BY reportname';

    qryMain.SQL.Text := trim(strSQL);
    qryMain.open;

  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;

function TdmRbStudy.FetchAllReports(Netlogon: string): string;
var
strSQL: string;
begin
  try

  Set_IDAC_Connection(conn, frmDssAdhocTool.BaseSystem);

  strSQL := 'SELECT '
    + 'reportid, '
    + 'reportname, '
    + 'filelocation, '
    + 'filename, '
    + 'Owner, '
    + 'sqltext, '
    + 'description, '
    + 'isshared, '
    + 'isdeleted, '
    + 'iscustomsql, '
    + 'islocked, '
    + 'lastmodified, '
    + 'createdate '
    + 'FROM '
    + 'aeadhocreport '
    + 'WHERE '
    + 'Owner = :netlogon'
    + 'ORDER BY reportname';

    qryMain.SQL.Text := trim(strSQL);
    qryMain.ParamByName('netlogon').asString := Netlogon;
    qryMain.open;

  except on e: exception do
    begin
      result := e.Message;
    end;
  end;
end;

{$R *.dfm}

end.
