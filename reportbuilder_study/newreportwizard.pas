unit newreportwizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TntComCtrls, ExtCtrls,
  ReportToolGlobals,
  ppReport,
  ppBands,
  ppCtrls,
  ppTypes,
  ppVar,
  rbStudyDM;

type
  TfrmNewReportWizard = class(TForm)
    PanelBottom: TPanel;
    PanelBody: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    LabeledEditReportName: TLabeledEdit;
    TabSheet2: TTabSheet;
    TntRichEditSQL: TTntRichEdit;
    TntRichEditDescription: TTntRichEdit;
    Shared: TCheckBox;
    Button1: TButton;
    btnClose: TButton;
    TabSheet3: TTabSheet;
    Panelpbottom: TPanel;
    PanelPTop: TPanel;
    LabeledEditPNmae: TLabeledEdit;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    LabeledEditDefaultValue: TLabeledEdit;
    Button3: TButton;
    ListViewParam: TListView;
    Label3: TLabel;
    btnSaveTemplate: TButton;
    cbDictionary: TComboBox;
    Label4: TLabel;
    cbDateStamp: TCheckBox;
    cbPageNum: TCheckBox;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSaveTemplateClick(Sender: TObject);
  private
    fNodeData: pTreeData; { Private declarations }
  public
    property NodeData: pTreeData read fNodeData write fNodeData;
    { Public declarations }
  end;

var
  frmNewReportWizard: TfrmNewReportWizard;

implementation
uses
rbStudy;

{$R *.dfm}

procedure TfrmNewReportWizard.btnCloseClick(Sender: TObject);
begin
self.Close;
end;

procedure TfrmNewReportWizard.btnSaveTemplateClick(Sender: TObject);
var
lSysVar: TppSystemVariable;
lReport: TppReport;
begin
  if trim(LabeledEditReportName.Text) = '' then
  begin
    showmessage('Please give the report a name.');
    LabeledEditReportName.SetFocus;
    exit;
  end;

  if trim(cbDictionary.Text) = '' then
  begin
    showmessage('Please select a dictioary.');
    cbDictionary.SetFocus;
    exit;
  end;
  lReport := TppReport.Create(Self);
  lReport.Name :=  'adhoc';
  lReport.TextFileName := LabeledEditReportName.Text + '.rtm';
  lReport.SaveAsTemplate := true;
  lReport.CreateDefaultBands;



  if trim(cbDictionary.Text) = 'Default Dictionary' then
  begin
    lReport.DataPipeline := frmDssAdhocTool.ppDBPipeline1;
  end
  else
  begin
    Showmessage('another dictionary');
  end;

  if cbDateStamp.Checked then
  begin
    lSysVar := TppSystemVariable.Create(self);
    lSysVar.Band := lReport.FooterBand;
    lSysVar.VarType := vtPrintDateTime;
    lSysVar.spLeft := 2;
    lSysVar.spTop := 2;
  end;

  if cbPageNum.Checked then
  begin
    lSysVar := TppSystemVariable.Create(self);
    lSysVar.Band := lReport.FooterBand;
    lSysVar.VarType := vtPageNoDesc;
    lSysVar.Alignment := taRightJustify;
    lSysVar.spLeft := (lReport.PrinterSetup.PageDef.spPrintableWidth
    -lSysVar.spWidth) -2; lSysVar.spTop := 2;
  end;


  //lets build the insert statemewnt here
  lReport.Template.FileName := LabeledEditReportName.Text + '.rtm';
  lReport.Template.SaveToFile;
 //cbPageNum


end;

end.
