unit aeHeaders;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmHeader = class(TForm)
    PanelBottom: TPanel;
    PanelBody: TPanel;
    btnCancel: TButton;
    btnApply: TButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHeader: TfrmHeader;

implementation

{$R *.dfm}

procedure TfrmHeader.btnApplyClick(Sender: TObject);
begin
//
end;

procedure TfrmHeader.btnCancelClick(Sender: TObject);
begin
  self.close;
end;

end.
