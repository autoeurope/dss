unit aeDynamicParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ReportToolGlobals, ExtCtrls, StdCtrls, ComCtrls, TntComCtrls;

type
  TfrmDynamicParams = class(TForm)
    PanelBottom: TPanel;
    PanelTop: TPanel;
    PanelBody: TPanel;
    btnClose: TButton;
    TntRichEditParmList: TTntRichEdit;
    procedure btnCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

    { Private declarations }
  public
    NodeD: pTreeData;
    //property Parms: array of TAeParams read farParms write farParms;
    { Public declarations }
  end;

var
  frmDynamicParams: TfrmDynamicParams;

implementation

{$R *.dfm}

procedure TfrmDynamicParams.btnCloseClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmDynamicParams.FormShow(Sender: TObject);
var
I: integer;
begin
TntRichEditParmList.Clear;
    if NodeD^.mHasParams then
    begin
      for I := 0 to high(NodeD^.mParmList) do
      begin
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmName);
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmType);
        TntRichEditParmList.Lines.Add(NodeD^.mParmList[I].mParmDescription);
        TntRichEditParmList.Lines.Add('-------------------------------------------');
      end;
    end;
end;

end.
