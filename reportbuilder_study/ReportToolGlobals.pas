unit ReportToolGlobals;

interface

type
  //pAeParams = ^TAeParams;
  TAeParams = record
    mParmID: integer;
    mParmName: widestring;
    mParmType: widestring;
    mParmDefaultValue: widestring;
    mParmDescription: widestring;
    mParmIsDelete: Boolean;
    mParmCreateDate: TDateTime;
  end;

  


type
  pTreeData = ^TTreeData;
  TTreeData = record
    mReportID: integer;
    mReportName: widestring;
    mLocation: widestring;
    mOwner: widestring;
    mSQLText: widestring;
    mDescription: widestring;
    mFileName: widestring;
    mShared: Boolean;
    mIsDelete: Boolean;
    mIsCustomQuery: Boolean;
    mIsLocked: Boolean;
    mCreateDate: TDateTime;
    mLastmodified: TDateTime;
    mHasParams: boolean;
    mParmList: array of TAeParams;
  end;

  const
  LOAD_REPORT_SQL = '';


implementation

end.
