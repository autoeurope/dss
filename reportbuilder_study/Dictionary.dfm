object frmDictionary: TfrmDictionary
  Left = 0
  Top = 0
  Caption = 'frmDictionary'
  ClientHeight = 299
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBottom: TPanel
    Left = 0
    Top = 258
    Width = 500
    Height = 41
    Align = alBottom
    Caption = 'PanelBottom'
    TabOrder = 0
    ExplicitLeft = 176
    ExplicitTop = 240
    ExplicitWidth = 185
    object btnSelect: TButton
      Left = 336
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Select'
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 422
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Cancel'
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object PanelBody: TPanel
    Left = 0
    Top = 0
    Width = 500
    Height = 258
    Align = alClient
    Caption = 'PanelBody'
    TabOrder = 1
    ExplicitLeft = 264
    ExplicitTop = 192
    ExplicitWidth = 185
    ExplicitHeight = 41
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 498
      Height = 256
      Align = alClient
      Columns = <
        item
          Caption = 'Dictionary'
          Width = 150
        end
        item
          Caption = 'Description'
          Width = 333
        end>
      TabOrder = 0
      ViewStyle = vsReport
      ExplicitLeft = 144
      ExplicitTop = 80
      ExplicitWidth = 250
      ExplicitHeight = 150
    end
  end
end
