//06/05/2012 https://autoeurope.jira.com/browse/DSS-7
//05/30/2012 https://autoeurope.jira.com/browse/DSS-2
//05/30/2012 https://autoeurope.jira.com/browse/DSS-3
//05/30/2012 https://autoeurope.jira.com/browse/DSS-4
//05/30/2012 https://autoeurope.jira.com/browse/DSS-5
//05/15/2012 adj retail --> 'sum(vs_base_rate_retail + vs_tax_tot_retail + vs_insurance_tot_retail) - sum(vs_discount_retail) adjusted_retail_total, ' +
//05/15/2012 Retail Total --> 'sum(vs_base_rate_retail + vs_tax_tot_retail + vs_insurance_tot_retail) retail_total, '
//05/08/2012 literal '###,###,##0.00' used about 50 times changed to the constant
//           _NUM_FORMAT_EIGHT_POUND
//05/08/2012 renamed the Boolean variable crap to bolAllChecked in method
//           GetSelectedBaseSysList
//05/08/2012 literal '###,##0.00' used about 50 times changed to the constant
//           _NUM_FORMAT_FIVE_POUND
//05/08/2012 renamed index ass to idxmem in the SetUpMemTable Method
//05/08/2012 renamed the dataset variable ass to dataSetCountry in the  getcountries
//           method
//05/08/2012 renamed the variable ass to strUser  int the FormCreate Method
//05/08/2012 renamed the following buttons for readability and ease of debugging:
//           Button32 = btnMainFill
//           Button9 = btnFzTest
//           Button5 = btnFzClear
//           Button4 = btnFzBuildSQL
//           btnSippAnalSIPP = btnSippAnalysisSIPP
//           btnSippAnal = btnSippAnalysis
//           btnTest = btnDumpGrid
//           Button31 = btnSpSet
//           Button11 = btnVoGetData
//           Button1 = btnCSV_HTML
//           Button3 = btnCSV_XML
//           OK_NEW = btnOK_NEW
//           Button26 = btnGRDKill
//           Button10 = btnGRDReset
//           btnReFuckingFresh = btnReFreshData
//           Button2 =  btnStepTwo
//           Button16 = btnAllPayTypes
//           Button17 = btnNoPayTypes
//           Button18 = btnAllSippsList1
//           Button19 = btnNoSippsList1
//           Button21 = btnAllSippsList2
//           Button20 = btnNoSippsList2
//           Button22 = btnAllSippsList3
//           Button23 = btnNoSippsList3
//           Button24 = btnAllSippsList4
//           Button25 = btnNoSippsList4
//           Button28 = btnDbAll
//           Button29 = btnDbClear
//05/07/2012 renamed dbgrid1 to DBGridOutput for readability and ease of debugging.
//05/07/2012 added an about form
//05/07/2012 added a menu system so we can call close or help from there instead
//           of a different button per tap.
//05/07/2012 removed all the hard coded form sizes depending on the selected tab
//           I replaced them with Constants that are easy to locate and change
//05/07/2012 removed the shell to the windows api for the help file and added a form with the web control
//           This way I can lock the browser completely down. and not trap com errors
//05/07/2012 renamed the default form and subsequent calls to frmMainMkt from form1
//05/07/2012 added the aeDssHelp form mapped to the existing HTML help
unit main_mkt;

interface

uses
  //system stuff
  Windows
  , Messages
  , SysUtils
  , Variants
  , Classes
  , Graphics
  , Controls
  , Forms
  , Dialogs
  , shellapi
  , dscalc
  , jxml
  , math
  , StdCtrls
  , ComCtrls
  , ExtCtrls
  , DB
  , Menus
  , Grids
  , DBGrids
  , Buttons
  , CheckLst
  , strutils
  ,newaeFireDacHelper
  //AE stuff
  ,aecommonfunc_II
  , kbmMemTable

  //ReportBuilder stuff
  , ppBands
  , ppDB
  , ppParameter
  , ppClass
  , ppCtrls
  , ppVar
  , ppPrnabl
  , ppCache
  , ppProd
  , ppReport
  , ppComm
  , ppRelatv
  , ppDBPipe
  , jvComboBox


  //Indy stuff
  , IdTCPConnection
  , IdTCPClient
  , IdExplicitTLSClientServerBase
  , IdMessageClient
  , IdSMTPBase
  , IdSMTP
  , IdBaseComponent
  , IdMessage
  , IdAttachmentFile
  , IdComponent

  //custom component stuff?
 { , cxGraphics
  , cxControls
  , cxContainer
  , cxEdit
  , cxTextEdit
  , cxMaskEdit
  , cxDropDownEdit
  , cxCheckComboBox
  , cxCheckBox
  , cxLookAndFeels
  , cxLookAndFeelPainters
  , dplusdisc
  , dxSkinsCore
  , dxSkinBlack
  , dxSkinBlue
  , dxSkinCaramel
  , dxSkinCoffee
  , dxSkinDarkRoom
  , dxSkinDarkSide
  , dxSkinFoggy
  , dxSkinGlassOceans
  , dxSkiniMaginary
  , dxSkinLilian
  , dxSkinLiquidSky
  , dxSkinLondonLiquidSky
  , dxSkinMcSkin
  , dxSkinMoneyTwins
  , dxSkinOffice2007Black
  , dxSkinOffice2007Blue
  , dxSkinOffice2007Green
  , dxSkinOffice2007Pink
  , dxSkinOffice2007Silver
  , dxSkinOffice2010Black
  , dxSkinOffice2010Blue
  , dxSkinOffice2010Silver
  , dxSkinPumpkin
  , dxSkinSeven
  , dxSkinSharp
  , dxSkinSilver
  , dxSkinSpringTime
  , dxSkinStardust
  , dxSkinSummer2008
  , dxSkinsDefaultPainters
  , dxSkinValentine
  , dxSkinXmas2008Blue  }

  //project stuff
  , aeDssHelp
  , UnitAbout
  , ReportConstants,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, ppDesignLayer, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.VCLUI.Wait, FireDAC.Moni.Base, FireDAC.Moni.Custom, FireDAC.Comp.UI,FireDAC.Phys.ODBCBase,
  FireDAC.Phys.Infx,  Vcl.Mask, JvExMask, JvToolEdit, FireDAC.Stan.StorageBin;



type
  TfrmMainMkt = class(TForm)
    PageControl1: TPageControl;
    sb: TStatusBar;
    tabStep1: TTabSheet;

    conn: TFDConnection;
    Q1: TFDQuery;
    DataSource1: TDataSource;
    tabData: TTabSheet;
    DBGridOutput: TDBGrid;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    dtpStart: TDateTimePicker;
    Label11: TLabel;
    dtpEnd: TDateTimePicker;
    Panel1: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    Label23: TLabel;
    Label33: TLabel;
    EditCountry: TEdit;
    Label34: TLabel;
    EditCity: TEdit;
    lblConsort: TLabel;
    Msg: TIdMessage;
    SMTP: TIdSMTP;
    pipeTrack: TppDBPipeline;
    reportTrack: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppLabel11: TppLabel;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    ppDBText12: TppDBText;
    ppLabel13: TppLabel;
    ppDBText13: TppDBText;
    ppLine1: TppLine;
    ppTitleBand1: TppTitleBand;
    ppLabel14: TppLabel;
    lblDate: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText14: TppDBText;
    ppLine2: TppLine;
    ppDBCalc1: TppDBCalc;
    asdf: TppLabel;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppSummaryBand1: TppSummaryBand;
    ppDBCalc3: TppDBCalc;
    lblLast: TppLabel;
    mem: TkbmMemTable;
    dsc: TDSCalc;
    jonLbl: TLabel;
    EditOperator: TEdit;
    Label36: TLabel;
    pipeSales: TppDBPipeline;
    repSales: TppReport;
    dsSales: TDataSource;
    ppDetailBand2: TppDetailBand;
    ppTitleBand2: TppTitleBand;
    ppSummaryBand2: TppSummaryBand;
    ppHeaderBand2: TppHeaderBand;
    ppLabel15: TppLabel;
    ppDBText15: TppDBText;
    ppLabel16: TppLabel;
    ppDBText16: TppDBText;
    ppLabel17: TppLabel;
    ppDBText17: TppDBText;
    ppDBText18: TppDBText;
    ppLabel18: TppLabel;
    ppDBText19: TppDBText;
    ppLabel19: TppLabel;
    ppDBText21: TppDBText;
    ppLabel21: TppLabel;
    ppLabel22: TppLabel;
    ppLabel23: TppLabel;
    ppSystemVariable2: TppSystemVariable;
    lblTitle: TppLabel;
    ppDBText22: TppDBText;
    ppLabel24: TppLabel;
    ppDBText23: TppDBText;
    ppLabel25: TppLabel;
    ppLabel26: TppLabel;
    ppDBText24: TppDBText;
    ppLabel27: TppLabel;
    ppDBText25: TppDBText;
    ppLabel28: TppLabel;
    ppDBText26: TppDBText;
    ppLabel29: TppLabel;
    ppDBText27: TppDBText;
    ppDBText28: TppDBText;
    ppLabel30: TppLabel;
    ppLabel31: TppLabel;
    ppDBText29: TppDBText;
    ppLabel32: TppLabel;
    ppDBText30: TppDBText;
    ppLabel33: TppLabel;
    ppDBText31: TppDBText;
    ppLabel34: TppLabel;
    ppDBText32: TppDBText;
    ppLabel35: TppLabel;
    ppDBText33: TppDBText;
    ppLabel37: TppLabel;
    ppDBText35: TppDBText;
    ppLabel38: TppLabel;
    ppDBText36: TppDBText;
    ppLabel39: TppLabel;
    ppDBText37: TppDBText;
    ppLabel40: TppLabel;
    ppDBText38: TppDBText;
    ppLabel41: TppLabel;
    ppDBText39: TppDBText;
    ppGroup2: TppGroup;
    ppGroupHeaderBand2: TppGroupHeaderBand;
    ppGroupFooterBand2: TppGroupFooterBand;
    ppDBText41: TppDBText;
    ppDBCalc2: TppDBCalc;
    ppDBCalc5: TppDBCalc;
    ppDBCalc6: TppDBCalc;
    ppDBCalc7: TppDBCalc;
    ppLabel43: TppLabel;
    ppLabel44: TppLabel;
    ppLabel46: TppLabel;
    ppLabel47: TppLabel;
    ppLabel48: TppLabel;
    ppLabel50: TppLabel;
    ppLabel51: TppLabel;
    ppDBCalc8: TppDBCalc;
    ppDBCalc10: TppDBCalc;
    ppDBCalc11: TppDBCalc;
    ppLine5: TppLine;
    ppLine6: TppLine;
    ppLabel52: TppLabel;
    ppLabel53: TppLabel;
    ppDBCalc12: TppDBCalc;
    dsclemail: TDataSource;
    pipeclemail: TppDBPipeline;
    repclemail: TppReport;
    ppTitleBand3: TppTitleBand;
    ppLabel54: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppLabel55: TppLabel;
    ppHeaderBand3: TppHeaderBand;
    ppLabel56: TppLabel;
    ppLabel57: TppLabel;
    ppLabel62: TppLabel;
    ppLabel69: TppLabel;
    ppLabel70: TppLabel;
    ppLabel71: TppLabel;
    ppLabel76: TppLabel;
    ppLabel77: TppLabel;
    ppLabel79: TppLabel;
    ppLine7: TppLine;
    ppDetailBand3: TppDetailBand;
    ppDBText42: TppDBText;
    ppDBText43: TppDBText;
    ppDBText48: TppDBText;
    ppDBText55: TppDBText;
    ppDBText56: TppDBText;
    ppDBText57: TppDBText;
    ppDBText62: TppDBText;
    ppDBText63: TppDBText;
    ppDBText65: TppDBText;
    ppSummaryBand3: TppSummaryBand;
    ppLabel82: TppLabel;
    ppLabel83: TppLabel;
    ppLabel86: TppLabel;
    ppDBCalc13: TppDBCalc;
    ppDBCalc16: TppDBCalc;
    ppLabel87: TppLabel;
    ppDBCalc17: TppDBCalc;
    ppGroup3: TppGroup;
    ppGroupHeaderBand3: TppGroupHeaderBand;
    ppLabel88: TppLabel;
    ppLabel92: TppLabel;
    ppLine8: TppLine;
    ppGroupFooterBand3: TppGroupFooterBand;
    ppDBText68: TppDBText;
    ppDBCalc21: TppDBCalc;
    //extra: TExtraOptions;
    tabStep2: TTabSheet;
    gbPUD: TGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    dtpPUStart: TDateTimePicker;
    dtpPUEnd: TDateTimePicker;
    Label39: TLabel;
    cbUsePUDate: TCheckBox;
    GroupBox12: TGroupBox;
    Label42: TLabel;
    EditAgeStart: TEdit;
    EditAgeEnd: TEdit;
    cbUseAge: TCheckBox;
    GroupBox13: TGroupBox;
    clbPayTYpe2: TCheckListBox;
    btnAllPayTypes: TButton;
    btnNoPayTypes: TButton;
    cbUsePayType: TCheckBox;
    GroupBox14: TGroupBox;
    Label43: TLabel;
    EditDurLow: TEdit;
    EditDurHigh: TEdit;
    cbUseDuration: TCheckBox;
    GroupBox15: TGroupBox;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    clbSIPP1: TCheckListBox;
    clbSipp2: TCheckListBox;
    clbSIPP4: TCheckListBox;
    clbSIPP3: TCheckListBox;
    btnAllSippsList1: TButton;
    btnNoSippsList1: TButton;
    btnNoSippsList2: TButton;
    btnAllSippsList2: TButton;
    btnAllSippsList3: TButton;
    btnNoSippsList3: TButton;
    btnAllSippsList4: TButton;
    btnNoSippsList4: TButton;
    cbUseSIPP: TCheckBox;
    GroupBox16: TGroupBox;
    rgGender2: TRadioGroup;
    cbUseGender: TCheckBox;
    tabOutput: TTabSheet;
    Label48: TLabel;
    btnStepTwo: TButton;
    btnOK_NEW: TButton;
    cbUseSummaryTable: TCheckBox;
    tabstep2b: TTabSheet;
    GroupBox17: TGroupBox;
    cbFuturesOnly: TCheckBox;
    cbActiveOnly: TCheckBox;
    cbcancelsonly: TCheckBox;
    GroupBox11: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    Label49: TLabel;
    EditDaysBetween: TEdit;
    cbUseLastMin: TCheckBox;
    Panel3: TPanel;
    DstList: TListBox;
    SrcList: TListBox;
    ExAllBtn: TSpeedButton;
    ExcludeBtn: TSpeedButton;
    IncAllBtn: TSpeedButton;
    IncludeBtn: TSpeedButton;
    DstLabel: TLabel;
    SrcLabel: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    rgRestrictByEmail: TRadioGroup;
    rgDirectTa: TRadioGroup;
    gbDropOff: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    dtpDropDtStart: TDateTimePicker;
    dtpDropDtend: TDateTimePicker;
    EditDropCtry: TEdit;
    EditDropCity: TEdit;
    cbUseDropDate: TCheckBox;
    cbUseDropCtry: TCheckBox;
    cbUseDropCity: TCheckBox;
    btnGRDReset: TButton;
    Memo3: TMemo;
    Label15: TLabel;
    ppLabel20: TppLabel;
    ppDBText20: TppDBText;
    ppLabel94: TppLabel;
    ppDBText69: TppDBText;
    ppLabel36: TppLabel;
    ppLabel42: TppLabel;
    ppDBText34: TppDBText;
    ppDBText40: TppDBText;
    Panel4: TPanel;
    rgDSType: TRadioGroup;
    rgOutputTarget: TRadioGroup;
    rgDetailLevel: TRadioGroup;
    btnPrint: TButton;
    Panel5: TPanel;
    btnCSV_HTML: TButton;
    btnCSV: TButton;
    btnCSV_XML: TButton;
    Label18: TLabel;
    Label22: TLabel;
    ppDBText44: TppDBText;
    ppLabel58: TppLabel;
    ppDBText45: TppDBText;
    ppLabel59: TppLabel;
    ppLabel60: TppLabel;
    ppDBText46: TppDBText;
    ppDBText49: TppDBText;
    ppLabel63: TppLabel;
    ppLabel64: TppLabel;
    ppDBText50: TppDBText;
    ppLabel65: TppLabel;
    ppDBText51: TppDBText;
    btnHelp: TButton;
    mem2pass: TkbmMemTable;
    GroupBox1: TGroupBox;
    cbUseTYpes: TCheckBox;
    cbProductType: TComboBox;
    btnReFreshData: TBitBtn;
    cbProductSubType: TComboBox;
    CboxUseSource: TComboBox;
    Label29: TLabel;
    cbUseSource: TCheckBox;
    Panel6: TPanel;
    rgdateType_FilterGroup: TRadioGroup;
    rgoldestNewest_FilterGroup: TRadioGroup;
    cbFIlterGroupMore: TCheckBox;
    btnresort: TButton;
    ppLabel67: TppLabel;
    ppDBCalc14: TppDBCalc;
    ppLabel68: TppLabel;
    ppDBCalc15: TppDBCalc;
    btnVoGetData: TButton;
    Panel7: TPanel;
    btnEMailSend: TButton;
    EditFrom: TEdit;
    EditToMail: TEdit;
    Label9: TLabel;
    cbemail: TCheckBox;
    ppLabel61: TppLabel;
    ppDBText47: TppDBText;
    ppLabel66: TppLabel;
    ppDBText52: TppDBText;
    ppLabel72: TppLabel;
    ppDBText58: TppDBText;
    ppLabel73: TppLabel;
    ppDBText59: TppDBText;
    ppLabel74: TppLabel;
    ppDBText60: TppDBText;
    ppLabel78: TppLabel;
    ppDBText64: TppDBText;
    repTA: TppReport;
    ppTitleBand4: TppTitleBand;
    ppLabel75: TppLabel;
    ppSystemVariable4: TppSystemVariable;
    ppLabel80: TppLabel;
    ppHeaderBand4: TppHeaderBand;
    ppLabel81: TppLabel;
    ppLabel84: TppLabel;
    ppLabel85: TppLabel;
    ppLabel89: TppLabel;
    ppLabel90: TppLabel;
    ppLabel91: TppLabel;
    ppLabel93: TppLabel;
    ppLabel95: TppLabel;
    ppLabel96: TppLabel;
    ppLine9: TppLine;
    ppLabel97: TppLabel;
    ppLabel98: TppLabel;
    ppLabel100: TppLabel;
    ppLabel101: TppLabel;
    ppLabel102: TppLabel;
    ppLabel103: TppLabel;
    ppLabel104: TppLabel;
    ppLabel105: TppLabel;
    ppLabel106: TppLabel;
    ppLabel108: TppLabel;
    ppDetailBand4: TppDetailBand;
    ppDBText53: TppDBText;
    ppDBText54: TppDBText;
    ppDBText61: TppDBText;
    ppDBText66: TppDBText;
    ppDBText67: TppDBText;
    ppDBText70: TppDBText;
    ppDBText71: TppDBText;
    ppDBText72: TppDBText;
    ppDBText73: TppDBText;
    ppDBText74: TppDBText;
    ppDBText75: TppDBText;
    ppDBText77: TppDBText;
    ppDBText78: TppDBText;
    ppDBText79: TppDBText;
    ppDBText80: TppDBText;
    ppDBText81: TppDBText;
    ppDBText82: TppDBText;
    ppDBText83: TppDBText;
    ppDBText85: TppDBText;
    ppSummaryBand4: TppSummaryBand;
    ppLabel109: TppLabel;
    ppLabel110: TppLabel;
    ppLabel111: TppLabel;
    ppDBCalc18: TppDBCalc;
    ppDBCalc19: TppDBCalc;
    ppLabel112: TppLabel;
    ppDBCalc20: TppDBCalc;
    ppGroup4: TppGroup;
    ppGroupHeaderBand4: TppGroupHeaderBand;
    ppLabel113: TppLabel;
    ppLabel114: TppLabel;
    ppLine10: TppLine;
    ppLabel115: TppLabel;
    ppLabel116: TppLabel;
    ppGroupFooterBand4: TppGroupFooterBand;
    ppDBText86: TppDBText;
    ppDBCalc22: TppDBCalc;
    ppDBCalc23: TppDBCalc;
    ppDBCalc24: TppDBCalc;
    pipeTA: TppDBPipeline;
    dsTA: TDataSource;
    ppLabel99: TppLabel;
    ppDBText76: TppDBText;
    rgStep1PayType: TRadioGroup;
    Button14: TButton;
    Button15: TButton;
    btnGRDKill: TButton;
    tabspecialreports: TTabSheet;
    od1: TOpenDialog;
    Label19: TLabel;
    Label21: TLabel;
    Label35: TLabel;
    clbDB: TCheckListBox;
    DBGrid2: TDBGrid;
    btnDbClear: TButton;
    Panel11: TPanel;
    rgSpecialReportsOutput: TRadioGroup;
    rgSpecialReportsDetail: TRadioGroup;
    Panel12: TPanel;
    Label4: TLabel;
    Label2: TLabel;
    btnCombined2: TButton;
    memSpecialReps: TkbmMemTable;
    dsSpecialReps: TDataSource;
    Splitter3: TSplitter;
    lblSpecial: TLabel;
    repcombinedFormatted: TppReport;
    ppTitleBand6: TppTitleBand;
    ppLabel133: TppLabel;
    ppSystemVariable6: TppSystemVariable;
    ppHeaderBand6: TppHeaderBand;
    ppLabel134: TppLabel;
    ppLabel136: TppLabel;
    ppLabel137: TppLabel;
    ppLabel138: TppLabel;
    ppLabel139: TppLabel;
    ppLabel140: TppLabel;
    ppLabel141: TppLabel;
    ppLabel142: TppLabel;
    ppLabel143: TppLabel;
    ppLabel144: TppLabel;
    ppLabel145: TppLabel;
    ppDetailBand6: TppDetailBand;
    ppSummaryBand6: TppSummaryBand;
    pipeCombinedFormatted: TppDBPipeline;
    DBGrid3: TDBGrid;
    ppDBText98: TppDBText;
    ppDBText102: TppDBText;
    ppDBText103: TppDBText;
    ppDBText104: TppDBText;
    ppDBText105: TppDBText;
    ppDBText106: TppDBText;
    ppDBText107: TppDBText;
    ppDBText108: TppDBText;
    ppDBText109: TppDBText;
    ppLabel135: TppLabel;
    ppDBText110: TppDBText;
    ppDBText111: TppDBText;
    ppDBText112: TppDBText;
    ppGroup7: TppGroup;
    ppGroupHeaderBand7: TppGroupHeaderBand;
    ppGroupFooterBand7: TppGroupFooterBand;
    ppDBText113: TppDBText;
    ppLabel146: TppLabel;
    ppLabel147: TppLabel;
    ppDBCalc39: TppDBCalc;
    ppDBCalc40: TppDBCalc;
    ppDBCalc41: TppDBCalc;
    ppDBCalc42: TppDBCalc;
    ppDBCalc43: TppDBCalc;
    ppDBCalc44: TppDBCalc;
    ppDBCalc45: TppDBCalc;
    ppDBCalc46: TppDBCalc;
    ppDBCalc47: TppDBCalc;
    ppLabel148: TppLabel;
    ppDBText114: TppDBText;
    ppDBCalc48: TppDBCalc;
    ppLabel150: TppLabel;
    ppDBText116: TppDBText;
    ppDBCalc50: TppDBCalc;
    ppGroup8: TppGroup;
    ppGroupHeaderBand8: TppGroupHeaderBand;
    ppGroupFooterBand8: TppGroupFooterBand;
    ppDBText117: TppDBText;
    ppLabel151: TppLabel;
    ppDBText118: TppDBText;
    ppDBCalc51: TppDBCalc;
    ppDBCalc52: TppDBCalc;
    ppDBCalc53: TppDBCalc;
    ppDBCalc54: TppDBCalc;
    ppDBCalc55: TppDBCalc;
    ppDBCalc56: TppDBCalc;
    ppDBCalc57: TppDBCalc;
    ppDBCalc58: TppDBCalc;
    ppDBCalc59: TppDBCalc;
    ppDBCalc60: TppDBCalc;
    ppDBCalc61: TppDBCalc;
    btnytd: TButton;
    pipeYTD: TppDBPipeline;
    repYTD: TppReport;
    ppHeaderBand7: TppHeaderBand;
    ppDetailBand7: TppDetailBand;
    ppTitleBand7: TppTitleBand;
    memSpecialRepsyear: TStringField;
    memSpecialRepscountry: TStringField;
    memSpecialRepsoperator: TStringField;
    lblYTDTITLE: TppLabel;
    ppSystemVariable7: TppSystemVariable;
    ppDBText119: TppDBText;
    ppLabel152: TppLabel;
    ppLabel153: TppLabel;
    ppLabel154: TppLabel;
    ppLabel155: TppLabel;
    ppLabel156: TppLabel;
    ppLabel157: TppLabel;
    ppLabel158: TppLabel;
    ppLabel159: TppLabel;
    ppLabel161: TppLabel;
    ppDBText120: TppDBText;
    ppDBText121: TppDBText;
    ppDBText122: TppDBText;
    ppDBText123: TppDBText;
    ppDBText124: TppDBText;
    ppDBText125: TppDBText;
    ppDBText126: TppDBText;
    ppDBText128: TppDBText;
    ppSummaryBand7: TppSummaryBand;
    ppGroup11: TppGroup;
    ppGroupHeaderBand11: TppGroupHeaderBand;
    ppGroupFooterBand11: TppGroupFooterBand;
    ppDBText129: TppDBText;
    ppLabel162: TppLabel;
    ppDBCalc63: TppDBCalc;
    GroupBox2: TGroupBox;
    cbUseRange2: TCheckBox;
    cbUseRange3: TCheckBox;
    dtprange2start: TDateTimePicker;
    dtprange2end: TDateTimePicker;
    dtprange3start: TDateTimePicker;
    dtprange3end: TDateTimePicker;
    Label7: TLabel;
    Label10: TLabel;
    ppDBCalc64: TppDBCalc;
    dbcalcCTRYPFT: TppDBCalc;
    dbcalcopdue: TppDBCalc;
    lblAveDurationCTRY: TppLabel;
    lblaveprofitCTRY: TppLabel;
    lblaveopDUeCTRY: TppLabel;
    ppDBText130: TppDBText;
    ppLabel163: TppLabel;
    ppDBText131: TppDBText;
    ppLabel164: TppLabel;
    ppDBText132: TppDBText;
    ppLabel165: TppLabel;
    ppLabel160: TppLabel;
    ppDBText127: TppDBText;
    ppDBCalc65: TppDBCalc;
    btnSpSet: TButton;
    ppLine11: TppLine;
    ppGroup9: TppGroup;
    ppGroupHeaderBand9: TppGroupHeaderBand;
    ppGroupFooterBand9: TppGroupFooterBand;
    ppDBText133: TppDBText;
    ppDBCalc66: TppDBCalc;
    ppDBCalc67: TppDBCalc;
    ppDBCalc68: TppDBCalc;
    ppDBCalc69: TppDBCalc;
    ppDBCalc70: TppDBCalc;
    lblAveDurationOper: TppLabel;
    lblaveprofitOper: TppLabel;
    lblaveopDUeOper: TppLabel;
    btnFleetMix: TButton;
    pipeFleetMix: TppDBPipeline;
    memSpecialRepsUSA_COUNT: TIntegerField;
    memSpecialRepsKEM_COUNT: TIntegerField;
    memSpecialRepsCANA_COUNT: TIntegerField;
    memSpecialRepsHOHO_COUNT: TIntegerField;
    memSpecialRepsUK_COUNT: TIntegerField;
    memSpecialRepsEURO_COUNT: TIntegerField;
    memSpecialRepsAUS_COUNT: TIntegerField;
    memSpecialRepsNEWZ_COUNT: TIntegerField;
    memSpecialRepsSAF_COUNT: TIntegerField;
    memSpecialRepsLAC_COUNT: TIntegerField;
    memSpecialRepsHOT_COUNT: TIntegerField;
    repFleetMix: TppReport;
    ppTitleBand8: TppTitleBand;
    lblFleetMixTitle: TppLabel;
    ppSystemVariable8: TppSystemVariable;
    ppHeaderBand8: TppHeaderBand;
    ppLabel168: TppLabel;
    ppLabel169: TppLabel;
    ppLabel170: TppLabel;
    ppLabel171: TppLabel;
    ppLabel172: TppLabel;
    ppLabel173: TppLabel;
    ppLabel174: TppLabel;
    ppLabel175: TppLabel;
    ppLabel176: TppLabel;
    ppLabel177: TppLabel;
    ppLabel178: TppLabel;
    ppLabel179: TppLabel;
    ppLabel180: TppLabel;
    ppLabel182: TppLabel;
    ppDetailBand8: TppDetailBand;
    ppDBText134: TppDBText;
    ppDBText135: TppDBText;
    ppDBText136: TppDBText;
    ppDBText137: TppDBText;
    ppDBText138: TppDBText;
    ppDBText139: TppDBText;
    ppDBText140: TppDBText;
    ppDBText141: TppDBText;
    ppDBText142: TppDBText;
    ppDBText143: TppDBText;
    ppDBText144: TppDBText;
    ppDBText145: TppDBText;
    ppDBText146: TppDBText;
    ppDBText148: TppDBText;
    ppSummaryBand8: TppSummaryBand;
    ppGroup10: TppGroup;
    ppGroupHeaderBand10: TppGroupHeaderBand;
    ppGroupFooterBand10: TppGroupFooterBand;
    ppDBText149: TppDBText;
    ppDBCalc71: TppDBCalc;
    ppDBCalc72: TppDBCalc;
    ppDBCalc73: TppDBCalc;
    ppDBCalc74: TppDBCalc;
    ppDBCalc75: TppDBCalc;
    ppDBCalc76: TppDBCalc;
    ppDBCalc77: TppDBCalc;
    ppDBCalc78: TppDBCalc;
    ppDBCalc79: TppDBCalc;
    ppDBCalc80: TppDBCalc;
    ppDBCalc82: TppDBCalc;
    ppGroup12: TppGroup;
    ppGroupHeaderBand12: TppGroupHeaderBand;
    ppGroupFooterBand12: TppGroupFooterBand;
    ppDBText150: TppDBText;
    ppLabel185: TppLabel;
    ppDBText151: TppDBText;
    ppDBCalc83: TppDBCalc;
    ppDBCalc84: TppDBCalc;
    ppDBCalc85: TppDBCalc;
    ppDBCalc86: TppDBCalc;
    ppDBCalc87: TppDBCalc;
    ppDBCalc88: TppDBCalc;
    ppDBCalc89: TppDBCalc;
    ppDBCalc90: TppDBCalc;
    ppDBCalc91: TppDBCalc;
    ppDBCalc92: TppDBCalc;
    ppDBCalc93: TppDBCalc;
    ppLabel181: TppLabel;
    ppDBText147: TppDBText;
    ppDBCalc81: TppDBCalc;
    ppLabel167: TppLabel;
    ppLabel186: TppLabel;
    ppDBCalc94: TppDBCalc;
    ppLabel187: TppLabel;
    ppLabel188: TppLabel;
    ppDBCalc95: TppDBCalc;
    ppDBCalc96: TppDBCalc;
    ppDBCalc97: TppDBCalc;
    ppDBCalc98: TppDBCalc;
    ppDBCalc99: TppDBCalc;
    ppDBCalc100: TppDBCalc;
    ppDBCalc101: TppDBCalc;
    ppDBCalc102: TppDBCalc;
    ppDBCalc103: TppDBCalc;
    ppDBCalc104: TppDBCalc;
    ppDBCalc105: TppDBCalc;
    ppDBCalc106: TppDBCalc;
    ppDBCalc107: TppDBCalc;
    ppDBCalc108: TppDBCalc;
    ppDBCalc109: TppDBCalc;
    ppDBCalc110: TppDBCalc;
    ppDBCalc111: TppDBCalc;
    ppDBCalc112: TppDBCalc;
    ppDBCalc113: TppDBCalc;
    ppDBCalc114: TppDBCalc;
    ppLabel189: TppLabel;
    ppLabel190: TppLabel;
    ppDBCalc115: TppDBCalc;
    lblUSAaveDuration: TppLabel;
    ppDBCalc116: TppDBCalc;
    lblkemaveDuration: TppLabel;
    ppDBCalc117: TppDBCalc;
    lblcanaaveDuration: TppLabel;
    ppDBCalc118: TppDBCalc;
    lblhohoaveDuration: TppLabel;
    ppDBCalc119: TppDBCalc;
    lblUKaveDuration: TppLabel;
    ppDBCalc120: TppDBCalc;
    lblEUROaveDuration: TppLabel;
    ppDBCalc121: TppDBCalc;
    lblAUSaveDuration: TppLabel;
    ppDBCalc122: TppDBCalc;
    lblNEWZaveDuration: TppLabel;
    ppDBCalc123: TppDBCalc;
    lblSAFaveDuration: TppLabel;
    ppDBCalc124: TppDBCalc;
    lblLACaveDuration: TppLabel;
    ppDBCalc125: TppDBCalc;
    lblHOTaveDuration: TppLabel;
    ppDBCalc126: TppDBCalc;
    lblAvePftUSA: TppLabel;
    ppDBCalc127: TppDBCalc;
    lblAvePftKEM: TppLabel;
    ppDBCalc128: TppDBCalc;
    lblAvePftCANA: TppLabel;
    ppDBCalc129: TppDBCalc;
    lblAvePftHOHO: TppLabel;
    ppDBCalc130: TppDBCalc;
    lblAvePftUK: TppLabel;
    ppDBCalc131: TppDBCalc;
    lblAvePftEURO: TppLabel;
    ppDBCalc132: TppDBCalc;
    lblAvePftAUS: TppLabel;
    ppDBCalc133: TppDBCalc;
    lblAvePftNEWZ: TppLabel;
    ppDBCalc134: TppDBCalc;
    lblAvePftSAF: TppLabel;
    ppDBCalc135: TppDBCalc;
    lblAvePftLAC: TppLabel;
    ppDBCalc136: TppDBCalc;
    lblAvePftHOT: TppLabel;
    memCountries: TkbmMemTable;
    ppLabel149: TppLabel;
    ppDBText115: TppDBText;
    memSpecialRepsoperator_master: TStringField;
    ppGroup13: TppGroup;
    ppGroupHeaderBand13: TppGroupHeaderBand;
    ppGroupFooterBand13: TppGroupFooterBand;
    ppDBText152: TppDBText;
    ppDBCalc49: TppDBCalc;
    ppDBCalc62: TppDBCalc;
    ppDBCalc137: TppDBCalc;
    ppDBCalc138: TppDBCalc;
    ppDBCalc139: TppDBCalc;
    ppDBCalc140: TppDBCalc;
    ppDBCalc141: TppDBCalc;
    ppDBCalc142: TppDBCalc;
    ppDBCalc143: TppDBCalc;
    ppDBCalc144: TppDBCalc;
    ppDBCalc145: TppDBCalc;
    ppDBText153: TppDBText;
    ppDBText154: TppDBText;
    ppDBText155: TppDBText;
    ppDBText156: TppDBText;
    ppGroup14: TppGroup;
    ppGroupHeaderBand14: TppGroupHeaderBand;
    ppGroupFooterBand14: TppGroupFooterBand;
    ppDBText157: TppDBText;
    ppDBCalc146: TppDBCalc;
    ppDBCalc147: TppDBCalc;
    aveduryear: TppLabel;
    ppDBCalc148: TppDBCalc;
    lblavepftyear: TppLabel;
    ppDBCalc149: TppDBCalc;
    lblaveopDueYEAR: TppLabel;
    ppDBCalc150: TppDBCalc;
    ppLabel9: TppLabel;
    ppLabel10: TppLabel;
    ppLabel107: TppLabel;
    ppLabel117: TppLabel;
    ppLabel118: TppLabel;
    ppLabel119: TppLabel;
    ppDBCalc25: TppDBCalc;
    ppDBCalc26: TppDBCalc;
    ppDBCalc27: TppDBCalc;
    ppDBCalc28: TppDBCalc;
    ppDBCalc29: TppDBCalc;
    ppDBCalc30: TppDBCalc;
    ppDBCalc31: TppDBCalc;
    ppDBCalc32: TppDBCalc;
    ppDBCalc33: TppDBCalc;
    ppDBCalc34: TppDBCalc;
    ppDBCalc35: TppDBCalc;
    ppDBCalc36: TppDBCalc;
    ppDBCalc37: TppDBCalc;
    ppDBCalc38: TppDBCalc;
    ppDBCalc151: TppDBCalc;
    ppDBCalc152: TppDBCalc;
    ppDBCalc153: TppDBCalc;
    ppDBCalc154: TppDBCalc;
    ppDBCalc155: TppDBCalc;
    ppDBCalc156: TppDBCalc;
    ppDBCalc157: TppDBCalc;
    ppDBCalc158: TppDBCalc;
    ppDBCalc159: TppDBCalc;
    ppDBCalc160: TppDBCalc;
    ppDBCalc161: TppDBCalc;
    ppDBCalc162: TppDBCalc;
    ppDBCalc163: TppDBCalc;
    ppDBCalc164: TppDBCalc;
    ppDBCalc165: TppDBCalc;
    ppDBCalc166: TppDBCalc;
    ppDBCalc167: TppDBCalc;
    ppDBCalc168: TppDBCalc;
    ppDBCalc169: TppDBCalc;
    ppLabel120: TppLabel;
    ppLabel121: TppLabel;
    ppLabel122: TppLabel;
    ppLabel123: TppLabel;
    ppLabel124: TppLabel;
    ppLabel125: TppLabel;
    ppLabel126: TppLabel;
    ppLabel127: TppLabel;
    ppLabel128: TppLabel;
    ppLabel129: TppLabel;
    ppLabel130: TppLabel;
    ppDBCalc170: TppDBCalc;
    ppDBCalc171: TppDBCalc;
    ppDBCalc172: TppDBCalc;
    ppDBCalc173: TppDBCalc;
    ppDBCalc174: TppDBCalc;
    ppDBCalc175: TppDBCalc;
    ppDBCalc176: TppDBCalc;
    ppDBCalc177: TppDBCalc;
    ppDBCalc178: TppDBCalc;
    ppDBCalc179: TppDBCalc;
    ppDBCalc180: TppDBCalc;
    ppLabel131: TppLabel;
    ppLabel132: TppLabel;
    ppLabel166: TppLabel;
    ppLabel183: TppLabel;
    ppLabel184: TppLabel;
    ppLabel191: TppLabel;
    ppLabel192: TppLabel;
    ppLabel193: TppLabel;
    ppLabel194: TppLabel;
    ppLabel195: TppLabel;
    ppLabel196: TppLabel;
    btnRPBUS: TButton;
    rpbus_get_names: TButton;
    repRepeatBiz: TppReport;
    piperRepeatBiz: TppDBPipeline;
    ppHeaderBand5: TppHeaderBand;
    ppDetailBand5: TppDetailBand;
    ppTitleBand5: TppTitleBand;
    lblRepeatBizTitle: TppLabel;
    ppSystemVariable5: TppSystemVariable;
    ppLabel197: TppLabel;
    ppDBText9: TppDBText;
    ppLabel198: TppLabel;
    ppLabel199: TppLabel;
    ppLabel200: TppLabel;
    ppLabel201: TppLabel;
    ppLabel202: TppLabel;
    ppLabel203: TppLabel;
    ppLabel204: TppLabel;
    ppDBText10: TppDBText;
    ppDBText84: TppDBText;
    ppDBText87: TppDBText;
    ppDBText88: TppDBText;
    ppDBText89: TppDBText;
    ppDBText90: TppDBText;
    ppDBText91: TppDBText;
    editDateOffset: TEdit;
    Label12: TLabel;
    rgCountMethod: TRadioGroup;
    Q2: TFDQuery;
    dtpMasterListStart: TDateTimePicker;
    dtpMasterListEnd: TDateTimePicker;
    btnMainFill: TButton;
    Label30: TLabel;
    btnClear: TBitBtn;
    btnDbAll: TButton;
    GroupBox3: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    dtpCreateDtStart: TDateTimePicker;
    dtpCreateDtENd: TDateTimePicker;
    cbUseCreateDate: TCheckBox;
    btnRpBusNew: TButton;
    sd1: TSaveDialog;
    btnDumpGrid: TButton;
    ppLabel205: TppLabel;
    ppLabel206: TppLabel;
    ppDBText92: TppDBText;
    ppDBText93: TppDBText;
    ppDBText94: TppDBText;
    cbSIPPWildCard: TCheckBox;
    EditSippWildcard: TEdit;
    btnLatestDates: TButton;
    gbReleaseNum: TGroupBox;
    cbReleaseNum: TCheckBox;
    EditReleaseNum: TEdit;
    Label50: TLabel;
    tabStep2c: TTabSheet;
    GroupBox5: TGroupBox;
    memoIataList: TMemo;
    Label51: TLabel;
    Label52: TLabel;
    cbUseIataList: TCheckBox;
    btnIataListClear: TButton;
    btnSippAnalysis: TButton;
    btnSippAnalysisSIPP: TButton;
    tabForbiddenZone: TTabSheet;
    Splitter1: TSplitter;
    Panel9: TPanel;
    Memo1: TMemo;
    Panel13: TPanel;
    Panel8: TPanel;
    GroupBox9: TGroupBox;
    Label24: TLabel;
    Label27: TLabel;
    dtpTrackReportsStart: TDateTimePicker;
    dtpTrackReportsEnd: TDateTimePicker;
    btnTrack: TButton;
    cbSumOnly: TCheckBox;
    btnFzTest: TButton;
    btnFzClear: TButton;
    btnFzBuildSQL: TButton;
    btnkill: TButton;
    Panel10: TPanel;
    btnGetDataWMySQL: TButton;
    memoSQL: TMemo;
    Splitter2: TSplitter;
    Label20: TLabel;
    pipeBrettCtryOp: TppDBPipeline;
    rptBrettCtryOp: TppReport;
    ppDetailBand9: TppDetailBand;
    ppTitleBand9: TppTitleBand;
    ppSummaryBand5: TppSummaryBand;
    ppHeaderBand9: TppHeaderBand;
    lblSippAnalSippTitle: TppLabel;
    ppSystemVariable9: TppSystemVariable;
    ppDBText95: TppDBText;
    lblHeadersys: TppLabel;
    lblHeaderctryPU: TppLabel;
    dbTextctryPU: TppDBText;
    ppLabel207: TppLabel;
    dbTestMstrOp: TppDBText;
    lblHeaderSource: TppLabel;
    DBTextSource: TppDBText;
    lblheaderYear: TppLabel;
    dbtextYear: TppDBText;
    lblHeaderPlusBasic: TppLabel;
    ppDBText96: TppDBText;
    dbTextSIPP: TppDBText;
    lblSIPP: TppLabel;
    lblhdrbookings: TppLabel;
    ppDBText97: TppDBText;
    lblHeaderRetail: TppLabel;
    lblRetail: TppDBText;
    lblHeaderDisc: TppLabel;
    dbtextDisc: TppDBText;
    lblheaderadjret: TppLabel;
    ppDBTextadjret: TppDBText;
    ppLabel208: TppLabel;
    dbtextWHL: TppDBText;
    ppLabel209: TppLabel;
    ppDBTextcomm: TppDBText;
    ppLabel210: TppLabel;
    ppDBTextdeposit: TppDBText;
    ppLabel211: TppLabel;
    ppDBTextduration: TppDBText;
    ppLabel212: TppLabel;
    ppDBTextProfit: TppDBText;
    ppLabel213: TppLabel;
    ppDBTextgrossmargin: TppDBText;
    ppDBTextnetmargin: TppDBText;
    ppLabel214: TppLabel;
    ppLabel215: TppLabel;
    ppDBTextpftmargin: TppDBText;
    ppLabel216: TppLabel;
    ppDBTextaveduration: TppDBText;
    ppLabel217: TppLabel;
    ppDBTextavePft: TppDBText;
    ppLabel218: TppLabel;
    ppDBTextaveWHL: TppDBText;
    ppLabel219: TppLabel;
    ppDBTextretaildaily: TppDBText;
    ppLabel220: TppLabel;
    ppDBTextdiscdly: TppDBText;
    ppLabel221: TppLabel;
    ppDBTextadjRetDaily: TppDBText;
    ppLabel222: TppLabel;
    ppDBTextcommdaily: TppDBText;
    ppLabel223: TppLabel;
    ppDBTextProfitDaily: TppDBText;
    ppLabel224: TppLabel;
    ppDBTextwholesaleDaily: TppDBText;
    sortYearSyCtry: TppDBText;
    sortYearSysCtryOp: TppDBText;
    ppGroup5: TppGroup;
    ppGroupHeaderBand5: TppGroupHeaderBand;
    ppGroupFooterBand5: TppGroupFooterBand;
    ppGroup6: TppGroup;
    ppGroupHeaderBand6: TppGroupHeaderBand;
    ppGroupFooterBand6: TppGroupFooterBand;
    ppDBText99: TppDBText;
    ppDBText100: TppDBText;
    ppDBText101: TppDBText;
    ppDBText158: TppDBText;
    ppDBText160: TppDBText;
    ppDBText161: TppDBText;
    ppDBText162: TppDBText;
    ppDBCalc182: TppDBCalc;
    ppDBCalc183: TppDBCalc;
    GroupBox6: TGroupBox;
    btnRunSalesProdAnal: TButton;
    cbspaShowDetail: TCheckBox;
    Label53: TLabel;
    cbspaSHowYearSysCtry: TCheckBox;
    cbspaSHowYearSysCtryMstrOp: TCheckBox;
    cbspaSHowYearSysCtryMstrOpSIPP: TCheckBox;
    ppDBCalc204: TppDBCalc;
    ppDBCalc205: TppDBCalc;
    ppDBCalc206: TppDBCalc;
    ppDBCalc207: TppDBCalc;
    ppDBCalc208: TppDBCalc;
    ppDBCalc209: TppDBCalc;
    ppDBCalc210: TppDBCalc;
    ppDBCalc211: TppDBCalc;
    ppDBCalc224: TppDBCalc;
    ppDBCalc225: TppDBCalc;
    ppDBCalc226: TppDBCalc;
    ppDBCalc227: TppDBCalc;
    ppDBCalc228: TppDBCalc;
    ppDBCalc229: TppDBCalc;
    ppDBCalc230: TppDBCalc;
    ppDBCalc231: TppDBCalc;
    ppLabel226: TppLabel;
    ppLabel227: TppLabel;
    ppLabel228: TppLabel;
    ppLabel230: TppLabel;
    ppLabel231: TppLabel;
    ppLabel232: TppLabel;
    ppLabel233: TppLabel;
    lblYearSysCtryRetDly: TppLabel;
    lblYearSysCtryDiscDly: TppLabel;
    lblYearSysCtryAdjretDly: TppLabel;
    lblYearSysCtryCommDly: TppLabel;
    lblYearSysCtryProfitDly: TppLabel;
    lblYearSysCtryWhlDly: TppLabel;
    lblYearSysCtryOPCommDly: TppLabel;
    lblYearSysCtryOPRetDly: TppLabel;
    lblYearSysCtryOPAdjretDly: TppLabel;
    lblYearSysCtryOPProfitDly: TppLabel;
    lblYearSysCtryOPWhlDly: TppLabel;
    lblYearSysCtryOPDiscDly: TppLabel;
    lblYearSysCtryAveDuration: TppLabel;
    lblYearSysCtryOPAveDuration: TppLabel;
    CtryOpGrossMargAve: TppLabel;
    CtryOpNetMargAve: TppLabel;
    CtryOpPftMargAve: TppLabel;
    CtryGrossMargAve: TppLabel;
    CtryNetMargAve: TppLabel;
    CtryPftMargAve: TppLabel;
    lblCtryOpAvePft: TppLabel;
    lblCtryOpAveWhl: TppLabel;
    lblCtryAvePft: TppLabel;
    lblCtryAveWhl: TppLabel;
    GroupBox7: TGroupBox;
    cbPlusBasic: TCheckBox;
    ComboPlusBasic: TComboBox;
    ppLabel234: TppLabel;
    ppDBText159: TppDBText;
    ppLabel236: TppLabel;
    ppLabel237: TppLabel;
    cbspaSHowYearSysCtryMstrOpSIPPSrc: TCheckBox;
    cbspaSHowYearSysCtryMstrOpSource: TCheckBox;
    sortYearSysCtryOpSource: TppDBText;
    ppGroup17: TppGroup;
    ppGroupHeaderBand17: TppGroupHeaderBand;
    ppGroupFooterBand17: TppGroupFooterBand;
    ppDBText174: TppDBText;
    ppDBText175: TppDBText;
    ppDBText176: TppDBText;
    ppDBText177: TppDBText;
    ppDBCalc201: TppDBCalc;
    ppDBCalc202: TppDBCalc;
    ppDBCalc203: TppDBCalc;
    ppDBCalc212: TppDBCalc;
    ppDBCalc213: TppDBCalc;
    ppDBCalc214: TppDBCalc;
    ppDBCalc215: TppDBCalc;
    ppDBCalc216: TppDBCalc;
    ppDBCalc217: TppDBCalc;
    ppLabel240: TppLabel;
    ppLabel241: TppLabel;
    lblYearSysCtryOPSourceCommDly: TppLabel;
    lblYearSysCtryOPSourceRetDly: TppLabel;
    lblYearSysCtryOPSourceAdjretDly: TppLabel;
    lblYearSysCtryOPSourceProfitDly: TppLabel;
    lblYearSysCtryOPSourceWhlDly: TppLabel;
    lblYearSysCtryOPSourceDiscDly: TppLabel;
    lblYearSysCtryOPSourceAveDuration: TppLabel;
    CtryOpSourceGrossMargAve: TppLabel;
    CtryOpSourceNetMargAve: TppLabel;
    CtryOpSourcePftMargAve: TppLabel;
    lblCtryOpSourceAvePft: TppLabel;
    lblCtryOpSourceAveWhl: TppLabel;
    ppLabel255: TppLabel;
    ppDBText178: TppDBText;
    rptBrettSipp2: TppReport;
    ppTitleBand10: TppTitleBand;
    lblSippAnalSippTitle2: TppLabel;
    ppSystemVariable10: TppSystemVariable;
    ppHeaderBand10: TppHeaderBand;
    ppLabel256: TppLabel;
    ppLabel257: TppLabel;
    ppLabel258: TppLabel;
    ppLabel259: TppLabel;
    ppLabel260: TppLabel;
    ppLabel261: TppLabel;
    ppLabel262: TppLabel;
    ppLabel263: TppLabel;
    ppLabel264: TppLabel;
    ppLabel265: TppLabel;
    ppLabel266: TppLabel;
    ppLabel267: TppLabel;
    ppLabel268: TppLabel;
    ppLabel269: TppLabel;
    ppLabel270: TppLabel;
    ppLabel271: TppLabel;
    ppLabel272: TppLabel;
    ppLabel273: TppLabel;
    ppLabel274: TppLabel;
    ppLabel275: TppLabel;
    ppLabel276: TppLabel;
    ppLabel277: TppLabel;
    ppLabel278: TppLabel;
    ppLabel279: TppLabel;
    ppLabel280: TppLabel;
    ppLabel281: TppLabel;
    ppLabel282: TppLabel;
    ppLabel283: TppLabel;
    ppLabel284: TppLabel;
    ppDetailBand10: TppDetailBand;
    ppDBText179: TppDBText;
    ppDBText180: TppDBText;
    ppDBText181: TppDBText;
    ppDBText182: TppDBText;
    ppDBText183: TppDBText;
    ppDBText184: TppDBText;
    ppDBText185: TppDBText;
    ppDBText186: TppDBText;
    ppDBText187: TppDBText;
    ppDBText188: TppDBText;
    ppDBText189: TppDBText;
    ppDBText190: TppDBText;
    ppDBText191: TppDBText;
    ppDBText192: TppDBText;
    ppDBText193: TppDBText;
    ppDBText194: TppDBText;
    ppDBText195: TppDBText;
    ppDBText196: TppDBText;
    ppDBText197: TppDBText;
    ppDBText198: TppDBText;
    ppDBText199: TppDBText;
    ppDBText200: TppDBText;
    ppDBText201: TppDBText;
    ppDBText202: TppDBText;
    ppDBText203: TppDBText;
    ppDBText204: TppDBText;
    ppDBText205: TppDBText;
    ppDBText206: TppDBText;
    sortYearSysCtryOpSIPP2: TppDBText;
    ppDBText210: TppDBText;
    sortYearSysCtryOpSIPPSource2: TppDBText;
    ppSummaryBand9: TppSummaryBand;
    ppGroup21: TppGroup;
    ppGroupHeaderBand21: TppGroupHeaderBand;
    ppGroupFooterBand21: TppGroupFooterBand;
    ppDBText225: TppDBText;
    ppDBText226: TppDBText;
    ppDBText227: TppDBText;
    ppDBText228: TppDBText;
    ppDBCalc253: TppDBCalc;
    ppDBCalc254: TppDBCalc;
    ppDBCalc255: TppDBCalc;
    ppDBCalc256: TppDBCalc;
    ppDBCalc257: TppDBCalc;
    ppDBCalc258: TppDBCalc;
    ppDBCalc259: TppDBCalc;
    ppDBCalc260: TppDBCalc;
    ppDBCalc261: TppDBCalc;
    ppLabel333: TppLabel;
    lblYearSysCtryOPSIPPSrcCommDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcRetDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcAdjretDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcProfitDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcWhlDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcDiscDly3: TppLabel;
    lblYearSysCtryOPSIPPSrcAveDuration3: TppLabel;
    ctryOpSippSrcGrossMargAve3: TppLabel;
    CtryOpSippSrcNetMargAve3: TppLabel;
    CtryOpSippSrcPftMargAve3: TppLabel;
    lblCtryOpSippSrcAvePft3: TppLabel;
    lblCtryOpSippSrcAveWhl3: TppLabel;
    ppDBText229: TppDBText;
    ppLabel346: TppLabel;
    ppLabel347: TppLabel;
    ppGroup22: TppGroup;
    ppGroupHeaderBand22: TppGroupHeaderBand;
    ppGroupFooterBand22: TppGroupFooterBand;
    ppDBText230: TppDBText;
    ppDBText231: TppDBText;
    ppDBText232: TppDBText;
    ppDBText233: TppDBText;
    ppDBCalc262: TppDBCalc;
    ppDBCalc263: TppDBCalc;
    ppDBCalc264: TppDBCalc;
    ppDBCalc265: TppDBCalc;
    ppDBCalc266: TppDBCalc;
    ppDBCalc267: TppDBCalc;
    ppDBCalc268: TppDBCalc;
    ppDBCalc269: TppDBCalc;
    ppDBCalc270: TppDBCalc;
    ppLabel348: TppLabel;
    lblYearSysCtryOPSIPPSrcCommDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcRetDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcAdjretDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcProfitDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcWhlDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcDiscDly2: TppLabel;
    lblYearSysCtryOPSIPPSrcAveDuration2: TppLabel;
    ctryOpSippSrcGrossMargAve2: TppLabel;
    CtryOpSippSrcNetMargAve2: TppLabel;
    CtryOpSippSrcPftMargAve2: TppLabel;
    lblCtryOpSippSrcAvePft2: TppLabel;
    lblCtryOpSippSrcAveWhl2: TppLabel;
    ppDBText234: TppDBText;
    ppLabel361: TppLabel;
    ppDBText235: TppDBText;
    pipeBrettSipp2: TppDBPipeline;
    lblHeaderGroupingSipp: TppLabel;
    lblHeaderGroupingCtryOp: TppLabel;
    GroupBox8: TGroupBox;
    cbUseClientCtry: TCheckBox;
    editClientCountry: TEdit;
    Label54: TLabel;
    Button30: TButton;
    chkLockMaster: TCheckBox;
    MainMenu1: TMainMenu;
    mnuFile: TMenuItem;
    mnuClose: TMenuItem;
    mnuHelpMain: TMenuItem;
    mnuHelp: TMenuItem;
    N1: TMenuItem;
    mnuAbout: TMenuItem;
    ppLabel225: TppLabel;
    ppDBText163: TppDBText;
    DBTextwholesalAvg: TppDBText;
    ppLabel229: TppLabel;
    ppLabel235: TppLabel;
    ppLabel238: TppLabel;
    ppDBText165: TppDBText;
    ppDBText166: TppDBText;
    ppDBCalc186: TppDBCalc;
    ppDBCalc187: TppDBCalc;
    ppDBCalc188: TppDBCalc;
    ppDBCalc189: TppDBCalc;
    ppDBCalc190: TppDBCalc;
    ppDBCalc191: TppDBCalc;
    lblAveWhlCurr: TppLabel;
    lblAveWhlCurrOp1: TppLabel;
    ppLabel239: TppLabel;
    ppDBTextaveExchange: TppDBText;
    ppLabelCtryOpSourceAveExchange: TppLabel;
    ppLabelCtryOpAveExchange: TppLabel;
    lblAveWhlCurrOp: TppLabel;
    ppLabelCtryAveExchange: TppLabel;
    btnMarcelloReport: TButton;
    IfxQueryMarcello: TFDQuery;
    kbmMarcello: TkbmMemTable;
    DBPipelineMarcello: TppDBPipeline;
    ppReportMarcello: TppReport;
    DataSourceMarcello: TDataSource;
    ppHeaderBandMarcello: TppHeaderBand;
    ppDetailBandMarcelloDetail: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppLabelMarcelloReportName: TppLabel;
    ppLabelMarcelloReportDate: TppLabel;
    ppLabelMarcelloSystem: TppLabel;
    ppDBTextMarcelloSystem: TppDBText;
    ppLabelMarcelloOperator: TppLabel;
    ppDBTextMarcelloOperator: TppDBText;
    ppLabelMarcelloVoucherCount: TppLabel;
    ppDBTextMarcelloVoucherCount: TppDBText;
    ppLabelMarcelloCountry: TppLabel;
    ppDBTextMarcelloCountry: TppDBText;
    ppLabelMarcelloHomeCountry: TppLabel;
    ppDBTextMarcelloHomeCountry: TppDBText;
    ppLabelMarcelloClientCountry: TppLabel;
    ppDBTextMarcelloClientCountry: TppDBText;
    ppLabeMarcelloRetailTotal: TppLabel;
    ppDBTextMarcelloRetailTotal: TppDBText;
    ppLabelMarcelloProfit: TppLabel;
    ppDBTextMarcelloProfit: TppDBText;
    ppLabelMarcelloVendor: TppLabel;
    ppDBTextMarcelloVendor: TppDBText;
    ppLabelMarcelloPayDte: TppLabel;
    ppDBTextMarcelloPayDte: TppDBText;
    ppLabelMarcelloCampid: TppLabel;
    ppDBTextMarcelloCampid: TppDBText;
    ppLabelMarcilleoCamp: TppLabel;
    ppDBTextMarcilleoCamp: TppDBText;
    ppLabel242: TppLabel;
    ppDBTextMarcilleoHost: TppDBText;
    ppGroup15: TppGroup;
    ppGroupHeaderBandMarcelloCampaign: TppGroupHeaderBand;
    ppGroupFooterBandMarcelloCampaign: TppGroupFooterBand;
    ppLabelMarcelloCampidDetail: TppLabel;
    ppLabeMarcelloCampaignDetail: TppLabel;
    ppDBCalcMarcelloRetailTotalDetail: TppDBCalc;
    ppDBTextMarcelloCampidDetail: TppDBText;
    ppDBTextMarcelloCampaignDetail: TppDBText;
    ppLabelMarcelloRetailTotalDetail: TppLabel;
    ppLabelMarcelloReportDateDetail: TppLabel;
    ppLabelMarcelloReportNameDetail: TppLabel;
    ppLabelMarcelloProfitDetail: TppLabel;
    ppDBCalcMarcelloProfitDetail: TppDBCalc;
    ppLabelMarcelloVoucherCountDetail: TppLabel;
    ppDBCalcMarcelloVoucherCountDetail: TppDBCalc;
    kbmHostCampaign: TkbmMemTable;
    IfxQueryHostCampid: TFDQuery;
    conHost: TFDConnection;
    ppReportMarcelloFull: TppReport;
    ppTitleBand11: TppTitleBand;
    ppLabelMarcelloReportNameFull: TppLabel;
    ppSystemVariable11: TppSystemVariable;
    ppHeaderBandMarcelloFull: TppHeaderBand;
    ppLabel243: TppLabel;
    ppLabel244: TppLabel;
    ppLabel245: TppLabel;
    ppLabel246: TppLabel;
    ppLabel247: TppLabel;
    ppLabel248: TppLabel;
    ppLabel249: TppLabel;
    ppLabel250: TppLabel;
    ppLabel251: TppLabel;
    ppLabel252: TppLabel;
    ppLabel253: TppLabel;
    ppLabel254: TppLabel;
    ppLabel285: TppLabel;
    ppLabel286: TppLabel;
    ppLabel287: TppLabel;
    ppLabel288: TppLabel;
    ppLabel289: TppLabel;
    ppLabel290: TppLabel;
    ppLabel291: TppLabel;
    ppLabel292: TppLabel;
    ppLabel293: TppLabel;
    ppLabel294: TppLabel;
    ppLabel295: TppLabel;
    ppLine12: TppLine;
    ppLabel296: TppLabel;
    ppLabel297: TppLabel;
    ppLabel298: TppLabel;
    ppLabel299: TppLabel;
    ppLabel300: TppLabel;
    ppLabel301: TppLabel;
    ppDetailBand11: TppDetailBand;
    ppDBText164: TppDBText;
    ppDBText167: TppDBText;
    ppDBText168: TppDBText;
    ppDBText169: TppDBText;
    ppDBText170: TppDBText;
    ppDBText171: TppDBText;
    ppDBText172: TppDBText;
    ppDBText173: TppDBText;
    ppDBText207: TppDBText;
    ppDBText208: TppDBText;
    ppDBText209: TppDBText;
    ppDBText211: TppDBText;
    ppDBText212: TppDBText;
    ppDBText213: TppDBText;
    ppDBText214: TppDBText;
    ppDBText215: TppDBText;
    ppDBText216: TppDBText;
    ppDBText217: TppDBText;
    ppDBText218: TppDBText;
    ppDBText219: TppDBText;
    ppDBText220: TppDBText;
    ppDBText221: TppDBText;
    ppDBText222: TppDBText;
    ppDBText223: TppDBText;
    ppDBText224: TppDBText;
    ppDBText236: TppDBText;
    ppDBText237: TppDBText;
    ppDBText238: TppDBText;
    ppDBText239: TppDBText;
    ppDBText240: TppDBText;
    ppSummaryBand10: TppSummaryBand;
    ppLabel45: TppLabel;
    ppDBText241: TppDBText;
    ppLabel302: TppLabel;
    ppDBText242: TppDBText;
    ppLabel303: TppLabel;
    ppDBText243: TppDBText;
    ppLabel304: TppLabel;
    ppDBText244: TppDBText;
    ppLabel305: TppLabel;
    ppDBText245: TppDBText;
    ppLabel306: TppLabel;
    ppDBText246: TppDBText;
    ppLabel307: TppLabel;
    ppDBText247: TppDBText;
    ppLabelMarcelloDateRangeFull: TppLabel;
    ppLabelMarcelloSystemsFull: TppLabel;
    cboperator: TJvCheckedComboBox;
    ppReport1: TppReport;
    ppDBPipeline1: TppDBPipeline;
    ppParameterList13: TppParameterList;
    ppDesignLayers13: TppDesignLayers;
    ppDesignLayer13: TppDesignLayer;
    ppHeaderBand11: TppHeaderBand;
    ppDetailBand12: TppDetailBand;
    ppFooterBand2: TppFooterBand;
    kbmMemTable1: TkbmMemTable;
    DataSource2: TDataSource;
    ppLabel49: TppLabel;
    Memo2: TMemo;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure connect1Click(Sender: TObject);
    procedure disconnect1Click(Sender: TObject);
    procedure btnCSVClick(Sender: TObject);
    procedure btnStepTwoClick(Sender: TObject);
    procedure btnCSV_XMLClick(Sender: TObject);
    procedure sheetPayTypeShow(Sender: TObject);
    procedure sheetCarTypeShow(Sender: TObject);
    procedure btnEMailSendClick(Sender: TObject);
    procedure btnTrackClick(Sender: TObject);
    procedure reportTrackBeforePrint(Sender: TObject);
    procedure ppDetailBand1BeforeGenerate(Sender: TObject);
    procedure btnCSV_HTMLClick(Sender: TObject);
    procedure tabDataShow(Sender: TObject);
    procedure JScroll(DataSet: TDataSet);
    procedure memAfterScroll(DataSet: TDataSet);
    procedure Q1AfterScroll(DataSet: TDataSet);
    procedure Q1BeforeOpen(DataSet: TDataSet);
    procedure EditCountryExit(Sender: TObject);
    procedure EditCityExit(Sender: TObject);
    procedure btnFzBuildSQLClick(Sender: TObject);
    procedure btnOK_NEWClick(Sender: TObject);
    procedure cbcancelsonlyClick(Sender: TObject);
    procedure cbActiveOnlyClick(Sender: TObject);
    procedure ppGroupHeaderBand2AfterPrint(Sender: TObject);
    procedure btnAllPayTypesClick(Sender: TObject);
    procedure btnNoPayTypesClick(Sender: TObject);
    procedure btnAllSippsList1Click(Sender: TObject);
    procedure btnNoSippsList1Click(Sender: TObject);
    procedure btnAllSippsList2Click(Sender: TObject);
    procedure btnNoSippsList2Click(Sender: TObject);
    procedure btnAllSippsList3Click(Sender: TObject);
    procedure btnNoSippsList3Click(Sender: TObject);
    procedure btnAllSippsList4Click(Sender: TObject);
    procedure btnNoSippsList4Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure tabStep2Show(Sender: TObject);
    procedure cbUsePUDateClick(Sender: TObject);
    procedure cbUseLastMinClick(Sender: TObject);
    procedure cbUseAgeClick(Sender: TObject);
    procedure cbUsePayTypeClick(Sender: TObject);
    procedure cbUseDurationClick(Sender: TObject);
    procedure cbUseGenderClick(Sender: TObject);
    procedure cbUseSIPPClick(Sender: TObject);
    procedure rgDirectTaClick(Sender: TObject);
    procedure btnFzClearClick(Sender: TObject);
    procedure btnLatestDatesClick(Sender: TObject);
    // ** list box
    procedure IncludeBtnClick(Sender: TObject);
    procedure ExcludeBtnClick(Sender: TObject);
    procedure IncAllBtnClick(Sender: TObject);
    procedure ExcAllBtnClick(Sender: TObject);
    // ** end list box
    procedure rgRestrictByEmailClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure cbUseDropDateClick(Sender: TObject);
    procedure EditDropCtryExit(Sender: TObject);
    procedure EditDropCityExit(Sender: TObject);
    procedure EditOperatorExit(Sender: TObject);
    procedure btnGRDResetClick(Sender: TObject);
    procedure cbUseDropCtryClick(Sender: TObject);
    procedure cbUseDropCityClick(Sender: TObject);
    procedure rgDSTypeClick(Sender: TObject);
    procedure btnFzTestClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnReFreshDataClick(Sender: TObject);
    procedure cbProductTypeSelect(Sender: TObject);
    procedure cbUseTYpesClick(Sender: TObject);
    procedure cbUseSourceClick(Sender: TObject);
    procedure tabstep2bShow(Sender: TObject);
    procedure cbFIlterGroupMoreClick(Sender: TObject);
    procedure tabOutputShow(Sender: TObject);
    procedure btnresortClick(Sender: TObject);
    procedure ppGroupHeaderBand3AfterPrint(Sender: TObject);
    procedure ppGroupHeaderBand4AfterPrint(Sender: TObject);
    procedure btnGetDataWMySQLClick(Sender: TObject);
    procedure btnkillClick(Sender: TObject);
    procedure btnGRDKillClick(Sender: TObject);
    procedure tabForbiddenZoneShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDbAllClick(Sender: TObject);
    procedure btnDbClearClick(Sender: TObject);
    procedure btnCombined2Click(Sender: TObject);
    procedure repcombinedFormattedBeforePrint(Sender: TObject);
    procedure ppHeaderBand6AfterPrint(Sender: TObject);
    procedure repcombinedFormattedPrintDialogClose(Sender: TObject);
    procedure btnytdClick(Sender: TObject);
    procedure repYTDPrintDialogClose(Sender: TObject);
    procedure repYTDBeforePrint(Sender: TObject);
    procedure ppHeaderBand7AfterPrint(Sender: TObject);
    procedure ppGroupFooterBand11BeforePrint(Sender: TObject);
    procedure btnSpSetClick(Sender: TObject);
    procedure tabspecialreportsShow(Sender: TObject);
    procedure ppGroupFooterBand9BeforePrint(Sender: TObject);
    procedure btnFleetMixClick(Sender: TObject);
    procedure repFleetMixBeforePrint(Sender: TObject);
    procedure repFleetMixPrintDialogClose(Sender: TObject);
    procedure ppHeaderBand8AfterPrint(Sender: TObject);
    procedure ppGroupFooterBand12BeforePrint(Sender: TObject);
    procedure connAfterConnect(Sender: TObject);
    procedure ppGroupFooterBand8BeforePrint(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ppGroupFooterBand14BeforePrint(Sender: TObject);
    procedure ppGroupFooterBand10BeforePrint(Sender: TObject);
    procedure btnRPBUSClick(Sender: TObject);
    procedure rpbus_get_namesClick(Sender: TObject);
    procedure repRepeatBizPrintDialogClose(Sender: TObject);
    procedure repRepeatBizBeforePrint(Sender: TObject);
    procedure Button30Click(Sender: TObject);
    procedure editDateOffsetDblClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnMainFillClick(Sender: TObject);
    procedure clbDBExit(Sender: TObject);
    procedure cboperatorEnter(Sender: TObject);
    procedure cbUseCreateDateClick(Sender: TObject);
    procedure btnRpBusNewClick(Sender: TObject);
    procedure btnDumpGridClick(Sender: TObject);
    procedure cbSIPPWildCardClick(Sender: TObject);
    procedure cbReleaseNumClick(Sender: TObject);
    procedure cbUseIataListClick(Sender: TObject);
    procedure btnIataListClearClick(Sender: TObject);
    procedure repSalesPrintDialogClose(Sender: TObject);
    procedure repclemailPrintDialogClose(Sender: TObject);
    procedure repTAPrintDialogClose(Sender: TObject);
    procedure btnSippAnalysisSIPPClick(Sender: TObject);
    procedure btnSippAnalysisClick(Sender: TObject);
    procedure cbUseRange2Click(Sender: TObject);
    procedure rgStep1PayTypeClick(Sender: TObject);
    procedure btnRunSalesProdAnalClick(Sender: TObject);
    procedure ppHeaderBand9AfterPrint(Sender: TObject);
    procedure rptBrettCtryOpPrintDialogClose(Sender: TObject);
    procedure ppGroupFooterBand5BeforePrint(Sender: TObject);
    procedure ppGroupFooterBand6BeforePrint(Sender: TObject);
    procedure cbPlusBasicClick(Sender: TObject);
    procedure dtpStartChange(Sender: TObject);
    procedure cbspaSHowYearSysCtryMstrOpSourceClick(Sender: TObject);
    procedure cbspaSHowYearSysCtryMstrOpSIPPSrcClick(Sender: TObject);
    procedure cbspaSHowYearSysCtryMstrOpSIPPClick(Sender: TObject);
    procedure ppGroupFooterBand17BeforePrint(Sender: TObject);
    procedure rptBrettSipp2PrintDialogClose(Sender: TObject);
    procedure ppGroupFooterBand22BeforePrint(Sender: TObject);
    procedure ppGroupFooterBand21BeforePrint(Sender: TObject);
    procedure cbspaSHowYearSysCtryMstrOpClick(Sender: TObject);
    procedure cbspaSHowYearSysCtryClick(Sender: TObject);
    procedure ppHeaderBand10AfterPrint(Sender: TObject);
    procedure cbUseClientCtryClick(Sender: TObject);
    procedure mnuCloseClick(Sender: TObject);
    procedure mnuHelpClick(Sender: TObject);
    procedure mnuAboutClick(Sender: TObject);
    procedure btnMarcelloReportClick(Sender: TObject);
    procedure ppReportMarcelloPrintDialogClose(Sender: TObject);
    procedure ppHeaderBandMarcelloAfterPrint(Sender: TObject);
    procedure IfxQueryMarcelloFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure ppDBTextMarcilleoCampGetText(Sender: TObject; var Text: string);
    procedure cbspaShowDetailClick(Sender: TObject);
    procedure ppGroupHeaderBandMarcelloCampaignAfterPrint(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ppHeaderBandMarcelloFullAfterPrint(Sender: TObject);
    procedure kbmMarcelloAfterScroll(DataSet: TDataSet);
 //   function GetIDACQuery(conn: TFDConnection): TFDQuery;
  private
 //   extra: TExtraOptions;
    LatestRunDate: TDateTime;
    fbolMarcello: boolean;
    procedure addToRunLog(sender: TButton); // add June 6, 2007 per IK - they all needed this yesterday
    procedure addToRunLog2(ReportName, ReportTitle: string);
    procedure FillDBS();
    procedure OpenQ(showdata: Boolean = true);
    function InitQ_CloseClear(): boolean;
    procedure OpenFile(InFileName: string);
    procedure FocusCtry();
    procedure FocusCity();
    procedure FocusOperator();
    function GetConsortArg(): string;
    function GetEMailArg(): string;
    procedure EMAIL(sFILENAME: string);
    procedure SetMemFields(Marcillo: boolean = false); // new method
    procedure FillMemFields(); // new method
    procedure SortMem();
    procedure PrintRoutine(InFileName, ReportTitle: string);
    procedure DoOutPut(sender: TObject; O_Type, InFileName: string);
    function BuildSQL(Sender: TObject; flag: string = ''): string;
    procedure FindFieldPositions(dset: TkbmMemTable; Marcello: boolean = false);
    procedure Build_RName_RTitle(sender: TObject; var Reportname, ReportTitle: string);
    procedure FillGroupByListBox();
    procedure DoSecondPass();
    procedure SetOrUnSetCLB(bDOCHECK: boolean);
    function GetDB_InStatement(): string;
    function GetSelectedBaseSysList(): string;
    procedure SippAnal(Sender: TObject);
    function BuildMarcelloSQL(isDetail: boolean): string;
    function FetchMarcelloMemTableData(sql: string): string;
    function RunMarcelloReport: string;
    procedure SetMemFieldsMarcillo;
    procedure PopulateHostCampIdMEM(isDetail: boolean);
  public
    FormIsCreated: boolean;
    LOG_PATH: string;
    specialReportsList: TStringList;
    // list box stuff
    procedure MoveSelected(List: TCustomListBox; Items: TStrings);
    procedure SetItem(List: TListBox; Index: Integer);
    function GetFirstSelection(List: TCustomListBox): Integer;
    procedure SetButtons;
    procedure fillptypebox();
    procedure fillSubTypeBox();
    procedure fillSourceBox();
    procedure PrintSpecialReportsRoutine(theReport: TppReport; InFileName, ReportTitle: string);
    procedure SetUpMemTable(WhatTODo: string; TableToSetUp: TkbmMemTable);
    function DoRefetchData(TableToLookAt: TkbmMemTable): Boolean;
    procedure GetCountries(); // fill mem table with country names
    function GetCountryName(ctrycode, provcode: string): string; // locate name based on ctry code and prov
    procedure FillOMList(var InList: TStringList);
    function getOffset(): string;
    function MSTR_List_Dates_OK(var errstr: string): Boolean;
    procedure FillNOTOperatorCB(sender: TObject);
    function GetNOT_OPER_IN_Text(): string;
    procedure SaveACSV(DS: TDatasource; reportTitle: string);
    procedure makeReport;
  //  procedure StrAdd(var TargetString: string; StringToAdd: string);
  //  function qs(s: string): string;
 //   function A_SplitStrings(const str: string; const separator: string; Strings: TStrings): TStrings;
//    procedure StringListToInStatement(var InOutList: TStringList);
  end;

var
  frmMainMkt: TfrmMainMkt;
  startTime,
    endTime: DWORD;
  GlobFName,
    globEMLFileName: string;
  xxxxx: Integer;
  bKILL: Boolean;
  gintProcessType: integer;

  // field defs for memTable (mem normally)
  tfmem_basesys,
    tfmem_voucher, // includes voucher||"-"||release
    tfmem_c_fullname, // lastname, firstname, init
    tfmem_confirmation, // confirmation number
    tfmem_date_created, // vchdate
    tfmem_date_of_svc, // pick up date
    tfmem_time_of_svc,
    tfmem_retail_total,
    tfmem_opdue_curr,
    tfmem_curr_type,
    tfmem_curr_exch_rate,
    tfmem_sipp,
    tfmem_oper_car_code,
    tfmem_date_of_transaction,
    tfmem_commission,
    tfmem_profit,
    tfmem_duration,
    tfmem_lead_days_create_pu, // changed from PD_PU jan 28 2008
    tfmem_dtyear, // add 04/27/2011
    tfmem_lead_days_pdpu, // must calculate - not stored
    tfmem_iata,
    tfmem_country_pu,
    tfmem_city_pu,
    tfmem_loc_pu,
    tfmem_operator,
    tfmem_consortium,
    tfmem_cc_name,
    tfmem_vcspecrequest,
    tfmem_service_type,
    tfmem_source,
    tfmem_c_prefix,
    tfmem_c_addr1,
    tfmem_c_addr2,
    tfmem_c_prov,
    tfmem_c_city,
    tfmem_c_postal,
    tfmem_c_country,
    tfmem_c_email,
    tfmem_c_phone,
    tfmem_c_age,
    tfmem_group_break_sort, // values set according to how to group ...
    tfmem_vcount,
    tfmem_promo1, tfmem_promo2, tfmem_promo3,
    // add balance of fields from g_Voucher_summary and g_vendor:
    // 1.28.2008
  tfmem_active_history,
    tfmem_p_or_q,
    tfmem_payment_type,
    tfmem_fp_pp,
    tfmem_plus_basic,
    tfmem_rid,
    tfmem_auth_code,
    tfmem_product_type,
    tfmem_business_source,
    tfmem_do_ctry,
    tfmem_do_city,
    tfmem_do_loc,
    tfmem_updated_dt, // vcstdte1
    tfmem_do_dt,
    tfmem_do_time,
    tfmem_op_rate_code,
    tfmem_bus_account_number,
    tfmem_car_name,
    tfmem_sipp_description,
    tfmem_num_passengers,
    tfmem_comm_due,
    tfmem_comm_percent,
    tfmem_home_curr, // system curr, not operator
    tfmem_vat_tax_rt,
    tfmem_home_ctry,
    tfmem_base_rate_retail,
    tfmem_discount_retail,
    tfmem_insurance_tot_retail,
    tfmem_tax_tot_retail,
    tfmem_pu_fee_retail,
    tfmem_do_fee_retail,
    tfmem_xtra_fee_retail,
    tfmem_base_rate_wholesale,
    tfmem_insurance_tot_wholesale,
    tfmem_tax_tot_wholesale,
    tfmem_pu_fee_wholesale,
    tfmem_do_fee_wholesale,
    tfmem_wholesale_total,
    tfmem_operator_due,
    tfmem_deposit_amt,
    tfmem_waived_amt,
    tfmem_deposit_after_waive,
    tfmem_due_at_pu,
    tfmem_deferred_amt,
    tfmem_gross_revenue,
    tfmem_operator_cost,
    tfmem_count_new,
    tfmem_count_cxl,
    tfmem_count_change,
    tfmem_direct_or_ta,
    tfmem_member_account,
    // vendor_fields:
  tfmem_gv_name,
    tfmem_gv_legal_name,
    tfmem_gv_contact_name,
    tfmem_gv_addr1,
    tfmem_gv_addr2,
    tfmem_gv_addr3,
    tfmem_gv_addr4,
    tfmem_gv_city,
    tfmem_gv_prov,
    tfmem_gv_postal,
    tfmem_gv_country,
    tfmem_gv_phone_prefix,
    tfmem_gv_phone_num,
    tfmem_gv_fax_prefix,
    tfmem_gv_fax_num,
    tfmem_gv_email,
    //marcello
  tfmem_vs_retail_total,
    tfmem_vs_profit,
    tfmem_vs_count_voucher,
    tfmem_vs_paid_dt,
    tfmem_gc_ctry, //client_ctry
    //marcello webfields wbadcampaigns wcamps
  tfmem_wcamps_campid,
    tfmem_wcamps_campaign,
    //marcello webfields wbadhosts wcamps
  tfmem_whosts_host: TField;

implementation

{$R *.dfm}

{procedure StringListToInStatement(var InOutList: TStringList);
var workingList: TStringList; // returns  ' IN("item1", "item2", "item3") '
  i, x: Integer; // assuming list contains 3 lines with items: item1 item2 item3
begin
  if InOutList = nil then exit
  else if trim(InOutList.Text) = '' then exit;

  try
    workingList := TStringList.Create;
    try
      workingList.Clear;
      x := InOutList.Count - 1;

      for i := 0 to x do
      begin
        if (i = x) and (i = 0) then // only 1 item in list - beginning and end
          workingList.add('IN ( "' + InOutList[i] + '")')
        else if (i = 0) then // begining
          workingList.add('IN ( "' + InOutList[i] + '",')
        else if (i = x) then // end
          workingList.Add('"' + InOutList[i] + '")')
        else // middle
          workingList.Add('"' + InOutList[i] + '",');
      end; // for
      InOutList.Text := workingList.Text;
    except on e: exception do
      begin
        raise(Exception.Create('StringListToInStatement error -> ' + e.Message));
      end;
    end;
  finally
    if assigned(WorkingList) then FreeAndNIl(WorkingList);
  end;
end;

function A_SplitStrings(const str: string; const separator: string; Strings: TStrings): TStrings;
// Fills a string list with the parts of "str" separated by "separator".
var
  n: integer;
  p, q, s: PChar;
  item: string;
begin
  if Strings = nil then
  begin
    Result := nil;
    exit;
  end
  else Result := Strings;
  Result.Clear;

  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);

    repeat
      q := AnsiStrPos(p, s);
      if q = nil then q := AnsiStrScan(p, #0);
      SetString(item, p, q - p);
      Result.Add(item);
      p := q + n;
    until q^ = #0;

  except
    item := '';
    raise;
  end;
end;

procedure SAdd(var TargetString: string; StringToAdd: string);
var i, j: integer;
begin
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end;



function qs(s: string): string;
var p: integer;
   // replace any single or double quotes or ` chars in text with spaces
   // then wrap s in quotes
begin
  if (length(trim(s)) = 0) then
  begin
    result := '"NULL"';
  end
  else
  begin
    // #39 = ' (singleQuote)
    // #34 = " (doubleQuote)
    // #96 = ` (backSlant SingleQuote)
    p := pos(#39, s);
    while (p > 0) do
    begin // ' replace singleQuotes with spaces
      s[p] := ' ';
      p := pos(#39, s);
    end;

    p := pos(#34, s);
    while (p > 0) do
    begin // " replace doubleQuotes with spaces
      s[p] := ' ';
      p := pos(#34, s);
    end;

    p := pos(#96, s);
    while (p > 0) do
    begin // ` replace backslanted singlQuotes with spaces
      s[p] := ' ';
      p := pos(#96, s);
    end;

    result := #34 + s + #34; // double quote
  end;
end;



procedure StrAdd(var TargetString: string; StringToAdd: string);
var // use this instead of string := String + string - Isfast = true
  i, j: integer;
begin
  // procedure Move(var Source: Type; var Dest: Type; Count: Integer);
  // Move copies Count bytes from Source to Dest. No range checking is performed.
  // Move compensates for overlaps between the source and destination blocks.
  // Whenever possible, use the global SizeOf function (Delphi) or the sizeof operator (C++)
  // to determine the count.
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end; }


procedure TfrmMainMkt.PopulateHostCampIdMEM(isDetail: boolean);
var
  strPromoCodeMem, strPromoCodeQ: string;
begin
  try
    try

      Set_FireDAC_Connection(conHost, 'WEBDATA');
      conHost.Open;
      IfxQueryHostCampid.Connection := conHost;
      IfxQueryHostCampid.SQL.Clear;
      IfxQueryHostCampid.SQL.Text := _HOST_CAMPID_SQL;
      IfxQueryHostCampid.Open;

      while not kbmMarcello.eof do
      begin
    //grab the the promo id to compare to web campaign ID
        strPromoCodeMem := kbmMarcello.FieldByName('promo2').AsString;
    //we need to remove the x's that are acting as place holders
        kbmMarcello.Edit;
        kbmMarcello.FieldByName('campid').AsString := '';
        kbmMarcello.FieldByName('campaign').AsString := '';
      //summary does not contain campaign or host
        if isDetail then
        begin

          kbmMarcello.FieldByName('host').AsString := '';
        end;
        kbmMarcello.Post;
    //we need do this only if we have an ID
        if trim(strPromoCodeMem) <> '' then
        begin
      //start back at the top
          IfxQueryHostCampid.First;
          strPromoCodeQ := '';
          while not IfxQueryHostCampid.eof do
          begin
        //grab the web campaign ID to compare to the promo id
            strPromoCodeQ := IfxQueryHostCampid.FieldByName('campid').AsString;
            if trim(strPromoCodeQ) = trim(strPromoCodeMem) then
            begin
              kbmMarcello.Edit;
              kbmMarcello.FieldByName('campid').AsString := strPromoCodeQ;
              kbmMarcello.FieldByName('campaign').AsString :=
                IfxQueryHostCampid.FieldByName('campaign').AsString;
              //showmessage(IfxQueryHostCampid.FieldByName('campaign').AsString);
          //summary does not contain campaign or host
              if isDetail then
              begin

                kbmMarcello.FieldByName('host').AsString :=
                  IfxQueryHostCampid.FieldByName('host').AsString;
              end;
              kbmMarcello.Post;
              break;
            end;
            IfxQueryHostCampid.Next;
        //
          end;
        end;
        kbmMarcello.Next;
      end;

    except on e: exception do
      begin
        showmessage('PopulateHostCampIdMEM ERROR: ' + e.Message);
      end;
    end;
  finally
    conHost.close;
    IfxQueryHostCampid.Close;
  end;
end;

function TfrmMainMkt.GetNOT_OPER_IN_Text(): string;
var
  temp: string;
  strings: TStrings;
  theList, theSecondList: String;
  i, x: integer;
begin
  result := '';
  try
      temp := cbOperator.Text; // assuming sSIPP is set
      if cbUseSummaryTable.Checked then
        theSecondList := 'vs_operator_code NOT in("'
      else
        theSecondList := ('vopid NOT In("');

        if temp.Contains(',')  then
             begin
              temp := ReplaceText(temp, ',', '","');
             end;

      thesecondlist := thesecondlist + temp + '")';

      if theSecondList > '!' then result := theSecondList;
    except on e: exception do
      begin
        raise(exception.create('Get DB in text --> ' + e.message));
      end;
    end;
end;


procedure TfrmMainMkt.FillNOTOperatorCB(sender: TObject);
var MyQuery: TFDQuery;
  i: Integer;
  theSQL: string;
  OneIsSelected: boolean;

  function Get_InStatement(): string;
  var temp: string;
    x: Integer;
    sql: string;
    DoIt: boolean;
  begin
    result := '';
    DoIt := false;

    sql := ' gob_basesys in(';

    for x := 0 to clbdb.Items.Count - 1 do
    begin
      if clbdb.Checked[x] then
      begin
        DoIt := true;
        sql := sql + '''' + clbdb.items[x] + ''',';
      end;
    end;

    if DoIt then
    begin // remove last comma and replace with ')'
      sql := copy(sql, 1, length(sql) - 1);
      sql := sql + ')';
   //   result := sql;
    end
    else sql := '';
    memo1.Lines.add('FILL NOT BOX sql: ' + sql);
    result := sql;
  end;

begin
  if not conn.Connected then
  begin
    showmessage('connect first.');
    exit;
  end;

  oneIsSelected := false;
  for i := 0 to clbdb.Items.Count - 1 do
  begin
    if clbdb.Checked[i] then oneIsSelected := true;
  end;

  if not OneIsSelected then
  begin
    if sender = cbOperator then
      showmessage('please select database(s) first');
    cbOperator.Items.Clear;
    exit;
  end;

  theSQL := '';
  theSQL := 'Select gob_operator_code From g_operators_booked where ' + Get_InStatement();

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSQL);
      MyQuery.Open;
      MyQuery.First;
      cbOperator.Items.Clear;


      while not (MyQuery.Eof) do
      begin
        if not (trim(MyQuery.Fields[0].AsString) = '') then
        cbOperator.Items.Add(MyQuery.Fields[0].AsString);
        MyQuery.Next;
      end;

     { with cbOperator do
      begin
        for i := Pred(Items.Count) downto 1 do
        begin
      //  showmessage(' it is making it here 3');
           cbOperator.items[i]:= true;
        end;
      end;   }
    except on e: exception do
      begin
        showmessage('Failed to fill Operators box. -->' + e.Message);
      end;
    end;
  finally
    MyQuery.free;
  end;
end;

function TfrmMainMkt.MSTR_List_Dates_OK(var errstr: string): Boolean;
begin
  result := true;

  if trunc(dtpStart.date) < Trunc(dtpMasterListStart.date) then
  begin
    errstr := 'Master List start date greater than start date.';
    result := false;
    exit;
  end
  else if trunc(dtpend.Date) > trunc(dtpMasterListEND.date) then
  begin
    errstr := 'Master List end date less than "end" date.';
    result := false;
    exit;
  end;

  if cbUseRange2.checked then
  begin
    if trunc(dtprange2start.date) < trunc(dtpMasterListStart.date) then
    begin
      errstr := 'Master List start date greater than range 2 start date.';
      result := false;
      exit;
    end
    else if trunc(dtprange2end.Date) > trunc(dtpMasterListEND.date) then
    begin
      errstr := 'Master List end date less than range 2 "end" date.';
      result := false;
      exit;
    end;
  end;

  if cbUseRange3.checked then
  begin
    if trunc(dtprange3start.date) < trunc(dtpMasterListStart.date) then
    begin
      errstr := 'Master List start date greater than range 3 start date.';
      result := false;
    end
    else if trunc(dtprange3end.Date) > trunc(dtpMasterListEND.date) then
    begin
      errstr := 'Master List end date less than range 3 "end" date.';
      result := false;
    end;
  end;
end;

function TfrmMainMkt.GetCountryName(ctrycode, provcode: string): string;
begin
  result := '';
  if not memCountries.Locate('countrycode;provincecode;',
    VarArrayOf([ctrycode, '*']), []) then
  begin // plug * for now
    exit;
  end
  else
  begin
    result := memCOuntries.FieldByName('countryname').asstring;
  end;
end;

procedure TfrmMainMkt.GetCountries();
var MyQuery: TFDQuery;
  dataSetCountry: Tdataset;
begin
  if not FormIsCreated then exit;

  if not conn.Connected then
  begin
    try connect1Click(nil);
    except;
    end;
  end;

  if not conn.Connected then
  begin
    raise(exception.Create('GetCountries: not connected to whse'));
    exit;
  end;

  try
    if not (memcountries.IsEmpty) then memCountries.EmptyTable;
  except on e: exception do
    begin
      raise(exception.Create('GetCountries empty table --> ' + e.Message));
      exit;
    end;
  end;

  memo1.Lines.add('filling mem countries');
  dataSetCountry := dsSpecialReps.DataSet;
  dsspecialReps.DataSet := memcountries;

  try
    MyQuery := getidacquery(conn);
    try
      myquery.SQL.Clear;
      myquery.SQL.Add('select * From countries;');
      MyQuery.Open;
      memCountries.LoadFromDataSet(myquery, [mtcpoStructure, mtcpoProperties, mtcpoCalculated]);
      MyQuery.Close;
      memo1.Lines.add('GetCountries fill table OK');
    except on e: exception do
      begin
        raise(exception.Create('GetCountries fill table --> ' + e.Message));
      end;
    end;
  finally
    if assigned(myquery) then freeandnil(myquery);
    dsspecialReps.DataSet := dataSetCountry;
  end;
end;

function TfrmMainMkt.DoRefetchData(TableToLookAt: TkbmMemTable): Boolean;
begin
  result := true;
  exit;
  // need to work on re-setting file names
  if TableToLookAt = nil then exit;
  if not (TableToLookAt.Active) then exit;
  if TableToLookAt.IsEmpty then exit;

  if MessageDlg('It looks like you already retrieved some data.' + #10#13 +
    'Do you need to re-fetch the data?'
    , mtConfirmation, [mbYes, mbNo], 0) = mrNo then result := false;
end;

procedure TfrmMainMkt.SetUpMemTable(WhatTODo: string; TableToSetUp: TkbmMemTable);
begin
  WhatTODo := UpperCase(WhatToDo);
  try
    with TableToSetUp do
    begin
      active := false;
      indexdefs.Clear;

      with FieldDefs do
      begin
        Clear;

        if WhatTODo = 'COMBINED' then
        begin
          Add('year', ftString, 5, false);
          add('operator_master', ftString, 20, false);
          add('operator', ftString, 20, false);
          add('country', ftString, 10, false);
          Add('USA_COUNT', ftInteger, 0, False);
          Add('KEM_COUNT', ftInteger, 0, False);
          Add('CANA_COUNT', ftInteger, 0, False);
          Add('HOHO_COUNT', ftInteger, 0, False);
          Add('UK_COUNT', ftInteger, 0, False);
          Add('EURO_COUNT', ftInteger, 0, False);
          Add('AUS_COUNT', ftInteger, 0, False);
          Add('NEWZ_COUNT', ftInteger, 0, False);
          Add('SAF_COUNT', ftInteger, 0, False);
          Add('LAC_COUNT', ftInteger, 0, False);
          Add('HOT_COUNT', ftInteger, 0, False);
          indexdefs.Add('idxmem', 'year;country;operator_master;', []);
        end
        else if WhatToDo = 'YTD' then
        begin
          Add('year', ftString, 5, false);
          add('system', ftString, 5, false);
          add('country', ftString, 10, false);
          add('operator_master', ftString, 20, false);
          add('operator', ftString, 20, false);
          add('bookings', ftInteger, 0, false);
          add('total_duration', ftFloat, 0, false);
          add('ave_duration', ftFloat, 0, false);
          add('total_profit', ftFloat, 0, false);
          add('ave_profit', ftFloat, 0, false);
          add('total_days', ftInteger, 0, false);
          add('total_op_due', ftFloat, 0, false);
          add('ave_op_due', ftFloat, 0, false);
          add('deposit', ftFloat, 0, false);
          indexdefs.Add('idxmem', 'year;system;operator;country', []);
        end
        else if WhatTODo = 'SIPPANALYSIS' then
        begin // this is brett's report 4/15/2011
                 // obsolete 5/13/2011
          Add('year', ftString, 5, false);
          add('system', ftString, 5, false);
          add('country_pu', ftString, 10, false);
          add('operator', ftString, 20, false);
          add('operator_master', ftString, 20, false);
          add('biz_source', ftString, 10, false);
          add('plus_basic', ftString, 1, false);
          add('bookings', ftInteger, 0, false);
          add('retail_total', ftFloat, 0, false);
          add('discount_total', ftFloat, 0, false);
          add('adjusted_retail_total', ftFloat, 0, false);
          add('wholesale_total', ftFloat, 0, false);
          add('commission_total', ftFloat, 0, false);
          add('deposit_total', ftFloat, 0, false);
          add('duration_total', ftFloat, 0, false);
          add('profit_total', ftFloat, 0, false);
          add('gross_margin', ftFloat, 0, false);
          add('net_margin', ftFloat, 0, false);
          add('profit_margin', ftFloat, 0, false);
          add('ave_duration', ftFloat, 0, false);
          add('ave_profit', ftFloat, 0, false);
          add('ave_wholesale', ftFloat, 0, false);
          //https://autoeurope.jira.com/browse/DSS-2
          add('wholesale_currency', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-3
          add('ave_whl_currency', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-4
          add('whl_currency_daily', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-5
          add('ave_exchange', ftFloat, 0, False);
          add('retail_daily', ftFloat, 0, False);
          add('discount_daily', ftFloat, 0, False);
          add('adjusted_retail_daily', ftFloat, 0, False);
          add('commission_daily', ftFloat, 0, False);
          add('profit_daily', ftFloat, 0, False);
          add('wholesale_daily', ftFloat, 0, False);
          Add('sort_field', ftString, 50, false);
        end
        else if WhatToDo = 'SIPPANALYSIS_SIPP' then
        begin
        //add to kbm here
          Add('year', ftString, 5, false);
          add('system', ftString, 5, false);
          add('country_pu', ftString, 10, false);
          add('operator', ftString, 20, false);
          add('operator_master', ftString, 20, false);
          add('biz_source', ftString, 10, false);
          add('plus_basic', ftString, 1, false);
          add('sipp', ftString, 20, false); // only diff between this and regular sipp analysis
          add('short_sipp', ftString, 4, false);
          add('bookings', ftInteger, 0, false);
          add('retail_total', ftFloat, 0, false);
          add('discount_total', ftFloat, 0, false);
          add('adjusted_retail_total', ftFloat, 0, false);
          add('wholesale_total', ftFloat, 0, false);
          add('commission_total', ftFloat, 0, false);
          add('deposit_total', ftFloat, 0, false);
          add('duration_total', ftFloat, 0, false);
          add('profit_total', ftFloat, 0, false);
          add('gross_margin', ftFloat, 0, false);
          add('net_margin', ftFloat, 0, false);
          add('profit_margin', ftFloat, 0, false);
          add('ave_duration', ftFloat, 0, false);
          add('ave_profit', ftFloat, 0, false);
          add('ave_wholesale', ftFloat, 0, false);
          add('retail_daily', ftFloat, 0, False);
          add('discount_daily', ftFloat, 0, False);
          add('adjusted_retail_daily', ftFloat, 0, False);
          add('commission_daily', ftFloat, 0, False);
          add('profit_daily', ftFloat, 0, False);
          add('wholesale_daily', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-2
          add('wholesale_currency', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-3
          add('ave_whl_currency', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-4
          add('whl_currency_daily', ftFloat, 0, False);
          //https://autoeurope.jira.com/browse/DSS-5
          add('ave_exchange', ftFloat, 0, False);
          Add('sort_year_sys_ctry', ftString, 25, false);
          Add('sort_year_sys_ctry_mstrop', ftString, 50, false);
          Add('sort_year_sys_ctry_mstrop_source', ftString, 75, false); // add 7/11/2011
          Add('sort_year_sys_ctry_mstrop_sipp', ftString, 75, false);
          Add('sort_year_sys_ctry_mstrop_sipp_source', ftString, 85, false); // new
        end
        else if WhatToDo = 'REPEATBIZ' then
        begin
          add('system', ftString, 5, false);
          add('orig_start_date', ftDate, 0, false);
          add('orig_end_date', ftDate, 0, false);
          add('orig_bookings', ftInteger, 0, false);
          add('repeat_start_date', ftDate, 0, false);
          add('repeat_end_date', ftDate, 0, false);
          add('repeat_bookings', ftInteger, 0, false);
          add('repeat_percent', ftFloat, 0, false);
          // no index needed
        end
        else if WhatToDo = 'REPEATBIZ_2' then
        begin
          add('system', ftString, 5, false);
          add('orig_start_date', ftDate, 0, false);
          add('orig_end_date', ftDate, 0, false);
          add('repeat_start_date', ftDate, 0, false);
          add('repeat_end_date', ftDate, 0, false);
          add('orig_bookingsDC', ftInteger, 0, false);
          add('orig_bookingsTA', ftInteger, 0, false);
          add('orig_bookingsTotal', ftInteger, 0, false);
          // add repeat_year_bookings_DC_TA 07/01/2010
          add('repeat_year_bookings_DC', ftInteger, 0, false);
          add('repeat_year_bookings_TA', ftInteger, 0, false);
          add('repeat_year_bookings_total', ftInteger, 0, false); // new get via same method as "orig_bookingsTotal"
          add('repeat_bookingsDC', ftInteger, 0, false);
          add('repeat_bookingsTA', ftInteger, 0, false);
          add('repeat_bookings_DC_TA', ftInteger, 0, false); // new = repeat_bookingsDC + repeat_bookingsTA
          add('repeat_percentDC', ftFloat, 0, false);
          add('repeat_percentTA', ftFloat, 0, false);
          add('repeat_percent_DC_TA', ftFloat, 0, false); // new plug
        end
        else if WhatTODo = 'FLEETMIX' then
        begin
          Add('year', ftString, 5, false);
          add('country', ftString, 10, false);
          add('operator', ftString, 20, false);
          add('sipp', ftString, 10, false);
          Add('USA_COUNT', ftInteger, 0, False);
          add('usa_plus_count', ftInteger, 0, false);
          add('usa_basic_count', ftInteger, 0, false);
          add('usa_ave_duration', ftFloat, 0, false);
          add('usa_ave_profit', ftFloat, 0, false);
          add('usa_op_due', ftFloat, 0, false);
          Add('KEM_COUNT', ftInteger, 0, False);
          add('kem_plus_count', ftInteger, 0, false);
          add('kem_basic_count', ftInteger, 0, false);
          add('kem_ave_duration', ftFloat, 0, false);
          add('kem_ave_profit', ftFloat, 0, false);
          add('kem_op_due', ftFloat, 0, false);
          Add('CANA_COUNT', ftInteger, 0, False);
          add('cana_plus_count', ftInteger, 0, false);
          add('cana_basic_count', ftInteger, 0, false);
          add('cana_ave_duration', ftFloat, 0, false);
          add('cana_ave_profit', ftFloat, 0, false);
          add('cana_op_due', ftFloat, 0, false);
          Add('HOHO_COUNT', ftInteger, 0, False);
          add('hoho_plus_count', ftInteger, 0, false);
          add('hoho_basic_count', ftInteger, 0, false);
          add('hoho_ave_duration', ftFloat, 0, false);
          add('hoho_ave_profit', ftFloat, 0, false);
          add('hoho_op_due', ftFloat, 0, false);
          Add('UK_COUNT', ftInteger, 0, False);
          add('uk_plus_count', ftInteger, 0, false);
          add('uk_basic_count', ftInteger, 0, false);
          add('uk_ave_duration', ftFloat, 0, false);
          add('uk_ave_profit', ftFloat, 0, false);
          add('uk_op_due', ftFloat, 0, false);
          Add('EURO_COUNT', ftInteger, 0, False);
          add('euro_plus_count', ftInteger, 0, false);
          add('euro_basic_count', ftInteger, 0, false);
          add('euro_ave_duration', ftFloat, 0, false);
          add('euro_ave_profit', ftFloat, 0, false);
          add('euro_op_due', ftFloat, 0, false);
          Add('AUS_COUNT', ftInteger, 0, False);
          add('aus_plus_count', ftInteger, 0, false);
          add('aus_basic_count', ftInteger, 0, false);
          add('aus_ave_duration', ftFloat, 0, false);
          add('aus_ave_profit', ftFloat, 0, false);
          add('aus_op_due', ftFloat, 0, false);
          Add('NEWZ_COUNT', ftInteger, 0, False);
          add('newz_plus_count', ftInteger, 0, false);
          add('newz_basic_count', ftInteger, 0, false);
          add('newz_ave_duration', ftFloat, 0, false);
          add('newz_ave_profit', ftFloat, 0, false);
          add('newz_op_due', ftFloat, 0, false);
          Add('SAF_COUNT', ftInteger, 0, False);
          add('saf_plus_count', ftInteger, 0, false);
          add('saf_basic_count', ftInteger, 0, false);
          add('saf_ave_duration', ftFloat, 0, false);
          add('saf_ave_profit', ftFloat, 0, false);
          add('saf_op_due', ftFloat, 0, false);
          Add('LAC_COUNT', ftInteger, 0, False);
          add('lac_plus_count', ftInteger, 0, false);
          add('lac_basic_count', ftInteger, 0, false);
          add('lac_ave_duration', ftFloat, 0, false);
          add('lac_ave_profit', ftFloat, 0, false);
          add('lac_op_due', ftFloat, 0, false);
          Add('HOT_COUNT', ftInteger, 0, False);
          add('hot_plus_count', ftInteger, 0, false);
          add('hot_basic_count', ftInteger, 0, false);
          add('hot_ave_duration', ftFloat, 0, false);
          add('hot_ave_profit', ftFloat, 0, false);
          add('hot_op_due', ftFloat, 0, false);
          indexdefs.Add('idxmem', 'year;country;operator;sipp', []);
        end
        else
          raise(exception.Create('wrong WhatTODo in SetUpMemTable!'));

        if not Exists then CreateTable;
        active := true;
      end; // with mTbl
    end;
  except on e: exception do
    begin
      raise(exception.create('set table --> ' + e.Message));
    end;
  end;
end;

function TfrmMainMkt.GetDB_InStatement(): string;
var temp: string;
  x: Integer;
  sql: string;
  DoIt: boolean;
begin
  result := '';
  DoIt := false;

  if cbUseSummaryTable.Checked then
    sql := ' vs_basesys in('
  else
    sql := ' basesys in(';

  for x := 0 to clbdb.Items.Count - 1 do
  begin
    if clbdb.Checked[x] then
    begin
      DoIt := true;
      sql := sql + '''' + clbdb.items[x] + ''',';
    end;
  end;

  if DoIt then
  begin // remove last comman and replace with ')'
    sql := copy(sql, 1, length(sql) - 1);
    sql := sql + ')';
  end
  else sql := '';
  result := sql;
end;

function TfrmMainMkt.GetSelectedBaseSysList(): string;
var s, temp: string;
  x: Integer;
  doit, bolAllChecked: boolean;
begin
  result := '';
  doit := false;
  s := '';

  bolAllChecked := true;
  for x := 0 to clbdb.Items.Count - 1 do
  begin // all are selected
    if not clbdb.Checked[x] then
    begin
      bolAllChecked := false;
      break;
    end;
  end;

  if (bolAllChecked) then
  begin
    s := 'ALL Systems';
  end
  else
  begin
    for x := 0 to clbdb.Items.Count - 1 do
    begin
      if clbdb.Checked[x] then
      begin
        doit := true;
        s := s + clbdb.items[x] + ',';
      end;
    end;

    if DoIt then
    begin // remove last comma
      s := copy(s, 1, length(s) - 1);
    end
    else s := '';
  end;
  result := s;
end;

procedure TfrmMainMkt.SortMem();
var temp: string;
begin
  sb.Panels[2].Text := 'sorting data. please wait.';
  application.ProcessMessages;
  temp := '';

  if cbFIlterGroupMore.Checked then
  begin // additional sorting required for two pass report
    case rgdateType_FilterGroup.ItemIndex of
      0: temp := 'date_created'; // 'vs_voucher_create_dt'; // create
      1: temp := 'date_of_transaction'; //'vs_paid_dt'; // pd_dt
      2: temp := 'date_of_svc'; // 'vs_pu_dt'; // pick up
      3: temp := 'do_dt'; //'vs_do_dt'; // drop
    else ;
    end;

    case rgoldestNewest_FilterGroup.ItemIndex of
      0: temp := temp + ';'; // oldest
      1: temp := temp + ':D;'; // newest :D is descending
    else ;
    end;
    {
      keep in mind this argument appears backwards - e.g. :D (descending) for newest -
      and non descending for oldest
      ~ reason for this is when traversing the dataset, I plug the "first different" record found into mem2
      rather than the "last of a group"
      ~ this makes for a cleaner 2nd pass of the dataSet in the Mem Table
    }
  end;

  Mem.SortOn('group_break_sort;' + temp, mem.SortOptions);
  if bkill then sb.Panels[2].Text := 'KILLED, incomplete dataset and sorting complete.'
  else sb.Panels[2].Text := 'sorting complete.';
  application.ProcessMessages;
end;

procedure TfrmMainMkt.fillptypebox();
var MyQuery: TFDQuery;
  theSQL, temp: string;
begin
  if not conn.Connected then
  begin
    showmessage('please connect to a database.');
    exit;
  end;

  theSQL := 'select distinct pcr_product_type from g_prod_code_ref order by 1';
  temp := 'ALL';
  if cbProductType.Text > '!' then temp := cbProductType.Text;

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.open;
    except on e: exception do
      begin
        jSaveFile(LOG_PATH, 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed to fill type box --> ' + e.message);

        showmessage('failed to fill type box --> ' + e.message);
        exit;
      end;
    end;

    MyQuery.First;
    cbProductType.Clear;
    cbProductType.Items.add('ALL');

    while not (MyQuery.Eof) do
    begin
      cbProductType.Items.add(MyQuery.Fields[0].AsString);
      MyQuery.Next;
    end;
    cbProductType.Text := temp;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

procedure TfrmMainMkt.fillSubTypeBox();
var MyQuery: TFDQuery;
  theSQL, temp: string;
begin
  if not conn.Connected then
  begin
    showmessage('please connect to a database.');
    exit;
  end;

  if cbProductType.Text = 'ALL' then
    theSQL := 'select distinct pcr_product_subtype from g_prod_code_ref order by 1'
  else
    theSQL := 'select distinct pcr_product_subtype from g_prod_code_ref where pcr_product_type = ''' +
      cbProductType.Text + ''' order by 1';

  temp := 'ALL';

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.open;
    except on e: exception do
      begin
        jSaveFile(LOG_PATH, 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed to fill type box --> ' + e.message);
        showmessage('failed to fill sub-type box --> ' + e.message);
        exit;
      end;
    end;

    MyQuery.First;
    cbProductSUBType.Clear;
    cbProductSUBType.Items.add('ALL');

    while not (MyQuery.Eof) do
    begin
      cbProductSUBType.Items.add(MyQuery.Fields[0].AsString);
      MyQuery.Next;
    end;
    cbProductSUBType.Text := temp;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

procedure TfrmMainMkt.fillSourceBox();
var MyQuery: TFDQuery;
  theSQL, temp: string;
begin
  if not conn.Connected then
  begin
    try connect1Click(nil);
    except;
    end;
  end;

  if not conn.Connected then
  begin
    showmessage('please connect to a database.');
    exit;
  end;

  theSQL := 'select distinct pcr_source_market from g_prod_code_ref order by 1';
  temp := 'ALL';
  if CboxUseSource.Text > '!' then temp := CboxUseSource.Text;

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.open;
    except on e: exception do
      begin
        jSaveFile(LOG_PATH, 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed to fill type box --> ' + e.message);
        showmessage('failed to fill source box --> ' + e.message);
        exit;
      end;
    end;

    MyQuery.First;
    CboxUseSource.Clear;
    CboxUseSource.Items.add('ALL');
    while not (MyQuery.Eof) do
    begin
      CboxUseSource.Items.add(MyQuery.Fields[0].AsString);
      MyQuery.Next;
    end;
    CboxUseSource.Text := temp;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

procedure TfrmMainMkt.FindFieldPositions(dset: TkbmMemTable; Marcello: boolean = false);
begin
  tfmem_basesys := nil;
  tfmem_voucher := nil;
  tfmem_confirmation := nil;
  tfmem_date_created := nil;
  tfmem_date_of_svc := nil;
  tfmem_time_of_svc := nil;
  tfmem_retail_total := nil;
  tfmem_opdue_curr := nil;
  tfmem_curr_type := nil;
  tfmem_curr_exch_rate := nil;
  tfmem_sipp := nil;
  tfmem_oper_car_code := nil;
  tfmem_date_of_transaction := nil;
  tfmem_commission := nil;
  tfmem_profit := nil;
  tfmem_duration := nil;
  tfmem_lead_days_create_pu := nil;
  tfmem_dtyear := nil;
  tfmem_lead_days_pdpu := nil;
  tfmem_iata := nil;
  tfmem_country_pu := nil;
  tfmem_city_pu := nil;
  tfmem_loc_pu := nil;
  tfmem_operator := nil;
  tfmem_consortium := nil;
  tfmem_cc_name := nil;
  tfmem_vcspecrequest := nil;
  tfmem_service_type := nil;
  tfmem_source := nil;
  tfmem_c_fullname := nil;
  tfmem_c_prefix := nil;
  tfmem_c_addr1 := nil;
  tfmem_c_addr2 := nil;
  tfmem_c_prov := nil;
  tfmem_c_city := nil;
  tfmem_c_postal := nil;
  tfmem_c_country := nil;
  tfmem_c_email := nil;
  tfmem_c_phone := nil;
  tfmem_c_age := nil;
  tfmem_group_break_sort := nil;
  tfmem_vcount := nil;
  tfmem_active_history := nil;
  tfmem_p_or_q := nil;
  tfmem_payment_type := nil;
  tfmem_fp_pp := nil;
  tfmem_plus_basic := nil;
  tfmem_rid := nil;
  tfmem_auth_code := nil;
  tfmem_product_type := nil;
  tfmem_business_source := nil;
  tfmem_do_ctry := nil;
  tfmem_do_city := nil;
  tfmem_do_loc := nil;
  tfmem_updated_dt := nil; // vcstdte1
  tfmem_do_dt := nil;
  tfmem_do_time := nil;
  tfmem_op_rate_code := nil;
  tfmem_bus_account_number := nil;
  tfmem_sipp_description := nil;
  tfmem_num_passengers := nil;
  tfmem_comm_due := nil;
  tfmem_comm_percent := nil;
  tfmem_home_curr := nil; // system curr := nil; not operator
  tfmem_vat_tax_rt := nil;
  tfmem_home_ctry := nil;
  tfmem_base_rate_retail := nil;
  tfmem_discount_retail := nil;
  tfmem_insurance_tot_retail := nil;
  tfmem_tax_tot_retail := nil;
  tfmem_pu_fee_retail := nil;
  tfmem_do_fee_retail := nil;
  tfmem_xtra_fee_retail := nil;
  tfmem_base_rate_wholesale := nil;
  tfmem_insurance_tot_wholesale := nil;
  tfmem_tax_tot_wholesale := nil;
  tfmem_pu_fee_wholesale := nil;
  tfmem_do_fee_wholesale := nil;
  tfmem_wholesale_total := nil;
  tfmem_operator_due := nil;
  tfmem_deposit_amt := nil;
  tfmem_waived_amt := nil;
  tfmem_deposit_after_waive := nil;
  tfmem_due_at_pu := nil;
  tfmem_deferred_amt := nil;
  tfmem_gross_revenue := nil;
  tfmem_operator_cost := nil;
  tfmem_count_new := nil;
  tfmem_count_cxl := nil;
  tfmem_count_change := nil;
  tfmem_direct_or_ta := nil;
  tfmem_member_account := nil;
  tfmem_gv_name := nil;
  tfmem_gv_legal_name := nil;
  tfmem_gv_contact_name := nil;
  tfmem_gv_addr1 := nil;
  tfmem_gv_addr2 := nil;
  tfmem_gv_addr3 := nil;
  tfmem_gv_addr4 := nil;
  tfmem_gv_city := nil;
  tfmem_gv_prov := nil;
  tfmem_gv_postal := nil;
  tfmem_gv_country := nil;
  tfmem_gv_phone_prefix := nil;
  tfmem_gv_phone_num := nil;
  tfmem_gv_fax_prefix := nil;
  tfmem_gv_fax_num := nil;
  tfmem_gv_email := nil;
  tfmem_promo1 := nil;
  tfmem_promo2 := nil;
  tfmem_promo3 := nil;

  //06/18/2012 nil marcello field defs
  if Marcello then
  begin
    tfmem_vs_retail_total := nil;
    tfmem_vs_profit := nil;
    tfmem_vs_count_voucher := nil;
    tfmem_vs_paid_dt := nil;
    //marcello webfields wbadcampaigns wcamps
    tfmem_wcamps_campid := nil;
    tfmem_wcamps_campaign := nil;
    //marcello webfields wbadhosts wcamps
    tfmem_whosts_host := nil;
    tfmem_gc_ctry := nil;
  end;

  if not assigned(dset) then
  begin
    raise(exception.Create('FindFieldPositions dataset nil'));
    exit;
  end;

  tfmem_basesys := dset.findfield('basesys');
  tfmem_voucher := dset.findfield('voucher');
  tfmem_confirmation := dset.findfield('confirmation');
  tfmem_date_created := dset.findfield('date_created');
  tfmem_date_of_svc := dset.findfield('date_of_svc');
  tfmem_time_of_svc := dset.findfield('time_of_svc');
  tfmem_retail_total := dset.findfield('retail_total');
  tfmem_opdue_curr := dset.findfield('opdue_curr');
  tfmem_curr_type := dset.findfield('curr_type');
  tfmem_curr_exch_rate := dset.findfield('curr_exch_rate');
  tfmem_sipp := dset.findfield('sipp');
  tfmem_oper_car_code := dset.findfield('oper_car_code');
  tfmem_date_of_transaction := dset.findfield('date_of_transaction');
  tfmem_commission := dset.findfield('commission');
  tfmem_profit := dset.findfield('profit');
  tfmem_duration := dset.findfield('duration');
  tfmem_lead_days_create_pu := dset.findfield('lead_days_create_pu');
  tfmem_dtyear := dset.FindField('dt_year'); // add 04/27/2011
  tfmem_lead_days_pdpu := dset.FindField('lead_days_pdpu'); // calulated in sql
  tfmem_iata := dset.findfield('iata');
  tfmem_country_pu := dset.findfield('country_pu');
  tfmem_city_pu := dset.findfield('city_pu');
  tfmem_loc_pu := dset.findfield('loc_pu');
  tfmem_operator := dset.findfield('operator');
  tfmem_consortium := dset.findfield('consortium');
  tfmem_cc_name := dset.findfield('cc_name');
  tfmem_vcspecrequest := dset.findfield('vcspecrequest');
  tfmem_service_type := dset.findfield('service_type');
  tfmem_source := dset.findfield('source');
  tfmem_c_fullname := dset.FindField('c_fullname');
  tfmem_c_prefix := dset.findfield('c_prefix');
  tfmem_c_addr1 := dset.findfield('c_addr1');
  tfmem_c_addr2 := dset.findfield('c_addr2');
  tfmem_c_prov := dset.findfield('c_prov');
  tfmem_c_city := dset.findfield('c_city');
  tfmem_c_postal := dset.findfield('c_postal');
  tfmem_c_country := dset.findfield('c_country');
  tfmem_c_email := dset.findfield('c_email');
  tfmem_c_phone := dset.findfield('c_phone');
  tfmem_c_age := dset.findfield('c_age');
  tfmem_group_break_sort := dset.findfield('group_break_sort');
  tfmem_vcount := dset.findfield('vcount');
  tfmem_active_history := dset.findfield('active_history');
  tfmem_p_or_q := dset.findfield('p_or_q');
  tfmem_payment_type := dset.findfield('payment_type');
  tfmem_fp_pp := dset.findfield('fp_pp');
  tfmem_plus_basic := dset.findfield('plus_basic');
  tfmem_rid := dset.findfield('rid');
  tfmem_auth_code := dset.findfield('auth_code');
  tfmem_product_type := dset.findfield('product_type');
  tfmem_business_source := dset.findfield('business_source');
  tfmem_do_ctry := dset.findfield('do_ctry');
  tfmem_do_city := dset.findfield('do_city');
  tfmem_do_loc := dset.findfield('do_loc');
  tfmem_updated_dt := dset.findfield('updated_dt'); // vcstdte1
  tfmem_do_dt := dset.findfield('do_dt');
  tfmem_do_time := dset.findfield('do_time');
  tfmem_op_rate_code := dset.findfield('op_rate_code');
  tfmem_bus_account_number := dset.findfield('bus_account_number');
  tfmem_car_name:= dset.findfield('car_name');
  tfmem_sipp_description := dset.findfield('sipp_description');
  tfmem_num_passengers := dset.findfield('num_passengers');
  tfmem_comm_due := dset.findfield('comm_due');
  tfmem_comm_percent := dset.findfield('comm_percent');
  tfmem_home_curr := dset.findfield('home_curr'); // system curr, not operator
  tfmem_vat_tax_rt := dset.findfield('vat_tax_rt');
  tfmem_home_ctry := dset.findfield('home_ctry');
  tfmem_base_rate_retail := dset.findfield('base_rate_retail');
  tfmem_discount_retail := dset.findfield('discount_retail');
  tfmem_insurance_tot_retail := dset.findfield('insurance_tot_retail');
  tfmem_tax_tot_retail := dset.findfield('tax_tot_retail');
  tfmem_pu_fee_retail := dset.findfield('pu_fee_retail');
  tfmem_do_fee_retail := dset.findfield('do_fee_retail');
  tfmem_xtra_fee_retail := dset.findfield('xtra_fee_retail');
  tfmem_base_rate_wholesale := dset.findfield('base_rate_wholesale');
  tfmem_insurance_tot_wholesale := dset.findfield('insurance_tot_wholesale');
  tfmem_tax_tot_wholesale := dset.findfield('tax_tot_wholesale');
  tfmem_pu_fee_wholesale := dset.findfield('pu_fee_wholesale');
  tfmem_do_fee_wholesale := dset.findfield('do_fee_wholesale');
  tfmem_wholesale_total := dset.findfield('wholesale_total');
  tfmem_operator_due := dset.findfield('operator_due');
  tfmem_deposit_amt := dset.findfield('deposit_amt');
  tfmem_waived_amt := dset.findfield('waived_amt');
  tfmem_deposit_after_waive := dset.findfield('deposit_after_waive');
  tfmem_due_at_pu := dset.findfield('due_at_pu');
  tfmem_deferred_amt := dset.findfield('deferred_amt');
  tfmem_gross_revenue := dset.findfield('gross_revenue');
  tfmem_operator_cost := dset.findfield('operator_cost');
  tfmem_count_new := dset.findfield('count_new');
  tfmem_count_cxl := dset.findfield('count_cxl');
  tfmem_count_change := dset.findfield('count_change');
  tfmem_direct_or_ta := dset.findfield('direct_or_ta');
  tfmem_member_account := dset.findfield('member_account');
  // ta info:
  tfmem_gv_name := dset.findfield('gv_name');
  tfmem_gv_legal_name := dset.findfield('gv_legal_name');
  tfmem_gv_contact_name := dset.findfield('gv_contact_name');
  tfmem_gv_addr1 := dset.findfield('gv_addr1');
  tfmem_gv_addr2 := dset.findfield('gv_addr2');
  tfmem_gv_addr3 := dset.findfield('gv_addr3');
  tfmem_gv_addr4 := dset.findfield('gv_addr4');
  tfmem_gv_city := dset.findfield('gv_city');
  tfmem_gv_prov := dset.findfield('gv_prov');
  tfmem_gv_postal := dset.findfield('gv_postal');
  tfmem_gv_country := dset.findfield('gv_country');
  tfmem_gv_phone_prefix := dset.findfield('gv_phone_prefix');
  tfmem_gv_phone_num := dset.findfield('gv_phone_num');
  tfmem_gv_fax_prefix := dset.findfield('gv_fax_prefix');
  tfmem_gv_fax_num := dset.findfield('gv_fax_num');
  tfmem_gv_email := dset.findfield('gv_email');

  // 06/23/2010
  tfmem_promo1 := dset.findfield('promo1');
  tfmem_promo2 := dset.findfield('promo2');
  tfmem_promo3 := dset.findfield('promo3');

  //06/18/2012 populate marcello field defs
  if Marcello then
  begin
    tfmem_vs_retail_total := dset.findfield('vs_retail_total');
    tfmem_vs_profit := dset.findfield('vs_profit profit');
    tfmem_vs_count_voucher := dset.findfield('vs_count_voucher');
    tfmem_vs_paid_dt := dset.findfield('vs_paid_dt,');
    //marcello webfields wbadcampaigns wcamps
    tfmem_wcamps_campid := dset.findfield('campid,');
    tfmem_wcamps_campaign := dset.findfield('campaign,');
    //marcello webfields wbadhosts wcamps
    tfmem_whosts_host := dset.findfield('host,');
    tfmem_gc_ctry := dset.findfield('gc_ctry,');
  end;

end;

function TfrmMainMkt.getOffset(): string;
var temp, temp2: string;
begin
  result := '+1';
  temp := '';
  temp2 := '';
  temp := copy(trim(editDateOffset.text), 1, 1); // should be + or -
  temp2 := copy(trim(editDateOffset.text), 2, 10);

  if not ((temp = '+') or (temp = '-')) then
  begin
    showmessage('1st character of date offset should be "+" or "-".');
    editDateOffset.text := '+1';
    exit;
  end;

  try
    strtoint(temp2);
  except on e: exception do
    begin
      showmessage('2nd thru last character of date offset should be an integer. --> ' + #10#13 +
        e.Message);
      editDateOffset.text := '+1';
      exit;
    end;
  end;
  result := trim(editDateOffset.text);
end;

function TfrmMainMkt.BuildSQL(Sender: TObject; flag: string = ''): string;

  function GetPTSQLSTR(): string;
  var x: Integer;
    sql: string;
    beerTime: boolean;
  begin
    result := '';
    beerTime := false;

    if cbUseSummaryTable.Checked then
      sql := ' and vs_payment_type in('
    else
      sql := ' and vccrcardid in(';

    for x := 0 to CLBPayType2.Items.Count - 1 do
    begin
      if CLBPayType2.Checked[x] then
      begin
        beerTime := true;
        case x of
          0: {AMEX} sql := sql + '0,';
          1: {VISA} sql := sql +  '1,';
          2: {MC} sql := sql + '2,';
          3: {Discover} sql := sql + '3,';
          4: {Diners} sql := sql + '4,';
          5: {cc bu} sql :=  sql + '12,';
          6: {DP} sql := sql + '8,';
          7: {Check} sql := sql + '9,';
          8: {Waive Deposit} sql := sql + '10,';
          9: {waive total} sql := sql + '11,';
          10: {Other} sql := sql +  '5,';
          11: {Switch} sql := sql + '6,';
          12: {TIAS} sql := sql + '21,';
          13: {TT} sql :=  sql + '22,';
          14: {ExcVch} sql := sql + '23,';
          15: {MCO} sql := sql + '24,';
          16: {EFT Bal DUe} sql :=  sql + '25,';
          17: {EFT Bal DUe} sql := sql + '20,';
        else ;
        end;
      end; //case
    end; // for

    if beerTime then
    begin
      sql := copy(sql, 1, length(sql) - 1);
      sql := sql + ')';
    end
    else sql := '';
    result := sql;
  end;

  function GetSIPPInStatement(sender: TCheckListBox; pos: Integer): string;
  var x: Integer;
    sql: string;
    DoItUp: boolean;
  begin
    result := '';
    DoItUp := false;

    if cbUseSummaryTable.Checked then
      sql := ' and vs_sipp[' + IntToStr(pos) + '] in('
    else
      sql := ' and voprd1[' + IntToStr(pos) + '] in(';

    for x := 0 to sender.Items.Count - 1 do
    begin
      if sender.Checked[x] then
      begin
        DoItUp := true;
        sql := sql + '''' + sender.items[x] + ''',';
      end;
    end;

    if DoItUp then
    begin
      sql := copy(sql, 1, length(sql) - 1);
      sql := sql + ')';
    end
    else sql := '';
    result := sql;
  end;

  function GetGenderArg(): string;
  begin // "gender" switch not needed per IK - Use Salutation (stored in surname) instead 5/15/2007
    result := ' and ( (length(gc_surname) > 0 and gc_surname is not null) ';

    case rgGender2.ItemIndex of
      0: begin result := result + ' and gc_surname in("MR.") ) '; end; // male
      1: begin result := result + ' and gc_surname in("MRS.", "MS.") ) '; end; // female
    else result := '';
    end;
  end;

  // revised 5/5/2010 - also changed [ '%' + temp + '%' ] to [ + temp +] below
  // right below each call to GetLike()
  function GetLike(InStr: string): string;
  var temp: string;
    i, x: Integer;
  begin
    result := '';
    temp := trim(instr);
    i := 0;
    x := 0;
    i := pos('%', temp);
    x := pos('_', temp);

    if (i > 0) or (x > 0) then
    begin
      if temp = '' then result := ''
      else result := temp;
    end;
  end;

var
  MainSQL, temp: string;
  iLIKE: integer;
  sl: TStringlist;

begin // BuildSQL()
  result := '';

  temp := GetDB_InStatement();
  if trim(temp) = '' then
  begin
    raise(exception.Create('You must select database(s).'));
    exit;
  end;
  temp := '';

  if (dtpend.date < dtpStart.Date) then // check vchpqbath / create date arguments
  begin
    PageControl1.ActivePage := TabStep1;
    raise(exception.Create('"To" date prior to "From" date.'));
    exit;
  end
  else if trim(EditCountry.text) = '' then
  begin
    FocusCtry();
    raise(exception.Create('Invalid country'));
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    FocusCity();
    raise(exception.Create('Invalid city.'));
    exit;
  end
  else if trim(EditOperator.Text) = '' then
  begin
    FocusOperator();
    raise(exception.Create('Invalid operator.'));
    exit;
  end;

  if (cbUsePUDate.Checked) and // check PU Date arguments
    (dtpPUEnd.date < dtpPUStart.Date) then
  begin
    PageControl1.ActivePage := TabStep2;
    raise(exception.Create('"Pick Up End" prior to "Pick Up Start".'));
    exit;
  end;

  if (cbUseGender.checked) and (rgGender2.ItemIndex < 0) then
  begin
    PageControl1.ActivePage := TabStep2;
    raise(exception.Create('Please select "Gender".'));
    exit;
  end;

  if (cbUseAge.Checked) then
  begin
    try
      StrToInt(EditAgeStart.Text);
      StrToInt(EditAgeEnd.Text);
    except on e: exception do
      begin
        PageControl1.ActivePage := TabStep2;
        raise(exception.Create('Age error --> ' + e.message));
        exit;
      end;
    end;
  end;

  if (cbUseLastMin.Checked) then
  begin
    try
      StrToInt(EditDaysBetween.Text);
    except on e: exception do
      begin
        PageControl1.ActivePage := TabStep2;
        raise(exception.Create('Days Between error --> ' + e.message));
        exit;
      end;
    end;
  end;

  if (cbUseDuration.Checked) then
  begin
    try
      StrToInt(EditDurHigh.Text);
      StrToInt(EditDurLow.Text);
    except on e: exception do
      begin
        PageControl1.ActivePage := TabStep2;
        raise(exception.Create('Duration error --> ' + e.message));
        exit;
      end;
    end;
  end;

  if (cbSIPPWildCard.checked) and (trim(EditSippWildcard.Text) = '') then // add sept 27, 2010
  begin
    PageControl1.ActivePage := TabStep2;
    raise(exception.Create('SIPP WILDCARD error --> If using wipp wildcard, a value is required.'));
    exit;
  end;

  // add release number for Brett 2/07/201
  if (cbReleaseNum.Checked) then
  begin
    try
      strtoint(EditReleaseNum.Text);
    except on e: Exception do
      begin
        PageControl1.ActivePage := TabStep2b;
        raise(exception.Create('release number must not be preceded by a "0" or is invalid --> ' + e.Message));
        exit;
      end;
    end;
  end;

  if (cbUseIataList.Checked) then
  begin
    if trim(memoIataList.Text) = '' then
    begin
      PageControl1.ActivePage := TabStep2c;
      raise(exception.Create('Please enter an Iata or a list of iatas if "use iata list" is selected.'));
      exit;
    end
    else if (memoIATAlist.Lines.Count > 100) then
    begin
      PageControl1.ActivePage := TabStep2c;
      raise(exception.Create('Maximum number of Iatas allowed is 100.'));
      exit;
    end;
  end;

  // client filter(s) added 07/19/2011
  if cbUseClientCtry.Checked then
  begin
    if trim(editCLientCountry.Text) = '' then
    begin
      PageControl1.ActivePage := TabStep2c;
      raise(exception.Create('Please enter a client country code if "use client country" is selected.'));
      exit;
    end;
  end;

  // main dataset args:
  if (Sender = btnCombined2) then
  begin
    //combined report: group sums by year, system, operator, country
    MainSQL := 'Select ';

     // change to "getOffset" so when laura selects 12/31/1999, the "paid dt will be year 2000
     // helps with grouping later
     // allow laura to set offset via editDateOffset on "special" Tab

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      MainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      MainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      MainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') dt_year, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    MainSQL := MainSQL +
      'trim(vs_basesys) system, ' +
      'DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
      'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country, ';

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      mainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') || "-" || trim(vs_basesys) year_system_break, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      mainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') || "-" || trim(vs_basesys) year_system_break, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      mainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') || "-" || trim(vs_basesys) year_system_break, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    mainSQL := MainSQL +
      ' sum(vs_retail_total) retail_total, ' +
      'sum(vs_operator_due) operator_due,  ' +
      'sum(vs_profit) profit, ' +
      'sum(vs_count_new) new_voucher_tot,  ' +
      'sum(vs_count_cxl) cxl_voucher_tot, ' +
      'sum(vs_count_change) change_voucher_tot,  ';

    if rgCountMethod.ItemIndex = 0 then // regular count
      MainSQL := MainSQL + 'sum(vs_count_voucher) voucher_tot  '
    else
      MainSQL := MainSQL + 'sum(vs_count_logical) voucher_tot  ';

  end // funny name, but actually means "sipp code analysis"
  else if (sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP) then
  begin
    MainSQL := 'Select ';

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      mainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      mainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      mainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') dt_year, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    mainSQL := MainSQL + // may have to change here
      ' trim(vs_basesys) system, ' +
      'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country_pu, ' +
      'DECODE(trim(vs_operator_code), "", "NULL", NULL, "NULL", trim(vs_operator_code)) operator, ' +
      'DECODE(trim(vs_business_source),"", "NULL", NULL, "NULL",trim(vs_business_source)) biz_source, ' +
      'DECODE(trim(vs_plus_basic),"", "NULL", NULL, "NULL",trim(vs_plus_basic)) plus_basic, ';

    if (sender = btnSippAnalysisSIPP) then
      MainSQL := MainSQL + 'DECODE(trim(vs_sipp),"", "NULL", NULL, "NULL",trim(vs_sipp)) sipp, ';

    if rgCountMethod.ItemIndex = 0 then // regular count
      MainSQL := MainSQL + 'sum(vs_count_voucher) bookings,  '
    else
      MainSQL := MainSQL + 'sum(vs_count_logical) bookings,  ';


    { 06/20/2011 - change "vs_retail_total" to (vs_base_rate_retail + vs_tax_tot_retail) in all cases except the
       "profit_Margin" calculation
      reason is that brett assumed vs_retail_total was *before* discount was removed - this is not the case
    }

    MainSQL := MainSQL +
      //'sum(vs_retail_total) retail_total, ' +  // changed 6/20/2011
      //'sum(vs_base_rate_retail + vs_tax_tot_retail) retail_total, ' + changes 5/15/2012
    'sum(vs_base_rate_retail + vs_tax_tot_retail + vs_insurance_tot_retail) retail_total, ' +

    'sum(vs_discount_retail) discount_total, ' +
     //https://autoeurope.jira.com/browse/DSS-2
    'sum(vs_operator_due_curr) wholesale_currency, ' +
     //https://autoeurope.jira.com/browse/DSS-3
    'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_operator_due_curr)  / sum(vs_count_voucher))::decimal (12,2) ave_whl_currency, ' +

    //https://autoeurope.jira.com/browse/DSS-4
    'DECODE(sum(vs_duration), 0, 0.00, sum(vs_operator_due_curr)  / sum(vs_duration))::decimal (12,2) whl_currency_daily, ' +
      // 'sum(vs_retail_total) - sum(vs_discount_retail) adjusted_retail_total, ' +   // changed 6/20/2011
    //'sum(vs_base_rate_retail + vs_tax_tot_retail) - sum(vs_discount_retail) adjusted_retail_total, ' + changes 5/15/2012
    'sum(vs_base_rate_retail + vs_tax_tot_retail + vs_insurance_tot_retail) - sum(vs_discount_retail) adjusted_retail_total, ' +
      'sum(vs_wholesale_total) wholesale_total, ' +
      'sum(vs_comm_tot) commission_total, ' +
      'sum(vs_deposit_amt) deposit_total, ' +
      'sum(vs_duration) duration_total, ' +
      'sum(vs_profit) profit_total, ' +

    //https://autoeurope.jira.com/browse/DSS-5
    'DECODE(sum(vs_operator_due_curr), 0, 0.00, sum(vs_wholesale_total)  / sum(vs_operator_due_curr))::decimal (12,2) ave_exchange, ' +


   //   'DECODE(sum(vs_retail_total), 0, 0.00, (( sum(vs_wholesale_total) / sum(vs_retail_total) -1 )) * (-1) )::decimal (12,3) as gross_margin, ' + // 6/20 change
    'DECODE(sum(vs_base_rate_retail + vs_tax_tot_retail), 0, 0.00, (( sum(vs_wholesale_total) / sum(vs_base_rate_retail + vs_tax_tot_retail) -1 )) * (-1) )::decimal (12,3) as gross_margin, ' +

    //  'DECODE( (sum(vs_retail_total) - sum(vs_discount_retail)), 0, 0.00, (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) )::decimal (12,3) as net_margin, ' +  // 6/20 change
    'DECODE( (sum(vs_base_rate_retail + vs_tax_tot_retail) - sum(vs_discount_retail)), 0, 0.00, (( sum(vs_wholesale_total) / (sum(vs_base_rate_retail + vs_tax_tot_retail) - sum(vs_discount_Retail)) -1 )) * (-1) )::decimal (12,3) as net_margin, ' +

    'DECODE(sum(vs_retail_total), 0, 0.00,  sum(vs_profit) / sum(vs_retail_total))::decimal (12,3) as profit_margin, '; // left for now 6/20

    if rgCountMethod.ItemIndex = 0 then // regular count
    begin
      MainSQL := MainSQL +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_duration) / sum(vs_count_voucher))::decimal (12,2) ave_duration, ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_profit)  /  sum(vs_count_voucher))::decimal (12,2) ave_profit, ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_wholesale_total)  / sum(vs_count_voucher))::decimal (12,2) ave_wholesale, ';
    end
    else
    begin
      MainSQL := MainSQL +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_duration) / sum(vs_count_logical))::decimal (12,2) ave_duration, ' +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_profit)  /  sum(vs_count_logical))::decimal (12,2) ave_profit, ' +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_wholesale_total)  / sum(vs_count_logical))::decimal (12,2) ave_wholesale, ';
    end;

    MainSQL := MainSQL +
     // 'DECODE(sum(vs_duration), 0, 0.00, sum(vs_retail_total)  / sum(vs_duration))::decimal (12,2) retail_daily, ' +  // 6/20 changed
    'DECODE(sum(vs_duration), 0, 0.00, sum(vs_base_rate_retail + vs_tax_tot_retail)  / sum(vs_duration))::decimal (12,2) retail_daily, ' +

    'DECODE(sum(vs_duration), 0, 0.00, sum(vs_discount_retail)  / sum(vs_duration))::decimal (12,2) discount_daily, ' +
    //  'DECODE(sum(vs_duration), 0, 0.00, (sum(vs_retail_total) - sum(vs_discount_retail)) / sum(vs_duration))::decimal (12,2) adjusted_retail_daily, ' + // 6/20 changed
    'DECODE(sum(vs_duration), 0, 0.00, (sum(vs_base_rate_retail + vs_tax_tot_retail) - sum(vs_discount_retail)) / sum(vs_duration))::decimal (12,2) adjusted_retail_daily, ' +

    'DECODE(sum(vs_duration), 0, 0.00, sum(vs_wholesale_total)  / sum(vs_duration))::decimal (12,2) wholesale_daily, ' +
      'DECODE(sum(vs_duration), 0, 0.00, sum(vs_comm_tot)  / sum(vs_duration))::decimal (12,2) commission_daily, ' +
      'DECODE(sum(vs_duration), 0, 0.00, sum(vs_profit)  / sum(vs_duration))::decimal (12,2) profit_daily ';
  end
  else if (sender = btnRPBUS) or (sender = btnRpBusNew) then
  begin
    MainSQL := ' Select vs_basesys system, ';

    if rgCountMethod.ItemIndex = 0 then // regular count
      MainSQL := MainSQL + 'sum(vs_count_voucher) orig_bookings, ' // yields a lower original number - better % in the end - this is the method use all along
    else
      MainSQL := MainSQL + 'sum(vs_count_logical) orig_bookings, ';

    MainSQL := MainSQL + // this was the way the "repeats" were counted at the end (yields higher % = not as accurate, but inflated)
      '0.0 repeat_bookings, ' +
      '0.0 repeat_percent ';
  end
  else if (sender = rpbus_get_names) then
  begin // FLAG is on for the second pass for repeat-biz report to create name list ....
    if flag = 'DC' then
      MainSQL := 'select trim(upper(gc_email)) name  ' // use "name" even thoug it's e-mail addr
    else if flag = 'TA' then // is ta args

      MainSQL := 'select trim(upper(gc_iata)) || trim(upper(gc_lastname)) || trim(upper(gc_firstname)) name '
    else // old method obsolete
      MainSQL := ' Select trim(upper(gc_lastname)) || ", " ||  trim(upper(gc_firstname)) name ';
  end
  else if (sender = btnytd) then
  begin
    MainSQL := 'Select ';

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      mainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      mainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      mainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') dt_year, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    mainSQL := MainSQL +
      ' trim(vs_basesys) system, ' +
      'DECODE(trim(vs_operator_code), "", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
      'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country, ';

    if rgCountMethod.ItemIndex = 0 then // regular count 3/20/2009
    begin
      MainSQL := MainSQL + 'sum(vs_count_voucher) bookings,  ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_duration)  / sum(vs_count_voucher))::decimal (12,2) ave_duration, ' +
       // ave profit
      'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_profit)  / sum(vs_count_voucher))::decimal (12,2) ave_profit, ' +
       // total days:
      'sum(vs_duration) total_days, ' +
       // ave op due
      'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_operator_due)  / sum(vs_count_voucher))::decimal (12,2) ave_op_due, ' +
        'sum(vs_profit) total_profit, sum(vs_operator_due) total_op_due, sum(vs_duration) total_duration, sum(vs_deposit_amt) deposit ';
    end
    else
    begin
      MainSQL := MainSQL + 'sum(vs_count_logical) bookings,  ' +

       // DECODE(): For the divisor, test for 0, if so then return 0.00
       // else it's safe to do the calculation as the divisor will be not be equal to 0

       //ave duration                                                                 // 2 xtra colons for cast in delphi !!
      'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_duration)  / sum(vs_count_logical))::decimal (12,2) ave_duration, ' +
       // ave profit
      'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_profit)  / sum(vs_count_logical))::decimal (12,2) ave_profit, ' +
       // total days:
      'sum(vs_duration) total_days, ' +
       // ave op due
      'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_operator_due)  / sum(vs_count_logical))::decimal (12,2) ave_op_due, ' +
        'sum(vs_profit) total_profit, sum(vs_operator_due) total_op_due, sum(vs_duration) total_duration, sum(vs_deposit_amt) deposit ';
    end
  end
  else if (sender = btnFleetMix) then
  begin
    // combined report: group sums by year, system, operator, country

    MainSQL := 'Select ';

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      mainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      mainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      mainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') dt_year, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    mainSQL := MainSQL +
      ' trim(vs_basesys) system, ' +
      'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country, ' +
      'DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
      'DECODE(trim(vs_sipp),"", "NULL", NULL, "NULL",trim(vs_sipp)) sipp, ';

    if rgCountMethod.ItemIndex = 0 then // regular count 3/20/2009
    begin
      MainSQL := MainSQL +
        'sum(vs_retail_total) retail_total, ' +
        'sum(vs_operator_due) op_due,  ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_operator_due)  / sum(vs_count_voucher))::decimal (12,2) ave_op_due, ' +
        'sum(vs_profit) profit, ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_duration)  / sum(vs_count_voucher))::decimal (12,2) ave_duration, ' +
        'DECODE(sum(vs_count_voucher), 0, 0.00, sum(vs_profit)  / sum(vs_count_voucher))::decimal (12,2) ave_profit, ' +
        'sum(vs_count_voucher) voucher_tot,  ' +
        'sum( case when ( vs_plus_basic = "P" ) then vs_count_voucher else 0 end ) as plus_count, ' +
        'sum( case when ( vs_plus_basic = "B" ) then vs_count_voucher else 0 end ) as basic_count ';
    end
    else
    begin
      MainSQL := MainSQL +
        'sum(vs_retail_total) retail_total, ' +
        'sum(vs_operator_due) op_due,  ' +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_operator_due)  / sum(vs_count_logical))::decimal (12,2) ave_op_due, ' +
        'sum(vs_profit) profit, ' +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_duration)  / sum(vs_count_logical))::decimal (12,2) ave_duration, ' +
        'DECODE(sum(vs_count_logical), 0, 0.00, sum(vs_profit)  / sum(vs_count_logical))::decimal (12,2) ave_profit, ' +
        'sum(vs_count_logical) voucher_tot,  ' +
        'sum( case when ( vs_plus_basic = "P" ) then vs_count_logical else 0 end ) as plus_count, ' +
        'sum( case when ( vs_plus_basic = "B" ) then vs_count_logical else 0 end ) as basic_count ';
    end;
  end
  else if cbUseSummaryTable.Checked then
  begin // default report. Also, cbUseSummaryTable is invisible and always checked
        // 4/27/2011 add 'year' to regular reports

    MainSQL := 'select ';

    if (rgStep1PayType.itemindex = 0) then
    begin // paid date
      MainSQL := MainSQL + 'year(vs_paid_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 1) then
    begin // create date
      MainSQL := MainSQL + 'year(vs_voucher_create_dt' + getOffset + ') dt_year, ';
    end
    else if (rgStep1PayType.itemindex = 2) then
    begin // pu date
      MainSQL := MainSQL + 'year(vs_pu_dt' + getOffset + ') dt_year, ';
    end
    else
    begin
      raise(exception.create('invalid date type - build sql'));
      exit;
    end;

    mainSQL := MainSQL + ' (vs_pu_dt - date(vs_paid_dt)) lead_days_pdpu, * ';
  end
  else
  begin // should never happen - cb is always checked
    MainSQL :=
  // voucher:
    'SELECT basesys, voconsort, cc_name, VCNUMB,VCHREL,VORD4,VOPAMT4,VCCASHDEPOSI,VOPSUBT, ' +
      'VOPDATE1, VOPCURRENCY, ALTRESNUM1, VCIATA, VOPCTRY1, VOPCITY1, VOPID, ' +
      'A.homectry, vcntype, voptype, vcstdte2, vchpqbath, vccrcardid, vccrcard, ' +
      'vcccmonth, vccconfirm, vcclientid, vcvendorattn, vccomments, vccomments1, vcarrvair, ' +
      'vcarrvflight, vceta, vcprepay, vcccauthcode, vchdate, vcrid, vcvpromo1, vcvpromo2, ' +
      'vcvpromo3, vcst1, vcst2, vcst3, vcstdte1, vcstdte3, vopid2, vopctry2, vopprov1, vopprov2, ' +
      'vopcity2, voploc1, voploc2, voprd1, vopcurrate, vord1, vord2, vord3, vord5, vord6, ' +
      'vord7, vord8, vord9, vord10, vowd1, vowd2, vowd3, vowd4, vowd5, vopwd6, vopwd7, vopwd8, ' +
      'vopwd9,vopwd10,vopdate2,voptime1,voptime2,vopcomt1,voptax2,vopper1,vopper2, ' +
      'vopper3,voptxper1,voptxper2,vopdatecreate,vopdatemodif,vopstype1,vopamt1,vopamt2, vcspecrequest, ' +
      'vopamt3,voiamt1,voiamt2,voiamt3,voiamt4,voiamt5,voprcode,Vcflighttckt,voiamt7, carmname, vorecord, ' +
      'voit1,voit1ret,voit1whl,voit2,voit2ret,voit2whl,voit3,voit3ret,voit3whl, ' +
      'voit4ret,voit4whl,voxtp1,voxtp1w,voxtp1t,voxtp2,voxtp2w,voxtp2t,vccom1, ' +
      'vccom2,vcdisaddamount,vcconsortsub,vccomtaken,vcfpover1,vccomover1, ' +
      'A.homecurr,affiliate1,affiliate2,voit1sw,vopamt5,vopamt6, ' +
      'vopamt7,vopamt8,vopamt9,origcurr,vchpqtype,vchpqvch,vchpqvchln,vchpqseq,vchpstat, ' +
      'vchpqnsw1,vchpqnsw2,vchpqnsw3,VOPTAX1, (vopdate1-date(vchdate)) lead_days, (vopdate1-date(vchpqbath)) lead_days_pdpu,' + // changed from (vopdate1-date(vchpqbath))
    // client:
    'gc_key, gc_uid, trim(gc_lastname) gc_lastname, trim(gc_firstname) gc_firstname,  trim(gc_initial) gc_initial, trim(gc_surname) gc_surname, ' +
      'gc_addr1, gc_addr2, gc_prov, gc_city, gc_postcode, gc_ctry, gc_phone, gc_fax, gc_cell, ' +
      'gc_email, gc_password, gc_system, gc_voucher, gc_release, gc_iata, gc_batchdt, ' +
    // ta
    'gv_name char, gv_legal_name, gv_contact_name char, gv_addr1 char, gv_addr2, gv_addr3, ' +
      'gv_addr4, gv_city, gv_prov, gv_postal, gv_country, gv_phone_prefix, gv_phone_num,  ' +
      'gv_fax_prefix, gv_fax_num, gv_email ';
  end;

  if (sender = btnCombined2) or (sender = btnytd) or
    (sender = btnFleetMix) or
    (// add cbUseClientCtry.Checked 7/21/2011 - don't join client table if not checked
       // client records sometimes get out of sync - will address that problem separately via dss feed application
    ((sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP)) and (not cbUseClientCtry.Checked)
    )
    then // move down for client join 7/20/2011
  begin
    mainsql := mainsql + ' FROM g_voucher_summary WHERE ';
  end
  else if (sender = btnRPBUS) or (sender = btnrpBusNew) or (sender = rpbus_get_names)
    or // add or (sender = btnSippAnal) or (sender = btnSippAnalSIPP) 7/20/2011
    (// add cbUseClientCtry.Checked 7/21/2011 - don't join client table if not checked
     // client records sometimes get out of sync - will address that problem separately via dss feed application
    ((sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP)) and (cbUseClientCtry.Checked)
    )
    then
  begin
    mainsql := mainsql + 'From g_voucher_summary, g_client WHERE ';
  end
  else if (cbUseSummaryTable.Checked) then // join ta info :: g_vendor next
  begin
    { change order 7/19/2011 due to new client args (tab 2c)
    if rgRestrictByEmail.ItemIndex = 2 then // no restiction
      stradd(mainsql, ' FROM g_voucher_summary A, outer g_client, outer cc_ref, outer g_vendor WHERE ')
    else if rgRestrictByEmail.ItemIndex = 1 then // ta restriction - remove outer on g_vendor so the e-mail restriction arg works
      stradd(mainsql, ' FROM g_voucher_summary A, outer g_client, outer cc_ref, g_vendor WHERE ')
    else if (rgRestrictByEmail.ItemIndex = 0) OR (cbUseClientCtry.Checked) then // client remove outer on g_client so the e-mail restriction arg works
      stradd(mainsql, ' FROM g_voucher_summary A, g_client, outer cc_ref, outer g_vendor WHERE ')
    }
    if (rgRestrictByEmail.ItemIndex = 0) or (cbUseClientCtry.Checked) then // client remove outer on g_client so the e-mail restriction arg works
      mainsql := mainsql + ' FROM g_voucher_summary A, g_client, outer cc_ref, outer g_vendor WHERE '
    else if rgRestrictByEmail.ItemIndex = 1 then // ta restriction - remove outer on g_vendor so the e-mail restriction arg works
      mainsql := mainsql + ' FROM g_voucher_summary A, outer g_client, outer cc_ref, g_vendor WHERE '
                                                 // add cbUseClientCtry 7/19/2011
    else // if rgRestrictByEmail.ItemIndex = 2 then // no restiction
      mainsql := mainsql + ' FROM g_voucher_summary A, outer g_client, outer cc_ref, outer g_vendor WHERE ';

  end
  else // never happen - obolete functionality for this interface
    mainsql := mainsql + ' FROM vouchandqueue A, outer g_client, outer cc_ref, outer g_vendor WHERE ';

  temp := '';
  if (sender = rpbus_get_names) then
  begin
   mainsql := mainsql + 'vs_basesys = ''' +
      memSpecialReps.FieldByName('system').AsString + '''';
    temp := 'asdf';
  end
  else
  begin
    temp := GetDB_InStatement();
    MainSQL := MainSQL + ' ' + temp; // basesys in("USA") --> restricts by db arg
                                  // if paramstr(1) = "AUS" --> basesys in ("AUS","NEWZ") etc
  end;

  if temp <> '' then
    MainSQL := MainSQL + ' and ';

  MainSQL := mainsql + ' ( '; // whole date arg in parens

  // date arguments
  if cbUseSummaryTable.Checked then
  begin
    case rgStep1PayType.ItemIndex of
      0: // use pd dt
        begin
          MainSQL := mainsql + ' vs_paid_dt BETWEEN :date1 and :date2 ';

          if (sender = btnCombined2) or (Sender = btnYTD) or (sender = btnFleetMix) or
            (sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP) then
          begin // adding multiple ranges for special reports tab only
            if cbuserange2.Checked then
            begin
              MainSQL := mainsql + ' OR vs_paid_dt BETWEEN :dtRange2Start and :dtRange2end ';
            end;

            if cbuserange3.Checked then
            begin
              MainSQL := mainsql + ' OR vs_paid_dt BETWEEN :dtRange3Start and :dtRange3end ';
            end;
          end;
        end;
      1: // use create dt
        begin
          MainSQL := mainsql + ' vs_voucher_create_dt BETWEEN :date1 and :date2 ';

          if (sender = btnCombined2) or (Sender = btnYTD) or (sender = btnFleetMix) or
            (sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP) then
          begin
            if cbuserange2.Checked then
            begin
              MainSQL := mainsql + ' OR vs_voucher_create_dt BETWEEN :dtRange2Start and :dtRange2end ';
            end;

            if cbuserange3.Checked then
            begin
              MainSQL := mainsql + ' OR vs_voucher_create_dt BETWEEN :dtRange3Start and :dtRange3end ';
            end;
          end;
        end;
      2: // use pick up dt
        begin
          MainSQL := mainsql + ' vs_pu_dt BETWEEN :date1 and :date2 ';

          if (sender = btnCombined2) or (Sender = btnYTD) or (sender = btnFleetMix) or
            (sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP) then
          begin
            if cbuserange2.Checked then
            begin
              MainSQL := mainsql + ' OR vs_pu_dt BETWEEN :dtRange2Start and :dtRange2end ';
            end;

            if cbuserange3.Checked then
            begin
              MainSQL := mainsql + ' OR vs_pu_dt BETWEEN :dtRange3Start and :dtRange3end ';
            end;
          end;
        end
    else
      begin
        raise(exception.Create('No Date Type Selected.'));
        exit;
      end;
    end; //case
  end
  else
  begin // should never enter this section
    MainSQL := mainsql + ' VCHPQBATH BETWEEN :date1 and :date2  ';
  end;

  MainSQL := mainsql + ' ) '; // whole date arg in parens

  if trim(EditCountry.Text) <> '*' then
  begin // LIKE
    temp := '';
    temp := trim(UpperCase(EditCountry.Text));
    try
      temp := GetLike(temp);
    except on e: exception do
      begin
        raise(exception.Create('Country code: ' + e.Message));
        result := '';
        exit;
      end;
    end;

    if temp > '' then // use like on
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := mainsql + ' and vs_pu_ctry LIKE ''' + temp + ''' '
      else
        MainSQL := mainsql + ' and vopctry1 LIKE ''' + temp + ''' ';
    end
    else
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := mainSQL + ' and vs_pu_ctry = ''' + trim(UpperCase(EditCountry.Text)) + ''' '
      else
        MainSQL := mainSQL + ' and vopctry1 = ''' + trim(UpperCase(EditCountry.Text)) + ''' ';
    end;
  end;

  if trim(EditCity.Text) <> '*' then
  begin
    temp := '';
    temp := trim(UpperCase(EditCity.Text));
    try
      temp := GetLike(temp);
    except on e: exception do
      begin
        raise(exception.Create('City Name: ' + e.Message));
        result := '';
        exit;
      end;
    end;

    if temp > '' then
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := MainSQL + ' and vs_pu_city LIKE ''' + temp + ''' '
      else
        MainSQL := MainSQL + ' and vopcity1 LIKE ''' + temp + ''' ';
    end
    else
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := MainSQL + ' and vs_pu_city = "' + trim(UpperCase(EditCity.Text)) + '" '
      else
        MainSQL := MainSQL + ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ';
    end;
  end;

  if trim(EditOperator.Text) <> '*' then
  begin
    temp := '';
    temp := trim(UpperCase(EditOperator.Text));
    try
      temp := GetLike(temp);
    except on e: exception do
      begin
        raise(exception.Create('Operator code: ' + e.Message));
        result := '';
        exit;
      end;
    end;

    if temp > '' then
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := MainSQL + ' and vs_operator_code LIKE ''' + temp + ''' '
      else
        MainSQL := MainSQL + ' and vopid LIKE ''' + temp + ''' ';
    end
    else
    begin
      if cbUseSummaryTable.Checked then
        MainSQL := MainSQL + ' and vs_operator_code = ''' + trim(UpperCase(EditOperator.Text)) + ''' '
      else
        MainSQL := MainSQL + ' and vopid = ''' + trim(UpperCase(EditOperator.Text)) + ''' ';
    end;
  end; // operator

  if trim(cbOperator.Text) > '' then
  begin
    MainSQL := MainSQL + ' and ' + GetNOT_OPER_IN_Text();
  end;

  // direct/ta or both:
  if rgDirectTa.ItemIndex = 0 then
  begin // direct client - default
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and vs_direct_or_ta = ''DC'' '
    else
      MainSQL := MainSQL + ' and substr(gc_iata,1,2) = ''99'' ';
  end
  else if rgDirectTa.ItemIndex = 1 then
  begin // ta
    if cbUseSummaryTable.Checked then
      MainSQL := mainSQL + ' and vs_direct_or_ta = ''TA'' '
    else
      MainSQL := mainSQL + ' and substr(gc_iata,1,2) <> ''99'' ';
  end
  else if rgDirectTa.ItemIndex = 2 then
  begin // both
    nada;
  end
  else // invalid
  begin
    raise(exception.Create('please select direct/ta/ALL.'));
    result := '';
    exit;
  end;

 MainSQL := mainSQL + ' ' + GetConsortArg(); // restrict to a certain consortium based on
                                          // paramstr(2) REQUIRED for this arg to work
  try
    MainSQL := MainSQL + ' ' + GetEMailArg();
  except on e: exception do
    begin
      raise(exception.Create('BuildSQL.GetEmlArg --> ' + E.Message));
      result := '';
      exit;
    end;
  end;

  if cbFuturesOnly.Checked then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and vs_pu_dt >= current '
    else
      MainSQL := MainSQL + ' and vopdate1 >= current ';
  end;

  if (cbActiveOnly.Checked) then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and vs_active_history = "A" '
    else
      MainSQL :=  MainSQL + ' and vcntype = "A" ';
  end
  else if (cbcancelsonly.checked) then // filter cxls only
  begin
    if cbUseSummaryTable.Checked then
    begin
      MainSQL := MainSQL + ' and mod(vs_release, 2) = 0 ';
      MainSQL := MainSQL + ' AND vs_release = (select max(vs_release) from g_voucher_summary where vs_basesys = A.vs_basesys and A.vs_voucher = vs_voucher)';
    end
    else
    begin
      MainSQL := MainSQL + ' and mod(vchrel, 2) = 0 ';
      MainSQL := MainSQL + ' AND vchrel = (select max(vchrel) from vouchandqueue where basesys = A.basesys and A.vcnumb = vcnumb)';
    end;
  end;

  if cbUsePUDate.Checked then
  begin // paramatize to handle different Locale date formats
    if cbUseSummaryTable.Checked then
      MainSQL :=  MainSQL + ' and vs_pu_dt between :date3 and :date4 '
    else
      MainSQL :=  MainSQL + ' and vopdate1 between :date3 and :date4 ';
  end;

  // 05/05/2010 add secondary create date argument
  if cbUseCreateDate.Checked then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL :=  mainsql + ' and vs_voucher_create_dt BETWEEN :dateCRTstart and :dateCRTend '
    else
      MainSQL := MainSQL + ' and vopdatecreate BETWEEN :dateCRTstart and :dateCRTend ';
  end;

  if cbUseLastMin.Checked then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and (vs_pu_dt - date(vs_paid_dt)) <= ' + EditDaysBetween.Text
    else
      MainSQL := MainSQL + ' and (vopdate1-date(vchpqbath)) <= ' + EditDaysBetween.Text;
  end;

  if cbUseAge.Checked then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and vs_client_age between ' + editAGEStart.text + ' and ' + editAGEEnd.text + ' '
    else // voiamt4 smallint = client Age
      MainSQL := MainSQL + ' and voiamt4 between ' + editAGEStart.text + ' and ' + editAGEEnd.text + ' ';
  end;

  if cbUsePayType.Checked then
  begin
    MainSQL := MainSQL + ' ' + GetPTSQLSTR();
  end;

  if cbUseDuration.checked then
  begin
    if cbUseSummaryTable.Checked then
      MainSQL := MainSQL + ' and vs_duration between ' + editdurlow.Text + ' and ' + editdurhigh.text
    else
      MainSQL :=  MainSQL + ' and vopdate2-vopdate1 between ' + editdurlow.Text + ' and ' + editdurhigh.text;
  end;

  // either use one or the other cbUseSIPP OR cbSIPPWildCard
  if cbUSESipp.Checked then
  begin
    MainSQL := MainSQL + GetSIPPInStatement(clbSIPP1, 1);
    MainSQL :=  MainSQL + GetSIPPInStatement(clbSIPP2, 2);
    MainSQL :=  MainSQL + GetSIPPInStatement(clbSIPP3, 3);
    MainSQL :=  MainSQL + GetSIPPInStatement(clbSIPP4, 4);
  end
  else if (cbSIPPWildCard.checked) then // add sept 27, 2010
  begin // do a 'like' sipp
    if trim(EditSippWildcard.Text) <> '*' then // if * do nothing = 'ALL SIPPS'
    begin
      temp := '';
      temp := trim(UpperCase(EditSippWildcard.Text));
      try
        temp := GetLike(temp);
      except on e: exception do
        begin
          raise(exception.Create('SIPP: ' + e.Message));
          result := '';
          exit;
        end;
      end;

      if temp > '' then
      begin
        if cbUseSummaryTable.Checked then
          MainSQL :=  MainSQL + ' and vs_sipp LIKE "' + temp + '" '
        else
          MainSQL := MainSQL + ' and voprd1 LIKE "' + temp + '" ';
      end
      else
      begin
        if cbUseSummaryTable.Checked then
          MainSQL :=  MainSQL + ' and vs_sipp = "' + trim(UpperCase(EditSippWildcard.Text)) + '" '
        else
          MainSQL :=  MainSQL + ' and voprd1 = "' + trim(UpperCase(EditSippWildcard.Text)) + '" ';
      end;
    end;
  end; // end add Sept 27, 2010

  if cbUseGender.checked then
  begin
    MainSQL := MainSQL + ' ' + GetGenderArg();
  end;

  // drop info added 1/31/2008
  if cbUseDropDate.Checked then // use drop date filters
  begin
    if (dtpDropDtend.date < dtpDropDtStart.Date) then
    begin
      raise(exception.Create('BuildSQL.GetDropDateArg --> end date prior to start date.'));
      result := '';
      exit;
    end;

    if cbUseSummaryTable.Checked then
      MainSQL :=  MainSQL + ' and vs_do_dt BETWEEN :date5 and :date6 '
    else
      MainSQL :=  MainSQL + ' and vopdate2 BETWEEN :date5 and :date6 ';
  end;

  if cbUseDropCtry.Checked then // drop country
  begin
    if trim(EditDropCtry.Text) <> '*' then
    begin
      temp := '';
      temp := trim(UpperCase(EditDropCtry.Text));
      try
        temp := GetLike(temp);
      except on e: exception do
        begin
          raise(exception.Create('Drop Country: ' + e.Message));
          result := '';
          exit;
        end;
      end;

      if temp > '' then
      begin
        if cbUseSummaryTable.Checked then
          MainSQL := MainSQL + ' and vs_do_ctry LIKE ''' + temp + ''' '
        else
          MainSQL := MainSQL + ' and vopctry2 LIKE ''' + temp + ''' ';
      end
      else
      begin
        if cbUseSummaryTable.Checked then
          MainSQL := MainSQL + ' and vs_do_ctry = ''' + trim(UpperCase(EditDropCtry.Text)) + ''' '
        else
          MainSQL := MainSQL + ' and vopctry2 = ''' + trim(UpperCase(EditDropCtry.Text)) + ''' ';
      end;
    end;
  end;

  if cbUseDropCity.Checked then // drop city
  begin
    if trim(EditDropCity.Text) <> '*' then
    begin
      temp := '';
      temp := trim(UpperCase(EditDropCity.Text));
      try
        temp := GetLike(temp);
      except on e: exception do
        begin
          raise(exception.Create('Drop City: ' + e.Message));
          result := '';
          exit;
        end;
      end;

      if temp > '' then
      begin
        if cbUseSummaryTable.Checked then
          MainSQL := MainSQL + ' and vs_do_city LIKE ''' + temp + ''' '
        else
          MainSQL := MainSQL + ' and vopcity2 LIKE "' + temp + ''' ';
      end
      else
      begin
        if cbUseSummaryTable.Checked then
          MainSQL := MainSQL + ' and vs_do_city = ''' + trim(UpperCase(EditDropCity.Text)) + ''' '
        else
          MainSQL := MainSQL + ' and vopcity2 = ''' + trim(UpperCase(EditDropCity.Text)) + ''' ';
      end;
    end;
  end; // drop info

  // product type / source
  if cbUseTypes.Checked then
  begin
    if cbProductType.text <> 'ALL' then
      MainSQL := MainSQL + ' and vs_product_type = ''' + trim(UpperCase(cbProductType.text)) + ''' ';

    if cbProductSubType.Text <> 'ALL' then
      MainSQL := MainSQL + ' and vs_product_type_tag = ''' + trim(UpperCase(cbProductSubType.Text)) + ''' ';
  end;

  if cbUseSource.Checked then
  begin
    if CboxUseSource.Text <> 'ALL' then
      MainSQL := MainSQL + ' and vs_business_source = ''' + trim(UpperCase(CboxUseSource.Text)) + ''' ';
  end;

  // release number args added for Brett 2/7/2011
  if (cbReleaseNum.Checked) then
  begin
    try
      strtoint(EditReleaseNum.Text);
    except on e: Exception do
      begin
        PageControl1.ActivePage := TabStep2b;
        raise(exception.Create('release num must not be Preceded by a "0" or is invalid --> ' + e.Message));
        exit;
      end;
    end;
    MainSQL := MainSQL + ' and vs_release = ' + trim(EditReleaseNum.Text) + ' ';
  end;

   // release number args added for Brett 2/7/2011
  if (cbPlusBasic.Checked) then
  begin
    if trim(ComboPlusBasic.text) = 'plus' then
      MainSQL := MainSQL + ' and vs_plus_basic = ''P'' '
    else if trim(ComboPlusBasic.text) = 'basic' then
      MainSQL :=  MainSQL + ' and vs_plus_basic = ''B'' '
    else
    begin
      ;
    end;
  end;

  // iata list added 04/11/2011
  if (cbUseIataList.Checked) then
  begin
    try
      sl := TStringList.Create;
      try
        sl.Text := memoIataList.text;
        stringListToInStatement(sl); // returns in("item1", "item2", "etc")
        MainSQL := MainSQL + ' and vs_iata ' + sl.Text + ' ';
      except on e: exception do
        begin
          raise(exception.create('buildSQL():failed to get IATA in statement -> ' + e.Message));
        end;
      end;
    finally
      sl.free;
    end;
  end;

  if (cbUseClientCtry.checked) and (trim(EditClientCountry.text) <> '') then // client country
  begin
    MainSQL := MainSQL + ' and gc_ctry LIKE "' + trim(uppercase(EditClientCountry.text)) + ''' ';
  end;

  // join the tables:
  if (Sender = btnytd) or (Sender = btnCombined2) or (sender = btnFleetMix)
    or // and not cbUseClientCtry.Checked added 7/21
    (
    ((sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP)) and (not cbUseClientCtry.Checked)
    )
    then // moved to below to add client 7/20/2011
  begin
    ; // no joins needed - just getting from g_voucher_summary
  end
  else if (sender = btnRPBUS) or (sender = rpbus_get_names) or (sender = btnRPBUSNew)
    or // and cbUseClientCtry.Checked added 7/21
    (
    ((sender = btnSippAnalysis) or (sender = btnSippAnalysisSIPP)) and (cbUseClientCtry.Checked)
    )
    then // moved to add gclient 7/20/2011
  begin
    if Flag <> '' then // flag is only used in repeat biz
      MainSQL := mainSQL + 'and vs_direct_or_ta = "' + flag + ''' ';

    MainSQL := mainSQL + ' and gc_system = vs_basesys and ';
    MainSQL := mainSQL + ' gc_voucher = vs_voucher ';
    // release number is not needed - only one record for client to N vouchers
  end
  else if cbUseSummaryTable.Checked then
  begin
    MainSQL := mainSQL + ' and gc_system = vs_basesys and gc_voucher = vs_voucher '; //and gc_release = vchrel ');
    MainSQL := mainSQL + ' and gv_basesys = vs_basesys and gv_iata = vs_iata ';
    MainSQL := mainSQL + ' and cc_id = vs_payment_type '; // cc_ref join
  end
  else
  begin
    // join vouchandqueue, g_client, cc_ref:
    MainSQL := mainSQL + ' and gc_system = basesys and gc_voucher = vcnumb ' ; //and gc_release = vchrel ');
    MainSQL := mainSQL + ' and gv_basesys = A.basesys and gv_iata = A.vciata ';
    MainSQL := mainSQL + ' and cc_id = vccrcardid '; // cc_ref join
  end;

  // grouping / sorting
  if (Sender = btnCombined2) then
  begin
    MainSQL := mainSQL + ' group by 1,2,3,4,5 ';
    MainSQL := mainSQL + ' order by 1,2,3,4 ';
  end
  else if (Sender = btnytd) then
  begin
    MainSQL := mainSQL + ' group by 1,2,3,4 ';
    MainSQL := mainSQL + ' order by 1,2,3,4 ';
  end
  else if (sender = btnFleetMix) then
  begin
    MainSQL := mainSQL + ' group by 1,2,3,4,5 ';
    MainSQL := mainSQL + ' order by 1,2,3,4,5 ';
  end
  else if (sender = btnRPBUS) or (sender = btnRPBusNew) then
  begin
    MainSQL := mainSQL + 'group by 1 order by 1';
  end;

  result := mainSQL;
end;

procedure TfrmMainMkt.FillMemFields();
var
  DREC: TDurationRec;
  sortStr: string;
  i, x: integer;

  procedure plug(InError: string);
  begin
    memo1.Lines.add(InError);
  end;
begin
  btnEMAILSend.Enabled := false;
  Q1.DisableControls;
  mem.DisableControls;
  xxxxx := 0;
  try
    try
      starttime := GetTickCount();
      OpenQuery(Q1); // handle the cursor
      Q1.First;

      while not Q1.eof do
      begin // fill the mem table
        if bkill then break;

        try
          if not (cbUseSummaryTable.Checked) then
          begin // this is to be obsoleted .. eg looking at anything other than the summary table.. that is
            if not DSC.Execute then
            begin
              plug(dsc.ErrorMessage);
            end
            else
            begin
              mem.Insert;

              tfmem_basesys.AsString := q1.FieldByName('basesys').AsString; //('basesys', ftString, 5, true);
              tfmem_voucher.AsString := q1.FieldByName('vcnumb').AsString + '-' +
                q1.FieldByName('vchrel').AsString;

              tfmem_confirmation.AsString := q1.FieldByName('vccconfirm').AsString; //('confirmation', ftString, 40, false);
              tfmem_date_created.AsDateTime := trunc(Q1.FieldByName('vchdate').AsDateTime); //('date_booked', ftDate, 0, false);
              tfmem_date_of_svc.AsDateTime := trunc(q1.FieldByName('vopdate1').AsDateTime); //('date_of_svc', ftDate, 0, false);
              tfmem_time_of_svc.AsString := FormatDateTime('HH:MM:SS', frac(Q1.FieldByName('voptime1').AsDateTime)); //('time_of_svc', ftString, 25, true);
              tfmem_retail_total.AsFloat := RoundTo(dsc.RetailTot, -2); //('retail_total', ftFloat, 0, true);
              tfmem_opdue_curr.AsFloat := RoundTo(dsc.OperatorDueCurr, -2); //('opdue_curr', ftFloat, 0, true);
              tfmem_curr_type.AsString := Q1.FieldByName('vopcurrency').AsString; //('curr_type', ftString, 5, false);
              tfmem_curr_exch_rate.AsFloat := RoundTo(dsc.ExchangeRate, -3); //('curr_exch_rate', ftFloat, 0, false);
              tfmem_sipp.AsString := Q1.FieldByName('voprd1').AsString; // sipp//('sipp', ftString, 10, false);
              tfmem_oper_car_code.AsString := Q1.FieldByName('vopcomt1').AsString; // sipp//('oper_car_code', ftString, 30, false); // vopcomt1
              tfmem_date_of_transaction.AsDateTime := trunc(Q1.FieldByName('vchpqbath').AsDateTime); //('date_of_transaction', ftDate, 0, false);

              tfmem_commission.AsFloat := RoundTo(dsc.Comm, -2); //('commission', ftFloat, 0, false);
              tfmem_profit.AsFloat := RoundTo(dsc.ProfitAComm, -2); //('profit', ftFloat, 0, false);
              tfmem_duration.asFloat := RoundTo(dsc.NumberDays, -2); //('duration', ftFloat, 0, false);
              tfmem_lead_days_create_pu.AsFloat := trunc(Q1.FieldByName('lead_days').AsFloat); //('days_between_PD_PU', ftFloat, 0, false);

              tfmem_dtyear.AsString := trim(Q1.FieldByName('dt_year').AsString); // add 04/27/2011
              tfmem_lead_days_pdpu.AsFloat := trunc(Q1.FieldByName('lead_days_pdpu').AsFloat);

              tfmem_iata.AsString := dsc.Iata; //('iata', ftstring, 9, false);
              tfmem_country_pu.AsString := dsc.CountryPU; //('country_pu', ftstring, 3, false); //vopctry1
              tfmem_city_pu.AsString := dsc.CityPU; //('city_pu', ftstring, 20, false); // vopcity1
              tfmem_loc_pu.AsString := dsc.LocPU; //('loc_pu', ftstring, 2, false); // voploc1
              tfmem_operator.AsString := dsc.OperatorPU; //('operator', ftstring, 20, false); // vopid
              tfmem_consortium.AsString := Q1.FieldByName('voconsort').AsString; //('consortium', ftString, 10, false); // voconsort
              tfmem_cc_name.AsString := Q1.FieldByName('cc_name').AsString; //('cc_name', ftString, 18, false); //payment type
              tfmem_vcspecrequest.AsString := Q1.FieldByName('vcspecrequest').AsString; //('vcspecrequest', ftString, 100, false); // special requests

              tfmem_service_type.AsString := GetServiceType2(Q1.FieldByName('basesys').AsString,
                Q1.FieldByName('vopsubt').AsString,
                Q1.FieldByName('vopstype1').AsString,
                Q1.FieldByName('vopid').AsString);

              tfmem_source.AsString := Q1.FieldByName('vopsubt').AsString; //('source', ftString, 2, false); // vopsubt

              tfmem_c_prefix.AsString := Q1.FieldByName('gc_surname').AsString; //('c_prefix', ftString, 8, false); // gc_surname is used for Mr.|Mrs. etc

              tfmem_c_fullname.AsString := trim(uppercase(Q1.FieldByName('gc_lastname').AsString)) + ', ' +
                trim(uppercase(Q1.FieldByName('gc_firstname').AsString)) + ' ' +
                trim(uppercase(Q1.FieldByName('gc_initial').AsString));

              tfmem_c_addr1.AsString := Q1.FieldByName('gc_addr1').AsString; //('c_addr1', ftString, 60, false);
              tfmem_c_addr2.AsString := Q1.FieldByName('gc_addr2').AsString; //('c_addr2', ftString, 60, false);
              tfmem_c_prov.AsString := Q1.FieldByName('gc_prov').AsString; //('c_prov', ftString, 10, false);
              tfmem_c_city.AsString := Q1.FieldByName('gc_city').AsString; //('c_city', ftString, 50, false);
              tfmem_c_postal.AsString := Q1.FieldByName('gc_postcode').AsString; //('c_postal', ftString, 20, false);
              tfmem_c_country.AsString := Q1.FieldByName('gc_ctry').AsString;
              tfmem_c_email.AsString := trim(uppercase(Q1.FieldByName('gc_email').AsString)); //('c_email', ftString, 250, false);
              tfmem_c_phone.AsString := Q1.FieldByName('gc_phone').AsString; //('c_phone', ftString, 40, false);
              tfmem_c_age.AsInteger := Q1.FieldByName('voiamt4').AsInteger; //('age', ftInteger, 0, false); //voiamt4 = age

              if odd(q1.FieldByName('vchrel').AsInteger) then
                tfmem_vcount.AsInteger := 1
              else tfmem_vcount.AsInteger := -1;

              // add 06/24/2010 - never be used since this secition is never entered
              tfmem_promo1.asstring := Q1.FieldByName('vcvpromo1').asstring;
              tfmem_promo2.asstring := Q1.FieldByName('vcvpromo2').asstring;
              tfmem_promo3.asstring := Q1.FieldByName('vcvpromo3').asstring;

              // must do this last
              sortStr := '[';
              x := 0;
              x := DstList.Count - 1;
              for i := 0 to x do
              begin
                sortStr := sortStr + trim(uppercase(mem.FieldByName(DstList.Items[i]).AsString));
                if i < x then sortStr := sortStr + ',';
              end;
              sortStr := sortStr + ']';

              tfmem_group_break_sort.AsString := sortStr;

              try
                mem.Post;
              except on e: exception do
                begin
                  memo1.Lines.add('post: --> ' + e.Message);
                  mem.Rollback;
                end;
              end;
            end;
          end
          else // use summary table to fill the mem
          begin
            mem.Insert;
           //jim 06/18/2012
            tfmem_basesys.AsString := q1.FieldByName('vs_basesys').AsString; //('basesys', ftString, 5, true);
            tfmem_voucher.AsString := q1.FieldByName('vs_voucher').AsString + '-' +
              q1.FieldByName('vs_release').AsString;

            tfmem_confirmation.AsString := q1.FieldByName('vs_op_confirmation').AsString; //('confirmation', ftString, 40, false);
            tfmem_date_created.AsDateTime := trunc(Q1.FieldByName('vs_voucher_create_dt').AsDateTime); //('date_booked', ftDate, 0, false);
            tfmem_date_of_svc.AsDateTime := trunc(q1.FieldByName('vs_pu_dt').AsDateTime); //('date_of_svc', ftDate, 0, false);
            tfmem_time_of_svc.AsString := FormatDateTime('HH:MM:SS', frac(Q1.FieldByName('vs_pu_time').AsDateTime)); //('time_of_svc', ftString, 25, true);

            tfmem_retail_total.AsFloat := RoundTo(q1.FieldByName('vs_retail_total').AsFloat, -2); //('retail_total', ftFloat, 0, true);
            tfmem_opdue_curr.AsFloat := RoundTo(q1.FieldByName('vs_operator_due_curr').AsFloat, -2); //('opdue_curr', ftFloat, 0, true);

            tfmem_curr_type.AsString := Q1.FieldByName('vs_currency_operator').AsString; //('curr_type', ftString, 5, false);
            tfmem_curr_exch_rate.AsFloat := RoundTo(Q1.FieldByName('vs_exchange_rt').AsFloat, -3); //('curr_exch_rate', ftFloat, 0, false);
            tfmem_sipp.AsString := Q1.FieldByName('vs_sipp').AsString; // sipp//('sipp', ftString, 10, false);
            tfmem_oper_car_code.AsString := Q1.FieldByName('vs_sipp_type').AsString; // sipp//('oper_car_code', ftString, 30, false); // vopcomt1
            tfmem_date_of_transaction.AsDateTime := trunc(Q1.FieldByName('vs_paid_dt').AsDateTime); //('date_of_transaction', ftDate, 0, false);

            tfmem_commission.AsFloat := RoundTo(Q1.FieldByName('vs_comm_tot').AsFloat, -2); //('commission', ftFloat, 0, false);
            tfmem_profit.AsFloat := RoundTo(Q1.FieldByName('vs_profit').AsFloat, -2); //('profit', ftFloat, 0, false);

            tfmem_duration.asFloat := RoundTo(Q1.FieldByName('vs_duration').AsFloat, -2); //('duration', ftFloat, 0, false);

            tfmem_lead_days_create_pu.AsFloat := trunc(Q1.FieldByName('vs_days_between_create_pu').AsFloat); //('days_between_PD_PU', ftFloat, 0, false);

            tfmem_dtyear.AsString := trim(Q1.FieldByName('dt_year').AsString); // add 04/27/2011
            tfmem_lead_days_pdpu.AsFloat := trunc(Q1.FieldByName('lead_days_pdpu').AsFloat);

            tfmem_iata.AsString := Q1.FieldByName('vs_iata').AsString; //('iata', ftstring, 9, false);
            tfmem_country_pu.AsString := Q1.FieldByName('vs_pu_ctry').Asstring; //('country_pu', ftstring, 3, false); //vopctry1
            tfmem_city_pu.AsString := Q1.FieldByName('vs_pu_city').AsString; //('city_pu', ftstring, 20, false); // vopcity1
            tfmem_loc_pu.AsString := Q1.FieldByName('vs_pu_loc').AsString; //('loc_pu', ftstring, 2, false); // voploc1
            tfmem_operator.AsString := Q1.FieldByName('vs_operator_code').AsString; //('operator', ftstring, 20, false); // vopid
            tfmem_consortium.AsString := Q1.FieldByName('vs_consortium').AsString; //('consortium', ftString, 10, false); // voconsort

            // must get cc_name from cc_ref table on the join ...
            tfmem_cc_name.AsString := Q1.FieldByName('cc_name').AsString; //('cc_name', ftString, 18, false); //payment type
            tfmem_vcspecrequest.AsString := Q1.FieldByName('vs_comments').AsString; //('vcspecrequest', ftString, 100, false); // special requests
            tfmem_service_type.AsString := Q1.FieldByName('vs_product_type_tag').AsString;
            tfmem_source.AsString := Q1.FieldByName('vs_subtype').AsString; //('source', ftString, 2, false); // vopsubt

            tfmem_c_fullname.AsString := trim(uppercase(Q1.FieldByName('gc_lastname').AsString)) + ', ' +
              trim(uppercase(Q1.FieldByName('gc_firstname').AsString)) + ' ' +
              trim(uppercase(Q1.FieldByName('gc_initial').AsString));

            tfmem_c_prefix.AsString := Q1.FieldByName('gc_surname').AsString; //('c_prefix', ftString, 8, false); // gc_surname is used for Mr.|Mrs. etc
            tfmem_c_addr1.AsString := Q1.FieldByName('gc_addr1').AsString; //('c_addr1', ftString, 60, false);
            tfmem_c_addr2.AsString := Q1.FieldByName('gc_addr2').AsString; //('c_addr2', ftString, 60, false);
            tfmem_c_prov.AsString := Q1.FieldByName('gc_prov').AsString; //('c_prov', ftString, 10, false);
            tfmem_c_city.AsString := Q1.FieldByName('gc_city').AsString; //('c_city', ftString, 50, false);
            tfmem_c_postal.AsString := Q1.FieldByName('gc_postcode').AsString; //('c_postal', ftString, 20, false);
            tfmem_c_country.AsString := Q1.FieldByName('gc_ctry').AsString;
            tfmem_c_email.AsString := trim(uppercase(Q1.FieldByName('gc_email').AsString)); //('c_email', ftString, 250, false);
            tfmem_c_phone.AsString := Q1.FieldByName('gc_phone').AsString; //('c_phone', ftString, 40, false);
            tfmem_c_age.AsInteger := Q1.FieldByName('vs_client_age').AsInteger; //('age', ftInteger, 0, false); //voiamt4 = age


            if rgCountMethod.ItemIndex = 0 then // regular count vs logical count
              tfmem_vcount.AsInteger := Q1.FieldByName('vs_count_voucher').AsInteger
            else
              tfmem_vcount.AsInteger := Q1.FieldByName('vs_count_logical').AsInteger;

            // add new fields here from summary table and g_vendor.........
            tfmem_active_history.AsString := Q1.FieldByName('vs_active_history').AsString;
            tfmem_p_or_q.AsString := Q1.FieldByName('vs_p_or_q').AsString;
            tfmem_payment_type.AsString := Q1.FieldByName('vs_payment_type').asstring;
            tfmem_fp_pp.AsString := Q1.FieldByName('vs_fp_pp').AsString;
            tfmem_plus_basic.AsString := Q1.FieldByName('vs_plus_basic').AsString;
            tfmem_rid.AsInteger := Q1.FieldByName('vs_rid').AsInteger;
            tfmem_auth_code.AsString := Q1.FieldByName('vs_auth_code').AsString;
            tfmem_product_type.AsString := Q1.FieldByName('vs_product_type').AsString;
            tfmem_business_source.AsString := Q1.FieldByName('vs_business_source').AsString;
            tfmem_do_ctry.AsString := Q1.FieldByName('vs_do_ctry').AsString;
            tfmem_do_city.AsString := Q1.FieldByName('vs_do_city').AsString;
            tfmem_do_loc.AsString := Q1.FieldByName('vs_do_loc').AsString;
            tfmem_updated_dt.AsDateTime := trunc(Q1.FieldByName('vs_updated_dt').AsDateTime);
            tfmem_do_dt.AsDateTime := trunc(Q1.FieldByName('vs_do_dt').AsDateTime);
            tfmem_do_time.AsString := FormatDateTime('HH:MM:SS', frac(Q1.FieldByName('vs_do_time').AsDateTime));
            tfmem_op_rate_code.asstring := Q1.FieldByName('vs_op_rate_code').AsString;
            tfmem_bus_account_number.asstring := Q1.FieldByName('vs_vcflighttckt').AsString;
            tfmem_car_name.asstring := Q1.FieldByName('vs_carmname').AsString;
            tfmem_sipp_description.AsString := Q1.FieldByName('vs_sipp_description').AsString;
            tfmem_num_passengers.AsInteger := Q1.FieldByName('vs_num_passengers').AsInteger;
            tfmem_comm_due.AsFloat := Q1.FieldByName('vs_comm_due').AsFloat;
            tfmem_comm_percent.AsFloat := Q1.FieldByName('vs_comm_percent').AsFloat;
            tfmem_home_curr.AsString := Q1.FieldByName('vs_home_curr').AsString;
            tfmem_vat_tax_rt.AsFloat := Q1.FieldByName('vs_vat_tax_rt').AsFloat;
            tfmem_home_ctry.AsString := Q1.FieldByName('vs_home_ctry').AsString;
            tfmem_base_rate_retail.AsFloat := Q1.FieldByName('vs_base_rate_retail').AsFloat;
            tfmem_discount_retail.AsFloat := Q1.FieldByName('vs_discount_retail').AsFloat;
            tfmem_insurance_tot_retail.AsFloat := Q1.FieldByName('vs_insurance_tot_retail').AsFloat;
            tfmem_tax_tot_retail.AsFloat := Q1.FieldByName('vs_tax_tot_retail').AsFloat;
            tfmem_pu_fee_retail.AsFloat := Q1.FieldByName('vs_pu_fee_retail').AsFloat;
            tfmem_do_fee_retail.AsFloat := Q1.FieldByName('vs_do_fee_retail').AsFloat;
            tfmem_xtra_fee_retail.AsFloat := Q1.FieldByName('vs_xtra_fee_retail').AsFloat;
            tfmem_base_rate_wholesale.AsFloat := Q1.FieldByName('vs_base_rate_wholesale').AsFloat;
            tfmem_insurance_tot_wholesale.AsFloat := Q1.FieldByName('vs_insurance_tot_wholesale').AsFloat;
            tfmem_tax_tot_wholesale.AsFloat := Q1.FieldByName('vs_tax_tot_wholesale').AsFloat;
            tfmem_pu_fee_wholesale.AsFloat := Q1.FieldByName('vs_pu_fee_wholesale').AsFloat;
            tfmem_do_fee_wholesale.AsFloat := Q1.FieldByName('vs_do_fee_wholesale').AsFloat;
            tfmem_wholesale_total.AsFloat := Q1.FieldByName('vs_wholesale_total').AsFloat;
            tfmem_operator_due.AsFloat := Q1.FieldByName('vs_operator_due').AsFloat;
            tfmem_deposit_amt.AsFloat := Q1.FieldByName('vs_deposit_amt').AsFloat;
            tfmem_waived_amt.AsFloat := Q1.FieldByName('vs_waived_amt').AsFloat;
            tfmem_deposit_after_waive.AsFloat := Q1.FieldByName('vs_deposit_after_waive').AsFloat;
            tfmem_due_at_pu.AsFloat := Q1.FieldByName('vs_due_at_pu').AsFloat;
            tfmem_deferred_amt.AsFloat := Q1.FieldByName('vs_deferred_amt').AsFloat;
            tfmem_gross_revenue.AsFloat := Q1.FieldByName('vs_gross_revenue').AsFloat;
            tfmem_operator_cost.AsFloat := Q1.FieldByName('vs_operator_cost').AsFloat;
            tfmem_count_new.AsInteger := Q1.FieldByName('vs_count_new').AsInteger;
            tfmem_count_cxl.AsInteger := Q1.FieldByName('vs_count_cxl').AsInteger;
            tfmem_count_change.AsInteger := Q1.FieldByName('vs_count_change').AsInteger;
            tfmem_direct_or_ta.AsString := Q1.FieldByName('vs_direct_or_ta').AsString;
            tfmem_member_account.asstring := Q1.FieldByName('vs_voiamt7').AsString;
            tfmem_gv_name.AsString := Q1.FieldByName('gv_name').AsString;
            tfmem_gv_legal_name.AsString := Q1.FieldByName('gv_legal_name').AsString;
            tfmem_gv_contact_name.AsString := Q1.FieldByName('gv_contact_name').AsString;
            tfmem_gv_addr1.AsString := Q1.FieldByName('gv_addr1').AsString;
            tfmem_gv_addr2.AsString := Q1.FieldByName('gv_addr2').AsString;
            tfmem_gv_addr3.AsString := Q1.FieldByName('gv_addr3').AsString;
            tfmem_gv_addr4.AsString := Q1.FieldByName('gv_addr4').AsString;
            tfmem_gv_city.AsString := Q1.FieldByName('gv_city').AsString;
            tfmem_gv_prov.AsString := Q1.FieldByName('gv_prov').AsString;
            tfmem_gv_postal.AsString := Q1.FieldByName('gv_postal').AsString;
            tfmem_gv_country.AsString := Q1.FieldByName('gv_country').AsString;
            tfmem_gv_phone_prefix.AsString := Q1.FieldByName('gv_phone_prefix').AsString;
            tfmem_gv_phone_num.AsString := Q1.FieldByName('gv_phone_num').AsString;
            tfmem_gv_fax_prefix.AsString := Q1.FieldByName('gv_fax_prefix').AsString;
            tfmem_gv_fax_num.AsString := Q1.FieldByName('gv_fax_num').AsString;
            tfmem_gv_email.AsString := Q1.FieldByName('gv_email').AsString;

            tfmem_promo1.asstring := Q1.FieldByName('vs_promo1').asstring;
            tfmem_promo2.asstring := Q1.FieldByName('vs_promo2').asstring;
            tfmem_promo3.asstring := Q1.FieldByName('vs_promo3').asstring;

            // must do this last
            sortStr := '[';
            x := 0;
            x := DstList.Count - 1;

            for i := 0 to x do
            begin
              sortStr := sortStr + trim(uppercase(mem.FieldByName(DstList.Items[i]).AsString));
              if i < x then sortStr := sortStr + ',';
            end;

            sortStr := sortStr + ']';
            tfmem_group_break_sort.AsString := sortStr;

            try
              mem.Post;
            except on e: exception do
              begin
                memo1.Lines.add('post: --> ' + e.Message);
                mem.Rollback;
              end;
            end;
          end; // else use summary table

        except on e: exception do
            plug('big except: ' + e.Message);
        end;
        Q1.Next;
      end;

      endtime := GetTickCount();
      DREC := DwordToDuration(ENDtime - Starttime);

      sb.panels[1].text := 'Min: ' + IntToStr(drec.minutes) +
        ' Sec: ' + IntToStr(drec.seconds);

      sb.Panels[2].Text := 'data retrieved OK';

      if not (mem.IsEmpty) then
      begin
        datasource1.DataSet := mem;
      end
      else
        showmessage('no data returned.');

      if bkill then
        sb.Panels[2].Text := 'Process killed. *** Incomplete Data *** ';

      application.processmessages;
    except on e: exception do
        showmessage('Failed to open. Err -> 1' + E.Message);
    end;
  finally
    Q1.EnableControls;
    mem.EnableControls;
  end;
end;

procedure TfrmMainMkt.DoOutPut(sender: TObject; O_Type, InFileName: string);
var sFILENAME: string; //  DoOutPut(Button1, 'HTML', GlobFName);
begin
  globEMLFileName := '';
  O_Type := trim(upperCase(O_Type));
  xxxxx := 0;

  if (Sender = btnCombined2) then
  begin
    if memSpecialReps.IsEmpty then
    begin
      showmessage('no data!.');
      exit;
    end;
  end
  else if mem.IsEmpty then
  begin
    showmessage('Please "Get Data" first by clicking the "GetData" button.');
    pagecontrol1.ActivePage := TabOutput;
    exit;
  end;

  if not ((O_Type = 'CSV') or (O_Type = 'XML') or (O_Type = 'HTML')) then
  begin
    showmessage('invalid output type.');
    exit;
  end;

  if (trim(InFileName) = '') then InFileName := GetGUIDString();

  try
    Q1.DisableControls;
    mem.DisableControls;
    sFILENAME := SetDirectoryForTSUser();

    if (O_Type = 'CSV') then
    begin
      sFILENAME := sFILENAME + 'CSV_' + InFileName + '.csv';
    end
    else if (O_Type = 'XML') then
    begin
      sFILENAME := sFILENAME + 'XML_' + InFileName + '.xml';
    end
    else // O_Type == 'HTML'
    begin
      sFILENAME := sFILENAME + 'HTML_' + InFileName + '.html';
    end;

    sb.Panels[2].Text := 'Please wait..Generating ' + O_Type;
    application.ProcessMessages;

    try
      if fileexists(sFILENAME) then deletefile(sFilename);
    except;
    end;

    globEMLFileName := sFILENAME;

    if (sender = btnCombined2) or (sender = btnFleetMix) or (sender = btnYTD) then
    begin
      try
        if (O_Type = 'CSV') then
          SaveCSV(DSSpecialReps, sFileName, 'AE_WHSE_RESULTS' {GetHeader(InFileName)}, ',')
        else if (O_Type = 'XML') then
          SaveXMLToFile(dsSpecialReps, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1)
        else // O_Type == 'HTML'
          SaveDataSetToHtmlFile(dsSpecialReps, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1);
      except on e: exception do
        begin
          showmessage('error saving ' + O_Type + ' --> ' + E.Message);
        end;
      end;
    end
    else
    begin
      try
        if (O_Type = 'CSV') then
          SaveCSV(DataSource1, sFileName, 'AE_WHSE_RESULTS' {GetHeader(InFileName)}, ',')
        else if (O_Type = 'XML') then
          SaveXMLToFile(datasource1, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1)
        else // O_Type == 'HTML'
          SaveDataSetToHtmlFile(datasource1, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1);
      except on e: exception do
        begin
          showmessage('error saving ' + O_Type + ' --> ' + E.Message);
        end;
      end;
    end;

    sb.Panels[2].Text := 'Done generating ' + O_Type + '.';
    application.ProcessMessages;

    if cbEmail.Checked then EMail(sFILENAME)
    else OpenFile(sFILENAME);

    btnEMailSend.Enabled := true;
  finally
    Q1.EnableControls;
    mem.EnableControls;
  end;
end;

procedure TfrmMainMkt.makeReport;
const
  TmpEspacamento = 0.75;
  StartTop = 0.1875;
  AutoSize = false;
  TmpWidth = 0.75;
var
  I: Integer;
  Txt: TppLabel;
  DBTxt : TppDBText;
  StartLeft: Double;
  sFILENAME : string;

  begin
  StartLeft := 0;
  sFILENAME := SetDirectoryForTSUser();
  ppDBPipeline1.DataSource := DataSource1;
  ppReport1.DataPipeline := ppDBPipeline1;
  ppHeaderBand11.Visible := True;
  with DataSource1.DataSet do

   begin
    for i := 0 to DataSource1.DataSet.FieldCount - 1 do
      begin
            Txt                 := TppLabel.Create(ppReport1);
            Txt.Caption         := DataSource1.DataSet.Fields[i].FieldName;
            Txt.Left            := StartLeft;
            Txt.Top             := StartTop;
            Txt.Band            := ppHeaderBand11;
            Txt.AutoSize        := AutoSize;
            Txt.Width           := TmpWidth;
            Txt.Font            := Font;
            //Txt.TextAlignment   := TextAlignment;
            //Txt.Border          := Border;
            Txt.Transparent     := true;
            Txt.Tag             := -50000;
            StartLeft           := TmpEspacamento + StartLeft;
      end;
    StartLeft := 0;

    for i := 0 to DataSource1.DataSet.FieldCount - 1 do
      begin
            DBTxt                 := TppDBText.Create(self);
            DBTxt.DataField       := DataSource1.DataSet.Fields[i].FieldName;
            DBTxt.DataPipeline    := ppDBPipeline1;
            DBTxt.Left            := StartLeft;
            DBTxt.Top             := StartTop;
            DBTxt.DesignLayer     := ppDesignLayer13;
            DBTxt.Band            := ppDetailBand12;
            DBTxt.AutoSize        := AutoSize;
            DBTxt.Width           := TmpWidth;
            DBTxt.Font            := Font;
            //Txt.TextAlignment   := TextAlignment;
            //Txt.Border          := Border;
            DBTxt.Transparent     := true;
            DBTxt.Tag             := -50000;
            StartLeft             := TmpEspacamento + StartLeft;
      end;
    end;

  sFILENAME := sFILENAME + 'Report.xlsx';
//  ppReport1.DeviceType := 'XLSXData';
 // ppReport1.TextFileName := sFILENAME;
//  ppReport1.AllowPrintToFile := True;
  ppReport1.Print
  end;



procedure TfrmMainMkt.addToRunLog(sender: TButton);
var temp, xxx, theSQL,
  usr, pc, basesys, futuresOnly,
    reportName, ctry, city,
    otherFlags, appname: string;
  x: integer;
  startDate, endDate: TDate;
  MyQuery: TFDQuery;
begin
  usr := '';
  pc := '';
  basesys := '';
  futuresOnly := '';
  reportName := '';
  ctry := '';
  city := '';
  otherFlags := '';
  appName := '';
  theSQL := '';
  usr := GetTheUserName();
  pc := GetMachineName();
  basesys := GetSelectedBaseSysList();

  if cbFuturesOnly.Checked then futuresOnly := 'y'
  else futuresOnly := 'n';

  reportname := GlobFName;
  ctry := EditCountry.Text;
  city := EditCity.Text;
  appName := application.Title;
  startDate := dtpStart.date;
  endDate := dtpEnd.Date;

  temp := 'DB''s: ' + basesys + '|' +
    'PD_dates: ' + dateToStr(dtpStart.date) + '-' + dateToStr(dtpEnd.Date) + '|';

  if cbFuturesOnly.Checked then
    temp := temp + 'for Future PU only|';

  temp := temp + 'country: [' + ctry + ']';
  temp := temp + 'city: [' + city + ']';

  try // log file::
    jSaveFile(LOG_PATH, 'dss_run_log', '.txt',
      usr + '|machine: ' + pc +
      '|' + Application.Name + '|' + temp + '| PC date: ' + DateTImeTOStr(now));
  except;
  end;

  if not conn.Connected then
  begin
    try connect1Click(nil);
    except exit;
    end;
  end;

  theSQL := 'insert into dss_track_reports values(0,"' + usr + '",' +
    '"' + pc + '",' +
    'current year to second,' +
    '"' + basesys + '",' +
    'date("' + dateToStr(startDate) + '"),' +
    'date("' + dateToStr(endDate) + '"),' +
    '"' + futuresOnly + '",' +
    '"' + reportName + '",' +
    '"' + ctry + '",' +
    '"' + city + '",' +
    '"' + otherFlags + '",' +
    '"' + appName + '");';
  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.ExecSQL;
    except on e: exception do
      begin
        jSaveFile(LOG_PATH, 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed insert on dss_track_reports: --> ' + e.message);
      end;
    end;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

procedure TfrmMainMkt.addToRunLog2(ReportName, ReportTitle: string);
var
  temp, xxx, theSQL,
    usr, pc, basesys, futuresOnly,
    ctry, city,
    appname: string;
  x: integer;
  startDate, endDate: TDate;
  MyQuery: TFDQuery;

begin
  usr := '';
  pc := '';
  basesys := '';
  futuresOnly := '';
  ctry := '';
  city := '';
  appName := '';
  theSQL := '';
  usr := GetTheUserName();
  pc := GetMachineName();
  basesys := GetSelectedBaseSysList();

  ctry := EditCountry.Text;
  city := EditCity.Text;
  appName := application.Title;
  startDate := dtpStart.date;
  endDate := dtpEnd.Date;

  reportname := WhiteSpaceTOUnderScores(reportname);
  reporttitle := WhiteSpaceTOUnderScores(reporttitle);

  reportname := strippunct(reportname);
  reporttitle := strippunct(reporttitle);

  try // log file::
    jSaveFile(LOG_PATH, 'dss_run_log2', '.txt',
      usr + '|machine: ' + pc +
      '|' + Application.Name + '|' + ReportTitle + '| PC date: ' + DateTImeTOStr(now));
  except;
  end;

  if not conn.Connected then
  begin
    try connect1Click(nil);
    except exit;
    end;
  end;

  theSQL := 'insert into dss_track_reports values(0,"' + usr + '",' +
    '"' + pc + '",' +
    'current year to second,' +
    '"' + basesys + '",' +
    'date("' + dateToStr(startDate) + '"),' +
    'date("' + dateToStr(endDate) + '"),' +
    '"X",' +
    '"' + reportName + '",' +
    '"' + ctry + '",' +
    '"' + city + '",' +
    '"' + ReportTitle + '",' +
    '"' + appName + '");';

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.ExecSQL;
    except on e: exception do
      begin
        jSaveFile(LOG_PATH, 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed insert on dss_track_reports: --> ' + e.message);
      end;
    end;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

procedure TfrmMainMkt.btnReFreshDataClick(Sender: TObject);
begin
  if not conn.Connected then
  begin
    try connect1Click(nil);
    except;
    end;
  end;
  fillptypebox();
  fillSubTypeBox();
  fillSourceBox();
end;

procedure TfrmMainMkt.btnresortClick(Sender: TObject);
begin
  // hidden button
  if mem.IsEmpty then
  begin
    showmessage('Please "GetData" first' + #10#13 + '(No Data to "re-sort").');
  end
  else
    SortMem();
end;

function TfrmMainMkt.GetConsortArg(): string;
var s: string; // restict to a certain consortium from paramstr(2)
begin
  result := '';
  s := trim(uppercase(paramstr(2))); // s/b consort code

  if s = '' then exit
  else
  begin
    if cbUseSummaryTable.Checked then
      result := ' and vs_consortium = "' + s + '" '
    else
      result := ' and voconsort = "' + s + '" ';
  end;
end;

function TfrmMainMkt.GetEMailArg(): string;
begin
  result := '';
  if rgRestrictByEmail.ItemIndex = 0 then // client e-mail restrict
    result := ' and (length(gc_email) > 0 and gc_email is not null) '
  else if rgRestrictByEmail.itemindex = 1 then
    result := ' and (length(gv_email) > 0 and gv_email is not null) ';
end;

procedure TfrmMainMkt.FocusCtry();
begin
  try
    pagecontrol1.ActivePage := tabStep1;
    EditCountry.SetFocus;
  except;
  end;
end;

procedure TfrmMainMkt.FocusCity();
begin
  try
    pagecontrol1.ActivePage := tabStep1;
    EditCity.SetFocus;
  except;
  end;
end;

procedure TfrmMainMkt.FocusOperator();
begin
  try
    pagecontrol1.ActivePage := tabStep1;
    EditOperator.SetFocus;
  except;
  end;
end;

procedure TfrmMainMkt.btnHelpClick(Sender: TObject);
var
  frmHelp: TfrmDSSHelp;
begin

  try
    frmHelp := TfrmDSSHelp.Create(Application);
    frmHelp.Show;

  except on e: exception do
    begin
      showmessage(e.Message + ' :Method --> TForm1.Button10Click');

    end;
  end;
end;
{begin // help button
  case ShellExecute(handle, 'Open', PChar('\\fileserver\update\help\dss\index.html'), nil, nil, SW_SHOWNORMAL) of
    0: Application.MessageBox(PChar('The operating system is out of memory or resources.'), 'Error', MB_OK);
    ERROR_FILE_NOT_FOUND: Application.MessageBox(PChar('The specified file was not found.'), 'Error', MB_OK);
    ERROR_PATH_NOT_FOUND: Application.MessageBox(PChar('The specified path was not found.'), 'Error', MB_OK);
    ERROR_BAD_FORMAT: Application.MessageBox(PChar('The EXE file is invalid (non-Win32 EXE or error in EXE image).'), 'Error', MB_OK);
    SE_ERR_ACCESSDENIED: Application.MessageBox(PChar('The operating system denied access to the specified file.'), 'Error', MB_OK);
    SE_ERR_ASSOCINCOMPLETE: Application.MessageBox(PChar('The filename association is incomplete or invalid.'), 'Error', MB_OK);
    SE_ERR_DDEBUSY: Application.MessageBox(PChar('The DDE transaction could not be completed because other DDE transactions were being processed.'), 'Error', MB_OK);
    SE_ERR_DDEFAIL: Application.MessageBox(PChar('The DDE transaction failed.'), 'Error', MB_OK);
    SE_ERR_DDETIMEOUT: Application.MessageBox(PChar('The DDE transaction could not be completed because the request timed out.'), 'Error', MB_OK);
    SE_ERR_DLLNOTFOUND: Application.MessageBox(PChar('The specified dynamic-link library was not found.'), 'Error', MB_OK);
    SE_ERR_NOASSOC: Application.MessageBox(PChar('There is no application associated with the given filename extension.'), 'Error', MB_OK);
    SE_ERR_OOM: Application.MessageBox(PChar('There was not enough memory to complete the operation.'), 'Error', MB_OK);
    SE_ERR_SHARE: Application.MessageBox(PChar('A sharing violation occurred.'), 'Error', MB_OK);
  else
    begin
       // all is OK
      //Application.MessageBox( PChar('General Help Error.'), 'Error', MB_OK );
    end;
  end; //case
end;}

procedure TfrmMainMkt.Button15Click(Sender: TObject);
begin
  PageControl1.ActivePage := tabOutput;
  application.ProcessMessages;
end;

procedure TfrmMainMkt.Button1Click(Sender: TObject);
begin
  SetMemFields(true); // create the mem table
  FillMemFields(); // fill it up
//PopulateHostCampIdMEM(cbspaShowDetail.Checked);
end;

procedure TfrmMainMkt.btnMarcelloReportClick(Sender: TObject);
var
  i: integer;
  IsOneSelected: boolean;
  strSQL, strReturns: string;
begin
  try

    if btnMarcelloReport.caption = 'Marcello Report' then
    begin
      btnMarcelloReport.caption := 'Cancel';

      IsOneSelected := false;
      strReturns := ''; //just a return value place holder
      fbolMarcello := true;
      cbspaSHowYearSysCtry.Visible := false;
  //cbspaSHowYearSysCtry.caption := 'Summary by campid';
      cbspaSHowYearSysCtryMstrOp.visible := false;
      cbspaSHowYearSysCtryMstrOpSource.visible := false;
      cbspaSHowYearSysCtryMstrOpSIPP.visible := false;
      cbspaSHowYearSysCtryMstrOpSIPPSrc.visible := false;



  //count the ticks
      for i := 0 to clbdb.Items.Count - 1 do
      begin
        if clbdb.Checked[i] then IsOneSelected := true;
      end;

  //blow out of the routine if there are no systems
      if not IsOneSelected then
      begin
        showmessage('Please Select the database(s).');
        exit;
      end;
    end
    else
    begin
      cbspaSHowYearSysCtry.caption := 'year, sys, ctry';
      cbspaSHowYearSysCtry.visible := true;
      cbspaSHowYearSysCtryMstrOp.visible := true;
      cbspaSHowYearSysCtryMstrOpSource.visible := true;
      cbspaSHowYearSysCtryMstrOpSIPP.visible := true;
      cbspaSHowYearSysCtryMstrOpSIPPSrc.visible := true;
      cbspaSHowYearSysCtry.Checked := false;
      cbspaSHowDetail.Checked := false;
      btnMarcelloReport.caption := 'Marcello Report';
    end;


  finally
    begin

    end;
  end;
end;

function TfrmMainMkt.RunMarcelloReport: string;
var
  sFILENAME, Reportname, strSQL, strReturns: string;
begin
  try
    try
      gintProcessType := 0;
    //combine the SQL
      strSQL := BuildMarcelloSQL(cbspaShowDetail.Checked);
      Memo1.Text := strSQL; //_Marcello_SQL;
    //pass the SQL off to be processed;
      SetMemFieldsMarcillo();
      xxxxx := 0;
      strReturns := FetchMarcelloMemTableData(strSQL);
    //showmessage(strSQL);
      gintProcessType := 1;
      xxxxx := 0;
      PopulateHostCampIdMEM(cbspaShowDetail.Checked);
    //were any records returned if not why run the report
      if trim(strReturns) = '0' then
      begin
        showmessage('No results were returned');
        exit;
      end;

    //if the place holder is empty we know we encountered an error.
      if trim(strReturns) = '' then
      begin
        showmessage('The query encountered an error and was terminated.');
        exit;
      end;

      strReturns := ''; //flush the buffer

      sFILENAME := SetDirectoryForTSUser();
      dbgrid3.DataSource := nil; //DataSourceMarcello;
      Reportname := 'Marcello_';
      xxxxx := 0;
      gintProcessType := 2;

   //turn the header back on
      ppHeaderBandMarcelloFull.Visible := true;
   //tell the report the range
      ppLabelMarcelloDateRangeFull.Text := 'Run dates: ' + datetostr(dtpStart.date)
        + ' - ' + datetostr(dtpEnd.date);

    //lets reuse this code as it has most of what I need.
      case rgSpecialReportsOutput.ItemIndex of
        0: //screen
          begin
            ppReportMarcelloFull.DeviceType := 'Screen';
            ppReportMarcelloFull.AllowPrintToFile := false;
            ppReportMarcelloFull.Print;
           //ppReportMarcello.DeviceType := 'Screen';
           //ppReportMarcello.AllowPrintToFile := false;
          //ppReportMarcello.Print;
          end;
        1: //csv
          begin

            if sd1.Execute then
            begin
              SaveCSV(DataSourceMarcello, sd1.FileName, '', ',');

           { SaveCSV(DataSourceMarcello, sd1.FileName, '', ',');}
            //SaveCSV_multiFile(DataSourceMarcello, sd1.FileName, '', '');
              globEMLFileName := sd1.FILENAME;
            end;


          end;
        2: //xls
          begin
            ppReportMarcelloFull.DeviceType := 'XLSXReport';
            ppReportMarcelloFull.TextFileName := sFILENAME + Reportname + 'XLSX';
            globEMLFileName := ppReportMarcelloFull.TextFileName;
            ppReportMarcelloFull.AllowPrintToFile := True;
            ppReportMarcelloFull.Print;

          //ppReportMarcello.DeviceType := 'XLSXReport';
          //ppReportMarcello.TextFileName := sFILENAME + Reportname + 'XLSX';
          //globEMLFileName := ppReportMarcello.TextFileName;
          //ppReportMarcello.AllowPrintToFile := True;
          //ppReportMarcello.Print;
          end;
      end;

      lblspecial.caption := 'done printing';


      result := '';

    except on e: exception do
      begin
        ShowMessage(e.message + #13#10 + ' Method: RunMarcelloReport');
        result := '';
      end;
    end;
  finally
    begin
      dbgrid3.DataSource := DataSourceMarcello;
      fbolMarcello := false;
      sb.Panels[2].Text := '';
      cbspaSHowYearSysCtry.caption := 'year, sys, ctry';
      cbspaSHowYearSysCtry.visible := true;
      cbspaSHowYearSysCtryMstrOp.visible := true;
      cbspaSHowYearSysCtryMstrOpSource.visible := true;
      cbspaSHowYearSysCtryMstrOpSIPP.visible := true;
      cbspaSHowYearSysCtryMstrOpSIPPSrc.visible := true;
      btnMarcelloReport.caption := 'Marcello Report';
      cbspaSHowYearSysCtry.Checked := false;
      cbspaSHowDetail.Checked := false;
    end;
  end;
end;



function TfrmMainMkt.FetchMarcelloMemTableData(sql: string): string;
begin
  try

    IfxQueryMarcello.SQL.Clear;
    IfxQueryMarcello.SQL.Text := trim(sql);
  //use date parms due to the Euro system
    IfxQueryMarcello.ParamByName('date1').AsDate := trunc(dtpstart.Date);
    IfxQueryMarcello.ParamByName('date2').AsDate := trunc(dtpend.Date);
    IfxQueryMarcello.Open;
    kbmMarcello.open;
    kbmMarcello.Edit;

  //add the field defs to the mem table
    SetMemFields(true); // create the mem table
 // kbmMarcello.cl
    if not IfxQueryMarcello.eof then
    begin
  ///////New Code Begin
      while not IfxQueryMarcello.eof do
      begin

        kbmMarcello.Insert;
           //jim 06/21/2012

        kbmMarcello.FieldByName('campid').AsString :=
          IfxQueryMarcello.FieldByName('campid').AsString;
        kbmMarcello.FieldByName('basesys').AsString :=
          IfxQueryMarcello.FieldByName('vs_basesys').AsString; //('basesys', ftString, 5, true);
        kbmMarcello.FieldByName('voucher').AsString :=
          IfxQueryMarcello.FieldByName('vs_voucher').AsString + '-' +
          IfxQueryMarcello.FieldByName('vs_release').AsString;
        kbmMarcello.FieldByName('confirmation').AsString :=
          IfxQueryMarcello.FieldByName('vs_op_confirmation').AsString; //('confirmation', ftString, 40, false);
        kbmMarcello.FieldByName('date_created').AsDateTime :=
          trunc(IfxQueryMarcello.FieldByName('vs_voucher_create_dt').AsDateTime); //('date_booked', ftDate, 0, false);
        kbmMarcello.FieldByName('date_of_svc').AsDateTime :=
          trunc(IfxQueryMarcello.FieldByName('vs_pu_dt').AsDateTime); //('date_of_svc', ftDate, 0, false);
        kbmMarcello.FieldByName('time_of_svc').AsString :=
          FormatDateTime('HH:MM:SS', frac(IfxQueryMarcello.FieldByName('vs_pu_time').AsDateTime)); //('time_of_svc', ftString, 25, true);
        kbmMarcello.FieldByName('retail_total').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_retail_total').AsFloat, -2); //('retail_total', ftFloat, 0, true);
        kbmMarcello.FieldByName('opdue_curr').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_operator_due_curr').AsFloat, -2); //('opdue_curr', ftFloat, 0, true);
        kbmMarcello.FieldByName('curr_type').AsString :=
          IfxQueryMarcello.FieldByName('vs_currency_operator').AsString; //('curr_type', ftString, 5, false);
        kbmMarcello.FieldByName('curr_exch_rate').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_exchange_rt').AsFloat, -3); //('curr_exch_rate', ftFloat, 0, false);
        kbmMarcello.FieldByName('sipp').AsString :=
          IfxQueryMarcello.FieldByName('vs_sipp').AsString; // sipp//('sipp', ftString, 10, false);
        kbmMarcello.FieldByName('oper_car_code').AsString :=
          IfxQueryMarcello.FieldByName('vs_sipp_type').AsString; // sipp//('oper_car_code', ftString, 30, false); // vopcomt1
        kbmMarcello.FieldByName('date_of_transaction').AsDateTime :=
          trunc(IfxQueryMarcello.FieldByName('vs_paid_dt').AsDateTime); //('date_of_transaction', ftDate, 0, false);
        kbmMarcello.FieldByName('commission').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_comm_tot').AsFloat, -2); //('commission', ftFloat, 0, false);
        kbmMarcello.FieldByName('profit').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_profit').AsFloat, -2); //('profit', ftFloat, 0, false);
        kbmMarcello.FieldByName('duration').AsFloat :=
          RoundTo(IfxQueryMarcello.FieldByName('vs_duration').AsFloat, -2); //('duration', ftFloat, 0, false);
        kbmMarcello.FieldByName('lead_days_create_pu').AsFloat :=
          trunc(IfxQueryMarcello.FieldByName('vs_days_between_create_pu').AsFloat); //('lead_days_create_pu', ftFloat, 0, false);
        kbmMarcello.FieldByName('dt_year').AsString :=
          trim(IfxQueryMarcello.FieldByName('dt_year').AsString); // add 04/27/2011 dt_year string
        kbmMarcello.FieldByName('lead_days_pdpu').AsFloat :=
          trunc(IfxQueryMarcello.FieldByName('lead_days_pdpu').AsFloat);
        kbmMarcello.FieldByName('iata').AsString :=
          IfxQueryMarcello.FieldByName('vs_iata').AsString; //('iata', ftstring, 9, false);
        kbmMarcello.FieldByName('country_pu').AsString :=
          IfxQueryMarcello.FieldByName('vs_pu_ctry').Asstring; //('country_pu', ftstring, 3, false); //vopctry1
        kbmMarcello.FieldByName('city_pu').AsString :=
          IfxQueryMarcello.FieldByName('vs_pu_city').AsString; //('city_pu', ftstring, 20, false); // vopcity1
        kbmMarcello.FieldByName('loc_pu').AsString :=
          IfxQueryMarcello.FieldByName('vs_pu_loc').AsString; //('loc_pu', ftstring, 2, false); // voploc1
        kbmMarcello.FieldByName('operator').AsString :=
          IfxQueryMarcello.FieldByName('vs_operator_code').AsString; //('operator', ftstring, 20, false); // vopid
        kbmMarcello.FieldByName('consortium').AsString :=
          IfxQueryMarcello.FieldByName('vs_consortium').AsString; //('consortium', ftString, 10, false); // voconsort
            // must get cc_name from cc_ref table on the join ...
        kbmMarcello.FieldByName('cc_name').AsString :=
          IfxQueryMarcello.FieldByName('cc_name').AsString; //('cc_name', ftString, 18, false); //payment type
        kbmMarcello.FieldByName('vcspecrequest').AsString :=
          IfxQueryMarcello.FieldByName('vs_comments').AsString; //('vcspecrequest', ftString, 100, false); // special requests
        kbmMarcello.FieldByName('service_type').AsString :=
          IfxQueryMarcello.FieldByName('vs_product_type_tag').AsString; //service_type
        kbmMarcello.FieldByName('source').AsString :=
          IfxQueryMarcello.FieldByName('vs_subtype').AsString; //('source', ftString, 2, false); // vopsubt
        kbmMarcello.FieldByName('c_fullname').AsString :=
          trim(uppercase(IfxQueryMarcello.FieldByName('gc_lastname').AsString)) + ', ' +
          trim(uppercase(IfxQueryMarcello.FieldByName('gc_firstname').AsString)) + ' ' +
          trim(uppercase(IfxQueryMarcello.FieldByName('gc_initial').AsString));
        kbmMarcello.FieldByName('c_prefix').AsString :=
          IfxQueryMarcello.FieldByName('gc_surname').AsString; //('c_prefix', ftString, 8, false); // gc_surname is used for Mr.|Mrs. etc
        kbmMarcello.FieldByName('c_addr1').AsString :=
          IfxQueryMarcello.FieldByName('gc_addr1').AsString; //('c_addr1', ftString, 60, false);
        kbmMarcello.FieldByName('c_addr2').AsString :=
          IfxQueryMarcello.FieldByName('gc_addr2').AsString; //('c_addr2', ftString, 60, false);
        kbmMarcello.FieldByName('c_prov').AsString :=
          IfxQueryMarcello.FieldByName('gc_prov').AsString; //('c_prov', ftString, 10, false);
        kbmMarcello.FieldByName('c_city').AsString :=
          IfxQueryMarcello.FieldByName('gc_city').AsString; //('c_city', ftString, 50, false);
        kbmMarcello.FieldByName('c_postal').AsString :=
          IfxQueryMarcello.FieldByName('gc_postcode').AsString; //('c_postal', ftString, 20, false);
        kbmMarcello.FieldByName('c_country').AsString :=
          IfxQueryMarcello.FieldByName('gc_ctry').AsString;
        kbmMarcello.FieldByName('c_email').AsString :=
          trim(uppercase(IfxQueryMarcello.FieldByName('gc_email').AsString)); //('c_email', ftString, 250, false);
        kbmMarcello.FieldByName('c_phone').AsString :=
          IfxQueryMarcello.FieldByName('gc_phone').AsString; //('c_phone', ftString, 40, false);
        kbmMarcello.FieldByName('c_age').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_client_age').AsInteger; //('c_age', ftInteger, 0, false); //voiamt4 = age
        kbmMarcello.FieldByName('vcount').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_count_voucher').AsInteger;
            // add new fields here from summary table and g_vendor.........
        kbmMarcello.FieldByName('active_history').AsString :=
          IfxQueryMarcello.FieldByName('vs_active_history').AsString; //('active_history', ftString, 1, false);
        kbmMarcello.FieldByName('p_or_q').AsString :=
          IfxQueryMarcello.FieldByName('vs_p_or_q').AsString; //('p_or_q', ftString, 1, false);
            //may be trouble some
        kbmMarcello.FieldByName('payment_type').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_payment_type').AsInteger; //('payment_type', ftInteger, 0, false);
        kbmMarcello.FieldByName('fp_pp').AsString :=
          IfxQueryMarcello.FieldByName('vs_fp_pp').AsString; //('fp_pp', ftString, 2, false);
        kbmMarcello.FieldByName('plus_basic').AsString :=
          IfxQueryMarcello.FieldByName('vs_plus_basic').AsString; //('plus_basic', ftString, 1, false);
        kbmMarcello.FieldByName('rid').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_rid').AsInteger; //('rid', ftInteger, 0, false);
        kbmMarcello.FieldByName('auth_code').AsString :=
          IfxQueryMarcello.FieldByName('vs_auth_code').AsString; //('auth_code', ftString, 10, false);
        kbmMarcello.FieldByName('product_type').AsString :=
          IfxQueryMarcello.FieldByName('vs_product_type').AsString; //('product_type', ftString, 10, false);
        kbmMarcello.FieldByName('business_source').AsString :=
          IfxQueryMarcello.FieldByName('vs_business_source').AsString; //('business_source', ftString, 10, false);
        kbmMarcello.FieldByName('do_ctry').AsString :=
          IfxQueryMarcello.FieldByName('vs_do_ctry').AsString; //('do_ctry', ftString, 2, false);
        kbmMarcello.FieldByName('do_city').AsString :=
          IfxQueryMarcello.FieldByName('vs_do_city').AsString; //('do_city', ftString, 50, false);
        kbmMarcello.FieldByName('do_loc').AsString :=
          IfxQueryMarcello.FieldByName('vs_do_loc').AsString; //('do_loc', ftString, 2, false);
        kbmMarcello.FieldByName('updated_dt').AsDateTime :=
          trunc(IfxQueryMarcello.FieldByName('vs_updated_dt').AsDateTime); //('updated_dt', ftDate, 0, false);
        kbmMarcello.FieldByName('do_dt').AsDateTime :=
          trunc(IfxQueryMarcello.FieldByName('vs_do_dt').AsDateTime); //('do_dt', ftDate, 0, false);
        kbmMarcello.FieldByName('do_time').AsString :=
          FormatDateTime('HH:MM:SS', frac(IfxQueryMarcello.FieldByName('vs_do_time').AsDateTime)); //('do_time', ftString, 25, false);
         kbmMarcello.FieldByName('op_rate_code').AsString :=
          IfxQueryMarcello.FieldByName('vs_op_rate_code').AsString;//('op_rate_code', ftString, 15, false);
         kbmMarcello.FieldByName('bus_account_number').AsString :=
          IfxQueryMarcello.FieldByName('vs_Vcflighttckt').AsString;
          kbmMarcello.FieldByName('car_name').AsString :=
          IfxQueryMarcello.FieldByName('vs_carmname').AsString;
        kbmMarcello.FieldByName('sipp_description').AsString :=
          IfxQueryMarcello.FieldByName('vs_sipp_description').AsString; //('sipp_description', ftString, 100, false);
        kbmMarcello.FieldByName('num_passengers').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_num_passengers').AsInteger;
        kbmMarcello.FieldByName('comm_due').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_comm_due').AsFloat; //('comm_due', ftFloat, 0, false);
        kbmMarcello.FieldByName('comm_percent').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_comm_percent').AsFloat; //('comm_percent', ftFloat, 0, false);
        kbmMarcello.FieldByName('home_curr').AsString :=
          IfxQueryMarcello.FieldByName('vs_home_curr').AsString; //('home_curr', ftString, 3, false);
        kbmMarcello.FieldByName('vat_tax_rt').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_vat_tax_rt').AsFloat; //('vat_tax_rt', ftFloat, 0, false);
        kbmMarcello.FieldByName('home_ctry').AsString :=
          IfxQueryMarcello.FieldByName('vs_home_ctry').AsString; //('home_ctry', ftString, 2, false);
        kbmMarcello.FieldByName('base_rate_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_base_rate_retail').AsFloat; //('base_rate_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('discount_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_discount_retail').AsFloat; //('discount_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('insurance_tot_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_insurance_tot_retail').AsFloat; //('insurance_tot_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('tax_tot_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_tax_tot_retail').AsFloat; //('tax_tot_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('pu_fee_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_pu_fee_retail').AsFloat; //('pu_fee_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('do_fee_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_do_fee_retail').AsFloat; //('do_fee_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('xtra_fee_retail').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_xtra_fee_retail').AsFloat; //('xtra_fee_retail', ftFloat, 0, false);
        kbmMarcello.FieldByName('base_rate_wholesale').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_base_rate_wholesale').AsFloat; //('base_rate_wholesale', ftFloat, 0, false);
        kbmMarcello.FieldByName('insurance_tot_wholesale').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_insurance_tot_wholesale').AsFloat; //('insurance_tot_wholesale', ftFloat, 0, false);
        kbmMarcello.FieldByName('tax_tot_wholesale').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_tax_tot_wholesale').AsFloat; //('tax_tot_wholesale', ftFloat, 0, false);
        kbmMarcello.FieldByName('pu_fee_wholesale').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_pu_fee_wholesale').AsFloat; //('pu_fee_wholesale', ftFloat, 0, false);
        kbmMarcello.FieldByName('do_fee_wholesale').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_do_fee_wholesale').AsFloat; //('do_fee_wholesale', ftFloat, 0, false);
        kbmMarcello.FieldByName('wholesale_total').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_wholesale_total').AsFloat; //('wholesale_total', ftFloat, 0, false);
        kbmMarcello.FieldByName('operator_due').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_operator_due').AsFloat; //('operator_due', ftFloat, 0, false);
        kbmMarcello.FieldByName('deposit_amt').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_deposit_amt').AsFloat; //('deposit_amt', ftFloat, 0, false);
        kbmMarcello.FieldByName('waived_amt').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_waived_amt').AsFloat; //('waived_amt', ftFloat, 0, false);
        kbmMarcello.FieldByName('deposit_after_waive').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_deposit_after_waive').AsFloat; //('deposit_after_waive', ftFloat, 0, false);
        kbmMarcello.FieldByName('due_at_pu').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_due_at_pu').AsFloat; //('due_at_pu', ftFloat, 0, false);
        kbmMarcello.FieldByName('deferred_amt').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_deferred_amt').AsFloat; //('deferred_amt', ftFloat, 0, false);
        kbmMarcello.FieldByName('gross_revenue').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_gross_revenue').AsFloat; //('gross_revenue', ftFloat, 0, false);
        kbmMarcello.FieldByName('operator_cost').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_operator_cost').AsFloat; //('operator_cost', ftFloat, 0, false);
        kbmMarcello.FieldByName('count_new').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_count_new').AsInteger; //('count_new', ftInteger, 0, false);
        kbmMarcello.FieldByName('count_cxl').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_count_cxl').AsInteger; //('count_cxl', ftInteger, 0, false);
        kbmMarcello.FieldByName('count_change').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_count_change').AsInteger; //('count_change', ftInteger, 0, false);
        kbmMarcello.FieldByName('direct_or_ta').AsString :=
          IfxQueryMarcello.FieldByName('vs_direct_or_ta').AsString; //('direct_or_ta', ftString, 2, false);
          kbmMarcello.FieldByName('member_account').AsString :=
        IfxQueryMarcello.FieldByName('vs_voiamt7').AsString;
        kbmMarcello.FieldByName('gv_name').AsString :=
          IfxQueryMarcello.FieldByName('gv_name').AsString; //('gv_name', ftString, 30, false);
        kbmMarcello.FieldByName('gv_legal_name').AsString :=
          IfxQueryMarcello.FieldByName('gv_legal_name').AsString; //('gv_legal_name', ftString, 30, false);
        kbmMarcello.FieldByName('gv_contact_name').AsString :=
          IfxQueryMarcello.FieldByName('gv_contact_name').AsString; //('gv_contact_name', ftString, 30, false);
        kbmMarcello.FieldByName('gv_addr1').AsString :=
          IfxQueryMarcello.FieldByName('gv_addr1').AsString; //('gv_addr1', ftString, 30, false);
        kbmMarcello.FieldByName('gv_addr2').AsString :=
          IfxQueryMarcello.FieldByName('gv_addr2').AsString; //('gv_addr2', ftString, 30, false);
        kbmMarcello.FieldByName('gv_addr3').AsString :=
          IfxQueryMarcello.FieldByName('gv_addr3').AsString; //('gv_addr3', ftString, 30, false);
        kbmMarcello.FieldByName('gv_addr4').AsString :=
          IfxQueryMarcello.FieldByName('gv_addr4').AsString; //('gv_addr4', ftString, 30, false);
        kbmMarcello.FieldByName('gv_city').AsString :=
          IfxQueryMarcello.FieldByName('gv_city').AsString; //('gv_city', ftString, 18, false);
        kbmMarcello.FieldByName('gv_prov').AsString :=
          IfxQueryMarcello.FieldByName('gv_prov').AsString; //('gv_prov', ftString, 3, false);
        kbmMarcello.FieldByName('gv_postal').AsString :=
          IfxQueryMarcello.FieldByName('gv_postal').AsString; //('gv_postal', ftString, 10, false);
        kbmMarcello.FieldByName('gv_country').AsString :=
          IfxQueryMarcello.FieldByName('gv_country').AsString; //('gv_country', ftString, 30, false);
        kbmMarcello.FieldByName('gv_phone_prefix').AsString :=
          IfxQueryMarcello.FieldByName('gv_phone_prefix').AsString; //('gv_phone_prefix', ftString, 6, false);
        kbmMarcello.FieldByName('gv_phone_num').AsString :=
          IfxQueryMarcello.FieldByName('gv_phone_num').AsString; //('gv_phone_num', ftString, 15, false);
        kbmMarcello.FieldByName('gv_fax_prefix').AsString :=
          IfxQueryMarcello.FieldByName('gv_fax_prefix').AsString; //('gv_fax_prefix', ftString, 6, false);
        kbmMarcello.FieldByName('gv_fax_num').AsString :=
          IfxQueryMarcello.FieldByName('gv_fax_num').AsString; //('gv_fax_num', ftString, 15, false);
        kbmMarcello.FieldByName('gv_email').AsString :=
          IfxQueryMarcello.FieldByName('gv_email').AsString; //('gv_email', ftString, 255, false);
        kbmMarcello.FieldByName('promo1').AsString :=
          IfxQueryMarcello.FieldByName('vs_promo1').asstring; //('promo1', ftString, 10, false);
        kbmMarcello.FieldByName('promo2').AsString :=
          IfxQueryMarcello.FieldByName('vs_promo2').asstring; //('promo2', ftString, 10, false);
        kbmMarcello.FieldByName('promo3').AsString :=
          IfxQueryMarcello.FieldByName('vs_promo3').asstring; //('promo3', ftString, 10, false);
            //marcello new
        kbmMarcello.FieldByName('gc_ctry').AsString :=
          IfxQueryMarcello.FieldByName('client_ctry').AsString; //('gc_ctry', ftString, 25, false);
        kbmMarcello.FieldByName('vs_retail_total').AsFloat :=
          IfxQueryMarcello.FieldByName('vs_retail_total').asFloat; //('gc_ctry', ftString, 25, false);
        kbmMarcello.FieldByName('vs_profit').AsFloat :=
          IfxQueryMarcello.FieldByName('profit').asFloat; //('vs_profit', ftFloat, 0, false);
        kbmMarcello.FieldByName('vs_count_voucher').AsInteger :=
          IfxQueryMarcello.FieldByName('vs_count_voucher').AsInteger; //('vs_count_voucher', ftInteger, 0, false);
        kbmMarcello.FieldByName('gv_name').AsString :=
          IfxQueryMarcello.FieldByName('vendor_name').AsString; //('promo3', ftString, 10, false);
        kbmMarcello.FieldByName('campid').AsString :=
          IfxQueryMarcello.FieldByName('campid').AsString; //('promo3', ftString, 10, false);
        kbmMarcello.FieldByName('campaign').AsString :=
          IfxQueryMarcello.FieldByName('campaign').AsString; //('promo3', ftString, 10, false);
        kbmMarcello.FieldByName('host').AsString :=
          IfxQueryMarcello.FieldByName('host').AsString; //('promo3', ftString, 10, false);
        kbmMarcello.FieldByName('pay_dt_year').AsString :=
          IfxQueryMarcello.FieldByName('pay_dt_year').AsString; //('pay_dt_year', ftDate, 0, false);

        kbmMarcello.Post;
        IfxQueryMarcello.Next;
      end;
  //////New Code End




  //lets load our table with the dataset
  //kbmMarcello.LoadFromDataSet(IfxQueryMarcello, [mtcpoStructure]);

      result := IntToStr(kbmMarcello.RecordCount);
    end
    else
    begin
      result := '0';
    end;

  except on e: exception do
    begin
      ShowMessage(e.message + #13#10 + ' Method: FetchMarcelloMemTableData');
      result := '';
    end;
  end;
end;

function TfrmMainMkt.BuildMarcelloSQL(isDetail: boolean): string;
var
  strSQL, strSQLInClause, strMSystems, strOldSwitches, strCountMode: string;
  i: integer;
  IsOneSelected,
    DoIt: boolean;
begin
  try
    result := '';
    strMSystems := '';
    strCountMode := '';

    //build the in
    strSQLInClause := ' v.vs_basesys IN(';
    //add to the list reuse Jon code
    for i := 0 to clbdb.Items.Count - 1 do
    begin
      if clbdb.Checked[i] then
      begin
        DoIt := true;
        strSQLInClause := strSQLInClause + '"' + clbdb.items[i] + ''',';
        strMSystems := strMSystems + clbdb.items[i] + ',';

      end;
    end;



    //reuse Jon code.
    if DoIt then
    begin
      // remove last comma
      strMSystems := copy(strMSystems, 1, length(strMSystems) - 1);
      // remove last comma and replace with ')'
      strSQLInClause := copy(strSQLInClause, 1, length(strSQLInClause) - 1);
      strSQLInClause := strSQLInClause + ')';
    end;

    //added count mode
    if rgCountMethod.ItemIndex = 0 then // regular count vs logical count
    begin
      strCountMode := ' and sum(v.vs_count_voucher) as vs_count_voucher  ';
    end
    else
    begin
      strCountMode := ' and sum(v.vs_count_logical) as vs_count_voucher  ';
    end;


    if isDetail {cbspaShowDetail.Checked} then
    begin
      strSQL := _MARCELLO_SQL_DETAIL_SELECT + _MARCELLO_SQL_FROM + strSQLInClause + strCountMode + _Marcello_SQL_SECOND;
      ppGroupHeaderBandMarcelloCampaign.Visible := false;
      ppGroupFooterBandMarcelloCampaign.Visible := false;
      ppDetailBandMarcelloDetail.visible := cbspaShowDetail.checked;
      ppHeaderBandMarcello.visible := cbspaShowDetail.checked;
      ppLabelMarcelloReportNameFull.caption := 'Marcello Detail Report';
      ppLabelMarcelloReportDate.Caption := formatdatetime('c', now);
      ppLabelMarcelloSystemsFull.Text := 'System(s): ' + strMSystems;
    end
    else if cbspaSHowYearSysCtry.checked then
    begin
      strSQL := _MARCELLO_SQL_SUMMARY_SELECT1
        + _MARCELLO_SQL_FROM
        + strSQLInClause
        + _Marcello_SQL_SECOND
        + _MARCELLO_GROUP_BY1
        + _MARCELLO_SQL_ORDER_BY_CAMPID;
     //strSQL :=  _MARCELLO_SQL_DETAIL_SELECT + _MARCELLO_SQL_FROM + strSQLInClause + _Marcello_SQL_SECOND;
     //we want the detail
      ppGroupHeaderBandMarcelloCampaign.Visible := true;
      ppGroupFooterBandMarcelloCampaign.Visible := true;
      ppDetailBandMarcelloDetail.visible := cbspaShowDetail.checked;
      ppHeaderBandMarcello.visible := cbspaShowDetail.checked;
      ppLabelMarcelloReportNameDetail.caption := 'Marcello Summary Report By Campaign ID';
      ppLabelMarcelloReportDateDetail.Caption := formatdatetime('c', now);
    end
    else
    begin
      strSQL := _MARCELLO_SQL_DETAIL_SELECT + _MARCELLO_SQL_FROM + strSQLInClause + _Marcello_SQL_SECOND;
     //we want the detail
     //ppHeaderBandMarcello.visible  := cbspaShowDetail.checked;
     //ppDetailBandMarcelloDetail.visible  := cbspaShowDetail.checked;
     //ppGroupFooterBandMarcelloCampaign.Visible := false;
     //ppGroupHeaderBandMarcelloCampaign.Visible := false;
      ppLabelMarcelloReportNameFull.caption := 'Marcello Detail Report';
     //ppLabelMarcelloReportDate.Caption := formatdatetime('c', now);
    end;

    result := strSQL;
  except on e: exception do
    begin
      ShowMessage(e.message + #13#10 + ' Method: FetchMarcelloMemTableData');
      result := '';
    end;
  end;
end;

procedure TfrmMainMkt.btnAllPayTypesClick(Sender: TObject);
begin
  SetCLB(clbpaytype2, true);
end;

procedure TfrmMainMkt.btnNoPayTypesClick(Sender: TObject);
begin
  SetCLB(clbpaytype2, false);
end;

procedure TfrmMainMkt.btnAllSippsList1Click(Sender: TObject);
begin
  SetCLB(clbsipp1, true);
end;

procedure TfrmMainMkt.btnNoSippsList1Click(Sender: TObject);
begin
  SetCLB(clbsipp1, false);
end;

procedure TfrmMainMkt.btnCSV_HTMLClick(Sender: TObject);
var TempFN, sFILENAME: string;
begin
  DoOutPut(btnCSV_HTML, 'HTML', GlobFName);
end;

procedure TfrmMainMkt.btnDumpGridClick(Sender: TObject);
var temp: string;
begin
  temp := '';
  temp := InputBox('enter title', 'enter title of report', 'repeat business report');
  saveACSV(dbgrid3.datasource, temp);
end;

procedure TfrmMainMkt.btnTrackClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  LatestRunDate := 0;

  initQ_CloseClear();
  theSQL := 'Select * From dss_track_reports ';
  theSQL := theSQL + ' where date(dtr_run_Date) between :date1 and :date2 ';
  theSQL := theSQL + ' order by dtr_user_name, dtr_run_date, dtr_report_name';
  q1.SQL.Text := theSQL;

  q1.ParamByName('date1').AsDateTime := trunc(dtpTrackReportsStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpTrackReportsEnd.DateTime);

  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);

  reportTrack.Reset;

  if cbSumOnly.Checked then
  begin
    ppHeaderBand1.Visible := false;
    ppDetailBand1.Visible := false;
    ppLine2.Visible := false;
  end
  else
  begin
    ppHeaderBand1.Visible := true;
    ppDetailBand1.Visible := true;
    ppLine2.Visible := true;
  end;

  OpenQ();
  reportTrack.Print;
end;

procedure TfrmMainMkt.FillOMList(var InList: TStringList);
var Q: TFDQuery;
begin
  try
    q := GetIDACQuery(conn);
    try
      InList.Clear;
      q.SQL.Clear;
      q.SQL.add('Select distinct trim(upper(rc_operator_oper_code)) || "=" || trim(upper(rc_operator_master)) ');
      q.SQL.add('from rc_operators where rc_operator_oper_code <> rc_operator_master ');
      q.SQL.add('and (rc_operator_oper_code is not null and rc_operator_oper_code > "")');
      q.SQL.add('and (rc_operator_master is not null and rc_operator_master > "")');
      memo1.Lines.Add('fill master list sql: ');
      memo1.Lines.add(q.SQL.text);

      q.Open;
      q.First;
      while not q.Eof do
      begin
        InList.Add(trim(uppercase(q.fields[0].asstring)));
        q.Next;
      end;
      q.Close;
    except on e: exception do
      begin
        memo1.Lines.add('fail fill operator master list --> ' + e.Message);
      end;
    end;
  finally
    if assigned(q) then FreeAndNIl(q);
  end;
end;

procedure TfrmMainMkt.btnytdClick(Sender: TObject);
var
  theSQL, ListSQL, reportTitle, reportName, temp, temp2: string;
  i, ii, x, z: Integer;
  yearList, SystemList, OperatorMasterList: TStringList;
  crap: string;
  asdf: boolean;
begin // to be edited

  temp := '';
  if not MSTR_List_Dates_OK(temp) then
  begin
    if messageDLG(temp + #10#13 + ' Continue anyway?',
      mtWarning, [mbYes, mbNo], 0) = mrNO then exit;
  end;
  temp := '';
  memo1.Clear; // NEXT NEXT NEXT
  yearList := TStringList.Create;
  systemList := TStringLIst.create;
  OperatorMasterList := TStringList.create;

  try
    conn.Close;
    initQ_CloseClear(); // clear Q1.sql and open conn
    theSQL := '';
    ListSQL := '';
    reportTitle := '';
    ReportName := '';
    bKILL := false;

    if cbUseGender.Checked then
    begin
      showmessage('gender filter can not be used for this report.');
      exit;
    end;

    try
      SetUpMemTable('YTD', memSpecialReps);
      memo1.Lines.add('SetUpMemTable for "YTD" report OK');
    except on e: exception do
      begin
        memo1.Lines.add('SetUpMemTable YTD --> ' + e.message);
        showmessage('SetUpMemTableErr YTD --> ' + e.message);
        exit;
      end;
    end;

    i := 0;
    q2.Close;
    q2.SQL.Clear;
    q1.Close;
    q1.SQL.Clear;

    try
      theSQL := BuildSQL(btnytd);
      ListSQL := theSQL;
      theSQL := theSQL + ' INTO TEMP assssssss WITH NO LOG;';
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    try // just getting distinct a list of operators
         // so parse/replace the where and the group by
      temp := '';
      temp2 := '';

      temp := 'Select DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
        'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country ';

      // take everything out to the WHERE
      i := 0;
      i := pos('FROM g_voucher_summary WHERE', ListSQL);
      if i = 0 then raise(exception.Create('can''t parse Oper/Ctry List SQL'));
      temp2 := copy(ListSQL, i, 1500);
      temp2 := trim(temp2);

      // Remove Group BY
      i := 0;
      i := pos('group by 1,2,3,4', temp2);
      if i = 0 then raise(exception.Create('can''t parse Oper/Ctry List SQL II'));
      temp2 := copy(temp2, 0, i - 1);
      temp2 := trim(temp2);

      ListSQL := temp + temp2 + ' Group by 1,2 Order by 1,2 INTO TEMP templist WITH NO LOG;';
      ListSQL := Trim(ListSQL);

      { ~~ should look like this:
        Select
        DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator,
        DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country
        FROM g_voucher_summary
        WHERE   vs_basesys in("AUS","CANA","DFW","EURO","HOHO","HOT","KEM","LAC","NEWZ","SAF","UK","USA") and
        (  vs_paid_dt BETWEEN date('03/01/2007') and date('03/31/2007')  OR
            vs_paid_dt BETWEEN date('03/01/2008') and date('03/31/2008')  OR
            vs_paid_dt BETWEEN date('03/01/2009') and date('03/31/2009')
        )
        Group by 1,2 Order by 1,2 -- INTO TEMP templist WITH NO LOG;
      }
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    GlobFName := 'YTD';
    Build_RName_RTitle(btnytd, Reportname, ReportTitle);
    addToRunLog2(Reportname, ReportTitle); // add back in later

    q1.SQL.Text := theSQL;
    q2.SQL.Text := ListSQL;

    // add params because euro date bombs when we call datetoStr()
    if sender <> btnGetDataWMySQL then
    begin
      q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
      q1.ParamByName('date2').AsDate := trunc(dtpend.Date);
      // pad dates for q2:
      q2.ParamByName('date1').AsDate := trunc(dtpMasterListStart.Date);
      q2.ParamByName('date2').AsDate := trunc(dtpMasterListEnd.Date);
    end;

    if cbUseRange2.Checked then
    begin
      q1.ParamByName('dtRange2Start').AsDate := trunc(dtprange2start.Date);
      q1.ParamByName('dtRange2End').AsDate := trunc(dtprange2end.Date);
      // pad dates for q2:
      q2.ParamByName('dtRange2Start').AsDate := trunc(dtpMasterListStart.date);
      q2.ParamByName('dtRange2End').AsDate := trunc(dtpMasterListEnd.date);
    end;

    if cbUseRange3.Checked then
    begin
      q1.ParamByName('dtRange3Start').AsDate := trunc(dtprange3start.Date);
      q1.ParamByName('dtRange3End').AsDate := trunc(dtprange3end.Date);
      // pad dates for q2
      q2.ParamByName('dtRange3Start').AsDate := trunc(dtpMasterListStart.date);
      q2.ParamByName('dtRange3End').AsDate := trunc(dtpMasterListEnd.date);
    end;

    if cbUsePUDate.Checked then
    begin // paramatize this to handle EURO date
      q1.ParamByName('date3').AsDate := dtpPUStart.Date;
      q1.ParamByName('date4').AsDate := dtpPUEND.Date;
    end;

    // add 5/05/2010
    if cbUseCreateDate.Checked then
    begin
      q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
      q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
    end;

    if cbUseDropDate.Checked then // use drop date filters
    begin
      q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
      q1.ParamByName('date6').AsDate := dtpDropDtend.date;
    end;

    // end add params
    memo1.Lines.add(' ------ theSQL: ------ ');
    memo1.Lines.add(theSQL);
    memo1.Lines.add(' ------ date params: ----- ');
    for i := 0 to q1.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

    // q2
    memo1.Lines.add(' ------ ListSQL: ------ ');
    memo1.Lines.add(ListSQL);
    memo1.Lines.add(' ------ ListSQL date params: ----- ');
    for i := 0 to q2.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q2.Params[i].name + ' ' + q2.params[i].asstring);

    i := 0;
    lblspecial.caption := 'getting data';
    application.ProcessMessages;
    try
      q1.ExecSQL;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + ' into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    memo1.Lines.Add(IntToStr(i) + ' records added to "temp" table.');
    lblspecial.caption := IntToStr(i) + ' records added to "temp" table.';
    application.ProcessMessages;

    i := 0;
    lblspecial.caption := 'getting List data';
    application.ProcessMessages;
    try
      q2.ExecSQL;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + 'List SQL into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    memo1.Lines.Add(IntToStr(i) + ' List records added to "temp" table.');
    lblspecial.caption := IntToStr(i) + ' records added to "temp" table.';
    application.ProcessMessages;

    try
      q1.Close;
      q1.SQL.Clear; // get list of years from dataset first
      q1.SQL.Add('select distinct dt_year From assssssss;');

      lblspecial.caption := 'getting count for list of years';
      application.ProcessMessages;

      q1.Open;
      q1.First;
      q1.Last;
      i := 0;
      i := q1.RecordCount;
      memo1.Lines.Add('count for list of years: [' + IntToStr(i) + ']');
      application.ProcessMessages;

      yearList.Clear;
      q1.First;
      while not q1.Eof do
      begin
        yearList.Add(q1.Fields[0].asstring);
        q1.Next;
      end;

      //  system list
      q1.Close;
      q1.SQL.Clear; // get list of years from dataset
      q1.SQL.Add('select distinct system From assssssss;');
      lblspecial.caption := 'getting count for list of systems';
      application.ProcessMessages;
      q1.Open;
      q1.First;
      q1.Last;
      i := 0;
      i := q1.RecordCount;
      memo1.Lines.Add('count for list of systems: [' + IntToStr(i) + ']');
      application.ProcessMessages;

      systemList.Clear;
      q1.First;
      while not q1.Eof do
      begin
        systemList.Add(q1.Fields[0].asstring);
        q1.Next;
      end;
      // system list end

      q2.Close;
      q2.SQL.Clear; // get list of years from dataset first
      q2.SQL.Add('select * From templist order by operator, country;');

      memo1.Lines.Add('getting count for list of operators');
      application.ProcessMessages;

      q2.Open;
      q2.First;
      q2.Last;
      i := 0;
      i := q2.RecordCount;

      memo1.Lines.Add('!!! count for oper/ctry list: [' + IntToStr(i) + ']');
      application.ProcessMessages;
    except on e: exception do
      begin
        memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
      end;
    end;

    lblspecial.Caption := 'setting up temp table';
    application.ProcessMessages;
    try
      // should be active already. SetUpMemTable() called above does this
      memSpecialReps.EmptyTable;

      // fill operator master list here:
      memo1.Lines.Add('filling operator masterlist');
      FillOMList(OperatorMasterList);
      memo1.Lines.Add('******** master list ************ ');
      memo1.Lines.Add(OperatorMasterList.Text);
      memo1.Lines.Add('******** master list done ************ ');

      for i := 0 to yearlist.Count - 1 do
      begin
        for ii := 0 to systemList.Count - 1 do
        begin
          q2.First;
          x := 0;
          z := 0;
          while not q2.Eof do
          begin
            try
              memSpecialReps.Insert;
              memSpecialReps.FieldByName('year').asstring := trim(uppercase(yearlist[i]));
              memSpecialReps.FieldByName('system').asstring := trim(uppercase(systemList[ii]));
              memSpecialReps.FieldByName('operator').asstring := trim(uppercase(q2.FieldByName('operator').AsString));

              crap := '';
              crap := OperatorMasterList.Values[trim(uppercase(q2.FieldByName('operator').AsString))];

              if crap > '' then
              begin
                memSpecialReps.FieldByName('operator_master').AsString := crap;
              end
              else
              begin
                memSpecialReps.FieldByName('operator_master').asstring :=
                  trim(uppercase(q2.FieldByName('operator').AsString));
              end;

              memSpecialReps.FieldByName('country').asstring := trim(uppercase(q2.FieldByName('country').AsString));
              memSpecialReps.FieldByName('bookings').AsInteger := 0;
              memSpecialReps.FieldByName('ave_duration').AsFloat := 0.00;
              memSpecialReps.FieldByName('ave_profit').AsFloat := 0.00;
              memSpecialReps.FieldByName('total_days').asinteger := 0;
              memSpecialReps.FieldByName('ave_op_due').AsFloat := 0.00;
              memSpecialReps.FieldByName('total_op_due').AsFloat := 0.00;
              memSpecialReps.FieldByName('total_duration').AsFloat := 0.00;
              memSpecialReps.FieldByName('total_profit').AsFloat := 0.00;
              memSpecialReps.FieldByName('deposit').AsFloat := 0.00;

              memSpecialReps.Post;

            except on e: exception do
              begin
                memSpecialReps.Cancel;
                memo1.Lines.Add('[' + IntToStr(x) +
                  '] !!FAIL!! records added for year [' + trim(yearlist[i]) + '] system [' +
                  trim(systemlist[ii]) + '] --> ' + e.message);
                q2.Next;
                continue;
              end;
            end;
            inc(x);
            q2.Next;
          end; // while
          memo1.Lines.Add('[' + IntToStr(x) + '] records added for year [' +
            trim(yearlist[i]) + '] system [' + trim(systemlist[ii]) + ']');
        end; // for system
      end; // for year
    except on e: exception do
      begin
        memo1.Lines.Add('generic failed to set up YTD temp table --> ' + e.Message);
      end;
    end;
  finally
    yearList.Free;
    systemList.Free;
    OperatorMasterList.Free;
  end;

  lblspecial.caption := 'temp table setup complete';
  application.ProcessMessages;

  try
    q1.Close;
    q1.SQL.Clear; // get list of years from dataset first
    q1.SQL.Add('select * From assssssss;');
    lblspecial.caption := 'start processing';
    application.ProcessMessages;
    q1.Open;
    lblspecial.caption := 'query is now open';
    application.ProcessMessages;

    i := 0;
    q1.First;
    x := 0;
    z := 0;
    while not q1.Eof do
    begin
      inc(I);
      lblspecial.caption := 'processing: ' + IntToStr(i);
      application.ProcessMessages;

      asdf := false;

      asdf := memSpecialReps.Locate('year;system;operator;country;',
        VarArrayOf([trim(uppercase(q1.FieldByName('dt_year').asstring)),
        trim(uppercase(q1.FieldByName('system').asstring)),
          trim(uppercase(q1.FieldByName('operator').asstring)),
          trim(uppercase(q1.FieldByName('country').asstring))]), []);

      if not (asdf) then
      begin
        inc(x);
        memo1.Lines.add(q1.FieldByName('dt_year').asstring + ' ' +
          q1.FieldByName('system').asstring + ' ' +
          q1.FieldByName('operator').asstring + ' ' +
          q1.FieldByName('country').asstring + ' not found!');
      end
      else
      begin
        inc(z);
        // fill it
        try
          memSpecialReps.Edit;

          // check for over-write (should never happen)
          if (memSpecialReps.FieldByName('bookings').AsInteger <> 0) then
          begin
            memo1.Lines.add(q1.FieldByName('system').AsString + ' ' +
              q1.FieldByName('dt_year').asstring + ' ' +
              q1.FieldByName('operator').asstring + ' ' +
              q1.FieldByName('country').asstring + ' dupe? ');
          end;

          // year, system, operator, country are set already
          memSpecialReps.FieldByName('bookings').AsInteger := q1.FieldByName('bookings').AsInteger;
          memSpecialReps.FieldByName('ave_duration').AsFloat := q1.FieldByName('ave_duration').AsFloat;
          memSpecialReps.FieldByName('ave_profit').AsFloat := q1.FieldByName('ave_profit').AsFloat;
          memSpecialReps.FieldByName('total_days').AsInteger := q1.FieldByName('total_days').AsInteger;
          memSpecialReps.FieldByName('ave_op_due').AsFloat := q1.FieldByName('ave_op_due').AsFloat;
          memSpecialReps.FieldByName('total_op_due').AsFloat := q1.FieldByName('total_op_due').AsFloat;
          memSpecialReps.FieldByName('total_duration').AsFloat := q1.FieldByName('total_duration').AsFloat;
          memSpecialReps.FieldByName('total_profit').AsFloat := q1.FieldByName('total_profit').AsFloat;
          memSpecialReps.FieldByName('deposit').AsFloat := q1.FieldByName('deposit').AsFloat;

          memSpecialReps.Post;
        except on e: exception do
          begin
            memSpecialReps.cancel;
            memo1.Lines.add('update memcombined: ' + e.Message);
          end;
        end;
      end;
      q1.Next;
    end;
    memo1.Lines.Add(IntToStr(x) + ' failed locates.');
    memo1.Lines.Add(IntToStr(z) + ' OK locates.');
  except on e: exception do
    begin
      memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
    end;
  end;

  lblspecial.caption := 'sorting by year, country, operator_master..';
  application.ProcessMessages;
  memSpecialReps.SortOn('year;system;country;operator_master;operator;', mem.SortOptions);
  lblspecial.caption := 'sorting [done].';
  application.ProcessMessages;
  lblspecial.caption := 'printing..';
  application.ProcessMessages;

  PrintSpecialReportsRoutine(repYTD, Reportname, ReportTitle);

  lblspecial.caption := 'total: [' + IntToStr(i) + '] done!';
  application.ProcessMessages;

  try
    q1.Close;
    q2.Close;
  except;
  end;
end; // ytd special rep

procedure TfrmMainMkt.btnEMailSendClick(Sender: TObject);
begin
  try
    if fileexists(globEMLFileName) then EMAIL(globEMLFileName)
    else showmessage('The File [' + globEMLFileName + ' does not exist.');
  except;
  end;
end;

procedure TfrmMainMkt.btnFleetMixClick(Sender: TObject);
var
  theSQL, ListSQL, reportTitle, reportName, temp, temp2: string;
  i, x, z: Integer;
  yearList: TStringList;
  crap: string;
  asdf: boolean;
begin
  memo1.Clear;
  yearList := TStringList.Create;

  temp := '';
  if not MSTR_List_Dates_OK(temp) then
  begin
    if messageDLG(temp + #10#13 + ' Continue anyway?',
      mtWarning, [mbYes, mbNo], 0) = mrNO then exit;
  end;
  temp := '';

  try
    conn.Close;
    initQ_CloseClear(); // clear Q1.sql and open conn
    theSQL := '';
    ListSQL := '';
    reportTitle := '';
    ReportName := '';
    bKILL := false;

    if cbUseGender.Checked then
    begin
      showmessage('gender filter can not be used for this report.');
      exit;
    end;

    try
      SetUpMemTable('FLEETMIX', memSpecialReps);
      memo1.Lines.add('SetUpMemTable for "FLEET MIX" report OK');
    except on e: exception do
      begin
        memo1.Lines.add('SetUpMemTable Fleet Mix --> ' + e.message);
        showmessage('SetUpMemTableErr Fleet Mix --> ' + e.message);
        exit;
      end;
    end;

    q2.Close;
    q2.SQL.Clear;
    q1.Close;
    q1.SQL.Clear;

    try
      theSQL := BuildSQL(btnFleetMix);
      ListSQL := theSQL;
      theSQL := theSQL + ' INTO TEMP crap2 WITH NO LOG;';
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    try // getting distinct a list of operators
        // parse/replace the where and the group by
      temp := '';
      temp2 := '';

      temp := 'Select ' +
        'DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
        'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country, ' +
        'DECODE(trim(vs_sipp),"", "NULL", NULL, "NULL",trim(vs_sipp)) sipp ';

      // take everything out to the "WHERE"
      i := 0;
      i := pos('FROM g_voucher_summary WHERE', ListSQL);
      if i = 0 then raise(exception.Create('can''t parse year/sys/Oper/Ctry List SQL'));
      temp2 := copy(ListSQL, i, 1500);
      temp2 := trim(temp2);

      // Remove "Group By"
      i := 0;
      i := pos('group by 1,2,3,4,5', temp2);
      if i = 0 then raise(exception.Create('can''t parse year/sys/Oper/Ctry List SQL II'));
      temp2 := copy(temp2, 0, i - 1);
      temp2 := trim(temp2);

      ListSQL := temp + temp2 + ' group by 1,2,3  order by 1,2,3  INTO TEMP templist WITH NO LOG;';
      ListSQL := Trim(ListSQL);
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    GlobFName := 'fleet_mix';
    Build_RName_RTitle(btnFleetMix, Reportname, ReportTitle);
    addToRunLog2(Reportname, ReportTitle); // add back in later

    q1.SQL.Text := theSQL;
    q2.SQL.Text := ListSQL;

    q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
    q1.ParamByName('date2').AsDate := trunc(dtpend.Date);

    q2.ParamByName('date1').AsDate := trunc(dtpMasterListStart.date);
    q2.ParamByName('date2').AsDate := trunc(dtpMasterListEND.Date);

    if cbUseRange2.Checked then
    begin
      q1.ParamByName('dtRange2Start').AsDate := trunc(dtprange2start.Date);
      q1.ParamByName('dtRange2End').AsDate := trunc(dtprange2end.Date);
      // pad dates for q2:
      q2.ParamByName('dtRange2Start').AsDate := trunc(dtpMasterListStart.date);
      q2.ParamByName('dtRange2End').AsDate := trunc(dtpMasterListEND.date);
    end;

    if cbUseRange3.Checked then
    begin
      q1.ParamByName('dtRange3Start').AsDate := trunc(dtprange3start.Date);
      q1.ParamByName('dtRange3End').AsDate := trunc(dtprange3end.Date);
      // pad dates for q2
      q2.ParamByName('dtRange3Start').AsDate := trunc(dtpMasterListStart.date);
      q2.ParamByName('dtRange3End').AsDate := trunc(dtpMasterListEND.date);
    end;

    if cbUsePUDate.Checked then
    begin // paramatize this to handle EURO date
      q1.ParamByName('date3').AsDate := dtpPUStart.Date;
      q1.ParamByName('date4').AsDate := dtpPUEND.Date;
    end;

    // add 5/05/2010
    if cbUseCreateDate.Checked then
    begin
      q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
      q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
    end;

    if cbUseDropDate.Checked then // use drop date filters
    begin
      q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
      q1.ParamByName('date6').AsDate := dtpDropDtend.date;
    end;

    // end add params
    memo1.Lines.add(' ------ theSQL: ------ ');
    memo1.Lines.add(theSQL);
    memo1.Lines.add(' ------ date params: ----- ');
    for i := 0 to q1.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

    // q2
    memo1.Lines.add(' ------ ListSQL: ------ ');
    memo1.Lines.add(ListSQL);
    memo1.Lines.add(' ------ ListSQL date params: ----- ');
    for i := 0 to q2.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q2.Params[i].name + ' ' + q2.params[i].asstring);

    i := 0;
    lblspecial.caption := 'getting data';
    application.ProcessMessages;

    try
      q1.ExecSQL;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + ' into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    memo1.Lines.Add(IntToStr(i) + ' records added to "temp" table.');
    lblspecial.caption := IntToStr(i) + ' records added to "temp" table.';
    application.ProcessMessages;

    i := 0;
    lblspecial.caption := 'getting List data';
    application.ProcessMessages;
    try
      q2.ExecSQL;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + 'List SQL into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    memo1.Lines.Add(IntToStr(i) + ' List records added to "temp" table.');
    lblspecial.caption := IntToStr(i) + ' records added to "temp" table.';
    application.ProcessMessages;

    try
      q1.Close;
      q1.SQL.Clear; // get list of years from dataset first
      q1.SQL.Add('select distinct dt_year From crap2;');

      lblspecial.caption := 'getting count for list of years';
      application.ProcessMessages;

      q1.Open;
      q1.First;
      q1.Last;
      i := 0;
      i := q1.RecordCount;
      memo1.Lines.Add('count for list of years: + [' + IntToStr(i) + ']');
      application.ProcessMessages;

      yearList.Clear;
      q1.First;
      while not q1.Eof do
      begin
        yearList.Add(q1.Fields[0].asstring);
        q1.Next;
      end;

    except on e: exception do
      begin
        memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
      end;
    end;

    try
      q2.Close;
      q2.SQL.Clear;
      q2.SQL.Add('select * From templist order by operator, country, sipp;');

      memo1.Lines.Add('getting count for list of operator|country|sipp');
      application.ProcessMessages;
      q2.Open;
      q2.First;
      q2.Last;
      i := 0;
      i := q2.RecordCount;
      memo1.Lines.Add('!!! count for oper/ctry list: [' + IntToStr(i) + ']');
      application.ProcessMessages;
    except on e: exception do
      begin
        memo1.Lines.Add('failed to oper|ctry|sipp list --> ' + e.Message);
      end;
    end;

    lblspecial.Caption := 'setting up temp table';
    application.ProcessMessages;
    try
      memSpecialReps.EmptyTable;

      for i := 0 to yearlist.Count - 1 do
      begin
        q2.First;
        x := 0; z := 0;
        while not q2.Eof do
        begin
          application.ProcessMessages;

          try
            memSpecialReps.Insert;

            memSpecialReps.FieldByName('year').asstring := trim(uppercase(yearlist[i]));
            memSpecialReps.FieldByName('country').asstring := trim(uppercase(q2.FieldByName('country').AsString));
            memSpecialReps.FieldByName('operator').asstring := trim(uppercase(q2.FieldByName('operator').AsString));
            memSpecialReps.FieldByName('sipp').asstring := trim(uppercase(q2.FieldByName('sipp').AsString));

            memSpecialReps.FieldByName('USA_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('usa_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('usa_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('usa_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('usa_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('usa_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('KEM_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('kem_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('kem_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('kem_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('kem_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('kem_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('CANA_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('cana_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('cana_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('cana_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('cana_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('cana_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('HOHO_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('hoho_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('hoho_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('hoho_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('hoho_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('hoho_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('UK_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('uk_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('uk_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('uk_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('uk_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('uk_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('EURO_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('euro_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('euro_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('euro_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('euro_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('euro_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('AUS_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('aus_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('aus_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('aus_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('aus_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('aus_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('NEWZ_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('newz_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('newz_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('newz_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('newz_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('newz_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('SAF_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('saf_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('saf_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('saf_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('saf_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('saf_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('LAC_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('lac_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('lac_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('lac_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('lac_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('lac_op_due').asfloat := 0.00;

            memSpecialReps.FieldByName('HOT_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('hot_plus_count').asinteger := 0;
            memSpecialReps.FieldByName('hot_basic_count').asinteger := 0;
            memSpecialReps.FieldByName('hot_ave_duration').asfloat := 0.00;
            memSpecialReps.FieldByName('hot_ave_profit').asfloat := 0.00;
            memSpecialReps.FieldByName('hot_op_due').asfloat := 0.00;
            memSpecialReps.Post;
          except on e: exception do
            begin
              memSpecialReps.Cancel;
              memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! records added for year [' + trim(yearlist[i]) + ']. ' + e.Message);
              q2.Next;
              continue;
            end;
          end;
          inc(x);
          q2.Next;
        end; // while

        memo1.Lines.Add('[' + IntToStr(x) + '] records added for year [' + trim(yearlist[i]) + '].');
      end; // for
    except on e: exception do
      begin
        memo1.Lines.Add('generic failed to set up temp table --> ' + e.Message);
      end;
    end;
  finally
    yearList.Free;
  end;

  lblspecial.caption := 'temp table setup complete';
  application.ProcessMessages;

  try
    q1.Close;
    q1.SQL.Clear;
    q1.SQL.Add('select * From crap2;');
    lblspecial.caption := 'start processing';
    application.ProcessMessages;
    q1.Open;
    lblspecial.caption := 'q1 is open';
    application.ProcessMessages;
    i := 0;

    q1.First;
    x := 0;
    z := 0;
    while not q1.Eof do
    begin
      inc(I);
      lblspecial.caption := 'processing: ' + IntToStr(i);
      application.ProcessMessages;
                                                                // country;operator;sipp
      asdf := false;
      asdf := memSpecialReps.Locate('year;country;operator;sipp;',
        VarArrayOf([
        trim(uppercase(q1.FieldByName('dt_year').asstring)),
          trim(uppercase(q1.FieldByName('country').asstring)),
          trim(uppercase(q1.FieldByName('operator').asstring)),
          trim(uppercase(q1.FieldByName('sipp').asstring))]), []);

      if asdf = false then
      begin
        inc(x);
        memo1.Lines.add(q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
          q1.FieldByName('country').asstring + ' ' + q1.FieldByName('sipp').asstring + ' not found!');
      end
      else
      begin
        inc(z);
        crap := trim(uppercase(q1.FieldByName('system').asstring));

        try
          memSpecialReps.Edit;

          if crap = 'AUS' then // insert to
          begin
            if (memSpecialReps.FieldByName('AUS_COUNT').AsInteger > 0) then
              memo1.Lines.add('AUS ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('AUS_COUNT').AsInteger :=
              memSpecialReps.FieldByName('AUS_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('aus_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('aus_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('aus_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('aus_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('aus_op_due').asfloat := q1.FieldByName('op_due').Asfloat;

          end
          else if crap = 'CANA' then // insert to
          begin
            if (memSpecialReps.FieldByName('CANA_COUNT').AsInteger > 0) then
              memo1.Lines.add('CANA ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('CANA_COUNT').AsInteger :=
              memSpecialReps.FieldByName('CANA_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('cana_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('cana_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('cana_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('cana_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('cana_op_due').asfloat := q1.FieldByName('op_due').Asfloat;

          end
          else if crap = 'EURO' then // insert to
          begin
            if (memSpecialReps.FieldByName('EURO_COUNT').AsInteger > 0) then
              memo1.Lines.add('EURO ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('EURO_COUNT').AsInteger :=
              memSpecialReps.FieldByName('EURO_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('euro_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('euro_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('euro_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('euro_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('euro_op_due').asfloat := q1.FieldByName('op_due').Asfloat;

          end
          else if crap = 'HOHO' then // insert to
          begin
            if (memSpecialReps.FieldByName('HOHO_COUNT').AsInteger > 0) then
              memo1.Lines.add('HOHO ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('HOHO_COUNT').AsInteger :=
              memSpecialReps.FieldByName('HOHO_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('hoho_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('hoho_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('hoho_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('hoho_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('hoho_op_due').asfloat := q1.FieldByName('op_due').Asfloat;


          end
          else if crap = 'HOT' then // insert to
          begin
            if (memSpecialReps.FieldByName('HOT_COUNT').AsInteger > 0) then
              memo1.Lines.add('HOT ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('HOT_COUNT').AsInteger :=
              memSpecialReps.FieldByName('HOT_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('hot_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('hot_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('hot_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('hot_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('hot_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'KEM' then // insert to
          begin
            if (memSpecialReps.FieldByName('KEM_COUNT').AsInteger > 0) then
              memo1.Lines.add('KEM ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('KEM_COUNT').AsInteger :=
              memSpecialReps.FieldByName('KEM_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('kem_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('kem_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('kem_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('kem_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('kem_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'NEWZ' then // insert to
          begin
            if (memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger > 0) then
              memo1.Lines.add('NEWZ ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger :=
              memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('newz_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('newz_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('newz_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('newz_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('newz_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'SAF' then // insert to
          begin
            if (memSpecialReps.FieldByName('SAF_COUNT').AsInteger > 0) then
              memo1.Lines.add('SAF ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('SAF_COUNT').AsInteger :=
              memSpecialReps.FieldByName('SAF_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('saf_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('saf_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('saf_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('saf_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('saf_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'UK' then // insert to
          begin
            if (memSpecialReps.FieldByName('UK_COUNT').AsInteger > 0) then
              memo1.Lines.add('UK ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('UK_COUNT').AsInteger :=
              memSpecialReps.FieldByName('UK_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('uk_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('uk_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('uk_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('uk_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('uk_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'USA' then // insert to
          begin
            if (memSpecialReps.FieldByName('USA_COUNT').AsInteger > 0) then
              memo1.Lines.add('USA ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('USA_COUNT').AsInteger :=
              memSpecialReps.FieldByName('USA_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('usa_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('usa_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('usa_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('usa_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('usa_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else if crap = 'LAC' then // insert to
          begin
            if (memSpecialReps.FieldByName('LAC_COUNT').AsInteger > 0) then
              memo1.Lines.add('LAC ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('LAC_COUNT').AsInteger :=
              memSpecialReps.FieldByName('LAC_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
            memSpecialReps.FieldByName('lac_plus_count').asinteger := q1.FieldByName('plus_count').AsInteger;
            memSpecialReps.FieldByName('lac_basic_count').asinteger := q1.FieldByName('basic_count').AsInteger; ;
            memSpecialReps.FieldByName('lac_ave_duration').asfloat := q1.FieldByName('ave_duration').AsFloat;
            memSpecialReps.FieldByName('lac_ave_profit').asfloat := q1.FieldByName('ave_profit').AsFloat;
            memSpecialReps.FieldByName('lac_op_due').asfloat := q1.FieldByName('op_due').Asfloat;
          end
          else
          begin
            memo1.Lines.add('[' + crap + '] is not a valid system flag. ');
            memo1.Lines.Add(q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
              q1.FieldByName('country').asstring + ' ' + q1.FieldByName('sipp').asstring + ' not updated.');
          end;

          memSpecialReps.Post;
        except on e: exception do
          begin
            memSpecialReps.cancel;
            memo1.Lines.add('update memcombined: ' + e.Message);
          end;
        end;
      end;
      q1.Next;
    end;

    memo1.Lines.Add(IntToStr(x) + ' failed locates.');
    memo1.Lines.Add(IntToStr(z) + ' OK locates.');
  except on e: exception do
    begin
      memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
    end;
  end;

  lblspecial.caption := 'sorting by year, country, operator, sipp..';
  application.ProcessMessages;
  memSpecialReps.SortOn('year;country;operator;sipp;', mem.SortOptions);
  lblspecial.caption := 'sorting [done].';
  application.ProcessMessages;
  lblspecial.caption := 'printing..';
  application.ProcessMessages;
  PrintSpecialReportsRoutine(repFleetMix, Reportname, ReportTitle);
  lblspecial.caption := 'total: [' + IntToStr(i) + '] done!';

  try
    q1.Close;
    q2.Close;
  except;
  end;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.btnGetDataWMySQLClick(Sender: TObject);
begin
  btnOK_NEWClick(btnGetDataWMySQL);
end;

procedure TfrmMainMkt.btnIataListClearClick(Sender: TObject);
begin
  memoIataList.Clear;
end;

procedure TfrmMainMkt.btnNoSippsList2Click(Sender: TObject);
begin
  SetCLB(clbsipp2, false);
end;

procedure TfrmMainMkt.btnAllSippsList2Click(Sender: TObject);
begin
  SetCLB(clbsipp2, true);
end;

procedure TfrmMainMkt.btnAllSippsList3Click(Sender: TObject);
begin
  SetCLB(clbsipp3, true);
end;

procedure TfrmMainMkt.btnNoSippsList3Click(Sender: TObject);
begin
  SetCLB(clbsipp3, false);
end;

procedure TfrmMainMkt.btnAllSippsList4Click(Sender: TObject);
begin
  SetCLB(clbsipp4, true);
end;

procedure TfrmMainMkt.btnNoSippsList4Click(Sender: TObject);
begin
  SetCLB(clbsipp4, false);
end;

procedure TfrmMainMkt.btnGRDKillClick(Sender: TObject);
begin
  btnkill.Click;
end;

procedure TfrmMainMkt.btnDbAllClick(Sender: TObject);
begin
  SetOrUnSetCLB(true);
end;

procedure TfrmMainMkt.btnDbClearClick(Sender: TObject);
begin
  SetOrUnSetCLB(false);
end;

procedure TfrmMainMkt.btnkillClick(Sender: TObject);
begin
  application.ProcessMessages;
  bkill := not (bkill);
  application.ProcessMessages;
end;

procedure TfrmMainMkt.btnStepTwoClick(Sender: TObject);
begin
  PageControl1.ActivePage := tabStep2;
  application.ProcessMessages;
end;

procedure TfrmMainMkt.btnRPBUSClick(Sender: TObject);
var // repeat biz
  theSQL, reportTitle, reportName, crap: string;
  i, x, z: Integer;
  asdf: boolean;
  crud: Real;
begin
  memo1.Clear;
  initQ_CloseClear(); // clear Q1.sql and open conn
  theSQL := '';
  reportTitle := '';
  ReportName := '';
  bKILL := false;

  crap := 'Please select date range in "step 1" which represents original booking range, ' + #10#13 +
    'and select "use range 2" in "special reports" which represents the dates where the clients ' + #10#13 +
    'came back to book again.';

  if not cbUseRange2.Checked then
  begin
    showmessage(crap);
    exit;
  end;

  if cbUseRange3.Checked then
  begin
    showmessage('"range 3" is not valid for repeat business report.' + #10#13 + crap);
    cbUseRange3.Checked := false;
    exit;
  end;

  crap := '';

  try
    SetUpMemTable('REPEATBIZ', memSpecialReps);
    memo1.Lines.add('SetUpMemTable for "REPEATBIZ" report OK');
    lblspecial.caption := 'SetUpMemTable for "REPEATBIZ" report OK';
  except on e: exception do
    begin
      memo1.Lines.add('SetUpMemTable --> ' + e.message);
      showmessage('SetUpMemTableErr --> ' + e.message);
      exit;
    end;
  end;

  q1.Close;
  q1.SQL.Clear;
  try
    theSQL := BuildSQL(btnrpbus);
  except on e: exception do
    begin
      memo1.Lines.add('BuildSQL Err--> ' + e.message);
      showmessage('BuildSQL Err--> ' + e.message);
      exit;
    end;
  end;

  GlobFName := 'repeat_biz';
  Build_RName_RTitle(btnRPBUS, Reportname, ReportTitle);

  addToRunLog2(Reportname, ReportTitle); // add back in later
  q1.SQL.Text := theSQL;
    // use params as euro date bombs when calling datetoStr()
  q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
  q1.ParamByName('date2').AsDate := trunc(dtpend.Date);

  if cbUsePUDate.Checked then
  begin // paramatize this to handle EURO date
    q1.ParamByName('date3').AsDate := dtpPUStart.Date;
    q1.ParamByName('date4').AsDate := dtpPUEND.Date;
  end;

  if cbUseDropDate.Checked then // use drop date filters
  begin
    q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
    q1.ParamByName('date6').AsDate := dtpDropDtend.date;
  end;

  // add 5/05/2010
  if cbUseCreateDate.Checked then
  begin
    q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
    q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
  end;

  // end add params
  memo1.Lines.add(' ------ theSQL: ------ ');
  memo1.Lines.add(theSQL);
  memo1.Lines.add(' ------ date params: ----- ');
  for i := 0 to q1.ParamCount - 1 do
    memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

  lblspecial.caption := 'getting initial count data';
  application.ProcessMessages;

  try
    q1.open;
  except on e: exception do
    begin
      memo1.Lines.Add(IntToStr(i) + ' err --> ' + e.Message);
      showmessage('err adf --> ' + e.Message);
      exit;
    end;
  end;

  memo1.Lines.Add('Initial count data retrieved.');
  lblspecial.caption := 'Initial count data retrieved.';
  application.ProcessMessages;
  lblspecial.Caption := 'setting up temp table';
  application.ProcessMessages;

  try
    memSpecialReps.EmptyTable;
    x := 0;
    q1.First;
    while not q1.Eof do
    begin
      try
        memSpecialReps.Insert;

        memSpecialReps.FieldByName('system').asstring := trim(uppercase(q1.FieldByName('system').AsString));
        memSpecialReps.FieldByName('orig_bookings').AsInteger := q1.FieldByName('orig_bookings').AsInteger;
        memSpecialReps.FieldByName('orig_start_date').AsDateTime := trunc(dtpstart.date);
        memSpecialReps.FieldByName('orig_end_date').AsDateTime := trunc(dtpEnd.date);
        memSpecialReps.FieldByName('repeat_bookings').AsInteger := q1.FieldByName('repeat_bookings').AsInteger;
        memSpecialReps.FieldByName('repeat_start_date').AsDateTime := trunc(dtprange2start.Date);
        memSpecialReps.FieldByName('repeat_end_date').AsDateTime := trunc(dtprange2end.Date);
        memSpecialReps.FieldByName('repeat_percent').AsFloat := q1.FieldByName('repeat_percent').AsFloat;

        memSpecialReps.Post;
      except on e: exception do
        begin
          memSpecialReps.Cancel;
          memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! 1 --> ' + e.Message);
          showmessage('[' + IntToStr(x) + '] !!FAIL to insert to temp table !! 1 --> ' + e.Message);
          q1.Next;
          continue;
        end;
      end;
      inc(x);
      q1.Next;
    end; // while
    memo1.Lines.Add('[' + IntToStr(x) + '] records added for repeat_biz');
    lblspecial.caption := '[' + IntToStr(x) + '] records added for repeat_biz';

  except on e: exception do
    begin
      memo1.Lines.Add('repeat biz failed to set up temp table --> ' + e.Message);
    end;
  end;

  lblspecial.caption := 'temp table setup complete';
  application.ProcessMessages;

  // now assuming the grid is full, loop it and determine the repeats

  memSpecialReps.First;
  while not MemSpecialReps.Eof do
  begin

    i := 0;
    try
      q1.Close;
      q1.SQL.Clear;
      theSQL := 'drop table rpbus_temp;';
      q1.SQL.Add(theSQL);

      lblspecial.caption := 'drop temp table';
      q1.ExecSQL;
      memo1.Lines.Add('[' + IntToStr(i) + '] records deleted for sytem [' +
        memSpecialReps.FieldByName('system').AsString + ']');
      lblspecial.caption := '[' + IntToStr(i) + '] records deleted for sytem [' +
        memSpecialReps.FieldByName('system').AsString + ']';

    except on e: exception do
      begin
        memo1.Lines.add('drop table Err. [' + IntToStr(i) + '] system [' +
          memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);
      end;
    end;

    i := 0;
    try
      q1.Close;
      q1.SQL.Clear;
      theSQL := '';
      theSQL := BuildSQL(rpbus_get_names);
      theSQL := theSQL + ' into temp rpbus_temp WITH NO LOG; ';

      q1.SQL.Text := theSQL;

      q1.ParamByName('date1').asdate := trunc(dtpstart.date);
      q1.ParamByName('date2').asdate := trunc(dtpend.date);

      memo1.Lines.add(' ------ Name SQL for into temp: ------ ');
      memo1.Lines.add(theSQL);
      memo1.Lines.add(' ------ params: ----- ');
      for i := 0 to q1.ParamCount - 1 do
        memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

      lblspecial.caption := '[' + IntToStr(i) + '] adding NAME records system [' +
        memSpecialReps.FieldByName('system').AsString + ']';
      application.ProcessMessages;

      i := 0;
      q1.ExecSQL;

      memo1.Lines.Add('[' + IntToStr(i) + '] NAME records added for system [' +
        memSpecialReps.FieldByName('system').AsString + ']');

      lblspecial.caption := '[' + IntToStr(i) + '] NAME records added for system [' +
        memSpecialReps.FieldByName('system').AsString + ']';

      application.ProcessMessages;
    except on e: exception do
      begin
        memo1.Lines.add('NAME records into temp table Err. [' + IntToStr(i) + '] system [' +
          memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);
        showmessage('failed to put names into temp space for system [' +
          memSpecialReps.FieldByName('system').AsString + '] Continue assuming bad calculation. Error --> ' + e.message);
        memspecialreps.Next;
        continue;
      end;
    end;

    // get bookings where names match up
    try
      q1.Close;
      q1.SQL.Clear;
      theSQL := '';

      if rgCountMethod.ItemIndex = 0 then
      begin
        theSQL :=
          'Select sum(vs_count_voucher) rentals From g_voucher_summary, g_client ';
      end
      else
      begin
        theSQL :=
        //'Select count(*) rentals From g_voucher_summary, g_client ' +
        // changed for consistency. the other arg inflates the % because it usually yeilds a higher #
        'Select sum(vs_count_logical) rentals From g_voucher_summary, g_client ';
      end;

      theSQL := theSQL +
        'where vs_basesys = "' + memspecialreps.fieldbyname('system').asstring + '" '; // and vs_active_history = "A" ' +

        // add this to give choice - running "all vouchers or "active only" yeilds the same results in the end
      if (cbActiveOnly.Checked) then
        theSQL := theSQL + 'and vs_active_history = ''A'' ';

      theSQL := theSQL + 'and vs_paid_dt between :date1 and :date2 ' +
        'and ( trim(upper(gc_lastname)) || ", " ||  trim(upper(gc_firstname)) in ( select name from rpbus_temp ) ) ' +
        'and gc_system = vs_basesys and  ' +
        'gc_voucher = vs_voucher '; //  release number not needed

      memo1.Lines.Add('getting repeat count for system [' +
        memSpecialReps.FieldByName('system').AsString + ']');

      q1.SQL.Text := theSQL;
      q1.ParamByName('date1').asdate := trunc(dtprange2start.date);
      q1.ParamByName('date2').asdate := trunc(dtprange2end.date);

      memo1.Lines.add(' ------ Count SQL for repeat biz: ------ ');
      memo1.Lines.add(theSQL);
      memo1.Lines.add(' ------ params: ----- ');

      for i := 0 to q1.ParamCount - 1 do
        memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

      lblspecial.caption := 'getting repeats for sytem [' + memSpecialReps.FieldByName('system').AsString + ']';

      q1.open;

      memo1.Lines.Add('[' + q1.fieldbyname('rentals').asstring + '] repeat customer records found for system [' +
        memSpecialReps.FieldByName('system').AsString + ']');

      lblspecial.caption := '[' + q1.fieldbyname('rentals').asstring + '] repeat customer records found for system [' +
        memSpecialReps.FieldByName('system').AsString + ']';

      memSpecialReps.Edit;
      memSpecialReps.FieldByName('repeat_bookings').AsInteger := q1.Fields[0].asInteger;
      memSpecialReps.Post;
    except on e: exception do
      begin
        memo1.Lines.add('NAME records into temp table Err. [' + IntToStr(i) + '] system [' +
          memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);
        memspecialreps.Next;
        continue;
      end;
    end;

    // update percentages:
    memSpecialReps.Edit;
    try
      crud := 0.0;
      crud := (memSpecialReps.FieldByName('repeat_bookings').AsInteger / memSpecialReps.FieldByName('orig_bookings').AsInteger) * 100;
      memSpecialReps.FieldByName('repeat_percent').AsString := FormatFloat('##0.00', crud);
    except
      memSpecialReps.FieldByName('repeat_percent').AsFloat := 0.0;
    end;
    memSpecialReps.Post;
    memSpecialReps.Next;
  end; // while not EOF memSpecialReps

  application.ProcessMessages;
  lblspecial.caption := 'printing..';
  PrintSpecialReportsRoutine(repRepeatBiz, Reportname, ReportTitle);
  lblspecial.caption := 'total: [' + IntToStr(i) + '] done!';
  application.ProcessMessages;
end;

procedure TfrmMainMkt.btnRpBusNewClick(Sender: TObject);
var // repeat biz
  theSQL, reportTitle, reportName, crap: string;
  i, x, z, xx: Integer;
  asdf: boolean;
  crud: Real;

  procedure SetTheParams(SourceTarget: string);
  begin
    if sourceTarget = 'SOURCE' then
    begin
      q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
      q1.ParamByName('date2').AsDate := trunc(dtpend.Date);
    end
    else
    begin
      q1.ParamByName('date1').AsDate := trunc(dtprange2start.Date);
      q1.ParamByName('date2').AsDate := trunc(dtprange2end.Date);
    end;

    if cbUsePUDate.Checked then
    begin // paramatize this to handle EURO date
      q1.ParamByName('date3').AsDate := dtpPUStart.Date;
      q1.ParamByName('date4').AsDate := dtpPUEND.Date;
    end;

    if cbUseDropDate.Checked then // use drop date filters
    begin
      q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
      q1.ParamByName('date6').AsDate := dtpDropDtend.date;
    end;

  // add 5/05/2010
    if cbUseCreateDate.Checked then
    begin
      q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
      q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
    end;
  end;
begin
  memo1.Clear;
  conn.Close;
  initQ_CloseClear(); // clear Q1.sql and open conn
  theSQL := '';
  reportTitle := '';
  ReportName := '';
  bKILL := false;

  crap := 'Please select date range in "step 1" which represents original booking range, ' + #10#13 +
    'and select "use range 2" in "special reports" which represents the dates where the clients ' + #10#13 +
    'came back to book again.';

  if not cbUseRange2.Checked then
  begin
    showmessage(crap);
    exit;
  end;

  if cbUseRange3.Checked then
  begin
    showmessage('"range 3" is not valid for repeat business report.' + #10#13 + crap);
    cbUseRange3.Checked := false;
    exit;
  end;

  crap := '';

  try
    SetUpMemTable('REPEATBIZ_2', memSpecialReps);
    memo1.Lines.add('SetUpMemTable for "REPEATBIZ_2" report OK');
    lblspecial.caption := 'SetUpMemTable for "REPEATBIZ_2" report OK';
  except on e: exception do
    begin
      memo1.Lines.add('SetUpMemTable --> ' + e.message);
      showmessage('SetUpMemTableErr --> ' + e.message);
      exit;
    end;
  end;

  q1.Close;
  q1.SQL.Clear;
  try
    // first get totals for both dc and ta
    theSQL := BuildSQL(btnrpbusNew); // here bra
  except on e: exception do
    begin
      memo1.Lines.add('BuildSQL Err--> ' + e.message);
      showmessage('BuildSQL Err--> ' + e.message);
      exit;
    end;
  end;

  GlobFName := 'repeat_biz';
  Build_RName_RTitle(btnRPBUS, Reportname, ReportTitle);
  addToRunLog2(Reportname, ReportTitle); // add back in later

  q1.SQL.Text := theSQL;

  settheparams('SOURCE');

  memo1.Lines.add(' ------ theSQL: ------ ');
  memo1.Lines.add(theSQL);
  memo1.Lines.add(' ------ date params: ----- ');
  for i := 0 to q1.ParamCount - 1 do
    memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

  lblspecial.caption := 'getting initial count data';
  application.ProcessMessages;

  try
    q1.open;
  except on e: exception do
    begin
      memo1.Lines.Add(IntToStr(i) + ' err --> ' + e.Message);
      showmessage('err adf --> ' + e.Message);
      exit;
    end;
  end;

  memo1.Lines.Add('Initial count data retrieved.');
  lblspecial.caption := 'Initial count data retrieved.';
  application.ProcessMessages;
  lblspecial.Caption := 'setting up temp table';
  application.ProcessMessages;

  try
    memSpecialReps.EmptyTable;
    x := 0;
    q1.First;
    while not q1.Eof do
    begin
      try
        memSpecialReps.Insert;

        memSpecialReps.FieldByName('system').asstring := trim(uppercase(q1.FieldByName('system').AsString));
        memSpecialReps.FieldByName('orig_bookingsTotal').AsInteger := q1.FieldByName('orig_bookings').AsInteger; //sum
        memSpecialReps.FieldByName('orig_start_date').AsDateTime := trunc(dtpstart.date);
        memSpecialReps.FieldByName('orig_end_date').AsDateTime := trunc(dtpEnd.date);
        memSpecialReps.FieldByName('repeat_start_date').AsDateTime := trunc(dtprange2start.Date);
        memSpecialReps.FieldByName('repeat_end_date').AsDateTime := trunc(dtprange2end.Date);
        memSpecialReps.FieldByName('orig_bookingsDC').AsInteger := 0;
        memSpecialReps.FieldByName('orig_bookingsTA').AsInteger := 0;
        memSpecialReps.FieldByName('repeat_bookingsDC').AsInteger := 0;
        memSpecialReps.FieldByName('repeat_bookingsTA').AsInteger := 0;
        memSpecialReps.FieldByName('repeat_percentDC').AsFloat := 0.0;
        memSpecialReps.FieldByName('repeat_percentTA').AsFloat := 0.0;

        // need to add get routine for this
        memSpecialReps.FieldByName('repeat_year_bookings_total').AsInteger := 0; //' , ftInteger, 0, false); // new get via same method as "orig_bookingsTotal"
        memSpecialReps.FieldByName('repeat_year_bookings_DC').AsInteger := 0;
        memSpecialReps.FieldByName('repeat_year_bookings_TA').AsInteger := 0;
        // combine and plug later
        memSpecialReps.FieldByName('repeat_percent_DC_TA').AsFloat := 0.0; // , ftFloat, 0, false); // new plug
        memSpecialReps.FieldByName('repeat_bookings_DC_TA').AsInteger := 0; ///, ftInteger, 0, false); // new = repeat_bookingsDC + repeat_bookingsTA

        memSpecialReps.Post;
      except on e: exception do
        begin
          memSpecialReps.Cancel;
          memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! 1 --> ' + e.Message);
          showmessage('[' + IntToStr(x) + '] !!FAIL to insert to temp table !! 1 --> ' + e.Message);
          q1.Next;
          continue;
        end;
      end;
      inc(x);
      q1.Next;
    end; // while

    memo1.Lines.Add('[' + IntToStr(x) + '] records added for repeat_biz 2');
    lblspecial.caption := '[' + IntToStr(x) + '] records added for repeat_biz 2';

  except on e: exception do
    begin
      memo1.Lines.Add('repeat biz 2 failed to set up temp table --> ' + e.Message);
    end;
  end;

  // get target year total bookings that qualify for this dataset
  q1.Close;
  q1.SQL.Clear;
  try
    theSQL := BuildSQL(btnrpbusNew); // here bra
  except on e: exception do
    begin
      memo1.Lines.add('BuildSQL Err--> ' + e.message);
      showmessage('BuildSQL Err--> ' + e.message);
      exit;
    end;
  end;

  q1.SQL.Text := theSQL;

  settheparams('TARGET');

  memo1.Lines.add(' ------ theSQL: ------ ');
  memo1.Lines.add(theSQL);
  memo1.Lines.add(' ------ date params: ----- ');
  for i := 0 to q1.ParamCount - 1 do
    memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

  lblspecial.caption := 'getting target year count data';
  application.ProcessMessages;

  try
    q1.open;
  except on e: exception do
    begin
      memo1.Lines.Add(IntToStr(i) + ' err --> ' + e.Message);
      showmessage('err adfs --> ' + e.Message);
      exit;
    end;
  end;

  memo1.Lines.Add('target year count data retrieved.');
  lblspecial.caption := 'target year count data retrieved.';
  application.ProcessMessages;

  try
    x := 0;
    q1.First;

    while not q1.Eof do
    begin
      try
        if not memSpecialReps.Locate('system;', VarArrayOf([trim(uppercase(q1.Fields[0].AsString))]), []) then
        begin // plug * for now
          memo1.Lines.Add('x locate err --> failed to locate [' + trim(uppercase(q1.Fields[0].AsString)) + ']');
          showmessage('x locate err --> failed to locate [' + trim(uppercase(q1.Fields[0].AsString)) + ']');
          q1.Next;
          continue;
        end;

        memSpecialReps.Edit;
        memSpecialReps.FieldByName('repeat_year_bookings_total').AsInteger :=
          q1.FieldByName('orig_bookings').AsInteger; //sum

        memSpecialReps.Post;
      except on e: exception do
        begin
          memSpecialReps.Cancel;
          memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! 1 --> ' + e.Message);
          showmessage('[' + IntToStr(x) + '] !!FAIL to insert to temp table !! 1 --> ' + e.Message);
          q1.Next;
          continue;
        end;
      end;
      inc(x);
      q1.Next;
    end; // while

    memo1.Lines.Add('[' + IntToStr(x) + '] records added for repeat_biz 2');
    lblspecial.caption := '[' + IntToStr(x) + '] records added for repeat_biz 2';

  except on e: exception do
    begin
      memo1.Lines.Add('repeat biz 2 failed add target year total bookings --> ' + e.Message);
    end;
  end;

 // end get target year total bookings that qualify for this dataset
 // totals should be set. now go do the same thing for TA and DC and plug those numbers into the mem table

  for xx := 0 to 3 do
  // 0 source DC
  // 1 source TA
  // 2 DC target
  // 3 TA target
  begin
    q1.Close;
    q1.SQL.Clear;
    try
      // first get totals for both dc and ta
      theSQL := '';

      if (xx = 0) or (xx = 2) then // source year 0, target 2
        theSQL := BuildSQL(btnrpbusNew, 'DC')
      else if (xx = 1) or (xx = 3) then // source year 1 target 3 for TA
        theSQL := BuildSQL(btnrpbusNew, 'TA');
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    q1.SQL.Text := theSQL;

    if (xx = 0) or (xx = 1) then settheparams('SOURCE')
    else settheparams('TARGET');

    memo1.Lines.add(' ------ theSQL: asdf ------ ' + inttostr(xx));
    memo1.Lines.add(theSQL);
    memo1.Lines.add(' ------ date params: ----- ');

    for i := 0 to q1.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

    if (xx = 0) or (xx = 2) then lblspecial.caption := 'getting DC count data'
    else lblspecial.caption := 'getting TA count data';

    application.ProcessMessages;

    try
      q1.open;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + ' err --> ' + e.Message);
        showmessage('err adf --> ' + e.Message);
        exit;
      end;
    end;

    if (xx = 0) or (xx = 2) then
    begin
      memo1.Lines.Add('DC count data retrieved.');
      lblspecial.caption := 'DC count data retrieved.';
      lblspecial.Caption := 'adding DC totals to temp table';
    end
    else
    begin
      memo1.Lines.Add('TA count data retrieved.');
      lblspecial.caption := 'TA count data retrieved.';
      lblspecial.Caption := 'adding TA totals to temp table';
    end;

    try
      x := 0;
      q1.First;
      while not q1.Eof do
      begin
        try
          application.ProcessMessages;
          // add LOCATE and PLUG original bookings for DC / TA
          if not memSpecialReps.Locate('system;', VarArrayOf([trim(uppercase(q1.Fields[0].AsString))]), []) then
          begin // plug * for now
            memo1.Lines.Add('locate err --> failed to locate [' + trim(uppercase(q1.Fields[0].AsString)) + ']');
            showmessage('locate err --> failed to locate [' + trim(uppercase(q1.Fields[0].AsString)) + ']');
            q1.Next;
            continue;
          end;

          memSpecialReps.Edit;

          if (xx = 0) then
            memSpecialReps.FieldByName('orig_bookingsDC').AsInteger := q1.FieldByName('orig_bookings').AsInteger
          else if (xx = 1) then
            memSpecialReps.FieldByName('orig_bookingsTA').AsInteger := q1.FieldByName('orig_bookings').AsInteger
          else if (xx = 2) then
            memSpecialReps.FieldByName('repeat_year_bookings_DC').AsInteger := q1.FieldByName('orig_bookings').AsInteger
          else if (xx = 3) then
            memSpecialReps.FieldByName('repeat_year_bookings_TA').AsInteger := q1.FieldByName('orig_bookings').AsInteger;

          memSpecialReps.Post;
        except on e: exception do
          begin
            memSpecialReps.Cancel;

            if (xx = 0) or (xx = 2) then
            begin
              memo1.Lines.Add('DC [' + IntToStr(x) + '] !!FAIL!! 3 --> ' + e.Message);
              showmessage('DC [' + IntToStr(x) + '] !!FAIL to update temp table !! 1 --> ' + e.Message);
            end
            else
            begin
              memo1.Lines.Add('TA [' + IntToStr(x) + '] !!FAIL!! 3 --> ' + e.Message);
              showmessage('TA [' + IntToStr(x) + '] !!FAIL to update temp table !! 1 --> ' + e.Message);
            end;

            q1.Next;
            continue;
          end;
        end;
        inc(x);
        q1.Next;
      end; // while

      if (xx = 0) or (xx = 2) then
      begin
        memo1.Lines.Add('[' + IntToStr(x) + '] DC records edited for repeat_biz 2');
        lblspecial.caption := '[' + IntToStr(x) + '] DC records edited repeat_biz 2';
      end
      else
      begin
        memo1.Lines.Add('[' + IntToStr(x) + '] TA records edited for repeat_biz 2');
        lblspecial.caption := '[' + IntToStr(x) + '] TA records edited repeat_biz 2';
      end;
    except on e: exception do
      begin
        memo1.Lines.Add('repeat biz 2 failed edit/plug original DC/TA bookings --> ' + e.Message);
      end;
    end;
  end; // for

  // all original bookings [ALL, DC, TA] should be plugged by now

  lblspecial.caption := 'temp table setup complete';
  application.ProcessMessages;

  // now assuming grid is full, loop it and determine the repeats

  memSpecialReps.First;
  while not MemSpecialReps.Eof do
  begin
    for xx := 0 to 1 do // if 0 then DC else TA
    begin
      i := 0;
      try
        q1.Close;
        q1.SQL.Clear;
        theSQL := 'drop table rpbus_temp;';
        q1.SQL.Add(theSQL);

        lblspecial.caption := 'drop temp table';
        q1.ExecSQL;

        memo1.Lines.Add('[' + IntToStr(i) + '] records deleted for system [' +
          memSpecialReps.FieldByName('system').AsString + ']');
        lblspecial.caption := '[' + IntToStr(i) + '] records deleted for system [' +
          memSpecialReps.FieldByName('system').AsString + ']';
      except on e: exception do
        begin
          memo1.Lines.add('drop table Err. [' + IntToStr(i) + '] system [' +
            memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);
        end;
      end;

      i := 0;
      try
        q1.Close;
        q1.SQL.Clear;
        theSQL := '';

        if xx = 0 then //DC
          theSQL := BuildSQL(rpbus_get_names, 'DC')
        else
          theSQL := BuildSQL(rpbus_get_names, 'TA');

        theSQL := theSQL + ' into temp rpbus_temp WITH NO LOG; ';

        q1.SQL.Text := theSQL;

        q1.ParamByName('date1').asdate := trunc(dtpstart.date);
        q1.ParamByName('date2').asdate := trunc(dtpend.date);

        if xx = 0 then memo1.Lines.add(' ------ DC "Name" SQL for into temp: ------ ')
        else memo1.Lines.add(' ------ TA "Name" SQL for into temp: ------ ');

        memo1.Lines.add(theSQL);
        memo1.Lines.add(' ------ params: ----- ');

        for i := 0 to q1.ParamCount - 1 do
          memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

        if xx = 0 then
          lblspecial.caption := '[' + IntToStr(i) + '] adding DC "NAME" records system [' +
            memSpecialReps.FieldByName('system').AsString + ']'
        else
          lblspecial.caption := '[' + IntToStr(i) + '] adding TA "NAME" records system [' +
            memSpecialReps.FieldByName('system').AsString + ']';

        application.ProcessMessages;

        i := 0;
        q1.ExecSQL;

        if xx = 0 then
        begin
          memo1.Lines.Add('[' + IntToStr(i) + '] DC "NAME" records added for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');

          lblspecial.caption := '[' + IntToStr(i) + '] DC "NAME" records added for system [' +
            memSpecialReps.FieldByName('system').AsString + ']';
        end
        else
        begin
          memo1.Lines.Add('[' + IntToStr(i) + '] TA "NAME" records added for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');

          lblspecial.caption := '[' + IntToStr(i) + '] TA NAME records added for system [' +
            memSpecialReps.FieldByName('system').AsString + ']';
        end;

        application.ProcessMessages;
      except on e: exception do
        begin
          memo1.Lines.add('NAME records into temp table Err. [' + IntToStr(i) + '] system [' +
            memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);
          showmessage('failed to put names into temp space for system [' +
            memSpecialReps.FieldByName('system').AsString + '] Continue assuming bad calculation. Error --> ' + e.message);
          memspecialreps.Next;
          continue;
        end;
      end;

    // get bookings where names match up
      try
        q1.Close;
        q1.SQL.Clear;
        theSQL := '';

        if rgCountMethod.ItemIndex = 0 then
        begin
          theSQL :=
            'Select sum(vs_count_voucher) rentals From g_voucher_summary, g_client ';
        end
        else
        begin
          theSQL :=
            'Select sum(vs_count_logical) rentals From g_voucher_summary, g_client ';
        end;

        theSQL := theSQL +
          'where vs_basesys = "' + memspecialreps.fieldbyname('system').asstring + '" ';

        if (cbActiveOnly.Checked) then
          theSQL := theSQL + 'and vs_active_history = ''A'' ';

        theSQL := theSQL + ' and vs_paid_dt between :date1 and :date2 ';

        if xx = 0 then
        begin //DC
          theSQL := theSQL +
            ' and vs_direct_or_ta = ''DC'' ' +
            ' and (length(gc_email) > 0 and gc_email is not null) ' +
            ' and ( trim(upper(gc_email)) in ( select name from rpbus_temp )) ';
        end
        else // TA
        begin
          theSQL := theSQL +
            ' and vs_direct_or_ta = ''TA'' ' +
            'and (trim(upper(gc_iata)) || trim(upper(gc_lastname)) || trim(upper(gc_firstname)) ' +
            ' in ( select name from rpbus_temp )) ';
        end;

        theSQL := theSQL + ' and gc_system = vs_basesys and  ' +
          'gc_voucher = vs_voucher ';

        if xx = 0 then
        begin
          memo1.Lines.Add('getting repeat DC count for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');

          lblspecial.caption := 'getting DC repeats for sytem [' + memSpecialReps.FieldByName('system').AsString + ']';
        end
        else
        begin
          memo1.Lines.Add('getting repeat TA count for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');
          lblspecial.caption := 'getting TA repeats for system [' + memSpecialReps.FieldByName('system').AsString + ']';
        end;

        q1.SQL.Text := theSQL;
        q1.ParamByName('date1').asdate := trunc(dtprange2start.date);
        q1.ParamByName('date2').asdate := trunc(dtprange2end.date);

        memo1.Lines.add(' ------ Count SQL for repeat biz: ------ ');
        memo1.Lines.add(theSQL);
        memo1.Lines.add(' ------ params: ----- ');

        for i := 0 to q1.ParamCount - 1 do
          memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

        q1.open;

        memSpecialReps.Edit;

        if xx = 0 then
        begin
          memo1.Lines.Add('[' + q1.fieldbyname('rentals').asstring + '] DC repeat customer records found for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');
          lblspecial.caption := '[' + q1.fieldbyname('rentals').asstring + '] DC repeat customer records found for system [' +
            memSpecialReps.FieldByName('system').AsString + ']';

          memSpecialReps.FieldByName('repeat_bookingsDC').AsInteger := q1.Fields[0].asInteger;
        end
        else // TA
        begin
          memo1.Lines.Add('[' + q1.fieldbyname('rentals').asstring + '] TA repeat customer records found for system [' +
            memSpecialReps.FieldByName('system').AsString + ']');
          lblspecial.caption := '[' + q1.fieldbyname('rentals').asstring + '] TA repeat customer records found for system [' +
            memSpecialReps.FieldByName('system').AsString + ']';

          memSpecialReps.FieldByName('repeat_bookingsTA').AsInteger := q1.Fields[0].asInteger;
        end;

        memSpecialReps.Post;
      except on e: exception do
        begin
          if xx = 0 then

            memo1.Lines.add('DC "NAME" records into temp table Err. [' + IntToStr(i) + '] system [' +
              memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message)

          else
            memo1.Lines.add('TA "NAME" records into temp table Err. [' + IntToStr(i) + '] system [' +
              memSpecialReps.FieldByName('system').AsString + '] --> ' + e.message);

          memspecialreps.Next;
          continue;
        end;
      end;

    // update percentages:
      memSpecialReps.Edit;
      if xx = 0 then
      begin
        try
          crud := 0.0;
          crud := (memSpecialReps.FieldByName('repeat_bookingsDC').AsInteger / memSpecialReps.FieldByName('orig_bookingsDC').AsInteger) * 100;
          memSpecialReps.FieldByName('repeat_percentDC').AsString := FormatFloat('##0.00', crud);
        except
          memSpecialReps.FieldByName('repeat_percentDC').AsFloat := 0.0;
        end;
      end
      else
      begin
        try
          crud := 0.0;
          crud := (memSpecialReps.FieldByName('repeat_bookingsTA').AsInteger / memSpecialReps.FieldByName('orig_bookingsTA').AsInteger) * 100;
          memSpecialReps.FieldByName('repeat_percentTA').AsString := FormatFloat('##0.00', crud);
        except
          memSpecialReps.FieldByName('repeat_percentTA').AsFloat := 0.0;
        end;
      end;

      memSpecialReps.Post;
    end; // for
    memSpecialReps.Next;
  end; // while not EOF memSpecialReps

  //  final pass to calc
  //  add('repeat_bookings_DC_TA', ftInteger, 0, false); // new = repeat_bookingsDC + repeat_bookingsTA
  /// add('repeat_percent_DC_TA', ftFloat, 0, false); // new plug

  memSpecialReps.First;
  while not MemSpecialReps.Eof do
  begin
    memspecialReps.Edit;

    memSPecialReps.FieldByName('repeat_bookings_DC_TA').asINteger :=
      memSPecialReps.FieldByName('repeat_bookingsDC').asINteger +
      memSPecialReps.FieldByName('repeat_bookingsTA').asINteger;

    try
      crud := 0.0;
      crud := ((memSpecialReps.FieldByName('repeat_bookingsDC').AsInteger + memSpecialReps.FieldByName('repeat_bookingsTA').AsInteger) /
        (memSpecialReps.FieldByName('orig_bookingsDC').AsInteger + memSpecialReps.FieldByName('orig_bookingsTA').AsInteger)) * 100;

      memSpecialReps.FieldByName('repeat_percent_DC_TA').AsString := FormatFloat('##0.00', crud);
    except
      memSpecialReps.FieldByName('repeat_percent_DC_TA').AsFloat := 0.0;
    end;

    memspecialReps.Post;
    memspecialreps.Next;
  end;

  application.ProcessMessages;
  lblspecial.caption := 'printing..';
  saveACSV(dbgrid3.datasource, 'repeat business report');
  lblspecial.caption := 'done printing';
end;

procedure TfrmMainMkt.btnRunSalesProdAnalClick(Sender: TObject);
begin

  if fbolMarcello then
  begin

    RunMarcelloReport;
  end
  else
  begin
    SippAnal(btnSippAnalysisSIPP);
  end;


end;

procedure TfrmMainMkt.SippAnal(Sender: TObject);
var // funny name, but means "SIPP code analysis"
  theSQL, ListSQL, reportTitle, reportName,
    crap, temp, temp2, asdff, sortmp, sFILENAME, BIZsource, lbl2: string;
  i, x, z, d: Integer;
  yearList, OperatorMasterList: TStringList;
  asdf, DoReFetch: boolean;
begin
  DoReFetch := true;

  if (not MemSpecialReps.IsEmpty) then
  begin
    if MessageDlg('It looks like you already retrieved some data.' + #10#13 +
      'Do you need to re-fetch the data?'
      , mtConfirmation, [mbYes, mbNo], 0) = mrNo then DoReFetch := false;
  end;

  if (not cbspaShowDetail.checked) and
    (not cbspaSHowYearSysCtry.checked) and
    (not cbspaSHowYearSysCtryMstrOp.checked) and
    (not cbspaSHowYearSysCtryMstrOpSIPP.checked)
    and (not cbspaSHowYearSysCtryMstrOpSIPPSrc.checked)
    and (not cbspaSHowYearSysCtryMstrOpSource.checked)
    then
  begin
    showmessage('Select band(s) to show.');
    exit;
  end;

  reportTitle := '';
  ReportName := '';
  lbl2 := '';
  GlobFName := 'sipp_analysis';
  Build_RName_RTitle(btnSippAnalysis, Reportname, ReportTitle);

  if (DoReFetch) then
  begin
    temp := '';
    if not MSTR_List_Dates_OK(temp) then
    begin
      if messageDLG(temp + #10#13 + ' Continue anyway?',
        mtWarning, [mbYes, mbNo], 0) = mrNO then exit;
    end;
    temp := '';

    memo1.Clear;
    yearList := TStringList.Create;
    try
      dbgrid2.DataSource := nil;
      dbgrid3.DataSource := nil;

      conn.Close; // doing this drops the temp tables and should handle the 'table has been altered' error
      initQ_CloseClear(); // clear Q1.sql and open conn
      theSQL := '';
      ListSQL := '';
      bKILL := false;

      if cbUseGender.Checked then
      begin
        showmessage('gender filter can not be used for this report.');
        exit;
      end;

      if (sender = btnSippAnalysisSIPP) then
      begin
        try // adds sipp code to arg compared to other
          SetUpMemTable('SIPPANALYSIS_SIPP', memSpecialReps);
          memo1.Lines.add('SetUpMemTable for "Sipp Analysis SIPP" report OK');
        except on e: exception do
          begin
            memo1.Lines.add('SetUpMemTable --> ' + e.message);
            showmessage('SetUpMemTableErr --> ' + e.message);
            exit;
          end;
        end;
      end
      else
      begin
        try // obsolete
          SetUpMemTable('SIPPANALYSIS', memSpecialReps);
          memo1.Lines.add('SetUpMemTable for "Sipp Analysis" report OK');
        except on e: exception do
          begin
            memo1.Lines.add('SetUpMemTable --> ' + e.message);
            showmessage('SetUpMemTableErr --> ' + e.message);
            exit;
          end;
        end;
      end;

      q2.Close;
      q2.SQL.Clear;
      q1.Close;
      q1.SQL.Clear;

      try
        if (sender = btnSippAnalysisSIPP) then
        begin // group by sipp code too
          theSQL := BuildSQL(btnSippAnalysisSIPP);
          theSQL := theSQL + ' group by 1,2,3,4,5,6,7 ';
          theSQL := theSQL + ' order by 1,2,3,4,5,6,7 ';
        end
        else
        begin // no sipp in grouping  - obsolete
          theSQL := BuildSQL(btnSippAnalysis);
          theSQL := theSQL + ' group by 1,2,3,4,5,6 ';
          theSQL := theSQL + ' order by 1,2,3,4,5,6 ';
        end;
        ListSQL := theSQL;
        theSQL := theSQL + ' INTO TEMP crap_x WITH NO LOG;';
      except on e: exception do
        begin
          memo1.Lines.add('BuildSQL Sipp Analysis Err--> ' + e.message);
          showmessage('BuildSQL Sipp Analysis error --> ' + e.message);
          exit;
        end;
      end;

      try // getting distinct list of operators
          // parse/replace the where and the group by
        temp := '';
        temp2 := '';

        temp := 'Select ' + // may have to add sipp
          'DECODE(trim(vs_basesys),"", "NULL", NULL, "NULL",trim(vs_basesys)) system, ' +
          'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country_pu, ' +
          'DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
          'DECODE(trim(vs_business_source),"", "NULL", NULL, "NULL",trim(vs_business_source)) biz_source, ' +
          'DECODE(trim(vs_plus_basic),"", "NULL", NULL, "NULL",trim(vs_plus_basic)) plus_basic ';

        if (sender = btnSippAnalysisSIPP) then
          temp := temp + ', DECODE(trim(vs_sipp),"", "NULL", NULL, "NULL",trim(vs_sipp)) sipp ';

      // take everything out to the WHERE clause
        i := 0;

        if (not cbUseClientCtry.Checked) then // change 7/21/2011 to accomodate g_client join
          i := pos('FROM g_voucher_summary WHERE', ListSQL)
        else
          i := pos('From g_voucher_summary, g_client WHERE', ListSQL);

        if i = 0 then raise(exception.Create('can''t parse sipp List SQL (sipp analysis)'));
        temp2 := copy(ListSQL, i, 3500);
        temp2 := trim(temp2);

      // Remove Group BY
        i := 0;
        i := pos('group by 1,2,3,4,5', temp2);
        if i = 0 then raise(exception.Create('can''t parse sipp List SQL II (sipp analysis) '));
        temp2 := copy(temp2, 0, i - 1);
        temp2 := trim(temp2);

        if (sender = btnSippAnalysisSIPP) then
          ListSQL := temp + temp2 + ' Group by 1,2,3,4,5,6 Order by 1,2,3,4,5,6 INTO TEMP templist_x WITH NO LOG;'
        else
          ListSQL := temp + temp2 + ' Group by 1,2,3,4,5 Order by 1,2,3,4,5 INTO TEMP templist_x WITH NO LOG;';

        ListSQL := Trim(ListSQL);
      except on e: exception do
        begin
          memo1.Lines.add('BuildListSQL Err (sipp ana) --> ' + e.message);
          showmessage('BuildListSQL Err (sipp ana) --> ' + e.message);
          exit;
        end;
      end;

      addToRunLog2(Reportname, ReportTitle); // add back in later

      q1.SQL.Text := theSQL;
      q2.SQL.text := ListSQL;

    // add params because euro date bombs when we call datetoStr()
      if sender <> btnGetDataWMySQL then
      begin
        q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
        q1.ParamByName('date2').AsDate := trunc(dtpend.Date);
      // pad dates for q2:
        q2.ParamByName('date1').AsDate := trunc(dtpMasterListStart.Date);
        q2.ParamByName('date2').AsDate := trunc(dtpMasterListEnd.Date);
      end;

      if cbUseRange2.Checked then
      begin
        q1.ParamByName('dtRange2Start').AsDate := trunc(dtprange2start.Date);
        q1.ParamByName('dtRange2End').AsDate := trunc(dtprange2end.Date);
      // pad dates for q2:
        q2.ParamByName('dtRange2Start').AsDate := trunc(dtpMasterListStart.Date);
        q2.ParamByName('dtRange2End').AsDate := trunc(dtpMasterListEnd.Date);
      end;

      if cbUseRange3.Checked then
      begin
        q1.ParamByName('dtRange3Start').AsDate := trunc(dtprange3start.Date);
        q1.ParamByName('dtRange3End').AsDate := trunc(dtprange3end.Date);
      // pad dates for q2
        q2.ParamByName('dtRange3Start').AsDate := trunc(dtpMasterListStart.Date);
        q2.ParamByName('dtRange3End').AsDate := trunc(dtpMasterListEnd.Date);
      end;

      if cbUsePUDate.Checked then
      begin // paramatize this to handle different date LOCALEs
        q1.ParamByName('date3').AsDate := trunc(dtpPUStart.Date);
        q1.ParamByName('date4').AsDate := trunc(dtpPUEND.Date);

        q2.ParamByName('date3').AsDate := trunc(dtpPUStart.Date);
        q2.ParamByName('date4').AsDate := trunc(dtpPUEND.Date);
      end;

    // add 5/05/2010
      if cbUseCreateDate.Checked then
      begin
        q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
        q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
      end;

      if cbUseDropDate.Checked then // use drop date filters
      begin
        q1.ParamByName('date5').AsDate := trunc(dtpDropDtStart.Date);
        q1.ParamByName('date6').AsDate := trunc(dtpDropDtend.date);
      end;

      memo1.Lines.add(' ------sipp analysis theSQL: ------ ');
      memo1.Lines.add(theSQL);
      memo1.Lines.add(' ------ date params: ----- ');
      for i := 0 to q1.ParamCount - 1 do
        memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

      // q2
      memo1.Lines.add(' ------sipp analysis ListSQL: ------ ');
      memo1.Lines.add(ListSQL);
      memo1.Lines.add(' ------ ListSQL date params: ----- ');

      for i := 0 to q2.ParamCount - 1 do
        memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q2.Params[i].name + ' ' + q2.params[i].asstring);

      i := 0;
      lblspecial.caption := 'getting data';
      application.ProcessMessages;

      try
        q1.ExecSQL;
      except on e: exception do
        begin
          memo1.Lines.Add(IntToStr(i) + ' sipp analysis into "temp" err --> ' + e.Message);
          exit;
        end;
      end;

      memo1.Lines.Add(IntToStr(i) + ' brett analysis records added to "temp" table.');
      lblspecial.caption := IntToStr(i) + ' brett analysis records added to "temp" table.';
      application.ProcessMessages;

      i := 0;
      lblspecial.caption := 'getting sipp analysis data';
      application.ProcessMessages;

      try
        q2.ExecSQL;
      except on e: exception do
        begin
          memo1.Lines.Add(IntToStr(i) + 'operator list SQL (sipp analysis) into "temp" err --> ' + e.Message);
          exit;
        end;
      end;

      try
        q1.Close;
        q1.SQL.Clear; // get list of years from dataset first
        q1.SQL.Add('select distinct dt_year From crap_x;');

        lblspecial.caption := 'getting count for list of years';
        application.ProcessMessages;

        q1.Open;
        q1.First;
        q1.Last;
        i := 0;
        i := q1.RecordCount;
        showmessage('q1 count: ' + inttostr(i));

        memo1.Lines.Add('count for list of years: + [' + IntToStr(i) + ']');
        application.ProcessMessages;

        yearList.Clear;
        q1.First;
        while not q1.Eof do
        begin
          yearList.Add(q1.Fields[0].asstring);
          q1.Next;
        end;

        q2.Close;
        q2.SQL.Clear; // get list of years from dataset first
        q2.SQL.Add('select * From templist_x;'); // no need to order - already ordered.
        memo1.Lines.Add('getting count for list of operators etc');
        application.ProcessMessages;
        q2.Open;
      except on e: exception do
        begin
          memo1.Lines.Add('failed to retrieve temp table & get Operators --> ' + e.Message);
        end;
      end;

      lblspecial.Caption := 'setting up temp table';
      application.ProcessMessages;

      try
        memSpecialReps.EmptyTable;
        OperatorMasterList := TStringList.create;
        // operator master list, to update operator_master during "locate" below:
        memo1.Lines.Add('filling operator masterlist');
        FillOMList(OperatorMasterList);
        memo1.Lines.Add('******** master list ************ ');
        memo1.Lines.Add(OperatorMasterList.Text);
        memo1.Lines.Add('******** master list done ************ ');

        for i := 0 to yearlist.Count - 1 do
        begin // setting up & initializing container
          q2.First;
          x := 0;
          z := 0;
          while not q2.Eof do
          begin
            try
              memSpecialReps.Insert;
              memSpecialReps.FieldByName('year').asstring := trim(uppercase(yearlist[i]));
              memSpecialReps.FieldByName('system').asstring := q2.FieldByName('system').AsString;
              memSpecialReps.FieldByName('country_pu').asstring := q2.FieldByName('country_pu').AsString;

              crap := '';
              crap := OperatorMasterList.Values[trim(uppercase(q2.FieldByName('operator').AsString))];
              if (trim(crap) = '') then crap := trim(uppercase(q2.FieldByName('operator').AsString));

              memSpecialReps.FieldByName('operator_master').AsString := crap;

              memSpecialReps.FieldByName('operator').asstring := trim(uppercase(q2.FieldByName('operator').AsString));
              memSpecialReps.FieldByName('biz_source').asstring := trim(uppercase(q2.FieldByName('biz_source').AsString));
              memSpecialReps.FieldByName('plus_basic').asstring := trim(uppercase(q2.FieldByName('plus_basic').AsString));

              if (sender = btnSippAnalysisSIPP) then
              begin
                memSpecialReps.FieldByName('sipp').asstring := trim(uppercase(q2.FieldByName('sipp').AsString));
                // add 5/13
                memSpecialReps.FieldByName('short_sipp').asstring :=
                  copy(trim(uppercase(q2.FieldByName('sipp').AsString)), 1, 4);
              end;

              memSpecialReps.FieldByName('bookings').asinteger := 0;
              memSpecialReps.FieldByName('retail_total').asFloat := 0.0;
              memSpecialReps.FieldByName('discount_total').asFloat := 0.0;
              memSpecialReps.FieldByName('adjusted_retail_total').asFloat := 0.0;
              memSpecialReps.FieldByName('wholesale_total').asFloat := 0.0;
              memSpecialReps.FieldByName('commission_total').asfLoat := 0.0;
              memSpecialReps.FieldByName('deposit_total').asFloat := 0.0;
              memSpecialReps.FieldByName('duration_total').asFLoat := 0.0;
              memSpecialReps.FieldByName('profit_total').asFloat := 0.0;
              memSpecialReps.FieldByName('gross_margin').asFLoat := 0.0;
              memSpecialReps.FieldByName('net_margin').asFloat := 0.0;
              memSpecialReps.FieldByName('profit_margin').asFloat := 0.0;
              memSpecialReps.FieldByName('ave_duration').asFloat := 0.0;
              memSpecialReps.FieldByName('ave_profit').asFloat := 0.0;
              memSpecialReps.FieldByName('ave_wholesale').asFloat := 0.0;
              //https://autoeurope.jira.com/browse/DSS-2
              memSpecialReps.FieldByName('wholesale_currency').asFloat := 0.0;
              //https://autoeurope.jira.com/browse/DSS-3
              memSpecialReps.FieldByName('ave_whl_currency').asFloat := 0.0;
              //https://autoeurope.jira.com/browse/DSS-4
              memSpecialReps.FieldByName('whl_currency_daily').asFloat := 0.0;
              //https://autoeurope.jira.com/browse/DSS-5
              memSpecialReps.FieldByName('ave_exchange').asFloat := 0.0;
              memSpecialReps.FieldByName('retail_daily').asFloat := 0.0;
              memSpecialReps.FieldByName('discount_daily').asFloat := 0.0;
              memSpecialReps.FieldByName('adjusted_retail_daily').asFloat := 0.0;
              memSpecialReps.FieldByName('commission_daily').asFloat := 0.0;
              memSpecialReps.FieldByName('profit_daily').asFloat := 0.0;
              memSpecialReps.FieldByName('wholesale_daily').asFloat := 0.0;

              if (sender = btnSippAnalysisSIPP) then
              begin // special sort fields for report
                memSpecialReps.FieldByName('sort_year_sys_ctry').asString := 'n/a';
                memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop').asString := 'n/a';
                memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_source').asString := 'n/a'; // add 7/11/2011
                memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp').asString := 'n/a'; // this is short sipp
                memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp_source').asString := 'n/a';
              end
              else
                memSpecialReps.FieldByName('sort_field').asString := 'n/a';

              memSpecialReps.Post;
            except on e: exception do
              begin
                memSpecialReps.Cancel;
                memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! records added for year [' + trim(yearlist[i]) + '].');
                q2.Next;
                continue;
              end;
            end;
            inc(x);
            q2.Next;
          end; // while
          memo1.Lines.Add('[' + IntToStr(x) + '] records added for year [' + trim(yearlist[i]) + '].');
        end; // for
      except on e: exception do
        begin
          memo1.Lines.Add('generic failed to set up temp table --> ' + e.Message);
        end;
      end;
    finally
      yearList.Free;
      operatorMasterlist.Free;
      dbgrid2.DataSource := DataSource1;
      dbgrid3.DataSource := dsSpecialReps;
    end;

    lblspecial.caption := 'temp table setup complete';
    application.ProcessMessages;

    try
      q1.Close;
      q1.SQL.Clear; // get list of years from dataset first
      q1.SQL.Add('select * From crap_x;');
      lblspecial.caption := 'start processing';
      application.ProcessMessages;
      q1.Open;
      lblspecial.caption := 'query has been opened..';
      application.ProcessMessages;
      i := 0;

      q1.First;
      x := 0;
      z := 0;

      dbgrid2.DataSource := nil;
      dbgrid3.DataSource := nil;

      while not q1.Eof do
      begin
        inc(I);
        lblspecial.caption := 'processing: ' + IntToStr(i);
        if bkill then break;
        application.ProcessMessages;

        asdf := false; // may have to add sipp not sure - up to brett

        if (sender = btnSippAnalysisSIPP) then
        begin
          asdf := memSpecialReps.Locate('year;system;country_pu;operator;biz_source;plus_basic;sipp;',
            VarArrayOf([q1.FieldByName('dt_year').asstring,
            q1.FieldByName('system').asstring,
              q1.FieldByName('country_pu').asstring,
              q1.FieldByName('operator').asstring,
              q1.FieldByName('biz_source').asstring,
              q1.FieldByName('plus_basic').asstring,
              q1.FieldByName('sipp').asstring
              ]), []);
        end
        else
        begin
          asdf := memSpecialReps.Locate('year;system;country_pu;operator;biz_source;plus_basic;',
            VarArrayOf([q1.FieldByName('dt_year').asstring,
            q1.FieldByName('system').asstring,
              q1.FieldByName('country_pu').asstring,
              q1.FieldByName('operator').asstring,
              q1.FieldByName('biz_source').asstring,
              q1.FieldByName('plus_basic').asstring
              ]), []);
        end;
          //asdf := true;
        if asdf = false then
        begin
          inc(x);
          memo1.Lines.add(q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
            q1.FieldByName('country_pu').asstring + ' not found!');
            //q1.FieldByName('country').asstring + ' not found!');
        end
        else
        begin
          inc(z);
         // fill it in here
          crap := trim(uppercase(q1.FieldByName('system').asstring));

          try
            memSpecialReps.Edit;

            memSpecialReps.FieldByName('bookings').AsInteger := q1.FieldByName('bookings').asinteger;
            memSpecialReps.FieldByName('retail_total').AsFloat := q1.FieldByName('retail_total').asFloat;
            memSpecialReps.FieldByName('discount_total').AsFloat := q1.FieldByName('discount_total').asFloat;
            memSpecialReps.FieldByName('adjusted_retail_total').AsFloat := q1.FieldByName('adjusted_retail_total').asFloat;
            memSpecialReps.FieldByName('wholesale_total').AsFloat := q1.FieldByName('wholesale_total').asFloat;
            memSpecialReps.FieldByName('commission_total').AsFloat := q1.FieldByName('commission_total').asFloat;
            memSpecialReps.FieldByName('deposit_total').AsFloat := q1.FieldByName('deposit_total').asFloat;
            memSpecialReps.FieldByName('duration_total').AsFloat := q1.FieldByName('duration_total').asFloat;
            memSpecialReps.FieldByName('profit_total').AsFloat := q1.FieldByName('profit_total').asFloat;
            memSpecialReps.FieldByName('gross_margin').AsFloat := q1.FieldByName('gross_margin').asFloat;
            memSpecialReps.FieldByName('net_margin').AsFloat := q1.FieldByName('net_margin').asFloat;
            memSpecialReps.FieldByName('profit_margin').AsFloat := q1.FieldByName('profit_margin').asFloat;
            memSpecialReps.FieldByName('ave_duration').AsFloat := q1.FieldByName('ave_duration').asFloat;
            memSpecialReps.FieldByName('ave_profit').AsFloat := q1.FieldByName('ave_profit').asFloat;
            memSpecialReps.FieldByName('ave_wholesale').AsFloat := q1.FieldByName('ave_wholesale').asFloat;
            //https://autoeurope.jira.com/browse/DSS-2
            memSpecialReps.FieldByName('wholesale_currency').AsFloat
              := q1.FieldByName('wholesale_currency').asFloat;
            //https://autoeurope.jira.com/browse/DSS-3
            memSpecialReps.FieldByName('ave_whl_currency').AsFloat
              := q1.FieldByName('ave_whl_currency').asFloat;
            //https://autoeurope.jira.com/browse/DSS-4
            memSpecialReps.FieldByName('whl_currency_daily').asFloat
              := q1.FieldByName('whl_currency_daily').asFloat;
            //https://autoeurope.jira.com/browse/DSS-5
            memSpecialReps.FieldByName('ave_exchange').asFloat
              := q1.FieldByName('ave_exchange').asFloat;
            memSpecialReps.FieldByName('retail_daily').AsFloat := q1.FieldByName('retail_daily').asFloat;
            memSpecialReps.FieldByName('discount_daily').AsFloat := q1.FieldByName('discount_daily').asFloat;
            memSpecialReps.FieldByName('adjusted_retail_daily').AsFloat := q1.FieldByName('adjusted_retail_daily').asFloat;
            memSpecialReps.FieldByName('commission_daily').AsFloat := q1.FieldByName('commission_daily').asFloat;
            memSpecialReps.FieldByName('profit_daily').AsFloat := q1.FieldByName('profit_daily').asFloat;
            memSpecialReps.FieldByName('wholesale_daily').AsFloat := q1.FieldByName('wholesale_daily').asFloat;

            if (sender = btnSippAnalysisSIPP) then
            begin // special sort fields for report
              crap := '';
              BIZsource := '';
              crap := memSpecialReps.FieldByName('operator_master').AsString;
              BIZsource := memSpecialReps.FieldByName('biz_source').AsString;

              sortmp := trim(uppercase(memSPecialReps.FieldByName('year').asstring)) +
                memSPecialReps.FieldByName('system').AsString +
                memSPecialReps.FieldByName('country_pu').AsString;

              memSpecialReps.FieldByName('sort_year_sys_ctry').asString := sortmp;

              memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop').asString := sortmp + crap;

              memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_source').asString := sortmp + crap + BIZsource; // add 7/11/2011

              memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp').asString := sortmp + crap +
                trim(uppercase(memSPecialReps.FieldByName('short_sipp').AsString));

              memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp_source').asString := sortmp + crap +
                trim(uppercase(memSPecialReps.FieldByName('short_sipp').AsString) + BIZsource);

            end
            else
              memSpecialReps.FieldByName('sort_field').AsString := 'N/A';

            memSpecialReps.Post;
          except on e: exception do
            begin
              memSpecialReps.cancel;
              memo1.Lines.add('update mem analysis failed: ' + e.Message);
            end;
          end;
        end;
        q1.Next;
      end;

      memo1.Lines.Add(IntToStr(x) + ' failed locates.');
      memo1.Lines.Add(IntToStr(z) + ' OK locates.');
    except on e: exception do
      begin
        memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
      end;
    end;

    dbgrid2.DataSource := DataSource1;
    dbgrid3.DataSource := dsSpecialReps;

    if (sender = btnSippAnalysisSIPP) then
    begin // may be a redundency .. re-sorted below as well - but who cares
      lblspecial.caption := 'sorting by year, country, master_operator, operator, sipp ..';
      application.ProcessMessages;
      memSpecialReps.SortOn('year;system;country_pu;operator_master;short_sipp;biz_source;sipp;plus_basic;', mem.SortOptions);
    end
    else
    begin // obsolete / hidden button
      lblspecial.caption := 'sorting by year, country, master_operator, operator ..';
      application.ProcessMessages;
      memSpecialReps.SortOn('year;system;country_pu;operator_master;operator;biz_source;plus_basic;', mem.SortOptions);
    end;

    lblspecial.caption := 'sorting [done].';
    application.ProcessMessages;
    dbgrid2.DataSource := nil;
    dbgrid3.DataSource := nil;

    if (sender = btnSippAnalysisSIPP) then
    begin
    // have to update group fields - otherwize group breaks will break on the default 'n/a'
      lblSPecial.Caption := 'tagging group fields 0';
      d := 0;
      memSPecialReps.First;
      while not memSpecialReps.Eof do
      begin
        inc(d);
        try
          crap := '';
          BIZsource := '';
          crap := memSpecialReps.FieldByName('operator_master').AsString;
          BIZsource := memSpecialReps.FieldByName('biz_source').AsString;

          sortmp := trim(uppercase(memSPecialReps.FieldByName('year').asstring)) +
            memSPecialReps.FieldByName('system').AsString +
            memSPecialReps.FieldByName('country_pu').AsString;

          crap := trim(uppercase(crap));
          BIZSource := trim(uppercase(BIZSource));
          sortmp := trim(uppercase(sortmp));

          memSPecialReps.Edit;
          memSpecialReps.FieldByName('sort_year_sys_ctry').asString := sortmp;
          memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop').asString := sortmp + crap;
          memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_source').asString := sortmp + crap + BIZsource;

          memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp').asString := sortmp + crap +
            trim(uppercase(memSPecialReps.FieldByName('short_sipp').AsString));

          memSpecialReps.FieldByName('sort_year_sys_ctry_mstrop_sipp_source').asString := sortmp + crap +
            trim(uppercase(memSPecialReps.FieldByName('short_sipp').AsString) + BIZSource);

          memSPecialReps.Post;
          try lblSPecial.Caption := 'tagging group fields [' + IntToStr(d) + ']'; except; end;
          application.ProcessMessages;
        except on e: exception do
          begin
            memo1.Lines.Add('failed to update group fields --> ' + e.Message);
            memSPecialReps.Cancel;
          end;
        end;
        memSPecialReps.Next;
      end; // while
    end; // button sipp anal sipp
    dbgrid2.DataSource := DataSource1;
    dbgrid3.DataSource := dsSpecialReps;
  end; //    DoReFetch

   // must re-sort here based on what sub-totals are begin viewed..refetch possibilites etc
  if (sender = btnSippAnalysisSIPP) then
  begin
    if (cbspaSHowYearSysCtryMstrOpSource.Checked) then // add 7/11/2011 new special brett sort
    begin
      memSpecialReps.SortOn('year;system;country_pu;operator_master;biz_source;short_sipp;sipp;plus_basic;', mem.SortOptions);
    end
    else
    begin
      lblspecial.caption := 'sorting by year, country, master_operator, sipp, source, Plus / Basic ..';
      application.ProcessMessages;
      memSpecialReps.SortOn('year;system;country_pu;operator_master;short_sipp;biz_source;sipp;plus_basic;', mem.SortOptions);
    end;
  end;

  lblspecial.caption := 'printing..';
  application.ProcessMessages;
  sFILENAME := '';
  sFILENAME := SetDirectoryForTSUser();
  dbgrid2.DataSource := nil;
  dbgrid3.DataSource := nil;

  rptBrettSIPP2.reset;
  rptBrettCtryOp.reset;

  if (sender = btnSippAnalysisSIPP) then
  begin
    lblSippAnalSippTitle.Caption := reportTitle;
    lblSippAnalSippTitle2.Caption := reportTitle;

    lbl2 := 'Grouped by [';

    if cbspaSHowYearSysCtry.Checked then sadd(lbl2, cbspaSHowYearSysCtry.Caption + ']')
    else if cbspaSHowYearSysCtryMstrOp.Checked then sadd(lbl2, cbspaSHowYearSysCtryMstrOp.Caption + ']')
    else if cbspaSHowYearSysCtryMstrOpSource.Checked then sadd(lbl2, cbspaSHowYearSysCtryMstrOpSource.Caption + ']')
    else if cbspaSHowYearSysCtryMstrOpSIPP.Checked then sadd(lbl2, cbspaSHowYearSysCtryMstrOpSIPP.Caption + ']')
    else if cbspaSHowYearSysCtryMstrOpSIPPSrc.Checked then sadd(lbl2, cbspaSHowYearSysCtryMstrOpSIPPSrc.Caption + ']');

    if cbspaShowDetail.Checked then sadd(lbl2, ' Including Details.');

    lblHeaderGroupingSipp.Caption := lbl2;
    lblHeaderGroupingCtryOp.Caption := lbl2;

    ppGroupHeaderBand6.Visible := false; // year, sys, ctry, mstr oper
    ppGroupHEaderBand5.Visible := false; // year, sys, ctry
    ppHeaderBand9.Visible := true; // header - hide if excel after 1 print
    ppHeaderBand10.Visible := true; // header - hide if excel after 1 print
    ppDetailBand9.Visible := true; // detail band
    ppDetailBand10.Visible := true; // detail band
    ppDetailBand9.Visible := cbspaShowDetail.checked;
    ppDetailBand10.Visible := cbspaShowDetail.checked; // sipp rep
    ppGroupFooterBand6.Visible := true; // year, sys, ctry, mstr oper
    ppGroupFooterBand5.Visible := true; // year, sys, ctry
    ppGroupFooterBand22.Visible := true; // year sys ctry op sipp (sipp2 report)
    ppGroupFooterBand21.Visible := true; // year sys ctry op sipp source (sipp2 report)
    ppGroupFooterBand17.Visible := true; // year sys ctyr op source
    ppGroupFooterBand21.Visible := cbspaSHowYearSysCtryMstrOpSIPP.Checked; // sipp2 report
    ppGroupFooterBand6.Visible := cbspaSHowYearSysCtryMstrOp.Checked;
    ppGroupFooterBand5.Visible := cbspaSHowYearSysCtry.Checked;
    ppGroupFooterBand22.Visible := cbspaSHowYearSysCtryMstrOpSIPPSrc.Checked; // sipp2 report
    // add for cbspaSHowYearSysCtryMstrOpSource 7/12/2011
    ppGroupFooterBand17.Visible := cbspaSHowYearSysCtryMstrOpSource.Checked;

    case rgSpecialReportsOutput.ItemIndex of
      0: //screen
        begin
          rptBrettCtryOp.DeviceType := 'Screen';
          rptBrettCtryOp.AllowPrintToFile := false;
          rptBrettSIPP2.DeviceType := 'Screen';
          rptBrettSIPP2.AllowPrintToFile := false;
          if (cbspaSHowYearSysCtryMstrOpSIPPSrc.checked) or (cbspaSHowYearSysCtryMstrOpSIPP.Checked) then rptBrettSIPP2.Print
          else rptBrettCtryOp.Print;
        end;
      1: //csv
        begin
          asdff := '';
          if sd1.Execute then
          begin
            SaveCSV_multiFile(dsSpecialReps, sd1.FileName, reportTitle, asdff);
            globEMLFileName := sd1.FILENAME;
          end;

          if asdff > '' then showmessage(asdff);
          lblspecial.caption := 'total: [' + IntToStr(i) + '] done!';
        end;
      2: //xls
        begin
          rptBrettCtryOp.DeviceType := 'XLSXReport';

          rptBrettCtryOp.TextFileName := sFILENAME + Reportname + 'XLSX';
          globEMLFileName := rptBrettCtryOp.TextFileName;
          rptBrettCtryOp.AllowPrintToFile := True;
          rptBrettSipp2.DeviceType := 'XLSXReport';
          rptBrettSipp2.TextFileName := sFILENAME + Reportname + 'XLSX';
          globEMLFileName := rptBrettSipp2.TextFileName;
          rptBrettSipp2.AllowPrintToFile := True;
          if (cbspaSHowYearSysCtryMstrOpSIPPSrc.checked) or (cbspaSHowYearSysCtryMstrOpSIPP.Checked) then
          begin
            rptBrettSIPP2.Print
          end
          else
          begin
            rptBrettCtryOp.Print;
          end;
        end;
    else
      ;
    end; // case
  end;

  lblspecial.caption := 'done printing';
  dbgrid2.DataSource := DataSource1;
  dbgrid3.DataSource := dsSpecialReps;

  try
    q1.Close;
    q2.Close;
  except;
  end;
end; // sipp anal

procedure TfrmMainMkt.btnSippAnalysisClick(Sender: TObject);
begin
  SippAnal(btnSippAnalysis);
end;

procedure TfrmMainMkt.btnSippAnalysisSIPPClick(Sender: TObject);
begin
  SippAnal(btnSippAnalysisSIPP);
end;

procedure TfrmMainMkt.SaveACSV(DS: TDatasource; reportTitle: string);
begin
  SD1.DefaultExt := 'csv';
  SD1.Filter := 'csv|*.csv';
  if sd1.execute then
  begin
    SaveCSV(DS, sd1.FileName, reportTitle);
  end
  else
    showmessage('saveCSV cxld');
end;

procedure TfrmMainMkt.Button30Click(Sender: TObject);
begin
  showmessage(
    'Master List dates apply to "year to date", "Fleet Mix" and "combined" reports.' + #10#13 + #10#13 +
    'Select the date range for which you would like your master list set up.' + #10#13 + #10#13 +
    'For example: If you intend to run two reports with separate date ranges,' + #10#13 +
    'choose the same dates here for each report. This will allow you to cut' + #10#13 +
    'and paste to and from each report easily because they will have the same master list.' + #10#13 + #10#13 +
    'Make sure this date range is wider than the least and greatest dates accross both reports.'
    );
end;

procedure TfrmMainMkt.btnSpSetClick(Sender: TObject);
begin
  dtprange2start.Date := strtodate('1/1/2008');
  dtprange2end.Date := strtodate('3/30/2008');
  cbUseRange2.Checked := true;
  dtpStart.Date := strtodate('1/1/2007');
  dtpEnd.Date := strtodate('3/30/2007');
end;

procedure TfrmMainMkt.btnMainFillClick(Sender: TObject);
begin
  FillNOTOperatorCB(btnMainFill);
end;

procedure TfrmMainMkt.btnCSV_XMLClick(Sender: TObject);
begin
  DoOutPut(btnCSV_XML, 'XML', GlobFName);
end;

procedure TfrmMainMkt.btnFzBuildSQLClick(Sender: TObject);
var temp: string;
begin
  temp := '';
  try
    temp := BuildSQL(nil);
    memo1.Lines.Add(temp);
  except on e: exception do
    begin
      memo1.Lines.add('BuildSQL Err--> ' + e.message);
      showmessage('BuildSQL Err--> ' + e.message);
    end;
  end;
end;

procedure TfrmMainMkt.btnFzClearClick(Sender: TObject);
begin
  memo1.Clear;
end;

procedure TfrmMainMkt.btnLatestDatesClick(Sender: TObject);
var q: TFDQuery;
  SQL, tmpSQL: string;
  x: Integer;
  sl: TStringList;
  isSelected: Boolean;
  MyDate: TDate;
begin
  {   -- get list of systems and dates that the WSHE is updated to:

    select basesys, max(vchpqbath) updated_through from vouchandqueue where
    basesys in('USA','KEM','AUS','CANA','HOHO','HOT','LAC', 'NEWZ','SAF', 'UK')
    and vchpqbath > date('12/15/2010')
    group by 1
    order by 1
  }
  if not (conn.Connected) then
  begin
    showmessage('sorry, not connected to the warehouse database.');
    exit;
  end;

  isSelected := false;
  tmpSQL := 'select basesys as system, max(vchpqbath) updated_through from vouchandqueue where ';

  try
    sl := TStringList.Create();
    sl.Clear;
    Q := GetIDACQuery(conn);
    try
      sql := ' basesys in(';

      for x := 0 to clbdb.Items.Count - 1 do
      begin
        if clbdb.Checked[x] then
        begin
          isSelected := true;
          sql := sql + '"' + clbdb.items[x] + '",';
        end;
      end;

      if not isSelected then
      begin
        showmessage('Please select systems in the "databases" list.');
        exit;
      end;

      // replace last comma with a ')'
      sql := copy(sql, 1, length(sql) - 1);
      sql := sql + ')';

      MyDate := now - 30;
      MyDate := trunc(myDate);
      tmpSQL := tmpSQL + sql;
      tmpSQL := tmpSql + ' and vchpqbath > :date1 group by 1  order by 1';
      q.SQL.Clear;
      q.SQL.Add(tmpSQL);
      q.ParamByName('date1').AsDate := MyDate;

      try
        memo1.Lines.add('get dates sql:');
        memo1.Lines.add(q.SQL.text);
        memo1.Lines.add('date param: ' + Datetostr(mydate));
      except;
      end;
                       // tabs
      sl.Add('System:' + #9#9 + 'Updated To:');
      sl.add('');

      q.Open;
      q.First;
      while not q.Eof do
      begin
        sl.add(q.Fields[0].asstring + #9#9 + q.Fields[1].asstring);
        q.Next;
      end;
      showmessage(sl.Text);
    except on e: exception do
      begin
        showmessage('error getting dates --> ' + E.message);
        exit;
      end;
    end;
  finally
    q.Close;
    FreeAndNIl(q);
    if assigned(sl) then FreeAndNil(sl);
  end;
end;

procedure TfrmMainMkt.btnGRDResetClick(Sender: TObject);
begin
  if (mem.Active) and not (mem.IsEmpty) then
  begin
    if MessageDlg('This will reset your data. You will have to "get Data" again. Continue?',
      mtConfirmation, mbOKCancel, 0) = mrCancel then
    begin
      exit;
    end;
  end;

  try
    SetMemFields();
  except;
  end;

  try
    FillGroupByListBox();
  except;
  end;

  try
    mem.Close;
  except;
  end;

  try
    mem.FieldDefs.Clear;
  except;
  end;
end;

procedure TfrmMainMkt.btnFzTestClick(Sender: TObject);
var Reportname, ReportTitle: string;
begin
  try
    DoSecondPass();
  except on E: Exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

procedure TfrmMainMkt.btnClearClick(Sender: TObject);
begin
  cboperator.Items.clear;
end;

procedure TfrmMainMkt.btnCombined2Click(Sender: TObject);
var
  theSQL, ListSQL, reportTitle, reportName, crap, temp, temp2: string;
  i, x, z: Integer;
  yearList, OperatorMasterList: TStringList;
  asdf: boolean;
begin
  temp := '';
  if not MSTR_List_Dates_OK(temp) then
  begin
    if messageDLG(temp + #10#13 + ' Continue anyway?',
      mtWarning, [mbYes, mbNo], 0) = mrNO then exit;
  end;
  temp := '';

  memo1.Clear;
  yearList := TStringList.Create;

  try
    conn.Close;
    initQ_CloseClear(); // clear Q1.sql and open conn
    theSQL := '';
    ListSQL := '';
    reportTitle := '';
    ReportName := '';
    bKILL := false;

    if cbUseGender.Checked then
    begin
      showmessage('gender filter can not be used for this report.');
      exit;
    end;

    try
      SetUpMemTable('COMBINED', memSpecialReps);
      memo1.Lines.add('SetUpMemTable for "COMBINED" report OK');
    except on e: exception do
      begin
        memo1.Lines.add('SetUpMemTable --> ' + e.message);
        showmessage('SetUpMemTableErr --> ' + e.message);
        exit;
      end;
    end;

    i := 0;
    q2.Close;
    q2.SQL.Clear;
    q1.Close;
    q1.SQL.Clear;

    try
      theSQL := BuildSQL(btnCombined2);
      ListSQL := theSQL;
      theSQL := theSQL + ' INTO TEMP crap WITH NO LOG;';
    except on e: exception do
      begin
        memo1.Lines.add('BuildSQL Err--> ' + e.message);
        showmessage('BuildSQL Err--> ' + e.message);
        exit;
      end;
    end;

    try // get distinct a list of operators
         // parse/replace where and group by
      temp := '';
      temp2 := '';

      temp := 'Select DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator, ' +
        'DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country ';

      // take everything out to 'WHERE'
      i := 0;
      i := pos('FROM g_voucher_summary WHERE', ListSQL);
      if i = 0 then raise(exception.Create('can''t parse Oper/Ctry List SQL (combined)'));
      temp2 := copy(ListSQL, i, 1500);
      temp2 := trim(temp2);

      // Remove Group BY
      i := 0;
      i := pos('group by 1,2,3,4', temp2);
      if i = 0 then raise(exception.Create('can''t parse Oper/Ctry List SQL II (combined) '));
      temp2 := copy(temp2, 0, i - 1);
      temp2 := trim(temp2);

      ListSQL := temp + temp2 + ' Group by 1,2 Order by 1,2 INTO TEMP templist WITH NO LOG;';
      ListSQL := Trim(ListSQL);

      { should look something like this:

        Select
          DECODE(trim(vs_operator_code),"", "NULL", NULL, "NULL",trim(vs_operator_code)) operator,
          DECODE(trim(vs_pu_ctry),"", "NULL", NULL, "NULL",trim(vs_pu_ctry)) country
          FROM g_voucher_summary
          WHERE   vs_basesys in("AUS","CANA","DFW","EURO","HOHO","HOT","KEM","LAC","NEWZ","SAF","UK","USA") and
          (  vs_paid_dt BETWEEN date('03/01/2007') and date('03/31/2007')  OR
             vs_paid_dt BETWEEN date('03/01/2008') and date('03/31/2008')  OR
             vs_paid_dt BETWEEN date('03/01/2009') and date('03/31/2009')
          )
          Group by 1,2 Order by 1,2 -- INTO TEMP templist WITH NO LOG;
      }
    except on e: exception do
      begin
        memo1.Lines.add('BuildListSQL Err (combined) --> ' + e.message);
        showmessage('BuildListSQL Err (combined) --> ' + e.message);
        exit;
      end;
    end;

    GlobFName := 'Generic_Combined';
    Build_RName_RTitle(btnCombined2, Reportname, ReportTitle);
    addToRunLog2(Reportname, ReportTitle); // add back in later

    q1.SQL.Text := theSQL;
    q2.SQL.text := ListSQL;

    // add params because euro date bombs when we call datetoStr()
    if sender <> btnGetDataWMySQL then
    begin
      q1.ParamByName('date1').AsDate := trunc(dtpstart.Date);
      q1.ParamByName('date2').AsDate := trunc(dtpend.Date);
      // pad dates for q2:
      q2.ParamByName('date1').AsDate := trunc(dtpMasterListStart.Date);
      q2.ParamByName('date2').AsDate := trunc(dtpMasterListEnd.Date);
    end;

    if cbUseRange2.Checked then
    begin
      q1.ParamByName('dtRange2Start').AsDate := trunc(dtprange2start.Date);
      q1.ParamByName('dtRange2End').AsDate := trunc(dtprange2end.Date);
      // pad dates for q2:
      q2.ParamByName('dtRange2Start').AsDate := trunc(dtpMasterListStart.Date);
      q2.ParamByName('dtRange2End').AsDate := trunc(dtpMasterListEnd.Date);
    end;

    if cbUseRange3.Checked then
    begin
      q1.ParamByName('dtRange3Start').AsDate := trunc(dtprange3start.Date);
      q1.ParamByName('dtRange3End').AsDate := trunc(dtprange3end.Date);
      // pad dates for q2
      q2.ParamByName('dtRange3Start').AsDate := trunc(dtpMasterListStart.Date);
      q2.ParamByName('dtRange3End').AsDate := trunc(dtpMasterListEnd.Date);
    end;

    if cbUsePUDate.Checked then
    begin // paramatize this to handle EURO date
      q1.ParamByName('date3').AsDate := dtpPUStart.Date;
      q1.ParamByName('date4').AsDate := dtpPUEND.Date;
    end;

    // add 5/05/2010
    if cbUseCreateDate.Checked then
    begin
      q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
      q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
    end;

    if cbUseDropDate.Checked then // use drop date filters
    begin
      q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
      q1.ParamByName('date6').AsDate := dtpDropDtend.date;
    end;

    // end add params
    memo1.Lines.add(' ------ theSQL: ------ ');
    memo1.Lines.add(theSQL);
    memo1.Lines.add(' ------ date params: ----- ');
    for i := 0 to q1.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

    // q2
    memo1.Lines.add(' ------ ListSQL: ------ ');
    memo1.Lines.add(ListSQL);
    memo1.Lines.add(' ------ ListSQL date params: ----- ');
    for i := 0 to q2.ParamCount - 1 do
      memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q2.Params[i].name + ' ' + q2.params[i].asstring);

    i := 0;
    lblspecial.caption := 'getting data';
    application.ProcessMessages;

    try
      q1.ExecSQL;
      i := q1.RowsAffected;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + ' into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    memo1.Lines.Add(IntToStr(i) + ' records added to "temp" table.');
    lblspecial.caption := IntToStr(i) + ' records added to "temp" table.';
    application.ProcessMessages;

    i := 0;
    lblspecial.caption := 'getting List data';
    application.ProcessMessages;

    try
      q2.ExecSQL;
    except on e: exception do
      begin
        memo1.Lines.Add(IntToStr(i) + 'List SQL (combined) into "temp" err --> ' + e.Message);
        exit;
      end;
    end;

    try
      q1.Close;
      q1.SQL.Clear; // get list of years from dataset first
      q1.SQL.Add('select distinct dt_year From crap;');

      lblspecial.caption := 'getting count for list of years';
      application.ProcessMessages;

      q1.Open;
      q1.First;
      q1.Last;
      i := 0;
      i := q1.RecordCount;
      memo1.Lines.Add('count for list of years: + [' + IntToStr(i) + ']');
      application.ProcessMessages;

      yearList.Clear;
      q1.First;
      while not q1.Eof do
      begin
        yearList.Add(q1.Fields[0].asstring);
        q1.Next;
      end;

      q2.Close;
      q2.SQL.Clear; // get list of years from dataset first
      q2.SQL.Add('select * From templist order by operator, country;');
      memo1.Lines.Add('getting count for list of operators');
      application.ProcessMessages;
      q2.Open;

      q2.First;
      q2.Last;
      i := 0;
      i := q2.RecordCount;

      memo1.Lines.Add('!!! count for oper/ctry list: + [' + IntToStr(i) + ']');
      application.ProcessMessages;
    except on e: exception do
      begin
        memo1.Lines.Add('failed to retrieve temp table & get Operators --> ' + e.Message);
      end;
    end;

    lblspecial.Caption := 'setting up temp table';
    application.ProcessMessages;

    try
      memSpecialReps.EmptyTable;
      OperatorMasterList := TStringList.create;

      // operator master list, to update operator_master during "locate" below:
      memo1.Lines.Add('filling operator masterlist');
      FillOMList(OperatorMasterList);
      memo1.Lines.Add('******** master list ************ ');
      memo1.Lines.Add(OperatorMasterList.Text);
      memo1.Lines.Add('******** master list done ************ ');

      for i := 0 to yearlist.Count - 1 do
      begin
        q2.First;
        x := 0;
        z := 0;
        while not q2.Eof do
        begin
          try
            memSpecialReps.Insert;
            memSpecialReps.FieldByName('year').asstring := trim(uppercase(yearlist[i]));

            crap := '';
            crap := OperatorMasterList.Values[trim(uppercase(q2.FieldByName('operator').AsString))];

            if crap > '' then
            begin
              memSpecialReps.FieldByName('operator_master').AsString := crap;
            end
            else
            begin
              memSpecialReps.FieldByName('operator_master').asstring := trim(uppercase(q2.FieldByName('operator').AsString));
            end;

            memSpecialReps.FieldByName('operator').asstring := trim(uppercase(q2.FieldByName('operator').AsString));
            memSpecialReps.FieldByName('country').asstring := trim(uppercase(q2.FieldByName('country').AsString));
            memSpecialReps.FieldByName('USA_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('KEM_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('CANA_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('HOHO_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('UK_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('EURO_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('AUS_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('NEWZ_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('SAF_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('LAC_COUNT').asinteger := 0;
            memSpecialReps.FieldByName('HOT_COUNT').asinteger := 0;
            memSpecialReps.Post;
          except on e: exception do
            begin
              memSpecialReps.Cancel;
              memo1.Lines.Add('[' + IntToStr(x) + '] !!FAIL!! records added for year [' + trim(yearlist[i]) + '].');
              q2.Next;
              continue;
            end;
          end;
          inc(x);
          q2.Next;
        end; // while
        memo1.Lines.Add('[' + IntToStr(x) + '] records added for year [' + trim(yearlist[i]) + '].');
      end; // for
    except on e: exception do
      begin
        memo1.Lines.Add('generic failed to set up temp table --> ' + e.Message);
      end;
    end;
  finally
    yearList.Free;
    operatorMasterlist.Free;
  end;

  lblspecial.caption := 'temp table setup complete';
  application.ProcessMessages;

  try
    q1.Close;
    q1.SQL.Clear; // get list of years from dataset first
    q1.SQL.Add('select * From crap;');
    lblspecial.caption := 'start processing';
    application.ProcessMessages;
    q1.Open;
    lblspecial.caption := 'q1 is open';
    application.ProcessMessages;
    i := 0;

    q1.First;
    x := 0;
    z := 0;
    while not q1.Eof do
    begin
      inc(I);
      lblspecial.caption := 'processing: ' + IntToStr(i);
      application.ProcessMessages;

      asdf := false;
      asdf := memSpecialReps.Locate('year;operator;country;',
        VarArrayOf([trim(uppercase(q1.FieldByName('dt_year').asstring)),
        trim(uppercase(q1.FieldByName('operator').asstring)),
          trim(uppercase(q1.FieldByName('country').asstring))]), []);

      if asdf = false then
      begin
        inc(x);
        memo1.Lines.add(q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
          q1.FieldByName('country').asstring + ' not found!');
      end
      else
      begin
        inc(z);
         // fill it in here
        crap := trim(uppercase(q1.FieldByName('system').asstring));

        try
          memSpecialReps.Edit;

          if crap = 'AUS' then // insert to
          begin
            if (memSpecialReps.FieldByName('AUS_COUNT').AsInteger > 0) then
              memo1.Lines.add('AUS ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('AUS_COUNT').AsInteger :=
              memSpecialReps.FieldByName('AUS_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'CANA' then // insert to
          begin
            if (memSpecialReps.FieldByName('CANA_COUNT').AsInteger > 0) then
              memo1.Lines.add('CANA ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');


            memSpecialReps.FieldByName('CANA_COUNT').AsInteger :=
              memSpecialReps.FieldByName('CANA_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'EURO' then // insert to
          begin
            if (memSpecialReps.FieldByName('EURO_COUNT').AsInteger > 0) then
              memo1.Lines.add('EURO ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('EURO_COUNT').AsInteger :=
              memSpecialReps.FieldByName('EURO_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'HOHO' then // insert to
          begin
            if (memSpecialReps.FieldByName('HOHO_COUNT').AsInteger > 0) then
              memo1.Lines.add('HOHO ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');


            memSpecialReps.FieldByName('HOHO_COUNT').AsInteger :=
              memSpecialReps.FieldByName('HOHO_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'HOT' then // insert to
          begin
            if (memSpecialReps.FieldByName('HOT_COUNT').AsInteger > 0) then
              memo1.Lines.add('HOT ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('HOT_COUNT').AsInteger :=
              memSpecialReps.FieldByName('HOT_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'KEM' then // insert to
          begin
            if (memSpecialReps.FieldByName('KEM_COUNT').AsInteger > 0) then
              memo1.Lines.add('KEM ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('KEM_COUNT').AsInteger :=
              memSpecialReps.FieldByName('KEM_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'NEWZ' then // insert to
          begin
            if (memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger > 0) then
              memo1.Lines.add('NEWZ ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger :=
              memSpecialReps.FieldByName('NEWZ_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'SAF' then // insert to
          begin
            if (memSpecialReps.FieldByName('SAF_COUNT').AsInteger > 0) then
              memo1.Lines.add('SAF ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('SAF_COUNT').AsInteger :=
              memSpecialReps.FieldByName('SAF_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'UK' then // insert to
          begin
            if (memSpecialReps.FieldByName('UK_COUNT').AsInteger > 0) then
              memo1.Lines.add('UK ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('UK_COUNT').AsInteger :=
              memSpecialReps.FieldByName('UK_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'USA' then // insert to
          begin
            if (memSpecialReps.FieldByName('USA_COUNT').AsInteger > 0) then
              memo1.Lines.add('USA ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('USA_COUNT').AsInteger :=
              memSpecialReps.FieldByName('USA_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else if crap = 'LAC' then // insert to
          begin
            if (memSpecialReps.FieldByName('LAC_COUNT').AsInteger > 0) then
              memo1.Lines.add('LAC ' + q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
                q1.FieldByName('country').asstring + ' dupe? ');

            memSpecialReps.FieldByName('LAC_COUNT').AsInteger :=
              memSpecialReps.FieldByName('LAC_COUNT').AsInteger + q1.FieldByName('voucher_tot').AsInteger;
          end
          else
          begin
            memo1.Lines.add('[' + crap + '] is not a valid system flag. ');
            memo1.Lines.Add(q1.FieldByName('dt_year').asstring + ' ' + q1.FieldByName('operator').asstring + ' ' +
              q1.FieldByName('country').asstring + ' not updated.');
          end;

          memSpecialReps.Post;
        except on e: exception do
          begin
            memSpecialReps.cancel;
            memo1.Lines.add('update memcombined: ' + e.Message);
          end;
        end;
      end;
      q1.Next;
    end;

    memo1.Lines.Add(IntToStr(x) + ' failed locates.');
    memo1.Lines.Add(IntToStr(z) + ' OK locates.');
  except on e: exception do
    begin
      memo1.Lines.Add('failed to retrieve temp table & get years --> ' + e.Message);
    end;
  end;

  lblspecial.caption := 'sorting by year, country, master_operator, operator ..';
  application.ProcessMessages;
  memSpecialReps.SortOn('year;country;operator_master;operator;', mem.SortOptions);
  lblspecial.caption := 'sorting [done].';
  application.ProcessMessages;
  lblspecial.caption := 'printing..';
  application.ProcessMessages;
  PrintSpecialReportsRoutine(repCombinedFormatted, Reportname, ReportTitle);
  lblspecial.caption := 'total: [' + IntToStr(i) + '] done!';

  try
    q1.Close;
    q2.Close;
  except;
  end;

  application.ProcessMessages;
end; // combined formatted

procedure TfrmMainMkt.SetOrUnSetCLB(bDOCHECK: boolean);
var
  i: integer;
begin
  for i := 0 to CLBdb.Items.Count - 1 do
  begin
    if (bDOCHECK) then
      CLBdb.Checked[i] := True
    else
      CLBdb.Checked[i] := false;
  end;
end;

procedure TfrmMainMkt.btnCSVClick(Sender: TObject);
  procedure ExportDataSetToCVS(DataSet: TDataSet; const FileName: string);
  var
    fld: TField;
    lst: TStringList;
    wasActive: Boolean;
    writer: TTextWriter;
  begin
    writer := TStreamWriter.Create(FileName);
    try
      lst := TStringList.Create;
      try
        lst.QuoteChar := '"';
        lst.Delimiter := ';';
        wasActive := DataSet.Active;
        try
          DataSet.Active := true;
          DataSet.GetFieldNames(lst);
          writer.WriteLine(lst.DelimitedText);
          DataSet.First;
          while not DataSet.Eof do begin
            lst.Clear;
            for fld in DataSet.Fields do
              lst.Add(fld.Text);
            writer.WriteLine(lst.DelimitedText);
            DataSet.Next;
          end;
        finally
          DataSet.Active := wasActive;
        end;
      finally
        lst.Free;
      end;
    finally
      writer.Free;
    end;
  end;

begin
// DoOutPut(BtnCSV, 'CSV', GlobFName);
  DBGridOutput.DataSource := nil;
  GlobFName := GlobFName + '.csv';
  SaveDialog1.FileName := SetDirectoryForTSUser + 'MarketingGrid.csv';
  If SaveDialog1.Execute() then
    Begin
      GlobFName := SaveDialog1.FileName ;
  ExportDataSetToCVS(datasource1.DataSet, GlobFName);
  If messagedlg('File saved as ' + GlobFName + '. Open file?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    Begin
        ShellExecute(handle,
          'Open',
          PChar(GlobFName),
          nil,
          nil,
          SW_SHOWNORMAL);
    End;
    End;
  DBGridOutput.DataSource := DataSource1;
  //makeReport;
end;

procedure TfrmMainMkt.Build_RName_RTitle(sender: TObject; var Reportname, ReportTitle: string);
var temp: string;

  function IsaLike(InStr: string): boolean;
  var i, x: Integer;
  begin
    result := false;
    i := 0;
    x := 0;
    i := pos('%', instr);
    x := pos('_', instr);
    if (i > 0) or (x > 0) then result := true;
  end;

  function StripPerc(InStr: string): string;
  var i, x: integer;
    temp: string;
  begin
    if (InStr = '') then
    begin
      Result := '';
      exit;
    end
    else
    begin
      temp := '';
      x := length(InStr);

      for i := 1 to x do
      begin
        case InStr[i] of
          '%': continue;
        else
          temp := temp + InStr[i];
        end;
      end;
    end;
    Result := temp;
  end;
begin
  ReportName := '';
  ReportTitle := '';

  if sender = btnytd then
    begin
      ReportName := 'YTD';
      ReportTitle := 'YTD Report: ';
    end
  else if sender = btnSippAnalysis then
    begin
      ReportName := 'SIPP_ANA';
      ReportTitle := 'Sipp_analysis Report: ';
    end
  else if sender = btnCombined2 then
    begin
      ReportName := 'Combined_II';
      ReportTitle := 'Combined_II Report: ';
    end
  else if sender = btnFleetMix then
    begin
      ReportName := 'fleet_mix';
      ReportTitle := 'Fleet Mix Report: ';
    end
  else if sender = btnRPBUS then
    begin
      ReportName := 'repeat_biz';
      ReportTitle := 'Repeat Business Report: ';
    end
  else
    begin
      case rgDSType.itemIndex of
        0: begin
            ReportName := 'Sales';
            ReportTitle := 'Sales Report ';
          end;
        1: begin
            ReportName := 'Client_Email';
            ReportTitle := 'Client Email Report: ';
          end;
        2: begin
            ReportName := 'TA_Email';
            ReportTitle := 'TA Email Report: ';
          end;
      else ;
      end;
    end;

  ReportName := ReportName + '_' + GetSelectedBaseSysList();
  ReportTitle := ReportTitle + 'System ' + GetSelectedBaseSysList();

  if (rgStep1PayType.ItemIndex = 0) then
    begin
      ReportName := ReportName + '_Pd_';
      ReportTitle := ReportTitle + ', Paid Dates ' + datetoStr(dtpStart.date) + ' to ' + dateTOStr(dtpEnd.date);
    end
  else if (rgStep1PayType.ItemIndex = 2) then // pick up date
    begin
      ReportName := ReportName + '_PU_';
      ReportTitle := ReportTitle + ', Pick Up Dates ' + datetoStr(dtpStart.date) + ' to ' + dateTOStr(dtpEnd.date);
    end
  else // must be create date
    begin
      ReportName := ReportName + '_Create_';
      ReportTitle := ReportTitle + ', Create Dates ' + datetoStr(dtpStart.date) + ' to ' + dateTOStr(dtpEnd.date);
    end;

  if cbUseRange2.checked then
    begin
      ReportName := ReportName + '_DtRange2_';
      ReportTitle := ReportTitle + ', Dates 2 ' + datetoStr(dtprange2start.date) + ' to ' + dateTOStr(dtprange2end.date);
    end;

  if cbUseRange3.checked then
    begin
      ReportName := ReportName + '_DtRange3_';
      ReportTitle := ReportTitle + ', Dates 3 ' + datetoStr(dtprange3start.date) + ' to ' + dateTOStr(dtprange3end.date);
    end;

  if EditCountry.text = '*' then
    begin
      ReportName := ReportName + '_AllCtry';
      ReportTitle := ReportTitle + ', All Countries';
    end
  else
    begin
      ReportName := ReportName + '_' + stripperc(EditCountry.text);

      if IsaLike(EditCountry.text) then
        ReportTitle := ReportTitle + ', Country Like ''' + stripperc(EditCountry.text) + ''''
      else
        ReportTitle := ReportTitle + ', Country ''' + EditCOuntry.text + '''';
    end;

  if EditCity.text = '*' then
    begin
      ReportName := ReportName + '_AllCty';
      ReportTitle := ReportTitle + ', All Cities';
    end
  else
    begin
      ReportName := ReportName + '_' + stripperc(EditCity.Text);

      if IsALike(editCity.Text) then
        ReportTitle := ReportTitle + ', City Like ''' + stripperc(EditCity.Text) + ''''
      else
        ReportTitle := ReportTitle + ', City ''' + EditCity.Text + '''';
    end;

  if EditOperator.text = '*' then
  begin
    ReportName := ReportName + '_AllOp';
    ReportTitle := ReportTitle + ', All Operators';
  end
  else
  begin
    ReportName := ReportName + '_' + stripperc(EditOperator.Text);

    if IsaLike(EditOperator.Text) then
      ReportTitle := ReportTitle + ', Operator Like ''' + stripperc(EditOperator.Text) + ''''
    else
      ReportTitle := ReportTitle + ', Operator ''' + EditOperator.Text + '''';
  end;

  if cbUsePUDate.Checked then
  begin
    ReportName := ReportName + '_byPuDt';
    ReportTitle := ReportTitle + ', PU Dates ' + datetoStr(dtpPUStart.date) + '-' + dateToStr(dtpPUEnd.date);
  end;

  if cbUseGender.Checked then
  begin
    ReportName := ReportName + '_' + rggender2.Items[rggender2.ItemIndex];
    ReportTitle := ReportTitle + ', Gender ' + rggender2.Items[rggender2.ItemIndex];
  end;

  if cbUseAge.Checked then
  begin
    ReportName := ReportName + '_Age';
    ReportTitle := ReportTitle + ', Age between ' + EditAgeStart.Text + ' and ' + EditAgeEnd.Text;
  end;

  if cbUseLastMin.Checked then
  begin
    ReportName := ReportName + '_LastMin';
    ReportTitle := ReportTitle + ', DaysBetweenPD_PU <= ' + EditDaysBetween.Text;
  end;

  if cbUseDuration.Checked then
  begin
    ReportName := ReportName + '_Dur';
    ReportTitle := ReportTitle + ', Duration between ' + EditDurLow.text + ' and ' + EditDurHigh.text;
  end;

  if cbFuturesOnly.Checked then
  begin
    ReportName := ReportName + '_FuturePU';
    ReportTitle := ReportTitle + ', Future Pick Up only';
  end;

  if cbActiveOnly.Checked then
  begin
    ReportName := ReportName + '_ActiveOnly';
    ReportTitle := ReportTitle + ', Last release only';
  end;

  if cbcancelsonly.Checked then
  begin
    ReportName := ReportName + '_CXL';
    ReportTitle := ReportTitle + ', CXLD only';
  end;

  if (cbUseSIPP.Checked) or (cbSIPPWildCard.Checked) then
  begin
    ReportName := ReportName + '_SIPP';
    ReportTitle := ReportTitle +  ', by sipp';
  end;

  if cbUsePayType.Checked then
  begin
    ReportName := ReportName + '_PayType';
    ReportTitle := ReportTitle + ', by Pay Type';
  end;

  // new args 2/6/2008
  if cbUseDropDate.Checked then
  begin
    ReportName := ReportName + '_dropdt_';
    ReportTitle := ReportTitle + ', Drop Dates ' + datetoStr(dtpDropDtStart.date) + ' to ' + dateTOStr(dtpDropDtend.date);
  end;

  if cbUseDropCtry.Checked then
  begin
    ReportName := ReportName + '_dropctry_';
    if EditDropCtry.text = '*' then
    begin
      ReportTitle := ReportTitle + ', All Drop Countries';
    end
    else
    begin
      if IsaLike(EditDropCtry.text) then
        ReportTitle := ReportTitle + ', drop country Like ''' + stripperc(EditDropCtry.text) + ''''
      else
        ReportTitle := ReportTitle + ', drop country:  ''' + stripperc(EditDropCtry.text) + ''''
    end;
  end;

  if cbUseDropCity.Checked then
  begin
    ReportName := ReportName + '_dropcity_';

    if EditDropCity.text = '*' then
    begin
      ReportTitle := ReportTitle + ', All Drop Cities';
    end
    else
    begin
      if IsaLike(EditDropCity.text) then
        ReportTitle := ReportTitle + ', drop city Like ''' + stripperc(EditDropCity.text) + ''''
      else
        ReportTitle := ReportTitle + ', drop city:  ''' + stripperc(EditDropCity.text) + '''';
    end;
  end;

  case rgRestrictByEmail.ItemIndex of
    0: // dc
      begin
        ReportName := ReportName + '_restrictDC_';
        ReportTitle := ReportTitle + ', restricted to valid DC email';
      end;
    1: // ta
      begin
        ReportName := ReportName + '_restrictTA_';
        ReportTitle := ReportTitle + ', restricted to valid TA email';
      end;
  else ;
  end;

  case RgDirectTa.ItemIndex of
    0: // dc
      begin
        ReportName := ReportName + '_DirectClient_';
        ReportTitle := ReportTitle + ', direct clients only';
      end;
    1: //ta
      begin
        ReportName := ReportName + '_TA_';
        ReportTitle := ReportTitle + ', TA bookings only';
      end;
  else ;
  end;

  if cbReleaseNum.checked then
  begin
    ReportName := ReportName + '_releaseNum_';
    ReportTitle := ReportTitle + ', only release number ' + EditReleaseNum.text;
  end;

  if cbPlusBasic.checked then
  begin
    if trim(ComboPlusBasic.text) = 'plus' then
    begin
      ReportName := ReportName + '_Plus_';
      ReportTitle := ReportTitle + ', Inclusive ';
    end
    else if trim(ComboPlusBasic.text) = 'basic' then
    begin
      ReportName := ReportName + '_Basic_';
      ReportTitle := ReportTitle + ', Basic ';
    end
    else
    begin
      ;
    end;
  end;

  if cbUseIataList.checked then
  begin
    ReportName := ReportName + '_iatas_';
    ReportTitle := ReportTitle + ', specific iatas';
  end;


  if cbUseClientCtry.Checked then
  begin
    ReportName := ReportName + '_cctry_';
    ReportTitle := ReportTitle + ', client ctry [' + editClientCountry.text + ']';
  end;

  if cbFilterGroupMore.Checked then
  begin
    reportname := reportname + '_leastMost_';

    case rgoldestNewest_FilterGroup.ItemIndex of
      0: ReportTitle := ReportTitle + ', Showing least current';
      1: ReportTitle := ReportTitle + ', Showing most current';
    else ;
    end;

    case rgdateType_FilterGroup.ItemIndex of
      0: ReportTitle := ReportTitle + ' create'; // create
      1: ReportTitle := ReportTitle + ' paid'; // paid
      2: ReportTitle := ReportTitle + ' pick up'; // pick up
      3: ReportTitle := ReportTitle + ' drop off'; // drop
    else ;
    end;
    ReportTitle := ReportTitle + ' date only.';
  end;
  Memo2.Lines.Add(reportname);
end;

procedure TfrmMainMkt.btnOK_NEWClick(Sender: TObject);
var
  theSQL, reportTitle, reportName: string;
  i: Integer;
begin
  cbUseRange2.Checked := false;
  cbUseRange3.Checked := false;

  memo1.Clear;
  initQ_CloseClear(); // clear Q1.sql and open conn
  theSQL := '';
  reportTitle := '';
  ReportName := '';
  bKILL := false;

  try
    if sender = btnGetDataWMySQL then
    begin
      sb.Panels[2].Text := 'Forbidden SQL...';
      theSQL := memoSQL.Text;
    end
    else
    begin
      theSQL := BuildSQL(nil);
    end;
  except on e: exception do
    begin
      memo1.Lines.add('BuildSQL Err--> ' + e.message);
      showmessage('BuildSQL Err--> ' + e.message);
      exit;
    end;
  end;

  if rgDSType.ItemIndex = 0 then
    GlobFName := 'Sales'
  else if rgDSType.ItemIndex = 1 then
    GlobFName := 'EMail'
  else
    GlobFName := 'TvlAgent';

  Build_RName_RTitle(nil, Reportname, ReportTitle);
  addToRunLog2(Reportname, ReportTitle); // add back in later
  globFName := 'Generic_' + globFName;
  q1.SQL.Text := theSQL;

// add params because euro date bombs when we call datetoStr()
  if sender <> btnGetDataWMySQL then
  begin
  memo1.Lines.Add(TheSQL);
    q1.ParamByName('date1').AsDate := dtpstart.Date;
    q1.ParamByName('date2').AsDate := dtpend.Date;
  end;

  if cbUsePUDate.Checked then
  begin // paramatize this to handle EURO Locale
    q1.ParamByName('date3').AsDate := dtpPUStart.Date;
    q1.ParamByName('date4').AsDate := dtpPUEND.Date;
  end;

  if cbUseDropDate.Checked then // use drop date filters
  begin
    q1.ParamByName('date5').AsDate := dtpDropDtStart.Date;
    q1.ParamByName('date6').AsDate := dtpDropDtend.date;
  end;

  // add 5/05/2010
  if cbUseCreateDate.Checked then
  begin
    q1.ParamByName('dateCRTstart').AsDate := trunc(dtpCreateDtStart.date);
    q1.ParamByName('dateCRTend').AsDate := trunc(dtpCreateDtend.Date);
  end;

  // end add params
  memo1.Lines.Text := theSQL;
  memo1.Lines.add(' ------ date params ------ ');

  for i := 0 to q1.ParamCount - 1 do
    memo1.Lines.Add('param ' + IntToStr(i) + ': ' + q1.Params[i].name + ' ' + q1.params[i].asstring);

  if sender = btnGetDataWMySQL then
  begin
    ReportName := 'CUSTOM_Query';
    ReportTitle := theSQL;

    memo1.Lines.add('-------------------');
    memo1.Lines.add('USING Forbidden SQL');
    memo1.Lines.add('-------------------');
  end;

  OpenQ(false);
  SetMemFields(); // create the mem table
  FillMemFields(); // fill it up
  SortMem();

  if cbFilterGroupMore.Checked then
  begin
    DoSecondPass();
  end;

  pagecontrol1.ActivePage := TabData;
end;

procedure TfrmMainMkt.DoSecondPass();
var Temp: string;

  procedure InsertShit();
  var i: Integer;
  begin
    try
      mem2pass.Insert;
      for i := 0 to mem.FieldCount - 1 do
      begin
        mem2pass.Fields[i].Value := mem.Fields[i].Value;
      end;
      mem2pass.Post;
    except on e: exception do
      begin
        memo1.lines.Add('error inserting into mem2: ' + e.Message);
        mem2pass.Cancel;
      end;
    end;
  end;

  procedure InsertShit2();
  var i: Integer;
  begin
    try
      mem.Insert;
      for i := 0 to mem2pass.FieldCount - 1 do
      begin
        mem.Fields[i].Value := mem2pass.Fields[i].Value;
      end;
      mem.Post;
    except on e: exception do
      begin
        memo1.lines.Add('error reverse inserting into mem: ' + e.Message);
        mem.Cancel;
      end;
    end;
  end;
begin
    // plugs either least or most current record FOR A GROUP SORT and drops the rest within each group

    // this func populated mem2pass with pre-sorted first record of group_break_sort
    // then re-fills mem with resulting data set
    // asssumes records are sorted with first record of defined group as the record required to retain
  xxxxx := 0;
  if mem.IsEmpty then
  begin
    showmessage('No data to do a second pass with.');
    exit;
  end;

  try
    mem2pass.EmptyTable;
  except on e: exception do
    begin
      showmessage('2nd pass fail: ' + e.Message);
      exit;
    end
  end;

  mem.First;
  temp := mem.FieldByName('group_break_sort').AsString;
  InsertShit();

  while not mem.Eof do
  begin // reverses sort order in mem2 but that's OK since it's fed back in again below
    // here, assuming the datset is sorted by date already [whichever they seleted + ASC or DESC],
    // I am just plugging/keeping the 1st record (least or greatest date) for each group

    if temp <> mem.FieldByName('group_break_sort').AsString then InsertShit();
    temp := mem.FieldByName('group_break_sort').AsString;
    mem.Next;
  end;

    // now re-fill the original mem table:
  try
    mem.EmptyTable;
  except on e: exception do
    begin
      showmessage('2nd pass fail xx: ' + e.Message);
      exit;
    end
  end;

  xxxxx := 0;
  mem2pass.First;
  while not mem2pass.Eof do
  begin // reverses order back to original sort
    InsertShit2();
    mem2pass.Next;
  end;
end;

procedure TfrmMainMkt.dtpStartChange(Sender: TObject);
begin
  try
    if not chkLockMaster.checked then
    begin
      dtpMasterListStart.date := dtpStart.date - 1;
    end;
  except;
  end;
end;

procedure TfrmMainMkt.btnPrintClick(Sender: TObject);
var reportTitle, reportName: string;
begin
  reportTitle := '';
  ReportName := '';
  if mem.IsEmpty then
  begin
    showmessage('Please "Get Data" first by clicking the "GetData" button.');
    pagecontrol1.ActivePage := TabOutput;
    exit;
  end;

  if rgDSType.ItemIndex = 0 then
    GlobFName := 'Sales'
  else if rgDSType.ItemIndex = 1 then
    GlobFName := 'EMail'
  else
    GlobFName := 'TvlAgent';

  Build_RName_RTitle(nil, Reportname, ReportTitle);
  PrintRoutine(Reportname, ReportTitle);
end;

procedure TfrmMainMkt.OpenFile(InFIleName: string);
var buttonSelected: INteger;
begin
  if trim(InFileName) = '' then exit;

  if FileExists(inFILENAME) then
  begin
    buttonSelected := MessageDlg('Would you like to open the file?',
      mtConfirmation, mbYesNoCancel, 0);

    if buttonSelected = mrYes then
    begin
      try
        ShellExecute(handle,
          'Open',
          PChar(inFILENAME),
          nil,
          nil,
          SW_SHOWNORMAL);
      except on e: exception do
        begin
          showmessage(e.message);
        end;
      end;
    end;
  end
  else
  begin
    showmessage(inFILENAME + ' not found.');
  end;
end;

function TfrmMainMkt.InitQ_CloseClear: boolean;
begin
  result := false;
  if not conn.Connected then
  begin
    try
      connect1Click(nil);
    except on e: exception do
      begin
        showmessage('please connect first.');
        exit;
      end;
    end;
  end;

  try
    q1.Close;
    q1.SQL.Clear;
    datasource1.DataSet := Q1; // this is set to InMem - in ByCarType Speciatly (Max's Requested report)
    result := true;
  except;
  end;
  memo1.Clear;
end;

procedure TfrmMainMkt.JScroll(DataSet: TDataSet);
begin
  inc(xxxxx);
  sb.Panels[2].Text := 'processing: ' + IntToStr(xxxxx);
  application.ProcessMessages;
end;

procedure TfrmMainMkt.kbmMarcelloAfterScroll(DataSet: TDataSet);
begin
  inc(xxxxx);

  if gintProcessType = 0 then
  begin
    sb.Panels[2].Text := 'processing transaction data: ' + IntToStr(xxxxx);
  end
  else if gintProcessType = 1 then
  begin
    sb.Panels[2].Text := 'merging campaign data: ' + IntToStr(xxxxx);
  end
  else if gintProcessType = 2 then
  begin
    sb.Panels[2].Text := 'generating output buffers: ' + IntToStr(xxxxx);
  end;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.memAfterScroll(DataSet: TDataSet);
begin
  JScroll(DataSet);
end;

procedure TfrmMainMkt.mnuAboutClick(Sender: TObject);
begin
  try
    try
      Application.CreateForm(TAboutBox, AboutBox);
      AboutBox.Showmodal;
    except on e: exception do
      begin
        showmessage(e.Message + ' :Method --> TfrmMainMkt.mnuAboutClick');

      end;
    end;
  finally

    FreeAndNil(AboutBox);
  end;
end;

procedure TfrmMainMkt.mnuCloseClick(Sender: TObject);
begin
  self.close;
end;

procedure TfrmMainMkt.mnuHelpClick(Sender: TObject);
begin
  btnHelpClick(Sender); /////
end;

procedure TfrmMainMkt.OpenQ(showdata: Boolean = true);
var DREC: TDurationRec;
begin
  btnEmailSend.Enabled := false;
  xxxxx := 0;
  try
    starttime := GetTickCount();
    OpenQuery(Q1); // handle the cursor
    endtime := GetTickCount();
    DREC := DwordToDuration(ENDtime - Starttime);

    if drec.hours > 0 then
      sb.panels[1].text := 'Hrs: ' + IntToStr(drec.hours) + ' Min: ' + IntToStr(drec.minutes) +
        ' Sec: ' + IntToStr(drec.seconds)
    else
      sb.panels[1].text := 'Min: ' + IntToStr(drec.minutes) + ' Sec: ' + IntToStr(drec.seconds);

    sb.Panels[2].Text := 'get data = OK';

    if not (Q1.IsEmpty) and (showdata) then
    begin
      pagecontrol1.ActivePage := tabdata;
      frmMainMkt.FormStyle := fsStayOnTop;
      application.processmessages;
      frmMainMkt.FormStyle := fsNormal;
    end
    else if (Q1.IsEmpty) then
      showmessage('no data returned.');

    application.processmessages;
  except on e: exception do
      showmessage('Failed to open. Err -> 2' + E.Message);
  end;
end;

procedure TfrmMainMkt.SetMemFields(Marcillo: boolean = false);
begin
  // close, Clear Field Defs, add field defs by name
  try
    Mem.EmptyTable;
  except;
  end;

  try
    mem.Close;
    mem2pass.Close;
    mem.FieldDefs.Clear;
    mem2pass.FieldDefs.Clear;

    mem.FieldDefs.Add('basesys', ftString, 5, true);
    mem.FieldDefs.Add('voucher', ftString, 20, true);
    mem.FieldDefs.Add('date_created', ftDate, 0, false); // vchdate
    mem.FieldDefs.Add('date_of_transaction', ftDate, 0, false); // paid date OR vchpqbath
    mem.FieldDefs.Add('updated_dt', ftDate, 0, false); // vcstdte1

    mem.FieldDefs.Add('date_of_svc', ftDate, 0, false); // pick up date
    mem.FieldDefs.Add('time_of_svc', ftString, 25, false);

    mem.FieldDefs.Add('do_dt', ftDate, 0, false);
    mem.FieldDefs.Add('do_time', ftString, 25, false);

    mem.FieldDefs.Add('duration', ftFloat, 0, false);
    mem.FieldDefs.Add('lead_days_create_pu', ftFloat, 0, false);


    mem.FieldDefs.add('dt_year', ftString, 5, false); // add year 04/27/2011

    mem.FieldDefs.Add('lead_days_pdpu', ftFloat, 0, false); // set in sql

    mem.FieldDefs.Add('country_pu', ftstring, 2, false); //vopctry1
    mem.FieldDefs.Add('city_pu', ftstring, 50, false); // vopcity1
    mem.FieldDefs.Add('loc_pu', ftstring, 2, false); // voploc1

    mem.FieldDefs.Add('do_ctry', ftString, 2, false);
    mem.FieldDefs.Add('do_city', ftString, 50, false);
    mem.FieldDefs.Add('do_loc', ftString, 2, false);
    mem.FieldDefs.Add('sipp', ftString, 10, false);
    mem.FieldDefs.Add('sipp_description', ftString, 100, false);
    mem.FieldDefs.Add('car_name', ftString, 100, false);
    mem.FieldDefs.Add('operator', ftstring, 20, false); // vopid
    mem.FieldDefs.Add('oper_car_code', ftString, 30, false); // vopcomt1

    mem.FieldDefs.Add('service_type', ftString, 15, false);
    mem.FieldDefs.Add('source', ftString, 2, false); // vopsubt
    mem.FieldDefs.Add('product_type', ftString, 10, false);
    mem.FieldDefs.Add('business_source', ftString, 10, false);
    mem.FieldDefs.Add('payment_type', ftInteger, 0, false);
    mem.FieldDefs.Add('cc_name', ftString, 18, false); //payment type

    // add balance of fields from g_Voucher_summary and g_vendor:
    // 1.28.2008
    mem.FieldDefs.Add('active_history', ftString, 1, false);
    mem.FieldDefs.Add('p_or_q', ftString, 1, false);

    mem.FieldDefs.Add('fp_pp', ftString, 2, false);
    mem.FieldDefs.Add('plus_basic', ftString, 1, false);
    mem.FieldDefs.Add('rid', ftInteger, 0, false);
    mem.FieldDefs.Add('auth_code', ftString, 10, false);
    mem.FieldDefs.Add('confirmation', ftString, 40, false);

    mem.FieldDefs.Add('op_rate_code', ftString, 15, false);
    mem.FieldDefs.Add('bus_account_number', ftString, 20, false);
    mem.FieldDefs.Add('num_passengers', ftInteger, 0, false);

    mem.FieldDefs.Add('home_curr', ftString, 3, false); // "system" not operator
    mem.FieldDefs.Add('vat_tax_rt', ftFloat, 0, false);
    mem.FieldDefs.Add('home_ctry', ftString, 2, false);
    mem.FieldDefs.Add('retail_total', ftFloat, 0, false);
    mem.FieldDefs.Add('base_rate_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('discount_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('insurance_tot_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('tax_tot_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('pu_fee_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('do_fee_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('xtra_fee_retail', ftFloat, 0, false);
    mem.FieldDefs.Add('base_rate_wholesale', ftFloat, 0, false);
    mem.FieldDefs.Add('insurance_tot_wholesale', ftFloat, 0, false);
    mem.FieldDefs.Add('tax_tot_wholesale', ftFloat, 0, false);
    mem.FieldDefs.Add('pu_fee_wholesale', ftFloat, 0, false);
    mem.FieldDefs.Add('do_fee_wholesale', ftFloat, 0, false);
    mem.FieldDefs.Add('wholesale_total', ftFloat, 0, false);
    mem.FieldDefs.Add('operator_due', ftFloat, 0, false);

    mem.FieldDefs.Add('commission', ftFloat, 0, false);
    mem.FieldDefs.Add('comm_due', ftFloat, 0, false);
    mem.FieldDefs.Add('comm_percent', ftFloat, 0, false);

    mem.FieldDefs.Add('profit', ftFloat, 0, false);

    mem.FieldDefs.Add('deposit_amt', ftFloat, 0, false);
    mem.FieldDefs.Add('waived_amt', ftFloat, 0, false);
    mem.FieldDefs.Add('deposit_after_waive', ftFloat, 0, false);
    mem.FieldDefs.Add('due_at_pu', ftFloat, 0, false);
    mem.FieldDefs.Add('deferred_amt', ftFloat, 0, false);

    mem.FieldDefs.Add('gross_revenue', ftFloat, 0, false);
    mem.FieldDefs.Add('operator_cost', ftFloat, 0, false);
    mem.FieldDefs.Add('opdue_curr', ftFloat, 0, false);
    mem.FieldDefs.Add('curr_type', ftString, 5, false);
    mem.FieldDefs.Add('curr_exch_rate', ftFloat, 0, false);

    mem.FieldDefs.Add('count_new', ftInteger, 0, false);
    mem.FieldDefs.Add('count_cxl', ftInteger, 0, false);
    mem.FieldDefs.Add('count_change', ftInteger, 0, false);
    mem.FieldDefs.Add('vcount', ftInteger, 0, false);
    mem.FieldDefs.Add('direct_or_ta', ftString, 2, false);
    mem.FieldDefs.Add('member_account', ftString, 10, false);
    mem.FieldDefs.Add('consortium', ftString, 10, false);
    mem.FieldDefs.Add('vcspecrequest', ftString, 100, false); // special requests

    //client
    mem.FieldDefs.add('c_fullname', ftString, 122, false); // gc_lastname, gc_firstname, gc_initial
    mem.FieldDefs.add('c_prefix', ftString, 8, false); // gc_surname is used for Mr.|Mrs. etc
    mem.FieldDefs.Add('c_addr1', ftString, 60, false);
    mem.FieldDefs.Add('c_addr2', ftString, 60, false);
    mem.FieldDefs.Add('c_prov', ftString, 10, false);
    mem.FieldDefs.Add('c_city', ftString, 50, false);
    mem.FieldDefs.Add('c_postal', ftString, 20, false);
    mem.FieldDefs.add('c_country', ftString, 4, false); // add 2/19/2008
    mem.FieldDefs.Add('c_email', ftString, 250, false);
    mem.FieldDefs.Add('c_phone', ftString, 40, false);
    mem.FieldDefs.Add('c_age', ftInteger, 0, false); //voiamt4 = age

    //ta
    mem.FieldDefs.Add('iata', ftstring, 9, false);
    mem.FieldDefs.Add('gv_name', ftString, 30, false);
    mem.FieldDefs.Add('gv_legal_name', ftString, 30, false);
    mem.FieldDefs.Add('gv_contact_name', ftString, 30, false);
    mem.FieldDefs.Add('gv_addr1', ftString, 30, false);
    mem.FieldDefs.Add('gv_addr2', ftString, 30, false);
    mem.FieldDefs.Add('gv_addr3', ftString, 30, false);
    mem.FieldDefs.Add('gv_addr4', ftString, 30, false);
    mem.FieldDefs.Add('gv_city', ftString, 18, false);
    mem.FieldDefs.Add('gv_prov', ftString, 3, false);
    mem.FieldDefs.Add('gv_postal', ftString, 10, false);
    mem.FieldDefs.Add('gv_country', ftString, 30, false);
    mem.FieldDefs.Add('gv_phone_prefix', ftString, 6, false);
    mem.FieldDefs.Add('gv_phone_num', ftString, 15, false);
    mem.FieldDefs.Add('gv_fax_prefix', ftString, 6, false);
    mem.FieldDefs.Add('gv_fax_num', ftString, 15, false);
    mem.FieldDefs.Add('gv_email', ftString, 255, false);
    mem.FieldDefs.Add('group_break_sort', ftString, 512, false); // values set according to how to group ...

    // add 06/23/2010 for aussie
    mem.FieldDefs.Add('promo1', ftString, 10, false);
    mem.FieldDefs.Add('promo2', ftString, 10, false);
    mem.FieldDefs.Add('promo3', ftString, 10, false);

    //add the marcello report info here 06/18/2012
    if Marcillo then
    begin
      mem.FieldDefs.Add('pay_dt_year', ftDate, 0, false);
      mem.FieldDefs.Add('gc_ctry', ftString, 25, false);
      mem.FieldDefs.Add('vs_retail_total', ftFloat, 0, false);
      mem.FieldDefs.Add('vs_profit', ftFloat, 0, false);
      mem.FieldDefs.Add('vs_count_voucher', ftInteger, 0, false);
        //mem.FieldDefs.Add('gv_name', ftString, 25, false);
      mem.FieldDefs.Add('campid', ftInteger, 0, false);
      mem.FieldDefs.Add('campaign', ftString, 25, false);
      mem.FieldDefs.Add('host', ftString, 25, false);
    end;

    mem2pass.FieldDefs := mem.FieldDefs;
    mem.CreateTable; // required
    mem2pass.CreateTable;
    mem.Open;
    mem2pass.Open;
    FindFieldPositions(mem, Marcillo);

  except on e: Exception do
    begin
      showmessage('set mem fields --> ' + e.Message);
    end;
  end;
end;

procedure TfrmMainMkt.SetMemFieldsMarcillo();
begin
  // close, Clear Field Defs, add field defs by name
  try
    kbmMarcello.EmptyTable;
  except;
  end;

  try
    kbmMarcello.Close;
    kbmMarcello.FieldDefs.Clear;

    kbmMarcello.FieldDefs.Add('basesys', ftString, 5, true);
    kbmMarcello.FieldDefs.Add('voucher', ftString, 20, true);
    kbmMarcello.FieldDefs.Add('date_created', ftDate, 0, false); // vchdate
    kbmMarcello.FieldDefs.Add('date_of_transaction', ftDate, 0, false); // paid date OR vchpqbath
    kbmMarcello.FieldDefs.Add('updated_dt', ftDate, 0, false); // vcstdte1

    kbmMarcello.FieldDefs.Add('date_of_svc', ftDate, 0, false); // pick up date
    kbmMarcello.FieldDefs.Add('time_of_svc', ftString, 25, false);

    kbmMarcello.FieldDefs.Add('do_dt', ftDate, 0, false);
    kbmMarcello.FieldDefs.Add('do_time', ftString, 25, false);

    kbmMarcello.FieldDefs.Add('duration', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('lead_days_create_pu', ftFloat, 0, false);


    kbmMarcello.FieldDefs.add('dt_year', ftString, 5, false); // add year 04/27/2011

    kbmMarcello.FieldDefs.Add('lead_days_pdpu', ftFloat, 0, false); // set in sql

    kbmMarcello.FieldDefs.Add('country_pu', ftstring, 2, false); //vopctry1
    kbmMarcello.FieldDefs.Add('city_pu', ftstring, 50, false); // vopcity1
    kbmMarcello.FieldDefs.Add('loc_pu', ftstring, 2, false); // voploc1

    kbmMarcello.FieldDefs.Add('do_ctry', ftString, 2, false);
    kbmMarcello.FieldDefs.Add('do_city', ftString, 50, false);
    kbmMarcello.FieldDefs.Add('do_loc', ftString, 2, false);
    kbmMarcello.FieldDefs.Add('sipp', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('sipp_description', ftString, 100, false);
    kbmMarcello.FieldDefs.Add('car_name', ftString, 100, false);
    kbmMarcello.FieldDefs.Add('operator', ftstring, 20, false); // vopid
    kbmMarcello.FieldDefs.Add('oper_car_code', ftString, 30, false); // vopcomt1

    kbmMarcello.FieldDefs.Add('service_type', ftString, 15, false);
    kbmMarcello.FieldDefs.Add('source', ftString, 2, false); // vopsubt
    kbmMarcello.FieldDefs.Add('product_type', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('business_source', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('payment_type', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('cc_name', ftString, 18, false); //payment type

    // add balance of fields from g_Voucher_summary and g_vendor:
    // 1.28.2008
    kbmMarcello.FieldDefs.Add('active_history', ftString, 1, false);
    kbmMarcello.FieldDefs.Add('p_or_q', ftString, 1, false);

    kbmMarcello.FieldDefs.Add('fp_pp', ftString, 2, false);
    kbmMarcello.FieldDefs.Add('plus_basic', ftString, 1, false);
    kbmMarcello.FieldDefs.Add('rid', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('auth_code', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('confirmation', ftString, 40, false);

    kbmMarcello.FieldDefs.Add('op_rate_code', ftString, 15, false);
    kbmMarcello.FieldDefs.Add('bus_account_number', ftString, 20, false);
    kbmMarcello.FieldDefs.Add('num_passengers', ftInteger, 0, false);

    kbmMarcello.FieldDefs.Add('home_curr', ftString, 3, false); // "system" not operator
    kbmMarcello.FieldDefs.Add('vat_tax_rt', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('home_ctry', ftString, 2, false);
    kbmMarcello.FieldDefs.Add('retail_total', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('base_rate_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('discount_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('insurance_tot_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('tax_tot_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('pu_fee_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('do_fee_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('xtra_fee_retail', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('base_rate_wholesale', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('insurance_tot_wholesale', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('tax_tot_wholesale', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('pu_fee_wholesale', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('do_fee_wholesale', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('wholesale_total', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('operator_due', ftFloat, 0, false);

    kbmMarcello.FieldDefs.Add('commission', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('comm_due', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('comm_percent', ftFloat, 0, false);

    kbmMarcello.FieldDefs.Add('profit', ftFloat, 0, false);

    kbmMarcello.FieldDefs.Add('deposit_amt', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('waived_amt', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('deposit_after_waive', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('due_at_pu', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('deferred_amt', ftFloat, 0, false);

    kbmMarcello.FieldDefs.Add('gross_revenue', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('operator_cost', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('opdue_curr', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('curr_type', ftString, 5, false);
    kbmMarcello.FieldDefs.Add('curr_exch_rate', ftFloat, 0, false);

    kbmMarcello.FieldDefs.Add('count_new', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('count_cxl', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('count_change', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('vcount', ftInteger, 0, false);
    kbmMarcello.FieldDefs.Add('direct_or_ta', ftString, 2, false);
    kbmMarcello.FieldDefs.Add('member_account', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('consortium', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('vcspecrequest', ftString, 100, false); // special requests

    //client
    kbmMarcello.FieldDefs.add('c_fullname', ftString, 122, false); // gc_lastname, gc_firstname, gc_initial
    kbmMarcello.FieldDefs.add('c_prefix', ftString, 8, false); // gc_surname is used for Mr.|Mrs. etc
    kbmMarcello.FieldDefs.Add('c_addr1', ftString, 60, false);
    kbmMarcello.FieldDefs.Add('c_addr2', ftString, 60, false);
    kbmMarcello.FieldDefs.Add('c_prov', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('c_city', ftString, 50, false);
    kbmMarcello.FieldDefs.Add('c_postal', ftString, 20, false);
    kbmMarcello.FieldDefs.add('c_country', ftString, 4, false); // add 2/19/2008
    kbmMarcello.FieldDefs.Add('c_email', ftString, 250, false);
    kbmMarcello.FieldDefs.Add('c_phone', ftString, 40, false);
    kbmMarcello.FieldDefs.Add('c_age', ftInteger, 0, false); //voiamt4 = age

    //ta
    kbmMarcello.FieldDefs.Add('iata', ftstring, 9, false);
    kbmMarcello.FieldDefs.Add('gv_name', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_legal_name', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_contact_name', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_addr1', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_addr2', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_addr3', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_addr4', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_city', ftString, 18, false);
    kbmMarcello.FieldDefs.Add('gv_prov', ftString, 3, false);
    kbmMarcello.FieldDefs.Add('gv_postal', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('gv_country', ftString, 30, false);
    kbmMarcello.FieldDefs.Add('gv_phone_prefix', ftString, 6, false);
    kbmMarcello.FieldDefs.Add('gv_phone_num', ftString, 15, false);
    kbmMarcello.FieldDefs.Add('gv_fax_prefix', ftString, 6, false);
    kbmMarcello.FieldDefs.Add('gv_fax_num', ftString, 15, false);
    kbmMarcello.FieldDefs.Add('gv_email', ftString, 255, false);
    kbmMarcello.FieldDefs.Add('group_break_sort', ftString, 512, false); // values set according to how to group ...

    // add 06/23/2010 for aussie
    kbmMarcello.FieldDefs.Add('promo1', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('promo2', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('promo3', ftString, 10, false);
    kbmMarcello.FieldDefs.Add('pay_dt_year', ftString, 5, false);
    //kbmMarcello.FieldDefs.Add('pay_dt_year', ftDate, 0, false);
    kbmMarcello.FieldDefs.Add('gc_ctry', ftString, 25, false);
    kbmMarcello.FieldDefs.Add('vs_retail_total', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('vs_profit', ftFloat, 0, false);
    kbmMarcello.FieldDefs.Add('vs_count_voucher', ftInteger, 0, false);
    //mem.FieldDefs.Add('gv_name', ftString, 25, false);
    kbmMarcello.FieldDefs.Add('campid', ftString, 25, false);
    kbmMarcello.FieldDefs.Add('campaign', ftString, 25, false);
    kbmMarcello.FieldDefs.Add('host', ftString, 25, false);

    //mem2pass.FieldDefs := mem.FieldDefs;
    kbmMarcello.CreateTable; // required
    //mem2pass.CreateTable;
    kbmMarcello.Open;
    //mem2pass.Open;
    //FindFieldPositions(kbmMarcello,true);

  except on e: Exception do
    begin
      showmessage('set mem fields --> ' + e.Message);
    end;
  end;
end;


procedure TfrmMainMkt.ppDBTextMarcilleoCampGetText(Sender: TObject;
  var Text: string);
begin

  if not AnsiContainsText(Text, 'SEO') then
  begin
    Text := '';
  end;
end;

procedure TfrmMainMkt.ppDetailBand1BeforeGenerate(Sender: TObject);
var temp: TDateTime;
begin
  temp := q1.FieldByName('dtr_run_date').AsDateTime;
  if temp >= LatestRunDate then
  begin
    LatestRunDate := temp;

    lblLast.Caption := 'Last run was ' +
      q1.FieldByName('dtr_user_name').AsString + ' on ' +
      FormatDateTime('mm/dd/yyyy', LatestRunDate) + ' at ' +
      FormatDateTime('HH:MM AM/PM', LatestRunDate);
  end;
end;


procedure TfrmMainMkt.ppGroupFooterBand10BeforePrint(Sender: TObject);
begin
  // year totals:
  // ave duration start:
  try // USA: sum of ave duration / sum of vcount               // total ave duration / voucher count
    ppLabel120.Caption := FormatFloat({'###,##0.00'}_NUM_FORMAT_FIVE_POUND, (ppDBCalc159.Value / ppDBCalc71.Value));
    if length(trim(ppLabel120.Caption)) <= 5 then
      ppLabel120.Caption := '     ' + ppLabel120.Caption;
  except ppLabel120.Caption := '     0.00';
  end;

  try // kem
    ppLabel121.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc160.Value / ppDBCalc72.Value));
    if length(trim(ppLabel121.caption)) <= 5 then
      ppLabel121.caption := '     ' + ppLabel121.caption;
  except ppLabel121.caption := '     0.00';
  end;

  try // cana
    ppLabel122.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc161.Value / ppDBCalc73.Value));
    if length(trim(ppLabel122.caption)) <= 5 then
      ppLabel122.caption := '     ' + ppLabel122.caption;
  except ppLabel122.caption := '     0.00';
  end;

  try // hoho
    ppLabel123.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc162.Value / ppDBCalc74.Value));
    if length(trim(ppLabel123.caption)) <= 5 then
      ppLabel123.caption := '     ' + ppLabel123.caption;
  except ppLabel123.caption := '     0.00';
  end;

  try // uk
    ppLabel124.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc163.Value / ppDBCalc75.Value));
    if length(trim(ppLabel124.caption)) <= 5 then
      ppLabel124.caption := '     ' + ppLabel124.caption;
  except ppLabel124.caption := '     0.00';
  end;

  try // euro
    ppLabel125.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc164.Value / ppDBCalc76.Value));
    if length(trim(ppLabel125.caption)) <= 5 then
      ppLabel125.caption := '     ' + ppLabel125.caption;
  except ppLabel125.caption := '     0.00';
  end;

  try // aus
    ppLabel126.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc165.Value / ppDBCalc77.Value));
    if length(trim(ppLabel126.caption)) <= 5 then
      ppLabel126.caption := '     ' + ppLabel126.caption;
  except ppLabel126.caption := '     0.00';
  end;

  try // nz
    ppLabel127.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc166.Value / ppDBCalc78.Value));
    if length(trim(ppLabel127.caption)) <= 5 then
      ppLabel127.caption := '     ' + ppLabel127.caption;
  except ppLabel127.caption := '     0.00';
  end;

  try // za
    ppLabel128.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc167.Value / ppDBCalc79.Value));
    if length(trim(ppLabel128.caption)) <= 5 then
      ppLabel128.caption := '     ' + ppLabel128.caption;
  except ppLabel128.caption := '     0.00';
  end;

  try // lax
    ppLabel129.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc168.Value / ppDBCalc80.Value));
    if length(trim(ppLabel129.caption)) <= 5 then
      ppLabel129.caption := '     ' + ppLabel129.caption;
  except ppLabel129.caption := '     0.00';
  end;

  try // hot
    ppLabel130.caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc169.Value / ppDBCalc82.Value));
    if length(trim(ppLabel130.caption)) <= 5 then
      ppLabel130.caption := '     ' + ppLabel130.caption;
  except ppLabel130.caption := '     0.00';
  end;
  // end ave dur

  // ave pft start
  // sum of ave profit / sum of vcount -
  try //USA
    ppLabel131.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc170.Value / ppDBCalc71.Value));
    if length(trim(ppLabel131.Caption)) <= 5 then
      ppLabel131.Caption := '     ' + ppLabel131.Caption;
  except ppLabel131.Caption := '     0.00';
  end;

  try // kem
    ppLabel132.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc171.Value / ppDBCalc72.Value));
    if length(trim(ppLabel132.Caption)) <= 5 then
      ppLabel132.Caption := '     ' + ppLabel132.Caption;
  except ppLabel132.Caption := '     0.00';
  end;

  try // cana
    ppLabel166.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc172.Value / ppDBCalc73.Value));
    if length(trim(ppLabel166.Caption)) <= 5 then
      ppLabel166.Caption := '     ' + ppLabel166.Caption;
  except ppLabel166.Caption := '     0.00';
  end;

  try // hoho
    ppLabel183.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc173.Value / ppDBCalc74.Value));
    if length(trim(ppLabel183.Caption)) <= 5 then
      ppLabel183.Caption := '     ' + ppLabel183.Caption;
  except ppLabel183.Caption := '     0.00';
  end;


  try // uk
    ppLabel184.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc174.Value / ppDBCalc75.Value));
    if length(trim(ppLabel184.Caption)) <= 5 then
      ppLabel184.Caption := '     ' + ppLabel184.Caption;
  except ppLabel184.Caption := '     0.00';
  end;

  try // euro
    ppLabel191.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc175.Value / ppDBCalc76.Value));
    if length(trim(ppLabel191.Caption)) <= 5 then
      ppLabel191.Caption := '     ' + ppLabel191.Caption;
  except ppLabel191.Caption := '     0.00';
  end;

  try // aus
    ppLabel192.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc176.Value / ppDBCalc77.Value));
    if length(trim(ppLabel192.Caption)) <= 5 then
      ppLabel192.Caption := '     ' + ppLabel192.Caption;
  except ppLabel192.Caption := '     0.00';
  end;

  try // nz
    ppLabel193.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc177.Value / ppDBCalc78.Value));
    if length(trim(ppLabel193.Caption)) <= 5 then
      ppLabel193.Caption := '     ' + ppLabel193.Caption;
  except ppLabel193.Caption := '     0.00';
  end;

  try // za
    ppLabel194.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc178.Value / ppDBCalc79.Value));
    if length(trim(ppLabel194.Caption)) <= 5 then
      ppLabel194.Caption := '     ' + ppLabel194.Caption;
  except ppLabel194.Caption := '     0.00';
  end;

  try // lac
    ppLabel195.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc179.Value / ppDBCalc80.Value));
    if length(trim(ppLabel195.Caption)) <= 5 then
      ppLabel195.Caption := '     ' + ppLabel195.Caption;
  except ppLabel195.Caption := '     0.00';
  end;

  try // lac
    ppLabel196.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc180.Value / ppDBCalc82.Value));
    if length(trim(ppLabel196.Caption)) <= 5 then
      ppLabel196.Caption := '     ' + ppLabel196.Caption;
  except ppLabel196.Caption := '     0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand11BeforePrint(Sender: TObject);
begin
  ppLabel162.Caption := getCountryName(trim(Uppercase(ppDBText129.Text)), '*');

  try // sum of ave duration / sum of vcount
    lblAveDUrationCTRY.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc64.Value / ppDBCalc63.Value)); // ave profit for web
    if trim(lblAveDUrationCTRY.Caption) = '0.00' then lblAveDUrationCTRY.Caption := '   0.00';
  except lblAveDUrationCTRY.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblAveProfitCTRY.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (dbcalcCTRYPFT.Value / ppDBCalc63.Value)); // ave profit for web
    if trim(lblAveProfitCTRY.Caption) = '0.00' then lblAveProfitCTRY.Caption := '   0.00';
  except lblAveProfitCTRY.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblaveopDUeCTRY.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (dbcalcopdue.Value / ppDBCalc63.Value)); // ave profit for web
    if trim(lblaveopDUeCTRY.Caption) = '0.00' then lblaveopDUeCTRY.Caption := '   0.00';
  except lblaveopDUeCTRY.Caption := '   0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand12BeforePrint(Sender: TObject);
begin
  try // sum of ave duration / sum of vcount               // total ave duration / voucher count
    lblUSAaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc115.Value / ppDBCalc83.Value));
    if length(trim(lblUSAaveDuration.Caption)) <= 5 then
      lblUSAaveDuration.Caption := '     ' + lblUSAaveDuration.Caption;
  except lblUSAaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblkemaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc116.Value / ppDBCalc84.Value));
    if length(trim(lblkemaveDuration.Caption)) <= 5 then
      lblkemaveDuration.Caption := '     ' + lblkemaveDuration.Caption;
  except lblkemaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblcanaaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc117.Value / ppDBCalc85.Value));
    if length(trim(lblcanaaveDuration.Caption)) <= 5 then
      lblcanaaveDuration.Caption := '     ' + lblcanaaveDuration.Caption;
  except lblcanaaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblhohoaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc118.Value / ppDBCalc86.Value));
    if length(trim(lblhohoaveDuration.Caption)) <= 5 then
      lblhohoaveDuration.Caption := '     ' + lblhohoaveDuration.Caption;
  except lblhohoaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblUKaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc119.Value / ppDBCalc87.Value));
    if length(trim(lblUKaveDuration.Caption)) <= 5 then
      lblUKaveDuration.Caption := '     ' + lblUKaveDuration.Caption;
  except lblUKaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblEUROaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc120.Value / ppDBCalc88.Value));
    if length(trim(lblEUROaveDuration.Caption)) <= 5 then
      lblEUROaveDuration.Caption := '     ' + lblEUROaveDuration.Caption;
  except lblEUROaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblAUSaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc121.Value / ppDBCalc89.Value));
    if length(trim(lblAUSaveDuration.Caption)) <= 5 then
      lblAUSaveDuration.Caption := '     ' + lblAUSaveDuration.Caption;
  except lblAUSaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblNEWZaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc122.Value / ppDBCalc90.Value));
    if length(trim(lblNEWZaveDuration.Caption)) <= 5 then
      lblNEWZaveDuration.Caption := '     ' + lblNEWZaveDuration.Caption;
  except lblNEWZaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblSAFaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc123.Value / ppDBCalc91.Value));
    if length(trim(lblSAFaveDuration.Caption)) <= 5 then
      lblSAFaveDuration.Caption := '     ' + lblSAFaveDuration.Caption;
  except lblSAFaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblLACaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc124.Value / ppDBCalc92.Value));
    if length(trim(lblLACaveDuration.Caption)) <= 5 then
      lblLACaveDuration.Caption := '     ' + lblLACaveDuration.Caption;
  except lblLACaveDuration.Caption := '     0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblHOTaveDuration.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc125.Value / ppDBCalc93.Value));
    if length(trim(lblHOTaveDuration.Caption)) <= 5 then
      lblHOTaveDuration.Caption := '     ' + lblHOTaveDuration.Caption;
  except lblHOTaveDuration.Caption := '     0.00';
  end;

  // ave profit labels:

  try // sum of ave profit / sum of vcount
    lblAvePftUSA.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc126.Value / ppDBCalc83.Value));
    if length(trim(lblAvePftUSA.Caption)) <= 5 then
      lblAvePftUSA.Caption := '     ' + lblAvePftUSA.Caption;
  except lblAvePftUSA.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftKEM.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc127.Value / ppDBCalc84.Value));
    if length(trim(lblAvePftKEM.Caption)) <= 5 then
      lblAvePftKEM.Caption := '     ' + lblAvePftKEM.Caption;
  except lblAvePftKEM.Caption := '     0.00';
  end;


  try // sum of ave profit / sum of vcount
    lblAvePftCANA.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc128.Value / ppDBCalc85.Value));
    if length(trim(lblAvePftCANA.Caption)) <= 5 then
      lblAvePftCANA.Caption := '     ' + lblAvePftCANA.Caption;
  except lblAvePftCANA.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftHOHO.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc129.Value / ppDBCalc86.Value));
    if length(trim(lblAvePftHOHO.Caption)) <= 5 then
      lblAvePftHOHO.Caption := '     ' + lblAvePftHOHO.Caption;
  except lblAvePftHOHO.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftUK.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc130.Value / ppDBCalc87.Value));
    if length(trim(lblAvePftUK.Caption)) <= 5 then
      lblAvePftUK.Caption := '     ' + lblAvePftUK.Caption;
  except lblAvePftUK.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftEURO.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc131.Value / ppDBCalc88.Value));
    if length(trim(lblAvePftEURO.Caption)) <= 5 then
      lblAvePftEURO.Caption := '     ' + lblAvePftEURO.Caption;
  except lblAvePftEURO.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftAUS.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc132.Value / ppDBCalc89.Value));
    if length(trim(lblAvePftAUS.Caption)) <= 5 then
      lblAvePftAUS.Caption := '     ' + lblAvePftAUS.Caption;
  except lblAvePftAUS.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftNEWZ.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc133.Value / ppDBCalc90.Value));
    if length(trim(lblAvePftNEWZ.Caption)) <= 5 then
      lblAvePftNEWZ.Caption := '     ' + lblAvePftNEWZ.Caption;
  except lblAvePftNEWZ.Caption := '     0.00';
  end;


  try // sum of ave profit / sum of vcount
    lblAvePftSAF.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc134.Value / ppDBCalc91.Value));
    if length(trim(lblAvePftSAF.Caption)) <= 5 then
      lblAvePftSAF.Caption := '     ' + lblAvePftSAF.Caption;
  except lblAvePftSAF.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftLAC.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc135.Value / ppDBCalc92.Value));
    if length(trim(lblAvePftLAC.Caption)) <= 5 then
      lblAvePftLAC.Caption := '     ' + lblAvePftLAC.Caption;
  except lblAvePftLAC.Caption := '     0.00';
  end;

  try // sum of ave profit / sum of vcount
    lblAvePftHOT.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc136.Value / ppDBCalc93.Value));
    if length(trim(lblAvePftHOT.Caption)) <= 5 then
      lblAvePftHOT.Caption := '     ' + lblAvePftHOT.Caption;
  except lblAvePftHOT.Caption := '     0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand14BeforePrint(Sender: TObject);
begin
  try // sum of ave duration / sum of vcount
    aveduryear.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc147.Value / ppDBCalc146.Value)); // ave duration year
    if trim(aveduryear.Caption) = '0.00' then aveduryear.Caption := '   0.00';
  except aveduryear.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblavepftyear.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc148.Value / ppDBCalc146.Value)); // ave profit year
    if trim(lblavepftyear.Caption) = '0.00' then lblavepftyear.Caption := '   0.00';
  except lblavepftyear.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblaveopDueYEAR.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc149.Value / ppDBCalc146.Value)); // ave profit for web
    if trim(lblaveopDueYEAR.Caption) = '0.00' then lblaveopDueYEAR.Caption := '   0.00';
  except lblaveopDueYEAR.Caption := '   0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand17BeforePrint(Sender: TObject);
  procedure setToZero(); // new 7/12/2011
  begin
    lblYearSysCtryOPSourceRetDly.Caption := '0.00';
    lblYearSysCtryOPSourceDiscDly.Caption := '0.00';
    lblYearSysCtryOPSourceAdjretDly.Caption := '0.00';
    lblYearSysCtryOPSourceCommDly.Caption := '0.00';
    lblYearSysCtryOPSourceProfitDly.Caption := '0.00';
    lblYearSysCtryOPSourceWhlDly.Caption := '0.00';
  end;
begin

  try
    lblYearSysCtryOPSourceRetDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND {'###,###,##0.00'}, ppDBCalc202.Value / ppDBCalc216.Value); // sum(retail) / sum(duration total)
    lblYearSysCtryOPSourceDiscDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc203.Value / ppDBCalc216.Value); // sum(discount) / sum(duration)
    lblYearSysCtryOPSourceAdjretDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc212.Value / ppDBCalc216.Value); // adj ret / sum(duration total)
    lblYearSysCtryOPSourceCommDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc214.Value / ppDBCalc216.Value); // sum(comm) / sum(duration total)
    lblYearSysCtryOPSourceProfitDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc217.Value / ppDBCalc216.Value); // sum(profit) / sum(duration total)
    lblYearSysCtryOPSourceWhlDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc213.Value / ppDBCalc216.Value); // sum(whl) /  sum(duration total)
  except on e: exception do
      setToZero();
  end;
   //r
  try
    lblYearSysCtryOPSourceAveDuration.caption :=
      FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc216.Value / ppDBCalc201.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblYearSysCtryOPSourceAveDuration.caption := '0.00';
  end;
    //'
  try
    lblAveWhlCurrOp1.caption :=
      FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc187.Value / ppDBCalc201.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblAveWhlCurrOp1.caption := '0.00';
  end;



  try
    ppLabelCtryOpSourceAveExchange.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc213.Value / ppDBCalc186.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      ppLabelCtryOpSourceAveExchange.caption := '0.00';
  end;




  try // (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) ) = net_margin
    CtryOpSourceNetMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      (ppDBCalc213.Value / (ppDBCalc202.Value - ppDBCalc203.Value) - 1) * (-1));
  except on e: exception do
      CtryOpSourceNetMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(vs_retail_total) = profit_margin
    CtryOpSourcePftMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc217.Value / ppDBCalc202.Value
      );
  except on e: exception do
      CtryOpSourcePftMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(voucher count) = ave pft
    lblCtryOpSourceAvePft.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc217.Value / ppDBCalc201.Value
      );
  except on e: exception do
      lblCtryOpSourceAvePft.Caption := '0.00';
  end;

  try // sum(wholesale) / sum(voucher count) = ave pft
    lblCtryOpSourceAveWhl.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc213.Value / ppDBCalc201.Value
      );
  except on e: exception do
      lblCtryOpSourceAveWhl.Caption := '0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand21BeforePrint(Sender: TObject);
       // brettsipp2 year system county op sipp
  procedure setToZero();
  begin
    lblYearSysCtryOPSIPPSrcRetDly3.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcDiscDly3.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcAdjretDly3.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcCommDly3.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcProfitDly3.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcWhlDly3.Caption := '0.00';
  end;
begin

  try // must change db calcs stilll
    lblYearSysCtryOPSIPPSrcRetDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc254.Value / ppDBCalc260.Value); // sum(retail) / sum(duration total)
    lblYearSysCtryOPSIPPSrcDiscDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc255.Value / ppDBCalc260.Value); // sum(discount) / sum(duration)
    lblYearSysCtryOPSIPPSrcAdjretDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc256.Value / ppDBCalc260.Value); // adj ret / sum(duration total)
    lblYearSysCtryOPSIPPSrcCommDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc258.Value / ppDBCalc260.Value); // sum(comm) / sum(duration total)
    lblYearSysCtryOPSIPPSrcProfitDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc261.Value / ppDBCalc260.Value); // sum(profit) / sum(duration total)
    lblYearSysCtryOPSIPPSrcWhlDly3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc257.Value / ppDBCalc260.Value); // sum(whl) /  sum(duration total)
  except on e: exception do
      setToZero();
  end;

  try
    lblYearSysCtryOPSIPPSrcAveDuration3.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc260.Value / ppDBCalc253.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblYearSysCtryOPSIPPSrcAveDuration3.caption := '0.00';
  end;

  try // ( sum(vs_wholesale_total) / sum(vs_retail_total) -1 ) * (-1) = gross_margin
    ctryOpSippSrcGrossMargAve3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ((ppDBCalc257.Value / ppDBCalc254.Value) - 1) * (-1));
  except on e: exception do
      ctryOpSippSrcGrossMargAve3.Caption := '0.0';
  end;

  try // (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) ) = net_margin
    CtryOpSippSrcNetMargAve3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      (ppDBCalc257.Value / (ppDBCalc254.Value - ppDBCalc255.Value) - 1) * (-1));
  except on e: exception do
      CtryOpSippSrcNetMargAve3.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(vs_retail_total) = profit_margin
    CtryOpSippSrcPftMargAve3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc261.Value / ppDBCalc254.Value
      );
  except on e: exception do
      CtryOpSippSrcPftMargAve3.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(voucher count) = ave pft
    lblCtryOpSippSrcAvePft3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc261.Value / ppDBCalc253.Value
      );
  except on e: exception do
      lblCtryOpSippSrcAvePft3.Caption := '0.00';
  end;

  try // sum(wholesale) / sum(voucher count) = ave pft
    lblCtryOpSippSrcAveWhl3.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc257.Value / ppDBCalc253.Value
      );
  except on e: exception do
      lblCtryOpSippSrcAveWhl3.Caption := '0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand22BeforePrint(Sender: TObject);
// brettSipp2 year system country op sipp source
  procedure setToZero();
  begin
    lblYearSysCtryOPSIPPSrcRetDly2.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcDiscDly2.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcAdjretDly2.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcCommDly2.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcProfitDly2.Caption := '0.00';
    lblYearSysCtryOPSIPPSrcWhlDly2.Caption := '0.00';
  end;
begin //  193 > 262

  try // must change db calcs stilll
    lblYearSysCtryOPSIPPSrcRetDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc262.Value / ppDBCalc269.Value); // sum(retail) / sum(duration total)
    lblYearSysCtryOPSIPPSrcDiscDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc264.Value / ppDBCalc269.Value); // sum(discount) / sum(duration)
    lblYearSysCtryOPSIPPSrcAdjretDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc265.Value / ppDBCalc269.Value); // adj ret / sum(duration total)
    lblYearSysCtryOPSIPPSrcCommDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc267.Value / ppDBCalc269.Value); // sum(comm) / sum(duration total)
    lblYearSysCtryOPSIPPSrcProfitDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc270.Value / ppDBCalc269.Value); // sum(profit) / sum(duration total)
    lblYearSysCtryOPSIPPSrcWhlDly2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc266.Value / ppDBCalc269.Value); // sum(whl) /  sum(duration total)
  except on e: exception do
      setToZero();
  end;

  try
    lblYearSysCtryOPSIPPSrcAveDuration2.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc269.Value / ppDBCalc262.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblYearSysCtryOPSIPPSrcAveDuration2.caption := '0.00';
  end;

  try // ( sum(vs_wholesale_total) / sum(vs_retail_total) -1 ) * (-1) = gross_margin
    ctryOpSippSrcGrossMargAve2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ((ppDBCalc266.Value / ppDBCalc263.Value) - 1) * (-1));
  except on e: exception do
      ctryOpSippSrcGrossMargAve2.Caption := '0.0';
  end;

  try // (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) ) = net_margin
    CtryOpSippSrcNetMargAve2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      (ppDBCalc266.Value / (ppDBCalc263.Value - ppDBCalc264.Value) - 1) * (-1));
  except on e: exception do
      CtryOpSippSrcNetMargAve2.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(vs_retail_total) = profit_margin
    CtryOpSippSrcPftMargAve2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc270.Value / ppDBCalc263.Value
      );
  except on e: exception do
      CtryOpSippSrcPftMargAve2.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(voucher count) = ave pft
    lblCtryOpSippSrcAvePft2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc270.Value / ppDBCalc262.Value
      );
  except on e: exception do
      lblCtryOpSippSrcAvePft2.Caption := '0.00';
  end;

  try // sum(wholesale) / sum(voucher count) = ave pft
    lblCtryOpSippSrcAveWhl2.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc266.Value / ppDBCalc262.Value
      );
  except on e: exception do
      lblCtryOpSippSrcAveWhl2.Caption := '0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand5BeforePrint(Sender: TObject);
  procedure setToZero();
  begin
    lblYearSysCtryRetDly.Caption := '0.00';
    lblYearSysCtryDiscDly.Caption := '0.00';
    lblYearSysCtryAdjretDly.Caption := '0.00';
    lblYearSysCtryCommDly.Caption := '0.00';
    lblYearSysCtryProfitDly.Caption := '0.00';
    lblYearSysCtryWhlDly.Caption := '0.00';
  end;
begin
  try
    lblYearSysCtryRetDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc224.Value / ppDBCalc230.Value); // sum(retail) / sum(duration total)
    lblYearSysCtryDiscDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc225.Value / ppDBCalc230.Value); // sum(discount) / sum(duration)
    lblYearSysCtryAdjretDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc226.Value / ppDBCalc230.Value); // sum(duration total)
    lblYearSysCtryCommDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc228.Value / ppDBCalc230.Value); // sum(duration total)
    lblYearSysCtryProfitDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc231.Value / ppDBCalc230.Value); // sum(duration total)
    lblYearSysCtryWhlDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc227.Value / ppDBCalc230.Value); // sum(duration total)
  except on e: exception do
      setToZero();
  end;

  try
    lblYearSysCtryAveDuration.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc230.Value / ppDBCalc183.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblYearSysCtryAveDuration.caption := '0.00';
  end;
                     // ppDBCalc188
  try
    lblAveWhlCurr.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc188.Value / ppDBCalc183.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblAveWhlCurr.caption := '0.00';
  end;

  try
    ppLabelCtryAveExchange.caption :=
      FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc227.Value / ppDBCalc188.Value); // sum(wholesale) / sum(wholesale currency)
  except on e: exception do
      ppLabelCtryAveExchange.caption := '0.00';
  end;

  try // ( sum(vs_wholesale_total) / sum(vs_retail_total) -1 ) * (-1) = gross_margin
    ctryGrossMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ((ppDBCalc227.Value / ppDBCalc224.Value) - 1) * (-1));
  except on e: exception do
      ctryGrossMargAve.Caption := '0.0';
  end;

  try // (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) ) = net_margin
    CtryNetMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      (ppDBCalc227.Value / (ppDBCalc224.Value - ppDBCalc225.Value) - 1) * (-1));
  except on e: exception do
      CtryNetMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(vs_retail_total) = profit_margin
    CtryPftMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc231.Value / ppDBCalc224.Value
      );
  except on e: exception do
      CtryPftMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(voucher count) = ave pft
    lblCtryAvePft.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc231.Value / ppDBCalc183.Value
      );
  except on e: exception do
      lblCtryAvePft.Caption := '0.00';
  end;

  try // sum(wholesale) / sum(voucher count) = ave pft
    lblCtryAveWhl.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc227.Value / ppDBCalc183.Value
      );
  except on e: exception do
      lblCtryAveWhl.Caption := '0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand6BeforePrint(Sender: TObject);
  procedure setToZero();
  begin
    lblYearSysCtryOPRetDly.Caption := '0.00';
    lblYearSysCtryOPDiscDly.Caption := '0.00';
    lblYearSysCtryOPAdjretDly.Caption := '0.00';
    lblYearSysCtryOPCommDly.Caption := '0.00';
    lblYearSysCtryOPProfitDly.Caption := '0.00';
    lblYearSysCtryOPWhlDly.Caption := '0.00';
  end;
begin

  try
    lblYearSysCtryOPRetDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc204.Value / ppDBCalc210.Value); // sum(retail) / sum(duration total)
    lblYearSysCtryOPDiscDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc205.Value / ppDBCalc210.Value); // sum(discount) / sum(duration)
    lblYearSysCtryOPAdjretDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc206.Value / ppDBCalc210.Value); // adj ret / sum(duration total)
    lblYearSysCtryOPCommDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc208.Value / ppDBCalc210.Value); // sum(comm) / sum(duration total)
    lblYearSysCtryOPProfitDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc211.Value / ppDBCalc210.Value); // sum(profit) / sum(duration total)
    lblYearSysCtryOPWhlDly.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc207.Value / ppDBCalc210.Value); // sum(whl) /  sum(duration total)
  except on e: exception do
      setToZero();
  end;

  try
    lblYearSysCtryOPAveDuration.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc210.Value / ppDBCalc182.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblYearSysCtryOPAveDuration.caption := '0.00';
  end;

  try
    ppLabelCtryOpAveExchange.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc207.Value / ppDBCalc187.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      ppLabelCtryOpAveExchange.caption := '0.00';
  end;


  //jim r
  try
    lblAveWhlCurrOp.caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND, ppDBCalc187.Value / ppDBCalc182.Value); // sum(duration total) / sum(bookings)
  except on e: exception do
      lblAveWhlCurrOp.caption := '0.00';
  end;

  try // ( sum(vs_wholesale_total) / sum(vs_retail_total) -1 ) * (-1) = gross_margin
    ctryOpGrossMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ((ppDBCalc207.Value / ppDBCalc204.Value) - 1) * (-1));
  except on e: exception do
      ctryOpGrossMargAve.Caption := '0.0';
  end;

  try // (( sum(vs_wholesale_total) / (sum(vs_retail_total) - sum(vs_discount_Retail)) -1 )) * (-1) ) = net_margin
    CtryOpNetMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      (ppDBCalc207.Value / (ppDBCalc204.Value - ppDBCalc205.Value) - 1) * (-1));
  except on e: exception do
      CtryOpNetMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(vs_retail_total) = profit_margin
    CtryOpPftMargAve.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc211.Value / ppDBCalc204.Value
      );
  except on e: exception do
      CtryOpPftMargAve.Caption := '0.0';
  end;

  try // sum(vs_profit) / sum(voucher count) = ave pft
    lblCtryOpAvePft.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc211.Value / ppDBCalc182.Value
      );
  except on e: exception do
      lblCtryOpAvePft.Caption := '0.00';
  end;

  try // sum(wholesale) / sum(voucher count) = ave pft
    lblCtryOpAveWhl.Caption := FormatFLoat(_NUM_FORMAT_EIGHT_POUND,
      ppDBCalc207.Value / ppDBCalc182.Value
      );
  except on e: exception do
      lblCtryOpAveWhl.Caption := '0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupFooterBand8BeforePrint(Sender: TObject);
begin
  ppLabel151.Caption := getCountryName(trim(Uppercase(ppDBText118.Text)), '*');
end;

procedure TfrmMainMkt.ppGroupFooterBand9BeforePrint(Sender: TObject);
begin
  try // sum of ave duration / sum of vcount
    lblAveDUrationOper.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc67.Value / ppDBCalc66.Value));
    if trim(lblAveDUrationOper.Caption) = '0.00' then lblAveDUrationOper.Caption := '   0.00';
  except lblAveDUrationOper.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblAveProfitOper.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc68.Value / ppDBCalc66.Value));
    if trim(lblAveProfitOper.Caption) = '0.00' then lblAveProfitOper.Caption := '   0.00';
  except lblAveProfitOper.Caption := '   0.00';
  end;

  try // sum of ave duration / sum of vcount
    lblaveopDUeOper.Caption := FormatFloat(_NUM_FORMAT_FIVE_POUND, (ppDBCalc69.Value / ppDBCalc66.Value)); // ave profit for web
    if trim(lblaveopDUeOper.Caption) = '0.00' then lblaveopDUeOper.Caption := '   0.00';
  except lblaveopDUeOper.Caption := '   0.00';
  end;
end;

procedure TfrmMainMkt.ppGroupHeaderBand2AfterPrint(Sender: TObject);
begin
  if (rgOutputTarget.ItemIndex = 1) // excel
    and (rgDetailLevel.ItemIndex <> 2) // summary only
    then
    ppGroupHeaderBand2.Visible := false;
end;

procedure TfrmMainMkt.ppGroupHeaderBand3AfterPrint(Sender: TObject);
begin
  ppGroupHeaderBand3.Visible := false;
end;

procedure TfrmMainMkt.ppGroupHeaderBand4AfterPrint(Sender: TObject);
begin
  ppGroupHeaderBand4.Visible := false;
end;

procedure TfrmMainMkt.ppGroupHeaderBandMarcelloCampaignAfterPrint(
  Sender: TObject);
begin
  ppGroupHeaderBandMarcelloCampaign.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBand10AfterPrint(Sender: TObject);
begin
  if (rgSpecialReportsOutput.ItemIndex = 2) then // excel
    ppHeaderBand10.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBandMarcelloFullAfterPrint(Sender: TObject);
begin
  ppHeaderBandMarcelloFull.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBand6AfterPrint(Sender: TObject);
begin
  ppHeaderBand6.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBand7AfterPrint(Sender: TObject);
begin
  if rgSpecialReportsOutput.ItemIndex = 2 then // xls
    ppHeaderBand7.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBand8AfterPrint(Sender: TObject);
begin
  ppHeaderBand8.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBand9AfterPrint(Sender: TObject);
begin
  if (rgSpecialReportsOutput.ItemIndex = 2) then // excel
    ppHeaderBand9.Visible := false;
end;

procedure TfrmMainMkt.ppHeaderBandMarcelloAfterPrint(Sender: TObject);
begin
  ppHeaderBandMarcello.Visible := false;
end;

procedure TfrmMainMkt.ppReportMarcelloPrintDialogClose(Sender: TObject);
begin
 //just playing follow the leader in behavior of the application
  if ppReportMarcello.PrintDialog.PrintToFile then
    globEMLFileName := ppReportMarcello.PrintDialog.TextFileName;

  if ppReportMarcello.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.Q1AfterScroll(DataSet: TDataSet);
begin
  JScroll(DataSet);
end;

procedure TfrmMainMkt.Q1BeforeOpen(DataSet: TDataSet);
begin
  sb.Panels[2].Text := 'getting data. please wait.';
  application.ProcessMessages;
end;

procedure TfrmMainMkt.repclemailPrintDialogClose(Sender: TObject);
begin
  if repSales.PrintDialog.PrintToFile then
    globEMLFileName := repClemail.PrintDialog.TextFileName;

  if repclemail.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.repcombinedFormattedBeforePrint(Sender: TObject);
begin
  ppHeaderBand6.Visible := true;

  if rgSpecialReportsOutput.ItemIndex = 2 then // xls
  begin
    // do this because if shiftage in the header labels
    ppLabel142.Left := 5.6146;

  end
  else
  begin
    ppLabel142.Left := 5.7292;
  end;
end;

procedure TfrmMainMkt.repcombinedFormattedPrintDialogClose(Sender: TObject);
begin
  if repcombinedformatted.PrintDialog.PrintToFile then
    globEMLFileName := repcombinedformatted.PrintDialog.TextFileName;

  if repcombinedformatted.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.repFleetMixBeforePrint(Sender: TObject);
begin
  ppHeaderBand8.Visible := true;

  if rgSpecialReportsOutput.ItemIndex = 2 then // xls
  begin
    // do this because of shiftage

    // country group:
    lblUSAaveDuration.Left := 2.6666;
    lblAvePftUSA.Left := 2.6666;
    lblkemaveDuration.Left := 3.3125;
    lblAvePftKEM.Left := 3.3125;
    lblcanaaveDuration.Left := 4.0729;
    lblAvePftCANA.Left := 4.0729;
    lblhohoaveDuration.Left := 4.7917;
    lblAvePftHOHO.Left := 4.7917;
    lblUKaveDuration.Left := 5.5521;
    lblAvePftUK.Left := 5.5521;
    lblEUROaveDuration.Left := 6.2083;
    lblAvePftEURO.Left := 6.2083;
    lblAUSaveDuration.Left := 6.8854;
    lblAvePftAUS.Left := 6.8854;
    lblNEWZaveDuration.Left := 7.5938;
    lblAvePftNEWZ.Left := 7.5938;
    lblSAFaveDuration.Left := 8.2708;
    lblAvePftSAF.Left := 8.2708;
    lblLACaveDuration.Left := 8.9896;
    lblAvePftLAC.Left := 8.9896;
    lblHOTaveDuration.Left := 9.6562;
    lblAvePftHOT.Left := 9.6562;

    // year group:
    ppLabel120.Left := 2.6666;
    ppLabel131.Left := 2.6666;
    ppLabel121.Left := 3.3125;
    ppLabel132.Left := 3.3125;
    ppLabel166.Left := 4.0729;
    ppLabel122.Left := 4.0729;
    ppLabel183.Left := 4.7917;
    ppLabel123.Left := 4.7917;
    ppLabel184.Left := 5.5521;
    ppLabel124.Left := 5.5521;
    ppLabel191.Left := 6.2083;
    ppLabel125.Left := 6.2083;
    ppLabel192.Left := 6.8854;
    ppLabel126.Left := 6.8854;
    ppLabel193.Left := 7.5938;
    ppLabel127.Left := 7.5938;
    ppLabel194.Left := 8.2708;
    ppLabel128.Left := 8.2708;
    ppLabel195.Left := 8.9896;
    ppLabel129.Left := 8.9896;
    ppLabel196.Left := 9.6562;
    ppLabel130.Left := 9.6562;
  end
  else
  begin // line stuff up for print to screen
    // country group:
    lblUSAaveDuration.Left := ppDBCalc83.Left;
    lblAvePftUSA.Left := ppDBCalc83.Left;
    lblkemaveDuration.Left := ppDBCalc84.Left;
    lblAvePftKEM.Left := ppDBCalc84.Left;
    lblcanaaveDuration.Left := ppDBCalc85.Left;
    lblAvePftCANA.Left := ppDBCalc85.Left;
    lblhohoaveDuration.Left := ppDBCalc86.Left;
    lblAvePftHOHO.Left := ppDBCalc86.Left;
    lblUKaveDuration.Left := ppDBCalc87.Left;
    lblAvePftUK.Left := ppDBCalc87.Left;
    lblEUROaveDuration.Left := ppDBCalc88.Left;
    lblAvePftEURO.Left := ppDBCalc88.Left;
    lblAUSaveDuration.Left := ppDBCalc89.Left;
    lblAvePftAUS.Left := ppDBCalc89.Left;
    lblNEWZaveDuration.Left := ppDBCalc90.Left;
    lblAvePftNEWZ.Left := ppDBCalc90.Left;
    lblSAFaveDuration.Left := ppDBCalc91.Left;
    lblAvePftSAF.Left := ppDBCalc91.Left;
    lblLACaveDuration.Left := ppDBCalc92.Left;
    lblAvePftLAC.Left := ppDBCalc92.Left;
    lblHOTaveDuration.Left := ppDBCalc93.Left;
    lblAvePftHOT.Left := ppDBCalc93.Left;

        // year group:
    ppLabel120.Left := ppDBCalc83.Left;
    ppLabel131.Left := ppDBCalc83.Left;
    ppLabel121.Left := ppDBCalc84.Left;
    ppLabel132.Left := ppDBCalc84.Left;
    ppLabel166.Left := ppDBCalc85.Left;
    ppLabel122.Left := ppDBCalc85.Left;
    ppLabel183.Left := ppDBCalc86.Left;
    ppLabel123.Left := ppDBCalc86.Left;
    ppLabel184.Left := ppDBCalc87.Left;
    ppLabel124.Left := ppDBCalc87.Left;
    ppLabel191.Left := ppDBCalc88.Left;
    ppLabel125.Left := ppDBCalc88.Left;
    ppLabel192.Left := ppDBCalc89.Left;
    ppLabel126.Left := ppDBCalc89.Left;
    ppLabel193.Left := ppDBCalc90.Left;
    ppLabel127.Left := ppDBCalc90.Left;
    ppLabel194.Left := ppDBCalc91.Left;
    ppLabel128.Left := ppDBCalc91.Left;
    ppLabel195.Left := ppDBCalc92.Left;
    ppLabel129.Left := ppDBCalc92.Left;
    ppLabel196.Left := ppDBCalc93.Left;
    ppLabel130.Left := ppDBCalc93.Left;
  end;
end;

procedure TfrmMainMkt.repFleetMixPrintDialogClose(Sender: TObject);
begin
  if repFleetMix.PrintDialog.PrintToFile then
    globEMLFileName := repFleetMix.PrintDialog.TextFileName;

  if repFleetMix.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.reportTrackBeforePrint(Sender: TObject);
begin
  lblDate.Caption := FormatDateTime('MM/DD/YYYY', dtpTrackReportsStart.dateTime) + ' to ' +
    FormatDateTime('MM/DD/YYYY', dtpTrackReportsEnd.dateTime);
end;

procedure TfrmMainMkt.repRepeatBizBeforePrint(Sender: TObject);
begin
  if rgSpecialReportsOutput.ItemIndex = 2 then // xls
  begin
    // do this because of xls col shift
    ppLabel199.Left := 1.5;
    ppLabel201.Left := 3.4479;
    ppLabel203.Left := 5.4271;
  end
  else
  begin
    ppLabel199.Left := 1.7187;
    ppLabel201.Left := 3.6667;
    ppLabel203.Left := 5.6562
  end;
end;

procedure TfrmMainMkt.repRepeatBizPrintDialogClose(Sender: TObject);
begin
  if repRepeatBiz.PrintDialog.PrintToFile then
    globEMLFileName := repRepeatBiz.PrintDialog.TextFileName;

  if repRepeatBiz.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.repSalesPrintDialogClose(Sender: TObject);
begin
  if repSales.PrintDialog.PrintToFile then
    globEMLFileName := repSales.PrintDialog.TextFileName;

  if repSales.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.repTAPrintDialogClose(Sender: TObject);
begin
  if repTA.PrintDialog.PrintToFile then
    globEMLFileName := repTA.PrintDialog.TextFileName;

  if repTA.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.repYTDBeforePrint(Sender: TObject);
begin
  ppHeaderBand7.Visible := true;

  if rgSpecialReportsOutput.ItemIndex = 2 then // xls
  begin
    lblaveprofitCTRY.Left := 6.7083;
    dbcalcopdue.Left := 7.375;
    lblaveopDUeCTRY.Left := 8.0417;
    ppDBCalc65.Left := 8.7083; // gtv for country group

    lblaveprofitOper.Left := 6.7083;
    ppDBCalc69.Left := 7.375;
    lblaveopDUeOper.Left := 8.0417;
    ppDBCalc70.Left := 8.7083; // gtv for country group
  end
  else // adjust labels so they line up on screen print
  begin
    lblaveprofitCTRY.width := ppDBText126.width;
    dbcalcopdue.width := ppDBText131.Width;
    lblaveopDUeCTRY.width := ppDBText128.Width;
    ppDBCalc65.width := ppDBText127.width; // gtv for country group


    lblaveprofitCTRY.left := ppDBText126.left;
    dbcalcopdue.left := ppDBText131.left;
    lblaveopDUeCTRY.left := ppDBText128.left;
    ppDBCalc65.left := ppDBText127.left; // gtv for country group

    lblaveprofitOper.width := ppDBText126.width;
    ppDBCalc69.width := ppDBText131.Width;
    lblaveopDUeOper.width := ppDBText128.Width;
    ppDBCalc70.width := ppDBText127.width; // gtv for country group


    lblaveprofitOper.Left := ppDBText126.left;
    ppDBCalc69.Left := ppDBText131.left;
    lblaveopDUeOper.Left := ppDBText128.left;
    ppDBCalc70.Left := ppDBText127.left; // gtv for country group
  end;
end;

procedure TfrmMainMkt.repYTDPrintDialogClose(Sender: TObject);
begin
  if repYTD.PrintDialog.PrintToFile then
    globEMLFileName := repYTD.PrintDialog.TextFileName;

  if repYTD.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.rgDirectTaClick(Sender: TObject);
begin
  if (rgRestrictByEmail.ItemIndex = 0) and (rgDirectTa.ItemIndex = 1) then // direct client  restrict for ta dataset
  begin // check DC/TA
    showmessage('why restrict to TA bookings when also resticting to valid client e-mails?' + #10#13 +
      '"restrict by e-mail" will be reset.');
    rgRestrictByEmail.ItemIndex := 1;
  end
  else if (rgRestrictByEmail.ItemIndex = 1) and (rgDirectTa.ItemIndex = 0) then // ta restrict for DC dataset
  begin
    showmessage('why restrict to DC bookings when also resticting to valid TA e-mails?' + #10#13 +
      '"restrict by e-mail" will be reset.');

    rgRestrictByEmail.ItemIndex := 0;
  end;
end;

procedure TfrmMainMkt.rgDSTypeClick(Sender: TObject);
begin
  if (rgdstype.ItemIndex = 1) and
    (
    (rgRestrictByEmail.ItemIndex <> 0) or
    (rgDirectTa.ItemIndex <> 0)) then
  begin
    showmessage('It is recomended to restrict to only valid client e-mails when using "client email" output.');
  end
  else if (rgdstype.ItemIndex = 2) and // ta-email report selected
    (
    (rgRestrictByEmail.ItemIndex <> 1) or
    (rgDirectTa.ItemIndex <> 1)) then
  begin
    showmessage('It is recomended to restrict to only valid travel agent e-mails when using "ta email" output.');
  end;
end;

procedure TfrmMainMkt.rgRestrictByEmailClick(Sender: TObject);
begin
  if (rgRestrictByEmail.ItemIndex = 0) and (rgDirectTa.ItemIndex = 1) then // direct client  restrict for ta dataset
  begin // check DC/TA
    showmessage('why restrict to valid client e-mails when only showing TA bookings?' + #10#13 +
      '"Direct Client or Travel Agent" will be reset.');
    rgdirectta.ItemIndex := 0;
  end
  else if (rgRestrictByEmail.ItemIndex = 1) and (rgDirectTa.ItemIndex = 0) then // ta restrict for DC dataset
  begin
    showmessage('why restrict to valid TA e-mails when only showing DC bookings?' + #10#13 +
      '"Direct Client or Travel Agent" will be reset.');
    rgdirectta.ItemIndex := 1;
  end;


  if (rgRestrictByEmail.ItemIndex = 1) and (cbUseClientCtry.Checked) then
  begin
    rgRestrictByEmail.ItemIndex := 2;
  // can not use this and restrict by TA e-mail at the same time becuase of outer clause variations in BuildSQL()
    showmessage('sorry, one can not restrict to "TA email" and "client country" (tab 2c)  at the same time.');


  end;





end;

procedure TfrmMainMkt.rgStep1PayTypeClick(Sender: TObject);
begin
  if (rgStep1PayType.ItemIndex = 2) then // pick up date - disable pu filter #2
  begin
    cbUsePUDate.Checked := false;
    gbPUD.Enabled := false;
  end
  else
  begin
    gbPUD.Enabled := true;
  end;

 // set the offset box on the special reports tab
  if (rgStep1PayType.ItemIndex = 0) then // paid
    editDateOffset.Text := '+1'
  else if (rgStep1PayType.ItemIndex = 1) then //create date
    editDateOffset.Text := '+0'
  else if (rgStep1PayType.ItemIndex = 2) then // pick date
    editDateOffset.Text := '+0'
  else
    editDateOffset.Text := '+1';
end;

procedure TfrmMainMkt.rpbus_get_namesClick(Sender: TObject);
begin
 {
  don't  delete the button named "rpbus_get_names" !
  it is used as 'sender' for building sql in repeat biz .. stupid yes, placeholder yes - just leave it, dumb ass
  -chester
 }
end;

procedure TfrmMainMkt.rptBrettCtryOpPrintDialogClose(Sender: TObject);
begin
  if rptBrettCtryOp.PrintDialog.PrintToFile then
    globEMLFileName := rptBrettCtryOp.PrintDialog.TextFileName;

  if rptBrettCtryOp.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.rptBrettSipp2PrintDialogClose(Sender: TObject);
begin
  if rptBrettSipp2.PrintDialog.PrintToFile then
    globEMLFileName := rptBrettSipp2.PrintDialog.TextFileName;

  if rptBrettSipp2.PrintDialog.ModalResult = mrCancel then
  begin
    globEMLFileName := '';
    showmessage('Print job canceled.');
  end;
end;

procedure TfrmMainMkt.PrintRoutine(InFileName, ReportTitle: string);
var sFILENAME: string;
begin
  case rgDetailLevel.ItemIndex of
    0: //full detail
      begin
        repsales.Header.Visible := true;
        repclemail.Header.Visible := true;
        repta.Header.Visible := true;
        ppGroupHeaderBand2.Visible := true; //sales rep
        ppGroupHeaderBand3.Visible := true; // email rep
        ppGroupHeaderBand4.Visible := true; // vendor
        repsales.Detail.visible := true;
        repclemail.Detail.visible := true;
        repTA.Detail.visible := true;
        ppGroupFooterBand2.Visible := true; //sales rep
        ppGroupFooterBand3.Visible := true; // email rep
        ppGroupFooterBand4.Visible := true; // ta rep
      end;
    1: //detail only
      begin
        repsales.Header.Visible := true;
        repclemail.Header.Visible := true;
        repta.Header.Visible := true;
        ppGroupHeaderBand2.Visible := false; // sales rep
        ppGroupHeaderBand3.Visible := false; // email rep
        ppGroupHeaderBand4.Visible := false;
        repsales.Detail.visible := true;
        repclemail.Detail.visible := true;
        repta.Detail.visible := true;
        ppGroupFooterBand2.Visible := false; //sales rep
        ppGroupFooterBand3.Visible := false; // email rep
        ppGroupFooterBand4.Visible := false;
      end;
    2: //group summary only
      begin
        repsales.Header.Visible := false;
        repclemail.Header.Visible := false;
        repta.Header.Visible := false;
        ppGroupHeaderBand2.Visible := true; // sales rep
        ppGroupHeaderBand3.Visible := true; // email rep
        ppGroupHeaderBand4.Visible := true;
        repsales.Detail.visible := false;
        repclemail.Detail.visible := false;
        repta.Detail.visible := false;
        ppGroupFooterBand2.Visible := true; //sales rep
        ppGroupFooterBand3.Visible := true; // email rep
        ppGroupFooterBand4.Visible := true;
      end;
  else ;
  end;

  try
    Q1.DisableControls;
    mem.DisableControls;
    sFILENAME := SetDirectoryForTSUser();
    SAdd(sFILENAME, 'xls_' + InFileName + '.csv');
    sb.Panels[2].Text := 'Please wait..Generating';
    application.ProcessMessages;

    try
      if fileexists(sFILENAME) then deletefile(sFilename);
    except;
    end;

    globEMLFileName := sFILENAME;
    repsales.ShowPrintDialog := true;
    repclemail.ShowPrintDialog := true;
    repta.ShowPrintDialog := true;
    // set titles:
    ppLabel55.caption := ReportTitle;
    lblTitle.caption := ReportTitle;
    pplabel80.Caption := ReportTitle;

    if rgOutputTarget.Itemindex = 1 then
    begin
      repsales.DeviceType := 'XLSXReport';
      repsales.TextFileName := sFILENAME;
      repsales.AllowPrintToFile := True;
      repclemail.DeviceType := 'XLSXReport';
      repclemail.TextFileName := sFILENAME;
      repclemail.AllowPrintToFile := True;
      repta.DeviceType := 'XLSXReport';
      repta.TextFileName := sFILENAME;
      repta.AllowPrintToFile := True;
    end
    else
    begin
      repsales.DeviceType := 'Screen';
      repsales.AllowPrintToFile := false;
      repclemail.DeviceType := 'Screen';
      repclemail.AllowPrintToFile := False;
      repta.DeviceType := 'Screen';
      repta.AllowPrintToFile := False;
    end;

    if (rgDSType.itemindex = 0) then repSales.Print
    else if (rgDSType.ItemIndex = 1) then repclemail.Print
    else if (rgDSType.ItemIndex = 2) then repta.Print;

    sb.Panels[2].Text := 'Done generating.';
    application.ProcessMessages;

    if cbEmail.Checked then EMail(globEMLFileName)
    else
    begin
      if rgOutputTarget.Itemindex = 1 then
        OpenFile(globEMLFileName);
    end;
    btnEMailSend.Enabled := true;
  finally
    Q1.EnableControls;
    mem.EnableControls;
  end;
end;

procedure TfrmMainMkt.PrintSpecialReportsRoutine(theReport: TppReport; InFileName, ReportTitle: string);
var sFILENAME: string;

  function StripQuotes(InStr: string): string;
  var i, x: integer;
    temp: string;
  begin
    if (InStr = '') then
    begin
      Result := '';
      exit;
    end
    else
    begin
      temp := '';
      x := length(InStr);

      for i := 1 to x do
      begin
        case InStr[i] of
          '"': continue;

        else
          temp := temp + InStr[i];
        end;
      end;
    end;
    Result := temp;
  end;
begin
  // set for full Detail

  //repcombinedFormatted
  ppGroupFooterband7.Visible := true; // year totals
  ppGroupFooterband8.Visible := true; // country totals
  ppdetailband6.Visible := true;
  ppGroupFooterBand13.Visible := true;

  // repYTD:
  ppDetailBand7.Visible := true;
  ppGroupFooterband11.Visible := true; // country totals
  ppGroupFooterband9.Visible := true;
  ppGroupFooterBand14.Visible := true; // year


  // repFleetMix
  ppDetailBand8.Visible := true;
  ppGroupFooterband12.Visible := true; // country totals
  ppGroupFooterband10.Visible := true;

  case rgSpecialReportsDetail.ItemIndex of
    1: //detail only
      begin
        ppGroupFooterband7.Visible := false;
        ppGroupFooterband8.Visible := false;
        ppGroupFooterBand13.Visible := false;
        ppGroupFooterband11.Visible := false; // country totals
        ppGroupFooterband9.Visible := false; // country totals
        ppGroupFooterBand14.Visible := false;
        ppGroupFooterband12.Visible := false; // fleet mix
        ppGroupFooterband10.Visible := false; // fleet mix
      end;
    2: //group summary only
      begin
        ppdetailband6.Visible := false;
        ppDetailBand7.Visible := false;
        ppDetailBand8.Visible := false; // fleet mix
      end;
  else ;
  end;

  sFILENAME := SetDirectoryForTSUser();
  sb.Panels[2].Text := 'Please wait..Generating';
  application.ProcessMessages;
  globEMLFileName := sFILENAME;

  repCombinedFormatted.ShowPrintDialog := true;
  repYTD.ShowPrintDialog := true;
  repFleetMix.ShowPrintDialog := true;
  repRepeatBiz.ShowPrintDialog := true;

    // set titles:
  ppLabel133.caption := ReportTitle;
  lblYTDTITLE.caption := ReportTitle;
  lblFleetMixTitle.caption := ReportTitle;
  lblRepeatBizTitle.caption := ReportTitle;

  if rgSpecialReportsOutput.ItemIndex = 1 then // csv
  begin
    SAdd(sFILENAME, InFileName + '.csv');
    if (theReport = repCombinedFormatted) or
      (theReport = repYTD) or
      (theReport = repFleetMix) or (theReport = reprepeatbiz) then
      DoOutPut(btnCombined2, 'CSV', GlobFName);

    exit;
  end
  else if rgSpecialReportsOutput.Itemindex = 2 then // xls
  begin
    SAdd(sFILENAME, InFileName + 'XLSX');
    repCombinedFormatted.DeviceType := 'XLSXReport';
    repCombinedFormatted.TextFileName := sFILENAME;
    repCombinedFormatted.AllowPrintToFile := True;
    repYTD.DeviceType := 'XLSXReport';
    repYTD.TextFileName := sFILENAME;
    repYTD.AllowPrintToFile := True;
    repFleetMix.DeviceType := 'XLSXReport';
    repFleetMix.TextFileName := sFILENAME;
    repFleetMix.AllowPrintToFile := True;
    reprepeatbiz.DeviceType := 'XLSXReport';
    reprepeatbiz.TextFileName := sFILENAME;
    reprepeatbiz.AllowPrintToFile := True;
  end
  else
  begin
    repCombinedFormatted.DeviceType := 'Screen';
    repCombinedFormatted.AllowPrintToFile := false;
    repYTD.DeviceType := 'Screen';
    repYTD.AllowPrintToFile := false;
    repFleetMix.DeviceType := 'Screen';
    repFleetMix.AllowPrintToFile := false;
    reprepeatbiz.DeviceType := 'Screen';
    reprepeatbiz.AllowPrintToFile := false;
  end;

  if theReport = repCombinedFormatted then
    repCombinedFormatted.Print
  else if theReport = repYTD then
    repYTD.Print
  else if theReport = repFleetMix then
    repFleetMix.Print
  else if theReport = reprepeatbiz then
    reprepeatbiz.Print;

  sb.Panels[2].Text := 'Done generating.';
  application.ProcessMessages;

  sFILENAME := globEMLFileName;

  sFILENAME := stripQuotes(sFILENAME);

  if rgSpecialReportsOutput.Itemindex in [1..2] then // csv / xls /pdf
    OpenFile(sFILENAME);

  btnEMailSend.Enabled := true;
end;

procedure TfrmMainMkt.sheetCarTypeShow(Sender: TObject);
begin
 // if Form1.Height < 432 then Form1.Height := 432;
 // if form1.Width < 449 then form1.Width := 449;
  if frmMainMkt.Height < _TAB_CAR_TYPE_HEIGHT then
    frmMainMkt.Height := _TAB_CAR_TYPE_HEIGHT;

  if frmMainMkt.Width < _TAB_CAR_TYPE_WIDTH then
    frmMainMkt.Width := _TAB_CAR_TYPE_WIDTH;
  application.ProcessMessages;
end;

procedure TfrmMainMkt.sheetPayTypeShow(Sender: TObject);
begin
 // if Form1.Height < 344 then Form1.Height := 344;
  if frmMainMkt.Height < _TAB_PAY_TYPE_HEIGHT then
    frmMainMkt.Height := _TAB_PAY_TYPE_HEIGHT;
  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabDataShow(Sender: TObject);
begin
  if datasource1.DataSet = Q1 then
  begin
    jonlbl.caption := 'Query';
  end
  else if datasource1.DataSet = mem then
  begin
    jonlbl.caption := 'InMem';
  end;

  //if form1.Width < 702 then form1.Width := 702;
  if frmMainMkt.Width < _TAB_DATA_WIDTH then
    frmMainMkt.Width := _TAB_DATA_WIDTH;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabForbiddenZoneShow(Sender: TObject);
begin
 // if Form1.Height < 441 then Form1.Height := 441;
  //if form1.Width < 621 then form1.Width := 621;

  if frmMainMkt.Height < _TAB_TFZ_HEIGHT then
    frmMainMkt.Height := _TAB_TFZ_HEIGHT;

  if frmMainMkt.Width < _TAB_TFZ_WIDTH then
    frmMainMkt.Width := _TAB_TFZ_WIDTH;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabOutputShow(Sender: TObject);
begin
  //if Form1.Height < 468 then Form1.Height := 468;
  //if form1.Width < 621 then form1.Width := 621;

  if frmMainMkt.Height < _TAB_OUTPUT_HEIGHT then
    frmMainMkt.Height := _TAB_OUTPUT_HEIGHT;

  if frmMainMkt.Width < _TAB_OUTPUT_WIDTH then
    frmMainMkt.Width := _TAB_OUTPUT_WIDTH;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabspecialreportsShow(Sender: TObject);
begin
  //if Form1.Height < 475 then Form1.Height := 475;
  //if form1.Width < 863 then form1.Width := 863;
  if frmMainMkt.Height < _TAB_SPECIAL_HEIGHT then
    frmMainMkt.Height := _TAB_SPECIAL_HEIGHT;

  if frmMainMkt.Width < _TAB_SPECIAL_WIDTH then
    frmMainMkt.Width := _TAB_SPECIAL_WIDTH;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabstep2bShow(Sender: TObject);
begin
  //if Form1.Height < 485 then Form1.Height := 485;
  if frmMainMkt.Height < _TAB_STEPTWO_B_HEIGHT then
    frmMainMkt.Height := _TAB_STEPTWO_B_HEIGHT;

  application.ProcessMessages;
end;

procedure TfrmMainMkt.tabStep2Show(Sender: TObject);
begin
  //if Form1.Height < 567 then Form1.Height := 567;
  //if form1.Width < 618 then form1.Width := 618;

  if frmMainMkt.Height < _TAB_STEPTWO_HEIGHT then
    frmMainMkt.Height := _TAB_STEPTWO_HEIGHT;

  if frmMainMkt.Width < _TAB_STEPTWO_WIDTH then
    frmMainMkt.Width := _TAB_STEPTWO_WIDTH;
  application.ProcessMessages;
end;

procedure TfrmMainMkt.cbActiveOnlyClick(Sender: TObject);
begin
  if cbActiveOnly.checked then cbcancelsonly.Checked := false;
end;

procedure TfrmMainMkt.cbcancelsonlyClick(Sender: TObject);
begin
  if cbcancelsonly.Checked then cbActiveOnly.checked := false;
end;

procedure TfrmMainMkt.cbFIlterGroupMoreClick(Sender: TObject);
begin
  if cbFIlterGroupMore.Checked then
  begin
    rgdateType_FilterGroup.Enabled := true;
    rgoldestNewest_FilterGroup.Enabled := true;
    btnresort.Enabled := true;
  end
  else
  begin
    rgdateType_FilterGroup.Enabled := false;
    rgoldestNewest_FilterGroup.Enabled := false;
    btnresort.Enabled := false;
  end;
end;

procedure TfrmMainMkt.cboperatorEnter(Sender: TObject);
begin
  FillNOTOperatorCB(cbOperator);
end;

procedure TfrmMainMkt.cbPlusBasicClick(Sender: TObject);
begin
  comboPlusBasic.Enabled := cbPlusBasic.Checked;
end;

procedure TfrmMainMkt.cbProductTypeSelect(Sender: TObject);
begin
  fillSubTypeBox();
end;

procedure TfrmMainMkt.cbReleaseNumClick(Sender: TObject);
begin
  EditReleaseNUm.Enabled := cbreleaseNum.Checked;
end;

procedure TfrmMainMkt.cbSIPPWildCardClick(Sender: TObject);
begin
  EditSippWildcard.Enabled := cbSippWildcard.Checked;

  if cbSIPPWildCard.Checked then
  begin
    cbUseSipp.Checked := false;
  end;
end;

procedure TfrmMainMkt.cbspaShowDetailClick(Sender: TObject);
begin

  if cbspaShowDetail.checked then
  begin
    if cbspaSHowYearSysCtry.caption = 'Summary by campid' then
    begin
      cbspaSHowYearSysCtry.Checked := false;
    end;
  end;
end;

procedure TfrmMainMkt.cbspaSHowYearSysCtryClick(Sender: TObject);
begin


  if cbspaSHowYearSysCtry.Checked then
  begin
    cbspaSHowYearSysCtryMstrOp.checked := false;
    cbspaSHowYearSysCtryMstrOpSource.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPP.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPPSrc.checked := false;
    if cbspaSHowYearSysCtry.caption = 'Summary by campid' then
    begin
      cbspaShowDetail.Checked := false;
    end;
  end;
end;

procedure TfrmMainMkt.cbspaSHowYearSysCtryMstrOpClick(Sender: TObject);
begin
  if cbspaSHowYearSysCtryMstrOp.Checked then
  begin
    cbspaSHowYearSysCtry.checked := false;
    cbspaSHowYearSysCtryMstrOpSource.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPP.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPPSrc.checked := false;
  end;
end;

procedure TfrmMainMkt.cbspaSHowYearSysCtryMstrOpSIPPClick(Sender: TObject);
begin
  if cbspaSHowYearSysCtryMstrOpSIPP.checked then
  begin
    cbspaSHowYearSysCtryMstrOpSource.checked := false;
    cbspaSHowYearSysCtry.checked := false;
    cbspaSHowYearSysCtryMstrOp.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPPSrc.checked := false;
  end;
end;

procedure TfrmMainMkt.cbspaSHowYearSysCtryMstrOpSIPPSrcClick(Sender: TObject);
begin
  if cbspaSHowYearSysCtryMstrOpSIPPSrc.checked then
  begin
    cbspaSHowYearSysCtryMstrOpSource.checked := false;
    cbspaSHowYearSysCtry.checked := false;
    cbspaSHowYearSysCtryMstrOp.checked := false;
    cbspaSHowYearSysCtryMstrOpSource.checked := false;
    cbspaSHowYearSysCtryMstrOpSIPP.checked := false;
  end;
end;

procedure TfrmMainMkt.cbspaSHowYearSysCtryMstrOpSourceClick(Sender: TObject);
begin
  if cbspaSHowYearSysCtryMstrOpSource.checked then
  begin
    cbspaSHowYearSysCtryMstrOpSIPP.Checked := false;
    cbspaSHowYearSysCtryMstrOpSIPPSrc.Checked := false;
    cbspaSHowYearSysCtry.Checked := false;
    cbspaSHowYearSysCtryMstrOp.Checked := false;
  end;
end;

procedure TfrmMainMkt.cbUseAgeClick(Sender: TObject);
begin
  EditAgeEnd.Enabled := cbUseAge.Checked;
  EditAgeStart.Enabled := cbUseAge.Checked;
end;

procedure TfrmMainMkt.cbUseClientCtryClick(Sender: TObject);
begin
  EditClientCountry.Enabled := cbUseClientCtry.Checked;
  if not (cbUseClientCtry.Checked) then EditClientCountry.clear;

  // can not use this and restrict by TA e-mail at the same time becuase of outer clause variations in BuildSQL()
  if (cbUseClientCtry.Checked) and (rgRestrictByEmail.ItemIndex = 1) then
  begin
    cbUseClientCtry.Checked := false;
    EditClientCountry.Enabled := false;
    EditClientCountry.clear;
    showmessage('sorry, one can not restrict to "TA email" (tab 2b) and "client country" at the same time.');
    exit;
  end;

  if (cbUseClientCtry.Checked) then
  begin
    showmessage('WARNING: joining client data can result in NOT matching sales journal.');
  end;
end;

procedure TfrmMainMkt.cbUseCreateDateClick(Sender: TObject);
begin
  dtpCreateDtStart.Enabled := cbUseCreateDate.Checked;
  dtpCreateDtEnd.Enabled := cbUseCreateDate.Checked;

  if (rgStep1PayType.ItemIndex = 1) and (cbUseCreateDate.Checked) then // create date is selected already in step1
  begin
    showmessage('"Create date" is already selected in step 1. ');
    cbUseCreateDate.Checked := false;
  end;
end;

procedure TfrmMainMkt.cbUseDropCityClick(Sender: TObject);
begin
  editDropCity.Enabled := cbUseDropCity.Checked;
end;

procedure TfrmMainMkt.cbUseDropCtryClick(Sender: TObject);
begin
  editDropCtry.Enabled := cbUseDropCtry.Checked;
end;

procedure TfrmMainMkt.cbUseDropDateClick(Sender: TObject);
begin
  dtpDropDtStart.Enabled := cbUseDropDate.Checked;
  dtpDropDtEnd.Enabled := cbUseDropDate.Checked;
end;


procedure TfrmMainMkt.cbUseDurationClick(Sender: TObject);
begin
  EditDurLow.Enabled := cbUseDuration.Checked;
  EditDurHigh.Enabled := cbUseDuration.Checked;
end;

procedure TfrmMainMkt.cbUseGenderClick(Sender: TObject);
begin
  rgGender2.Enabled := cbUseGender.Checked;
end;

procedure TfrmMainMkt.cbUseIataListClick(Sender: TObject);
begin
  memoIataList.Enabled := cbUseIataList.Checked;
  btnIataListClear.Enabled := cbUseIataList.Checked;
end;

procedure TfrmMainMkt.cbUseLastMinClick(Sender: TObject);
begin
  EditDaysBetween.Enabled := cbUseLastMin.Checked;
end;

procedure TfrmMainMkt.cbUsePayTypeClick(Sender: TObject);
begin
  clbPayTYpe2.Enabled := cbUsePayType.Checked;
end;

procedure TfrmMainMkt.cbUsePUDateClick(Sender: TObject);
begin
  dtpPUStart.Enabled := cbUsePUDate.checked;
  dtpPUEnd.Enabled := cbUsePUDate.checked;
end;

procedure TfrmMainMkt.cbUseRange2Click(Sender: TObject);
begin
  if cbuserange2.Checked then
  begin
    if not chkLockMaster.checked then
    begin
      dtpMasterListStart.date := dtpStart.date - 1;
      dtpMasterListEnd.date := dtprange2end.date + 1;
    end;
  end;

  if (cbUseRange3.checked) then
  begin // assuming this date is later than range2
    if not chkLockMaster.checked then
    begin
      if (dtprange3end.date > dtprange2end.date) then
        dtpMasterListEnd.date := dtprange3end.date + 1
      else
        dtpMasterListEnd.date := dtprange2end.date + 1;
    end;
  end;
end;

procedure TfrmMainMkt.cbUseSIPPClick(Sender: TObject);
begin
  clbSIPP1.Enabled := cbUseSipp.Checked;
  clbSIPP2.Enabled := cbUseSipp.Checked;
  clbSIPP3.Enabled := cbUseSipp.Checked;
  clbSIPP4.Enabled := cbUseSipp.Checked;
  if cbUseSIPP.Checked then cbSippWildcard.Checked := false;
end;

procedure TfrmMainMkt.cbUseTYpesClick(Sender: TObject);
begin
  if cbusetypes.Checked then
  begin
    cbProductType.Enabled := true;
    cbProductSUBType.Enabled := true;
    btnReFreshData.Enabled := true;
    btnReFreshData.Click;
  end
  else
  begin
    cbProductType.Enabled := false;
    cbProductSUBType.Enabled := false;
    btnReFreshData.Enabled := false;
  end;
end;


procedure TfrmMainMkt.clbDBExit(Sender: TObject);
begin
  FillNOTOperatorCB(clbdb);
end;

procedure TfrmMainMkt.cbUseSourceClick(Sender: TObject);
begin
  if cbUseSource.Checked then
  begin
    CboxUseSource.Enabled := true;
    fillSourceBox();
  end
  else
    CboxUseSource.Enabled := false;
end;

procedure TfrmMainMkt.connAfterConnect(Sender: TObject);
begin
  try
    GetCountries();
  except on e: exception do
    begin
      memo1.Lines.add(e.Message);
    end;
  end;
end;

procedure TfrmMainMkt.connect1Click(Sender: TObject);
begin
  if conn.Connected then try conn.Close; except; end;
  try
    If extractfilename(paramstr(0)) = 'MarketingRepsDev.exe' then
      Begin
        frmMainMkt.Caption := 'AE Data Storage Interface ********DEV*******';
        Set_FireDAC_Connection(conn, 'DEVWHSE');
      End
    Else
      Begin
        Set_FireDAC_Connection(conn, 'WHSE');
      End;
    conn.Open;
    sb.Panels[0].Text := 'OK: ' + GetCurrentDB(conn);
  except on e: exception do
    begin
      showmessage('Failed to connect: ' + e.message);
    end;
  end;
end;

procedure TfrmMainMkt.disconnect1Click(Sender: TObject);
begin
  try
    conn.close;
    sb.Panels[0].Text := 'disconnected';
  except on e: exception do
    begin
      showmessage('Failed to connect: ' + e.message);
    end;
  end;
end;

procedure TfrmMainMkt.EditCityExit(Sender: TObject);
begin
  EditCity.Text := trim(UpperCase(EditCity.Text));
end;

procedure TfrmMainMkt.EditCountryExit(Sender: TObject);
begin
  EditCountry.Text := trim(UpperCase(EditCOuntry.Text));
end;

procedure TfrmMainMkt.editDateOffsetDblClick(Sender: TObject);
begin
  showmessage('applies to combined, YTD, Fleet and brett reports only.');
end;

procedure TfrmMainMkt.EditDropCityExit(Sender: TObject);
begin
  EditDropCity.Text := trim(UpperCase(EditDropCity.Text));
end;

procedure TfrmMainMkt.EditDropCtryExit(Sender: TObject);
begin
  EditDropCtry.Text := trim(UpperCase(EditDropCtry.Text));
end;


procedure TfrmMainMkt.EditOperatorExit(Sender: TObject);
begin
  EditOperator.Text := trim(UpperCase(EditOperator.Text));
end;

procedure TfrmMainMkt.EMAIL(sFILENAME: string);
var errstr: string;
begin
  if (editToMail.Text = '') then
  begin
    showmessage('Please fill in the "To" field.');
    editToMail.SetFocus;
    exit;
  end
  else if (EditFrom.Text = '') then
  begin
    showmessage('Please fill in the "From" field.');
    EditFrom.SetFocus;
    exit;
  end;

  errStr := '';
  if not (IsValidEMailAddr(editToMail.Text, errStr)) then
  begin
    showmessage('Invalid "to" --> ' + errStr);
    EditToMail.SetFocus;
    exit;
  end;

  errStr := '';
  if not (IsValidEMailAddr(editFrom.Text, errStr)) then
  begin
    showmessage('Invalid "from" --> ' + errStr);
    EditFrom.SetFocus;
    exit;
  end;

  if not (fileexists(sFILENAME)) then
  begin
    showmessage('Can not find ' + sFILENAME + ' = E-mail Failure.');
    exit;
  end;

  Msg.Clear;
  try
    TIdAttachmentFile.Create(Msg.MessageParts, sFILENAME);
  except on e: exception do
    begin
      showmessage('Failed to attach file.');
      exit;
    end;
  end;

  with Msg do
  begin
    Body.Add('Attached is your Report.');
    From.Text := trim(EditFrom.text);
    Recipients.EMailAddresses := trim(editTomail.text);
    Subject := 'AE Data Store reports';
    Priority := TIdMessagePriority(mpNormal);
  end;

  try
    SMTP.Connect;
  except on e: exception do
    begin
      showmessage('failed to connect to server. --> ' + e.Message);
      exit;
    end;
  end;

  try
    try
      SMTP.Send(Msg);
      showmessage('e-mail Sent.');
    except on e: exception do
      begin
        showmessage(e.message + ': Failed to send e-mail.');
      end;
    end;
  finally
    SMTP.Disconnect;
  end;
end;

procedure TfrmMainMkt.FillDBS();
var MyStringList: TStringList;
  I: Integer;
  temp: string;
begin // this was changed around in Dec 2008 for combo list box
  try // originally the db's were stored in a combo box with "ALL"
    MyStringList := TStringList.Create;
    clbdb.Clear;
    clbdb.Sorted := false;

    if (trim(uppercase(paramstr(1))) = 'AUS') or (trim(uppercase(paramstr(1))) = 'NEWZ') then
    begin
      clbdb.Items.Add('AUS');
      clbdb.Items.Add('NEWZ');
    end
    else if (trim(uppercase(paramstr(1))) = 'UK') or (trim(uppercase(paramstr(1))) = 'EURO') then
    begin
      clbdb.Items.Add('EURO');
      clbdb.Items.Add('UK');
    end
    else if (trim(uppercase(paramstr(1))) > '') then
    begin // throttle to the db passed in only
      clbdb.Items.Add(trim(uppercase(paramstr(1))));

    end
    else
    begin
      try
        Get_DBList(MyStringList);
   //     Showmessage(MyStringList.Text);
        for I := 0 to MyStringList.Count - 1 do
        begin
          temp := UpperCase(MyStringList[i]);
          if not (
            (temp = 'TEST') or
            (temp = 'TESTOLD') or
            (temp = 'TESTGLOBAL') or
            (temp = 'TESTGLOBALOLD') or
            (temp = 'GLOBAL') or
            (temp = 'HIST') or
            (temp = 'EAV') or
            (temp = 'QUAL') or
            (temp = 'MKTG') or
            (temp = 'ZA') or
            (temp = 'PHONE') or
            (temp = 'DFW') or
            (temp = 'OPCALLS') or
            (temp = 'QUAL2') or
            (temp = 'QUAL2GLOBAL') or
            (temp = 'ARCH') or
            (temp = 'WHSE') or
            (temp = 'DEVLANGDATA') or
            (temp = 'ACCT') or
            (temp = 'AUSACCT') or (temp = 'POPTART') or
            (temp = 'WEBDATA') or (temp = 'WEBDATA11')
            ) then clbdb.Items.add(temp);
        end;
        clbdb.Sorted := true;
      except on e: exception do
        begin
          showmessage('Fill List of Databases Err --> ' + e.Message);
        end;
      end;
    end; // not aus
  finally
    if assigned(MyStringList) then freeandnil(MyStringList);
  end;
end;

procedure TfrmMainMkt.FormActivate(Sender: TObject);
begin
  getcountries();
end;

procedure TfrmMainMkt.FormCreate(Sender: TObject);
var strUser: string;
  sl: TStringLIst;
begin
 try

  FormIsCreated := false;
  fbolMarcello := false;
  // exit;
  try
    GetSupportingFiles('\\fileserver\UPDATE\ini\idac_c.ini', 'c:\ae\idac_c.ini');
  except;
  end;

  try
    GetSupportingFiles('\\fileserver\UPDATE\ini\firedac_e.ini', 'c:\ae\firedac_e.ini');
  except;
  end;

  try
    GetSupportingFiles('\\fileserver\UPDATE\ini\dss.ini', 'c:\ae\dss.ini');
  except;
  end;

  {try
  //  extra := TExtraOptions.create(nil);
  except;
  end;}

  LOG_PATH := 'c:\clog\';

  try
    try
      sl := TStringList.create;
      sl.LoadFromFile('c:\ae\dss.ini');
      LOG_PATH := sl.Values['logpath'];
      //showmessage(LOG_PATH);
    except on e: exception do
      begin
       showmessage( '  1  ' + e.Message);
        LOG_PATH := 'c:\clog\';
      end;
    end;

    try // special permissions
      specialReportsList := TStringList.Create;
      specialReportsList.Clear;
      Get_ListFromPipeString(specialReportsList, sl.Values['specialreps']);
      //showmessage(godlist.Text);
        except on e: exception do
      begin
       showmessage( '  2  ' + e.Message);
      end;
    end;

  finally
    sl.Free;
  end;

  dtpStart.Date := now - 35;
  dtpMasterListStart.Date := now - 40;
  dtpMasterListEND.Date := now;
  dtpEnd.Date := now - 5;
  dtpRange2start.Date := now - 35;
  dtpRange2End.Date := now - 5;
  dtpRange3start.Date := now - 35;
  dtpRange3End.Date := now - 5;
  dtpPUStart.Date := now;
  dtpPUend.date := now + 30;
  dtpDropDtStart.date := now;
  dtpDropDtend.Date := now + 30;
  dtpTrackReportsStart.Date := now - 30;
  dtpTrackReportsEnd.Date := now;

  dtpCreateDtStart.date := now - 35;
  dtpCreateDtend.Date := now - 5;

  sb.Panels[0].Text := 'disconnected';
  globEMLFileName := '';

  rgDirectTa.ItemIndex := 2;

  try
  FIllDBS(); // locks aus, UK, EURO
  except on e: exception do
      begin
       showmessage( '  7  ' + e.Message);
      end;
  end;

  GlobFName := '';
  height := _MAIN_FORM_HEIGHT; //constraints.minHeight;
  width := _MAIN_FORM_WIDTH; //constraints.minwidth;

  strUser := GetTheUserName();
  if not (strUser = 'SCOTT') then
  begin
    tabForbiddenZone.TabVisible := false;
    cbUseSummaryTable.Visible := false;
    jonLbl.Visible := false;
    btnSpSet.Visible := false;
    btnMainFill.Visible := false;
    btnCSV_HTML.Visible := false; // html dump
    btnCSV_XML.Visible := false; // xml dump
    btnDumpGrid.visible := false;
    btnSippAnalysisSIPP.visible := false; // funny name - no pun intended
    btnSippAnalysis.visible := false;
  end;


  if IsInList(specialReportsList, strUser) then
  begin
    tabspecialreports.TabVisible := true;
    cbActiveOnly.Checked := false;
  end
  else
  begin
    tabspecialreports.TabVisible := false;
  end;

  pagecontrol1.ActivePage := tabStep1;

  // to lock dow for DTAG or any other consortium for that matter:
  // MarketingReps.exe DB ConsortCode
  // MarketingReps.exe UK DTAG --> should only show records for voconsort "DTAG"
  // for AUS : lock to AUS and NEWZ: "MarketingReps.exe AUS" or "MarketingReps.exe NEWZ"
  if paramstr(2) <> '' then
    lblConsort.Caption := 'Consortium Code: ' + paramstr(2);

  try
    SetMemFields();
    except on e: exception do
      begin
       showmessage( '  3  ' + e.Message);
      end;
  end;

  try
    FillGroupByListBox();
    except on e: exception do
      begin
       showmessage( '  4  ' + e.Message);
      end;
  end;

  try
    mem.Close;
    except on e: exception do
      begin
       showmessage( '  5  ' + e.Message);
      end;
  end;

  try
    mem.FieldDefs.Clear;
    except on e: exception do
      begin
       showmessage( '  6  ' + e.Message);
      end;
  end;

  FormIsCreated := true;
 except on e: exception do
    begin
     showmessage( '  10  ' + e.Message);
    end;
 end;
end;

procedure TfrmMainMkt.FormDestroy(Sender: TObject);
begin

  try
    mem2pass.FieldDefs.Clear;
  except;
  end;

  try
    mem.FieldDefs.Clear;
  except;
  end;

  try
  //  freeandnil(extra);
  except;
  end;

  try
    if assigned(specialReportsList) then freeAndNIl(specialReportsList);
  except;
  end;
end;

procedure TfrmMainMkt.FillGroupByListBox();
var i: integer;
  temp: string;
begin
  srcList.Clear;
  DstList.Clear;
  srcList.Sorted := false;

  for i := 0 to mem.fieldcount - 1 do
  begin
    temp := '';
    temp := mem.fields[i].fieldname;

    if (temp = 'time_of_svc') or
      (temp = 'do_time') or
      (temp = 'sipp_description') or
      (temp = 'count_new') or
      (temp = 'count_cxl') or
      (temp = 'count_change') or
      (temp = 'vcount') or
      (temp = 'vcspecrequest') or
      (temp = 'group_break_sort') then
    begin
      nada;
    end
    else
      srcList.Items.Add(temp);
  end;

  srcList.Sorted := true;
end;

procedure TfrmMainMkt.IncludeBtnClick(Sender: TObject);
var
  Index: Integer;
begin
  Index := GetFirstSelection(SrcList);
  MoveSelected(SrcList, DstList.Items);
  SetItem(SrcList, Index);
end;

procedure TfrmMainMkt.ExcludeBtnClick(Sender: TObject);
var
  Index: Integer;
begin
  Index := GetFirstSelection(DstList);
  MoveSelected(DstList, SrcList.Items);
  SetItem(DstList, Index);
end;

procedure TfrmMainMkt.IfxQueryMarcelloFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  showmessage('hello');
//
end;

procedure TfrmMainMkt.IncAllBtnClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to SrcList.Items.Count - 1 do
    DstList.Items.AddObject(SrcList.Items[I],
      SrcList.Items.Objects[I]);
  SrcList.Items.Clear;
  SetItem(SrcList, 0);
end;

procedure TfrmMainMkt.ExcAllBtnClick(Sender: TObject);
var I: Integer;
begin
  for I := 0 to DstList.Items.Count - 1 do
    SrcList.Items.AddObject(DstList.Items[I], DstList.Items.Objects[I]);
  DstList.Items.Clear;
  SetItem(DstList, 0);
end;

procedure TfrmMainMkt.MoveSelected(List: TCustomListBox; Items: TStrings);
var
  I: Integer;
begin
  for I := List.Items.Count - 1 downto 0 do
    if List.Selected[I] then
    begin
      Items.AddObject(List.Items[I], List.Items.Objects[I]);
      List.Items.Delete(I);
    end;
end;

procedure TfrmMainMkt.SetButtons;
var
  SrcEmpty, DstEmpty: Boolean;
begin
  SrcEmpty := SrcList.Items.Count = 0;
  DstEmpty := DstList.Items.Count = 0;
  IncludeBtn.Enabled := not SrcEmpty;
  IncAllBtn.Enabled := not SrcEmpty;
  ExcludeBtn.Enabled := not DstEmpty;
  ExAllBtn.Enabled := not DstEmpty;
end;

function TfrmMainMkt.GetFirstSelection(List: TCustomListBox): Integer;
begin
  for Result := 0 to List.Items.Count - 1 do
    if List.Selected[Result] then Exit;
  Result := LB_ERR;
end;

procedure TfrmMainMkt.SetItem(List: TListBox; Index: Integer);
var
  MaxIndex: Integer;
begin
  with List do
  begin
    SetFocus;
    MaxIndex := List.Items.Count - 1;
    if Index = LB_ERR then Index := 0
    else if Index > MaxIndex then Index := MaxIndex;
    Selected[Index] := True;
  end;
  SetButtons;
end;


end.

