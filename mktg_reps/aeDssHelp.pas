unit aeDssHelp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, Menus;

type
  TfrmDSSHelp = class(TForm)
    twetweb: TMainMenu;
    mnuFile: TMenuItem;
    mnuClose: TMenuItem;
    WebBrowserHelp: TWebBrowser;
    procedure FormCreate(Sender: TObject);
    procedure mnuCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDSSHelp: TfrmDSSHelp;

implementation

{$R *.dfm}
 //\\fileserver\update\help\dss\index.html'
procedure TfrmDSSHelp.FormCreate(Sender: TObject);
begin
  WebBrowserHelp.Navigate('file://fileserver/update/help/dss/index.html');
end;

procedure TfrmDSSHelp.mnuCloseClick(Sender: TObject);
begin
  self.close;
end;

end.
