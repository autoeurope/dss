{
requirements:

compmaster.xtra3 = 'Y'  --> must be set to "Y" to turn discounts on for system
 --- compmaster is a local table to each system

if carte1.cr1rtsw1 = n or w or c = no discount
    n = no to all
    c = no discount for chaos
    w = no discount for web

in CHAOS App:
view params sent to GetDiscs() via memo3 in the quote screen "TEST STUFF" tab

-----------------------------------------------------------------------------------

Returns discount(s) from Table DPLus.

 USAGE: call Get_InMemDisc if you already know if Basic or Plus Rate -> returns Float value for discount
                     sERR = 'OK' if no error thrown
        call GetDiscs if not -> returns JDiscRec below

 ToDo:  -either create a new table called DPlus OR set SQL in this unit to eng_discount
         if eng_discount is used, alter table eng_discount add dplusdaysplus integer;
        -add dplusdays values to each record.
        -ladies to add all the discounts

        MORE TABLE MODIFICATIONS NEEDED:
        -set field disc_stat = 'A' for all active discounts as the sql uses disc_stat
        -set field disc_name = '*' where discname = '*N/A*'

        -comment out code that references DCunit1

// July 3, 2007:

-- added two items to JDiscRec:

   additional dollar amts
   for now they will return 0 until coded:

  --> fBDiscAdditionalAmt: real;
  --> fPDiscAdditionalAmt: real;

-- add two arguments to GetDiscs()  --> ChaosSystem, SourceMarket: String
   this will support Sale Territory arguments for EURO database.

   example: 'EURO', 'DE' --> Looks for "DESITE" discounts after "CHAOS"

// July 3, 2007  -- end

// as of 02/05/2004 on f50_dev
alter table dplus add disc_carnameidx integer default -1;

// Don's DPLus notes:

if (Car^.crr2sw3<>'Y') and (Car^.cr1rtsw2<>'Y') then

new OnRequest:: May 2005 carte1_general cr1ex_type1 = 'OR' = OnRequest

// if either is Y, the Car is flagged as "on request" ..... so it gets no discount

// also --> more importantly for discounts:

  if carte1.cr1rtsw1 = n or w or c = no discount
    n = no to all
    c = no discount for chaos
    w = no discount for web
 begin
     per_DiscountI:= Get_inMemDiscount( DiscountUser, DiscountCap, Car^.Oper,Car^.Sipp ,'I',
                     Car^.carmnamekey , USECAP , temp, inMem_Discount);


     if not Car^.InclusiveMandatory then  // don't get it if you don't need it
     per_DiscountB:= Get_inMemDiscount( DiscountUser, DiscountCap, Car^.Oper,Car^.Sipp ,'B',
                     Car^.carmnamekey , USECAP , temp, inMem_Discount);
 end;

.. we don't discount cargo vans on the web

// remove cargo van discounts

  try
    if Car^.Sipp[2]='K' then
    begin
      discountB:=0.0;
      discountI:=0.0;
    end;
  except
  end;

add CHAOS CAP logic 4/2005

add record to dplus table
require maint to plug cap per record
return cap value in JDiscRec so Ed knows what the upper limit should be for a discount

ALTER TABLE DPLUS ADD disc_cap float;

see CAP logic in GetInMemDisc
see also Global variable CAP : Float


****************************
Remember Carte1.cr1rtsw1 ?
this has to be checked prior to dplus.GetInMemDisc call

if 'n' = no discount
if 'w' = no web discount
if 'c' = no chaos discount

dplus assumes these have been checked prior to call:
in either case, the discount should be zero, therefore no need to call the dplus func:

if cr1rtsw2= 'Y' or  // on request
   crr2sw3 = 'Y' or  // on request
   cr1rtsw1= 'n' or  // no discount at all
   cr1rtsw1= 'c'     // no chaos disc
   cr1rtsw1= 'w'     // no web disc
then
begin
  dsc_rec.fbdisc := 0.00;
  dsc_rec.fpdisc := 0.00;
end;
}
(*

    the OnFilterRecord method NO LONGER needs to be coded in the host application and attached to the inMem's
    onFilter record event. it is all in here .. just pass the mem table in

    speed can be further enhanced by ordering the comparisons in the order of the condition
    most likely to result in accept=false;

    **** this is coded in this unit now and is unnessesary to add to host app ****

    CapValue is used in CHAOS as a record-level disc_cap - this is different from the web
    as the web applies cap based on public discount e.g. a USER should never recieve a higher discount
    than PublicDiscount
*)

{

// ********************************* d_minus logic added 6/15/2005 **

#### obsoleted ####
turn d_minus on or off:
set this (below) constant before compile: "const USE_D_MINUS = true"
**d_minus removed 7/27/2005 and replaced with dplus_adj

// **************************************** d_minus is dead 7/27/2005


IK wants to hedge discounts by USER and Plus days only as agreed upon today .
I have added country through carname in anticipation of later qualification.
for now, I hide all fields but
user, status, dates, plusdays and percent

the schema:

create table dplus_adj
  (
    da_user char(20) not null,
    da_stat char(1) default 'A' not null,
    da_dtstart date not null,
    da_dtend date not null,
    da_daysplus integer default 0,
    da_percent float not null,
    da_country char(2) default '*',
    da_prov char(5) default '*',
    da_city char(20) default '*',
    da_operator char(10) default '*',
    da_sipp char(10) default '*',
    da_bori char(1) default '*',
    da_discname char(20) default '*',
    da_nameidx integer default -1
  );
revoke all on dplus_adj from "public";
create index idx_dplus_adj1 on dplus_adj (da_user,da_stat,da_dtstart,da_dtend,da_daysplus desc) using btree;

08/29/2005: Field additions for Dave Murdock:

ALTER TABLE DPLUS ADD disc_dtchanged datetime year to second
 default current year to second;
ALTER TABLE DPLUS ADD disc_rid integer default 99;
update dplus set disc_dtchanged = current where 1=1;
update dplus set disc_rid = 99 where 1=1;

Sept 21, 2005:
alter table dplus add disc_type char(1);

D: Default
A: Append  -- obsoleted "append" July 2007
O: Over-ride

//*****
use this instead of TFields in the query object.
this way call findFIeld one time whereas FieldBYName has to do it every time

var
 tf_whatever:Tfield

loadfromdataset or whatever

tf_whatever:=inmem.findfield('whatever');

instead of

fieldbyname('whatever).asstring
use
tf_whatever.asstring

10/08/2009

added "MATCHES" routine so users can add wildcards for Operator and SIPP using MATCHES syntax
~ specifically "?" for a placeholder and "*" for wildcard

ALL matching is done in the memTable.OnFilterRecord
since SIPP and Operator are not within the initial query to retreive the discounts

changes made 4/8/2011

add affilaite over ride table: global.dplus_aff_ovrd

1: decide what discount name to use
2: add 2 params to loadDiscs sBASEDISC, sAFFPUSHPULL: String
    sUSER is the affilate or discname, sBASEDISC is base discount alias such as DESITE, sAFFPUSHPULL is value to
    use to look for push pull value from global.dplus_aff_ovrd

Don should pass in the following:
IF the request comes from Bill, he will pass discount name (don just uses that) and AffPushPull

 params in Load Discs:

 sUSER = eng_user.eu_user
 sBASEDISC = eng_user.eu_discgroup            - new
 sAFFPUSHPULL = eng_user.discount_override    - new

 .. part of LOADDISCS which includes new params on the far right:
  sUSER, sCTRY, sPROV, sCTY, sEAST: string; iNUMDAYS: real;
  dQuoteDate, dPUDATE: TDateTime; dQuoteTime, dPUTIME: TDateTime;
  var sERR: string; MEM: TkbmMemTable; sBASEDISC, sAFFPUSHPULL: string): boolean;


vouch.vopconf = discname
vouch.mswitch = over-ride
vopper3 disc%
vopamt+vopamt6 = disc amt

June 2011 add aeglobal:dplus_aff_exceptions
use affname to look for an 'exception discount' name in this table & Use that if it exists.

}

unit dPLUSdisc;

{$DEFINE USE_IDAC} // comment out to use BDE or Linux (windows only)
// {$DEFINE USE_DEBUG} // use for dpmaint gui - comment out for chaos and BE

interface

uses
  SysUtils,

{$IFDEF LINUX}
  DBXpress,
  SqlExpr,
  SQLQueryEx, //dh!
  ChaosCommon,
  main, // dh!
{$ELSE}

{$IFDEF USE_IDAC} // jm
  IfxQuery, IfxConnection,
{$ELSE}
  dbtables,
{$ENDIF}
{$ENDIF}
  DB,
  kbmMemTable;

type
  JDiscRec = record
    fBDisc: real; // Basic Discount %
    fPDisc_CAP: real; // Plus  CAP
    fBDisc_CAP: real; // Basic CAP
    fPDisc: real; // Plus  Discount %
    fBDiscAdditionalAmt: real; // basic $ Amt -- new 8/14/2007
    fPDiscAdditionalAmt: real; // Plus $ amt  -- new 8/14/2007
    sBERR: string; // err Basic = 'OK' if OK
    sPERR: string; // err Plus  "  "   "   "
  end;

type // created to pass back two floats from GetInMemDisc()
  JDiscRec2 = record
    fDiscPercent,
      fDiscDollarAmt: Real;
  end;

type
  TDummy = class
    procedure DiscFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  end;

function ConcatDT(dINDATE: TDateTime; tINTIME: TDateTime): real;
function jGetTimeStr(InTime: TDateTime): string; // return 15:00 for 3PM etc added 11/01/2006
procedure SAdd(var TargetString: string; StringToAdd: string);
// function sLog(sFULLPATH, sFILENAME, sSUFFIX, InString: string): Boolean;
function MATCHES(AString, Pattern: string): boolean; // added 10/08/2009

// new func 4/8/2011 - get affiliate push-pull amount
// function Get_Affilliate_push_pull(InOverd: String): real;

{$IFNDEF LINUX}
function GetDiscs({$IFDEF USE_IDAC} // called from chaos - not booking engines
  conn: TIfxConnection;
{$ELSE}
  sDBNAME,
{$ENDIF}
  sUSER, sCTRY, sPROV, sCTY, sOPER, sSIPP, sCARNAME, sEAST: string; iNUMDAYS: real;
  dQuoteDate, dPUDATE: TDateTime; dQuoteTime, dPUTIME: TDateTime; ChaosSystem, SourceMarket: string): JDiscRec;

  // webdata.eng_defaults add 'ChaosSystem' field, SourceMarket  --> possibly use this table - get organized before implementing
  // any aliasing on this side

function Init({$IFDEF USE_IDAC}conn: TIfxConnection{$ELSE}sDBPARAM: string{$ENDIF}): string;

{$ELSE}

 //!function Init(Connection: TSQLConnection; rq:pRequest): string;  ~ is Linux
function Init(Q: TSQLQueryEx; rq: pRequest): string;
{$ENDIF}

function LoadDiscs(
{$IFDEF USE_IDAC}
  conn: TifxConnection;
{$ELSE}
  sDBNAME,
{$ENDIF}
  sUSER, sCTRY, sPROV, sCTY, {sCARNAME,} sEAST: string; iNUMDAYS: real; // all but car & operator   -- add sBASEDISC and sAFFOUSHPULL 4/8/11
  dQuoteDate, dPUDATE: TDateTime; dQuoteTime, dPUTIME: TDateTime; var sERR: string; MEM: TkbmMemTable;
  sBASEDISC, sAFFPUSHPULL, sBASESYSTEM: string): boolean;
                           // add basesystem to support new dplus_aff_exceptions 06/14/2011

function Get_inMemDiscount(DiscountUser, DiscountCap, Oper, SIPP, BorI: string; // add sBASEDISC 04/08/2011
  sCARNAMEIDX: integer; USECAP: boolean; var errstr: string; MEM: TkbmMemTable; var CapValue: real; sBASEDISC: string): JDiscRec2;

procedure SetFieldPositions(MEM: TkbmMemTable);

var
  query1:
{$IFDEF LINUX}
  TSQLQueryEx //dh!
{$ELSE}

// is Windows
{$IFDEF USE_IDAC}TIfxQuery{$ELSE}TQuery{$ENDIF}
{$ENDIF};

  Discs: JDiscRec;

  im_type,
    im_B_or_I,
    im_Sipp,
    im_Op,
    im_DiscUser: string;

  im_disc_carnameidx: integer;

  Dummy: TDummy;

  tf_disc_type,
    tf_discuser,
    tf_disc_b_or_i,
    tf_discop,
    tf_discsipp, // string
    tf_discpercent, // float
    tf_disc_cap,
    tf_disc_carnameidx,
    tf_ECD_ID,
    tf_da_percent_push_pull,
    // new fields 8/14/2007
  tf_disc_dollar_amt,
    tf_da_dollar_amt,
    tf_affpushpull,
    tf_sDiscNameException
  : TField;

implementation

{$IFDEF USE_DEBUG}
uses dcunit1; // gui discount checker
{$ELSE}
{$ENDIF}

function MATCHES(AString, Pattern: string): boolean;
var
  j, n, n1, n2: integer;
  p1, p2: pchar;
begin
  AString := UpperCase(AString);
  Pattern := UpperCase(Pattern);
  n1 := Length(AString);
  n2 := Length(Pattern);

  if n1 < n2 then n := n1
  else n := n2;

  p1 := pchar(AString);
  p2 := pchar(Pattern);

  for j := 1 to n do
  begin
    if p2^ = '*' then
    begin
      result := true;
      exit;
    end;

    if (p2^ <> '?') and (p2^ <> p1^) then
    begin
      result := false;
      exit;
    end;

    inc(p1);
    inc(p2);
  end;

  if n1 > n2 then
  begin
    Result := False;
    exit;
  end
  else if n1 < n2 then
  begin
    for j := n1 + 1 to n2 do
    begin
      if not (p2^ in ['*', '?']) then
      begin
        result := false;
        exit;
      end;
      inc(p2);
    end;
  end;
  Result := True;
end;

procedure SAdd(var TargetString: string; StringToAdd: string);
var // use this instead of string := String + (this ripps so much faster)
  i, j: integer;
begin
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end;

{
// get promo SQL

Select * From dplus_promo
where
da_ecd_id = 1378
and da_stat = "A"
and
(
 current between da_datestart and da_dateend
 and
 5 between da_weekday_start and da_weekday_end
 and
 '15:00' between da_timestart and da_timeend
)
}

// added 11/01/2006 helper to promo join

function jGetTimeStr(InTime: TDateTime): string;
begin
  result := '00:00';
  try
    result := FormatDateTime('hh:mm', InTime);
  except;
  end;
end;

procedure SetFieldPositions(MEM: TkbmMemTable);
begin
  tf_disc_type := nil;
  tf_discuser := nil;
  tf_disc_b_or_i := nil;
  tf_discop := nil;
  tf_discsipp := nil;
  tf_discpercent := nil;
  tf_disc_cap := nil;
  tf_disc_carnameidx := nil;
  tf_ECD_ID := nil;
  tf_da_percent_push_pull := nil;
  tf_disc_dollar_amt := nil; // dplus - new $amount field 8/14/2007
  tf_da_dollar_amt := nil; // dplus promo - new $amount field
  tf_affpushpull := nil;
  tf_sDiscNameException := nil; // ad 6/20/2011
  if not assigned(MEM) then exit;

  tf_disc_type := mem.findfield('disc_type');
  tf_discuser := mem.findfield('discuser');
  tf_disc_b_or_i := mem.findfield('disc_b_or_i');
  tf_discop := mem.findfield('discop');
  tf_discsipp := mem.findfield('discsipp');
  tf_discpercent := mem.findfield('discpercent');
  tf_disc_cap := mem.findfield('disc_cap');
  tf_disc_carnameidx := mem.findfield('disc_carnameidx');
  tf_ECD_ID := mem.FindField('ecd_id');
  tf_da_percent_push_pull := mem.FindField('da_percent_push_pull');
  tf_disc_dollar_amt := mem.FindField('discdollaramt'); // dplus - new $amount field 8/14/2007
  tf_da_dollar_amt := mem.FindField('da_dollaramt'); // dplus_promo - new $amount field
  tf_affpushpull := mem.FindField('affpushpull'); // new affiliate push pull amt MUNIS_2 04/08/2011
  tf_sDiscNameException := mem.FindField('sDiscNameException'); // add 06/20/2011
end;

{$IFDEF LINUX}

//! function Init(Connection: TSQLConnection; rq:pRequest): string;   ~ it is Linux

function Init(Q: TSQLQueryEx; rq: pRequest): string;
begin // creation - initialization
  result := 'OK';

{$ELSE} // it's windows

function Init({$IFDEF USE_IDAC}conn: TIfxConnection{$ELSE}sDBPARAM: string{$ENDIF}): string;
begin // creation - initialization
  result := 'OK';

{$IFDEF USE_IDAC} // window IDAC init
  if not assigned(conn) then // TIfxConnection
  begin
    result := 'NO connection -> INIT.';
    exit;
  end;
{$ELSE} // windows BDE init
  if trim(sDBPARAM) = '' then // db.name e.g Database1.DatabaseName
  begin
    result := 'NO DBNAME -> INIT.';
    exit;
  end;
{$ENDIF} // use idac
{$ENDIF} // windows init

//dh!
{$IFDEF LINUX}
  // if rq<>nil then
  //   if rq^.rtype=6 then
  //      dmmain.ack( rq, 'D1');
{$ENDIF}

  try
    Query1 :=
{$IFDEF LINUX}
    Q; //TSQLQUERY.Create( dmmain   );   //dh!
{$ELSE} // it's windows
{$IFDEF USE_IDAC}
    TIfxQuery.Create(nil);
{$ELSE}
    TQuery.Create(nil);
{$ENDIF}
{$ENDIF} //.Create(nil);


//dh!
{$IFDEF LINUX}
  // if rq<>nil then
  //   if rq^.rtype=6 then
  //      dmmain.ack( rq, 'D2');
{$ENDIF}

    with Query1 do
    begin
{$IFDEF LINUX}
//!      Query1.SQLConnection := Connection;


{$ELSE}
      RequestLive := false;
      UniDirectional := false;

{$IFDEF USE_IDAC} // windows idac
      connection := conn;
{$ELSE} // window bde
      databasename := sDBPARAM;
{$ENDIF}

      Filtered := false;
{$ENDIF}


//dh!
{$IFDEF LINUX}
  // if rq<>nil then
  //   if rq^.rtype=6 then
  //      dmmain.ack( rq, 'D3');
{$ENDIF}

      ParamCheck := true;
      ObjectView := false;
      Active := false;
    end;
  except on e: exception do
    begin
      result := 'init failure ' + e.Message;
    end;
  end;
end;


procedure DestroyIt();
begin
{$IFNDEF LINUX} //dh!
  if assigned(Query1) then
  try FreeAndNil(Query1); except; end;
{$ENDIF} //dh!
end;

procedure TDummy.DiscFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var s: string; // *** Remember to remove FilterRecord from your host app ****
  i: integer;
begin
  with DataSet do
  begin
    // new type arg 9/28/2005
    s := UpperCase(trim(tf_disc_type.AsString));
    accept := (s = im_type);
    if not accept then exit;

    // im_DiscUser is set prior to this call - if user = user in record then ok keep going else hide record
    s := tf_discuser.AsString;
    accept := (s = im_DiscUser);
    if not accept then exit;

    // if bori matches then ok keep going else hide record
    s := tf_disc_b_or_i.AsString;
    accept := (s = '*') or (s = im_B_or_I);
    if not accept then exit;

    s := tf_discop.AsString;
    //accept := (s = '*') or (s = im_Op);  // change 10/8/2009 added MATCHES Clause
    accept := (s = '*') or (s = im_Op) or (MATCHES(im_Op, s));
    if not accept then exit;

    i := tf_disc_carnameidx.AsInteger;
    accept := (i = -1) or (i = im_disc_carnameidx);
    if not accept then exit;

    s := tf_discsipp.AsString;
    //accept := (s = '*') or (s = im_Sipp);  // change 10/08/2009  added MATCHES Clause
    accept := (s = '*') or (s = im_Sipp) or (MATCHES(im_SIPP, s));
    if not accept then exit;
  end;
end;

function ConcatDT(dINDATE: TDateTime; tINTIME: TDateTime): real;
begin // concatonate Date and Time
  try
    result := trunc(dINDATE) + frac(tINTIME);
  except
    result := -9999.99;
  end;
end;

function InitRec(): boolean;
begin
  result := true;
  try
    Discs.fBDisc := 0.00;
    Discs.fPDisc := 0.00;
    Discs.fBDisc_CAP := 0.00;
    Discs.fPDisc_CAP := 0.00;
    Discs.fBDiscAdditionalAmt := 0.00;
    Discs.fPDiscAdditionalAmt := 0.00;
    Discs.sBERR := 'OK';
    Discs.sPERR := 'OK';
  except result := false;
  end;
end;

{$IFNDEF LINUX}
// this func is never called via LINUX

function GetDiscs({$IFDEF USE_IDAC}
  conn: TIfxConnection;
{$ELSE}
  sDBNAME,
{$ENDIF}
  sUSER, sCTRY, sPROV, sCTY, sOPER, sSIPP, sCARNAME, sEAST: string; iNUMDAYS: real;
  dQuoteDate, dPUDATE: TDateTime; dQuoteTime, dPUTIME: TDateTime; ChaosSystem, SourceMarket: string): JDiscRec;

var
  i, iCARINDEX: integer;
  theCap: real;
  sERR, temp: string;
  MyMem: TkbmMemTable;
  MyRec: JDiscRec2;
begin
  sERR := '';

  if not InitRec() then
  begin
    Discs.fBDisc := 0.00;
    Discs.fPDisc := 0.00;
    Discs.fBDisc_CAP := 0.00;
    Discs.fPDisc_CAP := 0.00;
    Discs.fBDiscAdditionalAmt := 0.00;
    Discs.fPDiscAdditionalAmt := 0.00;
    Discs.sBERR := 'B_BadInit JDiscRec';
    discs.sPERR := 'P_BadInit JDiscRec';
    result := discs;
    exit;
  end;

  result := discs;
  try // remove cargo van discounts ( K is truck but AE uses it for cargo vans )
    if sSIPP[2] = 'K' then exit;
  except;
  end;

  try
    MyMem := TkbmMemTable.Create(nil);
    MyMem.SortOptions := [];
    MyMem.PersistentFile := '';
    MyMem.MasterSource := nil;
    MyMem.FieldDefs.Clear;
    MyMem.CreateTable; // Required.
  except on e: exception do
    begin
      Discs.sBERR := e.message + 'error on temptable create';
      Discs.sPERR := e.message + 'error on temptable create';
      result := discs;
      exit;
    end;
  end;

  sCARNAME := UpperCase(sCARNAME);
  iCARINDEX := -1;

  with {$IFDEF USE_IDAC}TIfxQuery.Create(nil){$ELSE}TQuery.Create(nil){$ENDIF} do
  try
    try
{$IFDEF USE_IDAC}
      connection := conn;
{$ELSE}
      DatabaseName := sDBNAME;
{$ENDIF}

      active := false;
      sql.Clear;
      sql.Add('Select cn_carmfgkey From carname where cn_carmfgname = :CarName');
      ParamByName('CarName').AsString := sCARNAME;
      active := true;
      iCARINDEX := Fields[0].AsInteger;
    except on e: exception do
      begin
        iCARINDEX := -1;
        Discs.sBERR := e.message + '--> Name Index err';
        Discs.sPERR := e.message + '--> Name Index err';
      end;
    end;
  finally
    free;
  end;

  if not assigned(MyMem) then
  begin
    Discs.sBERR := 'mem table not present';
    Discs.sPERR := 'mem table not present';
    result := discs; // redundant
    exit;
  end;

  // add 6/29/2007 to handle SourceCountry discounts in EURO only for now ..
  // Handle Euro Site Discounts  from CHAOS APP only
  // added aus 1/31/2008  per Dave Murdock
  if ((ChaosSystem = 'EURO') or (ChaosSystem = 'AUS')) and (trim(SourceMarket) > '') then
  begin
    sUSER := uppercase(trim(SourceMarket)) + 'SITE';
    // Sales Territory of DE would retreive discount user "DESITE" etc
  end;
                                                                        // 4/08/2011: new args after 'MyMem':
  if not LoadDiscs({$IFDEF USE_IDAC}conn, {$ELSE}sDBNAME, {$ENDIF} // basedisc & affpushpull are not known in chaos, so send blanks
    sUSER, sCTRY, sPROV, sCTY, sEAST, iNUMDAYS, dQuoteDate, dPUDATE, dQuoteTime, dPUTIME, sERR, MyMem, '', '', ChaosSystem) then
  begin
    Discs.sBERR := sERR;
    Discs.sPERR := sERR;
    result := discs;
    exit;
  end;

  sERR := 'OK';
  theCap := 0;

  for i := 0 to 1 do
  begin
    case i of
      0: begin
          sERR := 'OK';
          theCap := 0;

          // must add JDiscRec2 here after GetInMemDisc2 works
          // commented this line out 8/14/2007:
          //discs.fBDisc := Get_inMemDiscount(sUSER, '' {cap}, sOPER, sSIPP, 'B', iCARINDEX, false {UsePublicCap},
          //  sERR, MyMem, theCap);

          MyRec.fDiscDollarAmt := 0;
          MyRec.fDiscPercent := 0;

          MyRec := Get_inMemDiscount(sUSER, '' {cap}, sOPER, sSIPP, 'B', iCARINDEX, false {UsePublicCap},
            sERR, MyMem, theCap, '');

          discs.fBDisc := MyRec.fDiscPercent;
          discs.fBDiscAdditionalAmt := MyRec.fDiscDollarAmt;

          discs.fBDisc_CAP := theCap;
          discs.sBERR := sERR;
        end;
      1: begin
          sERR := 'OK';
          theCap := 0;

          // must add JDiscRec2 here after GetInMemDisc2 works
          //  discs.fPDisc := Get_inMemDiscount(sUSER, '' {cap}, sOPER, sSIPP, 'I', iCARINDEX, false {UsePublicCap},
          //    sERR, MyMem, theCap);

          MyRec.fDiscDollarAmt := 0;
          MyRec.fDiscPercent := 0;

          MyRec := Get_inMemDiscount(sUSER, '' {cap}, sOPER, sSIPP, 'I', iCARINDEX, false {UsePublicCap},
            sERR, MyMem, theCap, '');

          discs.fPDisc := MyRec.fDiscPercent;
          discs.fPDiscAdditionalAmt := MyRec.fDiscDollarAmt;


          discs.fPDisc_CAP := theCap;
          discs.sPERR := sERR;
        end;
    else ; // nada;
    end; // case
  end; // for

  try
    FreeAndNil(MyMem);
  except;
  end;

  result := Discs;
end;

{$ENDIF} // function never called via linux


// get all but car, operator & BorI, carname (carInt) -> filter further in Get_InMemDiscount()


{
Don's call from engines 11/01/2006:

LoadDiscs('xxxx',DiscountUser,ServiceCtry,ProvinceCode,ServiceCity,
                  TravelEast, numdays, now, d1, now, d1, temp, inMem_Discount) ;
}

function LoadDiscs(
{$IFDEF USE_IDAC}
  conn: TifxConnection;
{$ELSE}
  sDBNAME,
{$ENDIF}
  sUSER, sCTRY, sPROV, sCTY, sEAST: string; iNUMDAYS: real;
  dQuoteDate, dPUDATE: TDateTime; dQuoteTime, dPUTIME: TDateTime;
  var sERR: string; MEM: TkbmMemTable; sBASEDISC, sAFFPUSHPULL, sBASESYSTEM: string): boolean;
var // sBASEDISC, sAFFPUSHPULL added 4/08/2011
  discountuser, // should only see these passed in by booking engines currently
    servicectry,
    serviceprovince,
    servicecity,
    traveleast,
    temp,
    spu,
    timeStr,
    alias_user,
    sAFFPP_Value,
    sDiscNameException: string; // sDiscNameException added 06/14/2011 for finding "exception"
                                // discounts entered in aeglobal:dplus_aff_exceptions
                                // this table is replicated to all systems.aeglobal
  hoursnotice,
    pickup,
    snumdays,
    plusdays, fAffPP_Value: real;

  i: integer;

  QuoteDate: TDateTime;

begin
  result := true;
  temp := '';
  sERR := 'OK';
  timeStr := '00:00';

  if not MEM.IsEmpty then
  try
    MEM.EmptyTable;
  except on e: exception do
    begin
      sERR := 'MEM.EmptyTable ' + e.Message;
      result := false;
      exit;
    end;
  end;

  MEM.Filtered := false;
  MEM.Filter := '';

{$IFDEF USE_IDAC}
  if (not assigned(conn)) then
  begin
    sERR := 'NO connection';
    result := false;
    exit;
  end
{$ELSE}
  if (trim(sDBNAME) = '') then
  begin
    sERR := 'NO DB_NAME';
    result := false;
    exit;
  end
{$ENDIF}

  else if (iNUMDAYS = 0) then
  begin
    sERR := 'ZERO DAYS SPECIFIED';
    result := false;
    exit;
  end;

  // remove 4/08/2011
 //  else if trim(sUSER) = '' then
 //  begin
 //   sERR := 'NO USER_NAME SPECIFIED';
  //  result := false;
 //   exit;
 //  end;
 // end remove

  // new args taken from booking engine 4/08/2011:
//   Discuser = Override user passed in from WEB (IESITE, DESITE, FRSITE; may be others like newsletter etc)
//   else Discuser = assigned discuser in xml profile
//   else  Discuser = xml user name (if exists)
//   else discuser = *

  // new code start 04/08/2011

  sDiscNameException := '';  // this was added June 2011 to hande 'google' exceptions
  sDiscNameException := trim(Uppercase(sUSER)); // BE = eng_user.eu_user,  chaos throws 'CHAOS'

  sUSER := trim(Uppercase(sUSER)); // BE = eng_user.eu_user,  chaos throws 'CHAOS'
  sBASEDISC := trim(upperCase(sBASEDISC)); // BE = eng_user.eu_discgroup, chaos throws '' now
  sAFFPUSHPULL := trim(uppercase(sAFFPUSHPULL)); // BE = eng_user.discount_override, chaos ''

  sBASESYSTEM := trim(uppercase(sBASESYSTEM)); // add 06/14/2011
  if sBASESYSTEM = '' then sBASESYSTEM := '*';

  if sBASEDISC > '' then sUSER := sBASEDISC // use base discount if exists
  else if sUSER = '' then sUSER := '*';

  if sAFFPUSHPULL = 'NO_DISC' then exit; // no discount why load
  // end new 04/08/2011


  try // finally

{$IFNDEF LINUX}
{$IFDEF USE_IDAC}
    temp := Init(conn);
{$ELSE}
    temp := Init(sDBNAME);
{$ENDIF}
    if not (temp = 'OK') then
    begin
      sERR := 'bad init. message:  ' + temp;
      result := false;
      exit;
    end;
{$ENDIF}

    if trim(sCTRY) = '' then sCTRY := '*';
    if trim(sPROV) = '' then sPROV := '*';
    if trim(sCTY) = '' then sCTY := '*';
    if trim(uppercase(sEAST)) = 'Y' then sEAST := 'Y' else sEAST := '';
    discountuser := sUSER;
    servicectry := sCTRY;
    serviceprovince := sPROV;
    servicecity := sCTY;
    traveleast := sEAST;
    pickup := ConcatDT(dPUDATE, dPUTIME);
    plusdays := ConcatDT(dPUDATE, dPUTIME) - ConcatDT(dQUOTEDATE, dQuoteTime);
    hoursnotice := plusdays * 24;
    snumdays := iNUMDAYS;
    spu := ' date(' + #39 + formatdatetime('mm/dd/yyyy', pickup) + #39 + ') ';
    timeStr := '"' + jGetTimeStr(dQuoteTime) + '"';

    if plusdays < 0 then
    begin
      result := false;
      sERR := 'PU Prior to QUOTE';
      exit;
    end;

    // added 7/24/2009 -- replace "current" In sql with dQuoteDate
    try
      quotedate := 0.00;
      quotedate := trunc(dQuoteDate);
    except on e: exception do
      begin
        sERR := 'error assigning quoteDate --> ' + E.Message;
        result := false;
        exit;
      end;
    end;

    if (QuoteDate = 0.00) then
    begin
      sERR := 'quoteDate is zero.';
      result := false;
      exit;
    end;
    // end 7/24

    // new code for aff push pull 4/08/2011
    fAffPP_Value := 0.0;
    sAFFPP_Value := '0.0';
    if sAFFPUSHPULL > '' then // get the value associated with aff_override / affpushpull, whatever you call it
    begin // this will never get called from CHAOS
      try
        query1.active := false;
        query1.SQL.Clear;

         query1.SQL.add('select dafo_percent From aeglobal::dplus_aff_ovrd where dafo_name = "' + sAFFPUSHPULL + '";');

        // for testing only use above line in live
       // query1.SQL.add('select dafo_percent From aeglobal_test::dplus_aff_ovrd where dafo_name = "' + sAFFPUSHPULL + '";');

        query1.active := true;
        fAffPP_Value := query1.Fields[0].AsFloat;
        //sAFFPP_Value := FloatToStr(iAffPP_Value);
        sAFFPP_Value := formatFloat('##0.0', fAffPP_Value);

{$IFDEF USE_DEBUG}
        try
          formTestdiscs.memo2.lines.add('*******  AffPushPull **********************');
          formTestdiscs.memo2.lines.add('looked for [' + sAFFPUSHPULL + '] found [' + sAFFPP_Value + ']');
          formTestdiscs.memo2.lines.add('******* end AffPushPull *******************');
        except;
        end;
{$ELSE}
{$ENDIF}

      except on e: exception do
        begin
          sERR := 'failed to get aff_push pull --> ' + e.Message;
          fAffPP_Value := 0.0;
          sAFFPP_Value := '0.0';
        end;
      end;
    end;
    // end ned code 04/08/2011

    // new code to find the discount exception 6/14/2011
    // load the exception name into the mem table..in Get_in_memDisc, look for it and use it if it exists

    if sDiscNameException > '' then // get the value associated with aff_override / affpushpull, whatever you call it
    begin // this will never get called from CHAOS
      try
        query1.active := false;
        query1.SQL.Clear;

        query1.SQL.add('select dafx_discname From aeglobal::dplus_aff_exceptions where dafx_affname = "' + sDiscNameException + '" ');
        // comment out - this is fot testing
       //  query1.SQL.add('select dafx_discname From aeglobal_test::dplus_aff_exceptions where dafx_affname = "' + sDiscNameException + '" ');

{$IFDEF USE_DEBUG}
        try
          formTestdiscs.memo2.lines.add('*******  Name Exceptions **********************');
          formTestdiscs.memo2.lines.add('looked for name exc for [' + sDiscNameException + ']');
        except;
        end;
{$ELSE}
{$ENDIF}

        if sBASESYSTEM = '*' then
          query1.SQL.add('and dafx_basesystem = "*" order by dafx_basesystem DESC;')
        else
          query1.SQL.add('and dafx_basesystem in ("*", "' + sBASESYSTEM + '" ) order by dafx_basesystem DESC;');

        query1.active := true;

        if query1.IsEmpty then sDiscNameException := ''
        else sDiscNameException := trim(uppercase(query1.Fields[0].AsString));

{$IFDEF USE_DEBUG}
        try
          formTestdiscs.memo2.lines.add('Name exception found [' + sDiscNameException + ']');
          formTestdiscs.memo2.lines.add('******* end Name Exceptions *******************');
        except;
        end;
{$ELSE}
{$ENDIF}
      except on e: exception do
        begin
          sERR := 'FAILED to get discount name exception --> ' + e.Message;
          sDiscNameException := '';
        end;
      end;
    end;
    //end new code 06/14/2011

    try
      with query1 do
      begin
        Active := False;
        with SQL do
        begin
          Clear;

          Add('Select ecd_id, disc_b_or_i,discsipp,discop,discuser,disc_carnameidx,discpercent,discsort,disccountry,');
          Add('disccity,disc_prov,dplusdaysplus, disc_cap, disc_type, discdollaramt '); // added disc_type 9/22/2005
          Add(', da_percent_push_pull, da_dollaramt'); // new join for PROMO addition 11/1/2006

          // added 4/08/2011
          add(', ' + sAFFPP_Value + ' as affpushpull ');
          add(', "' + sDiscNameException + '" as sDiscNameException '); // new 6/14/2011 plug into table so you don't have to find the alias again

          // "outer dplus_promo" added for PROMO addition 11/1/2006
          Add(' from dplus, outer dplus_promo where discproduct = "CAR" and '); // old ok 6/15/2005

          // dh! only pull back the fields necessary. Filtering works much faster w/ fewer fields.
          // In fact, since the initial order by gets the records in the right order, we can even remove the columns
          // from the inmem table that had to be there for the order by
          // but are not required for the getinmemdiscount function   (shweeeeeet)

          Add('disc_stat = "A" and'); // remove upper 11/07/2006
          Add('(discdept in( :pDEPT, "*")) and');

          // adding 'CHAOS' as default discount, then getting other discount as over-ride or additional disc 9/22/2005
          //   if trim(UpperCase(discountuser)) = 'CHAOS' then Add('( discuser in(:pUSER,"*" )) and')
          //   else Add('( discuser in(:pUSER,"*","CHAOS" )) and');


          // add discExeption 06/14/2011
          if (sDiscNameException > '') then
          begin
            if trim(UpperCase(discountuser)) = 'CHAOS' then Add('( discuser in(:discException, :pUSER,"*" )) and')
            else Add('( discuser in(:discException, :pUSER,"*","CHAOS" )) and');
          end
          else
          begin
            if trim(UpperCase(discountuser)) = 'CHAOS' then Add('( discuser in(:pUSER,"*" )) and')
            else Add('( discuser in(:pUSER,"*","CHAOS" )) and');
          end;

          Add('(disccountry in(:pCTRY, "*")) and');
          Add('(disc_prov in(:pPROV, "*")) and');
          Add('(disccity in(:pCITY, "*")) and');

          // operator,sipp,b_OR_i unknown at this point & car nameIndex
          add('dplusdaysplus <= :pDPLUS and');
          Add('( ' + spu + ' not between disc_bo_startdt and disc_bo_enddt) and'); // black-out

          // remember to set discpubyuend = discpuby then set discpuby to date earlier than today.
          Add('( ' + spu + ' between discpickupby and discpickupbyend) and');
          Add('( :pDAYS between discmindays and discmaxdays) and');

       //   Add('( date(current) between discfrom and discto) and'); // may have to eventaully plug dQuoteDate instead of date(current) for UK / AUS Time differences
          Add('( :qdate between discfrom and discto) and'); // may have to eventaully plug dQuoteDate instead of date(current) for UK / AUS Time differences


          Add('(disc_hrs_notice <= :pHRSNOT or disc_hrs_notice is null)');

          if travelEast = 'Y' then add('and disctrvleast="Y" ');

          // new join for PROMO addition 11/1/2006
          Add('and da_ecd_id = ecd_id and da_stat = "A"');

       //   Add('and date(current) between da_datestart and da_dateend'); // note: for Time differences:
       //   Add('and WEEKDAY(current) between da_weekday_start and da_weekday_end'); // may have to use DayOfTheWeek(dQuoteDate) at some point
          Add('and :qdate between da_datestart and da_dateend'); // note: for Time differences:
          Add('and WEEKDAY(:qdate) between da_weekday_start and da_weekday_end'); // may have to use DayOfTheWeek(dQuoteDate) at some point

          Add('and ' + timeStr + ' between da_timestart and da_timeend'); // may have to use dQuoteTime at some point

          // end new join for PROMO addition 11/1/2006
          // use this argument for the carname index. was "discname desc" to the farthest right
          Add('order by discsort, disccountry desc,disc_prov desc,disccity desc,discop desc,discsipp desc,disc_b_or_i desc,dplusdaysplus desc,disc_carnameidx desc');

          parambyname('pDPLUS').asFloat := plusdays;
          parambyname('pDAYS').asFloat := snumdays;
          parambyname('pDEPT').AsString := 'I'; // change later for multiple departments
          parambyname('pUSER').AsString := discountuser;
          parambyname('pCTRY').AsString := servicectry;
          parambyname('pPROV').AsString := serviceprovince;
          parambyname('pCITY').AsString := servicecity;
          parambyname('pHRSNOT').AsFloat := hoursnotice;
          parambyname('qdate').AsDate := quotedate;
          if (sDiscNameException > '') then parambyname('discException').AsString := sDiscNameException;

        end;

{$IFDEF USE_DEBUG}
        //try
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', query1.SQL.Text);
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'plusdays: ' + FloatToStr(plusdays));
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'pdays: ' + FloatToStr(snumdays));
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'user: ' + discountuser);
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'pctry: ' + servicectry);
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'prov: ' + serviceprovince);
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'city: ' + servicecity);
        //  slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', 'hours notice: ' + FLoatToStr(hoursnotice));
       // except;
       // end;

        try
          formTestdiscs.memo2.lines.add('*******  load discs sql **********************');
          formTestdiscs.memo2.lines.add(query1.sql.text);
          sAdd(sERR, ' plusdays: ' + floatToStr(plusdays) + #10#13);
          sAdd(sERR, ' numdays: ' + floattostr(snumdays) + #10#13);
          sAdd(sERR, ' pDEPT: I' + #10#13);
          sAdd(sERR, ' USER: ' + sUSER + #10#13);
          sAdd(sERR, ' discException [' + sDiscNameException + ']' + #10#13);
          sAdd(sERR, ' PickUpDate: ' + DateToStr(pickup) + #10#13);
          sAdd(sERR, ' country: ' + servicectry + #10#13);
          sAdd(sERR, ' Province: ' + serviceprovince + #10#13);
          sAdd(sERR, ' city: ' + servicecity + #10#13);
          sAdd(sERR, ' HoursNotice: ' + FloatToStr(hoursnotice) + #10#13);
          sAdd(sERR, ' Quote date: ' + datetostr(quotedate) + #10#13);
          sAdd(sERR, ' Quote Time in: ' + timeStr);
          formTestdiscs.memo2.lines.add(serr);
          formTestdiscs.memo2.lines.add('******* end sql *******************');
        except;
        end;
{$ELSE}
{$ENDIF} // _debug only

        active := true;

        //dh! drop these fields
        //no longer needed by inmem_getdiscount...slows down filter
        with Query1.FieldDefs do
        begin
          Delete(IndexOf('disccountry'));
          Delete(IndexOf('disccity'));
          Delete(IndexOf('disc_prov'));
          Delete(IndexOf('discsort'));
          Delete(IndexOf('dplusdaysplus'));
        end;

        mem.filtered := False;
        MEM.LoadFromDataSet(Query1, [mtcpoStructure, mtcpoProperties, mtcpoLookup, mtcpoCalculated]);
        SetFieldPositions(MEM);

{$IFDEF USE_DEBUG}
        formTestdiscs.memo2.lines.add('*** discounts loaded *** ');
{$ELSE}
{$ENDIF}

      end; // Query1
    except on e: exception do
      begin
        result := false;
        sERR := 'BIG EXCEPT 1' + e.Message + '  ' + query1.SQL.text;
        exit;
      end;
    end;
  finally
    Destroyit();
  end;
end;


function Get_inMemDiscount(DiscountUser, DiscountCap, Oper, SIPP, BorI: string; // added sBASEDISC 04/08/2011
  sCARNAMEIDX: integer; USECAP: boolean; var errstr: string; MEM: TkbmMemTable; var CapValue: real; sBASEDISC: string): JDiscRec2;
var
  temp,
    _debugStr: string;

  // removed temp_disc & temp_cap - was only used for "append", therefore always zero
  // replaced all variables assigned by GetPromo() with JDiscRec2
  // temp_promo, user_promo etc
  // temp_disc, temp_cap, temp_promo, AE_PublicPromo, user_promo,

  AE_PublicDiscount, AE_PublicDollarAmt, AE_PublicCAP,
    userDiscount, userDollarAmt, UserCap,
    chaosDiscount, chaosDollarAmt, chaosCAP,
    overRideDiscount, overRideDollarAmt, overRideCap,
    // added affPushPull vars 4/08/2011
  CHAOS_AffPushPull,
    AE_Public_AffPushPull,
    user_AffPushPull,
    overRide_AffPushPull: Real;

  i: integer;

  foundOverRides,
    foundNewOverides: boolean;

  chaos_PromoRec,
    AE_PublicPromoRec,
    user_promoREC,
    overRide_PromoRec: JDiscRec2;

  function GetPromo(): JDiscRec2; // call: temp_promo := GetPromo();
  begin // added 11/01/2006 to support dplus_promo
    result.fDiscDollarAmt := 0;
    result.fDiscPercent := 0;

    if (not tf_da_percent_push_pull.IsNull) and (tf_da_percent_push_pull.AsFloat > 0) then
      result.fDiscPercent := tf_da_percent_push_pull.AsFloat;

    if (not tf_da_dollar_amt.IsNull) and (tf_da_dollar_amt.AsFloat > 0) then
      result.fDiscDollarAmt := tf_da_dollar_amt.AsFloat;
  end;

  function getAffPushPull(): real;
  begin // new 04/08/20111
    result := 0;
    if (not tf_affpushpull.IsNull) and (tf_affpushpull.AsFloat <> 0) then
      result := tf_affpushpull.AsFloat;
  end;

begin
  // new fields to code for 8/14/2007:
  // tf_disc_dollar_amt :=  mem.FindField('discdollaramt'); // dplus
  // tf_da_dollar_amt :=  mem.FindField('da_dollaramt');    // dplus_promo

  errstr := 'OK';
  _debugStr := '';
  CapValue := 0; // the value set as cap at DLPUS.Recordlevel (for chaos)

  // containers for var CapValue:
  AE_PublicCAP := 0;
  UserCap := 0;
  chaosCAP := 0;
  Result.fDiscDollarAmt := 0;
  Result.fDiscPercent := 0;

  if not assigned(MEM) then
  begin
    errstr := 'GetInMemDisc -> no mem table object';
    exit;
  end
  else
  begin
    MEM.Filtered := false;
    MEM.Filter := '';
    MEM.OnFilterRecord := Dummy.DiscFilterRecord;
  end;

  if (MEM.IsEmpty)
    then
  begin
    errstr := 'no records in InMem';
    exit;
  end;

  if (trim(Oper) = '')
    then
  begin
    errstr := 'param for operator not specified.';
    exit;
  end;

  if (trim(SIPP) = '')
    then
  begin
    errstr := 'sipp param missing. ';
    exit;
  end;

  if (trim(BorI) = '')
    then
  begin
    errstr := 'basic inclusive missing. ';
    exit;
  end;

{$IFDEF USE_DEBUG}
  _debugStr := 'Mem assigned OK';
{$ELSE}
{$ENDIF}

  BorI := trim(BorI);
  if not ((BorI = 'B') or (BorI = 'I')) then BorI := '*';
  SIPP := UpperCase(trim(SIPP));
  Oper := UpperCase(trim(Oper));
  if Trim(DiscountCap) = '' then DiscountCap := '*';
  if (sCARNAMEIDX < -1) then sCARNAMEIDX := -1;
  AE_PublicDiscount := 0;
  AE_PublicDollarAmt := 0;
  userDiscount := 0;
  userDollarAmt := 0;
  chaos_PromoRec.fDiscDollarAmt := 0; // replaced temp with "chaos" - must hold chaos promo rather than over-write the temp
  chaos_PromoRec.fDiscPercent := 0;
  AE_PublicPromoRec.fDiscDollarAmt := 0;
  AE_PublicPromoRec.fDiscPercent := 0;
  user_promoRec.fDiscDollarAmt := 0;
  user_promoRec.fDiscPercent := 0;
  CHAOS_AffPushPull := 0;
  AE_Public_AffPushPull := 0;
  user_AffPushPull := 0;

  // new variables for exception / overide discounts - these are actually over-rides but described as
  // "discount exceptions" by the D+ maint user folks (Preston)
  overRideDiscount := 0;
  overRideDollarAmt := 0;
  overRideCap := 0;
  overRide_AffPushPull := 0; 

  // new code start 04/08/2011
  DiscountUser := trim(Uppercase(discountUser)); // BE = eng_user.eu_user,  chaos throws 'CHAOS'
  sBASEDISC := trim(upperCase(sBASEDISC)); // BE = eng_user.eu_discgroup, chaos throws nothing now

  if sBASEDISC > '' then DiscountUser := sBASEDISC // use base discount if exists
  else if DiscountUser = '' then DiscountUser := '*'; // ? Maybe not star here ??
  // end  new code

  // begin new override code 6/14/2011
  // If there are over-ride discounts assigned, use them and exit.
  // ************************** begin new exception / over-ride code ************************************

  foundNewOverides := false;  // field will have a value if found - use on 1st filter ...
  if (not tf_sDiscNameException.IsNull) and (trim(tf_sDiscNameException.AsString) > '') then
  begin

{$IFDEF USE_DEBUG}
    formTestdiscs.memo2.lines.add('*** exception name [' +
      tf_sDiscNameException.AsString + '] exists. looking for exception discount *** ');
{$ELSE}
{$ENDIF}

  // June 2011: exception / over-ride retrieval
  // must get OVERIDE discount first IF it exists ALWAYS
    try // set global vars for use by onFilter function
      im_type := 'O'; // 'overd' is always 'O' for default
      im_B_or_I := BorI;
      im_Sipp := Sipp;
      im_Op := Oper;
      im_DiscUser := trim(upperCase(tf_sDiscNameException.AsString));
      im_disc_carnameidx := sCARNAMEIDX;

      if Mem.filtered then Mem.Refresh
      else MEM.Filtered := true;

    except on e: exception do
      begin
        errstr := e.Message + ' can''t set mem.filter override';
        exit;
      end;
    end;

    mem.First;
    overRideDiscount := 0;
    overRideDollarAmt := 0;
    if not (mem.IsEmpty) then foundNewOverides := true;

{$IFDEF USE_DEBUG}
    if foundNewOverides then
      formTestdiscs.memo2.lines.add('*** exception name 1st filter: foundOverRides is TRUE')
    else
      formTestdiscs.memo2.lines.add('*** exception name 1st filter: foundOverRides is FALSE');
{$ELSE}
{$ENDIF}

    try
      overRideDiscount := tf_discpercent.AsFloat;
      overRideDollarAmt := tf_disc_dollar_amt.AsFloat;
      overRideCap := tf_disc_cap.AsFloat;
      overRide_PromoRec := GetPromo();
      overRide_AffPushPull := 0; // we are not pushing or pulling over-rides

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' --> 1 ID: [' + tf_ECD_ID.AsString +
          '] overRideDiscount: ' + FloatTOStr(overRideDiscount) +
          ' overRideDollarAmt: ' + FloatTOStr(overRideDollarAmt) +
          ' overRideCap ' + FLoatTOStr(overRideCap) +
          ' overRide promo% ' + FloatToStr(overRide_promorec.fDiscPercent) +
          ' overRide promo$ ' + FloatToStr(overRide_promorec.fDiscDollarAmt);

        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.Add('overRide top disc: ' + temp);
        if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** 1st overRide DISCOUNT - filtered = true ****');
      except;
      end;
{$ELSE}
{$ENDIF}

    except
      overRideDiscount := 0;
      overRideDollarAmt := 0;
      overRideCap := 0;
      overRide_PromoRec.fDiscDollarAmt := 0;
      overRide_PromoRec.fDiscPercent := 0; // change to chaos promorec 2/22/2008

{$IFDEF USE_DEBUG}
      SAdd(_debugStr, ' --> 1 ERROR ' +
        ' overRideDiscount: ' + FLoatTOStr(overRideDiscount) +
        ' overRideDollarAmt: ' + FloatToStr(overRideDollarAmt) +
        ' overRideCap ' + FLoatTOStr(overRideCap) +
        ' overRide promo% ' + FloatToStr(overRide_promorec.fDiscPercent) +
        ' overRide promo$ ' + FloatToStr(overRide_promorec.fDiscDollarAmt) // change to chaos promorec 2/22/2008
        );

      formtestdiscs.Memo2.Lines.Add('ERROR getting overRide disc; overRide disc set to zero.');
{$ELSE}
{$ENDIF}
    end;

    //car name is sorted to the last record
    if sCARNAMEIDX <> -1 then
    try
      mem.Last;
      if (tf_disc_carnameidx.AsInteger > -1) and (tf_discpercent.AsFloat > overRideDiscount) then
      begin
        try
          overRideDiscount := tf_discpercent.AsFloat;
          overRideDollarAmt := tf_disc_dollar_amt.AsFloat;
          overRideCap := tf_disc_cap.AsFloat;
          overRide_PromoRec := GetPromo();
          overRide_AffPushPull := 0;

{$IFDEF USE_DEBUG}
          try
            temp := '';
            temp := ' specific car index:: --> 2 ID: ' + tf_ECD_ID.AsString +
              ' overRideDiscount: ' + FLoatTOStr(overRideDiscount) +
              ' overRideDollarAmt: ' + FloatToStr(overRideDollarAmt) +
              ' overRideCap ' + FLoatTOStr(overRideCap) +
              ' overRidePromo% ' + floattostr(overRide_PromoRec.fDiscPercent) +
              ' overRidePromo$ ' + floattostr(overRide_PromoRec.fDiscDollarAmt);

            SAdd(_debugStr, temp);
            formtestdiscs.Memo2.Lines.Add('last overRide disc based on carnameidx <> -1: ' + temp);
            if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** car name idx discount found for overRide ****');
          except;
          end;
{$ELSE}
{$ENDIF}
        except
          overRideDiscount := 0;
          overRideDollarAmt := 0;
          overRideCap := 0;
          overRide_PromoRec.fDiscDollarAmt := 0;
          overRide_PromoRec.fDiscPercent := 0;
        end;
      end;
    except
      overRideDiscount := 0;
      overRideDollarAmt := 0;
      overRideCap := 0;
      overRide_PromoRec.fDiscDollarAmt := 0;
      overRide_PromoRec.fDiscPercent := 0;
    end;

    if (foundNewOverides) then
    begin                                                                        // push pull always zero here
      result.fDiscPercent := overrideDiscount + override_PromoRec.fDiscPercent + override_AffPushPull;
      result.fDiscDollarAmt := overrideDollarAmt + override_PromoRec.fDiscDollarAmt;
      CapValue := overrideCap;

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' user was a super-new-override [' + im_DiscUser + '] so exiting with these values: ' +
          ' %: ' + floattostr(result.fDiscPercent) +
          ' $: ' + floattostr(result.fDiscDollarAmt) +
          ' capvalue: ' + FLoatTOStr(chaosCap);
        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.lines.add(temp);
      except;
      end;
{$ELSE}
{$ENDIF}
      exit; // no need to continue if found an over-ride ...
    end; // foundoverRides  was true
  end; // tf_sDiscNameException was not nil and contained a value


 // *********************************** end new override code 06/14/2011 ***************************************************************

 {$IFDEF USE_DEBUG}
  formTestdiscs.memo2.lines.add('**************** OverRides NOT found - continue with Standard Method *************');
{$ELSE}
{$ENDIF}

  // must get CHAOS discount first ALWAYS per IK
  try // set global vars for use by onFilter function
    im_type := 'D'; // 'CHAOS' is always 'D' for default
    im_B_or_I := BorI;
    im_Sipp := Sipp;
    im_Op := Oper;
    im_DiscUser := 'CHAOS'; // jm 9/25/2005
    im_disc_carnameidx := sCARNAMEIDX;

    if Mem.filtered then Mem.Refresh
    else MEM.Filtered := true;
  except on e: exception do
    begin
      errstr := e.Message + ' can''t set mem.filter';
      exit;
    end;
  end;

  mem.First;
  chaosDiscount := 0;
  chaosDollarAmt := 0;

  try
    chaosDiscount := tf_discpercent.AsFloat;
    chaosDollarAmt := tf_disc_dollar_amt.AsFloat;
    chaosCap := tf_disc_cap.AsFloat;
    chaos_PromoRec := GetPromo();
    CHAOS_AffPushPull := getAffPushPull();

{$IFDEF USE_DEBUG}
    try
      temp := '';
      temp := ' --> 1 ID: [' + tf_ECD_ID.AsString +
        '] chaosDiscount: ' + FloatTOStr(chaosDiscount) +
        ' chaosDollarAmt: ' + FloatTOStr(chaosDollarAmt) +
        ' chaosCap ' + FLoatTOStr(chaosCap) +
        ' chaos promo% ' + FloatToStr(chaos_promorec.fDiscPercent) +
        ' chaos promo$ ' + FloatToStr(chaos_promorec.fDiscDollarAmt);

      SAdd(_debugStr, temp);
      formtestdiscs.Memo2.Lines.Add('chaos top disc: ' + temp);
      if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** 1st CHAOS DISCOUNT - filtered = true ****');
    except;
    end;
{$ELSE}
{$ENDIF}

  except
    chaosDiscount := 0;
    chaosDollarAmt := 0;
    chaosCap := 0;
    chaos_PromoRec.fDiscDollarAmt := 0;
    chaos_PromoRec.fDiscPercent := 0; // change to chaos promorec 2/22/2008

{$IFDEF USE_DEBUG}
    SAdd(_debugStr, ' --> 1 ERROR ' +
      ' chaosDiscount: ' + FLoatTOStr(chaosDiscount) +
      ' chaosDollarAmt: ' + FloatToStr(chaosDollarAmt) +
      ' chaosCap ' + FLoatTOStr(chaosCap) +
      ' chaos promo% ' + FloatToStr(chaos_promorec.fDiscPercent) +
      ' chaos promo$ ' + FloatToStr(chaos_promorec.fDiscDollarAmt) // change to chaos promorec 2/22/2008
      );

    formtestdiscs.Memo2.Lines.Add('ERROR getting chaos top disc; chaos top disc set to zero.');
{$ELSE}
{$ENDIF}
  end;

  //car name is sorted to the last record
  if sCARNAMEIDX <> -1 then
  try
    mem.Last;
    if (tf_disc_carnameidx.AsInteger > -1) and (tf_discpercent.AsFloat > chaosDiscount) then
    begin
      try
        chaosDiscount := tf_discpercent.AsFloat;
        chaosDollarAmt := tf_disc_dollar_amt.AsFloat;
        chaosCap := tf_disc_cap.AsFloat;
        chaos_PromoRec := GetPromo();
        CHAOS_AffPushPull := getAffPushPull();

{$IFDEF USE_DEBUG}
        try
          temp := '';
          temp := ' specific car index:: --> 2 ID: ' + tf_ECD_ID.AsString +
            ' chaosDiscount: ' + FLoatTOStr(chaosDiscount) +
            ' chaosDollarAmt: ' + FloatToStr(chaosDollarAmt) +
            ' chaosCap ' + FLoatTOStr(chaosCap) +
            ' chaosPromo% ' + floattostr(chaos_PromoRec.fDiscPercent) +
            ' chaosPromo$ ' + floattostr(chaos_PromoRec.fDiscDollarAmt);

          SAdd(_debugStr, temp);
          formtestdiscs.Memo2.Lines.Add('last "CHAOS" disc based on carnameidx <> -1: ' + temp);
          if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** car name idx discount found for "CHAOS" ****');
        except;
        end;
{$ELSE}
{$ENDIF}

      except
        chaosDiscount := 0;
        chaosDollarAmt := 0;
        chaosCap := 0;
        chaos_PromoRec.fDiscDollarAmt := 0;
        chaos_PromoRec.fDiscPercent := 0;
      end;
    end;
  except
    chaosDiscount := 0;
    chaosDollarAmt := 0;
    chaosCap := 0;
    chaos_PromoRec.fDiscDollarAmt := 0;
    chaos_PromoRec.fDiscPercent := 0;
  end;

  if (Uppercase(discountuser) = 'CHAOS') then
  begin
   // result.fDiscPercent := chaosDiscount + chaos_PromoRec.fDiscPercent;
   // result.fDiscDollarAmt := ChaosDollarAmt + chaos_PromoRec.fDiscDollarAmt; // change to chaos_promoRec 1/22/2008

   // change 04/08/2011
    result.fDiscPercent := chaosDiscount + chaos_PromoRec.fDiscPercent + CHAOS_AffPushPull;
    result.fDiscDollarAmt := ChaosDollarAmt + chaos_PromoRec.fDiscDollarAmt; // change to chaos_promoRec 1/22/2008
    CapValue := chaosCap;

{$IFDEF USE_DEBUG}
    try
      temp := '';
      temp := ' user was "CHAOS" so exiting with these values: ' +
        ' %: ' + floattostr(result.fDiscPercent) +
        ' $: ' + floattostr(result.fDiscDollarAmt) +
        ' capvalue: ' + FLoatTOStr(chaosCap);
      SAdd(_debugStr, temp);
      formtestdiscs.Memo2.lines.add(temp);
    except;
    end;
{$ELSE}
{$ENDIF}

    exit; // no need to continue if "CHAOS" is the user
  end;

  // chaos discount should be set by this line

{$IFDEF USE_DEBUG}
  temp := '';
  temp := '  --> 4 : USER <> "CHAOS" chaos discount: ' + FLoatTOStr(chaosDiscount) +
    ' ChaosCap ' + FLoatTOStr(chaosCap);
  SAdd(_debugStr, temp);
  formtestdiscs.Memo2.Lines.Add(temp);
{$ELSE}
{$ENDIF}

  // ---- now get Don's (web) discount
  if (discountuser = '*') or (USECAP) then // most always true except when called via CHAOS since user is always "CHAOS"
  begin // get public CAP first
    AE_PublicDiscount := 0;
    AE_PublicDollarAmt := 0;
    AE_PublicPromoRec.fDiscDollarAmt := 0;
    AE_PublicPromoRec.fDiscPercent := 0;

{$IFDEF USE_DEBUG}
    temp := '';
    temp := ' --> 5 : "(discountuser = "*") or (USECAP) = TRUE';
    SAdd(_debugStr, temp);
    formtestdiscs.Memo2.Lines.Add(temp);
{$ELSE}
{$ENDIF}

    try // dh set globals for use by onFilter function
      im_B_or_I := BorI; //'*'; to get User specific public discount, use '*' here NOT B_OR_I but the record must User Public (*) record must exist
      im_Sipp := Sipp;
      im_Op := Oper;
      im_DiscUser := '*'; // to get User specific public discount, use the DiscUser not '*' here
      im_disc_carnameidx := sCARNAMEIDX;
    except on e: exception do
      begin
        errstr := e.Message + ' can''t set mem.filter->';
        exit;
      end;
    end;

    // remove Append Type 8/14/2007 - never used
    // includes removing For Loop - see old func which is commented out..
    im_type := 'O';

    try if Mem.filtered then Mem.Refresh else MEM.Filtered := true; except; end;
    mem.First;

    foundOverRides := not (mem.IsEmpty);
    try
      AE_PublicDiscount := tf_discpercent.AsFloat;
      AE_PublicDollarAmt := tf_disc_dollar_amt.AsFloat;
      AE_PublicCAP := tf_disc_cap.AsFloat;
      AE_PublicPromoRec := GetPromo();
      AE_Public_AffPushPull := getAffPushPull();


{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := '  [CAP = TRUE or USER = "*"] --> ID: ' + tf_ECD_ID.AsString + ' --> 10 AE_PublicDiscount = ' + tf_discpercent.AsString +
          ' AE_PublicCAP = ' + tf_disc_cap.AsString + ' AE_PublicPromo: ' +
          FloatToStr(AE_PublicPromoRec.fDiscPercent);
        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.Add(temp);
        if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** if discuser = "*" or UseCap - 1st filter ****');
      except;
      end;
{$ELSE}
{$ENDIF}
    except
      AE_PublicDiscount := 0;
      AE_PublicDollarAmt := 0;
      AE_PublicCAP := 0;
      AE_PublicPromoRec.fDiscDollarAmt := 0;
      AE_PublicPromoRec.fDiscPercent := 0;
    end;

    if sCARNAMEIDX <> -1 then
    try
      mem.Last;

      if (tf_disc_carnameidx.AsInteger > -1) and
        (tf_discpercent.AsFloat > AE_PublicDiscount) then
      begin
        try
          AE_PublicDiscount := tf_discpercent.AsFloat;
          AE_PublicDollarAmt := tf_disc_dollar_amt.AsFloat;
          AE_PublicCAP := tf_disc_cap.AsFloat;
          AE_PublicPromoRec := GetPromo();
          AE_Public_AffPushPull := getAffPushPull();

{$IFDEF USE_DEBUG}
          try
            temp := '';
            temp := ' [CAP = TRUE or USER = "*"] found car name --> ID: ' + tf_ECD_ID.AsString + ' --> AE_PublicDiscount = ' + tf_discpercent.AsString +
              ' AE_PublicCAP = ' + tf_disc_cap.AsString + ' AE_PublicPromo: ' + FloatToStr(AE_PublicPromoRec.fDiscPercent);
            SAdd(_debugStr, temp);
            formtestdiscs.memo2.lines.Add(temp);
            if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** if discuser = "*" or UseCap - 1ast filter - car name idx > -1 found ****');
          except;
          end;
{$ELSE}
{$ENDIF}
        except
          AE_PublicDiscount := 0;
          AE_PublicDollarAmt := 0;
          AE_PublicCAP := 0;
          AE_PublicPromoRec.fDiscDollarAmt := 0;
          AE_PublicPromoRec.fDiscPercent := 0;
        end;
      end;
    except;
    end;

    if not (foundOverRides) then
    begin // if no overrides for the CHAOS discount are found for public user *
      AE_PublicDiscount := chaosDiscount;
      AE_PublicDollarAmt := chaosDollarAmt;
      // chaos_promo -- add promo carry thru 1/22/2008
      // if we have not found any discounts at all for * then assign the CHAOS discount and promo to *
      AE_PublicPromoRec.fDiscDollarAmt := chaos_PromoRec.fDiscDollarAmt;
      AE_PublicPromoRec.fDiscPercent := chaos_PromoRec.fDiscPercent;
      AE_Public_AffPushPull := CHAOS_AffPushPull; // add 04/08/2011

      CapValue := chaosCap;

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' [CAP = TRUE or USER = "*"] override NOT found pubdisc = chaos disc] -->: ' +
          ' foundOverrides AE_PublicDiscount [' +
          FLoatTOStr(AE_PublicDIscount) + '] Promo% [' + FloatToStr(AE_PublicPromoRec.fDiscPercent) + ']';
        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.Add(temp);
      except;
      end;
{$ELSE}
{$ENDIF}
    end
    else
    begin // must be over-ride - already set AE_PublicDiscount
      CapValue := AE_PublicCAP;

{$IFDEF USE_DEBUG}
      temp := '';
      temp := ' [CAP = TRUE or USER = "*" override FOUND Cap Value = [' + FloatToStr(capValue) + ']';
      SAdd(_debugStr, temp);
      formtestdiscs.Memo2.Lines.add(temp);
{$ELSE}
{$ENDIF}
    end;

    result.fDiscPercent := AE_PublicDiscount + AE_PublicPromorec.fDiscPercent + AE_Public_AffPushPull;
    result.fDiscDollarAmt := AE_PublicDollarAMT + AE_PublicPromorec.fDiscDollarAmt;
  end; //if (discountuser = '*') or (USECAP)

  // now check for a specific user passed in other than Public Web (*) or chaos
  if (discountuser <> '*') and (discountuser <> 'CHAOS') then //!dh
  begin // get private if requested
    try
      // dh change global vars for use by onFilter function
      // must set all.
      im_B_or_I := BorI;
      im_Sipp := Sipp;
      im_Op := Oper;
      im_DiscUser := discountuser;
      im_disc_carnameidx := sCARNAMEIDX;
      if Mem.filtered then Mem.Refresh else MEM.Filtered := true;
    except on e: exception do
      begin
        errstr := e.Message + ' can''t set mem.filter->';
        exit;
      end;
    end;
{$IFDEF USE_DEBUG}
    temp := '';
    temp := ' --> 15 : entered over-rides [discountuser <> "*" OR "CHAOS"] ';
    formtestdiscs.Memo2.Lines.add(temp);
    SAdd(_debugStr, temp);
{$ELSE}
{$ENDIF}

    userDiscount := 0;
    userDollarAmt := 0;
    // removed for loop 8/14/2007 "Append" type obsoleted.
    im_type := 'O';
    try if Mem.filtered then Mem.Refresh else MEM.Filtered := true; except; end;

    mem.First;
    foundOverRides := not (mem.IsEmpty);

    try
      userDiscount := tf_discpercent.AsFloat;
      userDollarAmt := tf_disc_dollar_amt.AsFloat;
      userCap := tf_disc_cap.AsFloat;
      user_promoREC := GetPromo();
      user_AffPushPull := getAffPushPull();

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' ---> 17 [over-rides -> discountuser <> "*" OR "CHAOS"]: userDiscount : ' + tf_discpercent.Asstring +
          ' userCap : ' + tf_disc_cap.AsString +
          ' ID : [' + tf_ECD_ID.AsString + '] user_promo [' + floattostr(user_promorec.fDiscPercent) + ']';
        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.add(temp);
        if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** over-rides -> 1st filter for discountuser <> "*" OR "CHAOS" ****');
      except;
      end;
{$ELSE}
{$ENDIF}
    except
      userDiscount := 0;
      userDollarAmt := 0;
      userCap := 0;
      user_promoRec.fDiscDollarAmt := 0;
      user_promoRec.fDiscPercent := 0;
    end;

    if sCARNAMEIDX <> -1 then
    try
      mem.Last;
      if (tf_disc_carnameidx.AsInteger > -1) and (tf_discpercent.AsFloat > userDiscount) then
      begin
        try
          userDiscount := tf_discpercent.AsFloat;
          userDollarAmt := tf_disc_dollar_amt.AsFloat;
          userCap := tf_disc_cap.AsFloat;
          user_promoREC := GetPromo();
          user_AffPushPull := getAffPushPull();

{$IFDEF USE_DEBUG}
          temp := '';
          temp := ' ---> 19 : over-rides with carname found. userDiscount : ' +
            tf_discpercent.Asstring + ' userCap : ' + tf_disc_cap.AsString + ' ID : ' + tf_ECD_ID.AsString;
          SAdd(_debugStr, temp);
          formtestdiscs.Memo2.Lines.Add(temp);
          if formtestdiscs.cbDebug.checked then formtestdiscs.Memo2.Lines.Add('**** over-rides -> "Last" nameidx <> -1 filter for discountuser <> "*" OR "CHAOS" ****');
{$ELSE}
{$ENDIF}
        except
          userDiscount := 0;
          userDollarAmt := 0;
          userCap := 0;
          user_promoRec.fDiscDollarAmt := 0;
          user_promoRec.fDiscPercent := 0;
        end;
      end;
    except;
    end;

    if not (foundOverRides) then
    begin // must be append - never happens because there are no appends
      userDiscount := chaosDiscount;
      userDollarAmt := ChaosDollarAmt;
      // add chaos promo here too: must carry it thru if no other discounts are found
      user_PromoRec.fDiscDollarAmt := chaos_PromoRec.fDiscDollarAmt;
      user_PromoRec.fDiscPercent := chaos_PromoRec.fDiscPercent;
      user_AffPushPull := CHAOS_AffPushPull; // add 04/08/2011

      userCap := chaosCap;

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' 99 over-rides not found in discountuser <> "*" OR "CHAOS" userdiscs set to chaos disc. userdisc[' + FloatTOStr(userDiscount) + ' user_Promo [' +
          FLoattoStr(user_PromoRec.fDiscPercent);
        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.Add(temp);
      except;
      end;
{$ELSE}
{$ENDIF}
    end;

    if not USECAP then
    begin
       // change 04/08/2011
      //result.fDiscPercent := userDiscount + user_promoRec.fDiscPercent;
      result.fDiscPercent := userDiscount + user_promoRec.fDiscPercent + user_AffPushPull;
      result.fDiscDollarAmt := userDollarAmt + user_promoRec.fDiscDollarAmt;
      CapValue := userCap;

{$IFDEF USE_DEBUG}
      try
        temp := '';
        temp := ' 199 [discountuser <> "*" OR "CHAOS"] NOT useCap, so add user and promo : ID : ' + tf_ECD_ID.AsString +
          ' result% : ' + FloatTOStr(result.fDiscPercent) + ' $: ' + FLoatTOStr(result.fDiscDollarAmt);

        SAdd(_debugStr, temp);
        formtestdiscs.Memo2.Lines.Add(temp);
      except;
      end;
{$ELSE}
{$ENDIF}

    end
    else // are Using CAP
    begin
      // change 04/08/2011
      // if userDiscount + user_promoRec.fDiscPercent > AE_PublicDiscount + AE_PublicPromoRec.fDiscPercent then // 11/01/2006 changed from if userDiscount > AE_PublicDiscount then

      if (userDiscount + user_promoRec.fDiscPercent + user_AffPushPull >
        AE_PublicDiscount + AE_PublicPromoRec.fDiscPercent + AE_Public_AffPushPull) then
      begin
        // change 04/08/2011
        // result.fDiscPercent := AE_PublicDiscount + AE_PublicPromoRec.fDiscPercent;
        result.fDiscPercent := AE_PublicDiscount + AE_PublicPromoRec.fDiscPercent + AE_Public_AffPushPull;
        result.fDiscDollarAmt := AE_PublicDollarAmt + AE_PublicPromoRec.fDiscDollarAmt;
        capValue := AE_PublicCAP;

{$IFDEF USE_DEBUG}
        try
          temp := '';
          temp := ' [discountuser <> "*" OR "CHAOS"] 1999 USECAP=true (userDisc > AE_PubLicDisc) so give AE PUBLIC DISC - ID: ' +
            tf_ECD_ID.AsString + ' result% : ' + FloatTOStr(result.fDiscPercent) + ' $: ' + FLoatTOStr(result.fDiscDollarAmt);
          SAdd(_debugStr, temp);
          formtestdiscs.Memo2.Lines.add(temp);
        except;
        end;
{$ELSE}
{$ENDIF}
      end
      else
      begin
        // change 04/08/2011
        // result.fDiscPercent := userDiscount + user_promoRec.fDiscPercent;
        result.fDiscPercent := userDiscount + user_promoRec.fDiscPercent + user_AffPushPull;
        result.fDiscDollarAmt := userDollarAmt + user_PromoRec.fDiscDollarAmt;
        CapValue := UserCap;

{$IFDEF USE_DEBUG}
        try
          temp := '';
          temp := ' ---> 2000 USECAP=TRUE but userDisc is less than AE_PublicDisc, so give user disc. ID [' + tf_ECD_ID.AsString +
            '] result% [' + FloatTOStr(result.fDiscPercent) + '] $[' + FLoatTOStr(result.fDiscDollarAmt) + ']';
          SAdd(_debugStr, temp);
          formtestdiscs.Memo2.Lines.add(temp);
        except;
        end;
{$ELSE}
{$ENDIF}
      end;
    end;
  end;

  if result.fDiscDollarAmt < 0 then result.fDiscDollarAmt := 0;
  if result.fDiscPercent < 0 then result.fDiscPercent := 0;


{$IFDEF USE_DEBUG}
  // slog('\\itfs\chaoslog\', 'dplus_disc', '.txt', _debugStr);
  formtestdiscs.Memo2.Lines.Add('debug String: ');
  formtestdiscs.Memo2.Lines.Add(_debugStr);
  formtestdiscs.Memo2.Lines.Add('debug String END ************ ');
{$ELSE}
{$ENDIF}
end;

end.

