program MarketingReps;

uses
  Forms,
  main_mkt in 'main_mkt.pas' {frmMainMkt},
  masterListDTP in 'masterListDTP.pas' {FormListDates},
  aeDssHelp in 'aeDssHelp.pas' {frmDSSHelp},
  UnitAbout in 'UnitAbout.pas' {AboutBox},
  aecommonfunc_II in 'aecommonfunc_II.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Marketing DSS';
  Application.CreateForm(TfrmMainMkt, frmMainMkt);
  Application.CreateForm(TFormListDates, FormListDates);
  Application.CreateForm(TfrmDSSHelp, frmDSSHelp);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
