Select 
vs_basesys system,
vs_operator_code operator,
vs_pu_ctry country,
vs_sipp sipp,
sum(vs_comm_tot) comm,
sum(vs_comm_due) comm_due,
sum(vs_wholesale_total) whl_total,
sum(vs_retail_total) retail_total,
sum(vs_profit) profit,
sum(vs_gross_revenue) gross_rev,
sum(vs_count_voucher) voucher_count

From g_voucher_summary

where vs_basesys > '' and
vs_operator_code = 'ALM' and
vs_paid_dt between date('01/01/2008') and date('01/07/2008')

group by 1,2,3,4
order by 1,2,3,4

