unit AEFireDACHelper;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Data.DB, Vcl.StdCtrls, System.Actions, Vcl.ActnList,

  uTPLb_CryptographicLibrary, uTPLb_BaseNonVisualComponent, uTPLb_Codec,
  uTPLb_Constants, uTPLb_AES, uTPLb_StreamUtils,

  FireDAC.UI.Intf, FireDAC.VCLUI.Error,
  FireDAC.Stan.Error, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Phys.ODBC, FireDAC.VCLUI.Wait, FireDAC.Phys.ODBCBase,
  FireDAC.Comp.UI, FireDAC.Comp.Client, FireDAC.Comp.DataSet;


procedure Get_ListFromPipeString(var theList: TStringList; InPipeStr: string);
procedure Set_FireDAC_Connection(conn: TFDConnection; ALIAS: string; useDev: Boolean = True);
function  GetSupportingFiles(Source, Target: string): boolean;
procedure GetInMemList(var InList: TStringList;
  fullPath: string = 'C:\AE\FireDAC.enc');

implementation

function GetSupportingFiles(Source, Target: string): boolean;
var // refresh your files i.e. GetSupportingFiles('\\fileserver\UPDATE\yourFile.ini', 'c:\ae\YourFile.ini');
  SourcePath, DestinationPath: string;
  SourceInt, DestinationInt: integer;
  SourceDate, DestinationDate: TDateTime;
begin
  if (trim(Source) = '') or (trim(Target) = '') then
  begin
    result := false;
    exit;
  end
  else result := true;

  SourcePath := Source;
  SourceInt := FileAge(SourcePath);

  if (SourceInt = -1) then
  begin
    result := false;
    exit;
  end
  else
    SourceDate := FileDateToDateTime(SourceInt);

  DestinationPath := Target;
  DestinationInt := FileAge(DestinationPath);

  if (DestinationInt = -1) then DestinationDate := 0.00 // equal to 12/31/1899
  else DestinationDate := FileDateToDateTime(DestinationInt);

  if (SourceDate > DestinationDate) then
  begin
    if (CopyFile(PChar(SourcePath), PChar(DestinationPath), false) <> true) then
    begin
      result := false;
    end;
  end;
end;


procedure GetInMemList(var InList: TStringList;
  fullPath: string = 'C:\AE\FireDAC.enc');

function GetList(InFileName: string): string;
var MyList: TStringList;
begin
  MyList := nil;
  try
    try
      MyList := TStringList.Create;
      MyList.Clear;

      if not fileexists(InFileName) then
      begin
        raise(exception.Create('GetList err --> No file found by the name of "' + InFileName + '"'));
      end
      else
      begin
        MyList.LoadFromFile(InFileName);
      end;
      result := MyList.Text;
    except on e: exception do
      begin
        raise(exception.Create('GetList err --> ' + e.Message));
      end;
    end;
  finally
    if assigned(MyList) then FreeAndNil(MyList);
  end;
end;


var
  Codec1                         : TCodec;
  CryptographicLibrary1          : TCryptographicLibrary;
  Plaintext, Ciphertext          : TMemoryStream;
begin

  CryptographicLibrary1          := TCryptographicLibrary.Create( nil);
  Codec1                         := TCodec.Create( nil);
  Codec1.CryptoLibrary           := CryptographicLibrary1;
  Codec1.AsymetricKeySizeInBits  := 1024;
  Codec1.StreamCipherId          := uTPLb_Constants.BlockCipher_ProgId;
  Codec1.BlockCipherId           := 'native.AES-256';
  Codec1.ChainModeId             := uTPLb_Constants.CBC_ProgId;
  Codec1.Password                := 'q{3lsdjfiuga34r';

  GetSupportingFiles('\\fileserver\update\FireDac\FireDAC.enc', 'c:\AE\FireDAC.enc');
  try
    Plaintext                    := TMemoryStream.Create;
    Plaintext.Clear;
    Ciphertext                   := TMemoryStream.Create;
    Ciphertext.LoadFromFile('C:\AE\firedac.enc');
    try
      Codec1.DecryptStream(Plaintext, Ciphertext);                              //decrypt Stream
      Plaintext.Position            := 0;
      inlist.LoadFromStream(Plaintext);
    except on e: exception do
      begin
        raise(exception.Create('GetInMemIDACList Error --> ' + e.message));
      end;
    end;
  finally
    try
      Plaintext.Free;
      Ciphertext.Free;
      CryptographicLibrary1.Free;
      Codec1.Free;
    except
    end;
  end;
end;

procedure Set_FireDAC_Connection(conn: TFDConnection; ALIAS: string; useDev: Boolean = True);
var // encrypted
  sWhereTOGo: string;
  idaclist, paramlist: TStringList;
  i: integer;
  TempU, TempP: String;
begin
  if not (assigned(conn)) then
    Begin
      raise(exception.Create('Set_FireDAC_Connection --> No Connection assigned'));
      exit;
    End;


  If (trim(ALIAS) = '') then
    begin
      raise(exception.Create('Set_FireDAC_Connection --> No ALIAS assigned'));
      exit;
    end;

  If ALIAS = 'US' then
    ALIAS := 'USA';

  sWhereToGo := UpperCase(trim(ALIAS));
  IDACList := nil;
  paramList := nil;

  try
    IDACList := TStringList.Create;
    paramList := TStringList.Create;

    try
      paramlist.Clear;
      IDACList.Clear;

      // new code
      GetInMemList(IDACList); // retreive & decrypt  a file
     //  showmessage(idaclist.Text);
      conn.Close;
      conn.Params.Clear;

      if (IDACList.Values[sWhereToGo + '_INFORMIXSERVER'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32HOST'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32USER'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32PASS'] = '') or
        (IDACList.Values[sWhereToGo + '_DATABASE'] = '') then
      begin
        // could not find values[] in the ini file
        raise(exception.create('Set_Connection-->invalid ALIAS: ' + ALIAS));
        exit;
      end
      else
      begin
        paramList.Clear;

        paramlist.Add('User_Name=' + IDACList.Values[sWhereToGo + '_WIN32USER']);
        paramlist.Add('Password=' + IDACList.Values[sWhereToGo + '_WIN32PASS']);
//        paramlist.Add('ODBCDriver=IBM INFORMIX ODBC DRIVER');
        paramlist.Add('Database=' + IDACList.Values[sWhereToGo + '_DATABASE']);
        If useDev then
          paramlist.Add('ODBCAdvanced=HOST=' + IDACList.Values[sWhereToGo + '_WIN32HOST'] + ';PRO=' + IDACList.Values[sWhereToGo + '_WIN32PROTOCOL']+ ';SERV=sqlexec;SRVR=' + IDACList.Values[sWhereToGo + '_INFORMIXSERVER']+ ';cloc=en_US.819;dloc=en_US.819')
        Else
          paramlist.Add('ODBCAdvanced=HOST=' + IDACList.Values[sWhereToGo + '_WIN32HOST'] + ';PRO=onsoctcp;SERV=sqlexec;SRVR=' + IDACList.Values[sWhereToGo + '_INFORMIXSERVER']+ ';cloc=en_US.819;dloc=en_US.819');
        paramlist.Add('DriverID=INFX');
        Paramlist.Add('metaDefCatalog=*');
        Paramlist.Add('metaDefSchema=*');
        Paramlist.Add('MonitorBy=Custom');

        for i := 0 to paramlist.Count - 1 do
          conn.Params.Add(paramlist[i]);
      end;


    except on e: exception do
      begin
        raise(exception.create('Set_FireDAC_Connection-->' + e.message));
      end;
    end;
  finally
    if assigned(IDACList) then
    try
      IDACList.Free;
    except;
    end;

    if assigned(ParamList) then
    try
      ParamList.Free;
    except;
    end;
  end;
end;

procedure Get_ListFromPipeString(var theList: TStringList; InPipeStr: string);

function A_SplitStrings(const str: string; const separator: string; Strings: TStrings): TStrings;
// Fills a string list with the parts of "str" separated by "separator".
var
  n: integer;
  p, q, s: PChar;
  item: string;
begin
  if Strings = nil then
  begin
    Result := nil;
    exit;
  end
  else Result := Strings;
  Result.Clear;

  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);

    repeat
      q := AnsiStrPos(p, s);
      if q = nil then q := AnsiStrScan(p, #0);
      SetString(item, p, q - p);
      Result.Add(item);
      p := q + n;
    until q^ = #0;

  except
    item := '';
    raise;
  end;
end;
var strings: TStrings;
begin // get a TStringList from a string structured like "adsf|asdf|asdf|asdf"

  { example: load a list of dbconnections into a combobox
  MyList := TStringList.Create;
  try
    try
      MyList.LoadFromFile('c:\ae\theFile.ini');
      temp := MyList.Values['pubdbs'];  // pubdbs=USA|CANA|KEM|ETC
      MyList.Clear;
      Get_ListFromPipeString(MyList, temp);

      if trim(MyList.Text) = '' then FillManually()
      else ComboReasonCode.Items := MyList;

    except on e: exception do
      begin
        FillManually();
        showmessage('x- ' + e.Message);
      end;
    end;
  finally
    MyList.free;
  end;
  }
  if (theList = nil) or (trim(InPipeStr) = '') then exit;
  thelist.Clear;
  try
    strings := A_SplitStrings(InPipeStr, '|', theList);
  except on e: exception do
    begin
      raise(exception.create('Get_ListFromPipeString --> ' + e.message));
    end;
  end;
end;

end.
