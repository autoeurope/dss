unit aecommonfunc_II;

{$WARNINGS OFF}
{$HINTS OFF}
{
20130501TYLER
Began merging AeCommonFunc_II, JMClasses, JRCommon, TylerCommon, and WinFunctions
JRCommon has a pretty good start on this, so it's really just going to be a merge
of AECommonFunc_II, JRCommon and WinFunctions
}


{
remove refrences  to dbtables (BDE) 01/09/2006
add encryption routines for idac.ini


note: idac.ini is now idac_c.ini.
      idac.ini will go away.

      Set_idac_connection will handle decrypting the idac_c.ini file

      ***** do not change the file idac_c.ini  *****


      change your uses from AECommonfunc to aecommonfunc_II
      and re-compile the app.

      this new procedure will return the decrypted idac_c.ini at runtime:

      procedure GetInMemIDACList(var InList: TStringList);

}
interface

uses
  //system
  Windows,
  SysUtils,
  Classes,
  Controls,
  StdCtrls,
  Forms,
  Graphics,
  DB, 
  activex,
  Registry,
  INIFiles,
  strUtils,
  DateUtils,
  ComCtrls,
  ShlObj,
  ShellAPI,
  WinSvc,
  SyncObjs,
  psApi,
  mshtml,

  //IDAC
  //IfxConnection,
  CheckLst, 
 // IfxQuery,
    FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, ppDesignLayer, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.VCLUI.Wait, FireDAC.Moni.Base, FireDAC.Moni.Custom, FireDAC.Comp.UI,FireDAC.Phys.ODBCBase,
  FireDAC.Phys.Infx, aeFireDacHelper;
  //AE stuff
// apicryptunit2; // supports new stuff feb 2009

{WinFunctions declarations begin here}
type
  TWinInfo = class
  public
    function GetComputerNetName(): string;
    function GetUserFromWindows(): string;
    function FolderSize(Dir: string): integer;
    function TCPIPInstalled: boolean;
    function ReplaceSubString(DestString, ReplaceString, NewValueString: string): string;
    function DeleteSpaces(strValue: string): string;
    function SecondsIdle: DWord;
    Function StringReverse(strValue: String): String;
    function SearchAndReplace(sSrc, sLookFor, sReplaceWith: string): string;
    function SecToTime(Sec: Integer): string;
    function StringToCaseSelect(Selector: string; CaseList: array of string): Integer;
  end;
{WinFunctions declarations end here}

type AEFormatSettings = TFormatSettings;


type
  TSecurity = class(TObject)
  private
    Fconstant1: WORD;
    Fconstant2: WORD;
  public
    constructor Create(const constant1, constant2: WORD);
    function Encrypt(const s: string; const StartKey: WORD): string;
    function Decrypt(const s: string; const StartKey: WORD): string;
  end;

{.Z+}
type
  LStrRec = record
    AllocSize: Longint;
    RefCount: Longint;
    Length: Longint;
  end;

type TDurationRec = record
    days: word; // Time past in:
    hours: word;
    minutes: word;
    seconds: word;
  end;

type
  jProdRec = record
    system,
      description,
      prod_type,
      prod_subtype,
      source_market: string;
  end;

type
  TStringArray = array of string;

  TMyEnum = (me_0, me_1, me_2);

const
  lameCWStr = '<center><FONT Size="1"><a href="http://www.lamepage.com/lame_page">� 2007 lamepage.com.</a><br><a href="jws.dll/agreement"> All rights reserved.</a></font></center>';
     // special date display strings
  NODATESTR = 'No-Date';
  BEGOFTIMESTR = 'Beg-o-time';
  ENDOFTIMESTR = 'End-o-time';
  Codes64 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/';
  StrOffset = SizeOf(LStrRec);


{Original AECommonFunc_II starts here}
//function Glob: string; // dh - get remote global string to inject into sql --> "aeglobal@ids_p650::"

function Glob: string; overload; // dh
function Glob(nstr: string): string; overload; // edc
function CrossDBPrefixBySys(InSys: string): string; // jm
function GetDBPrefix(db: string): string; // dh 12/06/2007
procedure GetAEFormatSettings(var theFormatSettings: AEFormatSettings);
function SetCLB(sender: TObject; DoSet: Boolean): Boolean;
//function IsValidSIPP(InSIPP: string; InGlobalConn: TFDCOnnection; var ErrStr: string): boolean;
//procedure GetValidSIPPCombos(var slSIPPList: TStringList; InGlobalConn: TFDCOnnection);
procedure Get_DBList(var theList: TStringList);
procedure Get_Public_DBList(var theList: TStringList);

// FUnction Get_FileExtension(InFileName: String): String;
//FUnction changeDataBase(InDB: TDataBase; InAlias, USERNAME, PASSWORD: string): boolean;

function GetSupportingFiles(Source, Target: string): boolean;
procedure BrickWall();
function RandomColor(): TColor;
function GetDBColor(InAliasORParam: string): TColor;
function GetList(InFileName: string): string; // load file & return entire contents as String
function GetDBTag(InAlias: string): string;
function LeftPadChar(const S: AnsiString; C: AnsiChar; Len: Cardinal): AnsiString;
function GetRentalDays(FromDate, ToDate: TDate; FromTime, ToTime: TTime): Real;
//function GetServiceType(conn: TFDCOnnection; res, rel: integer): string; // car, hotel, AIR etc
function GetServiceType2(SourceDB {CANA,KEM,USA}, VOPSUBT, VOPSTYPE1, VOPID: string): string;
//procedure CloseIDAC(conn: TFDCOnnection);
//procedure OpenIDAC(conn: TFDCOnnection);
//procedure ClearIDACParams(conn: TFDCOnnection);
function GetCurrentDB(conn: TFDConnection): string; // return the actual DB name on the server
function GetCurrentHost(conn: TFDCOnnection): string;
// FUnction GetCurrentIDAC_DB_ALIAS(conn: TFDCOnnection): String; // return a dbname a user will understand
//function GetCurrent_DB_PARAMSTR(conn: TFDCOnnection): string; // return paramSTR used
//function Get_Old_BDE_AliasFromIDAC(conn: TFDCOnnection): string; // return what Database.aliasName would have been with BDE
//function Get_Old_BDE_AliasFromParam(InParam: string): string;
function GetIDACQuery(conn: TFDCOnnection): TFDQuery; // create and return an new ifx query object

procedure OpenQuery(InQuery: TFDQuery); // sets screen.cursor
//function ExecuteQuery(InQuery: TFDQuery): int64;

//function GetFullDB(InDBParam: string): string; // obsolete
//function GetFullDB2(InDBPARAM: string): string; // jm 12/06/09 -- replaces getFullDB();

// add 'c:\ifxbin;' to front of ENV_VAR 'PATH' for current session
// use this as first call in the "program unit" just before "Application.Initialize" for the dbx stuff
function SetPathForCurrentSession(PathToAdd: string): Boolean;
function SetINFXDIRForCurrentSession(DirToSet: string): Boolean;
// get a csv file from a dataset as InList.Text
procedure GetCSV(DS: TDataSource; var InList: TStringList; delim: string = ',');
procedure SaveCSV(DS: TDataSource; sFileName, sHEADER: string; delim: string = ',');
procedure SaveCSV_multiFile(DS: TDataSource; sFileName, sHEADER: string; var errStr: string; delim: string = ',');
procedure SaveDataSetToHtmlFile(DS: TDataSource; InHeader, InDataName, sFILENAME: string; MaxFields: integer);


function GetMiddleDate(date1, date2: TDateTime): TDateTime;


procedure WriteLog(path, InStr: string);

// function Log_IfxSQL(InSQL, BeginOREnd: string; InConn: TFDCOnnection): Boolean; // removed 10/13/2010 see backup copy
//FUnction Log_BDESQL(InSQL, BeginOREnd: String; InConn: TDataBase): Boolean;
// hack for webmod : pass appName in since application does not exist in a webmodule
// function WebMod_Log_IfxSQL(InSQL, BeginOREnd, appName: string; InConn: TFDCOnnection): Boolean;  // removed 10/13/2010 see backup copy

function MaskCC(InCC: string): string;
procedure Get_ListFromPipeString(var theList: TStringList; InPipeStr: string);
function A_SplitStrings(const str: string; const separator: string; Strings: TStrings): TStrings;
procedure SAdd(var TargetString: string; StringToAdd: string);
function UName(): string;
function MName(): string;

// new 3/6/2008: get product type, source mkt, description from g_prod_code_Ref (global)
procedure get_prod_desc_type_subtype_source(basesys, subtype, stype1: string; InConn: TFDCOnnection; var theRec: jProdRec);

// new 3/10/2008 get opt outs entered in global system
//function Is_IATA_OptOut(InGlobalConn: TFDCOnnection; InIata: string): boolean;
//function IsEmailOptOut(InConnGlobal: TFDCOnnection; InEmailAddress: string): boolean;

// supports encrypted ini file:
//procedure Set_IDAC_Connection(conn: TFDCOnnection; ALIAS: string;
//  ConnectionType: string = ''; IsODBCIfxPath: boolean = false);

// get and decrypt idac_c.ini or other file into a TStringList
//procedure GetInMemList(var InList: TStringList;
//  fullPath: string = 'C:\AE\IDAC_c.ini');

procedure StringListToInStatement(var InOutList: TStringList);

function GetGlobalBySysParam(Sys: string): string;

function MATCHES_(AString, Pattern: string): boolean;


function GetServerFromParamStr(InStr: string): string; // jm 11/11/2009
function GetDatabaseFromParamStr(InStr: string): string; // jm 11/11/2009
function GetHOSTFromParamStr(InStr: string): string; // jm 11/11/2009
function Get_part_ADO_connectionStr(InSysParam: string): string;
// FUnction Get_ADO_ConnStr(alias: string): String;

function quote_string(s: string): string;
{Original AECommonFunc_II ends here}

{JMClasses start here}
{  STRING HELPER FUNCTIONS  }
function StripTags(var s: string): boolean;
function TextToBinary(InText: string): string;
function BinaryToText(InBinary: string): string;
function StripStr(InStr: string): string;
function StrToPassword(InStr: string): string;
function GetBetweenString(InStr, InDelim1, InDelim2: string): string;
function StripWhiteSpace(Instr: string): string;
function WhiteSpaceToUnderScores(InStr: string): string;
function StripPunct(InStr: string): string;
function StripCommas(InStr: string): string;
function StripOperators(InStr: string): string;
function StripDoubleQuotes(InStr: string): string;
function StripBinaryStr(InStr: string): string;
function PigLatin(engWord: string): string;
function IsPalendrome(InStr: string): boolean;
function GetTheUserName(): string;
function GetMachineName: string;
function GetGUIDString(): string;
function GetTimeStr(InTime: TDateTime): string;
function ValidGuid(strGuid: string): boolean; var intCount: integer;
function AsciiCountL(const S, WordDelims: AnsiString; Quote: AnsiChar): Cardinal;
function IsAllDigits(Instr: string): boolean;
function IsAtSign(InStr: string): boolean;

function qs(s: string): string;
   // replace any single or double quotes or ` chars in text with spaces
   // then wrap s in single quotes

procedure jShuffle(var InStrings: TStrings);

  {-Return the number of words in a string.}
function CharExistsL(const S: AnsiString; C: AnsiChar): Boolean;
  {-Determine whether a given character exists in a string. }
function ExtractWordL(N: Cardinal; const S, WordDelims: AnsiString): AnsiString;
  {-Given an array of word delimiters, return the N'th word in a string.}
function WordPositionL(N: Cardinal; const S, WordDelims: AnsiString; var Pos: Cardinal): Boolean;
  {-Given an array of word delimiters, set Pos to the start position of the
    N'th word in a string.  Result indicates success/failure.}
function LeftPadL(const S: AnsiString; Len: Cardinal): AnsiString;
  {-Pad a string on the left with spaces.}
function LeftPadChL(const S: AnsiString; C: AnsiChar; Len: Cardinal): AnsiString;
  {-Pad a string on the left with a specified character.}
function substrdelim(string_in, delim: string; stringindex: integer): string;
// return a substring at an index

{ CHARACTER HELPER FUNCTIONS }
function IsPunct(a: Char): boolean;
function IsAlpha(a: Char): boolean;
function IsSpace(a: Char): boolean;
function IsDigit(a: char): boolean;
function IsHex(a: Char): boolean;
function IsDot(CheckDot: char): Boolean;
function IsFloat(CheckStr: string): Boolean;
function Insult(): string;

{  Logging  }
function SaveLogFile(sFILENAME {usually Application.Title}, InString: string): Boolean;
// thread-safe savelogfile:
function SafeSaveLogFile(threadCSection: TCriticalSection; sFILENAME, InString: string): Boolean;

function j_GetFileType(Path: string): string;
function j_GetGenericFileType(Path: string): string;

function jSaveFile(sFULLPATH, sFILENAME, sSUFFIX, InString: string): Boolean;

// same as jsavefile except includes ByDate Naming in filename automatically eg 1 file per day  - after midnight = new file
function SaveLogFileX(sFULLPATH, sFILENAME, {always use Applciation.Title here} sSUFFIX, InString: string): Boolean;


function GetLogFileName(sFILENAME: string {usually Application.Title}): string;
function FlushOutOldLogFiles(): Boolean;
function FlushOutOldLogFilesX(sEXT: string): Boolean;
function GetFileListOLD(sEXT, sPATH: string): TStringList;
function GetFileList(sEXT, sPATH: string; MyList: TStringList): TStringList;
function SetIfNotCreateDirectory(): boolean; overload;
function SetIfNotCreateDirectory(sPATH: string): boolean; overload;
{ General }
function LockForm(Sender: TObject): boolean; // set form constraints maxValues = MinValues
function GetOSVersion(): string;
procedure GetBuildInfo(var V1, V2, V3, V4: Word);
function GetFileInfo(sFULLPATH: string): string;
function GetRegularBuildInfoString: string;
function Reboot_XP(DoWhat: Word): Boolean;
function ConcatDateTime(dINDATE: TDate; tINTIME: TTime): string;
function ConcatDateTime2(dINDATE: TDate; tINTIME: TTime): TDateTime;
function jGetError(): string;
function FormatAPIMessage(Error: DWORD): AnsiString;
function jShellEXE(InFullPath: string): string;
function IsRemoteSession: Boolean;
//FUnction PlaySound(InFullPath: String): boolean;

// files
function DeleteFilesOfTypeInDirectory(FullPath, FileExt: string): Boolean;
function DeleteFileTypeOfAge(sEXT, sDIRECTORY: string; iDAYSOLD: integer): Boolean;
//convert
function FahrenheitToCelcius(INREAL: real): real;
function CelciusToFahrenheit(INREAL: real): real;
function MilliliterToFluidOZ(MIL: real): real; stdcall;
function KiloToPound(Kilo: real): real; stdcall;
function IntToBinStr(Value: cardinal): string;
function CharToBinStr(InChar: CHAR): string;
function BinStrToChar(InBinStr: string): CHAR;
function BinStrToChar2(InBinStr: string): CHAR;

function OuncesPureAlc(NumDrinks, OZPerDrink, PERCENT: real): real;
function BACOverTime(IsMale: boolean; OzPureAlc, lbsWeight, hours: real): real;
function BloodAlcoholOverTime(Male: boolean; NumDrinks, OzPerDrink, percentAlc, weightLbs, Time: real): real; stdcall;
function HowDrunkIAm(BAC, Tolerance: real): shortstring; stdcall;

function SetDirectoryForTSUser(): string;
function GetDirectoryForTSUser(): string;

procedure GetMemoryStatus(var AvailPhys, TotalPhys, PercentUsed, TotalPageFile, pageFileFree, userBytesOfAddressSpace, freeUserBytes: string);

procedure RANDOM_BEEP();
procedure JBEEP(sBEEPNAME: string);
procedure nada();

// instantly format a web page w/ tables


// validate the format of an e-mail address
function IsValidEMailAddr(InAddr: string; var errStr: string): boolean;
function GetNamefromEmailAddr(InEmail: string): string;
function GetDomainFromEmailaddr(InEmail: string): string;


function IsInList(InList: TStringList; InStr: string): boolean;
function IsInStrings(InStrings: TStrings; InStr: string): boolean;
function JHash(s: string; ARRAY_SIZE: LongWord): LongWord; // hash a string into an array - 1
//function MinutesPast(TheDate: Tdate): integer;
function GetDuration(NewTime, OldTime: TDateTime): TDurationRec;
function DwordToDuration(InMillisecs: DWORD): TDurationRec;
function jGetFileSize(FullFilePath: string): LongInt;

procedure ServiceNames(sMachine: string; var InList: TStringList; const uType: integer = SERVICE_WIN32);
function ServiceStart(sMachine, sService: string; iSecondsToWait: integer): boolean;
function ServiceStop(sMachine, sService: string; iSecondsToWait: integer): boolean;
function ServiceStatus(sMachine, sService: string): string;

// more strings
function Occurs(const str: string; c: char): integer; overload; // Returns the number of times a character occurs in a string
function Occurs(const str: string; const substr: string): integer; overload; // Returns the number of times a substr occurs in a string
function AnsiOccurs(const str: string; const substr: string): integer; overload;
function Split(const str: string; const separator: string = ','): TStringArray; // Returns an array with the parts of "str" separated by "separator"
function AnsiSplit(const str: string; const separator: string = ','): TStringArray;
function SplitStrings(const str: string; const separator: string = ','; Strings: TStrings = nil): TStrings;
function AnsiSplitStrings(const str: string; const separator: string = ','; Strings: TStrings = nil): TStrings;
//procedure StrAdd(var TargetString: string; StringToAdd: string);
//procedure StrAddFront(var TargetString: string; StringToAdd: string);
procedure StrInsert(Substr: string; var Dest: string; Index: Integer);

function Get_FileExtension(InFileName: string): string;

function jGetList(InFileName: string): string;
function jGetSupportingFiles(Source, Target: string): boolean;
function jSetPathForCurrentSession(PathToAdd: string): Boolean;
procedure jSLEEP(InMilliseconds: integer = 0);
function SecondsIdle(): DWord; // system level idle time
// fake a user logon and run somthing as them
function WinExecAsUser(FileName: string; username: string; password: string; Visibility: integer): string;
//function GetWindowsSysFolder: string;
function GetRegistryValue(RootKey: HKEY; KeyPath, KeyToRead: string): string;
function DeleteRegKeyValue(KeyPath, keyValue: string): boolean;

function SetRegistryValue(RootKey: HKEY; KeyPath, KeyToSet: string; ValueToSet: string = ''): string;
function IPMatchFromList(var IPList: TStringList; IP: string): Boolean;
function IPMatchMask(IP, Mask: string): Boolean;
function NumStripStr(InStr: string): string;
function BrowseForFolder(Handle: HWND {form1.handle}): string;
{JMClasses declarations end here}

{JRCommon declarations start here}
function IsIntegerOrBackspace(key: char): boolean;
  //ListView
procedure ListViewSearch(LstView: TListView; strSearchText: string);
//tree view
function GetNodeByText(ATree: TTreeView; AValue: string; AVisible: Boolean): TTreeNode; overload;
function GetNodeByText(ATree: TTreeView; InNode: TTreeNode;
  AValue: string; AVisible: boolean): TTreeNode; overload;
procedure UpOneLevel(ATree: TTreeView);
function IsTreeviewFullyExpanded(ATree: TTreeview): Boolean;
function IsTreeviewFullyCollapsed(ATree: TTreeview): Boolean;
function IsDuplicateName(Node: TTreeNode; NewName: string;
  Inclusive: boolean): boolean;
function DeleteNodeByName(ATree: TTreeview; strText: string): string;
//string
function GetBetween(InStr, InDelim1, InDelim2: string): string;
function ReplaceSubString(DestString, ReplaceString, NewValueString: string): string;
function SearchAndReplace(sSrc, sLookFor, sReplaceWith: string): string;
function DeleteLineBreaks(const S: string): string;
//Date Time
function GetDaysDateTwoPeriods(StartDate, EndDate: TDateTime): TStringArray;
function GetlastMonth(const D: TDateTime): integer;
function GetSundayWeek(const D: TDateTime): TDateTime;
function GetSunday(const D: TDateTime): TDateTime;
function ExtractYear(const D: TDateTime): word;
function ExtractLastYear(const D: TDateTime): word;
function EndOfTime: TDateTime;
function BegOfTime: TDateTime;
function NotAValidDate: TDatetime;
function NiceSmallDate(const D: TDateTime): string;
function MinutesPast(TheDate: Tdate): integer;
function GetDurationStringMin(NewTime, OldTime: TDateTime): string;
//function GetTimeZoneDelta: TDateTime;
//system
function GetComputerNetName(): string;
function GetUserFromWindows: string;
function SetDirectoryForAEUser(): string; overload;
function SetDirectoryForAEUser(const strUser: string): string; overload;
function GetExeByExtension(sExt: string): string;
{check registry for launching app}
procedure ShellByEXEFileName(sEXE, sFileName: string); overload;
procedure ShellByEXEFileName(WinHand: HWND; sEXE, sFileName: string); overload;

//ini
procedure WriteINIValueInteger(strPath, strSection, strKey: string; intValue: integer);
function FetchINIValue(strPath, strSection, strKey: string): string;
//Email
function IsValidEmail(const Value: string): boolean;
//stream
function ComponentToString(Component: TComponent): string;
function StringToComponent(Value: string): TComponent;

//Logging
function SaveToLogFile(sFILENAME, InString: string): Boolean;

//base64
function Encode64(S: string): string;
function Decode64(S: string): string;
////
function ShortStrToMonth(strMonth: string): integer;

function IncOffsetCommon(TheDateTime: TDateTime; MaineOffset: extended): TDateTime;
//valid cc
function Vc(C: string): Integer;
function GetBetweenStringJ(InStr, InDelim1, InDelim2: string): string;

//stuff from Tyler

function SurroundString(Delimiter, Input: string): string;
function Iif(Condition: boolean; IfTrue, IfFalse: Variant): Variant;
procedure GetAllBetweenString(InStr, InDelim1, InDelim2: string; Output: TStringList);
function Quoted(Input: string): string;
function Tagged(Open, Input, Close: string): string;
function BuildSFQString(TableName: string): string;
procedure InitValue(Variable, Value: variant);
function SmartUTF8Decode(Input: string): widestring;
function NextPos(Start: Integer; Needle, Haystack: string): Integer;
function GetMemoryUsage: LongInt;
function IsOdd(Input: Integer): Boolean;
function IsValidEMailAddress(InAddr: string; var errStr: string): boolean;
function TimeBetween(StartDateTime, EndDateTime: TDateTime): string;

///twebbrowser
//List Web Form Names
function WebFormNames(const document: IHTMLDocument2): TStringList;
//get the instance of a web form by index 
function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;

{JRCommon declarations end here}

implementation

{Original AECommonFunc_II implementation starts here}
function quote_string(s: string): string;
var p: integer;
   // replace any single or double quotes or ` chars in text with spaces
   // then wrap s in single quotes
begin
  if (length(trim(s)) = 0) then
    result := 'NULL'
  else
  begin
    // #39 = ' (singleQuote)
    // #34 = " (doubleQuote)
    // #96 = ` (backSlant SingleQuote)
    p := pos(#39, s);
    while (p > 0) do
    begin // ' replace singleQuotes with spaces
      s[p] := ' ';
      p := pos(#39, s);
    end;

    p := pos(#34, s);
    while (p > 0) do
    begin // " replace doubleQuotes with spaces
      s[p] := ' ';
      p := pos(#34, s);
    end;

    p := pos(#96, s);
    while (p > 0) do
    begin // ` replace backslanted singlQuotes with spaces
      s[p] := ' ';
      p := pos(#96, s);
    end;
    result := #39 + s + #39;
  end;
end;

procedure StringListToInStatement(var InOutList: TStringList);
var workingList: TStringList; // returns  ' IN("item1", "item2", "item3") '
  i, x: Integer; // assuming list contains 3 lines with items: item1 item2 item3
begin
  if InOutList = nil then exit
  else if trim(InOutList.Text) = '' then exit;

  try
    workingList := TStringList.Create;
    try
      workingList.Clear;
      x := InOutList.Count - 1;

      for i := 0 to x do
      begin
        if (i = x) and (i = 0) then // only 1 item in list - beginning and end
          workingList.add('IN ( "' + InOutList[i] + '")')
        else if (i = 0) then // begining
          workingList.add('IN ( "' + InOutList[i] + '",')
        else if (i = x) then // end
          workingList.Add('"' + InOutList[i] + '")')
        else // middle
          workingList.Add('"' + InOutList[i] + '",');
      end; // for
      InOutList.Text := workingList.Text;
    except on e: exception do
      begin
        raise(Exception.Create('StringListToInStatement error -> ' + e.Message));
      end;
    end;
  finally
    if assigned(WorkingList) then FreeAndNIl(WorkingList);
  end;
end;

function GetMiddleDate(date1, date2: TDateTime): TDateTime;
var centerdate: TDateTime;
  asdf: Real;
begin
  result := 0;
  try
    asdf := date2 - date1;
    asdf := asdf / 2;
    result := date1 + asdf;
  except
    result := 0;
  end;
end;

function Get_part_ADO_connectionStr(InSysParam: string): string;
var server, host, db: string;
begin
    // what ed wants:  'DATABASE=reserve;HOST=f50dev;SRVR=online_f50;
  server := '';
  host := '';
  db := '';
  result := '';
  InSysParam := trim(uppercase(InSysparam));
  if InSysParam = '' then exit;

  server := GetServerFromParamStr(InSysParam);
  host := GetHOSTFromParamStr(InSysParam);
  db := GetDatabaseFromParamStr(InSysParam);

  if (server = '') or (host = '') or (db = '') then exit
  else result := 'DATABASE=' + db + ';HOST=' + host + ';SRVR=' + server + ';';
end;

function GetDatabaseFromParamStr(InStr: string): string; // jm 11/11/2009
var IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
    // IDACList.Text := GetList('c:\ae\IDAC.ini');
    //GetInMemList(IDACList);
    result := IDACList.Values[instr + '_DATABASE'];
  finally
    IDACList.Free;
  end;
end;

function GetHOSTFromParamStr(InStr: string): string; // jm 11/11/2009
var IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
  //  GetInMemList(IDACList);
    result := IDACList.Values[instr + '_WIN32HOST'];
  finally
    IDACList.Free;
  end;
end;

function GetServerFromParamStr(InStr: string): string; // jm 11/11/2009
var IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
    //GetInMemList(IDACList);
    result := IDACList.Values[instr + '_INFORMIXSERVER'];
  finally
    IDACList.Free;
  end;
end;

function MATCHES_(AString, Pattern: string): boolean;
var
  j, n, n1, n2: integer;
  p1, p2: pchar;
begin
  AString := UpperCase(AString);
  Pattern := UpperCase(Pattern);
  n1 := Length(AString);
  n2 := Length(Pattern);

  if n1 < n2 then n := n1
  else n := n2;

  p1 := pchar(AString);
  p2 := pchar(Pattern);

  for j := 1 to n do
  begin
    if p2^ = '*' then
    begin
      result := true;
      exit;
    end;

    if (p2^ <> '?') and (p2^ <> p1^) then
    begin
      result := false;
      exit;
    end;

    inc(p1);
    inc(p2);
  end;

  if n1 > n2 then
  begin
    Result := False;
    exit;
  end
  else if n1 < n2 then
  begin
    for j := n1 + 1 to n2 do
    begin
      if not (p2^ in ['*', '?']) then
      begin
        result := false;
        exit;
      end;
      inc(p2);
    end;
  end;
  Result := True;
end;


function GetGlobalBySysParam(Sys: string): string;
var { get the replicated global DB location on the corresponding system machine
      pass in the param used for the system - i.e. "USA" , "KEM", HOHO" etc

      return value examples:
      aeglobal@ids_p570c::
      aeglobal@ids_p570e::

      Note: if one can assume there is an "aeglobal" within the same instance, all you have to do is use
      "aeglobal::" . NO "@INFORMIXSERVER" is required. Since INFX looks at the sqlhosts file to get the location,
      you can assume performance will be the same for both, assuming the location you requested is local,
      and INFX resolves it as local
     }
  IDACList: TStringList;
begin
  result := '';
  try
    IDACList := TStringList.Create;
    try
      //GetInMemList(IDACList);

      if trim(IDACList.Values[trim(Uppercase(Sys)) + '_INFORMIXSERVER']) = '' then exit // not found in idac_c.ini
      else result := format('%s@%s::', ['aeglobal',
          IDACList.Values[trim(Uppercase(Sys)) + '_INFORMIXSERVER']]);
    except;
    end;
  finally
    if assigned(IDACList) then FreeAndNil(IDACList);
  end;
end;

{procedure GetInMemList(var InList: TStringList; fullPath: string = 'C:\AE\IDAC_c.ini');
var
  templist: TStringList;
  i: Integer;
 // crec: cryptrec;
begin
  try
    templist := tstringlist.create;
    try
      templist.Clear;
      // templist.text := GetList('\\fileserver\update\ini\IDAC_c.ini');
      templist.text := GetList(fullPath);

      if trim(tempList.Text) = '' then
      begin
        raise(exception.Create('GetInMemIDACList --> failed to retreive idac list'));
        exit;
      end;

      InList.Clear;
      for i := 0 to templist.Count - 1 do
      begin
    //    crec.cryptstr := '';
        crec.version := 0;

        if trim(templist[i]) = '' then
          InList.Add('')
        else
        begin
          // InList.Add(API_Decrypt(true, templist[i]));  // old way
          crec := API_Decrypt(true, templist[i]);
          InList.Add(crec.cryptstr);
        end;
      end;
    except on e: exception do
      begin
        raise(exception.Create('GetInMemIDACList Error --> ' + e.message));
      end;
    end;
  finally
    if assigned(templist) then FreeAndNil(tempList);
  end;
end;  }

{procedure Set_IDAC_Connection(conn: TFDCOnnection; ALIAS: string;
  ConnectionType: string = ''; IsODBCIfxPath: boolean = false);
var // encrypted
  sWhereTOGo, TempU, TempP: string;
  idaclist, paramlist: TStringList;
  i: integer;
begin
   { // possible locale issue between ver 9.4 an 11.x

     // fix: After you call set_idac_connection(param, param);
     // plug the locale

     // for v 11.x, USE:
     conn.Params.Add('CLIENT_LOCALE=en_US.819');
     conn.Params.Add('DB_LOCALE=en_US.819');

     // ver 9.x
     CLIENT_LOCALE=en_US.CP1252
     DB_LOCALE=en_US.CP1252


  if not (assigned(conn)) or (trim(ALIAS) = '') then
  begin
    raise(exception.Create('Set_IDAC_Connection_II --> params'));
    exit;
  end;

  sWhereToGo := UpperCase(trim(ALIAS));
  IDACList := nil;
  paramList := nil;

  try
    IDACList := TStringList.Create;
    paramList := TStringList.Create;

    try
      paramlist.Clear;
      IDACList.Clear;
      // new code
      GetInMemList(IDACList); // retreive & decrypt  a file
     //  showmessage(idaclist.Text);

      CloseIDAC(conn);
      ClearIDACParams(conn);

      if IsODBCIfxPath then
      begin
        // this should not be needed unless there are other data objects such as odbc
        // on the same server that may set the default local - when
        // brian was running air shit on the dev server, without the next two lines
        // my server was returning "unable to set local categories"
        // I have not had problems with the INFORMIX DIR yet

        // conn.Params.Add('INFORMIXDIR=' + IDACList.Values['SDK_INFORMIXDIR']);
        conn.Params.Add('CLIENT_LOCALE=' + IDACList.Values['SDK_CLIENT_LOCALE']);
        conn.Params.Add('DB_LOCALE=' + IDACList.Values['SDK_DB_LOCALE']);
      end
      else
      begin
        // conn.Params.Add('INFORMIXDIR=' + IDACList.Values['IFX_INFORMIXDIR']);
      end;   }

      // common
   {   conn.Params.Add('WIN32PROTOCOL=' + IDACList.Values['WIN32PROTOCOL']);
      conn.Params.Add('WIN32SERVICE=' + IDACList.Values['WIN32SERVICE']);

      if (IDACList.Values[sWhereToGo + '_INFORMIXSERVER'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32HOST'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32USER'] = '') or
        (IDACList.Values[sWhereToGo + '_WIN32PASS'] = '') or
        (IDACList.Values[sWhereToGo + '_DATABASE'] = '') then
      begin
        // could not find values[] in the ini file
        raise(exception.create('Set_IDAC_Connection-->invalid ALIAS: ' + ALIAS));
        exit;
      end
      else
      begin
        paramList.Clear;
        paramlist.Add('INFORMIXSERVER=' + IDACList.Values[sWhereToGo + '_INFORMIXSERVER']);
        paramlist.Add('WIN32HOST=' + IDACList.Values[sWhereToGo + '_WIN32HOST']);
        paramlist.Add('WIN32USER=' + IDACList.Values[sWhereToGo + '_WIN32USER']);
        paramlist.Add('WIN32PASS=' + IDACList.Values[sWhereToGo + '_WIN32PASS']);
        paramlist.Add('DATABASE=' + IDACList.Values[sWhereToGo + '_DATABASE']);

        for i := 0 to paramlist.Count - 1 do
          conn.Params.Add(paramlist[i]);
      end;

      ConnectionType := trim(uppercase(ConnectionType)); // this is essentially username
      // over-ride the default user / pass by category
      if ConnectionType = 'REPORTS' then
      begin
        conn.Params.Values['WIN32USER'] := IDACList.Values['REP_U'];
        conn.Params.Values['WIN32PASS'] := IDACList.Values['REP_P'];
      end
      else if ConnectionType = 'MAINTX' then
      begin
        conn.Params.Values['WIN32USER'] := IDACList.Values['MAINTX_U'];
        conn.Params.Values['WIN32PASS'] := IDACList.Values['MAINTX_P'];
      end
      else if (ConnectionType > '') then
      begin // catch all for future unames - make sure the name-value pair in idac_c is
             // ConnectionType+_U=uname and ConnectionType+_P=pass [like MAINTX above]
        //conn.Params.Values['WIN32USER'] := IDACList.Values[ConnectionType + '_U'];
        // conn.Params.Values['WIN32PASS'] := IDACList.Values[ConnectionType + '_P'];
        TempU := '';
        TempP := '';
        // only overwrite if foucn something ...
        TempU := IDACList.Values[ConnectionType + '_U'];
        TempP := IDACList.Values[ConnectionType + '_P'];
        if (Length(Trim(TempU)) > 0) and (Length(Trim(TempP)) > 0) then
        begin
          conn.Params.Values['WIN32USER'] := TempU;
          conn.Params.Values['WIN32PASS'] := TempP;    }
    //    end;
     //  showmessage(IDACList.Values[ConnectionType + '_U']);
     //  showmessage(conn.Params.Values['WIN32USER']);
     //  showmessage(conn.Params.Text);
   {   end;

      // add for dan 3/23/2009
      if (sWhereToGo = 'SAP') or (sWhereToGo = 'RECON') or (sWhereToGo = 'SAPDEV') then
      begin   }
        // causes slowness --> take out 1/14/2010
        //  conn.Params.Add('INFORMIXSQLHOSTS=' + IDACList.Values[sWhereToGo + '_INFORMIXSQLHOSTS']);
   //     conn.Params.Add('ONCONFIG=' + IDACList.Values[sWhereToGo + '_ONCONFIG']);
        // over write this since it's set above -- changed 1/19/2009
 {       conn.Params.Values['WIN32SERVICE'] := IDACList.Values[sWhereToGo + '_WIN32SERVICE'];
      end;

    except on e: exception do
      begin
        raise(exception.create('Set_IDAC_Connection-->' + e.message));
      end;
    end;
  finally
    if assigned(IDACList) then
    try
      IDACList.Free;
    except;
    end;

    if assigned(ParamList) then
    try
      ParamList.Free;
    except;
    end;
  end;
end;    }

function Glob: string; // dh
var // Get the table prefix for AEGlobal i.e. "aeglobal@ids_p650::"
    // deprecated but still here for backward compatibility.
    // use CrossDBPrefixBySys('GLOBAL') instead
  IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
    //GetInMemList(IDACList);

    result := format('%s@%s::', [IDACList.Values['GLOBAL_DATABASE'], IDACList.Values['GLOBAL_INFORMIXSERVER']]);
  finally
    IDACList.Free;
  end;
end;

function Glob(nstr: string): string;
var IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
   //IDACList.Text := GetList('c:\ae\IDAC.ini');
    //GetInMemList(IDACList);
    result := format('%s@%s', [IDACList.Values['GLOBAL_DATABASE'], IDACList.Values['GLOBAL_INFORMIXSERVER']]);
  finally
    IDACList.Free;
  end;
end;

function CrossDBPrefixBySys(InSys: string): string; // jm  4/29/2009
var // Get the table prefix for a system i.e. "aeglobal@ids_p650::"
    // pass in the param used for the system - i.e. "USA" , "KEM", HOHO" etc
  IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
    //GetInMemList(IDACList);
    result := format('%s@%s::', [IDACList.Values[trim(Uppercase(InSys)) + '_DATABASE'],
      IDACList.Values[trim(Uppercase(InSys)) + '_INFORMIXSERVER']]);
  finally
    IDACList.Free;
  end;
end;

function GetDBPrefix(db: string): string; // dh 12/06/2007
var IDACList: TStringList;
begin
  result := '';
  IDACList := TStringList.Create;
  try
    // IDACList.Text := GetList('c:\ae\IDAC.ini');
  //  GetInMemList(IDACList);
    result := format('%s@%s::', [IDACList.Values[db + '_DATABASE'], IDACList.Values[db + '_INFORMIXSERVER']]);
  finally
    IDACList.Free;
  end;
end;

{function IsEmailOptOut(InConnGlobal: TFDCOnnection; InEmailAddress: string): boolean;
var //errstr,
  theSQL: string;
  QOptOut: TFDQuery;
begin // find an e-mail opt out
      // it is expected the e-mail address is validated PRIOR to calling this func.
      // to minimize calls to the db (why check if the structure of the eml addr is not OK?
      // see: JMClasses.pas IsValidEmail();
  result := false;
  if (not assigned(InConnGlobal)) or (not InConnGlobal.Connected) then
  begin
    raise(exception.Create('IsEmailOptOut No glob Connection'));
    exit;
  end;

  InEmailAddress := trim(UpperCase(InEmailAddress));

  if InEmailAddress = '' then
  begin
    raise(exception.Create('IsEmailOptOut --> email address empty.'));
    exit;
  end;

  QOptOut := GetIdacQuery(InConnGlobal);

  theSQL := 'select mailer_addr from g_mailer_optout where ' +
    'mailer_addr = "' + InEmailAddress + '"';

  try
    try
      QOptOut.SQL.Clear;
    except on e: exception do
      begin
        raise(exception.Create('IsEmailOptOut sql.clear --> ' + e.Message));
        exit;
      end;
    end;

    try
      QOptOut.sql.Add(theSQL);
      QOptOut.Open;
    except on e: exception do
      begin
        ///showmessage('find iata opt out error --> ' + e.message);
        raise(exception.Create('IsEmailOptOut OpenQuery --> ' + e.Message));
        exit;
      end;
    end;

    if not QOptOut.IsEmpty then
    begin
      result := true;
    end;
  finally
    if assigned(QOptOut) then freeAndNIl(QOptOut);
  end;
end;
                                                   // vopsubt vopstype1
}
procedure get_prod_desc_type_subtype_source(basesys, subtype, stype1: string;
  InConn: TFDCOnnection; var theRec: jProdRec);
var thesql, plug, standalonesys: string;
  Q: TFDQuery;
begin // assume InConn points to Global OR WHSE
       // always called wrapped in try / catch

      // example:
    {  procedure TForm1.get_it_();
        var theRec: jProdRec;
      begin
        try
            get_prod_desc_type_subtype_source(
              sourceQ.FieldByName('basesys').AsString,
              sourceQ.FieldByName('vopsubt').AsString,
              sourceQ.FieldByName('vopstype1').AsString,
              IfxConnection1,
              theRec);
        except on e:exception do
        begin
          memo1.Lines.add(e.Message);
          exit;
        end;
        end;

        memo1.Lines.add('system:      ' + theRec.system);
        memo1.Lines.add('description: ' + therec.description);
        memo1.Lines.add('Product Type ' + theRec.prod_type);
        memo1.Lines.add('SubType:     ' + theRec.prod_subtype);
        memo1.Lines.add('Source:      ' + theRec.source_market);
      end;    }


  if (not assigned(InConn)) or (not InConn.Connected) then
  begin
    raise(exception.Create('No Connection'));
    exit;
  end;

// basesys is always specific.
// salonesys (stand-alone system) is always * unless it's hotel or
// air then it's "HOT" or "AIR" (They are stand-alone systems)

  try
    try
      Q := GetIDACQuery(InConn);
      theRec.description := '';
      theRec.prod_subtype := '';
      theRec.prod_type := '';
      theRec.source_market := '';
      theRec.system := '';
      standalonesys := '';

      basesys := trim(UpperCase(basesys));
      subtype := trim(UpperCase(subtype));
      stype1 := trim(UpperCase(stype1));

      // add this code 5/23/2008 ***********************************************
      // Hotel is a 'stand alone' system - ie different rules apply which are specific to it.
      // this should work fine for situations where vopstype1 is set to 'H' or 'A' for
      // Hotel and Air in systems  other than Hotel such as CANADA.
      // a good example is Hotel In Canadian system ..
      if stype1 = 'H' then standalonesys := 'HOT'
      else if stype1 = 'A' then standalonesys := 'AIR';
      // ***********************************************************************


      if (basesys = 'HOT') then
      begin
        standalonesys := basesys;
        // hotel and air are stored as 'HOT' in WHSE and global
        // if Hotel and stype1 = "A" then basesys s/b set to "AIR" to get proper record from
        // prod_code_ref table.
        // if Hotel and stype1 = '' OR Hotel and "H" then basesys s/b set to "HOT"
        if stype1 = '' then stype1 := 'H'
        else if (stype1 = 'A') then
        begin
          basesys := 'AIR';
          standalonesys := basesys; // reset
        end;
      end
      else if (basesys = 'AIR') then
      begin
        standalonesys := basesys;
        stype1 := 'A';
      end;
      //else standalonesys := '*'; // default for a "non-stand alone" system

      if trim(standalonesys) = '' then standalonesys := '*'; // default for a "non-stand alone" system

      if stype1 = '' then plug := ' (pcr_chaos_stype1 is null or length(pcr_chaos_stype1) = 0)'
      else plug := ' (pcr_chaos_stype1 = "' + stype1 + '")';

      theSQL := 'Select pcr_basesys, pcr_description, pcr_product_type, pcr_product_subtype, pcr_source_market, pcr_old_new_code from g_prod_code_ref where ' +
        '(((pcr_basesys = "' + basesys + '" and pcr_basediv = "*") or (pcr_basesys = "' + standalonesys + '" and pcr_basediv = "*")) ' +
        ' and pcr_chaos_subtype = "' + subtype + '" ' +
        ' and ' + plug + ' and pcr_old_new_code in ("n", "o"))' +
        ' order by 1 DESC, 6'; // sort important - if n (new type) and o (old type) are returned, it takes the n (new)
                               // still supports Old types in the systems which are not used on new vouchers any more
      q.SQL.Clear;
      Q.SQL.Text := theSQL;
      q.Active := true;

      if q.IsEmpty then
      begin
        raise(exception.Create('ERROR! no result for basesys: [' + basesys + '] stand alone system: [' +
          standalonesys + '] subtype: [' + subtype + '] stype1: [' + stype1 + ']'));
        exit;
      end
      else
      begin
        therec.system := q.Fields[0].AsString;
        therec.description := q.Fields[1].AsString;
        therec.prod_type := q.Fields[2].AsString;
        theRec.prod_subtype := q.Fields[3].AsString;
        theRec.source_market := q.Fields[4].AsString;
      end;
    except on e: exception do
      begin
        theRec.description := '';
        theRec.prod_subtype := '';
        theRec.prod_type := '';
        theRec.source_market := '';
        theRec.system := '';
        raise(exception.Create('general error in "get_prod_desc_type_subtype_source" --> ' + e.Message));
      end;
    end;
  finally
    try
      if assigned(Q) then FreeAndNil(Q);
    except;
    end;
  end;
end;

{function Is_IATA_OptOut(InGlobalConn: TFDCOnnection; InIata: string): boolean;
var theSQL, temp: string;
  Q: TFDQuery;
  i: Integer;
begin {
        find iata opt out - use for aemailer and lyris feed etc
        assume InGlobalConn is connected to aeglobal

       always call like this:
       try
          if Is_IATA_OptOut('99999999', MyGlobalConnWhichIsConnectedANDPointingToGlobal) Then
          begin
             // yes opt out
          end
          else
          begin
            // not an iata opt out
          end;
       except on e:exception do
        begin
          showmessage(e.message);
          exit;
        end;
       end;

  result := false;

  i := 0;
  i := length(InIATA);
  if i > 9 then
  begin
    raise(exception.Create('Is_IATA_OptOut IATA too long.'));
    exit;
  end;

  if not (assigned(InGlobalConn)) then
  begin
    raise(exception.Create('Is_IATA_OptOut Connection is nil.'));
    exit;
  end;

  if not (InGlobalConn.Connected) then
  begin
    raise(exception.Create('Is_IATA_OptOut Global Connection is not connected.'));
    exit;
  end;

  temp := '';
  temp := GetCurrent_DB_PARAMSTR(InGlobalConn);
  if temp <> 'GLOBAL' then
  begin
    raise(exception.Create('Is_IATA_OptOut --> Must be connected to "Global", not "' + temp + '".'));
    exit;
  end;

  if (trim(UpperCase(InIata)) = '') then
  begin
    raise(exception.Create('Is_IATA_OptOut --> No Iata provided'));
    exit;
  end;

  theSQL := '';
  theSQL := 'select gmoi_iata from g_mailer_optout_iata where ' +
    'gmoi_basesys = "*" and gmoi_basediv = "*" and ' +
    'gmoi_iata = "' + trim(UpperCase(InIata)) + '" and gmoi_active = "A"';

  try
    try
      Q := GetIDACQuery(InGlobalConn);
      Q.sql.Add(theSQL);
      Q.Active := true;
      if not (Q.IsEmpty) then result := true;
    except on e: exception do
      begin
        raise(exception.Create('Is_IATA_OptOut --> ' + e.Message));
        result := false;
        exit;
      end;
    end;
  finally
    if assigned(Q) then freeAndNil(Q);
  end;
end;    }

procedure SAdd(var TargetString: string; StringToAdd: string);
var i, j: integer;
begin
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end;

function SetCLB(sender: TObject; DoSet: Boolean): Boolean;
var i: integer;
begin // check or un-check all items in a TCheckListBox
  result := false;
  if not (sender is TCheckListBox) then exit;
  for i := 0 to TCheckListBox(Sender).Items.Count - 1 do
  begin
    TCheckListBox(Sender).Checked[i] := DoSet;
  end;
  result := true;
end;

procedure OpenQuery(InQuery: TFDQuery); // handle the cursor
var Save_Cursor: TCursor;
begin // handling cursor at the button click is more robust.
  try // bde handled this nicely but idac does not
    Save_Cursor := Screen.Cursor;
    Screen.Cursor := crSQLWait;
    application.ProcessMessages;
    try
      InQuery.Open;
    except on E: Exception do
        raise(exception.create(e.Message));
    end;
  finally
    screen.Cursor := Save_Cursor;
  end;
end;

{function ExecuteQuery(InQuery: TFDQuery): int64;
var Save_Cursor: TCursor; // handle the cursor
begin
  Result := 0;
  try
    Save_Cursor := Screen.Cursor;
    Screen.Cursor := crSQLWait;
    application.ProcessMessages;
    try
      Result := InQuery.ExecSQL; // Returns number of rows affected
    except on E: Exception do
        raise(exception.create(e.Message));
    end;
  finally
    Screen.Cursor := Save_Cursor;
  end;
end;

function IsValidSIPP(InSIPP: string; InGlobalConn: TFDCOnnection; var ErrStr: string): boolean;
var scode1, scode2, scodel4, theSQL, temp: string;
  i: integer;
  {  join the sipp def and the desc tables:

    Select sl4sipp,sl4llang, sl4ldesc From g_sippl4, outer g_sippl4lng
    where sl4sippstat = "A" and
    sl4serial = sl4lserlink
    order by sl4sipp

  function glast4(sl4code: string): boolean;
  var
    MightNotHaveBeenInitialized: TFDQuery;
  begin
    result := false;
    theSQL := 'select * from g_sippl4 where sl4basesys = "*" and sl4basediv = "*" and sl4sipp = "' + sl4Code + '"';
    try
      try
        MightNotHaveBeenInitialized := GetIDACQuery(InGlobalConn);
        MightNotHaveBeenInitialized.sql.Add(theSQL);
        MightNotHaveBeenInitialized.Active := true;
        // if found then it's ok
        if not (MightNotHaveBeenInitialized.IsEmpty) then result := true;
      except on e: exception do
        begin
          ErrStr := '4 -> ' + e.Message;
        end;
      end;
    finally
      MightNotHaveBeenInitialized.free;
    end;
  end;
begin
  result := false;
  ErrStr := '';

  if (InGlobalConn = nil) then
  begin
    ErrStr := 'No Connection provided.';
    exit;
  end
  else if not (InGlobalConn.Connected) then
  begin
    ErrStr := 'Not Connected.';
    exit;
  end;

  temp := '';
  temp := GetCurrent_DB_PARAMSTR(InGlobalCOnn);
  if (temp <> 'GLOBAL') then
  begin
    ErrStr := 'Not Connected to Global. Connected to ' + temp;
    exit;
  end;

  if trim(InSIPP) = '' then
  begin
    ErrStr := 'No SIPP provided.';
    exit;
  end
  else scode1 := InSIPP;

  i := length(scode1);

  if i < 5 then
  begin
    result := true;
    exit;
  end;

  if (i = 5) or (i = 7) then
  begin
    ErrStr := 'Odd length SIPP not allowed.';
    result := false;
    exit;
  end;

  scodel4 := '';
  scodel4 := copy(scode1, 5, 2);

  if glast4(scodel4) then result := true
  else
  begin
    ErrStr := ErrStr + 'Valid Pattern not defined for position five and six of SIPP CODE';
    result := false;
    exit;
  end;

  if i = 8 then scodel4 := copy(scode1, 7, 2);

  if glast4(scodel4) then result := true
  else
  begin
    ErrStr := ErrStr + ' Valid Pattern not defined  for position 7 and 8 of SIPP CODE';
    result := false;
  end;
end;

procedure GetValidSIPPCombos(var slSIPPList: TStringList; InGlobalConn: TFDCOnnection);
var theSQL: string;
  MightNotHaveBeenInitialized: TFDQuery;
begin
  if slSIPPList = nil then exit
  else slSIPPList.Clear;
  theSQL := 'select distinct sl4sipp from g_sippl4 where sl4basesys = "*" and sl4basediv = "*" ' +
    ' and sl4sippstat = "A"';
  try
    try
      MightNotHaveBeenInitialized := GetIDACQuery(InGlobalConn);
      MightNotHaveBeenInitialized.RequestLive := false;
      MightNotHaveBeenInitialized.sql.Add(theSQL);
      MightNotHaveBeenInitialized.Active := true;
        // if found then it's ok
      if (MightNotHaveBeenInitialized.IsEmpty) then exit
      else
      begin
        MightNotHaveBeenInitialized.First;
        while not MightNotHaveBeenInitialized.Eof do
        begin
          slSIPPList.Add(MightNotHaveBeenInitialized.Fields[0].asstring);
          MightNotHaveBeenInitialized.Next;
        end;
      end;
    except on e: exception do
      begin
        raise(exception.Create(e.Message));
      end;
    end;
  finally
    MightNotHaveBeenInitialized.free;
  end;
end;   }

procedure GetAEFormatSettings(var theFormatSettings: AEFormatSettings);
var i: integer; // fills a structure of TFormatSettings with USA English Format
                // you can use this to pass into FormatDateTime of set Global formats
  FormatSettingsIdontCareAbout: TFormatSettings;
begin
  GetLocaleFormatSettings(SysLocale.DefaultLCID, FormatSettingsIdontCareAbout);
  theFormatSettings.CurrencyFormat := FormatSettingsIdontCareAbout.CurrencyFormat;
  theFormatSettings.NegCurrFormat := FormatSettingsIdontCareAbout.NegCurrFormat;
  theFormatSettings.CurrencyDecimals := FormatSettingsIdontCareAbout.NegCurrFormat;
  theFormatSettings.CurrencyString := FormatSettingsIdontCareAbout.CurrencyString;
  theFormatSettings.TwoDigitYearCenturyWindow := FormatSettingsIdontCareAbout.TwoDigitYearCenturyWindow;
  theFormatSettings.ThousandSeparator := ',';
  theFormatSettings.DecimalSeparator := '.';
  theFormatSettings.DateSeparator := '/';
  theFormatSettings.TimeSeparator := ':';
  theFormatSettings.ListSeparator := ',';
  theFormatSettings.ShortDateFormat := 'M/d/yyyy';
  theFormatSettings.LongDateFormat := 'dddd, MMMM dd, yyyy';
  theFormatSettings.TimeAMString := 'AM';
  theFormatSettings.TimePMString := 'PM';
  theFormatSettings.ShortTimeFormat := 'h:mm AMPM';
  theFormatSettings.LongTimeFormat := 'h:mm:ss AMPM';

  for i := 1 to 12 do
  begin
    case i of
      1: begin
          theFormatSettings.ShortMonthNames[i] := 'Jan';
          theFormatSettings.LongMonthNames[i] := 'January';
          theFormatSettings.ShortDayNames[i] := 'Sun';
          theFormatSettings.LongDayNames[i] := 'Sunday';
        end;
      2: begin
          theFormatSettings.ShortMonthNames[i] := 'Feb';
          theFormatSettings.LongMonthNames[i] := 'February';
          theFormatSettings.ShortDayNames[i] := 'Mon';
          theFormatSettings.LongDayNames[i] := 'Monday';
        end;
      3: begin
          theFormatSettings.ShortMonthNames[i] := 'Mar';
          theFormatSettings.LongMonthNames[i] := 'March';
          theFormatSettings.ShortDayNames[i] := 'Tue';
          theFormatSettings.LongDayNames[i] := 'Tuesday';
        end;
      4: begin
          theFormatSettings.ShortMonthNames[i] := 'Apr';
          theFormatSettings.LongMonthNames[i] := 'April';
          theFormatSettings.ShortDayNames[i] := 'Wed';
          theFormatSettings.LongDayNames[i] := 'Wednesday';
        end;
      5: begin
          theFormatSettings.ShortMonthNames[i] := 'May';
          theFormatSettings.LongMonthNames[i] := 'May';
          theFormatSettings.ShortDayNames[i] := 'Thu';
          theFormatSettings.LongDayNames[i] := 'Thursday';
        end;
      6: begin
          theFormatSettings.ShortMonthNames[i] := 'Jun';
          theFormatSettings.LongMonthNames[i] := 'June';
          theFormatSettings.ShortDayNames[i] := 'Fri';
          theFormatSettings.LongDayNames[i] := 'Friday';
        end;
      7: begin
          theFormatSettings.ShortMonthNames[i] := 'Jul';
          theFormatSettings.LongMonthNames[i] := 'July';
          theFormatSettings.ShortDayNames[i] := 'Sat';
          theFormatSettings.LongDayNames[i] := 'Saturday';
        end;
      8: begin
          theFormatSettings.ShortMonthNames[i] := 'Aug';
          theFormatSettings.LongMonthNames[i] := 'August';
        end;
      9: begin
          theFormatSettings.ShortMonthNames[i] := 'Sep';
          theFormatSettings.LongMonthNames[i] := 'September';
        end;
      10: begin
          theFormatSettings.ShortMonthNames[i] := 'Oct';
          theFormatSettings.LongMonthNames[i] := 'October';
        end;
      11: begin
          theFormatSettings.ShortMonthNames[i] := 'Nov';
          theFormatSettings.LongMonthNames[i] := 'November';
        end;
      12: begin
          theFormatSettings.ShortMonthNames[i] := 'Dec';
          theFormatSettings.LongMonthNames[i] := 'December';
        end;
    end;
  end;
end;

procedure Get_ListFromPipeString(var theList: TStringList; InPipeStr: string);
var strings: TStrings;
begin // get a TStringList from a string structured like "adsf|asdf|asdf|asdf"

  { example: load a list of dbconnections into a combobox
  MyList := TStringList.Create;
  try
    try
      MyList.LoadFromFile('c:\ae\theFile.ini');
      temp := MyList.Values['pubdbs'];  // pubdbs=USA|CANA|KEM|ETC
      MyList.Clear;
      Get_ListFromPipeString(MyList, temp);

      if trim(MyList.Text) = '' then FillManually()
      else ComboReasonCode.Items := MyList;

    except on e: exception do
      begin
        FillManually();
        showmessage('x- ' + e.Message);
      end;
    end;
  finally
    MyList.free;
  end;
  }
  if (theList = nil) or (trim(InPipeStr) = '') then exit;
  thelist.Clear;
  try
    strings := A_SplitStrings(InPipeStr, '|', theList);
  except on e: exception do
    begin
      raise(exception.create('Get_ListFromPipeString --> ' + e.message));
    end;
  end;
end;

function A_SplitStrings(const str: string; const separator: string; Strings: TStrings): TStrings;
// Fills a string list with the parts of "str" separated by "separator".
var
  n: integer;
  p, q, s: PChar;
  item: string;
begin
  if Strings = nil then
  begin
    Result := nil;
    exit;
  end
  else Result := Strings;
  Result.Clear;

  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);

    repeat
      q := AnsiStrPos(p, s);
      if q = nil then q := AnsiStrScan(p, #0);
      SetString(item, p, q - p);
      Result.Add(item);
      p := q + n;
    until q^ = #0;

  except
    item := '';
    raise;
  end;
end;

function UName(): string; // get current logged on user for client machine - same as GetTheUserName()
var buffer: array[0..255] of char;
  buffSize: DWORD;
begin
  buffSize := sizeOf(buffer);
  GetUserName(@buffer, buffSize);
  result := trim(uppercase(buffer));
end;

function MName(): string; // get client machine name
var i: longint;
  MyArray: array[0..255] of char;
begin
  result := '';
  FillChar(MyArray, SizeOf(MyArray), #0);
  i := 255;
  GetComputerName(MyArray, dword(i));
  result := Format('%s', [MyArray]);
end;

function MaskCC(InCC: string): string;
var
  temp: string;
  i, x: Integer;
  function IsDigit(a: char): boolean;
  begin
    case a of
      '0'..'9': Result := True;
    else
      Result := False
    end;
  end;
begin
  result := '****************';
  temp := '';
  x := length(InCC);

  for i := 1 to x do
  begin
    if IsDigit(InCC[i]) then
    begin
      temp := temp + InCC[i];
    end;
  end;
  i := length(temp) - 3;
  result := '************' + copy(temp, i, 4);
end;




procedure WriteLog(path, InStr: string);
var
  t: textfile;
  MyList: TStringList;
  sFULLPATH: string;
begin // path includes full path plus name of file. the filename is appended with date
  if path = '' then path := '\\fileserver\chaoslog\ERR_LOG' + uname();

  sFULLPATH := path + FormatDateTime('mmmddyyyy', Now) + '.txt';

  if (FileExists(sFULLPATH)) then
  begin
    try
      assignfile(t, sFULLPATH);
      try
        append(t);
      except
        begin
          rewrite(t);
        end;
      end;
      writeln(t, '------->');
      writeln(t, InStr + '|' + DateTimeTOStr(Now));
      flush(t);
    finally
      try
        closeFile(t);
      except;
      end;
    end;
  end
  else
  begin
    MyList := nil;
    try
      try
        MyList := TStringList.Create;
      except
        exit;
      end;

      MyList.Add('start log file ' + DateTimeTOStr(Now));
      MyList.Add(InStr + ' ' + DateTimeTOStr(Now));
      MyList.SaveToFile(sFULLPATH);
    finally
      MyList.Free;
    end;
  end;
end;

{function GetServiceType(conn: TFDCOnnection; res, rel: integer): string;
var
  MyQuery: TFDQuery;
  DataBase, SUBTYPE, HOTAIRTYPE, sOPERATOR: string;
begin
  result := '';
  DataBase := '';
  SUBTYPE := '';
  HOTAIRTYPE := '';
  sOPERATOR := '';

  if (not assigned(conn)) or (not (conn.Connected)) then
  begin
    raise(exception.Create('GetServiceType Connection ERR'));
    exit;
  end;

  try
    IntToStr(res);
    IntToStr(rel);
  except on e: exception do
    begin
      raise(exception.Create('GetServiceType invalid res-rel --> ' + e.message));
      exit;
    end;
  end;

  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add('select VOPSUBT, VOPSTYPE1, VOPID from vouch where vcnumb = :reservation and vchrel= :release');
      MyQuery.Params.ParamValues['reservation'] := res;
      MyQuery.Params.ParamValues['release'] := rel;
      MyQuery.Active := true;

      if myquery.IsEmpty then
      begin
        raise(exception.Create('GetServiceType no voucher found.'));
        exit;
      end;

      SUBTYPE := UpperCase(MyQuery.Fields[0].AsString);
      HOTAIRTYPE := uppercase(MyQuery.Fields[1].AsString);
      sOperator := uppercase(MyQuery.Fields[2].AsString);
      MyQuery.Active := false;
      DataBase := GetCurrentDB(conn);
      DataBase := UpperCase(DataBase);

      if DataBase = '?' then
      begin
        result := '?';
        exit;
      end
      else if DataBase = 'HRESERVE' then
      begin
        if (HOTAIRTYPE = 'A') and (SUBTYPE = 'CF') then result := 'CHANGE FEE'
        else if (HOTAIRTYPE = 'A') and (SUBTYPE = 'XF') then result := 'CANCELATION FEE'
        else if (HOTAIRTYPE = 'A') then result := 'AIR' // if SUBTYPE = 'B' also, it is Business class air
        else if (SUBTYPE = 'P') then result := 'PACKAGE'
        else result := 'HOTEL';
      end
      else // all the other DB's use the same type
      begin
        if (HOTAIRTYPE = 'H') then result := 'HOTEL' // add
        else if (SUBTYPE = 'T') then result := '?'
        else if (SUBTYPE = 'C') or
          (SUBTYPE = 'I') or
          (SUBTYPE = 'B') or
          (SUBTYPE = 'W') or
          (SUBTYPE = 'WX') or
          (SUBTYPE = 'WM') or
          (SUBTYPE = 'GS') or
          (SUBTYPE = 'GW') then result := 'CAR'
        else if (SUBTYPE = 'CP') then result := 'CELL'
        else if (SUBTYPE = 'GP') then result := 'GPS'
        else if (SUBTYPE = 'M') then result := 'MOTORCYCLE'
        else if (SUBTYPE = 'P') then result := 'PACKAGE'
        else if (SUBTYPE = 'H') then result := 'CHAUFFEUR'
        else if (SUBTYPE = 'L') then result := 'LEASE'
        else result := '?';
      end;

      // changed 3/21/2007
      //  if sOperator = 'TG' then result := 'TRAVEL GUARD';
      // this is a catch-all which should not have to be used, but there are still exceptions to rules...
      if (sOperator = 'TG') or (sOperator = 'TI') then result := 'TRAVEL GUARD';

    except on e: exception do
      begin
        raise(exception.Create('GetServiceType SQL_ERR --> ' + e.message));
      end;
    end;
  finally
    MyQuery.free;
  end;
end;   }

function GetServiceType2(SourceDB {CANA | KEM etc}, VOPSUBT, VOPSTYPE1, VOPID: string): string;
var
  DataBase, SUBTYPE, HOTAIRTYPE, sOPERATOR: string;
begin
  result := '';
  DataBase := '';
  SUBTYPE := '';
  HOTAIRTYPE := '';
  sOPERATOR := '';

  SourceDB := trim(UpperCase(SourceDB));
  VOPSUBT := trim(UpperCase(VOPSUBT));
  VOPSTYPE1 := trim(UpperCase(VOPSTYPE1)); // sometimes nil
  VOPID := trim(UpperCase(VOPID));

  if (sourcedb = '') or (VOPSUBT = '') or (VOPID = '') then
  begin
    raise(exception.Create('GetServiceType2 invalid param'));
    exit;
  end;

  try
    SUBTYPE := VOPSUBT;
    HOTAIRTYPE := VOPSTYPE1;
    sOperator := VOPID;
    DataBase := SourceDB;

    if DataBase = 'HOT' then
    begin //change these 3/4/2009 according to g_prod_code_ref
                                       // old code           new code
      if (HOTAIRTYPE = 'A') and ((SUBTYPE = 'XF') or (SUBTYPE = 'XC')) then result := 'CHANGE FEE'
                                      // old code         // new code
      else if (HOTAIRTYPE = 'A') and ((SUBTYPE = 'CF') or (SUBTYPE = 'XX')) then result := 'CANCELATION FEE'

      else if (HOTAIRTYPE = 'A') then result := 'AIR' // if SUBTYPE = 'B' also, it is Business class air
      else if (SUBTYPE = 'P') then result := 'PACKAGE'
      else result := 'HOTEL';
    end
    else // all the other DB's use the same type
    begin
      if (SUBTYPE = 'T') then result := '?'
      else if (SUBTYPE = 'C') or
        (SUBTYPE = 'I') or
        (SUBTYPE = 'B') or
        (SUBTYPE = 'W') or
        (SUBTYPE = 'WX') or
        (SUBTYPE = 'WM') or
        (SUBTYPE = 'GS') or
        (SUBTYPE = 'GW') then result := 'CAR'
      else if (SUBTYPE = 'CP') then result := 'CELL'
      else if (SUBTYPE = 'GP') then result := 'GPS'
      else if (SUBTYPE = 'M') then result := 'MOTORCYCLE'
      else if (SUBTYPE = 'P') then result := 'PACKAGE'
      else if (SUBTYPE = 'H') then result := 'CHAUFFEUR'
      else if (SUBTYPE = 'L') then result := 'LEASE'
      else result := '?';
    end;

      // changed 3/21/2007
      //  if sOperator = 'TG' then result := 'TRAVEL GUARD';
      // this is a catch-all which should not have to be used, but there are still exceptions to rules...
    if (sOperator = 'TG') or (sOperator = 'TI') then result := 'TRAVEL GUARD';

  except on e: exception do
    begin
      raise(exception.Create('GetServiceType2 ERR --> ' + e.message));
    end;
  end;
end;


function GetCurrentDB(conn: TFDConnection): string;
begin
  result := '?';
  if not assigned(conn) then exit
  else if not (conn.Connected) then exit;

  try
    result := conn.Params.Values['DATABASE'];
  except result := '?';
  end;
end;

function GetCurrentHost(conn: TFDCOnnection): string;
begin
  result := '?';
  if not assigned(conn) then exit;
  if not (conn.Connected) then exit; //add 9/12/20006
  try
    result := conn.Params.Values['WIN32HOST'];
  except result := '?';
  end;
end;

{function GetCurrent_DB_PARAMSTR(conn: TFDCOnnection): string;
var // return the paramStr used when calling programs from command prompt
  db, host: string;
begin
  result := '?';
  if not assigned(conn) then exit
  else if not (conn.Connected) then exit;

  try
    db := UpperCase(conn.Params.Values['DATABASE']);
    host := UpperCase(conn.Params.Values['WIN32HOST']);

 //   showmessage('db: ['+ db + ' host: ['+ Host+']');

    if (db = 'RESERVE@ONLINE_F50') or ((db = 'RESERVE') and (host = 'F50DEV')) then result := 'TESTOLD'
    else if (db = 'RESERVE_TEST') then result := 'TEST'
    else if (db = 'RESERVE@IDS_P650') or (db = 'RESERVE') then result := 'USA'
    else if (db = 'CRESERVE@IDS_P570C') or (db = 'CRESERVE') then result := 'CANA'
    else if (db = 'KEMRESERVE@IDS_P570C') or (db = 'KEMRESERVE') then result := 'KEM'
    else if (db = 'HOHO@IDS_P570C') or (db = 'HOHO') then result := 'HOHO'
    else if (DB = 'UKRESERVE@IDS_P570E') or (db = 'UKRESERVE') then result := 'UK'
    else if (DB = 'SAFRESERVE@IDS_P570E') or (db = 'SAFRESERVE') then result := 'SAF'
    else if (db = 'OPCALLS') then result := 'OPCALLS'
    else if (DB = 'EURORESERVE@IDS_P570D') or (db = 'EURORESERVE') then result := 'EURO'
    else if (DB = 'RPBUS@IDS_P570C') or (db = 'RPBUS') or (db = 'AEWHSE') then result := 'WHSE'
    else if (DB = 'VOUCHERS_PRE2003@ONLINE_F50AUS') or (db = 'VOUCHERS_PRE2003') then result := 'ARCH'
    else if (db = 'WEBDATA') then result := 'WEBDATA'
    else if (db = 'DEVLANGDATA@ONLINE_RES3') or (db = 'DEVLANGDATA') then result := 'DEVLANGDATA'
    else if (db = 'AUSRESERVE@IDS_P570d') or (db = 'AUSRESERVE') then result := 'AUS'
    else if (db = 'NZRESERVE@IDS_P570d') or (db = 'NZRESERVE') then result := 'NEWZ'
    else if (DB = 'HRESERVE@ONRES') or (db = 'HRESERVE') then result := 'HOT'
    else if (db = 'LACRESERVE@IDS_P570C') or (db = 'LACRESERVE') then result := 'LAC'
    else if (db = 'DFWRESERVE@IDS_P570C') or (db = 'DFWRESERVE') then result := 'DFW'
    else if (db = 'AEACCT@ONLINE_RES') or (db = 'AEACCT') then result := 'ACCT'
    else if (db = 'AUSACCT@IDS_P570D') or (db = 'AUSACCT') then result := 'AUSACCT'
    else if (db = 'AEGLOBAL@ONLINE_F50') or ((db = 'AEGLOBAL') and (host = 'F50DEV')) then result := 'TESTGLOBALOLD'
    else if (db = 'AEGLOBAL_TEST') then result := 'TESTGLOBAL'
    else if (db = 'RESERVE_DEV@IDS_P520') or ((db = 'RESERVE_DEV') and (host = 'IDS_P520')) then result := 'QUAL'
    else if (db = 'RESERVE_DEV') and (host = 'IDSDEV.AEMAINE.COM') then result := 'QUAL2'
    else if (db = 'AEGLOBAL_DEV') and (host = 'IDSDEV.AEMAINE.COM') then result := 'QUAL2GLOBAL'
    else if (db = 'AEGLOBAL@IDS_P570') or (db = 'AEGLOBAL') then result := 'GLOBAL'
  except;
  end;
end;

function GetFullDB(InDBParam: string): string;
begin // obsolete - see dynamic function GetFullDB2(InDBParam: string): string; // jm 12/06009
  result := '?';
  InDBParam := UpperCase(InDBParam);
  if InDBParam = 'TEST' then result := 'RESERVE_TEST@IDSDEV'
  else if InDBParam = 'USA' then result := 'RESERVE@IDS_P650'
  else if InDBParam = 'CANA' then result := 'CRESERVE@IDS_P570C'
  else if InDBParam = 'KEM' then result := 'KEMRESERVE@IDS_P570C'
  else if InDBParam = 'HOHO' then result := 'HOHO@IDS_P570C'
  else if InDBParam = 'UK' then result := 'UKRESERVE@IDS_P570E'
  else if InDBParam = 'EURO' then result := 'EURORESERVE@IDS_P570D'
  else if InDBParam = 'ARCH' then result := 'VOUCHERS_PRE2003@ONLINE_F50AUS'
  else if InDBParam = 'WEBDATA' then result := 'WEBDATA@ONRES'
  else if InDBParam = 'DEVLANGDATA' then result := 'DEVLANGDATA@ONLINE_RES3'
  else if InDBParam = 'AUS' then result := 'AUSRESERVE@IDS_P570D'
  //else if InDBParam = 'AEAU' then result := 'AEAURESERVE@IDS_P650'
  else if InDBParam = 'NEWZ' then result := 'NZRESERVE@IDS_P570D'
  else if InDBParam = 'HOT' then result := 'HRESERVE@IDS_P570C'
  else if InDBParam = 'LAC' then result := 'LACRESERVE@IDS_P570C'
  else if InDBParam = 'DFW' then result := 'DFWRESERVE@IDS_P570C'
  else if InDBParam = 'ACCT' then result := 'AEACCT@ONLINE_RES'
  else if InDBParam = 'SAF' then result := 'SAFRESERVE@IDS_P570E'
  else if InDBParam = 'AUSACCT' then result := 'AUSACCT@IDS_P570D'
  else if InDBParam = 'TESTGLOBAL' then result := 'AEGLOBAL_TEST@IDSDEV'
  else if InDBParam = 'GLOBAL' then result := 'AEGLOBAL@IDS_P570'
  else if (InDBParam = 'WHSE') or (InDBParam = 'MKTG') then result := 'AEWHSE@IDS_P520';
end;
{
function GetFullDB2(InDBPARAM: string): string; // jm 12/06009
var IDACList: TStringList; // replaces GetFUllDB
begin
  result := '';
  IDACList := TStringList.Create;
  try
    GetInMemList(IDACList);
    result := IdacList.Values[InDBPARAM + '_DATABASE'] + '@' +
      IDACList.Values[InDBPARAM + '_INFORMIXSERVER'];
  finally
    IDACList.Free;
  end;
end;
{

function Get_Old_BDE_AliasFromIDAC(conn: TFDCOnnection): string; // return what Database.aliasName would have been with BDE
var temp: string;
begin // do not mod. this has to be backward compatible
  result := '?';
  if not assigned(conn) then exit;
  if not (conn.Connected) then exit; //add 9/12/20006

  temp := GetCurrent_DB_PARAMSTR(conn);
  if temp = 'USA' then result := 'reserve'
  else if temp = 'CANA' then result := 'reservec'
  else if temp = 'KEM' then result := 'reservekem'
  else if temp = 'UK' then result := 'reserveuk'
  else if temp = 'EURO' then result := 'reserveeuro'
  else if temp = 'LAC' then result := 'reservelac'
  else if temp = 'DFW' then result := 'reservedfw'
  else if temp = 'NEWZ' then result := 'reservenz'
  else if temp = 'SAF' then result := 'reservesaf'
  else if temp = 'HOHO' then result := 'hohoreserve'
  else if temp = 'ACCT' then result := 'resacct'
  else if temp = 'AUSACCT' then result := 'ausacct'
  else if temp = 'TEST' then result := 'idsdev'
  else if temp = 'WEBDATA' then result := 'webdata'
  else if (temp = 'HOT') or (temp = 'AIR') then result := 'reserve5'
  else if temp = 'AUS' then result := 'reserveaus'
  else if temp = 'AEAU' then result := 'aeaureserve'
  else if temp = 'GLOBAL' then result := 'aeglobal'
  else if temp = 'TESTGLOBAL' then result := 'TESTGLOBAL'
  else if temp = 'DEVLANGDATA' then result := 'DEVLANGDATA'
  else if temp = 'ARCH' then result := 'VOUCHERS_PRE2003'
  else if (temp = 'MKTG') or (temp = 'WHSE') then result := 'WHSE'
  else if (temp = 'QUAL') then result := 'QUAL'
  else ;
end;


function Get_Old_BDE_AliasFromParam(InParam: string): string; // return what Database.aliasName would have been with BDE
var temp: string;
begin // do not mod. this has to be backward compatible
  result := '?';
  temp := trim(Uppercase(InParam));

  if temp = 'USA' then result := 'reserve'
  else if temp = 'CANA' then result := 'reservec'
  else if temp = 'KEM' then result := 'reservekem'
  else if temp = 'UK' then result := 'reserveuk'
  else if temp = 'EURO' then result := 'reserveeuro'
  else if temp = 'LAC' then result := 'reservelac'
  else if temp = 'DFW' then result := 'reservedfw'
  else if temp = 'NEWZ' then result := 'reservenz'
  else if temp = 'SAF' then result := 'reservesaf'
  else if temp = 'HOHO' then result := 'hohoreserve'
  else if temp = 'ACCT' then result := 'resacct'
  else if temp = 'AUSACCT' then result := 'ausacct'
  else if temp = 'TEST' then result := 'idsdev'
  else if temp = 'WEBDATA' then result := 'webdata'
  else if (temp = 'HOT') or (temp = 'AIR') then result := 'reserve5'
  else if temp = 'AUS' then result := 'reserveaus'
  else if temp = 'AEAU' then result := 'aeaureserve'
  else if temp = 'GLOBAL' then result := 'aeglobal'
  else if temp = 'TESTGLOBAL' then result := 'TESTGLOBAL'
  else if temp = 'DEVLANGDATA' then result := 'DEVLANGDATA'
  else if temp = 'ARCH' then result := 'VOUCHERS_PRE2003'
  else if (temp = 'MKTG') or (temp = 'WHSE') then result := 'WHSE'
  else if (temp = 'QUAL') then result := 'QUAL'
  else ;
end;   }


procedure Get_DBList(var theList: TStringList);
var temp: string;
  strings: TStrings;
begin
  if theList = nil then exit;
  thelist.Clear;

  try
    GetInMemList(theList);
    temp := theList.Values['dbs'];
    strings := A_SplitStrings(temp, '|', theList);
  except on e: exception do
    begin
      raise(exception.create('Get DB List --> ' + e.message));
    end;
  end;
end;

procedure Get_Public_DBList(var theList: TStringList);
var temp: string;
  strings: TStrings;
begin
  if theList = nil then exit;
  thelist.Clear;

  try
   // GetInMemList(theList);
    temp := theList.Values['pubdbs'];
    strings := A_SplitStrings(temp, '|', theList);
  except on e: exception do
    begin
      raise(exception.create('Get Public DB List --> ' + e.message));
    end;
  end;
end;

function LeftPadChar(const S: AnsiString; C: AnsiChar; Len: Cardinal): AnsiString;
  {-Pad a string on the left with a specified character.}
begin
  if Length(S) >= LongInt(Len) then {!!.D4}
    Result := S
  else if Length(S) < MaxLongInt then begin
    SetLength(Result, Len);
    Move(S[1], Result[Succ(Word(Len)) - Length(S)], Length(S));
    FillChar(Result[1], LongInt(Len) - Length(S), C); {!!.D4}
  end;
end;

function GetRentalDays(FromDate, ToDate: TDate; FromTime, ToTime: TTime): Real;
begin
  result := 0;
  try
    result := trunc(ToDate) - trunc(FromDate);
    if frac(ToTime) > frac(FromTime) then result := result + 1;
  except result := -99
  end;
end;

function GetDBTag(InAlias: string): string;
var // obsolete
  temp: string;
begin
  result := '';
  temp := UpperCase(InAlias);
  if (temp = 'RESERVE') then result := 'USA'
  else if (temp = 'RESERVEC') then result := 'CANA'
  else if (temp = 'F50_DEV') or (temp = 'RESERVED') or (temp = 'IDSDEV') then result := 'TEST'
  else if (temp = 'RESERVEKEM') then result := 'KEM'
  else if (temp = 'HOHORESERVE') then result := 'HOHO'
  else if (temp = 'RESERVEUK') then result := 'UK'
  else if (temp = 'RESERVESAF') or (temp = 'SAFRESERVE') then result := 'SAF'
  else if (temp = 'RESERVEEURO') then result := 'EURO'
  else if (temp = 'RESERVEAUS') then result := 'AUS'
  else if (temp = 'AEAURESERVE') or (temp = 'RESERVEAEAU') then result := 'AEAU'
  else if (temp = 'RESERVENZ') or (temp = 'NZRESERVE') then result := 'NEWZ'
  else if (temp = 'RESERVEDFW') then result := 'DFW'
  else if (temp = 'RESERVELAC') then result := 'LAC'
  else if (temp = 'AEGLOBAL') then result := 'GLOBAL'
  else if (temp = 'TESTGLOBAL') then result := 'TESTGLOBAL' // wtf
  else if (temp = 'AUSACCT') then result := 'AACC'
  else if (temp = 'RESACCT') then result := 'ACCT'
  else if (temp = 'RPBUS') or (temp = 'AEWHSE') then result := 'WHSE'
  else if (temp = 'RESERVE5') then result := 'HOT'
  else if (temp = 'WEBDATA') then result := 'WEB'
  else if (temp = 'OPCALLS') then result := 'OPCALLS'
  else if (temp = 'DEVLANGDATA') then result := 'DWEB'
  else result := '??';
end;

function SetPathForCurrentSession(PathToAdd: string): Boolean;
const ENV_VAR_BUFF_SIZE = $4000;
var i: boolean;
  OldPath: string;
begin
  result := false;
  i := false;
  SetLength(OldPath, ENV_VAR_BUFF_SIZE);
  SetLength(OldPath, GetEnvironmentVariable('PATH', PChar(OldPath), ENV_VAR_BUFF_SIZE));
  i := SetEnvironmentVariable('PATH', PChar(PathToAdd + OldPath));
  if i then result := true;
end;

function SetINFXDIRForCurrentSession(DirToSet: string): Boolean;
{C:\informix\ for IDAC C:\ifxbin\ for DBX }
const ENV_VAR_BUFF_SIZE = $4000;
var i: boolean;
  OldDir: string;
begin
  result := false;
  i := false;
  SetLength(OldDir, ENV_VAR_BUFF_SIZE);
  SetLength(OldDir, GetEnvironmentVariable('INFORMIXDIR', PChar(OldDir), ENV_VAR_BUFF_SIZE));
  i := SetEnvironmentVariable('INFORMIXDIR', PChar(DirToSet));
  if i then result := true;
end;

function RandomColor(): TColor;
begin
  Randomize;
  Result := RGB(Random(256), Random(256), Random(256));
end;

function GetDBColor(InAliasORParam: string): TColor;
begin
  result := clWhite;

  if InAliasORParam = '' then exit;
  InAliasORParam := UpperCase(InAliasORParam);

  if (InAliasORParam = 'RESERVE') or (InAliasORParam = 'USA') then result := $00FCCD3F
  else if (InAliasORParam = 'VOUCHERS_PRE2003') or (InAliasORParam = 'ARCH') or
    (InAliasORParam = 'WHSE') then result := $00FCCD3F
  else if (InAliasORParam = 'RESERVEC') or (InAliasORParam = 'CANA') then result := clRed
  else if (InAliasORParam = 'RESERVEKEM') or (InAliasORParam = 'KEM') then result := $0000A6FF // FFA600 for html
  else if (InAliasORParam = 'AEGLOBAL') or (InAliasORParam = 'GLOBAL') then result := RandomColor()
  else if (InAliasORParam = 'TESTGLOBAL') then result := RandomColor()
  else if (InAliasORParam = 'RESERVE5') or (InAliasORParam = 'HOT') then result := clGreen
  else if (InAliasORParam = 'HOHORESERVE') or (InAliasORParam = 'HOHO') then result := $006AD5BD
  else if (InAliasORParam = 'RESERVEEURO') or (InAliasORParam = 'EURO') then result := clSilver
  else if (InAliasORParam = 'RESERVEUK') or (InAliasORParam = 'UK') then result := clGray
  else if (InAliasORParam = 'IDSDEV') or (InAliasORParam = 'RESERVED') or (InAliasORParam = 'TEST') then result := clLime
  else if (InAliasORParam = 'RESERVEAUS') or (InAliasORParam = 'AUS') then result := $00358BB0
  else if (InAliasORParam = 'RESERVENZ') or (InAliasORParam = 'NEWZ') then result := $00358EE0
  else if (InAliasORParam = 'RESERVEDFW') or (InAliasORParam = 'DFW') then result := clOlive
  else if (InAliasORParam = 'RESERVELAC') or (InAliasORParam = 'LAC') then result := $00B3D5D9 //clYellow
  else if (InAliasORParam = 'EAVRESERVE') or (InAliasORParam = 'EAV') then result := $0093DB62 // defunkt db
  else if (InAliasORParam = 'QUAL') or (InAliasORParam = 'RESERVEDEV') then result := clLime
  else if (InAliasORParam = 'SAFRESERVE') or (InAliasORParam = 'SAF') or (InAliasORParam = 'ZA') then result := $0057A0EA
  else if (InAliasORParam = 'AEAURESERVE') or (InAliasORParam = 'AEAU') then result := $00B3D5D9 // defunkt db
  else if (InAliasORParam = 'OPCALLS') or (InAliasORParam = 'OPCALLS') then result := $00B3D5D9
  else
  begin
    //raise(exception.create('GetDBClolor-->invalid ALIAS: ' + InAliasORParam));
    result := RandomColor();
  end;
end;

procedure BrickWall();
begin
  ; //nada
end;

// IDAC FUNCTIONS:

function GetSupportingFiles(Source, Target: string): boolean;
var // refresh your files i.e. GetSupportingFiles('\\fileserver\UPDATE\yourFile.ini', 'c:\ae\YourFile.ini');
  SourcePath, DestinationPath: string;
  SourceInt, DestinationInt: integer;
  SourceDate, DestinationDate: TDateTime;
begin
  if (trim(Source) = '') or (trim(Target) = '') then
  begin
    result := false;
    exit;
  end
  else result := true;

  SourcePath := Source;
  SourceInt := FileAge(SourcePath);

  if (SourceInt = -1) then
  begin
    result := false;
    exit;
  end
  else
    SourceDate := FileDateToDateTime(SourceInt);

  DestinationPath := Target;
  DestinationInt := FileAge(DestinationPath);

  if (DestinationInt = -1) then DestinationDate := 0.00 // equal to 12/31/1899
  else DestinationDate := FileDateToDateTime(DestinationInt);

  if (SourceDate > DestinationDate) then
  begin
    if (CopyFile(PChar(SourcePath), PChar(DestinationPath), false) <> true) then
    begin
      result := false;
    end;
  end;
end;

function GetList(InFileName: string): string;
var MyList: TStringList;
begin
  MyList := nil;
  try
    try
      MyList := TStringList.Create;
      MyList.Clear;

      if not fileexists(InFileName) then
      begin
        raise(exception.Create('GetList err --> No file found by the name of "' + InFileName + '"'));
      end
      else
      begin
        MyList.LoadFromFile(InFileName);
      end;
      result := MyList.Text;
    except on e: exception do
      begin
        raise(exception.Create('GetList err --> ' + e.Message));
      end;
    end;
  finally
    if assigned(MyList) then FreeAndNil(MyList);
  end;
end;

{procedure CloseIDAC(conn: TFDCOnnection);
begin
  try
    Conn.Connected := false;
  except on e: Exception do
    begin
      raise(exception.Create('CloseUp-->' + e.message));
    end;
  end;
end;

procedure ClearIDACParams(conn: TFDCOnnection);
begin
  try
    conn.Params.Clear;
  except on e: Exception do
    begin
      raise(exception.Create('ClearIDACParams-->' + e.message));
    end;
  end;
end;

procedure OpenIDAC(conn: TFDCOnnection);
begin
  try
    conn.Connected := true;
  except on e: Exception do
    begin
      raise(exception.Create('OpenUp-->' + e.message));
    end;
  end;
end;
}
function GetIDACQuery(conn: TFDCOnnection): TFDQuery;
var // remember to free from whence you called
  MyQuery: TFDQuery;
begin
  result := nil;
  if not assigned(conn) then exit;
  try
    MyQuery := TFDQuery.Create(nil);
    MyQuery.Connection := conn;
    result := MyQuery;
  except on e: exception do
    begin
      raise(exception.Create('GetIDACQuery --> ' + e.Message));
    end;
  end;
end;


// add 12/31/2010

procedure SaveCSV_multiFile(DS: TDataSource; sFileName, sHEADER: string; var errStr: string; delim: string = ',');
var // unload to several files serparated at the 50,000 line mark
  SavePlace: TBookMark;
  i, x, LineCount, FileNameModifier, y: integer;
  s, holdFileName, ext: string;
  tx: TextFile;
  sl: TStringList;

  procedure WriteHeaders();
  var ss: string;
    ii: Integer;
  begin
    ss := '';
    ss := sHEADER;
    if ss > '!' then
    begin
      Writeln(tx, ss); // apply header
      Writeln(tx, ''); // write a blank line
    end;

    ss := '';
    with DS, DS.dataset do
    begin
      for ii := 0 to dataset.FieldCount - 1 do
      begin
        if (ii > 0) then SAdd(ss, delim);
        SAdd(ss, fields[ii].Fieldname);
      end;
      Writeln(tx, ss);
    end;
  end;
begin
  if trim(sFILENAME) = '' then
  begin
    errStr := 'SaveCSV_multiFile error --> no file name provided';
    exit;
  end;

  try sl := TStringList.Create(); except; end;

  holdfilename := sFILENAME;
  errStr := '';
  ext := '';
  ext := extractFileExt(holdFilename);
 // showmessage('extention: [' + ext + ']');

  AssignFile(tx, sFILENAME);
  rewrite(tx);
  FileNameModifier := 1;
  x := 0;

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;
    WriteHeaders();
    LineCount := 0;
    begin
      First;
      while not eof do
      begin
        inc(LineCount);
        inc(x);

        if LineCount >= 50000 then
        begin // if the linecount goes above 50,000 then write to a new file
          CloseFile(tx);

          try
            sl.Add(sFILENAME + ' completed. [' + IntToStr(LineCount) + '] lines.');
          except;
          end;


          y := 0;
          y := pos(ext, holdFileName);

          if y > 0 then
          begin
            sFILENAME := copy(holdFileName, 1, y - 1);
            sFILENAME := sFILENAME + '_' + IntTOStr(FileNameModifier) + ext;
            // showmessage('new filename: [' + sFILENAME + ']');
          end
          else // catch all jic re-naming fails
            sFILENAME := holdFileName + '_' + IntToStr(FileNameModifier);

          inc(FileNameModifier);
          AssignFile(tx, sFILENAME);
          rewrite(tx);
          WriteHeaders();
          LineCount := 0;
        end;

        s := '';
        for i := 0 to dataset.FieldCount - 1 do
        begin
          case fields[i].DataType of
            ftBlob, ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes, ftADT, ftArray,
              ftReference, ftDataSet:
              begin
                if (i > 0) then SAdd(s, delim);
                SAdd(s, 'BLOB');
              end;
            ftString:
              begin
                if (i > 0) then SAdd(s, delim);
                SAdd(s, '"' + fields[i].AsString + '"');
              end
          else
            begin
              if (i > 0) then SAdd(s, delim);
              SAdd(s, fields[i].AsString);
            end;
          end;
        end;
        Writeln(tx, s);
        next;
      end;
    end;

    CloseFile(tx);

    try
      sl.Add(sFILENAME + ' completed. [' + IntToStr(LineCount) + '] lines.');
      sl.Add('done CSV. Total lines: ' + IntToStr(x));
    except;
    end;

    GotoBookMark(SavePlace);
  finally
    //dataset.enablecontrols
    errstr := sl.Text;
    dataset.enablecontrols;
    if assigned(sl) then FreeAndNil(sl);
  end;
end;

procedure ExportDataSetToCVS(DataSet: TDataSet; const FileName: string);
var
  fld: TField;
  lst: TStringList;
  wasActive: Boolean;
  writer: TTextWriter;
begin
  writer := TStreamWriter.Create(FileName);
  try
    lst := TStringList.Create;
    try
      lst.QuoteChar := '"';
      lst.Delimiter := ';';
      wasActive := DataSet.Active;
      try
        DataSet.Active := true;
        DataSet.GetFieldNames(lst);
        writer.WriteLine(lst.DelimitedText);
        DataSet.First;
        while not DataSet.Eof do begin
          lst.Clear;
          for fld in DataSet.Fields do
            lst.Add(fld.Text);
          writer.WriteLine(lst.DelimitedText);
          DataSet.Next;
        end;
      finally
        DataSet.Active := wasActive;
      end;
    finally
      lst.Free;
    end;
  finally
    writer.Free;
  end;
end;


procedure SaveCSV(DS: TDataSource; sFileName, sHEADER: string; delim: string = ',');
var
  SavePlace: TBookMark;
  i: integer;
  s, temp: string;
  tx: TextFile;
begin
  if trim(sFILENAME) = '' then exit;
  AssignFile(tx, sFILENAME);
  rewrite(tx);

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;

    s := '';
    s := sHEADER;
    if s > '!' then
    begin
      Writeln(tx, s); // apply header
      Writeln(tx, ''); // write a blank line
    end;

    s := '';
    for i := 0 to dataset.FieldCount - 1 do
    begin
      if (i > 0) then S := S + delim;
      S := S + fields[i].Fieldname;
    end;

    Writeln(tx, s);
    begin
      First;
      while not eof do
      begin
        s := '';
        for i := 0 to dataset.FieldCount - 1 {iFieldsToPrint - 1} do
        begin
          case fields[i].DataType of
            ftBlob, ftMemo, ftGraphic, ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes, ftADT, ftArray,
              ftReference, ftDataSet:
              begin
                if (i > 0) then S := S + delim;
                S := S + 'BLOB';
              end;
            ftString:
              begin
                if (i > 0) then S := S + delim;
                S := S + '"' + fields[i].AsString + '"';
              end
          else
            begin
              if (i > 0) then S := S + delim;
              S := S + fields[i].AsString;
            end;
          end;
        end;
        Writeln(tx, s);
        next;
      end;
    end;
    CloseFile(tx);
    GotoBookMark(SavePlace);
  finally
    dataset.enablecontrols
  end;
end;

procedure GetCSV(DS: TDataSource; var InList: TStringList; delim: string = ',');
var
  SavePlace: TBookMark;
  i: integer;
  s: string;
begin
  if (InList = nil) or (DS = nil) then exit;
  InList.Clear;

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;
    s := '';

    for i := 0 to Fields.Count - 1 do
    begin
      if (i > 0) then s := s + delim;
      s := s + fields[i].Fieldname;
    end;

    InList.Add(s);
    begin
      First;
      while not eof do
      begin
        s := '';
        for i := 0 to Fields.Count - 1 do
        begin
          case fields[i].DataType of
            ftBlob, ftMemo, ftGraphic,
              ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes,
              ftADT, ftArray,
              ftReference,
              ftDataSet:
              begin
                if (i > 0) then s := s + delim;
                s := s + 'BLOB';
              end;
            ftString:
              begin
                if (i > 0) then s := s + delim;
                s := s + '"' + fields[i].AsString + '"';
              end
          else
            begin
              if (i > 0) then s := s + delim;
              s := s + fields[i].AsString;
            end;
          end; // case
        end; // for

        InList.Add(s);
        next;
      end; // while
    end; // with do
    GotoBookMark(SavePlace);
  finally
    dataset.enablecontrols
  end;
end;


procedure SaveDataSetToHtmlFile(DS: TDataSource; InHeader, InDataName, sFILENAME: string; MaxFields: integer);
var
  SavePlace: TBookMark;
  i: integer;
  s, s2, temp, temp2: string;
  tx: TextFile;
begin
  if sFILENAME = '' then exit;
  AssignFile(TX, sFilename);
  rewrite(tx);

  if (MaxFields > DS.DataSet.FieldCount - 1) then
    MaxFields := DS.DataSet.FieldCount - 1;

  if (DS = nil) then exit;
  if InHeader = '' then InHeader := 'DataSet';
  if InDataName = '' then InDataName := 'DataSetName';

  s := '<html>';
  SAdd(s, '<h3>' + InHeader + ' DataSetName="' + InDataName + '" TimeStamp="' + FormatDateTime('mmmm/dd/yyyy : hh:mm AM/PM ', now) + '</h3>');

  Writeln(tx, s);
  s := '';
  s2 := '';

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;
    SAdd(s, '<table border="1" bordercolor="#000000">'); // field Definitions start tag

    for i := 0 to MaxFields do
    begin
      temp := '';
      case fields[i].DataType of
        ftUnknown: temp := 'unknown';
        ftSmallint: temp := 'int-16';
        ftInteger: temp := 'int-32';
        ftWord: temp := 'unsigned int-16';
        ftBoolean: temp := 'boolean';
        ftFloat: temp := 'float';
        ftCurrency: temp := 'money';
        ftString: temp := 'string';
        ftDate: temp := 'date';
        ftTime: temp := 'time';
        ftDateTime: temp := 'datetime';
        ftBCD: temp := 'bcd';
        ftBytes: temp := 'fixed bytes';
        ftVarBytes: temp := 'variable bytes';
        ftAutoInc: temp := 'autoinc int-32';
        ftBlob: temp := 'BLOB';
        ftMemo: temp := 'text memo';
        ftGraphic: temp := 'bitmap';
        ftFmtMemo: temp := 'fmt memo';
        ftParadoxOle: temp := 'paradox ole';
        ftDBaseOle: temp := 'dbase ole';
        ftTypedBinary: temp := 'typed binary';
        ftCursor: temp := 'oracle output cursor';
        ftFixedChar: temp := 'fixed char';
        ftWideString: temp := 'wide string';
        ftLargeint: temp := 'large int';
        ftADT: temp := 'abstract data type';
        ftArray: temp := 'array';
        ftReference: temp := 'reference';
        ftDataSet: temp := 'dataset';
        ftVariant: temp := 'variant';
        ftInterface: temp := 'interface ref';
        ftIDispatch: temp := 'dispatch ref';
        ftGuid: temp := 'guid';
        ftFMTBcd: temp := 'binary decimal';
      else
        temp := 'unknown';
      end;
      SAdd(s, '<td><b>' + fields[i].Fieldname + '</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>');
      SAdd(s2, '<td><b>' + temp + '</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>');
    end; // case

    SAdd(s, '</tr>');
    SAdd(s2, '</tr>');
    system.Insert('<tr>', s, -1);
    system.Insert('<tr>', s2, -1);
    Writeln(tx, s);
    Writeln(tx, s2); s := '';
    s2 := '';

    First;
    while not eof do
    begin
      SAdd(s, '<tr>');
      for i := 0 to MaxFields do
      begin
        case fields[i].DataType of
          ftBlob, ftMemo, ftGraphic,
            ftFmtMemo, ftParadoxOle, ftDBaseOle,
            ftTypedBinary, ftCursor, ftVarBytes,
            ftADT, ftArray,
            ftReference,
            ftDataSet:
            begin
              SAdd(s, '<td>' + 'BLOB' + '</td>');
            end;
          ftString:
            begin
              temp2 := fields[i].AsString;
              if trim(temp2) = '' then temp2 := 'No_Data';
              SAdd(s, '<td>' + temp2 + '</td>');
            end
        else
          begin
            temp2 := fields[i].AsString;
            if trim(temp2) = '' then temp2 := 'No_Data';
            SAdd(s, '<td>' + temp2 + '</td>');
          end;
        end;
      end;

      SAdd(s, '</tr>');
      Writeln(tx, s);
      s := '';
      next;
    end;

    SAdd(s, '</table></html>');
    Writeln(tx, s);
    s := '';
    CloseFile(tx);
    GotoBookMark(SavePlace);
  finally
    dataset.enablecontrols
  end;
end;


     {
FUnction Get_ADO_ConnStr(alias: string): String;
var sWhereTOGo: string;
  ADOlist: TStringList;

  // ASSUMES THIS HAS BEEN CALLED PRIOR: GetSupportingFiles('\\fileserver\UPDATE\ini\ado_c.ini', 'c:\ae\ado_c.ini');
  // call w/ try, except to catch errors

begin
  result := '';

  if (trim(ALIAS) = '') then
  begin
    raise(exception.Create('Get_ADO_ConnStr: no alias passed in.'));
    exit;
  end;

  if not (FileExists('C:\AE\ado_c.ini')) then
  begin
    raise(exception.Create('Get_ADO_ConnStr: no ado_ini file on client..'));
    exit;
  end;

  sWhereToGo := UpperCase(trim(ALIAS));
  ADOlist := nil;

  try

    ADOlist := TStringList.Create;
    try
      ADOlist.Clear;
      GetInMemList(ADOlist, 'C:\AE\ado_c.ini'); // retreive & decrypt  a file
      // showmessage(ADOlist.Text);
      Result := ADOList.Values[ALIAS];
    except on e: exception do
      begin
        raise(exception.create('Get_ADO_ConnStr :: failed to get connection string: --> ' + e.message));
      end;
    end;

  finally
    if assigned(ADOList) then
    try
      ADOList.Free;
    except;
    end;
  end;
end;     }
{original AECommonFunc_II implementation ends here}

{JMClasses implementation starts here}
function NumStripStr(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      if IsDigit(InStr[I]) then
      begin
        Temp := Temp + InStr[I];
      end;
    end;
  end;
  Result := temp;
end;


function BrowseForFolder(Handle: HWND {form1.handle}): string;
var
  BrowseInfo: TBrowseInfo; //browse info structure for the API function call
  PIDL: PItemIDList; //a PIDL, storage method for paths used by Shell
  SelectedPath: array[0..MAX_PATH] of Char; //the buffer where the result will be returned
begin
  Result := '';

  // initialize TBrowseInfo structure to nulls (0)
  FillChar(BrowseInfo, SizeOf(BrowseInfo), #0);
  BrowseInfo.hwndOwner := Handle; // Form1.Handle: default
  BrowseInfo.pszDisplayName := @SelectedPath[0]; // buffer address for API to store result
  BrowseInfo.lpszTitle := 'Select a folder';
  BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS; {only file system folders}

  // show the folder browser and return the result to PIDL itemlist
  PIDL := SHBrowseForFolder(BrowseInfo);

  // get selected directory from the itemlist and include the full path
  if Assigned(PIDL) then
    if SHGetPathFromIDList(PIDL, SelectedPath) then
      Result := string(SelectedPath);
end;


function StripTags(var S: string): boolean;
var TagBegin, TagEnd, TagLength: integer;
begin // if StripTags(ass) then showmessage('stop injecting scripts into my dll!');
  result := false;
  TagBegin := 0;
  TagBegin := Pos('<', S); // search position of first <
  if TagBegin > 0 then result := true; // found a tag

  while (TagBegin > 0) do begin // while there is a < in S
    TagEnd := Pos('>', S); // find the matching >
    TagLength := TagEnd - TagBegin + 1;
    Delete(S, TagBegin, TagLength); // delete the tag
    TagBegin := Pos('<', S); // search for next <
  end;
end;

function TextToBinary(InText: string): string;
var temp, temp2: string;
  i, x: integer;
begin
  temp := inText;
  temp2 := '';
  x := length(temp);
  for i := 1 to x do
  begin
    //stradd(temp2, CharToBinStr(temp[i]));
  end;
  result := temp2;
end;

function BinaryToText(InBinary: string): string;
var temp, temp2: string;
  i, x: integer;
  MyList: TStringList;
begin
  try
    mylist := TStringList.create;
    temp := InBinary;
    temp := StripBinaryStr(temp);
    temp2 := '';

    x := length(temp);
    for i := 1 to x do
    begin
      if i mod (8) = 0 then
        MyList.add(copy(temp, i - 7, 8));
    end;

    for i := 0 to MyList.Count - 1 do
   //   StrAdd(temp2, BinStrTOChar(MyList[i]));

    temp2 := trim(temp2);
    result := temp2;
  finally
    MyList.Free;
  end;
end;

function IsRemoteSession: Boolean;
const
  sm_RemoteSession = $1000; { from WinUser.h }
begin
  Result := GetSystemMetrics(sm_RemoteSession) <> 0;
end;


procedure jShuffle(var InStrings: TStrings);
var
  Temp: string;
  x, i, c, r1, r2: integer;
begin
  Randomize;
  x := InStrings.Count;
  c := x + 100;

  for i := 0 to c do
  begin
    r1 := Random(x);
    r2 := Random(x);

    if r1 <> r2 then
    begin
      temp := trim(InStrings[r1]);
      InStrings[r1] := InStrings[r2];
      InStrings[r2] := Temp;
    end;
  end;
end;

function GetTimeStr(InTime: TDateTime): string;
begin
  result := '00:00';
  try
    result := FormatDateTime('hh:mm', InTime);
  except;
  end;
end;

function substrdelim(string_in, delim: string; stringindex: integer): string;
var // dan's hot substring func -
    // return a substring from an index pos in a delimited string
  indx, dpos, epos: integer;
  tmpstring: string;
begin
  tmpstring := string_in;
  if (length(delim) > 0) and (delim <> ' ') then
    delim := trim(delim);
  tmpstring := trim(tmpstring) + delim;
  indx := 0;
  dpos := 0;
  while (indx < stringindex) do
  begin
    if (dpos > 0) then
      delete(tmpstring, 1, dpos);
    dpos := pos(delim, tmpstring);
    if (dpos <> -1) then
    begin
      inc(indx);
      continue;
    end;
    RESULT := '';
    exit;
  end;
  epos := dpos - 1;
  if (epos = 0) then
    RESULT := ''
  else
    RESULT := copy(tmpstring, 1, epos);
  exit;
end;

function j_GetFileType(Path: string): string;
var Info: TSHFileInfo;
begin
  SHGetFileInfo(PChar(Path), 0, Info, SizeOf(Info), SHGFI_TYPENAME);
  Result := Info.szTypeName;
end;

function j_GetGenericFileType(Path: string): string;
var Info: TSHFileInfo;
begin
  SHGetFileInfo(PChar(Path), FILE_ATTRIBUTE_NORMAL, Info, SizeOf(Info),
    SHGFI_TYPENAME or SHGFI_USEFILEATTRIBUTES);
  Result := Info.szTypeName;
end;

function SafeSaveLogFile(threadCSection: TCriticalSection; sFILENAME, InString: string): Boolean;
begin
  if (threadCSection = nil) or
    (trim(sFILENAME) = '') or
    (trim(InString) = '') then
  begin
    result := false;
    exit;
  end
  else result := true;

  try
    try
      threadCSection.Acquire; { lock out other threads }
      SaveLogFile(sFILENAME, InString);
    except on e: exception do
      begin
        result := false;
        raise(Exception.Create(e.message));
      end;
    end;
  finally
    threadCSection.Release;
  end;
end;

function Get_FileExtension(InFileName: string): string;
var // returns the type of file
    // InFileName expects the full path of the file.
  strings: TStrings;
  theList: TStringList;
  i: Integer;
begin
  result := '';

  i := 0;
  if not (InFileName > '!') then exit; // no string sent in
  i := pos('.', InFileName);
  if i = 0 then exit; // there is no extension assuming there is no period in the file name too ..

  theList := TStringList.Create;
  if theList = nil then exit;

  try
    try
      theList.Text := InFileName;
      strings := AnsiSplitStrings(InFileName, '.', theList); // take the last one
      result := strings[strings.count - 1];
    except on e: exception do
      begin
        raise(exception.create('GetFileExtention error --> ' + e.message));
      end;
    end;
  finally
    theList.Free;
  end;
end;

function GetBetweenString(InStr, InDelim1, InDelim2: string): string;
var i: Integer; // return a string between the two specified delimiters
  temp: string;
begin
  result := '';
  if (inDelim1 = '') or (inDelim2 = '') or (InStr = '') then exit;
  temp := '';

  i := pos(InDelim1, InStr);
  if i > 0 then temp := copy(InStr, i + length(InDelim1), length(InStr))
  else exit;

  i := 0;
  i := pos(InDelim2, temp);

// if i > 0 then temp := copy(temp, 0, i - 1)
//  else exit;

  if i > 0 then temp := copy(temp, 0, i - Length(InDelim2))
  else exit;

  result := temp;
end;

function qs(s: string): string;
var p: integer;
   // replace any single or double quotes or ` chars in text with spaces
   // then wrap s in quotes
begin
  if (length(trim(s)) = 0) then
  begin
    result := '"NULL"';
  end
  else
  begin
    // #39 = ' (singleQuote)
    // #34 = " (doubleQuote)
    // #96 = ` (backSlant SingleQuote)
    p := pos(#39, s);
    while (p > 0) do
    begin // ' replace singleQuotes with spaces
      s[p] := ' ';
      p := pos(#39, s);
    end;

    p := pos(#34, s);
    while (p > 0) do
    begin // " replace doubleQuotes with spaces
      s[p] := ' ';
      p := pos(#34, s);
    end;

    p := pos(#96, s);
    while (p > 0) do
    begin // ` replace backslanted singlQuotes with spaces
      s[p] := ' ';
      p := pos(#96, s);
    end;

    result := #34 + s + #34; // double quote
  end;
end;

function StringToCaseSelect(Selector: string; CaseList: array of string): Integer;
var cnt: integer;
begin
  Result := -1;
  for cnt := 0 to Length(CaseList) - 1 do
  begin
    if CompareText(Selector, CaseList[cnt]) = 0 then
    begin
      Result := cnt;
      Break;
    end;
  end;
end;
{
Example of usage

CreditCardName := 'Unknown';
case StringToCaseSelect(thisBilling.cc_type,
        ['VI', 'CA', 'AX', 'DS', 'DC', 'DI']) of
        0: CreditCardName := 'Visa';
        1: CreditCardName := 'MasterCard';
        2: CreditCardName := 'American Express';
        3: CreditCardName := 'Discover';
        4: CreditCardName := 'Diners';
        5: CreditCardName := 'Discover'; else CreditCardName := 'Unknown'; end;
}

function IPMatchMask(IP, Mask: string): Boolean;
{
check who is connecting to the server.
Like IRC this uses a similar masking technique.

useage:
if not (IPMatchMask(Socket.LocalAddress,'212.*.*.5')) then Socket.Close;
this will terminate anyone trying to connect to the server who doesn't have the IP of 212.*.*.5
}
  procedure SplitDelimitedText(Text: string; Delimiter: string; Results: TStrings);
  var TheWord, CommaText: string;
  begin
    Results.Clear;
    CommaText := Text + Delimiter;
    while (Pos(Delimiter, CommaText) > 0) do
    begin
      TheWord := Copy(CommaText, 1, Pos(Delimiter, CommaText) - 1);
      CommaText := Copy(CommaText, Pos(Delimiter, CommaText) +
        Length(Delimiter), Length(CommaText));
      Results.Add(TheWord);
    end;
  end;

var
  IPs, Masks: TStringList;
  i: Integer;
begin
  IPs := TStringList.Create;
  try
    Masks := TStringList.Create;
    try
      SplitDelimitedText(LowerCase(IP), '.', IPs);
      SplitDelimitedText(LowerCase(Mask), '.', Masks);
      Result := True;

      if IPs.Count <> Masks.Count then
      begin
        Result := False;
        Exit;
      end;
      for i := 0 to IPs.Count - 1 do
      begin
        if (IPs.Strings[I] <> Masks.Strings[I]) and (Masks.Strings[I] <> '*') then Result := False;
      end;
    finally
      Masks.Free;
    end;
  finally
    IPs.Free;
  end;
end;

function jShellEXE(InFullPath: string): string;
var i: integer;
  command, param1: string;
begin
  result := '';
  command := '';
  param1 := '';

  i := pos(' ', InFullPath); // <--- separate a single parameter, if needed
  if i > 0 then
  begin
    command := trim(copy(InFullPath, 1, i - 1)); // first arg
    param1 := trim(copy(InFullPath, i + 1, length(InFullPath))); // sencond arg
  end
  else command := InFullPath;


  {
  SW_HIDE
Hides the window and activates another window.

SW_MAXIMIZE
Maximizes the specified window.

SW_MINIMIZE
Minimizes the specified window and activates the next top-level window in the z-order.

SW_RESTORE
Activates and displays the window. If the window is minimized or maximized,
Windows restores it to its original size and position. An application should
specify this flag when restoring a minimized window.

SW_SHOW
Activates the window and displays it in its current size and position.

SW_SHOWDEFAULT
Sets the show state based on the SW_ flag specified in the STARTUPINFO structure
passed to the CreateProcess function by the program that started the application.
An application should call ShowWindow with this flag to set the initial show state
of its main window.

SW_SHOWMAXIMIZED
Activates the window and displays it as a maximized window.

SW_SHOWMINIMIZED
Activates the window and displays it as a minimized window.

SW_SHOWMINNOACTIVE
Displays the window as a minimized window. The active window remains active.

SW_SHOWNA
Displays the window in its current state. The active window remains active.

SW_SHOWNOACTIVATE
Displays a window in its most recent size and position. The active window remains active.

SW_SHOWNORMAL
Activates and displays a window. If the window is minimized or maximized,
Windows restores it to its original size and position.
An application should specify this flag when displaying the window for the first time.
 }



  i := 0;
  i := ShellExecute(0, 'open', PCHAR(command), PCHAR(param1), nil, SW_HIDE);
  if i > 32 then Result := 'OK'
  else
  begin
    case i of
      ERROR_FILE_NOT_FOUND: Result := 'The specified file was not found.';
      ERROR_PATH_NOT_FOUND: Result := 'The specified path was not found.';
      ERROR_BAD_FORMAT: Result := 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image).';
      SE_ERR_ACCESSDENIED: Result := 'he operating system denied access to the specified file.';
      SE_ERR_ASSOCINCOMPLETE: Result := 'The filename association is incomplete or invalid. ';
      SE_ERR_DDEBUSY: Result := 'The DDE transaction could not be completed because other DDE transactions were being processed.';
      SE_ERR_DDEFAIL: Result := 'The DDE transaction failed.';
      SE_ERR_DDETIMEOUT: Result := 'The DDE transaction could not be completed because the request timed out.';
      SE_ERR_DLLNOTFOUND: Result := 'The specified dynamic-link library was not found.';
      SE_ERR_NOASSOC: Result := 'There is no application associated with the given filename extension.';
      SE_ERR_OOM: Result := 'There was not enough memory to complete the operation.';
      SE_ERR_SHARE: Result := 'A sharing violation occurred. ';
    else ;
    end;
  end;
end;
  {
function MinutesPast(TheDate: Tdate): integer;
const
  SecsPerDay: Integer = 60 * 60 * 24;
  SecsPerHour: Integer = 60 * 60;
  SecsPerMinute: Integer = 60;
  SecsPerSecond: Integer = 1;
var
  TimeDiff, Days, Hours, Minutes, Seconds: Integer; begin
  TimeDiff := SecondsBetween(Now, TheDate);
  if TimeDiff >= 60 then Minutes := TimeDiff div SecsPerminute
  else Minutes := 0;
  Result := Minutes;
end;
  }

function IPMatchFromList(var IPList: TStringList; IP: string): Boolean;
var
  i: integer;

  function xInList(var InList: TStringList; InStr: string): boolean;
  var i: Integer;
  begin
    result := false;
    if not assigned(InList) then exit;
    InStr := UpperCase(trim(InStr));
    for i := 0 to InList.Count - 1 do
    begin
      if uppercase(trim(InList[i])) = InStr then
      begin
        result := true;
        break;
      end;
    end;
  end;
  { call:
    procedure Button1Click(Sender: TObject);
      var IPList: TStringList;
    begin
      try
        IPList := TStringList.Create;
        IPList.Text := memo1.Text; // list of IP's
        if IPMatchFromList(IPList, theActual_IP ) then showmessage('Match!')
        else showmessage('No Match');
      finally
        IPList.Free;
      end;
    end;
  }

begin
  result := false;
  if (IPList = nil) or (trim(IPLIst.Text) = '') or (trim(IP) = '') then exit;

  for i := 0 to IPList.Count - 1 do
  begin
    if xInList(IPList, IP) then // first look for specific IP's
    begin
      result := true;
      break;
    end;

    if IPMatchMask(IP, IPList[i]) then // look for masked IP's
    begin
      result := true;
      break;
    end;
  end;
end;

constructor TSecurity.Create(const constant1, constant2: WORD);
begin
  Fconstant1 := constant1;
  Fconstant2 := constant2
end {Create};

function TSecurity.Encrypt(const s: string; const StartKey: WORD): string;
var
  b: BYTE;
  i: INTEGER;
  key: WORD;
begin
  key := StartKey;
  RESULT := '';
  for i := 1 to LENGTH(s) do
  begin
    b := BYTE(s[i]) xor (key shr 8);
    key := (b + key) * Fconstant1 + Fconstant2;
    RESULT := RESULT + IntToHex(b, 2)
  end
end {Encrypt};

function TSecurity.Decrypt(const s: string; const StartKey: WORD): string;
var
  b: BYTE;
  i: INTEGER;
  key: WORD;
begin
  b := 0; // Avoid compiler initialization warning
  key := StartKey;
  RESULT := '';
  for i := 1 to LENGTH(s) div 2 do
  begin
    try
      b := StrToInt('$' + COPY(s, 2 * i - 1, 2));
    except
      on EConvertError do b := 0
    end;
    RESULT := RESULT + CHAR(b xor (key shr 8));
    key := (b + key) * Fconstant1 + Fconstant2
  end
end {Decrypt};


{
function GetWindowsSysFolder: string;
  procedure StrResetLength(var S: AnsiString);
  begin
    SetLength(S, StrLen(PChar(S)));
  end;
var
  Required: Cardinal;
begin
  Result := '';
  Required := GetSystemDirectory(nil, 0);
  if Required <> 0 then
  begin
    SetLength(Result, Required);
    GetSystemDirectory(PChar(Result), Required);
    StrResetLength(Result);
  end;
end;
 }

function GetRegistryValue(RootKey: HKEY; KeyPath, KeyToRead: string): string;
var Registry: TRegistry;
begin
  result := '';
  if (trim(KeyPath) = '') or (trim(KeyToRead) = '') then exit;
  Registry := TRegistry.Create(KEY_READ); //read only
  try
    try
      Registry.RootKey := RootKey;

      if not (Registry.KeyExists(KeyPath)) then
      begin
        result := KeyPath + ' does not exist';
        exit;
      end;

      Registry.OpenKey(KeyPath, False); // False = don't create key if nil
      //Registry.WriteString('Text', 'ass');
      Result := Registry.ReadString(KeyToRead);
    except on e: exception do
      begin
        result := 'GetRegistryValue -> ' + e.Message;
      end;
    end;
  finally
    Registry.Free;
  end;
end;

function SetRegistryValue(RootKey: HKEY; KeyPath, KeyToSet: string; ValueToSet: string = ''): string;
var Registry: TRegistry;
begin
  result := '';
  if (trim(KeyPath) = '') or (trim(KeyToSet) = '') or (trim(ValueToSet) = '') then exit;
  Registry := TRegistry.Create();
  try
    try
      Registry.RootKey := RootKey;

      if not (Registry.KeyExists(KeyPath)) then
      begin
        result := KeyPath + ' does not exist';
        exit;
      end;
      Registry.OpenKey(KeyPath, False); // False = don't create key if nil
      Registry.WriteString(KeyToSet, ValueToSet);
    except on e: exception do
      begin
        result := 'GetRegistryValue -> ' + e.Message;
      end;
    end;
  finally
    Registry.Free;
  end;
end;

function DeleteRegKeyValue(KeyPath, KeyValue: string): boolean;
var
  Registry: TRegistry;
  thepath, name: string;
  RootKey: HKEY;
begin
  result := true;
  name := KeyValue;
  thepath := KeyPath;
  Registry := TRegistry.Create();
  try
    try
      Registry.RootKey := HKEY_LOCAL_MACHINE;
      if not Registry.OpenKey(thepath, false) then
      begin
        result := false;
        exit;
      end;
      Registry.DeleteValue(KeyValue);
    except on e: exception do
      begin
        result := false;
      end;
    end;
  finally
    Registry.Free;
  end;
end;

function IsInList(InList: TStringList; InStr: string): boolean;
var i: Integer;
begin
  result := false;
  if not assigned(InList) then exit;
  InStr := UpperCase(trim(InStr));

  for i := 0 to InList.Count - 1 do
  begin
    if uppercase(trim(InList[i])) = InStr then
    begin
      result := true;
      break;
    end;
  end;
end;

function IsInStrings(InStrings: TStrings; InStr: string): boolean;
var i: Integer;
begin
  result := false;
  InStr := UpperCase(trim(InStr));
  for i := 0 to InStrings.Count - 1 do
  begin
    if uppercase(trim(InStrings[i])) = InStr then
    begin
      result := true;
      break;
    end;
  end;
end;

function SecondsIdle(): DWord;
var
  liInfo: TLastInputInfo;
  theResult: boolean;
begin
  liInfo.cbSize := SizeOf(TLastInputInfo);
  theResult := GetLastInputInfo(liInfo); // retrieves time of last input event (system level structure).
  if not theResult then result := 0
  else Result := (GetTickCount - liInfo.dwTime) div 1000;
end;

function WinExecAsUser(FileName: string; username: string; password: string; Visibility: integer): string;
var { V1 by Pat Ritchey, V2 by P.Below }
  zAppName: array[0..512] of char;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  h: thandle;
begin { WinExecAndWait32V2 }
  result := 'WinExecAsUser OK - user: ' + username + ': Filename: ' + Filename;
  StrPCopy(zAppName, FileName);
  FillChar(StartupInfo, Sizeof(StartupInfo), #0);
  StartupInfo.cb := Sizeof(StartupInfo);
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := Visibility;
  if not LogonUser(pchar(username), '.', pchar(Password), LOGON32_LOGON_INTERACTIVE,
    LOGON32_PROVIDER_DEFAULT, h) then
    result := 'WinExecAsUser fail --> ' + SysErrorMessage(GetLastError);

  CreateProcessAsUser(h, nil,
    zAppName, { pointer to command line string }
    nil, { pointer to process security attributes }
    nil, { pointer to thread security attributes }
    false, { handle inheritance flag }
    CREATE_NEW_CONSOLE or { creation flags }
    NORMAL_PRIORITY_CLASS,
    nil, { pointer to new environment block }
    nil, { pointer to current directory name }
    StartupInfo, { pointer to STARTUPINFO }
    ProcessInfo); { pointer to PROCESS_INF }

  if GetLastError <> 0 then result := 'WinExecAsUser fail --> ' + SysErrorMessage(GetLastError);
end;

// just save to the same file all the time - don't create one every day

function jSaveFile(sFULLPATH, sFILENAME, sSUFFIX, InString: string): Boolean;
var
  t: textfile;
  MyList: TStringList;
  //  jSaveFile('\\fileserver\chaoslog\', 'dss_run_log', '.txt', someStr + DateTImeTOStr(now));
begin
  result := true;
                            // c:\CLog
  if not (SetIfNotCreateDirectory(sFULLPATH)) or (trim(sFULLPATH) = '') then
  begin
    result := false;
    exit;
  end;

  sFULLPATH := sFULLPATH + sFILENAME + sSUFFIX;

  if (FileExists(sFULLPATH)) then
  begin
    try
      assignfile(t, sFULLPATH);
      try
        append(t);
      except
        begin
          rewrite(t);
          result := false;
        end;
      end;
      writeln(t, InString);
      flush(t);
    finally
      try
        closeFile(t);
      except
        result := false;
      end;
    end;
  end
  else
  begin
    try
      try
        MyList := TStringList.Create;
      except
        begin
          result := false;
          exit;
        end;
      end;
      MyList.Clear;
      MyList.Add(InString);
      MyList.SaveToFile(sFULLPATH);
    finally
      MyList.Free;
    end;
  end;
end;

function FormatAPIMessage(Error: DWORD): AnsiString;
var
  Buff: {PWIdeCHar; } array[0..2048] of Char;
  i: DWORD;
begin
 {  useage :
    ShowMessage( 'There has been an Windows Error: ' + FormatAPIMessage( GetLastError) );
 }
  i := FormatMessage(
    FORMAT_MESSAGE_IGNORE_INSERTS +
    FORMAT_MESSAGE_FROM_SYSTEM,
    nil, // pointer to  message source
    Error, // requested message identifier
    0, // language identifier for requested message
    buff, // pointer to message buffer
    2024, // maximum size of message buffer
    nil); // address of array of message inserts

  if (i > 0) then Result := Buff
  else Result := inttostr(GetLastError);
end;

function jGetError(): string;
begin
  // Called if an error occurs during initialization
  // The try/finally construct is used to get the correct value from
  // GetLastError in case a call in Cleanup fails.
  result := '';
  try
    result := FORMAT('%s, Error #%d', [result, GetLastError]);
  finally
   // Cleanup;
  end;
end;


procedure jSLEEP(InMilliseconds: integer = 0);
begin
  windows.Sleep(InMilliseconds);
end;

function LeftPadL(const S: AnsiString; Len: Cardinal): AnsiString;
  {-Pad a string on the left with spaces.}
begin
  Result := LeftPadChL(S, ' ', Len);
end;

function LeftPadChL(const S: AnsiString; C: AnsiChar; Len: Cardinal): AnsiString;
  {-Pad a string on the left with a specified character.}
begin
  if Length(S) >= LongInt(Len) then {!!.D4}
    Result := S
  else if Length(S) < MaxLongInt then begin
    SetLength(Result, Len);
    Move(S[1], Result[Succ(Word(Len)) - Length(S)], Length(S));
    FillChar(Result[1], LongInt(Len) - Length(S), C); {!!.D4}
  end;
end;

procedure StrAdd(var TargetString: string; StringToAdd: string);
var // use this instead of string := String + string - Isfast = true
  i, j: integer;
begin
  // procedure Move(var Source: Type; var Dest: Type; Count: Integer);
  // Move copies Count bytes from Source to Dest. No range checking is performed.
  // Move compensates for overlaps between the source and destination blocks.
  // Whenever possible, use the global SizeOf function (Delphi) or the sizeof operator (C++)
  // to determine the count.
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end;

procedure StrInsert(Substr: string; var Dest: string; Index: Integer);
begin // insert a substing into a string
  // add to front : strInsert(StrToInsert, temp, -1);
  // add to 5 pos : strInsert(StrToInsert, temp, 5);
  system.Insert(Substr, Dest, Index);
end;

function jSetPathForCurrentSession(PathToAdd: string): Boolean;
const ENV_VAR_BUFF_SIZE = $4000;
var i: boolean;
  OldPath: string;
begin
  result := false;
  i := false;
  SetLength(OldPath, ENV_VAR_BUFF_SIZE);
  SetLength(OldPath, GetEnvironmentVariable('PATH', PChar(OldPath), ENV_VAR_BUFF_SIZE));
  i := SetEnvironmentVariable('PATH', PChar(PathToAdd + OldPath));
  if i then result := true;
end;

function jGetList(InFileName: string): string;
var
  MyList: TStringList;
begin
  try
    try
      MyList := TStringList.Create;
      MyList.Clear;

      if not fileexists(InFileName) then
      begin
        raise(exception.Create('jGetList err --> No file found by the name of "' + InFileName + '"'));
      end
      else
      begin
        MyList.LoadFromFile(InFileName);
      end;
      result := MyList.Text;
    except on e: exception do
      begin
        raise(exception.Create('jGetList err --> ' + e.Message));
      end;
    end;
  finally
    if assigned(Mylist) then FreeAndNil(MyList);
  end;
end;

function jGetSupportingFiles(Source, Target: string): boolean;
var // get re-newed P:\ support files:
  SourcePath, DestinationPath: string;
  SourceInt, DestinationInt: integer;
  SourceDate, DestinationDate: TDateTime;
begin
  if (trim(Source) = '') or (trim(Target) = '') then
  begin
    result := false;
    exit;
  end
  else result := true;

  if not fileexists(Target) then
  begin
    if (CopyFile(PChar(Source), PChar(Target), false) <> true) then
    begin
      result := false;
    end;
    exit;
  end;

  SourcePath := Source;
  SourceInt := FileAge(SourcePath);

  if (SourceInt = -1) then
  begin
    result := false;
    exit;
  end
  else SourceDate := FileDateToDateTime(SourceInt);

  DestinationPath := Target;
  DestinationInt := FileAge(Target);

  if (DestinationInt = -1) then DestinationDate := StrToDateTime('12/31/1899')
  else DestinationDate := FileDateToDateTime(DestinationInt);

  if (SourceDate > DestinationDate) then
  begin
    if (CopyFile(PChar(SourcePath), PChar(DestinationPath), false) <> true) then
    begin
      result := false;
    end;
  end;
end;

function jGetFileSize(FullFilePath: string): LongInt;
var f: file of Byte;
begin
  result := 0;
  if fileexists(FullFilePath) then
  begin
    AssignFile(f, FullFilePath);
    Reset(f);
    try
      result := FileSize(f);
    finally
      CloseFile(f);
    end;
  end;
end;

function DwordToDuration(InMillisecs: DWORD): TDurationRec;
var TimePassed: DWORD; // use to convert TEND- TStart (ms) to days
begin
 // if NewTime > OldTime then TimePassed := NewTime - OldTime // Get TimePassed
  //else TimePassed := OldTime - NewTime; // OldTime is the new time, NewTime is the old
  result.days := 0;
  result.hours := 0;
  result.minutes := 0;
  result.seconds := 0;

  if InMillisecs >= 86400000 then
    Result.days := trunc(InMillisecs / 86400000);

  if result.days > 0 then Timepassed := InMillisecs mod 86400000
  else TimePassed := InMillisecs;

  if TimePassed > 3600000 then
    Result.hours := trunc(TimePassed / 3600000);

  if result.hours > 0 then Timepassed := TimePassed mod 3600000;

  if TimePassed > 60000 then // TimePassed := TimePassed mod 60000;
    Result.minutes := trunc(TimePassed / 60000); // Minutes without hours

  if result.minutes > 0 then Timepassed := TimePassed mod 60000;

  Result.seconds := trunc(TimePassed / 1000);
end;

function GetDuration(NewTime, OldTime: TDateTime): TDurationRec;
var
  TimePassed: TDateTime;
begin
  result.seconds := 0;
  result.hours := 0;
  result.minutes := 0;
  result.days := 0;


  if NewTime > OldTime then TimePassed := NewTime - OldTime // Get TimePassed
  else TimePassed := OldTime - NewTime; // OldTime is the new time, NewTime is the old

  Result.days := trunc(TimePassed); // Days -> integer part of time
  TimePassed := TimePassed - Result.days;
  Result.hours := trunc(TimePassed * 24); // Hours without days

  TimePassed := TimePassed * 24 - Result.hours;
  Result.minutes := trunc(TimePassed * 60); // Minutes without hours

  TimePassed := TimePassed * 60 - Result.minutes;
  Result.seconds := trunc(TimePassed * 60); // Seconds without minutes
end;

function JHash(s: string; ARRAY_SIZE: LongWord): LongWord; // same as unsigned
var i, x: integer; // hash func for string passwords etc
begin
  result := 0;
  x := length(s);
  for i := 1 to x do
    result := result xor (result shl 5) + (ord(s[i]));

  result := result mod ARRAY_SIZE;
end;

function IsAtSign(InStr: string): boolean;
var i, x: integer;
begin
  result := false;
  x := length(InStr);
  for i := 1 to x do
  begin
    case InStr[i] of
      '@': begin result := true; break; end;
    else ;
    end;
  end;
end;

function jPage(HeadScripts, sPAGETITLE, sBodyTitleText, sLEFTLINKS, sBODY: string;
  colorBACK: string = '#ffffff'; colorCENTER: string = '#F8F8F8';
  colorMAIN: string = '#c0c0c0'): string;
var // returns canned html page formatted with tables
  Page: TStringList;
  temp: string;
begin
  try
    Page := TStringList.Create;
  except on e: exception do
    begin
      result := 'Can''t create page. ERR: ' + e.Message;
      exit;
    end;
  end;

  if trim(colorBACK) = '' then colorBACK := '#808080';
  if trim(colorCENTER) = '' then colorCENTER := '#E3E2B0';
  if trim(colorMAIN) = '' then colorMAIN := '#c0c0c0';

  try
    with Page do
    begin
      add('<HTML>');
      add('<head>');

      add('<script language="javascript" type="text/javascript">');
      add('function TruncText(field,  maxlimit) {');
      add('if (field.value.length > maxlimit)');
      add('field.value = field.value.substring(0, maxlimit);');
      add('}');
      add('</SCRIPT>');

      if trim(headscripts) > '!' then add(headscripts);

      add('<TITLE>' + sPAGETITLE + '</TITLE>');
      add('</head>');

      add('<a name="top"></a>');
      add('<body bgcolor="' + colorBACK + '" link="#000000" vlink="#0000ff" alink="#008000">');
      add('<br>');

      add('<center><font size="+3">' + sPAGETITLE + '</font></center><br>');

      add('<table width="100%" cellspacing="0" cellpadding="1" border="0">');
      add('<tr>');
      add('	<td width="5%" bgcolor="' + colorBACK + '">&nbsp;</td>');
      add('	<td width="90%" bgcolor="#000000" valign="top">');

      add('		<!-- inner main table -->');
      add('		<table width="100%" cellspacing="0" cellpadding="4" bgcolor="' + colorCENTER + '" border="0">');
      add('			<tr>');
      add('			  <!--start left links here  -->');
      add('				<td width="15%" valign="top">');

      add('					<table width="100%" cellspacing="0" cellpadding="4" border="0">');
      add('						<!--left side links start here-->');

      add(sLEFTLINKS);
      add('					</table>');

      add('				</td>');
      add('				<!-- center start-->');
      add('				<td align="left" valign="top">');
      add('					<!-- "black border" table that surrounds the main center box -->');
      add('					<table width="100%" cellspacing="0" cellpadding="1" border="0" bgcolor="#000000">');
      add('						<tr>');
      add('							<td>');
      add('								<table width="100%" cellspacing="0" cellpadding="20" bgcolor="' + colorMAIN + '" border="0">');
      add('								   <!-- Put all your center crap here -->');
      add('									<tr>');
      add('										<td>');
      add('											<font color="#0000ff" face="Fixedsys">');
      add('											<p>');

      // middle header
      add('												' + sBodyTitleText + '<br>');
      add('											</p>');
      add('											</font>');
      add('											<p>');

      // add all body text here
      add(sBODY);
      add('                     </p>');
      add('										</td>');
      add('									</tr>');
      add('								</table> <!-- Put all your center crap here -->');
      add('							</td>');
      add('						</tr>');
      add('					</table>	<!-- end outer black border table-->');
      add('				</td>');
      add('			</tr>');
      add('		</table>');
      add('	</td> <!-- end 90% center -->');
      add('	<td width="5%" bgcolor="' + colorBACK + '">&nbsp;</td>');
      add('</tr>');
      add('</table>');
      add('<Br>' + lameCWStr);


      add('<!-- <a href="#top"><font size="-2" face="arial,helvetica,Fixedsys">Back to Top</font></a><br><br>');
      add('-->');
      add('</body>');
      add('</HTML>');
    end;
    result := Page.text;
  finally
    Page.Free;
  end;
end;

function GetGUIDString(): string;
var gGUID: TGUID;
begin
  try
    //CoInitialize(nil);
    // The skill of gymnastics, the kill of karate
    CoInitializeEx(nil, COINIT_SPEED_OVER_MEMORY);
    CoCreateGuid(gGUID);
    result := (GUIDToString(gGUID));
    CoUnInitialize;
  except on e: exception do
    begin
      result := ('[err: ' + e.Message + ']');
    end;
  end;
end;

function KiloToPound(Kilo: real): real; stdcall;
begin
  result := Kilo * 2.206349;
end;

function MilliliterToFluidOZ(MIL: real): real; stdcall;
var
  t: real;
begin
  t := MIL * 0.03381402;
  result := t;
end;

procedure GetMemoryStatus(var AvailPhys,
  TotalPhys,
  PercentUsed,
  TotalPageFile,
  pageFileFree,
  userBytesOfAddressSpace,
  freeUserBytes
  : string);
var
  MemInfo: TMemoryStatus;
begin
  MemInfo.dwLength := SizeOf(MemInfo);
  GlobalMemoryStatus(MemInfo);
  PercentUsed := FormatFloat('###.00%', MemInfo.dwMemoryLoad);
  TotalPhys := IntToStr(MemInfo.dwTotalPhys);
  AvailPhys := IntToStr(MemInfo.dwAvailPhys); // Free physical memory in bytes
  TotalPageFile := IntToStr(MemInfo.dwTotalPageFile); // total bytes
  PageFileFree := IntToStr(MemInfo.dwAvailPageFile);
  UserBytesOfAddressSpace := IntToStr(MemInfo.dwTotalVirtual);
  FreeUserBytes := IntToStr(MemInfo.dwAvailVirtual);
end;

function SetDirectoryForTSUser(): string;
var dir: string;
begin
  result := '';
  dir := 'c:\ae\_' + trim(GetTheUserName()) + '\';
  forcedirectories(dir);
  result := dir;
end;

function GetDirectoryForTSUser(): string;
begin
  result := 'c:\ae\_' + trim(GetTheUserName()) + '\';
end;

function ConcatDateTime(dINDATE: TDate; tINTIME: TTime): string;
var
  fTEMP: real;
begin
  try
    fTEMP := trunc(dINDATE) + frac(tINTIME);
    result := DateTimeToStr(fTEMP);
  except
    result := '';
  end;
end;

function ConcatDateTime2(dINDATE: TDate; tINTIME: TTime): TDateTime;
var
  fTEMP: real;
begin
  try
    fTEMP := trunc(dINDATE) + frac(tINTIME);
    result := (fTEMP);
  except
    result := 0;
  end;
end;

function Reboot_XP(DoWhat: Word): Boolean;
var
  OSInfo: TOSVersionInfo;
  TokenPriv: TTokenPrivileges;
  TokenHandle: THandle;
  PreviousState: TTokenPrivileges;
  ReturnLength: DWORD;
begin
  Result := False;
  //if (DoWhat < 0) or (DoWhat > 4) then DoWhat := EWX_REBOOT;

  OSInfo.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  if not GetVersionEx(OSInfo) then Exit;
  case OSInfo.dwPlatformId of
    VER_PLATFORM_WIN32_WINDOWS: Result := ExitWindowsEx(EWX_REBOOT, 0);
    VER_PLATFORM_WIN32_NT:
      begin
        if OpenProcessToken(GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, TokenHandle) then
        begin
          if LookupPrivilegeValue(nil, 'SeShutdownPrivilege', TokenPriv.Privileges[0].LUID) then
          begin
            TokenPriv.PrivilegeCount := 1;
            TokenPriv.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
            if AdjustTokenPrivileges(TokenHandle, False, TokenPriv, SizeOf(TTokenPrivileges), PreviousState, ReturnLength) then
              {
                EWX_FORCE	Forces processes to terminate. When this flag is set, Windows does not send the messages WM_QUERYENDSESSION and WM_ENDSESSION to the applications currently running in the system. This can cause the applications to lose data. Therefore, you should only use this flag in an emergency.
                EWX_LOGOFF	Shuts down all processes running in the security context of the process that called the ExitWindowsEx function. Then it logs the user off.
                EWX_POWEROFF	Shuts down the system and turns off the power. The system must support the power-off feature.Windows NT: The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section. Windows 95: Security privileges are not supported or required.
                EWX_REBOOT	Shuts down the system and then restarts the system. Windows NT: The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section. Windows 95: Security privileges are not supported or required.
                EWX_SHUTDOWN	Shuts down the system to a point at which it is safe to turn off the power. All file buffers have been flushed to disk, and all running processes have stopped. Windows NT: The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section. Windows 95: Security privileges are not supported or required.
              }
              Result := ExitWindowsEx(DoWhat, 0);
          end;
        end;
      end;
  end;
end;

function GetMachineName: string;
var
  i: longint;
  MyArray: array[0..255] of char;
begin
  result := '';
  FillChar(MyArray, SizeOf(MyArray), #0);
  i := 255;
  GetComputerName(MyArray, dword(i));
  result := Format('%s', [MyArray]);
end;

function WordPositionL(N: Cardinal; const S, WordDelims: AnsiString; var Pos: Cardinal): Boolean;
  {-Given an array of word delimiters, set Pos to the start position of the
    N'th word in a string.  Result indicates success/failure.}
var
  Count: Longint;
  I: Longint;
begin
  Count := 0;
  I := 1;
  Result := False;

  while (I <= Length(S)) and (Count <> LongInt(N)) do begin
    {skip over delimiters}
    while (I <= Length(S)) and CharExistsL(WordDelims, S[I]) do
      Inc(I);

    {if we're not beyond end of S, we're at the start of a word}
    if I <= Length(S) then
      Inc(Count);

    {if not finished, find the end of the current word}
    if Count <> LongInt(N) then
      while (I <= Length(S)) and not CharExistsL(WordDelims, S[I]) do
        Inc(I)
    else begin
      Pos := I;
      Result := True;
    end;
  end;
end;

function ExtractWordL(N: Cardinal; const S, WordDelims: AnsiString): AnsiString;
  {-Given an array of word delimiters, return the N'th word in a string.}
var
  C: Cardinal;
  I, J: Longint;
begin
  Result := '';
  if WordPositionL(N, S, WordDelims, C) then begin
    I := C;
    {find the end of the current word}
    J := I;
    while (I <= Length(S)) and not
      CharExistsL(WordDelims, S[I]) do
      Inc(I);
    SetLength(Result, I - J);
    Move(S[J], Result[1], I - J);
  end;
end;

{--------------- Word / Char manipulation -------------------------}

function CharExistsL(const S: AnsiString; C: AnsiChar): Boolean; register;
  {-Count the number of a given character in a string. }
asm
  push  ebx
  xor   ecx, ecx
  or    eax, eax
  jz    @@Done
  mov   ebx, [eax-StrOffset].LStrRec.Length
  or    ebx, ebx
  jz    @@Done
  jmp   @@5

@@Loop:
  cmp   dl, [eax+3]
  jne   @@1
  inc   ecx
  jmp   @@Done

@@1:
  cmp   dl, [eax+2]
  jne   @@2
  inc   ecx
  jmp   @@Done

@@2:
  cmp   dl, [eax+1]
  jne   @@3
  inc   ecx
  jmp   @@Done

@@3:
  cmp   dl, [eax+0]
  jne   @@4
  inc   ecx
  jmp   @@Done

@@4:
  add   eax, 4
  sub   ebx, 4

@@5:
  cmp   ebx, 4
  jge   @@Loop

  cmp   ebx, 3
  je    @@1

  cmp   ebx, 2
  je    @@2

  cmp   ebx, 1
  je    @@3

@@Done:
  mov   eax, ecx
  pop   ebx
end;

function AsciiCountL(const S, WordDelims: AnsiString; Quote: AnsiChar): Cardinal;
// Return the number of words in a string.
var
  I: Longint;
  InQuote: Boolean;
begin
  Result := 0;
  I := 1;
  InQuote := False;
  while I <= Length(S) do begin
    {skip over delimiters}
    while (I <= Length(S)) and (S[I] <> Quote)
      and CharExistsL(WordDelims, S[I]) do
      Inc(I);
    {if we're not beyond end of S, we're at the start of a word}
    if I <= Length(S) then
      Inc(Result);
    {find the end of the current word}
    while (I <= Length(S)) and
      (InQuote or not CharExistsL(WordDelims, S[I])) do begin
      if S[I] = Quote then
        InQuote := not InQuote;
      Inc(I);
    end;
  end;
end;

function IntToBinStr(Value: cardinal): string;
var
  i: Integer;
begin
  SetLength(result, 32);
  for i := 1 to 32 do
  begin
    if ((Value shl (i - 1)) shr 31) = 0 then
      result[i] := '0'
    else
      result[i] := '1';
  end;
end;


function CharToBinStr(InChar: CHAR): string;
begin
  result := '';

  case InChar of // Dec   Oct   Hex    Binary          Description
    ' ': result := '00100000'; // 032   040   020   00100000   SPC space spc
    '!': result := '00100001'; // 033   041   021   00100001   ! exclamation-point bang wow boing hey
    '"': result := '00100010'; // 034   042   022   00100010   " straight-double-quotation-mark dirk
    '#': result := '00100011'; // 035   043   023   00100011   # number-sign she sharp crosshatch octothorpe
    '$': result := '00100100'; // 036   044   024   00100100   $ @@ dollar-sign money buck escape
    '%': result := '00100101'; // 037   045   025   00100101   % percent-sign per double-o-seven mod
    '&': result := '00100110'; // 038   046   026   00100110   & ampersand and address snowman donald-duck
    '''': result := '00100111'; // 039   047   027   00100111   ' apostrophe quote tick prime
    '(': result := '00101000'; // 040   050   028   00101000   ( left-parenthesis open sad
    ')': result := '00101001'; // 041   051   029   00101001   ) right-parenthesis close happy
    '*': result := '00101010'; // 042   052   02a   00101010   * asterisk star times wildcard Hale
    '+': result := '00101011'; // 043   053   02b   00101011   + addition-sign plus and
    ',': result := '00101100'; // 044   054   02c   00101100   , comma __..__
    '-': result := '00101101'; // 045   055   02d   00101101   - subtraction-sign minus hyphen negative dash
    '.': result := '00101110'; // 046   056   02e   00101110   . period dot decimal radix full-stop ._._._
    '/': result := '00101111'; // 047   057   02f   00101111   / right-slash virgule stroke over
    '0': result := '00110000'; // 048   060   030   00110000   0 _____
    '1': result := '00110001'; // 049   061   031   00110001   1 .____
    '2': result := '00110010'; // 050   062   032   00110010   2 ..___
    '3': result := '00110011'; // 051   063   033   00110011   3 ...__
    '4': result := '00110100'; // 052   064   034   00110100   4 ...._
    '5': result := '00110101'; // 053   065   035   00110101   5 .....
    '6': result := '00110110'; // 054   066   036   00110110   6 _....
    '7': result := '00110111'; // 055   067   037   00110111   7 __...
    '8': result := '00111000'; // 056   070   038   00111000   8 ___..
    '9': result := '00111001'; // 057   071   039   00111001   9 ____.
    ':': result := '00111010'; // 058   072   03a   00111010   : colon double-dots ___...
    ';': result := '00111011'; // 059   073   03b   00111011   ; semicolon go-on _._._.
    '<': result := '00111100'; // 060   074   03c   00111100   < less-than bra in west left-chevron
    '=': result := '00111101'; // 061   075   03d   00111101   = equals quadrathorpe
    '>': result := '00111110'; // 062   076   03e   00111110   > greater-than (bra)ket out east right-chevron
    '?': result := '00111111'; // 063   077   03f   00111111   ? UNL question-mark query what  ..__..
    '@': result := '01000000'; // 064   100   040   01000000   @ at-symbol at-sign strudel whirl snail
    'A': result := '01000001'; // 065   101   041   01000001   A ._
    'B': result := '01000010'; // 066   102   042   01000010   B _...
    'C': result := '01000011'; // 067   103   043   01000011   C _._.
    'D': result := '01000100'; // 068   104   044   01000100   D _..
    'E': result := '01000101'; // 069   105   045   01000101   E .
    'F': result := '01000110'; // 070   106   046   01000110   F .._.
    'G': result := '01000111'; // 071   107   047   01000111   G __.
    'H': result := '01001000'; // 072   110   048   01001000   H ....
    'I': result := '01001001'; // 073   111   049   01001001   I ..
    'J': result := '01001010'; // 074   112   04a   01001010   J .___
    'K': result := '01001011'; // 075   113   04b   01001011   K _._
    'L': result := '01001100'; // 076   114   04c   01001100   L ._..
    'M': result := '01001101'; // 077   115   04d   01001101   M __
    'N': result := '01001110'; // 078   116   04e   01001110   N _.
    'O': result := '01001111'; // 079   117   04f   01001111   O ___
    'P': result := '01010000'; // 080   120   050   01010000   P .__.
    'Q': result := '01010001'; // 081   121   051   01010001   Q __._
    'R': result := '01010010'; // 082   122   052   01010010   R ._.
    'S': result := '01010011'; // 083   123   053   01010011   S ...
    'T': result := '01010100'; // 084   124   054   01010100   T _
    'U': result := '01010101'; // 085   125   055   01010101   U .._
    'V': result := '01010110'; // 086   126   056   01010110   V ..._
    'W': result := '01010111'; // 087   127   057   01010111   W .__
    'X': result := '01011000'; // 088   130   058   01011000   X _.._
    'Y': result := '01011001'; // 089   131   059   01011001   Y _.__
    'Z': result := '01011010'; // 090   132   05a   01011010   Z __..
    '[': result := '01011011'; // 091   133   05b   01011011   [ left-bracket open-square
    '\': result := '01011100'; // 092   134   05c   01011100   \ left-slash backslash bash
    ']': result := '01011101'; // 093   135   05d   01011101   ] right-bracket close-square
    '^': result := '01011110'; // 094   136   05e   01011110   ^ hat circumflex caret up-arrow
    '_': result := '01011111'; // 095   137   05f   01011111   _ UNT underscore underbar
    '`': result := '01100000'; // 096   140   060   01100000   ` accent-grave backprime backquote
    'a': result := '01100001'; // 097   141   061   01100001   a alpha able
    'b': result := '01100010'; // 098   142   062   01100010   b bravo baker
    'c': result := '01100011'; // 099   143   063   01100011   c charlie
    'd': result := '01100100'; // 100   144   064   01100100   d delta
    'e': result := '01100101'; // 101   145   065   01100101   e echo
    'f': result := '01100110'; // 102   146   066   01100110   f foxtrot fox
    'g': result := '01100111'; // 103   147   067   01100111   g golf
    'h': result := '01101000'; // 104   150   068   01101000   h hotel
    'i': result := '01101001'; // 105   151   069   01101001   i india
    'j': result := '01101010'; // 106   152   06a   01101010   j juliett
    'k': result := '01101011'; // 107   153   06b   01101011   k kilo
    'l': result := '01101100'; // 108   154   06c   01101100   l lima
    'm': result := '01101101'; // 109   155   06d   01101101   m mike
    'n': result := '01101110'; // 110   156   06e   01101110   n november
    'o': result := '01101111'; // 111   157   06f   01101111   o oscar
    'p': result := '01110000'; // 112   160   070   01110000   p papa
    'q': result := '01110001'; // 113   161   071   01110001   q quebec
    'r': result := '01110010'; // 114   162   072   01110010   r romeo
    's': result := '01110011'; // 115   163   073   01110011   s sierra
    't': result := '01110100'; // 116   164   074   01110100   t tango
    'u': result := '01110101'; // 117   165   075   01110101   u uniform
    'v': result := '01110110'; // 118   166   076   01110110   v victor
    'w': result := '01110111'; // 119   167   077   01110111   w whiskey
    'x': result := '01111000'; // 120   170   078   01111000   x x-ray
    'y': result := '01111001'; // 121   171   079   01111001   y yankee
    'z': result := '01111010'; // 122   172   07a   01111010   z zulu
    '{': result := '01111011'; // 123   173   07b   01111011   { left-brace begin leftit
    '|': result := '01111100'; // 124   174   07c   01111100   | logical-or vertical-bar pipe
    '}': result := '01111101'; // 125   175   07d   01111101   } right-brace end rightit
    '~': result := '01111110'; // 126   176   07e   01111110   ~ similar tilde wave squiggle approx wave
  else result := '00000000';
  end;
end;


function BinStrToChar(InBinStr: string): CHAR;
begin
  result := ' ';

  if InBinstr = '00100000' then result := ' ' // 032   040   020   00100000   SPC space spc
  else if InBinStr = '00100001' then result := '!' // 033   041   021   00100001   ! exclamation-point bang wow boing hey
  else if InBinStr = '00100010' then result := '"' // 034   042   022   00100010   " straight-double-quotation-mark dirk
  else if InBinStr = '00100011' then result := '#' // 035   043   023   00100011   # number-sign she sharp crosshatch octothorpe
  else if InBinStr = '00100100' then result := '$' // 036   044   024   00100100   $ @@ dollar-sign money buck escape
  else if InBinStr = '00100101' then result := '%' // 037   045   025   00100101   % percent-sign per double-o-seven mod
  else if InBinStr = '00100110' then result := '&' // 038   046   026   00100110   & ampersand and address snowman donald-duck
  else if InBinStr = '00100111' then result := '''' // 039   047   027   00100111   ' apostrophe quote tick prime
  else if InBinStr = '00101000' then result := '(' // 040   050   028   00101000   ( left-parenthesis open sad
  else if InBinStr = '00101001' then result := ')' // 041   051   029   00101001   ) right-parenthesis close happy
  else if InBinStr = '00101010' then result := '*' // 042   052   02a   00101010   * asterisk star times wildcard Hale
  else if InBinStr = '00101011' then result := '+' // 043   053   02b   00101011   + addition-sign plus and
  else if InBinStr = '00101100' then result := ',' // 044   054   02c   00101100   , comma __..__
  else if InBinStr = '00101101' then result := '-' // 045   055   02d   00101101   - subtraction-sign minus hyphen negative dash
  else if InBinStr = '00101110' then result := '.' // 046   056   02e   00101110   . period dot decimal radix full-stop ._._._
  else if InBinStr = '00101111' then result := '/' // 047   057   02f   00101111   / right-slash virgule stroke over
  else if InBinStr = '00110000' then result := '0' // 048   060   030   00110000   0 _____
  else if InBinStr = '00110001' then result := '1' // 049   061   031   00110001   1 .____
  else if InBinStr = '00110010' then result := '2' // 050   062   032   00110010   2 ..___
  else if InBinStr = '00110011' then result := '3' // 051   063   033   00110011   3 ...__
  else if InBinStr = '00110100' then result := '4' // 052   064   034   00110100   4 ...._
  else if InBinStr = '00110101' then result := '5' // 053   065   035   00110101   5 .....
  else if InBinStr = '00110110' then result := '6' // 054   066   036   00110110   6 _....
  else if InBinStr = '00110111' then result := '7' // 055   067   037   00110111   7 __...
  else if InBinStr = '00111000' then result := '8' // 056   070   038   00111000   8 ___..
  else if InBinStr = '00111001' then result := '9' // 057   071   039   00111001   9 ____.
  else if InBinStr = '00111010' then result := ':' // 058   072   03a   00111010   : colon double-dots ___...
  else if InBinStr = '00111011' then result := ';' // 059   073   03b   00111011   ; semicolon go-on _._._.
  else if InBinStr = '00111100' then result := '<' // 060   074   03c   00111100   < less-than bra in west left-chevron
  else if InBinStr = '00111101' then result := '=' // 061   075   03d   00111101   = equals quadrathorpe
  else if InBinStr = '00111110' then result := '>' // 062   076   03e   00111110   > greater-than (bra)ket out east right-chevron
  else if InBinStr = '00111111' then result := '?' // 063   077   03f   00111111   ? UNL question-mark query what  ..__..
  else if InBinStr = '01000000' then result := '@' // 064   100   040   01000000   @ at-symbol at-sign strudel whirl snail
  else if InBinStr = '01000001' then result := 'A' // 065   101   041   01000001   A ._
  else if InBinStr = '01000010' then result := 'B' // 066   102   042   01000010   B _...
  else if InBinStr = '01000011' then result := 'C' // 067   103   043   01000011   C _._.
  else if InBinStr = '01000100' then result := 'D' // 068   104   044   01000100   D _..
  else if InBinStr = '01000101' then result := 'E' // 069   105   045   01000101   E .
  else if InBinStr = '01000110' then result := 'F' // 070   106   046   01000110   F .._.
  else if InBinStr = '01000111' then result := 'G' // 071   107   047   01000111   G __.
  else if InBinStr = '01001000' then result := 'H' // 072   110   048   01001000   H ....
  else if InBinStr = '01001001' then result := 'I' // 073   111   049   01001001   I ..
  else if InBinStr = '01001010' then result := 'J' // 074   112   04a   01001010   J .___
  else if InBinStr = '01001011' then result := 'K' // 075   113   04b   01001011   K _._
  else if InBinStr = '01001100' then result := 'L' // 076   114   04c   01001100   L ._..
  else if InBinStr = '01001101' then result := 'M' // 077   115   04d   01001101   M __
  else if InBinStr = '01001110' then result := 'N' // 078   116   04e   01001110   N _.
  else if InBinStr = '01001111' then result := 'O' // 079   117   04f   01001111   O ___
  else if InBinStr = '01010000' then result := 'P' // 080   120   050   01010000   P .__.
  else if InBinStr = '01010001' then result := 'Q' // 081   121   051   01010001   Q __._
  else if InBinStr = '01010010' then result := 'R' // 082   122   052   01010010   R ._.
  else if InBinStr = '01010011' then result := 'S' // 083   123   053   01010011   S ...
  else if InBinStr = '01010100' then result := 'T' // 084   124   054   01010100   T _
  else if InBinStr = '01010101' then result := 'U' // 085   125   055   01010101   U .._
  else if InBinStr = '01010110' then result := 'V' // 086   126   056   01010110   V ..._
  else if InBinStr = '01010111' then result := 'W' // 087   127   057   01010111   W .__
  else if InBinStr = '01011000' then result := 'X' // 088   130   058   01011000   X _.._
  else if InBinStr = '01011001' then result := 'Y' // 089   131   059   01011001   Y _.__
  else if InBinStr = '01011010' then result := 'Z' // 090   132   05a   01011010   Z __..
  else if InBinStr = '01011011' then result := '[' // 091   133   05b   01011011   [ left-bracket open-square
  else if InBinStr = '01011100' then result := '\' // 092   134   05c   01011100   \ left-slash backslash bash
  else if InBinStr = '01011101' then result := ']' // 093   135   05d   01011101   ] right-bracket close-square
  else if InBinStr = '01011110' then result := '^' // 094   136   05e   01011110   ^ hat circumflex caret up-arrow
  else if InBinStr = '01011111' then result := '_' // 095   137   05f   01011111   _ UNT underscore underbar
  else if InBinStr = '01100000' then result := '`' // 096   140   060   01100000   ` accent-grave backprime backquote
  else if InBinStr = '01100001' then result := 'a' // 097   141   061   01100001   a alpha able
  else if InBinStr = '01100010' then result := 'b' // 098   142   062   01100010   b bravo baker
  else if InBinStr = '01100011' then result := 'c' // 099   143   063   01100011   c charlie
  else if InBinStr = '01100100' then result := 'd' // 100   144   064   01100100   d delta
  else if InBinStr = '01100101' then result := 'e' // 101   145   065   01100101   e echo
  else if InBinStr = '01100110' then result := 'f' // 102   146   066   01100110   f foxtrot fox
  else if InBinStr = '01100111' then result := 'g' // 103   147   067   01100111   g golf
  else if InBinStr = '01101000' then result := 'h' // 104   150   068   01101000   h hotel
  else if InBinStr = '01101001' then result := 'i' // 105   151   069   01101001   i india
  else if InBinStr = '01101010' then result := 'j' // 106   152   06a   01101010   j juliett
  else if InBinStr = '01101011' then result := 'k' // 107   153   06b   01101011   k kilo
  else if InBinStr = '01101100' then result := 'l' // 108   154   06c   01101100   l lima
  else if InBinStr = '01101101' then result := 'm' // 109   155   06d   01101101   m mike
  else if InBinStr = '01101110' then result := 'n' // 110   156   06e   01101110   n november
  else if InBinStr = '01101111' then result := 'o' // 111   157   06f   01101111   o oscar
  else if InBinStr = '01110000' then result := 'p' // 112   160   070   01110000   p papa
  else if InBinStr = '01110001' then result := 'q' // 113   161   071   01110001   q quebec
  else if InBinStr = '01110010' then result := 'r' // 114   162   072   01110010   r romeo
  else if InBinStr = '01110011' then result := 's' // 115   163   073   01110011   s sierra
  else if InBinStr = '01110100' then result := 't' // 116   164   074   01110100   t tango
  else if InBinStr = '01110101' then result := 'u' // 117   165   075   01110101   u uniform
  else if InBinStr = '01110110' then result := 'v' // 118   166   076   01110110   v victor
  else if InBinStr = '01110111' then result := 'w' // 119   167   077   01110111   w whiskey
  else if InBinStr = '01111000' then result := 'x' // 120   170   078   01111000   x x-ray
  else if InBinStr = '01111001' then result := 'y' // 121   171   079   01111001   y yankee
  else if InBinStr = '01111010' then result := 'z' // 122   172   07a   01111010   z zulu
  else if InBinStr = '01111011' then result := '{' // 123   173   07b   01111011   { left-brace begin leftit
  else if InBinStr = '01111100' then result := '|' // 124   174   07c   01111100   | logical-or vertical-bar pipe
  else if InBinStr = '01111101' then result := '}' // 125   175   07d   01111101   } right-brace end rightit
  else if InBinStr = '01111110' then result := '~' // 126   176   07e   01111110   ~ similar tilde wave squiggle approx wave
  else if InBinStr = '00000000' then result := ' ';
end;


function BinStrToChar2(InBinStr: string): CHAR; //PrintOutResults(InStr:string):char;
var strTemp: string;
  I: Integer;
  Sum: Extended;

  function AToThePowerOfB(A: Extended; B: Integer): Extended;
  var i: Integer;
  begin
    Result := 1;
    for i := 1 to B do
    begin
      Result := Result * A;
    end;
  end;
begin
  try
    try
      strTemp := InBinStr;
      Sum := 0.00;

      for I := Length(strTemp) downto 1 do
        Sum := Sum + StrToInt(strTemp[I]) * (AToThePowerOfB(2, (Length(strTemp) - I)));

      Result := chr(Trunc(Sum));
    except on e: exception do
      begin
        //
      end;
    end;
  finally
    //
  end;
end;


procedure RANDOM_BEEP();
var X: integer;
begin
  X := 13; // how many variations + 1
  Randomize;
  X := Random(X);

  case X of
    0: JBEEP('OCTAVE');
    1: JBEEP('SHARK');
    2: JBEEP('UP');
    3: JBEEP('DOWN');
    4: JBEEP('SHAVE');
    5: JBEEP('UPDOWN');
    6: JBEEP('MOUNTAIN');
    7: JBEEP('SMOKE');
    8: JBEEP('CHARGE');
    9: JBEEP('DIXIELAND');
    10: JBEEP('LACOOK');
    11: JBEEP('DUAL');
    12: JBEEP('HAVA');
    13: JBEEP('DOG');
  else
    nada;
  end;
end;

procedure nada();
begin
  ; //like the name
end;

procedure JBEEP(sBEEPNAME: string);
var
  temp: string;
begin
  temp := trim(UpperCase(sBEEPNAME));

  if temp = '' then exit
  else if temp = 'OCTAVE' then
  begin
    Windows.Beep(250, 100);
    Windows.Beep(300, 100);
    Windows.Beep(250, 100);
    Windows.Beep(300, 100);
    Windows.Beep(250, 100);
    Windows.Beep(300, 100);
    Windows.Beep(400, 325);
  end
  else if temp = 'SHARK' then
  begin
    Windows.Beep(58, 200);
    Windows.Sleep(10);
    Windows.Beep(66, 250);
    Windows.Sleep(100);
    Windows.Beep(58, 200);
    Windows.Sleep(10);
    Windows.Beep(66, 250);
    Windows.Sleep(1000);
    Windows.Beep(58, 200);
    Windows.Sleep(10);
    Windows.Beep(66, 250);
    Windows.Sleep(50);
    Windows.Beep(58, 200);
    Windows.Sleep(10);
    Windows.Beep(66, 250);
  end
  else if temp = 'LACOOK' then
  begin
    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(280, 500);
    Windows.Sleep(50);
    Windows.Beep(350, 500);

    Windows.Sleep(60);

    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(210, 150);
    Windows.Sleep(50);
    Windows.Beep(280, 500);
    Windows.Sleep(50);
    Windows.Beep(350, 500);

    Windows.Sleep(150);

    Windows.Beep(375, 200);
    Windows.Beep(350, 285);
    Windows.Sleep(75);
    Windows.Beep(350, 200);
    Windows.Beep(310, 285);
    Windows.Sleep(75);
    Windows.Beep(310, 200);
    Windows.Beep(280, 300);
  end
  else if temp = 'UP' then
  begin
    Windows.Beep(100, 200);
    Windows.Beep(150, 30);
    Windows.Beep(120, 30);
    Windows.Beep(200, 30);
    Windows.Beep(300, 30);
    Windows.Beep(400, 30);
    Windows.Beep(500, 30);
    Windows.Beep(600, 30);
    Windows.Beep(700, 30);
    Windows.Beep(800, 30);
    Windows.Beep(800, 200);
  end
  else if temp = 'CHARGE' then
  begin        // -f  -l
    Windows.Beep(120, 250);
    Windows.Sleep(10);
    Windows.Beep(150, 250);
    Windows.Sleep(10);
    Windows.Beep(180, 250);
    Windows.Sleep(5);
    Windows.Beep(240, 400);
    Windows.Sleep(5);
    Windows.Beep(180, 200);
    Windows.Beep(240, 500);
  end
  else if temp = 'DOWN' then
  begin
    Windows.Beep(800, 200);
    Windows.Beep(700, 30);
    Windows.Beep(600, 30);
    Windows.Beep(500, 30);
    Windows.Beep(400, 30);
    Windows.Beep(300, 30);
    Windows.Beep(200, 30);
    Windows.Beep(120, 30);
    Windows.Beep(150, 30);
    Windows.Beep(100, 200);
  end
  else if temp = 'SHAVE' then
  begin
    Windows.Beep(400, 250);
    Windows.Sleep(100);
    Windows.Beep(300, 200);
    Windows.Beep(300, 200);
    Windows.Beep(330, 250);
    Windows.Beep(300, 250);
    Windows.Sleep(200);
    Windows.Beep(380, 250);
    Windows.Sleep(50);
    Windows.Beep(400, 250);
  end
  else if temp = 'DIXIELAND' then
  begin
    Windows.Beep(300, 150);
    Windows.Beep(250, 150);
    Windows.Beep(200, 150);
    Windows.Sleep(20);
    Windows.Beep(200, 200);
    Windows.Sleep(20);
    Windows.Beep(200, 200);
    Windows.Beep(225, 100);
    Windows.Sleep(20);
    Windows.Beep(250, 100);
    Windows.Sleep(20);
    Windows.Beep(270, 100);
    Windows.Sleep(20);
    Windows.Beep(300, 200);
    Windows.Sleep(20);
    Windows.Beep(300, 200);
    Windows.Sleep(20);
    Windows.Beep(300, 200);
    Windows.Sleep(20);
    Windows.Beep(250, 300);
  end
  else if temp = 'UPDOWN' then
  begin
    Windows.Beep(10, 15);
    Windows.Beep(20, 15);
    Windows.Beep(15, 15);
    Windows.Beep(40, 15);
    Windows.Beep(50, 15);
    Windows.Beep(60, 15);
    Windows.Beep(70, 15);
    Windows.Beep(80, 15);
    Windows.Beep(90, 15);
    Windows.Beep(100, 15);
    Windows.Beep(150, 15);
    Windows.Beep(120, 15);
    Windows.Beep(200, 15);
    Windows.Beep(150, 15);
    Windows.Beep(400, 15);
    Windows.Beep(500, 15);
    Windows.Beep(600, 15);
    Windows.Beep(700, 15);
    Windows.Beep(800, 100);
    Windows.Sleep(200);
    Windows.Beep(800, 100);
    Windows.Beep(700, 15);
    Windows.Beep(600, 15);
    Windows.Beep(500, 15);
    Windows.Beep(400, 15);
    Windows.Beep(150, 15);
    Windows.Beep(200, 15);
    Windows.Beep(120, 15);
    Windows.Beep(150, 15);
    Windows.Beep(100, 200);
  end
  else if temp = 'DUAL' then
  begin // dualing banjos
    Windows.Beep(250, 150);
    Windows.Sleep(10);
    Windows.Beep(260, 150);
    Windows.Sleep(10);
    Windows.Beep(300, 150);
    Windows.Sleep(10);
    Windows.Beep(250, 200);
    Windows.Beep(260, 200);
    Windows.Beep(225, 200);
    Windows.Beep(250, 200);
    Windows.Beep(200, 200);
    Windows.Beep(225, 300);
  end
  else if temp = 'MOUNTAIN' then
  begin // big rock candy mountain
    Windows.Beep(150, 200);
    Windows.Sleep(100);
    Windows.Beep(200, 300);
    Windows.Sleep(10);
    Windows.Beep(200, 300);
    Windows.Beep(165, 200);
    Windows.Sleep(100);
    Windows.Beep(150, 200);
    Windows.Sleep(100);
    Windows.Beep(200, 150);
    Windows.Sleep(10);
    Windows.Beep(200, 150);
    Windows.Sleep(10);
    Windows.Beep(200, 150);
    Windows.Sleep(10);
    Windows.Beep(200, 150);
    Windows.Sleep(10);
    Windows.Beep(165, 200);
    Windows.Sleep(100);
    Windows.Beep(150, 200);
    Windows.Sleep(100);
    Windows.Beep(200, 300);
    Windows.Sleep(10);
    Windows.Beep(200, 300);
    Windows.Sleep(10);
    Windows.Beep(225, 300);
    Windows.Sleep(10);
    Windows.Beep(200, 300);
    Windows.Sleep(10);
    Windows.Beep(250, 300);
    Windows.Sleep(20);
    Windows.Beep(200, 500);
  end
  else if temp = 'SMOKE' then
  begin // smoke on the water
    Windows.Beep(100, 500);
    Windows.Sleep(50);
    Windows.Beep(120, 500);
    Windows.Sleep(50);
    Windows.Beep(135, 750);
    Windows.Sleep(50);
    Windows.Beep(100, 500);
    Windows.Sleep(50);
    Windows.Beep(120, 500);
    Windows.Sleep(30);
    Windows.Beep(140, 250);
    Windows.Beep(135, 500);
    Windows.Sleep(500);
    Windows.Beep(100, 500);
    Windows.Sleep(50);
    Windows.Beep(120, 500);
    Windows.Sleep(50);
    Windows.Beep(135, 750);
    Windows.Sleep(50);
    Windows.Beep(120, 500);
    Windows.Beep(100, 250);
    Windows.Sleep(10);
    Windows.Beep(100, 500);
  end
  else if temp = 'HAVA' then
  begin
    Windows.Beep(200, 200);
    Windows.Sleep(200);
    Windows.Beep(200, 400);
    Windows.Sleep(100);
    Windows.Beep(250, 200);
    Windows.Beep(225, 200);
    Windows.Beep(200, 200);

    Windows.Sleep(20);

    Windows.Beep(250, 200);
    Windows.Sleep(200);
    Windows.Beep(250, 400);
    Windows.Sleep(100);
    Windows.Beep(300, 200);
    Windows.Beep(265, 200);
    Windows.Beep(250, 200);

    Windows.Sleep(20);

    Windows.Beep(265, 200);
    Windows.Sleep(200);
    Windows.Beep(265, 400);
    Windows.Sleep(100);
    Windows.Beep(330, 200);
    Windows.Beep(300, 200);
    Windows.Beep(265, 200);

    Windows.Sleep(20);

    Windows.Beep(250, 400);
    Windows.Sleep(20);
    Windows.Beep(200, 200);
    Windows.Sleep(20);
    Windows.Beep(200, 200);
    Windows.Beep(250, 500);
  end
  else if temp = 'DOG' then
  begin
    Windows.Beep(12000, 500);
    Windows.Beep(13000, 1000);
    Windows.Beep(14000, 2000);
    Windows.Beep(14500, 3000);
  end;
end;

function OuncesPureAlc(NumDrinks, OZPerDrink, PERCENT: real): real;
begin // must throw percent as "40" for 40% NOT ".40"
  result := (NumDrinks * OZPerDrink) * (PERCENT / 100);
end;

function BACOverTime(IsMale: boolean; OzPureAlc, lbsWeight, hours: real): real;
var
  eightTen: integer;
  temp: real;
begin
  if IsMale then eightTen := 8
  else eightTen := 10;

  temp := ((OzPureAlc * eightTen) / lbsWeight) - (hours * 0.017 {ave burnoff});

  if temp > 0 then result := temp
  else result := 0;
end;

function BloodAlcoholOverTime(Male: boolean; NumDrinks, OzPerDrink, percentAlc, weightLbs, Time: real): real; stdcall;
var // old funciton here to support backward compatibility
  temp: real;
  OzPureAlc: Real;
begin
  if (NumDrinks < 0) or
    (OzPerDrink < 0) or
    (percentAlc <= 0.999) or
    (weightLbs <= 0) or
    (Time < 0)
    then
  begin
    result := -1;
    exit;
  end;

  try
    OZPureAlc := OuncesPureAlc(NumDrinks, OzPerDrink, percentAlc);
    temp := BACOverTime(Male, OZPureAlc, WeightLbs, Time);
  except
    result := -3;
    exit;
  end;

  if temp >= 0.001 then
    result := temp
  else
    result := 0;
end;

function HowDrunkIAm(BAC, Tolerance: real): shortstring; stdcall;
var
  tempBAC: real;
begin
  if (Tolerance < 1) and (Tolerance > 0) then
  begin
    result := 'Please use percent tolerance as a whole number. For example: use "40" not .40';
    exit;
  end
  else if (Tolerance > 100) then
  begin
    result := 'no such thing as a tolerance greater than 100%';
    exit;
  end;

  try
    Tolerance := Tolerance / 100;
    tempBAC := BAC * Tolerance;
    tempBAC := BAC - tempBAC;
  except
    result := ' Zero Divide error. ';
    exit;
  end;

  if (BAC <= 0) then
  begin
    result := 'Sober.'
  end
  else if (BAC <= 0.019) then
  begin
    result := 'Less than one drink.'
  end
  else if (BAC <= 0.02) then
  begin
    result := 'About 1 or less. Light drinkers feel effects.';
  end
  else if (BAC <= 0.03) then
  begin
    result := 'About 1.5 drinks an hour. Most people feel relaxed.';
  end
  else if (BAC <= 0.04) then
  begin
    result := 'About 2 drinks an hour. Relaxed, talkative and happy. Fine motor skills affected.';
  end
  else if (BAC <= 0.05) then
  begin
    result := 'About two and a half drinks per hour. Lightheadedness, giddiness, lowered inhibitions, etc.';
  end
  else if (BAC <= 0.06) then
  begin
    result := 'About 3 drinks an hour. You feel relaxed, talkative and happy.'
  end
  else if (BAC <= 0.07) then
  begin
    result := 'About 3.5 drinks an hour. Feeling pretty good.';
  end
  else if (BAC <= 0.08) then
  begin
    result := '4 drinks an hour. Muscle coordination is impaired and reaction time is slower.';
  end
  else if (BAC <= 0.09) then
  begin
    result := '4.5 drinks in an hour. Motor coordination effected. Legally intoxicated.';
  end
  else if (BAC <= 0.10) then
  begin
    result := '5 drinks an hour. Deterioration of reaction time and muscle control are present. Slurring and general clumsiness.';
  end
  else if (BAC <= 0.15) then
  begin
    result := '5-7 drinks an hour. Balance is impaired and all faculties are affected. Equal to 1/2 pint of 40% alcohol.';
  end
  else if (BAC <= 0.20) then
  begin
    result := '7-10 drinks an hour. Motor and emotional controls are measurably affected. Possible double vision.';
  end
  else if (BAC <= 0.30) then
  begin
    result := '10-14 drinks an hour. Confusion, stupor and OR loss of consciousness.';
  end
  else if (BAC <= 0.40) then
  begin
    result := '10-15 drinks an hour. Unconsciousness � threshold of coma.';
  end
  else if (BAC <= 0.50) then
  begin
    result := '14-20 drinks an hour. Possible coma. Lethal for 50% of the population';
  end
  else if (BAC <= 0.75) then
  begin
    result := 'Deep coma. lethal for 75% of the population.';
  end
  else
    result := 'Totally leathal Dose of Booze, Jack';

  if Tolerance > 0 then
  begin
    result := result + ' BAC after ' + floattostr(Tolerance * 100) + '% tolerance: ' + FormatFloat('####0.000', tempBAC) + '. You actually feel: "' + HowDrunkIAm(tempBAC, 0) + '"';
  end;
end;

function FahrenheitToCelcius(INREAL: real): real;
begin
  result := (5 / 9) * (INREAL - 32);
end;

function CelciusToFahrenheit(INREAL: real): real;
begin
  result := ((9 / 5) * INREAL) + 32;
end;

function GetOSVersion(): string;
var
  verInfo: TOSVERSIONINFO;
begin
  result := '';
  try
    verInfo.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
    if GetVersionEx(verInfo) then
    begin
      case verInfo.dwPlatformId of
        VER_PLATFORM_WIN32s: result := 'Win16';
        VER_PLATFORM_WIN32_WINDOWS: result := 'W9X';
        VER_PLATFORM_WIN32_NT: result := 'NT';
      end;
    end;
  except result := '';
  end;
end;

// Lock a form MaxSize. Requirements: Sender must be of TForm type and MinWidth, MaxWidth are set

function LockForm(Sender: TObject): boolean;
begin
  result := true;
  with Sender as TForm do
  begin
    try
      Constraints.MaxHeight := Constraints.MinHeight;
      Constraints.MaxWidth := Constraints.MinWidth;
      BorderIcons := [biSystemMenu, biMinimize];
    except result := false;
    end;
  end;
end;

function GetTheUserName(): string;
var
  buffer: array[0..255] of char;
  buffSize: DWORD;
begin
  buffSize := sizeOf(buffer);
  GetUserName(@buffer, buffSize);
  result := trim(uppercase(buffer));
end;

function SetIfNotCreateDirectory(): boolean;
var // set OR create c:\CLog
  d: array[0..MAX_PATH] of char;
begin
  result := true;
  d := 'c:\CLog';
  if not SetCurrentDirectory(d) then
  begin
    if not (CreateDirectory(d, nil)) then
    begin
      Result := false;
    end;
  end;
end;

function SetIfNotCreateDirectory(sPATH: string): boolean;
var // set OR create sPATH
  d: string; //array[0..MAX_PATH] of char;
begin
  if trim(sPATH) = '' then
  begin
    result := false;
    exit;
  end
  else
    result := true;

  d := sPATH;

  if not SetCurrentDirectory(PCHAR(d)) then
  begin
    if not (CreateDirectory(PCHAR(d), nil)) then
    begin
      Result := false;
    end;
  end;
end;

{logging}

function FlushOutOldLogFiles(): Boolean;
var
  i: integer;
  SearchRec: TSearchRec;
  FN, DirName: string;
begin
  FN := '*.txt';
  DirName := 'C:\CLog\';

  if not (SetIfNotCreateDirectory()) then exit;
  i := FindFirst(FN, faAnyFile, SearchRec);

  while (i = 0) do
  begin
    DeleteFile(PChar(DirName + SearchRec.Name));
    i := FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;

function FlushOutOldLogFilesX(sEXT: string): Boolean;
var
  i: integer;
  SearchRec: TSearchRec;
  FN, DirName: string;
begin
  FN := sEXT; ///'*.txt';
  DirName := 'C:\CLog\';

  if not (SetIfNotCreateDirectory()) then exit;
  i := FindFirst(FN, faAnyFile, SearchRec);

  while (i = 0) do
  begin
    DeleteFile(PChar(DirName + SearchRec.Name));
    i := FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;

function GetFileListOLD(sEXT, sPATH: string): TStringList;
var
  i: integer;
  SearchRec: TSearchRec;
  FN, DirName: string;
  MyList: TStringList;
begin
  try
    MyList := TStringList.Create;

    FN := sEXT; // '*.*';
    DirName := sPATH; // 'C:\CLog\';

    if not (SetIfNotCreateDirectory()) then exit;

    i := FindFirst(FN, faAnyFile, SearchRec);

    while (i = 0) do
    begin
   // DeleteFile(PChar(DirName + SearchRec.Name));
      MyList.Add(pchar(DirName + SearchRec.Name));
      i := FindNext(SearchRec);
    end;
    FindClose(SearchRec);

    result := MyList;
  finally
    MyList.Free;
  end;
end;

function GetFileList(sEXT, sPATH: string; MyList: TStringList): TStringList;
var
  i: integer;
  SearchRec: TSearchRec;
begin
  i := FindFirst(sPATH + sEXT, not faDirectory, SearchRec);
  while (i = 0) do
  begin
    MyList.Add(pchar(sPATH + SearchRec.Name));
    i := FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;

function DeleteFileTypeOfAge(sEXT, sDIRECTORY: string; iDAYSOLD: integer): Boolean;
var
  i: integer;
  SearchRec: TSearchRec;
  DirName: string;
  dtCUTOFF, FileDate: TDateTime;
begin
  result := true;
  { assumtions:
     sDIRECTORY is Full Path including last backslash e.g. 'c:\clog\'
     sEXT should have '*.' in front of it & sEXT was thrown in like this: 'txt'
     for text file (not incl the dot or star)

    example:
     DeleteFileTypeOfAge('txt', 'C:\CLog\', 10);
     DeleteFileTypeOfAge('csv', 'C:\CLog\', 10);
  }
  if (trim(sEXT) = '') or (trim(sDIRECTORY) = '') then
  begin
    result := false;
    exit;
  end;

  if not (Pos('*.', sEXT) = 1) then sEXT := '*.' + sEXT;
  if (sDIRECTORY[(length(sDIRECTORY))]) <> '\' then sDIRECTORY := sDIRECTORY + '\';

  if not (SetIfNotCreateDirectory(sDIRECTORY)) then
  begin
    result := false;
    exit;
  end;

  i := FindFirst(sEXT, faAnyFile, SearchRec);

  if i <> 0 then
  begin
    //result := false;
    exit;
  end;

  dtCUTOFF := now - iDAYSOLD;

  while (i = 0) do
  begin
    if FileAge(PChar(DirName + SearchRec.Name)) = -1 then continue
    else FileDate := FileDateToDateTime(FileAge(PChar(DirName + SearchRec.Name)));

    if (FileDate < dtCUTOFF) then
      DeleteFile(PChar(DirName + SearchRec.Name));
    i := FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;

function DeleteFilesOfTypeInDirectory(FullPath, FileExt: string): Boolean;
var
  i: integer;
  SearchRec: TSearchRec;
  FN, DirName: string;
begin
  result := true;

  if (FullPath = '') then
  begin
    result := false;
    exit;
  end;

  if (FileExt = '') then
  begin
    FileExt := '*.*';
  end;

  FN := FileExt;
  DirName := FullPath;

  if not (SetIfNotCreateDirectory(FullPath)) then
  begin
    result := false;
    exit;
  end;

  i := FindFirst(FN, faAnyFile, SearchRec);

  while (i = 0) do
  begin
    DeleteFile(PChar(DirName + SearchRec.Name));
    i := SysUtils.FindNext(SearchRec);
  end;

  FindClose(SearchRec);
end;

function GetLogFileName(sFILENAME: string {usually Application.Titile}): string;
begin
  sFILENAME := trim(sFILENAME);
  result := 'C:\CLog\' + sFILENAME + GetTheUserName() + FormatDateTime('mmmddyyyy', Now) + '.txt';
end;


function SaveLogFile(sFILENAME, InString: string): Boolean;
var
  t: textfile;
  temp: string;
  MyList: TStringList;
begin
  result := true;

  if not (SetIfNotCreateDirectory()) or (trim(sFILENAME) = '') then
  begin
    result := false;
    exit;
  end;

  temp := sFILENAME;

  sFILENAME := 'C:\CLog\' + temp {+ GetTheUserName() }+ FormatDateTime('mmmddyyyy', Now) + '.txt';

  if (FileExists(sFILENAME)) then
  begin
    try
      assignfile(t, sFILENAME);
      try
        append(t);
      except
        begin
          rewrite(t);
          result := false;
        end;
      end;
      writeln(t, InString);
      flush(t);
    finally
      try
        closeFile(t);
      except
        result := false;
      end;
    end;
  end
  else
  begin
    try
      try
        MyList := TStringList.Create;
      except
        begin
          result := false;
          exit;
        end;
      end;

      MyList.Clear;
      MyList.Add('Start Log File ' + DateTimeToStr(now));
      MyList.Add(InString);
      MyList.SaveToFile(sFILENAME);
    finally
      MyList.Free;
    end;
  end;
end;

// set suffix and fill file path

function SaveLogFileX(sFULLPATH, sFILENAME, {always use Applciation.Title here} sSUFFIX, InString: string): Boolean;
var // savelogfileX('c:\clog\', 'dss_record', '.txt', temp );
    // savelogfileX('\\fileserver\update\crap\', 'dss_record', '.txt', temp );
  t: textfile;
  MyList: TStringList;
 // attempts: Integer;
begin
  result := true;
 /// ass := 'ass1';
  // c:\CLog
  if not (SetIfNotCreateDirectory(sFULLPATH)) or (trim(sFULLPATH) = '') then
  begin

    result := false;
    exit;
  end;

  //holdpath := sFULLPATH;
  sFULLPATH := sFULLPATH + sFILENAME + FormatDateTime('mmmddyyyy', Now) + sSUFFIX;

  if (FileExists(sFULLPATH)) then
  begin
    try
      assignfile(t, sFULLPATH);

     // attempts := 1;

     // while attempts <= 5 do
     // begin
        try
          append(t);
          //attempts := 99;
        except
          begin
            rewrite(t); // results in loss of previous info in file if there is file contention
            result := false;
          //  sleep(100);
          //  inc(attempts);
          end;
        end;
     // end; // while

      //if attempts = 99 then
      //begin
        try
          writeln(t, InString);
          flush(t);
        except;
        end;
      //end
      //else result := false;

    finally
      try
        closeFile(t);
      except
        result := false;
      end;
    end;
  end
  else
  begin
    try
      try
        MyList := TStringList.Create;
      except
        begin
          result := false;
          exit;
        end;
      end;

      MyList.Clear;
      MyList.Add('Start ' + DateTimeToStr(now));
      MyList.Add(InString);
      MyList.SaveToFile(sFULLPATH);
    finally
      MyList.Free;
    end;
  end;
end;

{
  STRIPS ALL Math OPERATOR CHARACTERS OUT OF A STRING.
  EX: 'A+=&*^><!/ MAN, A PLAN, A CANAL, PANAMA'
  RETURNS 'A MAN, A PLAN, A CANAL, PANAMA'
}

function StripOperators(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '+': continue;
        '%': continue;
        '^': continue;
        '&': continue;
        '=': continue;
        '*': continue;
        '/': continue;
        '<': continue;
        '>': continue;
        '|': continue;
        '-': continue;
        '!': continue;
        '?': continue;
        ':': continue;
      else
        temp := temp + InStr[i];
      end;
    end;
  end;
  Result := temp;
end;

function WhiteSpaceTOUnderScores(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        ' ': temp := temp + '_';

      else
        temp := temp + InStr[i];
      end;
    end;
  end;
  Result := temp;
end;




function StripDoubleQuotes(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '"': continue;
      else
        temp := temp + InStr[i];
      end;
    end;
  end;
  Result := temp;
end;

{  STRIPS ALL PUNCTUATION CHARACTERS OUT OF A STRING.
  EX: 'A MAN, A PLAN, A CANAL, PANAMA' RETURNS 'A MAN A PLAN A CANAL PANAMA'  }

function StripPunct(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '!'..'"': continue;
        '''': continue;
        ','..'.': continue;
        ':'..';': continue;
        '?': continue;
        '~': continue;
        '`': continue;
        '�': continue;
      else
        temp := temp + InStr[i];
      end;
    end;
  end;
  Result := temp;
end;

{  STRIPS ALL PUNCTUATION CHARACTERS OUT OF A STRING.
  EX: 'A MAN, A PLAN, A CANAL, PANAMA' RETURNS 'A MAN A PLAN A CANAL PANAMA'  }

function StripCommas(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        ',': continue;
      else
        temp := temp + InStr[i];
      end;
    end;
  end;
  Result := temp;
end;

{  STRIPS ALL WHITE SPACES OUT OF A STRING.
  EX: 'A MAN, A PLAN, A CANAL, PANAMA' RETURNS: 'AMAN,A,PLAN,A,CANAL,PANAMA'  }

function StripWhiteSpace(Instr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);
                 // '0'..'9': Result := True;
    for i := 1 to x do
    begin // strip string
      if InStr[i] = ' ' then continue
      else temp := temp + InStr[i];
    end;
  end;
  Result := temp;
end;

{ STRIPS ALL BUT ALPHA CHARACTERS OUT OF A STRING THEN RETURNS THE COMPACTED STRING
  EX: 'A MAN, A PLAN, A CANAL, PANAMA' RETURNS 'AMANAPLANACANALPANAMA'  }

function StripStr(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        'A'..'Z': temp := temp + InStr[i];
        'a'..'z': temp := temp + InStr[i];
      else
        continue;
      end;
    end;
  end;
  Result := temp;
end;

function StripBinaryStr(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '0': temp := temp + InStr[i];
        '1': temp := temp + InStr[i];
      else
        continue;
      end;
    end;
  end;
  Result := temp;
end;

function IsAllDigits(Instr: string): boolean;
var
  i, x: integer;
begin
  result := true;

  if (InStr = '') then
  begin
    Result := false;
    exit;
  end
  else
  begin
    x := length(InStr);
    for i := 1 to x do
    begin
      if not IsDigit(InStr[i]) then
      begin
        result := false;
        break;
      end;
    end;
  end;
end;

function StrToPassword(InStr: string): string;
var i, x: integer;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    InStr := StripWhiteSpace(InStr);
    InStr := UpperCase(InStr);
    x := length(InStr);
    for i := 1 to x do
    begin
      case InStr[i] of
        'E': InStr[i] := '3';
        'O': InStr[i] := '0';
        'I': Instr[i] := '1';
        'S': InStr[i] := '5';
      else
        continue;
      end;
    end;
  end;
  Result := InStr;
end;

{ CALLS StripStr(string) then returns true if stripped string is a palendrome. }

function IsPalendrome(InStr: string): boolean;
var // recursive solution
  min, max: integer;
  temp: string;
begin
  temp := uppercase(InStr);
  temp := StripStr(temp);
  min := 1;
  max := length(temp);

  if (min >= max) then
    Result := true
  else if (temp[min] = temp[max]) then
  begin
    Delete(temp, 1, 1);
    max := length(temp);
    Delete(temp, max, 1);
    IsPalendrome(temp);
  end
  else
    Result := false;
end;

{ Returns Pig-Latin version of string received }

function PigLatin(engWord: string): string;
var latinWord: string;
begin
  try
    latinWord := Copy(engWord, 2, 256);
    latinWord := latinWord + Copy(engWord, 0, 1) + 'ay';
    result := latinWord;
  except;
  end;
end;

// RETURNS TRUE IF CHARACTER IS PUNCTUATION

function IsPunct(a: Char): boolean;
begin
  case a of
    '!'..'"': Result := True;
    '''': Result := True;
    ','..'.': Result := True;
    ':'..';': Result := True;
    '?': Result := True;
    '~': Result := True;
    '`': Result := True;
    '�': Result := True;
  else
    Result := False;
  end;
end;

// RETUNS TRUE IS CHARACTER IS A WHITESPACE

function IsSpace(a: Char): boolean;
begin
  if (a = ' ') then Result := True
  else Result := False;
end;

// RETURNS TRUE IS CHARACTER IS AN ALPHA

function IsAlpha(a: Char): boolean;
begin
  case a of
    'A'..'Z': Result := True;
    'a'..'z': Result := True;
  else
    Result := False
  end;
end;

// RETURNS TRUE IF CHAR IS A NUMBER

function IsDigit(a: char): boolean;
begin
  case a of
    '0'..'9': Result := True;
  else
    Result := False
  end;
end;

// RETURNS TRUE IF CHAR IS HEXADECIMAL

function IsHex(a: Char): boolean;
begin
  case a of
    'A'..'F': Result := True;
    'a'..'f': Result := True;
    '0'..'9': Result := True;
  else
    Result := False;
  end;
end;

function IsDot(CheckDot: char): Boolean;
begin
  result := false;
  if (CheckDot = '.') then
    result := true;
end;

function IsFloat(CheckStr: string): Boolean;
var
  i, count: integer;
begin
  result := true;
  count := 0;

  if not (CheckStr > '!') then
  begin
    result := false;
    exit;
  end;

  for i := 1 to length(CheckStr) do
  begin // edit boxs are 1-based

    if not IsDigit(CheckStr[i]) then
    begin
      result := false;

      if not IsDot(CheckStr[i]) then exit
      else
      begin
        result := true;
        inc(count);
      end;

      if (count > 1) then
      begin
        result := false;
        exit;
      end;
    end;
  end; // for
end;

function Insult(): string;
var List: TStringList;
  ReturnStr: string;
  max, x: integer;
begin
  List := TStringList.Create();

  List.Add('Why do you always sit on your face?');
  List.Add('Save your air for your blow-up date.');
  List.Add('Brains aren''t everything. In fact in your case they''re nothing.');
  List.Add('Don''t let you mind wander. It''s far too small to be let out on its own.');
  List.Add('I don''t know what makes you so dumb but it really works.');
  List.Add('I don''t think you are an idiot, but what''s my opinion compared to that of thousands of others?');
  List.Add('I''d like to see things from your point of view, but I can''t seem to get my head that far up your ass.');
  List.Add('If I want and shit from you, I''ll squeeze your head.');
  List.Add('I''ve come across rotting bodies that are less offensive than you are.');
  List.Add('You are the kind of person who, when someone first meets you, they don''t like you. But when they get to know you better, they hate you.');
  List.Add('Your face is very becoming. It''s becoming more and more ugly every time I see it.');
  List.Add('Your momma''s so ugly your father takes her to work with him so that he doesn''t have to kiss her goodbye.');
  List.Add('Eat Shit.');
  List.Add('Pig-fucking pig fucker.');
  List.Add('Fuckin'' Fucker.');
  List.Add('Suck Crap, bitch.');
  List.Add('You mushroom capped yogurt slinger.');
  List.Add('I''ve seen better looking bodies at a junk-yard.');
  List.Add('Lick My Shit-Filled Butt.');
  List.Add('Fuck You.');
  List.Add('You As baffled as Adam on Mothers Day.');
  List.Add('You got a mind like the contents of a plain brown envelope.');
  List.Add('Your Mama.');
  List.Add('You are as confused as a blind lesbian in a fish market.');
  List.Add('Your about as interesting as yesterdays shit.');
  List.Add('Yo momma''s so ugly when she joined the ugly contest, they said "Sorry, no professionals."');
  List.Add('Your so ugly that words can''t explain your face. So i will just go throw up.');
  List.Add('Yo Bitch: If my dog were that ugly I would shave his ass and make him walk backwards.');
  List.Add('Hey buddy that''s a nice shirt. What brand is it, clearance?');
  List.Add('You are pure white trash. Get back to your trailer and heat up some spam.');
  List.Add('You have the athletic prowess of a soaking-wet sandwich.');
  List.Add('Most people agree that you have all the charisma of a crusty breast implant.');
  List.Add('You are just slightly more literate than a large pube.');
  List.Add('You smell like an anus.');
  List.Add('Consume excretion and cease to exist.');
  List.Add('May a thousand insects infest your anal pore.');
  List.Add('You stupid piece of shit.');
  List.Add('One could serve a smorgasbord on that fat ass of yours.');
  List.Add('You need a shower.');
  List.Add('You smell like ass.');
  List.Add('I''ll paint that nose for five-hundred dollars.');
  List.Add('You Rectum.');
  List.Add('Take your thumb out of your ass.');
  List.Add('I would listen if you deserved the respect.');
  List.Add('Get a life.');
  List.Add('Bob on My Knob.');
  List.Add('You are ugly as shit.');
  List.Add('Shut your pie hole.');
  List.Add('Hey asswipe, shut your face.');
  List.Add('Pie it you cum guzzlin crack ho');
  List.Add('Fuckin'' knob gobblin'' coke bitch.');
  List.Add('Go suck an egg.');
  List.Add('You are like yesterday''s coffee -- a little weak in the bean.');
  List.Add('You have a one-track mind, and the traffic in it is very light.');
  List.Add('You are so stupid you would buy a topless bathing suit for your half-sister.');
  List.Add('You  have a very sympathetic face. It has everyone''s sympathy.');
  List.Add('You go to the dentist twice a year. Once for each tooth.');
  List.Add('You have a nice head on your shoulders. But it would look better on a neck.');
  List.Add('You are a legend in your own mirror.');
  List.Add('You are so cheap, when you take a dollar bill out of your pocket, George Washington blinks at the light.');
  List.Add('I never forget a face, But in your case I''ll make an exception.');
  List.Add('I worship the ground that awaits your corpse.');
  List.Add('I''ve had many cases of love that were just infatuation, but the hate that I feel for you is the real thing.');
  List.Add('I heard you weren''t fit to fuck pigs, but I''ll advise you are.');
  List.Add('I would have liked to insult you, but the sad truth is that you wouldn''t understand me.');
  List.Add('If you were twice as smart as you are now, you would be really stupid.');
  List.Add('I''m blonde. What''s your excuse?');
  List.Add('I''d like to see things from your point of view, but I can''t seem to get my head that far up your ass.');
  List.Add('If shit was music, you''d be an orchestra.');
  List.Add('If anyone told you to be yourself they would be giving you bad advice.');
  List.Add('There are only two things I dislike about you: Your faces.');
  List.Add('You are not as bad as people say - you are much, much worse.');
  List.Add('You are better at sex than anyone. Now all you need is a partner.');
  List.Add('People would follow you anywhere.....but only out of morbid curiosity.');
  List.Add('Yo momma''s so ugly they filmed "Gorillas in the Mist" in her shower.');
  List.Add('Your inferiority complex is fully justified.');
  List.Add('Bitch!');
  List.Add('You are a few beers short of a six-pack.');
  List.Add('You are an experiment in Artificial Stupidity.');
  List.Add('You are as smart as bait.');
  List.Add('You got a body by Fisher, brains by Mattel.');
  List.Add('You''re dumber than a box of hair.');
  List.Add('Your elevator doesn''t go all the way to the top floor.');
  List.Add('You fell out of the stupid tree and hit every branch on the way down.');
  List.Add('You have an IQ like a football score.');
  List.Add('Your belt doesn''t go through all the loops.');
  List.Add('If you had another brain, it would be lonely.');
  List.Add('You aren''t the sharpest knife in the drawer.');
  List.Add('You are proof that evolution CAN go in reverse.');
  List.Add('The cheese slid off your cracker?');
  List.Add('I can see the lights are on, but nobody''s home.');
  List.Add('Your wheel is spinning, but your hamster is dead.');
  List.Add('You are so fucking stupid; you would think martial arts are paintings by a sheriff.');
  List.Add('I love what you''ve done with your hair. How did you get it to come out of one nostril like that?');
  List.Add('You look like you''ve been pulled backward through a knothole.');
  List.Add('You look like you''ve been ridden hard and put away wet.');
  List.Add('Your face can shrivel a man like a 3-hour bath.');
  List.Add('Your mother is so big, that her graduation picture had to be an aerial view.');
  List.Add('If art imitates life, you''d be a black velvet painting.');
  List.Add('Your face could turn Medusa to stone.');
  List.Add('You have the intelligence of a bucket of rocks.');
  List.Add('Your origins are so low; you''d have to limbo under your family tree.');
  List.Add('You have the shape of a shit-filled sock.');
  List.Add('Your ass looks like a bag of nickels.');
  List.Add('Your mouth is so big; you could suck an egg from a chicken.');
  List.Add('You are as charming as a dead mouse in a loaf of bread.');
  List.Add('You have less backbone than a chocolate �clair.');
  List.Add('Your face looks as if you were weaned on a pickle.');
  List.Add('You have about as much class as a lawn flamingo.');
  List.Add('The eyes are the windows to the mind and your eyes look like the holes in a privy.');
  List.Add('Your fragrance of choice is "Eau De Skunk".');
  List.Add('Please sit down and take a mess off your feet.');
  List.Add('You have the face of a saint. Saint Bernard, that is.');
  List.Add('I refuse to engage in a battle of wits, as I will not take advantage of the handicapped.');
  List.Add('Your skin is so soft, just like the texture of smut that grows on corn.');
  List.Add('You have such a full round face just like a baboon''s butt.');
  List.Add('Your face looks like a baboon''s ass mounted on a ping-pong ball.');
  List.Add('If ignorance is bliss, you must be the happiest person on earth.');
  List.Add('If brains were wind, you would not have enough to blow your nose.');
  List.Add('The only time you ever got to second base was at the petting zoo.');
  List.Add('Is that your face or are you breaking it in for a bulldog?');
  List.Add('You have such a striking face. Tell me, how many times were you struck?');
  List.Add('Travel broadens a person. You look as if you have been all over the world.');
  List.Add('You must have a very large brain, to hold so much ignorance.');
  List.Add('A mudpack is good for the complexion. I suggest you leave it on.');
  List.Add('You are so dumb; you think "getting lucky" is finding a penny on the ground.');
  List.Add('One more wrinkle and you''d pass for a prune.');
  List.Add('If ugly were a crime, you''d get a life sentence.');
  List.Add('Looking at you, I realize what a waste of skin you are.');
  List.Add('Was it hard learning to be so ugly or were you a quick study?');
  List.Add('If your conscience could be surgically removed, it would be a minor operation.');
  List.Add('Is that your nose or did you inhale a cantaloupe?');
  List.Add('You are so dumb; you think a blood vessel is some kind of ship.');
  List.Add('You are so dumb; you planted a dogwood tree and expected a litter of puppies.');
  List.Add('You are so dumb; you thought a buttress was a female goat.');
  List.Add('You are so dumb; you think manual labor is the president of Mexico.');
  List.Add('Stagnant pond scum licker');
  List.Add('Yo, Maggot muncher');
  List.Add('Yo, Latrine lips');
  List.Add('You must come from the shallow end of the gene pool.');
  List.Add('You perplex me. Usually someone with your limited physical appeal makes up for it with a REAL personality.');
  List.Add('Waiting for you to say something intelligent is like putting a candle in the window for Jimmy Hoffa.');
  List.Add('If Moses had seen your face, there would have been another commandment.');
  List.Add('I wish your charm could be bottled; then a cork could be put in it.');
  List.Add('You appear to be the culmination of years of major genetic mishaps.');
  List.Add('How is that bladder control problem?');
  List.Add('You have been fooled more times than a public welfare worker.');
  List.Add('They say Will Rogers never met a man he didn''t like; obviously he never met you.');
  List.Add('I heard you spend your spare time chasing carriages and eating Alpo.');
  List.Add('Your simian countenance suggests a heritage unusually rich in species diversity.');
  List.Add('Your mother is so fat; she''s on both sides of the family.');
  List.Add('You are so dumb, when told that it''s chilly outside, you get a bowl and go outside.');
  List.Add('Your mother is so fat that before god said, "Let there be light," he told her to move out of the way.');
  List.Add('I understand you have Van Gogh''s ear for music.');
  List.Add('I hear you come from a long line of first cousins.');
  List.Add('You were so ugly as a child; your mother breast-fed you through a straw.');
  List.Add('You''re so dumb, you need a cue card to say "Huh?"');
  List.Add('I researched your entire family tree and it seems you were the sap.');
  List.Add('Your mother is like a carpenter''s dream...flat as a board and easy to nail.');
  List.Add('Your mother is so ugly, that when she entered an ugly contest, they said, "Sorry, no professionals."');
  List.Add('Your mother is so fat, her shadow weighs 50 pounds.');
  List.Add('Your momma is so fat, when she goes to the zoo, elephants throw peanuts.');
  List.Add('Your mother is so ugly; her face can stop a sundial.');
  List.Add('Your mother is twice the man you are.');
  List.Add('You''re so dumb; you think a naval academy is a school for belly dancers.');
  List.Add('Your father is so dumb, he has to take his clothes off to count to 21 and even then he only gets to 20-1/2.');
  List.Add('I heard your parents took you to a dog show and you won.');
  List.Add('If you spoke your mind, you''d be speechless.');
  List.Add('Your mother is so fat, when she sings, it actually IS over.');
  List.Add('Your girlfriend tells me that your lovemaking is like going to the dentist; sit back, relax and you won''t feel a thing!');

  max := List.Count - 1;
  Randomize;
  x := Random(max);
  ReturnStr := trim(List[x]);
  List.Free;
  List := nil;
  Result := ReturnStr;
end;

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  try
    try
      VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
      GetMem(VerInfo, VerInfoSize);
      GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
      VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
      with VerValue^ do
      begin
        V1 := dwFileVersionMS shr 16;
        V2 := dwFileVersionMS and $FFFF;
        V3 := dwFileVersionLS shr 16;
        V4 := dwFileVersionLS and $FFFF;
      end;
    except
      V1 := 0; V2 := 0; V3 := 0; V4 := 0;
    end;
  finally FreeMem(VerInfo, VerInfoSize); end;
end;

function GetRegularBuildInfoString: string;
var
  V1, V2, V3, V4: Word;
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := Format('Build: %d.%d.%d.%d', [V1, V2, V3, V4]);
end;

function GetFileInfo(sFULLPATH: string): string;
var
  hFile: THandle;
  FileInfo: BY_HANDLE_FILE_INFORMATION;
begin
  result := '';
  if not fileexists(sFULLPATH) then
  begin
    result := sFULLPATH + ' does not exist';
    exit;
  end;

  hfile := CreateFile(pchar(sFULLPATH),
    GENERIC_READ,
    FILE_SHARE_READ,
    nil,
    OPEN_EXISTING,
    FILE_ATTRIBUTE_ARCHIVE,
    0);

  if GetFileInformationByHandle(hFile, FileInfo) then
  begin
    result :=
      'Serial: ' + IntToStr(FileInfo.dwVolumeSerialNumber) + '|' +
      'fileSizeHigh: ' + IntToStr(FileInfo.nFileSizeHigh) + '|' +
      'FileSizeLow: ' + IntToStr(FileInfo.nFileSizeLow) + '|' +
      'FileIndexHigh: ' + IntToStr(FileInfo.nFileIndexHigh) + '|' +
      'FileIndexLow: ' + IntToStr(FileInfo.nFileIndexLow) + '|' +
      'Number Links: ' + IntToStr(FileInfo.nNumberOfLinks) + '|';
  end;

  try
    CloseHandle(hfile);
  except;
  end;
end;

function ValidGuid(strGuid: string): boolean; var
  intCount, LEN, i: integer;

  function IsBadChar(a: Char): boolean;
  begin
    case a of
      ',', ' ', '~', '`', '!', '#', '$', '%',
        '^', '&', '*', '(', ')', '+', '=', '[', ']', '|', '\', ':', ';', '"',
        '''', '>', '?', '/', '@', '.'
        : Result := True;
    else
      Result := False;
    end;
  end;
begin
  result := false;
  if length(strGuid) <> 38 then
  begin
    exit;
  end
  else
  begin
    if AnsiContainsStr(strGuid, '{') then
    begin
      if AnsiContainsStr(strGuid, '}') then
      begin
        if AnsiStartsStr('{', strGuid) then
        begin
          if AnsiEndsStr('}', strGuid) then
          begin
            if AnsiContainsStr(strGuid, '-') then
            begin
              intCount := Occurs(strGuid, '-');
              if intcount <> 4 then
              begin
                exit;
              end
              else
              begin
                intCount := 0;
                intCount := Occurs(strGuid, '}');
                if intcount = 1 then
                begin
                  intCount := 0;
                  intCount := Occurs(strGuid, '{');
                  if intcount = 1 then
                  begin
                    if AnsiContainsStr(strGuid, ' ') then
                    begin
                      exit;
                    end
                    else
                    begin
                      result := true;
                    end;
                  end
                  else
                  begin
                    exit;
                  end;
                end
                else
                begin
                  exit;
                end;
              end;
            end
            else
            begin
              exit;
            end;
          end
          else
          begin
            exit;
          end;
        end
        else
        begin
          exit;
        end;
      end
      else
      begin
        exit;
      end;
    end
    else
    begin
      exit;
    end;
  end;

  LEN := Length(strGuid);
  for i := 1 to LEN do
  begin
    if (IsBadChar(strGuid[i])) then
    begin
      exit;
    end;
  end;
end;

function IsValidEMailAddr(InAddr: string; var errStr: string): boolean;
var
  sDOM1: string;
  i, atCOUNT, periodCOUNT, LEN: integer;
  // Test for all non - AlphaNumeric characters.
  // A valid email address cannot contain a comma,space,tilde,reverse quote(`),exclamation point(!),Pound symbol,dollar sign($),
  // percent sign(%),carrot sign(^), ampersand, asterisk, parenthesis, parenthesis, plus, equal, open brace, square open bracket([),
  // close brace, close bracket(]), PIPE, backslash(\), colon(:), semicolon(;), double quote, apostrophe, Greater Than symbol,
  // question mark(?) OR forwardslash(/).
  function IsBadChar(a: Char): boolean;
  begin
    case a of
      ',', ' ', '~', '`', '!', '#', '$', '%',
        '^', '&', '*', '(', ')', '+', '=', '{',
        '}', '[', ']', '|', '\', ':', ';', '"',
        '''', '>', '?', '/'
        : Result := True;
    else
      Result := False;
    end;
  end;
begin
  result := true;
  if trim(errStr) = '' then errStr := 'OK EMA';
  InAddr := trim(InAddr);
  LEN := Length(InAddr);

  // must be at least 6 characters in the email (a@a.co)
  if (LEN < 6) then
  begin
    errStr := 'e-mail addresses must be at least 6 characters long.';
    result := false;
    exit;
  end;

  atCOUNT := 0;
  periodCOUNT := 0;

  for i := 1 to LEN do
  begin
    // -An @ sign OR period cannot be the last character
    // -An @ sign OR period cannot be the first character
    // -valid email address contains only one @ OR period
    // -The email address must contain at least one @ OR period

    if (IsBadChar(InAddr[i])) then
    begin
      errStr := 'can not have non alpha-numberic character [ ' + InAddr[i] + ' ] in an e-mail address.';
      result := false;
      exit;
    end;

    // check @ sign
    if (i = 1) and (InAddr[i] = '@') then
    begin
      errStr := 'can not have "@" at the beginning of an e-mail address.';
      result := false;
      exit;
    end
    else if (i = LEN) and (InAddr[i] = '@') then
    begin
      errStr := 'can not have "@" at the end of an e-mail address.';
      result := false;
      exit;
    end;

    // check period
    if (i = 1) and (InAddr[i] = '.') then
    begin
      errStr := 'can not have a period at the beginning of an e-mail address.';
      result := false;
      exit;
    end
    else if (i = LEN) and (InAddr[i] = '.') then
    begin
      errStr := 'can not have a period at the end of an e-mail address.';
      result := false;
      exit;
    end;

    if InAddr[i] = '@' then inc(atCOUNT);
    if InAddr[i] = '.' then inc(periodCOUNT);
  end; // for loop

  if (atCOUNT < 1) then
  begin
    errStr := 'Must have at least one "@" sign in an e-mail address.';
    result := false;
    exit;
  end
  else if (atCOUNT > 1) then
  begin
    errStr := 'Can''t have more than one "@" sign in an e-mail address.';
    result := false;
    exit;
  end;

  if periodCOUNT < 1 then
  begin
    errStr := 'Must have at least one period [.] in an e-mail address.';
    result := false;
    exit;
  end;

  // The User portion of the email (all to the left of the @) must have at least 1 character : this is OK, as verified the 1st char is not an @
  //sUSER := copy(InAddr,1, (pos('@',InAddr)-1) );  // get everything to the left of the @ sign

  // get everything to the right of the @ sign:
  sDOM1 := copy(InAddr, (pos('@', InAddr) + 1), 255);

  // The Domain portion of the email to contain at least 2 parts
  if (pos('.', sDOM1) = 0) then
  begin
    errStr := 'Must have at least two parts separated by a period after the @ sign in an e-mail address';
    result := false;
  end;
end;

function GetNamefromEmailAddr(InEmail: string): string;
var i: Integer;
begin // assume valid email address
      // example chester@autoeurope.com returns "chester"
  result := '';
  i := pos('@', InEMail);
  result := copy(InEMail, 1, i - 1);

end;

function GetDomainFromEmailaddr(InEmail: string): string;
var i: Integer;
begin
  // assume valid email address
  // example chester@autoeurope.com returns "autoeurope.com"
  result := '';
  i := pos('@', InEMail);
  result := trim(copy(InEMail, i + 1, 255));
end;


//*************************************************************************************************************************************************************

function Occurs(const str: string; c: char): integer;
// Returns the number of times a character occurs in a string
var
  p: PChar;
begin
  Result := 0;
  p := PChar(Pointer(str));
  while p <> nil do begin
    p := StrScan(p, c);
    if p <> nil then begin
      inc(Result);
      inc(p);
    end;
  end;
end;

function Occurs(const str: string; const substr: string): integer;
// Returns the number of times a substring occurs in a string
var
  p, q: PChar;
  n: integer;
begin
  Result := 0;
  n := Length(substr);
  if n = 0 then exit;
  q := PChar(Pointer(substr));
  p := PChar(Pointer(str));
  while p <> nil do begin
    p := StrPos(p, q);
    if p <> nil then begin
      inc(Result);
      inc(p, n);
    end;
  end;
end;

function AnsiOccurs(const str: string; const substr: string): integer;
// Returns the number of times a substring occurs in an ANSI string
var
  p, q: PChar;
  n: integer;
begin
  Result := 0;
  n := Length(substr);
  if n = 0 then exit;
  q := PChar(Pointer(substr));
  p := PChar(Pointer(str));
  while p <> nil do begin
    p := AnsiStrPos(p, q);
    if p <> nil then begin
      inc(Result);
      inc(p, n);
    end;
  end;
end;

function Split(const str: string; const separator: string): TStringArray;
// Returns an array with the parts of "str" separated by "separator"
var
  i, n: integer;
  p, q, s: PChar;
begin
  SetLength(Result, Occurs(str, separator) + 1);
  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);
    i := 0;
    repeat
      q := StrPos(p, s);
      if q = nil then q := StrScan(p, #0);
      SetString(Result[i], p, q - p);
      p := q + n;
      inc(i);
    until q^ = #0;
  except
    Result := nil;
    raise;
  end;
end;

function AnsiSplit(const str: string;
  const separator: string): TStringArray;
// Returns an array with the parts of "str" separated by "separator"
// ANSI version
var
  i, n: integer;
  p, q, s: PChar;
begin
  SetLength(Result, AnsiOccurs(str, separator) + 1);
  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);
    i := 0;
    repeat
      q := AnsiStrPos(p, s);
      if q = nil then q := AnsiStrScan(p, #0);
      SetString(Result[i], p, q - p);
      p := q + n;
      inc(i);
    until q^ = #0;
  except
    Result := nil;
    raise;
  end;
end;

function SplitStrings(const str: string; const separator: string;
  Strings: TStrings): TStrings;
// Fills a string list with the parts of "str" separated by
// "separator". If Nil is passed instead of a string list,
// the function creates a TStringList object which has to
// be freed by the caller
var
  n: integer;
  p, q, s: PChar;
  item: string;
begin
  if Strings = nil then
    Result := TStringList.Create
  else
    Result := Strings;
  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);
    repeat
      q := StrPos(p, s);
      if q = nil then q := StrScan(p, #0);
      SetString(item, p, q - p);
      Result.Add(item);
      p := q + n;
    until q^ = #0;
  except
    item := '';
    if Strings = nil then Result.Free;
    raise;
  end;
end;

function AnsiSplitStrings(const str: string; const separator: string;
  Strings: TStrings): TStrings;
// Fills a string list with the parts of "str" separated by
// "separator". If Nil is passed instead of a string list,
// the function creates a TStringList object which has to
// be freed by the caller
// ANSI version
var
  n: integer;
  p, q, s: PChar;
  item: string;
begin
  if Strings = nil then
    Result := TStringList.Create
  else
    Result := Strings;
  try
    p := PChar(str);
    s := PChar(separator);
    n := Length(separator);
    repeat
      q := AnsiStrPos(p, s);
      if q = nil then q := AnsiStrScan(p, #0);
      SetString(item, p, q - p);
      Result.Add(item);
      p := q + n;
    until q^ = #0;
  except
    item := '';
    if Strings = nil then Result.Free;
    raise;
  end;
end;

function ServiceStart(sMachine, sService: string; iSecondsToWait: integer): boolean;
var
  schm,
    schs: SC_Handle;
  ss: TServiceStatus;
  psTemp: PChar;
  dwChkP: DWord;
begin
  iSecondsToWait := iSecondsToWait * 1000;
  schm := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);

  if (schm > 0) then
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_START or SERVICE_QUERY_STATUS);

    if (schs > 0) then
    begin
      psTemp := nil;
      if (StartService(schs, 0, psTemp)) then
      begin
        if (QueryServiceStatus(schs, ss)) then
        begin
          while (SERVICE_RUNNING <> ss.dwCurrentState) do
          begin
            dwChkP := ss.dwCheckPoint;
            Sleep({ss.dwWaitHint}iSecondsToWait);

            if (not QueryServiceStatus(schs, ss)) then break;

            if (ss.dwCheckPoint < dwChkP) then break;
          end;
        end;
      end;
      CloseServiceHandle(schs);
    end
    else
      raise(exception.Create(FormatAPIMessage(GetLastError)));

    CloseServiceHandle(schm);
  end
  else
  begin
    raise(exception.Create(FormatAPIMessage(GetLastError)));
  end;

  Result := SERVICE_RUNNING = ss.dwCurrentState;
end;

function ServiceStatus(sMachine, sService: string): string;
//function ServiceStatus(const ServiceName: string): integer;
var
  SCM, SCH: SC_Handle;
  ServiceStatus: TServiceStatus;
  stat: integer;
begin
  stat := 0;
  result := 'unknown status';
  SCM := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);

  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(sService), SERVICE_ALL_ACCESS);

    if SCH <> 0 then
    begin
      QueryServiceStatus(SCH, ServiceStatus);
      stat := ServiceStatus.dwCurrentState;
      CloseServiceHandle(SCH);
    end
    else
      raise(exception.Create(FormatAPIMessage(GetLastError)));

    CloseServiceHandle(SCM);
  end
  else
    raise(exception.Create(FormatAPIMessage(GetLastError)));

  if stat = 1 then result := 'stopped'
  else if stat = 4 then result := 'running'
  else if stat = 7 then result := 'paused';
end;

function ServiceStop(sMachine, sService: string; iSecondsToWait: integer): boolean;
var schm, // service control mgr handle
  schs: SC_Handle; // service handle
  ss: TServiceStatus;
  dwChkP: DWord; // check point
begin
  iSecondsToWait := iSecondsToWait * 1000;
  schm := OpenSCManager(PChar(sMachine), nil, SC_MANAGER_CONNECT);
  if (schm > 0) then // if success
  begin
    schs := OpenService(schm, PChar(sService), SERVICE_STOP or SERVICE_QUERY_STATUS);
    if (schs > 0) then
    begin
      if (ControlService(schs, SERVICE_CONTROL_STOP, ss)) then
      begin
        // check status
        if (QueryServiceStatus(schs, ss)) then
        begin
          while (SERVICE_STOPPED <> ss.dwCurrentState) do
          begin
            // dwCheckPoint contains a value the service  increments periodically  to report its progress
            // during a lengthy operation. save current value
            dwChkP := ss.dwCheckPoint;

           // idle events should be handled here...

            Sleep({ss.dwWaitHint}iSecondsToWait);
            if (not QueryServiceStatus(schs, ss)) then break;
            // QueryServiceStatus didn't increment dwCheckPoint. avoid infinite loop
            if (ss.dwCheckPoint < dwChkP) then break;
          end;
        end;
      end;

      // close service handle
      CloseServiceHandle(schs);
    end
    else
      raise(exception.Create(FormatAPIMessage(GetLastError)));

    CloseServiceHandle(schm);
  end
  else
    raise(exception.Create(FormatAPIMessage(GetLastError)));
  // return TRUE if service status is stopped
  Result := SERVICE_STOPPED = ss.dwCurrentState;
end;

procedure ServiceNames(sMachine: string; var InList: TStringList; const uType: integer = SERVICE_WIN32);
type
  TEnumServices = array[0..0] of TEnumServiceStatus;
  PEnumServices = ^TEnumServices;
var
  SCM: SC_Handle;
  Services: PEnumServices;
  Len: Cardinal;
  ServiceCount, ResumeHandle, i: Cardinal;
  serviceName, DisplayName: string;
begin
  if not assigned(InList) then exit;
  InList.Clear;
  ResumeHandle := 0;
  SCM := OpenSCManager(PCHAR(sMachine), nil, SC_MANAGER_ALL_ACCESS);
  Len := 0;
  ServiceCount := 0;
  Services := nil;

  try
    if SCM <> 0 then
    begin
      EnumServicesStatus(SCM, uType, SERVICE_STATE_ALL, Services[0], 0, Len, ServiceCount, ResumeHandle);
      GetMem(Services, Len);
      EnumServicesStatus(SCM, {SERVICE_DRIVER or } SERVICE_WIN32, SERVICE_STATE_ALL,
        Services[0], Len, Len, ServiceCount, ResumeHandle);

      InList.Add(Format('%-55s %-25s', ['_Display Name:', '_Service Name:']));

      for i := 0 to ServiceCount - 1 do
      begin
        ServiceName := Services[i].lpServiceName;
        DisplayName := Services[i].lpDisplayName;
        InList.Add(Format('%-55s %-25s', [DisplayName, ServiceName]));
      end;

      inList.Sort;
      FreeMem(Services);
    end
    else raise(exception.Create(FormatAPIMessage(GetLastError)));
  finally
    CloseServiceHandle(SCM);
  end;
end;


{
 M$   Aug 7 2003, 9:56 pm     show options

Newsgroups: borland.public.delphi.nativeapi
From: "M$" <some...@microsoft.com> - Find messages by this author
Date: Fri, 8 Aug 2003 12:54:32 +0800
Local: Thurs, Aug 7 2003 9:54 pm
Subject: Re: How to know if service is running?
Reply to Author | Forward | Print | Individual Message | Show original | Report Abuse

Hi,There are some function,hope it helpful for you.


function ServiceStart(const ServiceName: string): Boolean;
var
  SCM, SCH: SC_Handle;
  P: PChar;
begin
  Result := False;
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      Result := StartService(SCH, 0, P);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function ServiceStop(const ServiceName: string): Boolean;
var
  SCM, SCH: SC_Handle;
  ServiceStatus: TServiceStatus;
begin
  Result := False;
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      Result := ControlService(SCH, SERVICE_CONTROL_STOP, ServiceStatus);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function ServiceShutdown(const ServiceName: string): Boolean;
var
  SCM, SCH: SC_Handle;
  ServiceStatus: TServiceStatus;
begin
  Result := False;
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      Result := ControlService(SCH, SERVICE_CONTROL_SHUTDOWN,
ServiceStatus);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function ServiceStatus(const ServiceName: string): integer;
var
  SCM, SCH: SC_Handle;
  ServiceStatus: TServiceStatus;
begin
  Result := 0;
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      QueryServiceStatus(SCH, ServiceStatus);
      Result := ServiceStatus.dwCurrentState;
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function ServiceRunning(const ServiceName: string): Boolean;
begin
  Result := ServiceStatus(ServiceName) = SERVICE_RUNNING;
end;


function ServiceStopped(const ServiceName: string): Boolean;
begin
  Result := ServiceStatus(ServiceName) = SERVICE_STOPPED;
end;


function ServiceDisplayName(const ServiceName: string): string;
var
  SCM, SCH: SC_Handle;
  L: DWORD;
begin
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      GetServiceDisplayName(SCM, PChar(ServiceName), nil, L);
      SetLength(Result, L);
      GetServiceDisplayName(SCM, PChar(ServiceName), PChar(Result), L);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function ServiceFileName(const Name: string): string;
var
  SCM, SCH: SC_Handle;
  ServiceConfig: PQueryServiceConfig;
  R: DWORD;
begin
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(Name), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      QueryServiceConfig(SCH, nil, 0, R); // Get Buffer Length
      GetMem(ServiceConfig, R + 1);
      QueryServiceConfig(SCH, ServiceConfig, R + 1, R);
      Result := ServiceConfig.lpBinaryPathName�;
      FreeMem(ServiceConfig);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


function QueryServiceConfig2(hService: SC_HANDLE; dwInfoLevel: DWORD;
  lpBuffer: PChar; cbBufSize: DWORD; var pcbBytesNeeded: DWORD): BOOL;
  stdcall; external advapi32 name 'QueryServiceConfig2A';


function ServiceDescription(const ServiceName: string): string;
const
  SERVICE_CONFIG_DESCRIPTION = $00000001;
type
  TServiceDescription = packed record
    lpDescription: LPSTR;
  end;
  PServiceDescription = ^TServiceDescription;
var
  SCM, SCH: SC_Handle;
  P: PServiceDescription;
  R: DWORD;
begin
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCM <> 0 then
  begin
    SCH := OpenService(SCM, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if SCH <> 0 then
    begin
      QueryServiceConfig2(SCH, SERVICE_CONFIG_DESCRIPTION, nil, 0, R); //
Get Buffer Length
      GetMem(P, R + 1);
      QueryServiceConfig2(SCH, SERVICE_CONFIG_DESCRIPTION, PChar(P), R + 1,
R);
      Result := P.lpDescription;
      FreeMem(P);
      CloseServiceHandle(SCH);
    end;
    CloseServiceHandle(SCM);
  end;
end;


procedure ServiceNames(Names: TStrings; DisplayNames: TStrings = nil; const
uType: integer = SERVICE_WIN32);
type
  TEnumServices = array[0..0] of TEnumServiceStatus;
  PEnumServices = ^TEnumServices;
var
  SCM: SC_Handle;
  Services: PEnumServices;
  Len: Cardinal;
  ServiceCount, ResumeHandle, i: Cardinal;
begin
  ResumeHandle := 0;
  SCM := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  Len := 0;
  ServiceCount := 0;
  Services := nil;
  try
    Names.BeginUpdate;
    if Assigned(DisplayNames) then DisplayNames.BeginUpdate;


    if SCM <> 0 then
    begin
      EnumServicesStatus(SCM, uType, SERVICE_STATE_ALL,
        Services[0], 0, Len, ServiceCount, ResumeHandle);
      GetMem(Services, Len);
      EnumServicesStatus(SCM, SERVICE_DRIVER or SERVICE_WIN32,
SERVICE_STATE_ALL,
        Services[0], Len, Len, ServiceCount, ResumeHandle);
      for i := 0 to ServiceCount - 1 do
      begin
        Names.Add(Services[i].lpServic�eName);
        if Assigned(DisplayNames) then
DisplayNames.Add(Services[i].l�pDisplayName);
      end;
      FreeMem(Services);
    end;
  finally
    Names.EndUpdate;
    if Assigned(DisplayNames) then DisplayNames.EndUpdate;
    CloseServiceHandle(SCM);
  end;
end;



}

{JMClasses implementation ends here}

{JRCommon implementation starts here}
///twebbrowser


//List Web Form Names
function WebFormNames(const document: IHTMLDocument2): TStringList;
 var
   forms : IHTMLElementCollection;
   form : IHTMLFormElement;
   idx : integer;
 begin
   forms := document.Forms as IHTMLElementCollection;
   result := TStringList.Create;
   for idx := 0 to -1 + forms.length do
   begin
     form := forms.item(idx,0) as IHTMLFormElement;
     result.Add(form.name) ;
   end;
 end;

 //get the instance of a web form by index
 function WebFormGet(const formNumber: integer; const document: IHTMLDocument2): IHTMLFormElement;
 var
   forms : IHTMLElementCollection;
 begin
   forms := document.Forms as IHTMLElementCollection;
   result := forms.Item(formNumber,'') as IHTMLFormElement
 end;
 ///END twebbrowser

function SmartUTF8Decode(Input: string): widestring;
var
  strTemp1: widestring;
begin
  strTemp1 := UTF8Decode(Input);
  Result := iif((strTemp1 = ''), Input, strTemp1);
end;

function ShortStrToMonth(strMonth: string): integer;
var
  strDate: string;
  intMonth: integer;
begin
  intMonth := 0;
  strDate := lowercase(strMonth);


  if strDate = 'jan' then
  begin
    intMonth := 1;
  end
  else if strDate = 'feb' then
  begin
    intMonth := 2;
  end
  else if strDate = 'mar' then
  begin
    intMonth := 3;
  end
  else if strDate = 'apr' then
  begin
    intMonth := 4;
  end
  else if strDate = 'may' then
  begin
    intMonth := 5;
  end
  else if strDate = 'jun' then
  begin
    intMonth := 6;
  end
  else if strDate = 'jul' then
  begin
    intMonth := 7;
  end
  else if strDate = 'aug' then
  begin
    intMonth := 8;
  end
  else if strDate = 'sep' then
  begin
    intMonth := 9;
  end
  else if strDate = 'oct' then
  begin
    intMonth := 10;
  end
  else if strDate = 'nov' then
  begin
    intMonth := 11;
  end
  else if strDate = 'dec' then
  begin
    intMonth := 12;
  end;

  result := intmonth;

end;


function IsIntegerOrBackspace(key: char): boolean;
begin
  if not (key in ['0'..'9', #8]) then
  begin
    result := false;
  end
  else
  begin
    result := true;
  end;
end;

function Vc(C: string): Integer;
var
  Card: string[21];
  VCard: array[0..21] of Byte absolute Card;
  XCard: Integer;
  Cstr: string[21];
  y,
    x: Integer;
begin
  Cstr := '';
  FillChar(VCard, 22, #0);
  Card := C;
  for x := 1 to 20 do
    if (VCard[x] in [48..57]) then
      Cstr := Cstr + Chr(VCard[x]);
  Card := '';
  Card := Cstr;
  XCard := 0;
  if not odd(Length(Card)) then
    for x := (Length(Card) - 1) downto 1 do
    begin
      if odd(x) then
        y := ((VCard[x] - 48) * 2)
      else
        y := (VCard[x] - 48);
      if (y >= 10) then
        y := ((y - 10) + 1);
      XCard := (XCard + y)
    end
  else
    for x := (Length(Card) - 1) downto 1 do
    begin
      if odd(x) then
        y := (VCard[x] - 48)
      else
        y := ((VCard[x] - 48) * 2);
      if (y >= 10) then
        y := ((y - 10) + 1);
      XCard := (XCard + y)
    end;
  x := (10 - (XCard mod 10));
  if (x = 10) then
    x := 0;
  if (x = (VCard[Length(Card)] - 48)) then
    Vc := Ord(Cstr[1]) - Ord('2')
  else
    Vc := 0
end;


function DeleteLineBreaks(const S: string): string;
var
  Source, SourceEnd: PChar;
begin
  Source := Pointer(S);
  SourceEnd := Source + Length(S);
  while Source < SourceEnd do
  begin
    case Source^ of
      #10: Source^ := #32;
      #13: Source^ := #32;
    end;
    Inc(Source);
  end;
  Result := S;
end;



function IncOffsetCommon(TheDateTime: TDateTime; MaineOffset: extended): TDateTime;
begin
  Result := IncMinute(TheDateTime, Trunc(MaineOffset * 60));
end;

 //

function Encode64(S: string): string;
var
  i: Integer;
  a: Integer;
  x: Integer;
  b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Ord(s[i]);
    b := b * 256 + x;
    a := a + 8;
    while a >= 6 do
    begin
      a := a - 6;
      x := b div (1 shl a);
      b := b mod (1 shl a);
      Result := Result + Codes64[x + 1];
    end;
  end;
  if a > 0 then
  begin
    x := b shl (6 - a);
    Result := Result + Codes64[x + 1];
  end;
end;

function Decode64(S: string): string;
var
  i: Integer;
  a: Integer;
  x: Integer;
  b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Pos(s[i], codes64) - 1;
    if x >= 0 then
    begin
      b := b * 64 + x;
      a := a + 6;
      if a >= 8 then
      begin
        a := a - 8;
        x := b shr a;
        b := b mod (1 shl a);
        x := x mod 256;
        Result := Result + chr(x);
      end;
    end
    else
      Exit;
  end;
end;


procedure ListViewSearch(LstView: TListView; strSearchText: string);
var
  lvItem: TListItem;
begin
  lvItem := LstView.FindCaption(0, // StartIndex: Integer;
    uppercase(trim(strSearchText)), // Search string: string;
    True, // Partial,
    True, // Inclusive
    False); // Wrap  : boolean;
  if lvItem <> nil then
  begin
    LstView.Selected := lvItem;
    lvItem.MakeVisible(True);
    LstView.SetFocus;
  end;

end;

///TreeView

//get the first node found under a specified parent none
//Example: TTreeNode := GetNodeByText(TreeView1, TreeView1.Selected,
//                     Edit1.Text, true);

function GetNodeByText
  (ATree: TTreeView; InNode: TTreeNode;
  AValue: string; AVisible: boolean): TTreeNode; overload;
var
  NodeText: string;
  NextNode: TTreeNode;
begin
  Result := nil;
  NextNode := InNode.GetFirstChild;
  while NextNode <> nil do
  begin
    NodeText := NextNode.Text;
    if UpperCase(NodeText) = UpperCase(AValue) then
    begin
      NextNode.Selected := AVisible;
      Break;
    end
    else
      NextNode := NextNode.getNextSibling;
  end; //while
  Result := NextNode;
end;

//get the first node found

function GetNodeByText(ATree: TTreeView; AValue: string;
  AVisible: Boolean): TTreeNode; overload;
var
  Node: TTreeNode;
begin
  Result := nil;
  if ATree.Items.Count = 0 then Exit;
  Node := ATree.Items[0];
  while Node <> nil do
  begin
    if UpperCase(Node.Text) = UpperCase(AValue) then
    begin
      Result := Node;
      if AVisible then
        Result.MakeVisible;
      Break;
    end;
    Node := Node.GetNext;
  end;
end;

procedure UpOneLevel(ATree: TTreeView);
var
  Node: TTreeNode;
  bolFirst: boolean;
begin
 //are we a top level node?
  try
    bolFirst := ATree.Selected.IsFirstNode;

    if bolFirst then
    begin
    //top level so exit
      exit;
    end
    else
    begin

    //are we still on the tree
      if ATree.Selected.Parent <> nil then
      begin
      //set the node to the parent node
        Node := ATree.Selected.Parent;
      //select the parent node
        Node.Selected := true;

      end;
    end;


  except on e: exception do
    begin
      //supress the error
    end;
  end;

end;

function IsTreeviewFullyExpanded(ATree: TTreeview): Boolean;
var
  Node: TTreeNode;
begin
  Assert(Assigned(ATree));
  if ATree.Items.Count > 0 then
  begin
    Node := ATree.Items[0];
    Result := True;
    while Result and Assigned(Node) do
    begin
      Result := Node.Expanded or not Node.HasChildren;
      Node := Node.GetNext;
    end; {While}
  end {If}
  else
    Result := False
end;

function IsTreeviewFullyCollapsed(ATree: TTreeview): Boolean;
var
  Node: TTreeNode;
begin
  Assert(Assigned(ATree));
  if ATree.Items.Count > 0 then
  begin
    Node := ATree.Items[0];
    Result := True;
    while Result and Assigned(Node) do
    begin
      Result := not (Node.Expanded and Node.HasChildren);
      Node := Node.GetNext;
    end; {While}
  end {If}
  else
    Result := False
end;

function IsDuplicateName(Node: TTreeNode; NewName: string;
  Inclusive: boolean): boolean;
var
  TestNode: TTreeNode;
begin
  if (Node = nil) then
  begin
    Result := false;
    Exit;
  end;
  if (Inclusive) then
    if (CompareText(Node.Text, NewName) = 0) then
    begin
      Result := true;
      Exit;
    end;
  TestNode := Node;
  repeat
    TestNode := TestNode.GetPrevSibling;
    if (TestNode <> nil) then
      if (CompareText(TestNode.Text, NewName) = 0) then
      begin
        Result := true;
        Exit;
      end;
  until (TestNode = nil);

  TestNode := Node;
  repeat
    TestNode := TestNode.GetNextSibling;
    if (TestNode <> nil) then
      if (CompareText(TestNode.Text, NewName) = 0) then
      begin
        Result := true;
        Exit;
      end;
  until (TestNode = nil);
  Result := false;
end;


function DeleteNodeByName(ATree: TTreeview; strText: string): string;
var
  i: integer;
  bFound: boolean;
begin
  i := 0;
  bFound := false;
  while (not bFound) and (i < ATree.Items.Count - 1) do
  begin
    bFound := (ATree.Items[i].Text = strText);
    if not bFound then i := i + 1;
  end;
  if bFound then
    ATree.Items[i].Destroy;
  ATree.Refresh;
end;

///String

function GetBetween(InStr, InDelim1, InDelim2: string): string;
var i: Integer; // return a string between the two specified delimiters
  temp: string;
begin
  result := '';
  if (inDelim1 = '') or (inDelim2 = '') or (InStr = '') then exit;

  temp := '';
  i := pos(InDelim1, InStr);

  if i > 0 then temp := copy(InStr, i + 1, length(InStr))
  else exit;

  i := 0;
  i := pos(InDelim2, temp);

  if i > 0 then temp := copy(temp, 0, i - 1)
  else exit;
  result := temp;
end;

function ReplaceSubString(DestString, ReplaceString, NewValueString: string): string;
var
  Position: Integer;
begin
  Position := Pos(ReplaceString, DestString);
  Delete(DestString, Position, Length(ReplaceString));
  Insert(NewValueString, DestString, Position);
  Result := DestString;
end;

function SearchAndReplace(sSrc, sLookFor, sReplaceWith: string): string;
var
  nPos, nLenLookFor: integer;
begin
  nPos := Pos(sLookFor, sSrc);
  nLenLookFor := Length(sLookFor);
  while (nPos > 0) do begin
    Delete(sSrc, nPos, nLenLookFor);
    Insert(sReplaceWith, sSrc, nPos);
    nPos := Pos(sLookFor, sSrc);
  end;
  Result := sSrc;
end;

///DateTime////

function GetlastMonth(const D: TDateTime): integer;
var
  intCurMonth, intMonth: integer;
begin
  intCurMonth := MonthOfTheYear(D);

  case intCurMonth of
    1: begin
        intMonth := 12;
      end;
    2: begin
        intMonth := 1;
      end;
    3: begin
        intMonth := 2;
      end;

    4: begin
        intMonth := 3;
      end;
    5: begin
        intMonth := 4;
      end;

    6: begin
        intMonth := 5;
      end;
    7: begin
        intMonth := 6;
      end;
    8:
      begin
        intMonth := 7;
      end;

    9: begin
        intMonth := 8;
      end;
    10: begin
        intMonth := 9;
      end;
    11: begin
        intMonth := 10;
      end;
    12: begin
        intMonth := 11;
      end;
  end;

  result := intMonth;

end;

function GetSundayWeek(const D: TDateTime): TDateTime;
var
  intDay: integer;
begin
  intDay := DayOfWeek(D);
  intDay := (intDay + 6);

  result := now - intDay;
end;

function GetSunday(const D: TDateTime): TDateTime;
var
  intDay: integer;
begin
  intDay := DayOfWeek(D);
  intDay := (intDay - 1);

  result := now - intDay;
end;

function ExtractYear(const D: TDateTime): word;
{return the year portion of the date specified}
var
  yr, mo, dy: word;
begin
  decodedate(D, yr, mo, dy);
  result := yr;
end;

function ExtractLastYear(const D: TDateTime): word;
{return last year from the year portion of the date specified}
var
  yr, mo, dy: word;
begin
  decodedate(D, yr, mo, dy);
  yr := yr - 1;
  result := yr;
end;


function EndOfTime: TDateTime;
{ return a date notionally representing the end of time}
begin
  result := encodedate(4096, 12, 31);
end;

function BegOfTime: TDateTime;
{ return a date notionally representing the beginning of time}
begin
  result := encodedate(1, 1, 1);
end;

function NotAValidDate: TDatetime;
{ return a value which we deem to be a non-date (actually 30 dec 1899) }
begin
  result := -700000;
end;

function NiceSmallDate(const D: TDateTime): string;
{ string representation of date in dd mmm yy format
  This is compatible with PARSEDATE()}
begin
  if D = NotAValidDate then result := NODATESTR
  else
    if D = BegOfTime then result := BEGOFTIMESTR
    else
      if D = EndOfTime then result := ENDOFTIMESTR
      else result := formatdatetime('d mmm yy', D);
end;

function MinutesPast(TheDate: Tdate): integer;

const
  SecsPerDay: Integer = 60 * 60 * 24;
  SecsPerHour: Integer = 60 * 60;
  SecsPerMinute: Integer = 60;
  SecsPerSecond: Integer = 1;
var
  TimeDiff, Minutes: Integer; begin
  TimeDiff := SecondsBetween(Now, TheDate);

  if TimeDiff >= 60 then
  begin

    Minutes := TimeDiff div SecsPerminute;

  end
  else
    Minutes := 0;
  begin
  end;


  Result := Minutes;

end;

function GetDurationString(NewTime, OldTime: TDateTime): string;
var
  Duration: TDurationRec;
  intHours: integer;
  strHours, strMinutes, strSeconds, strFormatted: string;
begin
  duration := GetDuration(NewTime, OldTime);
  intHours := (duration.days * 24) + (duration.hours);
  strHours := inttostr(intHours);
  strMinutes := inttostr(duration.minutes);
  strSeconds := inttostr(duration.seconds);


  if length(strHours) = 1 then
  begin
    strHours := '0' + strHours;
  end;

  if length(strMinutes) = 1 then
  begin
    strMinutes := '0' + strMinutes;
  end;

  if length(strSeconds) = 1 then
  begin
    strSeconds := '0' + strSeconds;
  end;

  strFormatted := inttostr(intHours) + ':' + strMinutes
    + ':' + strSeconds;
  result := strFormatted;
end;

function GetDurationStringMin(NewTime, OldTime: TDateTime): string;
var
  Duration: TDurationRec;
  intHours: integer;
  strHours, strMinutes, strSeconds, strFormatted: string;
begin
  duration := GetDuration(NewTime, OldTime);
  intHours := (duration.days * 24) + (duration.hours);
  strHours := inttostr(intHours);
  strMinutes := inttostr(duration.minutes);
  strSeconds := inttostr(duration.seconds);


  if length(strHours) = 1 then
  begin
    strHours := '0' + strHours;
  end;

  if length(strMinutes) = 1 then
  begin
    strMinutes := '0' + strMinutes;
  end;


  strFormatted := inttostr(intHours) + ':' + strMinutes;

  result := strFormatted;
end;

function GetDaysDateTwoPeriods(StartDate, EndDate: TDateTime): TStringArray;
var
  intDaysinSample: integer;
begin
  intDaysinSample := DaysBetween(StartDate, EndDate);

///
end;


///System

function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function GetUserFromWindows: string;
var
  UserName: string;
  UserNameLen: Dword;
begin
  UserNameLen := 255;
  SetLength(userName, UserNameLen);
  if GetUserName(PChar(UserName), UserNameLen) then
    Result := Copy(UserName, 1, UserNameLen - 1)
  else
    Result := 'Unknown';
end;

procedure WriteINIValueInteger(strPath, strSection, strKey: string; intValue: integer);
var
  myINI: TINIFile;
begin
  myINI := TINIFile.Create(trim(strPath));
  myINI.WriteInteger(trim(strSection), trim(strKey), intValue);
  freeandnil(myINI);
end;


function FetchINIValue(strPath, strSection, strKey: string): string;
var
  myINI: TINIFile;
  strIniValue: string;
begin
  myINI := TINIFile.Create(trim(strPath));
  strIniValue := myINI.ReadString(strSection, strKey, 'failure');
  freeandnil(myINI);
  result := trim(strIniValue);

end;

function SetDirectoryForAEUser(const strUser: string): string;
var
  dir: string;
begin
  result := '';
  dir := 'c:\ae\_' + trim(strUser) + '\';
  if forcedirectories(dir) then result := dir;
end;

function SetDirectoryForAEUser(): string;
var
  dir: string;
begin
  result := '';
  dir := 'c:\ae\_' + trim(GetUserFromWindows()) + '\';
  if forcedirectories(dir) then result := dir;
end;

function GetExeByExtension(sExt: string): string;
var
  sExtDesc: string;
begin
  with TRegistry.Create do
  begin
    try
      RootKey := HKEY_CLASSES_ROOT;
      if OpenKeyReadOnly(sExt) then
      begin
        sExtDesc := ReadString('');
        CloseKey;
      end;
      if sExtDesc <> '' then
      begin
        if OpenKeyReadOnly(sExtDesc + '\Shell\Open\Command') then
        begin
          Result := ReadString('');
        end
      end;
    finally
      Free;
    end;
  end;

  if Result <> '' then
  begin
    if Result[1] = '"' then
    begin
      Result := Copy(Result, 2, -1 + Pos('"', Copy(Result, 2, MaxINt)));
    end
  end;
end;

procedure ShellByEXEFileName(sEXE, sFileName: string);
var
  sApp: string;
begin
  sApp := GetExeByExtension('.XLS');
  if (sApp <> '') and FileExists(sApp) then
  begin
    ShellExecute(0, 'OPEN', PChar(sApp), PChar(sFileName), nil, SW_SHOWNORMAL);
  end;
end;

procedure ShellByEXEFileName(WinHand: HWND; sEXE, sFileName: string); overload;
var
  sApp: string;
begin
  sApp := GetExeByExtension('.htm');
  if (sApp <> '') and FileExists(sApp) then
  begin
    ShellExecute(WinHand, 'OPEN', PChar(sApp), PChar(sFileName), nil, SW_SHOWNORMAL);
  end;
end;


//Email

function IsValidEmail(const Value: string): boolean;
  function CheckAllowed(const s: string): boolean;
  var
    i: integer;
  begin
    Result := false;
    for i := 1 to Length(s) do
    begin
      // illegal char in s -> no valid address
      if not (s[i] in ['a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.']) then
        Exit;
    end;
    Result := true;
  end;
var
  i: integer;
  namePart, serverPart: string;
begin // of IsValidEmail
  Result := false;
  i := Pos('@', Value);
  if (i = 0) or (pos('..', Value) > 0) then
    Exit;
  namePart := Copy(Value, 1, i - 1);
  serverPart := Copy(Value, i + 1, Length(Value));
  if (Length(namePart) = 0) // @ or name missing
    or ((Length(serverPart) < 4)) // name or server missing or
    then Exit; // too short
  i := Pos('.', serverPart);
  // must have dot and at least 3 places from end
  if (i < 2) or (i > (Length(serverPart) - 2)) then
    Exit;
  Result := CheckAllowed(namePart) and CheckAllowed(serverPart);
end;
//stream

function ComponentToString(Component: TComponent): string;

var
  BinStream: TMemoryStream;
  StrStream: TStringStream;
  s: string;
begin
  BinStream := TMemoryStream.Create;
  try
    StrStream := TStringStream.Create(s);
    try
      BinStream.WriteComponent(Component);
      BinStream.Seek(0, soFromBeginning);
      ObjectBinaryToText(BinStream, StrStream);
      StrStream.Seek(0, soFromBeginning);
      Result := StrStream.DataString;
    finally
      StrStream.Free;

    end;
  finally
    BinStream.Free
  end;
end;

function StringToComponent(Value: string): TComponent;
var
  StrStream: TStringStream;
  BinStream: TMemoryStream;
begin
  StrStream := TStringStream.Create(Value);
  try
    BinStream := TMemoryStream.Create;
    try
      ObjectTextToBinary(StrStream, BinStream);
      BinStream.Seek(0, soFromBeginning);
      Result := BinStream.ReadComponent(nil);

    finally
      BinStream.Free;
    end;
  finally
    StrStream.Free;
  end;
end;

//log

function SaveToLogFile(sFILENAME, InString: string): Boolean;
var
  t: textfile;
  MyList: TStringList;
begin
  result := true;
  try
    if not (SetIfNotCreateDirectory()) or (trim(sFILENAME) = '') then
    begin
      result := false;
      exit;
    end;
    if (FileExists(sFILENAME)) then
    begin
      try
        assignfile(t, sFILENAME);
        try
          append(t);
        except
          begin
            rewrite(t);
            result := false;
          end;
        end;
        writeln(t, InString);
        flush(t);
      finally
        try
          closeFile(t);
        except
          result := false;
        end;
      end;
    end
    else
    begin
      try
        try
          MyList := TStringList.Create;
        except
          begin
            result := false;
            exit;
          end;
        end;
        MyList.Clear;
        MyList.Add('Start Log File ' + DateTimeToStr(now));
          //MyList.Add('IATA,Qs,Pages');
        MyList.Add(InString);
        MyList.SaveToFile(sFILENAME);
      finally
        try;
          mylist.Clear;
          mylist.Free;
        except;
        end;
      end;
    end;
  except on E: Exception do
    begin
      Result := False;
      //SaveToLogFile(ErrorLog, 'SQL: ' + e.Message);
    end;
  end;
end;

//-------------------------------------
// get service status
//
// return status code if successful
// -1 if not
//
// return codes:
//   SERVICE_STOPPED
//   SERVICE_RUNNING
//   SERVICE_PAUSED
//
// following return codes
// are used to indicate that
// the service is in the
// middle of getting to one
// of the above states:
//   SERVICE_START_PENDING
//   SERVICE_STOP_PENDING
//   SERVICE_CONTINUE_PENDING
//   SERVICE_PAUSE_PENDING
//
// sMachine:
//   machine name, ie: \SERVER
//   empty = local machine
//
// sService
//   service name, ie: Alerter
//

function ServiceGetStatus(
  sMachine,
  sService: string): DWord;
var
  //
  // service control
  // manager handle
  schm,
  //
  // service handle
  schs: SC_Handle;
  //
  // service status
  ss: TServiceStatus;
  //
  // current service status
  dwStat: DWord;
begin
  //dwStat := -1;

  // connect to the service
  // control manager
  schm := OpenSCManager(
    PChar(sMachine),
    nil,
    SC_MANAGER_CONNECT);

  // if successful...
  if (schm > 0) then
  begin
    // open a handle to
    // the specified service
    schs := OpenService(
      schm,
      PChar(sService),
      // we want to
      // query service status
      SERVICE_QUERY_STATUS);

    // if successful...
    if (schs > 0) then
    begin
      // retrieve the current status
      // of the specified service
      if (QueryServiceStatus(
        schs,
        ss)) then
      begin
        dwStat := ss.dwCurrentState;
      end;

      // close service handle
      CloseServiceHandle(schs);
    end;

    // close service control
    // manager handle
    CloseServiceHandle(schm);
  end;

  Result := dwStat;
end;
////


function SurroundString(Delimiter, Input: string): string;
begin
  Result := Delimiter + Input + Delimiter;
end;

function Quoted(Input: string): string;
begin
  Input := AnsiReplaceStr(Input, '''', '''''');
  Result := SurroundString('''', Input);
end;

procedure InitValue(Variable, Value: variant);
begin
  Variable := Value;
end;

function iif(Condition: boolean; IfTrue, IfFalse: Variant): Variant;
begin
  if Condition then
  begin
    Result := IfTrue;
  end
  else
  begin
    Result := IfFalse;
  end;
end;

function Tagged(Open, Input, Close: string): string;
begin
  Result := Open + Input + Close;
end;

procedure GetAllBetweenString(InStr, InDelim1, InDelim2: string; Output: TStringList);
//return all strings found between the two specified delimiters
var
  temp: string;
  IsMoreStrings: Boolean;
begin
  temp := InStr;

  if (inDelim1 = '') or
    (inDelim2 = '') or
    (InStr = '') or
    (not AnsiContainsStr(InStr, InDelim1)) then Exit;

  IsMoreStrings := TRUE;

  while IsMoreStrings do
  begin
    Output.Add(GetBetweenStringJ(temp, InDelim1, InDelim2));
    temp := AnsiRightStr(temp, Length(temp) - pos(InDelim2, temp));
    IsMoreStrings := ((AnsiContainsStr(temp, InDelim1)) and (AnsiContainsStr(temp, InDelim2)));
  end;
end;

function GetBetweenStringJ(InStr, InDelim1, InDelim2: string): string;
var i: Integer; // return a string between the two specified delimiters
  temp: string;
begin
  result := '';
  if (inDelim1 = '') or (inDelim2 = '') or (InStr = '') then exit;
  temp := '';

  i := pos(InDelim1, InStr);
  if i > 0 then temp := copy(InStr, i + length(InDelim1), length(InStr))
  else exit;

  i := 0;
  i := pos(InDelim2, temp);

// if i > 0 then temp := copy(temp, 0, i - 1)
//  else exit;

  if i > 0 then temp := copy(temp, 0, i - Length(InDelim2))
  else exit;

  result := temp;
end;

//used mostly for populating combo boxes
 {
procedure SimpleSQLQuery(System, strSQL, strFieldName, strError: string; strlResult: TStringList);
var
  PopConn: TFDCOnnection;
  PopQ: TFDQuery;
begin
  try
    try
      PopConn := TFDCOnnection.Create(nil);
      PopQ := TFDQuery.Create(nil);
      set_IDAC_Connection(PopConn, System);
      PopQ.Connection := PopConn;

      with PopQ do
      begin
        SQL.Text := strSQL;
        Open;
        strlResult.Clear;
        while not EOF do
        begin
          strlResult.Add(FieldByName(strFieldName).AsString);
          Next;
        end;
      end;

    except on e: exception do
      begin
        raise exception.Create(e.message + ' TfrmRedbook.procedure PopulateCBOs');
      end;
    end;
  finally
    FreeAndNil(PopQ);
    FreeAndNil(PopConn);
  end;
end;
}

//used mostly for populating combo boxes
{
procedure SimpleSQLQuery2(System, strSQL, strFieldName1,
  strFieldName2, strError: string; strlResult: TStringList);
var
  PopConn: TFDCOnnection;
  PopQ: TFDQuery;
begin
  try
    try
      PopConn := TFDCOnnection.Create(nil);
      PopQ := TFDQuery.Create(nil);
      set_IDAC_Connection(PopConn, System);
      PopQ.Connection := PopConn;

      with PopQ do
      begin
        SQL.Text := strSQL;
        Open;
        strlResult.Clear;
        while not EOF do
        begin
          strlResult.Add(FieldByName(strFieldName1).AsString + '=' + FieldByName(strFieldName2).AsString);
          Next;
        end;
      end;

    except on e: exception do
      begin
        raise exception.Create(e.message + ' SimpleSQLQuery2');
      end;
    end;
  finally
    FreeAndNil(PopQ);
    FreeAndNil(PopConn);
  end;
end;
}

function BuildSFQString(TableName: string): string;
begin
  try
    try
      Result := 'select dbinfo(''sqlca.sqlerrd1'') as SerialID from systables where ' +
        'tabname = ' + Quoted(TableName);
    except on e: exception do
      begin
        raise exception.Create(e.message + ' ');
      end;
    end;
  finally
    //
  end;
end;

function NextPos(Start: Integer; Needle, Haystack: string): Integer;
//returns the position of the first character of the Needle that occurs after Start
var
  strTemp: string;
begin
  strTemp := AnsiRightStr(Haystack, Length(Haystack) - Start);
  result := iif((AnsiContainsStr(strTemp, Needle)), (Pos(Needle, strTemp) + Start), -1);
end;

function GetMemoryUsage: LongInt;
var
  pmc: PPROCESS_MEMORY_COUNTERS;
  cb: Integer;
  MemStat: tMemoryStatus;
begin
  try
    try
    {
      MemStat.dwLength:=SizeOf(MemStat);
      GlobalMemoryStatus(MemStat);

      // Get the total and available system memory
      MemTest3.Lines.Add('Total system memory: ' +
         FormatFloat('###,###',MemStat.dwTotalPhys/1024)+' KByte');
      MemTest3.Lines.Add('Free physical memory: '+
         FormatFloat('###,###',MemStat.dwAvailPhys/1024)+' KByte');
    }
      // Get the used memory for the current process
      cb := SizeOf(TProcessMemoryCounters);
      GetMem(pmc, cb);
      pmc^.cb := cb;
      if GetProcessMemoryInfo(GetCurrentProcess(), pmc, cb) then
      begin
        Result := Longint(pmc^.WorkingSetSize) div 1024;
      end;

    except on e: exception do
      begin
        raise exception.Create(e.message + ' GetMemoryUsage');
      end;
    end;
  finally
    FreeMem(pmc);
  end;
end;

function IsOdd(Input: Integer): Boolean;
begin
  Result := ((Input mod 2) = 1);
end;


function IsValidEMailAddress(InAddr: string; var errStr: string): boolean;
var
  sDOM1: string;
  i, atCOUNT, periodCOUNT, LEN: integer;
  GoodChars: set of char;

  function IsBadChar(a: Char): boolean;
  begin
    Result := iif((a in GoodChars), False, True);
  end;

begin
  GoodChars := ['A'..'z', '0'..'9', '!', '#', '$', '%', '&', '''', '*', '+', '-',
    '/', '=', '?', '^', '_', '`', '{', '|', '}', '~', '.', '@'];
  result := true;
  if trim(errStr) = '' then errStr := 'OK EMA';
  InAddr := trim(InAddr);
  LEN := Length(InAddr);

  // must be at least 6 characters in the email (a@a.co)
  if (LEN < 6) then
  begin
    errStr := 'e-mail addresses must be at least 6 characters long.';
    result := false;
    exit;
  end;

  atCOUNT := 0;
  periodCOUNT := 0;

  for i := 1 to LEN do
  begin
    // -An @ sign OR period cannot be the last character
    // -An @ sign OR period cannot be the first character
    // -valid email address contains only one @ OR period
    // -The email address must contain at least one @ OR period

    if (IsBadChar(InAddr[i])) then
    begin
      errStr := 'can not have non alpha-numberic character [ ' + InAddr[i] + ' ] in an e-mail address.';
      result := false;
      exit;
    end;

    // check @ sign
    if (i = 1) and (InAddr[i] = '@') then
    begin
      errStr := 'can not have "@" at the beginning of an e-mail address.';
      result := false;
      exit;
    end
    else if (i = LEN) and (InAddr[i] = '@') then
    begin
      errStr := 'can not have "@" at the end of an e-mail address.';
      result := false;
      exit;
    end;

    // check period
    if (i = 1) and (InAddr[i] = '.') then
    begin
      errStr := 'can not have a period at the beginning of an e-mail address.';
      result := false;
      exit;
    end
    else if (i = LEN) and (InAddr[i] = '.') then
    begin
      errStr := 'can not have a period at the end of an e-mail address.';
      result := false;
      exit;
    end;

    if InAddr[i] = '@' then inc(atCOUNT);
    if InAddr[i] = '.' then inc(periodCOUNT);
  end; // for loop

  if (atCOUNT < 1) then
  begin
    errStr := 'Must have at least one "@" sign in an e-mail address.';
    result := false;
    exit;
  end
  else if (atCOUNT > 1) then
  begin
    errStr := 'Can''t have more than one "@" sign in an e-mail address.';
    result := false;
    exit;
  end;

  if periodCOUNT < 1 then
  begin
    errStr := 'Must have at least one period [.] in an e-mail address.';
    result := false;
    exit;
  end;

  if AnsiContainsStr(InAddr, '..') then
  begin
    errStr := 'e-mail addresses cannot have two periods [..] in a row';
    result := false;
    exit;
  end;

  // The User portion of the email (all to the left of the @) must have at least 1 character : this is OK, as verified the 1st char is not an @
  //sUSER := copy(InAddr,1, (pos('@',InAddr)-1) );  // get everything to the left of the @ sign

  // get everything to the right of the @ sign:
  sDOM1 := copy(InAddr, (pos('@', InAddr) + 1), 255);

  // The Domain portion of the email to contain at least 2 parts
  if (pos('.', sDOM1) = 0) then
  begin
    errStr := 'Must have at least two parts separated by a period after the @ sign in an e-mail address';
    result := false;
  end;
end;

function TimeBetween(StartDateTime, EndDateTime: TDateTime): string;
var
  tempDate: Extended;
  intDays: Integer;
begin
  tempDate := (EndDateTime - StartDateTime);
  intDays := DaysBetween(StartDateTime, EndDateTime);
  result := IntToStr(intDays) + ' Day(s) ' + FormatDateTime('h "Hour(s)" n "Minute(s)" s "Second(s)"', (tempDate));
end;

{JRCommon implementation ends here}

{WinFunctions implementation starts here}
function TWinInfo.GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

function TWinInfo.GetUserFromWindows: string;
Var
  UserName: string;
  UserNameLen: Dword;
Begin
  UserNameLen := 255;
  SetLength(userName, UserNameLen);
  If GetUserName(PChar(UserName), UserNameLen) Then
    Result := Copy(UserName, 1, UserNameLen - 1)
  Else
    Result := 'Unknown';
End;
//

function TWinInfo.FolderSize(Dir: string): integer;
var
  SearchRec: TSearchRec;
  Separator: string;
  DirBytes: integer;
begin
  //Result := 0;
  if Copy(Dir, Length(Dir), 1) = '\' then
    Separator := ''
  else
    Separator := '\';

  if FindFirst(Dir + Separator + '*.*', faAnyFile, SearchRec) = 0 then begin
    if FileExists(Dir + Separator + SearchRec.Name) then begin
      DirBytes := DirBytes + SearchRec.Size;
    end else if DirectoryExists(Dir + Separator + SearchRec.Name) then begin
      if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then begin
        FolderSize(Dir + Separator + SearchRec.Name);
      end;
    end;
    while FindNext(SearchRec) = 0 do begin
      if FileExists(Dir + Separator + SearchRec.Name) then begin
        DirBytes := DirBytes + SearchRec.Size;
      end else if DirectoryExists(Dir + Separator + SearchRec.Name) then
      begin
        if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then begin
          FolderSize(Dir + Separator + SearchRec.Name);
        end;
      end;
    end;
  end;
  FindClose(SearchRec);
  Result := DirBytes;
end;

//

function TWinInfo.TCPIPInstalled: boolean;
var
  Reg: TRegistry;
  RKeys: Tstrings;
begin
  Result := False;
  try
    Reg := TRegistry.Create;
    RKeys := TStringList.Create;
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\Enum\Network\MSTCP', False) Then
    begin
      reg.GetKeyNames(RKeys);
      Result := RKeys.Count > 0;
    end;
  finally
    Reg.free;
    RKeys.free;
  end;
end;

//replaces one instance

function TWinInfo.ReplaceSubString(DestString, ReplaceString, NewValueString: string): string;
var
  Position: Integer;
begin
  Position := Pos(ReplaceString, DestString);
  Delete(DestString, Position, Length(ReplaceString));
  Insert(NewValueString, DestString, Position);
  Result := DestString;
end;

function TWinInfo.DeleteSpaces(strValue: string): string;
var
  i: Integer;
begin
  i := 0;
  while i <= Length(strValue) do
    if strValue[i] = ' ' then Delete(strValue, i, 1)
    else Inc(i);
  Result := strValue;
end;

function TWinInfo.SecondsIdle: DWord;
 {Usage
  procedure TForm1.Timer1Timer(Sender: TObject) ;
begin
   Caption := Format('System IDLE last %d seconds', [SecondsIdle]) ;
end;
}
var
  liInfo: TLastInputInfo;
begin
  liInfo.cbSize := SizeOf(TLastInputInfo);
  GetLastInputInfo(liInfo);
  Result := (GetTickCount - liInfo.dwTime) DIV 1000;
end;


Function TWinInfo.StringReverse(strValue: String): String;
Var
  i: Integer;
Begin
  Result := '';
  For i := Length(strValue) DownTo 1 Do
  Begin
    Result := Result + Copy(strValue, i, 1);
  End;
End;

function TWinInfo.SearchAndReplace(sSrc, sLookFor, sReplaceWith: string): string;
var
  nPos, nLenLookFor: integer;
begin
  nPos := Pos(sLookFor, sSrc);
  nLenLookFor := Length(sLookFor);
  while (nPos > 0) do begin
    Delete(sSrc, nPos, nLenLookFor);
    Insert(sReplaceWith, sSrc, nPos);
    nPos := Pos(sLookFor, sSrc);
  end;
  Result := sSrc;
end;

function TWinInfo.SecToTime(Sec: Integer): string;
var
  H, M, S: string;
  ZH, ZM, ZS: Integer;
begin
  ZH := Sec div 3600;
  ZM := Sec div 60 - ZH * 60;
  ZS := Sec - (ZH * 3600 + ZM * 60);
  H := IntToStr(ZH);
  M := IntToStr(ZM);
  S := IntToStr(ZS);
  Result := H + ':' + M + ':' + S;
end;

function TWinInfo.StringToCaseSelect(Selector: string; CaseList: array of string): Integer;
var cnt: integer;
begin
  Result := -1;
  for cnt := 0 to Length(CaseList) - 1 do
  begin
    if CompareText(Selector, CaseList[cnt]) = 0 then
    begin
      Result := cnt;
      Break;
    end;
  end;
end;

{WinFunctions implementation ends here}



end.

