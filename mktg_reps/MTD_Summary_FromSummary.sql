mtd summary sql ::

Select 
sum(vs_base_rate_retail) retail_base,
sum(vs_insurance_tot_retail) retail_ins,
sum(vs_do_fee_retail) retail_delivery,
sum(vs_pu_fee_retail) retail_pick_up,
sum(vs_xtra_fee_retail) retail_other_fees,
sum(vs_tax_tot_retail) retail_tax,
sum(vs_base_rate_retail+vs_insurance_tot_retail+vs_do_fee_retail+
vs_pu_fee_retail+vs_xtra_fee_retail+vs_tax_tot_retail) retail_sub_total,
sum(vs_comm_tot) commission,
sum(vs_discount_retail) retail_discount,
sum(vs_base_rate_retail+vs_insurance_tot_retail+vs_do_fee_retail+
vs_pu_fee_retail+vs_xtra_fee_retail+vs_tax_tot_retail-vs_comm_tot-vs_discount_retail) NET,
sum(vs_deposit_amt) deposits,
sum(vs_waived_amt) waived_deposits,
sum(vs_deposit_amt-vs_waived_amt) cash_deposits,
sum(vs_deferred_amt) deferred,
sum(vs_profit) profit,
sum(vs_insurance_tot_wholesale / vs_exchange_rt) OD_CDW,
sum(vs_operator_due) OpDue,
sum(vs_gross_revenue) gross_revenue,
sum(vs_operator_cost) operator_cost,
sum(vs_comm_tot) commission_expense,
sum(vs_comm_tot-vs_comm_due) comm_withheld,
sum(vs_comm_due) commission_due,
sum(vs_count_new) new_vouchers, sum(vs_count_change) changes, sum(vs_count_cxl) cancels,
(sum(vs_duration) / sum(vs_count_voucher) ) ave_duration,
sum(vs_count_voucher) vouchers 

From g_voucher_summary
where vs_basesys = "USA" and 
vs_paid_dt between date("12/01/2007") and date("12/07/2007")

-- issues with opdue:
-- must use case statements into a temp table then select *
-- unless TotalOpDUe is OK without displaying details

select 

case
when vs_fp_pp = 'FP' then 
 (0)
else
  (vs_insurance_tot_wholesale / vs_exchange_rt)
end as whl_cdw,

case
when vs_fp_pp = 'FP' then 
 (0)
else
  (vs_do_fee_wholesale / vs_exchange_rt)
end as whl_delivery,

case
when vs_fp_pp = 'FP' then 
 (0)
else
  (vs_tax_tot_wholesale / vs_exchange_rt)
end as whl_tax, 
*
From 
where vs_basesys = "USA" and 
vs_paid_dt between date("12/01/2007") and date("12/07/2007") into temp crap with no log;

select
sum(), sum(), sum() form crap (etc)




