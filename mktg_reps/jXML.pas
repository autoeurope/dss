unit jXML;

interface

uses DB,
  SysUtils, dialogs, classes, strUtils;

procedure XAdd(var TargetString: string; StringToAdd: string);
procedure SaveXMLtoStream(DS: TDataSource; InXHeader, InDataName, InDataAttribs: string; MaxFields, MaxRows: integer; var theStream: TMemoryStream);
procedure SaveXMLToFile(DS: TDataSource; InXHeader, InDataName, sFILENAME: string; MaxFields: integer);

implementation

procedure XAdd(var TargetString: string; StringToAdd: string);
var // use this instead of string := String + (this ripps so much faster)
  i, j: integer;
begin
  if StringToAdd = '' then exit;
  i := length(TargetString);
  j := length(StringToAdd);
  SetLength(TargetString, i + j);
  Move(StringToAdd[1], TargetString[succ(i)], j);
end;

procedure SaveXMLtoStream(DS: TDataSource; InXHeader, InDataName, InDataAttribs: string; MaxFields, MaxRows: integer; var theStream: TMemoryStream);
var
  SavePlace: TBookMark;
  i, x: integer;
  s, temp, temp2: string;
  MemPtr: PChar;
begin
  s := '';
  theStream.Clear;
  theStream.Position := 0;

  IF (MaxFields >  DS.DataSet.FieldCount - 1)  then
    MaxFields := DS.DataSet.FieldCount -1;

  if (DS = nil) then exit;
  if InXHeader = '' then InXHeader := 'DataSet';

  if AnsiContainsStr(InXHeader, ' ') then
  begin
    InXHeader := AnsiReplaceStr(InXHeader, ' ', '_');
  end;

  if InDataName = '' then InDataName := 'DataSetName';

  s := '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes" ?>';
  XAdd(s, '<' + InXHeader + ' DataSetName="' + InDataName + '" ' + InDataAttribs + ' TimeStamp="' + FormatDateTime('mmmm/dd/yyyy : hh:mm AM/PM ', now) + '">');
  MemPtr := Pchar(s);
  theStream.Write(MemPtr^, Length(s));
  s := '';

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;
    XAdd(s, '<DataFieldNames>'); // field Definitions start tag

    for i := 0 to MaxFields do
    begin
      temp := '';
      case fields[i].DataType of
        ftUnknown: temp := 'unknown';
        ftSmallint: temp := 'int-16';
        ftInteger: temp := 'int-32';
        ftWord: temp := 'unsigned int-16';
        ftBoolean: temp := 'boolean';
        ftFloat: temp := 'float';
        ftCurrency: temp := 'money';
        ftString: temp := 'string';
        ftDate: temp := 'date';
        ftTime: temp := 'time';
        ftDateTime: temp := 'datetime';
        ftBCD: temp := 'bcd';
        ftBytes: temp := 'fixed bytes';
        ftVarBytes: temp := 'variable bytes';
        ftAutoInc: temp := 'autoinc int-32';
        ftBlob: temp := 'BLOB';
        ftMemo: temp := 'text memo';
        ftGraphic: temp := 'bitmap';
        ftFmtMemo: temp := 'fmt memo';
        ftParadoxOle: temp := 'paradox ole';
        ftDBaseOle: temp := 'dbase ole';
        ftTypedBinary: temp := 'typed binary';
        ftCursor: temp := 'oracle output cursor';
        ftFixedChar: temp := 'fixed char';
        ftWideString: temp := 'wide string';
        ftLargeint: temp := 'large int';
        ftADT: temp := 'abstract data type';
        ftArray: temp := 'array';
        ftReference: temp := 'reference';
        ftDataSet: temp := 'dataset';
        ftVariant: temp := 'variant';
        ftInterface: temp := 'interface ref';
        ftIDispatch: temp := 'dispatch ref';
        ftGuid: temp := 'guid';
        ftFMTBcd: temp := 'binary decimal';
      else
        temp := 'unknown';
      end;
      XAdd(s, '<Field_' + inttostr(i) + ' FieldName="' + fields[i].Fieldname + '" DataType="' + temp + '" />');
    end; // for

    XAdd(s, '</DataFieldNames>');
    MemPtr := Pchar(s);
    theStream.Write(MemPtr^, Length(s));
    s := '';

    begin
      XAdd(s, '<Data>');
      First;
      x := 1;
      while not eof do
      begin
        XAdd(s, '<ROW>');

        if (x >= MaxRows) then
        begin
          XAdd(s, '<Truncated_DataSet>Maximum Allowed Rows: ' + IntToStr(MaxRows) + ' exceeded.</Truncated_DataSet>');
          XAdd(s, '</ROW>');
          MemPtr := Pchar(s);
          theStream.Write(MemPtr^, Length(s));
          s := '';
          break;
        end;

        for i := 0 to MaxFields do
        begin
          case fields[i].DataType of
            ftBlob, ftMemo, ftGraphic,
              ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes,
              ftADT, ftArray,
              ftReference,
              ftDataSet:
              begin
                XAdd(s, '<' + fields[i].Fieldname + '>' + 'BLOB' + '</' + fields[i].Fieldname + '>');
              end;
            ftString:
              begin
                temp2 := '';
                temp2 := fields[i].AsString;
                temp2 := AnsiReplaceStr(temp2, '&', '&amp;');
                temp2 := AnsiReplaceStr(temp2, '<', '&lt;');
                temp2 := AnsiReplaceStr(temp2, '>', '&gt;');
                temp2 := AnsiReplaceStr(temp2, '"', '&quot;');
                temp2 := AnsiReplaceStr(temp2, '''', '&apos;');
                XAdd(s, '<' + fields[i].Fieldname + '>' + fields[i].AsString + '</' + fields[i].Fieldname + '>');
              end
          else
            begin
              XAdd(s, '<' + fields[i].Fieldname + '>' + fields[i].AsString + '</' + fields[i].Fieldname + '>');
            end;
          end;
        end;

        XAdd(s, '</ROW>');
        MemPtr := Pchar(s);
        theStream.Write(MemPtr^, Length(s));
        s := '';
        next;
        inc(x);
      end;

      XAdd(s, '</Data>');
      XAdd(s, '</' + InXHeader + '>'); // final end tag
      MemPtr := Pchar(s);
      theStream.Write(MemPtr^, Length(s));
      s := '';

    end;
    GotoBookMark(SavePlace);
  finally
    dataset.enablecontrols
  end;
end;

procedure SaveXMLToFile(DS: TDataSource; InXHeader, InDataName, sFILENAME: string; MaxFields: integer);
var
  SavePlace: TBookMark;
  i: integer;
  s, temp, temp2: string;
  tx: TextFile;
begin
  if sFILENAME = '' then exit;
  AssignFile(TX, sFilename);
  rewrite(tx);

  IF (MaxFields >  DS.DataSet.FieldCount - 1)  then
    MaxFields := DS.DataSet.FieldCount -1;

  if (DS = nil) then exit;
  if InXHeader = '' then InXHeader := 'DataSet';
  if AnsiContainsStr(InXHeader, ' ') then
  begin
    InXHeader := AnsiReplaceStr(InXHeader, ' ', '_');
  end;

  if InDataName = '' then InDataName := 'DataSetName';

  s := '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes" ?>';
  XAdd(s, '<' + InXHeader + ' DataSetName="' + InDataName + '" TimeStamp="' + FormatDateTime('mmmm/dd/yyyy : hh:mm AM/PM ', now) + '">');
  Writeln(tx, s);
  s := '';

  with DS, DS.dataset do
  try
    dataset.disablecontrols;
    SavePlace := dataset.GetBookMark;

    XAdd(s, '<DataFieldNames>'); // field Definitions start tag
    for i := 0 to MaxFields do
    begin
      temp := '';
      case fields[i].DataType of
        ftUnknown: temp := 'unknown';
        ftSmallint: temp := 'int-16';
        ftInteger: temp := 'int-32';
        ftWord: temp := 'unsigned int-16';
        ftBoolean: temp := 'boolean';
        ftFloat: temp := 'float';
        ftCurrency: temp := 'money';
        ftString: temp := 'string';
        ftDate: temp := 'date';
        ftTime: temp := 'time';
        ftDateTime: temp := 'datetime';
        ftBCD: temp := 'bcd';
        ftBytes: temp := 'fixed bytes';
        ftVarBytes: temp := 'variable bytes';
        ftAutoInc: temp := 'autoinc int-32';
        ftBlob: temp := 'BLOB';
        ftMemo: temp := 'text memo';
        ftGraphic: temp := 'bitmap';
        ftFmtMemo: temp := 'fmt memo';
        ftParadoxOle: temp := 'paradox ole';
        ftDBaseOle: temp := 'dbase ole';
        ftTypedBinary: temp := 'typed binary';
        ftCursor: temp := 'oracle output cursor';
        ftFixedChar: temp := 'fixed char';
        ftWideString: temp := 'wide string';
        ftLargeint: temp := 'large int';
        ftADT: temp := 'abstract data type';
        ftArray: temp := 'array';
        ftReference: temp := 'reference';
        ftDataSet: temp := 'dataset';
        ftVariant: temp := 'variant';
        ftInterface: temp := 'interface ref';
        ftIDispatch: temp := 'dispatch ref';
        ftGuid: temp := 'guid';
        ftFMTBcd: temp := 'binary decimal';
      else
        temp := 'unknown';
      end;
      XAdd(s, '<Field_' + inttostr(i) + ' FieldName="' + fields[i].Fieldname + '" DataType="' + temp + '" />');
    end;
    XAdd(s, '</DataFieldNames>');
    Writeln(tx, s);
    s := '';

    begin
      XAdd(s, '<Data>');
      First;
      while not eof do
      begin
        XAdd(s, '<ROW>');
        for i := 0 to MaxFields do
        begin
          case fields[i].DataType of
            ftBlob, ftMemo, ftGraphic,
              ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes,
              ftADT, ftArray,
              ftReference,
              ftDataSet:
              begin
                XAdd(s, '<' + fields[i].Fieldname + '>' + 'BLOB' + '</' + fields[i].Fieldname + '>');
              end;
            ftString:
              begin
                temp2 := fields[i].AsString;
                temp2 := AnsiReplaceStr(temp2, '&', '&amp;');
                temp2 := AnsiReplaceStr(temp2, '<', '&lt;');
                temp2 := AnsiReplaceStr(temp2, '>', '&gt;');
                temp2 := AnsiReplaceStr(temp2, '"', '&quot;');
                temp2 := AnsiReplaceStr(temp2, '''', '&apos;');
                XAdd(s, '<' + fields[i].Fieldname + '>' + temp2 + '</' + fields[i].Fieldname + '>');
              end
          else
            begin
              XAdd(s, '<' + fields[i].Fieldname + '>' + fields[i].AsString + '</' + fields[i].Fieldname + '>');
            end;
          end;
        end;

        XAdd(s, '</ROW>');
        Writeln(tx, s);
        s := '';
        next;
      end;
      XAdd(s, '</Data>');
      XAdd(s, '</' + InXHeader + '>'); // final end tag

      Writeln(tx, s);
      s := '';
    end;
    CloseFile(tx);
    GotoBookMark(SavePlace);
  finally
    dataset.enablecontrols
  end;
end;


end.

