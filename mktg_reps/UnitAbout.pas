unit UnitAbout;

interface

uses
  Windows,
  SysUtils,
  Classes,
  //Graphics,
  Forms,
  Controls,
  StdCtrls,
  Buttons,
  ExtCtrls, Graphics;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    btnOK: TButton;
    Label1: TLabel;
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

{$R *.dfm}

procedure TAboutBox.btnOKClick(Sender: TObject);
begin
self.Close;
end;

end.

