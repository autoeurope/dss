New rules:

if DC then compare lists of e-mail addresses
if TA then compare concatonation of iata + client_fname + client_lname

~ also show how many DC's do not have an e-mail addr compared to total DC's

--------------------------------------------------------------------------
using routines from "new_method_02052010.sql" (this file)

~ the outputs for RPB_Summary3.xls ARE acceptable per IK for 2008-2009.

I did explain that the result set represents "clients which came back one or more 
times in year 2" & that the totals represent a sum of AE's standard voucher counting method as done
on the sales journal. 

Move ahead and compare years 2007 to 2008 in the same manner, then put the two together to see the 
trend from year to year..

--------------------------------------------------------------------------

this gives total bookings for everything:

get totals for both years, target and source - in this case we are comparing 2008 to 2009
i.e. "how many customers came back in 09 which booked us in 08"


Select vs_basesys system, sum(vs_count_voucher) 
orig_bookings, 0.0 repeat_bookings, 
0.0 repeat_percent 
From g_voucher_summary, g_client WHERE  
 
vs_basesys in("AUS", "CANA","EURO", "HOHO", "HOT", "KEM", "UK", "NEWZ", "USA") and  
(  vs_paid_dt BETWEEN date('01/01/2008') and date('12/31/2008')  ) 
  
and gc_system = vs_basesys 
and gc_voucher = vs_voucher 
group by 1 order by 1

-- get total bookings, total ta bookings and total dc bookings for each year

Select vs_basesys system, sum(vs_count_voucher) 
orig_bookings, 0.0 repeat_bookings, 
0.0 repeat_percent 
From g_voucher_summary, g_client WHERE  
 
vs_basesys in("AUS", "CANA","EURO", "HOHO", "HOT", "KEM", "UK", "NEWZ", "USA") and  	
(  vs_paid_dt BETWEEN date('01/01/2008') and date('12/31/2008'))  	-- change dates here			
and vs_direct_or_ta = "DC" -- dss uses "flag" to set this                    	-- use TA or DC here 
and gc_system = vs_basesys 
and gc_voucher = vs_voucher 
group by 1 order by 1


outputs:

total bookings 2008:
AUS	73858
CANA	34907
EURO	553814
HOHO	24016
HOT	41722
KEM	16616
NEWZ	1102
UK	279003
USA	137979

total TA bookings 2008:
AUS	62942
CANA	24439
EURO	182373
HOHO	23888
HOT	19616
KEM	3531
NEWZ	425
UK	98708
USA	67749

total DC bookings 2008:

AUS	10916
CANA	10468
EURO	371441
HOHO	128
HOT	22106
KEM	13085
NEWZ	677
UK	180295
USA	70230


total bookings 2009:

AUS	90996
CANA	28514
EURO	573868
HOHO	23560
HOT	42708
KEM	15348
NEWZ	2386
UK	248932
USA	123464

total TA Bookings 2009:
AUS	77928
CANA	20068
EURO	208368
HOHO	23348
HOT	17648
KEM	2750
NEWZ	1886
UK	93631
USA	56364


total DC bookings 2009:
AUS	13068
CANA	8446
EURO	365500
HOHO	212
HOT	25060
KEM	12598
NEWZ	500
UK	155301
USA	67100

-- get total DC Bookings WITH an e-mail address - this is just to compare how many bookings
-- have an e-mail to the total:


Select vs_basesys system, sum(vs_count_voucher) 
orig_bookings, 0.0 repeat_bookings, 
0.0 repeat_percent 
From g_voucher_summary, g_client WHERE   
vs_basesys in("AUS", "CANA","EURO", "HOHO", "HOT", "KEM", "UK", "NEWZ", "USA") and  
(  vs_paid_dt BETWEEN date('01/01/2009') and date('12/31/2009'))  -- change as needed
and vs_direct_or_ta = "DC"					  -- DC
and (length(gc_email) > 0 and gc_email is not null) 
and gc_system = vs_basesys 
and gc_voucher = vs_voucher 
group by 1 order by 1

DC Bookings with an email address 2008:
AUS	10741
CANA	10324
EURO	370516
HOHO	120
HOT	20939
KEM	12823
NEWZ	676
UK	179895
USA	68903

DC Bookings with an email address 2009:
AUS	12830
CANA	8363
EURO	365014
HOHO	211
HOT	24344
KEM	12388
NEWZ	500
UK	155104
USA	66317

end prelim counts ---------------------------------------------------------------

TA Comparisons: 

** new rule: Use IATA || client_lNAME || client_fNAME as the comparison list.
** do this for each system 


1: create list to compare from source year:


drop table rpbus_temp;


select trim(upper(gc_iata)) || trim(upper(gc_lastname)) || trim(upper(gc_firstname)) iataname 
From g_voucher_summary, g_client 
WHERE vs_basesys = "USA" and	-- change as needed
( vs_paid_dt BETWEEN date('01/01/2008') and date('12/31/2008') )
and vs_direct_or_ta = "TA"  
and gc_system = vs_basesys and 
gc_voucher = vs_voucher  
into temp rpbus_temp WITH NO LOG;

2: compare to target year:

Select sum(vs_count_voucher) rentals From g_voucher_summary, 
g_client where 
vs_basesys = "USA" and 		-- change as needed 
( vs_paid_dt BETWEEN date('01/01/2009') and date('12/31/2009') )  
and vs_direct_or_ta = "TA" and
(trim(upper(gc_iata)) || trim(upper(gc_lastname)) || trim(upper(gc_firstname)) 
in ( select iataname from rpbus_temp ))
and gc_system = vs_basesys 
and gc_voucher = vs_voucher

TA clients who booked in 08 and came back in 09:

AUS:	4845
CANA:	3218
EURO:	19997
HOHO:	1844
HOT:	1024
KEM:	427
NEWZ:	29
UK:	8527
USA:	7705


--------------------------------------------------------------------------------

DC Comparisons:

** new rule: Use client e-mail the comparison list.
** do this for each system 


1: create list to compare from source year:


drop table rpbus_temp;


select trim(upper(gc_email)) emailaddr 
From g_voucher_summary, g_client 
WHERE vs_basesys = "USA" and		-- change as needed
( vs_paid_dt BETWEEN date('01/01/2008') and date('12/31/2008'))
and vs_direct_or_ta = "DC" 
and (length(gc_email) > 0 and gc_email is not null)
and gc_system = vs_basesys and 
gc_voucher = vs_voucher  
into temp rpbus_temp WITH NO LOG;


2: compare to target year:

Select sum(vs_count_voucher) rentals 
From g_voucher_summary, g_client where 
vs_basesys = "USA" and 		-- change as needed 
( vs_paid_dt BETWEEN date('01/01/2009') and date('12/31/2009') )  
and vs_direct_or_ta = "DC" 
and (length(gc_email) > 0 and gc_email is not null)
and ( trim(upper(gc_email)) in ( select emailaddr from rpbus_temp ))
and gc_system = vs_basesys 
and gc_voucher = vs_voucher


DC clients who booked in 08 and came back in 09:

AUS:	1390
CANA:	1642
EURO:	97351
HOHO:	169
HOT:	1315
KEM:	2518
NEWZ:	35
UK:	35620
USA:	10227









