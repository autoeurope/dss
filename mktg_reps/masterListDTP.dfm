object FormListDates: TFormListDates
  Left = 0
  Top = 0
  Caption = 'Master List Date Selector'
  ClientHeight = 87
  ClientWidth = 373
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 4
    Width = 335
    Height = 13
    Caption = 
      'Select the date range for which you would like your master list ' +
      'set up.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 13
    Width = 353
    Height = 13
    Caption = 
      'For example: If you intend to run two reports with separate date' +
      ' ranges,'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 32
    Width = 337
    Height = 13
    Caption = 
      'choose the same dates here for each report. This will allow you ' +
      'to cut '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 62
    Width = 355
    Height = 13
    Caption = 
      'Make sure range is wider than the least and greatest dates  accr' +
      'oss both.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 46
    Width = 345
    Height = 13
    Caption = 
      ' and paste  to each report, because they will have the same mast' +
      'er list.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
end
