@ECHO OFF

::Assumed that the target machine has Indy, Jedi, and IDAC installed

setlocal enabledelayedexpansion
:PROMPT
SET /P AREYOUSURE=This batch file will retrieve from the BitBucket repository the files required to compile the DSS - Marketing reps updater; are you sure you want to run it (YES/[N])?
IF /I "%AREYOUSURE%" NEQ "YES" GOTO END
SET USERPASS=Enter
SET /P USERPASS=Enter your credentials(username:password) or hit Enter to bypass using SSH^>

::***************************************************************************
::The script starts here
::***************************************************************************


IF %USERPASS% EQU Enter (
	SET arrRemoteRepo[0]=ssh://hg@bitbucket.org/autoeurope/dsc-kbmmemtable
	SET arrRemoteRepo[1]=ssh://hg@bitbucket.org/autoeurope/dsc-crypt
	SET arrRemoteRepo[2]=ssh://hg@bitbucket.org/autoeurope/dsc-extra9
	SET arrRemoteRepo[3]=ssh://hg@bitbucket.org/autoeurope/dsc-tperlregex
	SET arrRemoteRepo[4]=ssh://hg@bitbucket.org/autoeurope/dsc-dscalc
	SET arrRemoteRepo[5]=ssh://hg@bitbucket.org/autoeurope/dsc-tsippdesc
) ELSE (
	SET arrRemoteRepo[0]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-kbmmemtable
	SET arrRemoteRepo[1]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-crypt
	SET arrRemoteRepo[2]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-extra9
	SET arrRemoteRepo[3]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-tperlregex
	SET arrRemoteRepo[4]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-dscalc
	SET arrRemoteRepo[5]=https://%USERPASS%@bitbucket.org/autoeurope/dsc-tsippdesc
)
SET arrRepoVersion[0]=1.0
SET arrRepoVersion[1]=1.1
SET arrRepoVersion[2]=1.0
SET arrRepoVersion[3]=1.0
SET arrRepoVersion[4]=1.0
SET arrRepoVersion[5]=1.0

SET arrLocalRepo[0]=KBMMemTable
SET arrLocalRepo[1]=Crypt_Pak
SET arrLocalRepo[2]=Extra9
SET arrLocalRepo[3]=PerlRegex
SET arrLocalRepo[4]=DSCalc
SET arrLocalRepo[5]=SIPPDesc


::weird behavior with findstr /e tag will not find a token that ends with EOF instead of end of line, so append a newline character to precede EOF
ECHO.>>.hgignore
FOR /l %%A IN (0,1,5) DO (
IF EXIST !arrLocalRepo[%%A]! (
    ECHO Pulling version !arrRepoVersion[%%A]! from !arrRemoteRepo[%%A]! to !arrLocalRepo[%%A]!
	cd !arrLocalRepo[%%A]!
    hg pull -r !arrRepoVersion[%%A]! !arrRemoteRepo[%%A]!
	cd..	
) else (
    ECHO Cloning version !arrRepoVersion[%%A]! from !arrRemoteRepo[%%A]! to !arrLocalRepo[%%A]!
    hg clone -r  !arrRepoVersion[%%A]! !arrRemoteRepo[%%A]! !arrLocalRepo[%%A]! 
)
::If the pulled repo is not already on the ignore list, add it
findstr /bier "!arrLocalRepo[%%A]!" .hgignore >NUL
IF !ErrorLevel! EQU 0 (
ECHO !arrLocalRepo[%%A]! Found
) ELSE (
ECHO !arrLocalRepo[%%A]!>>.hgignore
)
)

ECHO Done retrieving components.
pause
:END