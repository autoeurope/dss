object frmDSSHelp: TfrmDSSHelp
  Left = 0
  Top = 0
  Caption = 'AE Data Storage Help'
  ClientHeight = 379
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = twetweb
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 17
  object WebBrowserHelp: TWebBrowser
    Left = 0
    Top = 0
    Width = 724
    Height = 379
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 693
    ExplicitHeight = 363
    ControlData = {
      4C000000DD3B0000561F00000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126200000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object twetweb: TMainMenu
    Left = 448
    Top = 208
    object mnuFile: TMenuItem
      Caption = 'File'
      object mnuClose: TMenuItem
        Caption = 'Close'
        OnClick = mnuCloseClick
      end
    end
  end
end
