//05/30/2012 Added _Marcello_SQL constant
unit ReportConstants;

interface

const
  MTD_SQL =
    'SELECT {+index(vouchandqueue sysbatch)} basesys, VCNUMB,VCHREL,VORD4,VOPAMT4,VCCASHDEPOSI,VOPSUBT, ' +
    'VOPDATE1,VOPCURRENCY, ALTRESNUM1,VCIATA,VOPCTRY1,VOPCITY1,VOPID, ' +
    'A.homectry,vcntype,voptype,vcstdte2,vchpqbath,vccrcardid,vccrcard, ' +
    'vcccmonth,vccconfirm,vcclientid, vcvendorattn, vccomments,vccomments1,vcarrvair, ' +
    'vcarrvflight,vceta,vcprepay,vcccauthcode,vchdate,vcrid,vcvpromo1,vcvpromo2, ' +
    'vcvpromo3,vcst1,vcst2,vcst3,vcstdte1,vcstdte3,vopid2,vopctry2,vopprov1,vopprov2, ' +
    'vopcity2,voploc1,voploc2,voprd1,vopcurrate,vord1,vord2,vord3,vord5,vord6, ' +
    'vord7,vord8,vord9,vord10,vowd1,vowd2,vowd3,vowd4,vowd5,vopwd6,vopwd7,vopwd8, ' +
    'vopwd9,vopwd10,vopdate2,voptime1,voptime2,vopcomt1,voptax2,vopper1,vopper2, ' +
    'vopper3,voptxper1,voptxper2,vopdatecreate,vopdatemodif,vopstype1,vopamt1,vopamt2, ' +
    'vopamt3,voiamt1,voiamt2,voiamt3,voiamt4,voiamt5,voprcode,voconsort,vorecord, ' +
    'voit1,voit1ret,voit1whl,voit2,voit2ret,voit2whl,voit3,voit3ret,voit3whl, ' +
    'voit4ret,voit4whl,voxtp1,voxtp1w,voxtp1t,voxtp2,voxtp2w,voxtp2t,vccom1, ' +
    'vccom2,vcdisaddamount,vcconsortsub,vccomtaken,vcfpover1,vccomover1, ' +
    'A.homecurr,affiliate1,affiliate2,voit1sw,vopamt5,vopamt6, ' +
    'vopamt7,vopamt8,vopamt9,origcurr,vchpqtype,vchpqvch,vchpqvchln,vchpqseq,vchpstat, ' +
    'vchpqnsw1,vchpqnsw2,vchpqnsw3,VOPTAX1 ' +
    'FROM vouchandqueue A WHERE ' +
    'VCHPQBATH BETWEEN :date1 and :date2 ';

      //items summed
   _Marcello_SQL_SUMMARY_SELECT1 = 'SELECT ' +
    'v.vs_iata iata, ' +
    'vend.gv_name vendor_name, ' +
    '"XXXXXXXX" campaign,  ' +
    'v.vs_pu_ctry country, ' +
    '"XXXXXXXX" campid, ' +
    'sum(v.vs_retail_total) as vs_retail_total,  ' +
    'sum(v.vs_profit ) as profit,  ' +
    'sum(v.vs_count_voucher) as vs_count_voucher  ';

     _MARCELLO_GROUP_BY1 = ' group by wcamps.campaign, ' +
   'v.vs_iata, vend.gv_name, vs_pu_ctry, ' +
   'wcamps.campid';

   //items summed
   _Marcello_SQL_SUMMARY_SELECT = 'SELECT ' +
    'v.vs_basesys system, ' +
    'v.vs_operator_code operator, ' +
    'v.vs_home_ctry home_ctry,  ' +
    'c.gc_ctry client_ctry, ' +
    'v.vs_iata iata, ' +
    'vend.gv_name vendor_name, ' +
    'wcamps.campaign,  ' +
    'year(v.vs_paid_dt) pay_dt_year, ' +
    'v.vs_pu_ctry country, ' +
    '"XXXXXXXX" campid, ' +
    '" " campaign, ' +
    '" " host, '  +
    'sum(v.vs_retail_total) as vs_retail_total,  ' +
    'sum(v.vs_profit ) as profit,  ' +
    'sum(v.vs_count_voucher) as vs_count_voucher  ';

   //all items not summed
   _Marcello_SQL_DETAIL_SELECT = 'SELECT ' +
  'year(v.vs_paid_dt) pay_dt_year, ' +
  'v.vs_basesys system, ' +
  'v.vs_operator_code operator, ' +
  'v.vs_pu_ctry country, ' +
  'v.vs_home_ctry home_ctry, ' +
  'c.gc_ctry client_ctry, ' +
  'v.vs_retail_total, ' +
  'v.vs_profit profit, ' +
  'v.vs_count_voucher, ' +
  'vend.gv_name vendor_name, ' +
  'v.vs_promo2, ' +
  '"XXXXXXXXXXXXXXXXX" campid, ' +
  '"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" campaign, ' +
  '"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" host, '  +
  'year(vs_paid_dt+1) dt_year,  (vs_pu_dt - date(vs_paid_dt)) lead_days_pdpu, ' +
  ' * ';

 _MARCELLO_SQL_FROM ='FROM ' +
  'g_voucher_summary v, ' +
  'g_client c, ' +
  'g_vendor vend, outer cc_ref ' +
  'WHERE ';

  //'OUTER ( webdata@ids_ps701a::wbadcampaigns wcamps, webdata@ids_ps701a::wbadhosts whosts ) ' +

   _MARCELLO_SQL_ORDER_BY_CAMPID = ' ORDER BY wcamps.campid ';

  _Marcello_SQL_FIRST = 'SELECT ' +
  'year(v.vs_paid_dt) pay_dt_year, ' +
  'v.vs_basesys system, ' +
  'v.vs_operator_code operator, ' +
  'v.vs_pu_ctry country, ' +
  'v.vs_home_ctry home_ctry, ' +
  'c.gc_ctry client_ctry, ' +
  'v.vs_retail_total, ' +
  'v.vs_profit profit, ' +
  'v.vs_count_voucher, ' +
  'vend.gv_name vendor_name, ' +
  'wcamps.campid, ' +
  'wcamps.campaign, ' +
  'whosts.host ' +
  'FROM ' +
  'g_voucher_summary v, ' +
  'g_client c, ' +
  'g_vendor vend, ' +
  'OUTER ( webdata@ids_ps701a::wbadcampaigns wcamps, webdata@ids_ps701a::wbadhosts whosts ) ' +
  'WHERE ';

_Marcello_SQL = 'SELECT ' +
  'year(v.vs_paid_dt) pay_dt_year, ' +
  'v.vs_basesys system, ' +
  'v.vs_operator_code operator, ' +
  'v.vs_pu_ctry country, ' +
  'v.vs_home_ctry home_ctry, ' +
  'v.vs_iata iata, ' +
  'c.gc_ctry client_ctry, ' +
  'v.vs_retail_total, ' +
  'v.vs_profit profit, ' +
  'v.vs_count_voucher, ' +
  'vend.gv_name vendor_name, ' +
  'wcamps.campid, ' +
  'wcamps.campaign, ' +
  'whosts.host ' +
  'From ' +
  'g_voucher_summary v,   ' +
  'g_client c,    ' +
  'g_vendor vend, ' +
  'outer ( webdata@ids_ps701a::wbadcampaigns wcamps, webdata@ids_ps701a::wbadhosts whosts ) ' +
  'where ';

  _Marcello_SQL_SECOND = ' AND c.gc_system = v.vs_basesys ' +
  'AND v.vs_paid_dt BETWEEN date(:date1) AND date(:date2) ' +
  'AND c.gc_voucher = v.vs_voucher ' +
  'AND vend.gv_basesys = v.vs_basesys ' +
  'and cc_id = vs_payment_type ' +
  'AND vend.gv_iata = v.vs_iata ';

  _MARCELLO_GROUP_BY = ' group by wcamps.campaign, v.vs_basesys,v.vs_operator_code,v.vs_home_ctry,c.gc_ctry, ' +
   'v.vs_iata, vend.gv_name, vs_paid_dt, vs_pu_ctry, ' +
   'wcamps.campid, whosts.host';

  _MTD_SQL =
    'SELECT {+index(vouchandqueue sysbatch)} basesys, VCNUMB,VCHREL,VORD4,VOPAMT4,VCCASHDEPOSI,VOPSUBT, ' +
    'VOPDATE1,VOPCURRENCY, ALTRESNUM1,VCIATA,VOPCTRY1,VOPCITY1,VOPID, ' +
    'A.homectry,vcntype,voptype,vcstdte2,vchpqbath,vccrcardid,vccrcard, ' +
    'vcccmonth,vccconfirm,vcclientid, vcvendorattn, vccomments,vccomments1,vcarrvair, ' +
    'vcarrvflight,vceta,vcprepay,vcccauthcode,vchdate,vcrid,vcvpromo1,vcvpromo2, ' +
    'vcvpromo3,vcst1,vcst2,vcst3,vcstdte1,vcstdte3,vopid2,vopctry2,vopprov1,vopprov2, ' +
    'vopcity2,voploc1,voploc2,voprd1,vopcurrate,vord1,vord2,vord3,vord5,vord6, ' +
    'vord7,vord8,vord9,vord10,vowd1,vowd2,vowd3,vowd4,vowd5,vopwd6,vopwd7,vopwd8, ' +
    'vopwd9,vopwd10,vopdate2,voptime1,voptime2,vopcomt1,voptax2,vopper1,vopper2, ' +
    'vopper3,voptxper1,voptxper2,vopdatecreate,vopdatemodif,vopstype1,vopamt1,vopamt2, ' +
    'vopamt3,voiamt1,voiamt2,voiamt3,voiamt4,voiamt5,voprcode,voconsort,vorecord, ' +
    'voit1,voit1ret,voit1whl,voit2,voit2ret,voit2whl,voit3,voit3ret,voit3whl, ' +
    'voit4ret,voit4whl,voxtp1,voxtp1w,voxtp1t,voxtp2,voxtp2w,voxtp2t,vccom1, ' +
    'vccom2,vcdisaddamount,vcconsortsub,vccomtaken,vcfpover1,vccomover1, ' +
    'A.homecurr,affiliate1,affiliate2,voit1sw,vopamt5,vopamt6, ' +
    'vopamt7,vopamt8,vopamt9,origcurr,vchpqtype,vchpqvch,vchpqvchln,vchpqseq,vchpstat, ' +
    'vchpqnsw1,vchpqnsw2,vchpqnsw3,VOPTAX1 ' +
    'FROM vouchandqueue A WHERE ' +
    'VCHPQBATH BETWEEN :date1 and :date2 ';

    _HOST_CAMPID_SQL = 'select '
    + 'wcamps.campid, '
    + 'wcamps.campaign, '
    + 'whosts.host '
    + 'From  '
    + 'wbadcampaigns wcamps, '
    + 'wbadhosts whosts '
    + 'where '
    + 'whosts.hostid = wcamps.hostid ';

    _YEAR = 'year';
    _SYSTEM = 'system';
    _COUNTRY = 'country';
    _COUNTRY_PU = 'country_pu';
    _CSV = 'CSV';
    _XML = 'XML';
    _HTML = 'HTML';
    _AE_WHSE_RESULTS = 'AE_WHSE_RESULTS';
    _ALL = 'ALL';

    _MAIN_FORM_HEIGHT = 390;
    _MAIN_FORM_WIDTH = 470;
    _TAB_STEPTWO_HEIGHT = 582;//567;
    _TAB_STEPTWO_WIDTH = 618;
    _TAB_SPECIAL_HEIGHT = 490;//475
    _TAB_SPECIAL_WIDTH  = 863;
    _TAB_STEPTWO_B_HEIGHT = 500;//485;
    //no width?
    _TAB_OUTPUT_HEIGHT = 483;//468
    _TAB_OUTPUT_WIDTH  = 621;
    _TAB_TFZ_HEIGHT = 456;//441
    _TAB_TFZ_WIDTH  = 621;
    _TAB_DATA_WIDTH = 702;
    _TAB_PAY_TYPE_HEIGHT = 359;//344;
    _TAB_CAR_TYPE_HEIGHT = 447;//432
    _TAB_CAR_TYPE_WIDTH = 449;

    _NUM_FORMAT_FIVE_POUND = '###,##0.00';
    _NUM_FORMAT_EIGHT_POUND = '###,###,##0.00';
    _REMOVE_THE_X = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
     //485
implementation

end.
