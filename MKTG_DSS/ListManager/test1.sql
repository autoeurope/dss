select * from openquery(DATA_STORE,'select gc_email, gc_lastname, gc_firstname, gc_addr1, gc_city, gc_postcode,
gc_ctry from g_client where gc_system in( "EURO") and gc_voucher > 719578 and (gc_email is not null and gc_email > "")')





//******* example joins

SELECT loc.OrderID, loc.ProductID, rmt.ProductName
FROM [Order Details] loc INNER JOIN Infonet.Northwind.dbo.Products rmt
	ON loc.ProductID = rmt.ProductID

To:

SELECT loc.OrderID, loc.ProductID, rmt.ProductName
FROM [Order Details] loc INNER JOIN 

OPENQUERY(InfoNet, 'SELECT * FROM Northwind.dbo.Products') rmt
	ON loc.ProductID = rmt.ProductID


select * from [members_] members INNER JOIN

openquery(DATA_STORE, 'select * from g_client where gc_system 
in( "USA") and gc_voucher = 1142030 and gc_release = 3') dss

ON 
(
members.voucher_base_system = dss.gc_system and
members.voucher_key = dss.gc_voucher and
members.voucher_release_key = dss.gc_release

);




select * from dbo.members_ as members 

INNER JOIN

openquery(DATA_STORE 'select * from g_client where gc_system 
in("USA") and gc_voucher = 1142030 and gc_release = 3') dss

ON 
(
dbo.members.voucher_base_system = dss.gc_system and
dbo.members.voucher_key = dss.gc_voucher and
dbo.members.voucher_release_key = dss.gc_release

);


// this works on  SQL Server 2005

select * from openquery(DATA_STORE, 'select * from g_client where gc_system 
in(''USA'') and gc_voucher = 1142030 and gc_release = 3')



// so does this

select * from members_ members INNER JOIN

openquery(DATA_STORE, 'select * from g_client where gc_system 
in(''USA'') and gc_voucher = 1142030 and gc_release = 3') dss

ON 
(
members.voucher_base_system = dss.gc_system and
members.voucher_key = dss.gc_voucher and
members.voucher_release_key = dss.gc_release

);













