object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 542
  ClientWidth = 477
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    477
    542)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 96
    Top = 480
    Width = 50
    Height = 13
    Caption = 'List Name:'
  end
  object Label2: TLabel
    Left = 96
    Top = 507
    Width = 67
    Height = 13
    Caption = 'member type:'
  end
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 97
    Height = 25
    Caption = 'SQLSVR connect'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 80
    Width = 461
    Height = 119
    Anchors = [akLeft, akTop, akRight]
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    WordWrap = False
    ExplicitWidth = 462
  end
  object Button2: TButton
    Left = 421
    Top = 475
    Width = 49
    Height = 25
    Caption = 'test'
    TabOrder = 2
    OnClick = Button2Click
  end
  object EditListName: TEdit
    Left = 184
    Top = 480
    Width = 81
    Height = 21
    TabOrder = 3
    Text = 'p_mail'
  end
  object cbMemberTYpe: TComboBox
    Left = 184
    Top = 507
    Width = 81
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = 'normal'
    Items.Strings = (
      'normal'
      'confirm'
      'private'
      #39'expired'
      'held'
      'unsub'
      'needs-goodbye'
      'needs-hello'
      'needs-confirm')
  end
  object Edit1: TEdit
    Left = 269
    Top = 479
    Width = 146
    Height = 21
    TabOrder = 5
    Text = 'chester@autoeurope.com'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 331
    Width = 461
    Height = 138
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button3: TButton
    Left = 8
    Top = 39
    Width = 97
    Height = 25
    Caption = 'test open'
    TabOrder = 7
    OnClick = Button3Click
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 205
    Width = 461
    Height = 120
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource2
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button4: TButton
    Left = 328
    Top = 8
    Width = 75
    Height = 25
    Caption = 'open source'
    TabOrder = 9
    OnClick = Button4Click
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Driver={SQL Server};Server=TOPSECRET;Database=ListManager;Truste' +
      'd_Connection=Yes;'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 112
    Top = 8
  end
  object q1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 152
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = q1
    Left = 152
    Top = 8
  end
  object conn: TIfxConnection
    DesignConnection = True
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    IsolationLevel = ilDirtyRead
    Left = 416
    Top = 8
  end
  object QSource: TIfxQuery
    Connection = conn
    SQL.Strings = (
      'select * from g_client')
    Left = 416
    Top = 40
  end
  object DataSource2: TDataSource
    DataSet = QSource
    Left = 416
    Top = 72
  end
end
