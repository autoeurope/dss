object FormDssFeed: TFormDssFeed
  Left = 0
  Top = 0
  Caption = 'DSS Feed'
  ClientHeight = 426
  ClientWidth = 834
  Color = clSilver
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    834
    426)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 36
    Height = 13
    Caption = 'source:'
  end
  object Label4: TLabel
    Left = 111
    Top = 8
    Width = 58
    Height = 13
    Caption = 'Batch Start:'
  end
  object Label5: TLabel
    Left = 223
    Top = 8
    Width = 52
    Height = 13
    Caption = 'Batch End:'
  end
  object Memo1: TMemo
    Left = 8
    Top = 148
    Width = 353
    Height = 125
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object btnVQOpen: TButton
    Left = 8
    Top = 86
    Width = 97
    Height = 25
    Caption = 'VQ source Open'
    TabOrder = 1
    OnClick = btnVQOpenClick
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 279
    Width = 352
    Height = 122
    Anchors = [akLeft, akTop, akBottom]
    DataSource = dsSource
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dtp1: TDateTimePicker
    Left = 111
    Top = 27
    Width = 106
    Height = 21
    Date = 39196.577854745370000000
    Time = 39196.577854745370000000
    TabOrder = 3
  end
  object DBGrid2: TDBGrid
    Left = 367
    Top = 279
    Width = 459
    Height = 122
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsTargetVQ
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 8
    Top = 117
    Width = 97
    Height = 25
    Caption = 'VQ Target Open'
    TabOrder = 5
    OnClick = Button2Click
  end
  object btnVCCopyDate: TButton
    Left = 111
    Top = 55
    Width = 90
    Height = 25
    Caption = 'VC Copy Date'
    TabOrder = 6
    OnClick = btnVCCopyDateClick
  end
  object dtp2: TDateTimePicker
    Left = 223
    Top = 27
    Width = 103
    Height = 21
    Date = 39196.577854745370000000
    Time = 39196.577854745370000000
    TabOrder = 7
  end
  object Button3: TButton
    Left = 8
    Top = 55
    Width = 97
    Height = 25
    Caption = 'Last VQ Date'
    TabOrder = 8
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 111
    Top = 86
    Width = 90
    Height = 25
    Caption = 'check Sum'
    TabOrder = 9
    OnClick = Button4Click
  end
  object cbsourceparam: TComboBox
    Left = 8
    Top = 27
    Width = 97
    Height = 21
    DropDownCount = 15
    ItemHeight = 13
    TabOrder = 10
    Text = 'cbsourceparam'
  end
  object btnClientSourceOPen: TButton
    Left = 574
    Top = 85
    Width = 130
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'open client source'
    TabOrder = 11
    OnClick = btnClientSourceOPenClick
  end
  object Button5: TButton
    Left = 574
    Top = 54
    Width = 130
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'get last client update'
    TabOrder = 12
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 574
    Top = 116
    Width = 130
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'open client target'
    TabOrder = 13
    OnClick = Button6Click
  end
  object btnClientDateCopy: TButton
    Left = 718
    Top = 54
    Width = 107
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'client copy date'
    TabOrder = 14
    OnClick = btnClientDateCopyClick
  end
  object Button7: TButton
    Left = 332
    Top = 8
    Width = 49
    Height = 25
    Caption = 'kill'
    TabOrder = 15
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 718
    Top = 85
    Width = 107
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'client check sum'
    TabOrder = 16
    OnClick = Button8Click
  end
  object Button1: TButton
    Left = 387
    Top = 8
    Width = 75
    Height = 25
    Caption = 'test run'
    TabOrder = 17
    OnClick = Button1Click
  end
  object Memo2: TMemo
    Left = 366
    Top = 148
    Width = 459
    Height = 125
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 18
  end
  object Button9: TButton
    Left = 505
    Top = 8
    Width = 79
    Height = 25
    Caption = 'Update Stats'
    TabOrder = 19
    OnClick = Button9Click
  end
  object sbar: TStatusBar
    Left = 0
    Top = 407
    Width = 834
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object Button10: TButton
    Left = 632
    Top = 8
    Width = 65
    Height = 25
    Caption = 'test Mail'
    TabOrder = 21
    OnClick = Button10Click
  end
  object btnVQCopyOneRecord: TButton
    Left = 111
    Top = 117
    Width = 90
    Height = 25
    Caption = 'copy one'
    TabOrder = 22
    OnClick = btnVQCopyOneRecordClick
  end
  object btnCopyOneClient: TButton
    Left = 718
    Top = 117
    Width = 108
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'copy one'
    TabOrder = 23
    OnClick = btnCopyOneClientClick
  end
  object connTarget: TIfxConnection
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    Left = 528
    Top = 8
  end
  object connSource: TIfxConnection
    Left = 464
    Top = 8
  end
  object QSource: TIfxQuery
    Connection = connSource
    BeforeOpen = QSourceBeforeOpen
    Left = 464
    Top = 40
  end
  object dsSource: TDataSource
    DataSet = QSource
    Left = 464
    Top = 72
  end
  object dsTargetVQ: TDataSource
    DataSet = Tbl_Target_VQ
    Left = 528
    Top = 72
  end
  object Tbl_Target_VQ: TIfxTable
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    BeforeOpen = Tbl_Target_VQBeforeOpen
    IndexName = 'sysvchrel'
    TableName = 'vouchandqueue'
    Left = 528
    Top = 40
  end
  object Msg: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 352
    Top = 72
  end
  object SMTP: TIdSMTP
    UseEhlo = False
    Host = 'po.aemaine.com'
    SASLMechanisms = <>
    Left = 392
    Top = 72
  end
  object QclientSource: TIfxQuery
    Connection = connSource
    BeforeOpen = QclientSourceBeforeOpen
    Left = 464
    Top = 352
  end
  object dsClientSource: TDataSource
    DataSet = QclientSource
    Left = 464
    Top = 384
  end
  object dsTargetClient: TDataSource
    DataSet = tbl_target_client
    Left = 496
    Top = 384
  end
  object tbl_target_client: TIfxTable
    Connection = connTarget
    BeforeOpen = tbl_target_clientBeforeOpen
    TableName = 'g_client'
    Left = 496
    Top = 352
  end
end
