unit main_update;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, DB, IfxCustomDataSet, IfxQuery, IfxConnection, ///dialogs,
  jmclasses,
  aecommonfunc,
  apicryptunit1, Grids, DBGrids, ComCtrls, IfxTable, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdAttachmentFile,
  IdSMTPBase, IdSMTP, IdBaseComponent, IdMessage;



const
  SQL_Get_VandC =
    'SELECT "[[basesys]]" basesys, "uid" clientid, "" cryptcc, vcnumb, vchrel, voit1, voit2, voit3, ' +
    'voit1ret, voit2ret, voit3ret, voit4ret, voit1whl, vopsubt, vopstype1, voit2whl, voit3whl, ' +
    'voit4whl, voptax1, voptax2, vcntype, vciata, voptype, vccrcardid, vccrcard, vcccmonth, ' +
    'vcrid, vccomtaken, vccom1, vccom2, vcdisaddamount, affiliate1, affiliate2, vodddtestat, ' +
    'vopcurrency, vopcurrate, vcccauthcode, vopid, vopid2, vopctry1, vopctry2, vopprov1, ' +
    'vopprov2, vopcity1, vopcity2, voploc1, voploc2, voprd1, voiamt5, vcfpover1, voxtp1, voxtp1w, ' +
    'voxtp2w, voxtp3w, voxtp4w, vopper1, vopper2, vchdate, voptxper1, voptxper2, vccashdeposi, ' +
    'vopdate1, vopdate2, voptime1, voptime2, voptime3, vopper3, voit1sw, vopcomt1, vcvpromo1, ' +
    'vcvpromo2, vcvpromo3, vccconfirm, vcclientid, vcvendorattn, vccomments, vccomments1, ' +
    'vcspecrequest, vcarrvair, vcarrvflight, vceta, vcprepay, vcresoffice, vcst1, vcst2, ' +
    'vcst3, vcstdte1, vcstdte2, vcstdte3, vord1, vord2, vord3, vord4, vord5, vord6, vord7, vord8, ' +
    'vord9, vord10, vowd1, vowd2, vowd3, vowd4, vowd5, vopwd6, vopwd7, vopwd8, vopwd9, vopwd10, ' +
    'vopdatecreate, vopdatemodif, vopamt1, vopamt2, vopamt3, vopamt4, vopamt5, vopamt6, vopamt7, ' +
    'vopamt8, vopamt9, origcurr, voiamt1, voiamt2, voiamt3, voiamt4, voprcode, voprt1, voconsort, ' +
    'voconsort2, voconsort3, vcconsortsub, vcconsortsub2, vcconsortsub3, vorecord, voxtp1t, voxtp2, ' +
    'voxtp2t, vocusttype, vccomover1, vouch.homecurr, vouch.homectry, vouch.altresnum1, vchpqbath, ' +
    'vchpqtype, vchpqvch, vchpqvchln, vchpqseq, vchpstat, vchpmanneed, vchpoper, vchptranctry, vchpcity, ' +
    'vchplocation, vchplprintdate, vchpqcccredsale, vchpqnsw1, vchpqnsw2, vchpqnsw3 ' +
  //  'FROM VOUCH, VCHPQUEUE WHERE VCHPQBATH = :BatchDate AND VCNUMB = VCHPQVCH  AND VCHREL = VCHPQVCHLN ' +
  'FROM VOUCH, VCHPQUEUE WHERE (VCHPQBATH between :bdate1 and :bdate2) AND VCNUMB = VCHPQVCH  AND VCHREL = VCHPQVCHLN ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ' +
    '( (vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N"))';  // change to OR 7/16/2007


  SQL_getUpdateList = // must have logging on for x-db join
    'Select distinct vchpqbath From vchpqueue where vchpqbath not in ' +
    '( Select vchpqbath From rpbus@online_f50aus::vouchandqueue where basesys = "[[basesys]]")';


  SQL_CheckSumTarget = 'Select sum(  vord4-vopamt2+voit1ret+voit2ret+voit3ret+voit4ret+vord6+vord8+voptax1+vord3) ' +
    ' From vouchandqueue where basesys = :basesys and vchpqbath between :date1 and :date2';


  SQL_CheckSumSource = 'SELECT sum(vord4-vopamt2+voit1ret+voit2ret+voit3ret+voit4ret+vord6+vord8+voptax1+vord3) ' +
    ' FROM VOUCH, VCHPQUEUE WHERE VCHPQBATH BETWEEN :date1 and :date2 AND VCNUMB = VCHPQVCH ' +
    ' AND VCHREL = VCHPQVCHLN AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" ' +
    ' and  (VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ' +
    '((vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N"))';

  SQL_CheckSumClientSource = 'select sum(vcnumb+vchrel) from ' +
    'vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' +
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2';

  SQL_CheckCountClientSource = 'select count(*) from ' +
    'vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' +
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2';


  SQL_CheckSumClientTarget = 'Select sum(  gc_voucher+ gc_release ) ' +
    ' From g_client where gc_system = :basesys and gc_batchdt between :date1 and :date2';

  SQL_CheckCountClientTarget = 'Select count(*) ' +
    ' From g_client where gc_system = :basesys and gc_batchdt between :date1 and :date2';


  SQL_Get_CLients = 'select 0 gc_key, "UID" gc_uid, trim(upper(clvlast)) gc_lastname, ' +
    'trim(upper(clvfirst)) gc_firstname,  ' +
    'trim(upper(clvinit)) gc_initial, ' +
    'trim(upper(clvsurname)) gc_surname,  ' +
    'trim(upper(clvaddr1)) gc_addr1,  ' +
    'trim(upper(clvaddr2)) gc_addr2, ' +
    'trim(upper(clvprov)) gc_prov,  ' +
    'trim(upper(clvcity)) gc_city, ' +
    'trim(upper(clvpostcode)) gc_postcode,  ' +
    'trim(upper(clvctry)) gc_ctry, ' +
    'trim(upper(clvphone)) gc_phone,  ' +
    'trim(upper(clfax)) gc_fax, ' +
    '"cell" gc_cell,  ' +
    'trim(upper(clemail)) gc_email,  ' +
    '"*PASSW*" gc_password,  ' +
    '"[[basesys]]" gc_system,  ' +
    'vcnumb gc_voucher, ' +
    'vchrel gc_release, ' +
    'trim(upper(vciata)) gc_iata,  ' +
    'vchpqbath gc_batchdt,  ' +
    '"12/31/1899" gc_lastupdt  ' +
    'from vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' +   // or change 7/16/2007 as above
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2 order by vcnumb, vchrel';



type
  TFormDssFeed = class(TForm)
    connTarget: TIfxConnection;
    connSource: TIfxConnection;
    QSource: TIfxQuery;
    Label1: TLabel;
    Memo1: TMemo;
    btnVQOpen: TButton;
    DBGrid1: TDBGrid;
    dsSource: TDataSource;
    dsTargetVQ: TDataSource;
    dtp1: TDateTimePicker;
    Tbl_Target_VQ: TIfxTable;
    DBGrid2: TDBGrid;
    Button2: TButton;
    btnVCCopyDate: TButton;
    dtp2: TDateTimePicker;
    Label4: TLabel;
    Label5: TLabel;
    Button3: TButton;
    Msg: TIdMessage;
    SMTP: TIdSMTP;
    Button4: TButton;
    cbsourceparam: TComboBox;
    btnClientSourceOPen: TButton;
    QclientSource: TIfxQuery;
    dsClientSource: TDataSource;
    dsTargetClient: TDataSource;
    tbl_target_client: TIfxTable;
    Button5: TButton;
    Button6: TButton;
    btnClientDateCopy: TButton;
    Button7: TButton;
    Button8: TButton;
    Button1: TButton;
    Memo2: TMemo;
    Button9: TButton;
    sbar: TStatusBar;
    Button10: TButton;
    btnVQCopyOneRecord: TButton;
    btnCopyOneClient: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnVQOpenClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnVCCopyDateClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btnClientSourceOPenClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure QSourceBeforeOpen(DataSet: TDataSet);
    procedure Tbl_Target_VQBeforeOpen(DataSet: TDataSet);
    procedure QclientSourceBeforeOpen(DataSet: TDataSet);
    procedure tbl_target_clientBeforeOpen(DataSet: TDataSet);
    procedure btnClientDateCopyClick(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure btnVQCopyOneRecordClick(Sender: TObject);
    procedure btnCopyOneClientClick(Sender: TObject);

  private
    paramList, BaseSysList, logList: TStringList;
    g_source: string;
    bKill: boolean;

    function CryptIt(Instr: string): string;
    procedure SetConn(InConn: TIfxCOnnection);
    function Update_VCNTYPE(bsys: string; vch, rel: Integer): string;
    function insertVQ(): string;
    function insert_g_client(): string;
    function OpenSourceVQ(InBaseSys: string; date1, date2: TDate): boolean;

    function GetLastVQUpdate(baseSYS: string): TDate;
    function OpenSourceClient(InBaseSys: string; date1, date2: TDate): boolean;

    function GetLastGClientUpdate(baseSYS: string): TDate;
    procedure runByParams(IsAnError: Boolean; errorStr: string);
    function StripIt(InStr: string): string;
    procedure EMAIL(sSubject, sBody: string);
    procedure CheckSums(dstart, dend: TDate; basesys: string);
    procedure CheckClientSums(dStart, dEnd: TDate; baseSys: string);
    function FindAndUpdateClient(basesys: string; voucher: Integer): boolean;

    function AutoUpdateVQ(basesystem: string): string;
    function AutoUpdateGClient(basesystem: string): string;

    procedure VQCopyForDates(BaseSys: string; startDT, EndDT: TDate);
    procedure ClientCopyForDates(BaseSys: string; startDT, EndDT: TDate);

    procedure UpdateStats();
    procedure SetBar0(InStr: string);
    procedure SetBar1(InStr: string);
  public
    { Public declarations }
  end;

var
  FormDssFeed: TFormDssFeed;

implementation

{$R *.dfm}

procedure TFormDssFeed.SetBar0(InStr: string);
begin
  sbar.Panels[0].text := InStr;
end;

procedure TFormDssFeed.SetBar1(InStr: string);
begin
  sbar.Panels[1].text := InStr;
end;

procedure TFormDssFeed.UpdateStats;
var ass: integer;

  procedure asdf();
  begin
    application.ProcessMessages;
    ass := 0;
  end;

  procedure sav(InStr: string);
  begin
    memo2.Lines.Add(Instr);
    SetBar1(InStr);
    jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', InStr);
  end;

begin
  asdf();

  try
    setconn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'target connect fail Update Stats --> ' + E.Message);
    end;
  end;

  try
    sav('update statistics start.');

    ass := connTarget.execute('update statistics medium for table vouchandqueue distributions only;');
    sav('update statistics medium for table vouchandqueue distributions only; --> ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics medium for table g_client distributions only;');
    sav('update statistics medium for table g_client distributions only; ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table vouchandqueue(vchpqbath);');
    sav('update statistics high for table vouchandqueue(vchpqbath);' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table vouchandqueue(basesys);');
    sav('update statistics high for table vouchandqueue(basesys); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table vouchandqueue(clientid);');
    sav('update statistics high for table vouchandqueue(clientid);' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table vouchandqueue(vcnumb); ');
    sav('update statistics high for table vouchandqueue(vcnumb); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table vouchandqueue(vchrel); ');
    sav('update statistics high for table vouchandqueue(vchrel); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vcntype); ');
    memo2.Lines.Add('update statistics low for table vouchandqueue(vcntype); ' + IntToStr(ass));
    jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'update statistics low for table vouchandqueue(vcntype); ' + IntToStr(ass));

    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vciata);  ');
    sav('update statistics low for table vouchandqueue(vciata);  ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vopid); ');
    sav('update statistics low for table vouchandqueue(vopid); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vopctry1); ');
    sav('update statistics low for table vouchandqueue(vopctry1); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vopcity1); ');
    sav('update statistics low for table vouchandqueue(vopcity1); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(voploc1);  ');
    sav('update statistics low for table vouchandqueue(voploc1);  ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vopdate1); ');
    sav('update statistics low for table vouchandqueue(vopdate1); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(vopdate2); ');
    sav('update statistics low for table vouchandqueue(vopdate2); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table vouchandqueue(voconsort); ');
    sav('update statistics low for table vouchandqueue(voconsort); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_key); ');
    sav('update statistics high for table g_client(gc_key); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_phone); ');
    sav('update statistics high for table g_client(gc_phone); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_uid); ');
    sav('update statistics high for table g_client(gc_uid); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_email); ');
    sav('update statistics high for table g_client(gc_email); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_system); ');
    sav('update statistics high for table g_client(gc_system); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_lastname); ');
    sav('update statistics high for table g_client(gc_lastname); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_iata); ');
    sav('update statistics high for table g_client(gc_iata); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_batchdt); ');
    sav('update statistics high for table g_client(gc_batchdt); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table g_client(gc_lastupdt); ');
    sav('update statistics high for table g_client(gc_lastupdt); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table g_client(gc_firstname); ');
    sav('update statistics low for table g_client(gc_firstname); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table g_client(gc_voucher); ');
    sav('update statistics low for table g_client(gc_voucher); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics low for table g_client(gc_release); ');
    sav('update statistics low for table g_client(gc_release); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table dss_track_reports(dtr_key); ');
    sav('update statistics high for table dss_track_reports(dtr_key); '+ IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table dss_track_reports(dtr_user_name); ');
    sav('update statistics high for table dss_track_reports(dtr_user_name); ' + IntToStr(ass));
    asdf();

    ass := connTarget.execute('update statistics high for table dss_track_reports(dtr_run_date);  ');
    sav('update statistics high for table dss_track_reports(dtr_run_date);  ' + IntToStr(ass));
    asdf();

    sav('update statistics done');
    asdf();
  except on e: Exception do
    begin
      savelogfile('dss_upd', 'Err update statistics result --> [' + IntToStr(ass) + '] ' + e.Message);
      memo2.Lines.Add('Err update statistics result --> [' + IntToStr(ass) + '] ' + e.Message);
    end;
  end;
end;

function TFormDssFeed.AutoUpdateVQ(basesystem: string): string;
var
  ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';
  ErrorList := TStringList.create;

  try
    try
      MyDate := GetLastVQUpdate(basesystem); // sets target connection active
                                             // also validates baseSystem
      if MyDate = 0 then
      begin
        ErrorList.add('No date returned *** aborted ** GetLastVCUpdate(' + basesystem + ')');
        result := errorList.Text;
        exit;
      end
      else
      begin
        date1 := Mydate + 1;
        date2 := now - 7;

        if trunc(date2) <= trunc(date1) then date2 := date1;

        if (trunc(date2) >= now - 7) then
        begin
          result := 'no need to update ' + basesystem + ' last update: ' + DateTOStr(Mydate);
          exit;
        end;
      end; // MyDate <> 0

      // should have valid values for basesystem, date1 and date2 here

      try // close and open source for feed
        OpenSourceVQ(baseSystem, date1, date2);
      except on e: exception do
        begin
          ErrorList.add('system: ' + baseSystem + datetostr(date1) + ' to ' + datetostr(date2) + ' --> source connect fail --> ' + E.Message);
          exit;
        end;
      end;

      memo2.Lines.add('debug system: ' + baseSystem + ' ' + datetostr(date1) + ' to ' + datetostr(date2));

      memo1.Clear;

      // this func adds errors to the memo -
      VQCopyForDates(baseSystem, date1, date2);

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdateVQ: General exception --> ' + e.Message)
      end;
    end;

    if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
      // EMAIL('errors on AutoUpdateVQ', errorList.Text);
    end;
  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

function TFormDssFeed.AutoUpdateGClient(basesystem: string): string;
var ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';
  ErrorList := TStringList.create;

  try
    try
      MyDate := GetLastGClientUpdate(basesystem); // sets target connection active
                                             // also validates baseSystem
      if MyDate = 0 then
      begin
        ErrorList.add('No date returned *** aborted ** GetLastGClientUpdate(' + basesystem + ')');
        result := errorList.Text;
        exit;
      end
      else
      begin
        date1 := Mydate + 1;
        date2 := now - 7;

        if trunc(date2) <= trunc(date1) then date2 := date1;

        if (trunc(date2) >= now - 7) then
        begin
          result := 'no need to update client info for ' + basesystem + '. last update: ' + DateTOStr(Mydate);
          exit;
        end;
      end; // MyDate <> 0

           // should have valid values for basesystem, date1 and date2 here
      try // close and open client Source.
        OpenSourceClient(baseSystem, date1, date2);
      except on e: exception do
        begin
          ErrorList.add('system: ' + baseSystem + datetostr(date1) + ' to ' + datetostr(date2) + ' --> source connect fail --> ' + E.Message);
          exit;
        end;
      end;

      memo1.Clear;

      // this func adds errors to the memo -
      ClientCopyForDates(baseSystem, date1, date2);

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdateVQ: General exception --> ' + e.Message)
      end;
    end;

    if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
     // EMAIL('errors on AutoUpdateVQ', errorList.Text);
    end;


  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;


function TFormDssFeed.FindAndUpdateClient(basesys: string; voucher: Integer): boolean;
var theSQL, temp: string;
  MyQuery: TIfxQuery;
  i: integer;
begin
  result := false;
  basesys := trim(uppercase(basesys));

  try
    theSql := 'select * From g_client where gc_system = "' + basesys + '" and gc_voucher = ' +
      IntToStr(voucher);
  except on e: exception do
    begin
    //exit;
    end;
  end;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.RequestLive := true;
      MyQuery.sql.Add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        MyQuery.Edit;

        with QClientSource do
        begin
          for i := 0 to fieldcount - 1 do
          begin

            temp := '';
            temp := trim(LowerCase(fields[i].fieldname));

            if (temp = 'gc_key') or
              (temp = 'gc_uid') then continue
            else if (temp = 'gc_phone') or (temp = 'gc_fax') or (temp = 'gc_cell') then
            begin // STRIP ALL PHONE NUMBERS OF NON NUMERIC CHARACTERS
              MyQuery.fieldbyname(fields[i].fieldname).AsString :=
                StripIt(trim(UpperCase(fields[i].AsString)));
            end
            else
            begin
              try
                case fields[i].DataType of
                  ftString:
                    begin // format all strings:
                      MyQuery.fieldbyname(fields[i].fieldname).AsString :=
                        trim(UpperCase(fields[i].AsString));
                    end
                else
                  begin
                    MyQuery.fieldbyname(fields[i].fieldname).value := fields[i].value;
                  end;
                end; // case
              except on e: exception do
                begin
                  raise(exception.Create('FindAndUpdateClient FOR LOOP:  ' + e.message));
                  savelogfile('dss_upd', 'FindAndUpdateClient FOR LOOP:  ' + e.message);
                end;
              end;
            end; // else
          end;
        end; // with do

        MyQuery.Post;
        result := true;
      end;
    except on e: exception do
      begin
        result := false;
        savelogfile('dss_upd', 'FindAndUpdateClient --> sys: ' +
          basesys + ' voucher: ' + IntTOStr(voucher)
          + ' --> ' + E.Message);
        raise(exception.Create('FindAndUpdateClient --> sys: ' +
          basesys + ' voucher: ' + IntTOStr(voucher)
          + ' --> ' + E.Message));
        //exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end;

function TFormDssFeed.GetLastVQUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(basesyslist, baseSYS)) then
  begin
    savelogfile('dss_upd', 'GetLastVCUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    SetConn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastVCUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'select max(vchpqbath) from vouchandqueue where ' +
    'basesys = "' + baseSYS + '" and vchpqbath > :date1';

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := now - 60;
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLastUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;

end;


function TFormDssFeed.Update_VCNTYPE(bsys: string; vch, rel: Integer): string;
var MyQuery: TIfxQuery; // update vcntype to History when new vouchers come in
  theSQL, v, r: string;
  i: integer;
begin
  result := '';

  theSQL := 'update vouchandqueue set vcntype = "H" where basesys = :sbasesys and ' +
    'vcnumb = :InVCNUMB and vchrel < :Invchrel';

  bsys := trim(Uppercase(bsys));
  if bsys = '' then
  begin
    result := 'no basesys';
    exit;
  end;

  v := '';
  r := '';
  i := 0;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      r := IntToStr(rel);
      v := IntTOStr(vch);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('sbasesys').AsString := bsys;
      MyQuery.ParamByName('InVCNUMB').AsInteger := vch;
      MyQuery.ParamByName('Invchrel').AsInteger := rel;
      i := MyQuery.ExecSQL;
    except on e: exception do
      begin
        result := bsys + ' voucher: ' + IntTostr(vch) + '-' + IntToStr(rel) + ' --> ' + E.Message;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;
     {
function TForm1.DeleteOldClient(bsys: string; vch, rel: Integer): string;
var MyQuery: TIfxQuery; // update vcntype to History when new vouchers come in
  theSQL, v, r: string;
  i: integer;
begin
  result := '';

  theSql := 'delete from g_client where gc_system = :sBaseSys and ' +
    'gc_voucher = :InVOUCH and gc_release < :InRELEASE';

  bsys := trim(Uppercase(bsys));
  if bsys = '' then
  begin
    result := 'no basesys';
    exit;
  end;

  v := '';
  r := '';
  i := 0;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      r := IntToStr(rel);
      v := IntTOStr(vch);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('sBaseSys').AsString := bsys;
      MyQuery.ParamByName('InVOUCH').AsInteger := vch;
      MyQuery.ParamByName('InRELEASE').AsInteger := rel;
      i := MyQuery.ExecSQL;
    except on e: exception do
      begin
        result := bsys + ' voucher: ' + IntTostr(vch) + '-' + IntToStr(rel) + ' --> ' + E.Message;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;
      }

function TFormDssFeed.insertVQ(): string;
var temp: string;
  i: Integer;
begin
  result := '';

  try
    Tbl_Target_VQ.Insert;
  except;
  end;

  Tbl_Target_VQ.FieldByName('basesys').AsString :=
    trim(Uppercase(QSource.FieldByName('basesys').AsString));

  Tbl_Target_VQ.FieldByName('clientid').AsString := ''; // maybe we have client ID in vouch later?

  temp := '';
  try
    if trim(QSource.FieldByName('vccrcard').AsString) > '' then
      temp := CryptIt(QSource.FieldByName('vccrcard').AsString);
  except on e: Exception do
    begin
      stradd(result, ' crypt err: --> ' + e.Message);

    end;
  end;

  if temp > '' then Tbl_Target_VQ.FieldByName('cryptcc').AsString := temp
  else Tbl_Target_VQ.FieldByName('cryptcc').AsString := '';
  temp := '';

  with QSource do
  begin
    for i := 0 to fieldcount - 1 do
    begin
      temp := '';
      temp := trim(LowerCase(fields[i].fieldname));
      if (temp = 'basesys') or
        (temp = 'clientid') or
        (temp = 'vccrcard') or
        (temp = 'cryptcc') then continue
      else
      begin
        try
          case fields[i].DataType of
           {  ftBlob, ftMemo, ftGraphic,
              ftFmtMemo, ftParadoxOle, ftDBaseOle,
              ftTypedBinary, ftCursor, ftVarBytes,
              ftLargeInt, ftADT, ftArray,
              ftReference,
              ftDataSet:
              begin
               //****************************** target := 'BLOB' ;
             end;
           }
            ftString:
              begin // format all strings:
                Tbl_Target_VQ.fieldbyname(fields[i].fieldname).AsString := trim(UpperCase(fields[i].AsString));
              end
          else
            begin
              Tbl_Target_VQ.fieldbyname(fields[i].fieldname).value := fields[i].value;
            end;
          end; // case
        except on e: exception do
            stradd(result, ' FOR LOOP:  ' + e.message);
        end;
      end; // else
    end;
  end; // with do

  try
    Tbl_Target_VQ.Post;
  except on e: exception do
      stradd(result, 'post --> ' + e.message);
  end; //try
  application.ProcessMessages;
end;

procedure TFormDssFeed.SetConn(InConn: TIfxCOnnection);
begin
  if not InConn.Connected then
  begin
    try
      InConn.Connected := true;
    except on e: exception do
      begin
        raise(Exception.Create('setConn --> ' + e.Message));
      end;
    end;
  end;
end;

procedure TFormDssFeed.tbl_target_clientBeforeOpen(DataSet: TDataSet);
begin
  dbgrid2.DataSource := dsTargetClient;
end;

procedure TFormDssFeed.Tbl_Target_VQBeforeOpen(DataSet: TDataSet);
begin
  dbgrid2.DataSource := dsTargetVQ;
end;

procedure TFormDssFeed.Button10Click(Sender: TObject);
begin
   EMAIL('Test', memo1.Lines.text);
end;

procedure TFormDssFeed.btnCopyOneClientClick(Sender: TObject);
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean;      // manually run client update
begin // assume source is open and has data
  bKill := false;

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        memo2.lines.add('target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;

  if QClientSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update for client')
  else
  begin
   // QClientSource.First;

    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;

    //while not (QClientSource.Eof) do
    //begin
     // if bKill then Break;
      inc(myCount);

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
      svchrel := QclientSource.FieldByName('gc_release').AsString;
      ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
      ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

      if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
      begin
        dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
        try
          jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);

        except;
        end;
        inc(dupeCOunt);
      end
      else
      begin
        dupeStr := '';
      end;

      holdVoucher := ivcnumb;
      holdRelease := ivchrel;

      try
        bFound := FindAndUpdateClient(sbasesys, ivcnumb);
      except on e: exception do
        begin
          memo1.Lines.add(e.Message);
          savelogfile('dss_upd', sbasesys +
            ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
          bFound := false;
        end;
      end;

      // debug:
      if bFound then
        memo1.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
      else
      begin
        errstr := insert_g_client();

        memo1.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
      end;

      if errstr > '' then
      begin
        memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);

        savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      end;
      errstr := '';
      // Break;
    //  QClientSource.Next;
    //end;
  end;

 // CheckClientSums(dtp1.date, dtp2.date, cbSourceParam.Text);
 // memo1.Lines.add('Dupes:  ' + IntToStr(DupeCount));

 // if dupeCOunt > 0 then
 // begin
  //  try
  //    jSaveFile('c:\clog\', 'dss_record', '.txt', 'Dupes:  ' + IntToStr(DupeCount));
   //   LogList.Add('Dupes:  ' + IntToStr(DupeCount));

   // except;
   // end;
  //end;

 // if bKill then memo1.Lines.add('done and Killed')
 // else memo1.Lines.add('done');
end;

procedure TFormDssFeed.btnVQCopyOneRecordClick(Sender: TObject);
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update')
  else
  begin
   // QSource.First;

   // while not (QSource.Eof) do
   // begin
      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QSource.FieldByName('basesys').AsString;
      svcnumb := QSource.FieldByName('vcnumb').AsString;
      svchrel := QSource.FieldByName('vchrel').AsString;
      ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QSource.FieldByName('vchrel').AsInteger;


      errstr := insertVQ();

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

      errstr := '';
      if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);

     // QSource.Next;
    //end;
  end;

 // CheckSums(dtp1.date, dtp2.date, cbSourceParam.Text);
 // memo1.Lines.add('done');
end;

procedure TFormDssFeed.Button1Click(Sender: TObject);
var ass: string;
  IsErr: boolean;
begin // test button

  if cbSourceParam.text = '' then
  begin
    memo2.lines.add('select a source');
    cbSOurceParam.DroppedDown := true;
    exit;
  end;

  IsErr := false;
  try
    ass := AutoUpdateVQ(cbSourceParam.text);
  except on e: exception do
    begin
      IsErr := true;
      memo2.Lines.Add('result: ' + ass + ':: AutoUpdateVQ(' + cbSourceParam.text + ') -->' + e.message);
         //exit;
    end;
  end;
  if not (IsErr) then memo2.Lines.Add('result: ' + ass + ' AutoUpdateVQ no errors.')
  else memo2.Lines.Add('result: ' + ass + ' AutoUpdateVQ YES errors.');

  IsErr := false;
  ass := '';
  try
    ass := AutoUpdateGClient(cbSourceParam.text);
  except on e: exception do
    begin
      IsErr := true;
      memo2.Lines.Add('AutoUpdateGClient(' + cbSourceParam.text + ') --> Msg: ' +
        ass + ' e.msg: ' + e.message);
    end;
  end;

  if not (IsErr) then memo2.Lines.Add('result: ' + ass + ' AutoUpdateGClient no errors.');


end;

procedure TFormDssFeed.Button2Click(Sender: TObject);
begin
  if (Tbl_Target_VQ.Active) then Tbl_Target_VQ.Close;
  if connTarget.Connected then connTarget.Close;
 // dsTargetVQ.DataSet := Tbl_Target_VQ;

  try
    connTarget.Connected := true;
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
      exit;
    end;
  end;

  try
    Tbl_Target_VQ.Open;
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
      exit;
    end;
  end;

end;

procedure TFormDssFeed.Button3Click(Sender: TObject);
var MyDate: TDate;
begin
  MyDate := 0;
  MyDate := GetLastVQUpdate(cbSourceParam.text);

  if MyDate = 0 then
  begin
    memo2.lines.add('Err --> no date returned');
  end
  else
  begin
    memo2.lines.add('Last date processed: ' + DateToStr(MyDate));

    dtp1.Date := Mydate + 1;
    dtp2.Date := now - 7;
    if trunc(dtp2.Date) <= trunc(dtp1.date) then dtp2.Date := dtp1.Date;

    if (trunc(dtp2.Date) >= now - 7) then
      memo2.lines.add('VouchandQueue is up to date.' + #10#13 + 'No update needed.')
    else
      memo2.lines.add('run from ' + datetostr(dtp1.date) + ' to ' + datetostr(dtp2.date));
  end;
end;


procedure TFormDssFeed.Button4Click(Sender: TObject);
begin
  checksums(dtp1.date, dtp2.Date, cbSourceParam.text);
end;

procedure TFormDssFeed.Button5Click(Sender: TObject);
var MyDate: TDate;
begin
  MyDate := 0;
  MyDate := GetLastGClientUpdate(cbSourceParam.text);

  if MyDate = 0 then
  begin
    memo2.lines.add('Err --> no date returned');
  end
  else
  begin
    memo2.lines.add('Last date processed: ' + DateToStr(MyDate));

    dtp1.Date := Mydate + 1;
    dtp2.Date := now - 7;

    if trunc(dtp2.Date) <= trunc(dtp1.date) then dtp2.Date := dtp1.Date;

    if (trunc(dtp2.Date) >= now - 7) then
      memo2.lines.add('GClient is already up to date.')
    else
      memo2.lines.add('Run from ' + datetostr(dtp1.date) + ' to ' + datetostr(dtp2.date));
  end;
end;


function TFormDssFeed.GetLastGClientUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(basesyslist, baseSYS)) then
  begin
    memo1.Lines.add('GetLastGCClientUpdate --> invalid basesys: [' + baseSYS + ']');
    savelogfile('dss_upd', 'GetLastGCClientUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    SetConn(connTarget);
  except on e: exception do
    begin
      memo1.Lines.add('GetLastGCCLientUpdate --> connect failure --> ' + E.Message);
      savelogfile('dss_upd', 'GetLastGCCLientUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'select max(gc_batchdt) from g_client where ' +
    'gc_system = "' + baseSYS + '" and gc_batchdt > :date1';

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := now - 60;
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        memo1.Lines.add('GetLastGclientUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'GetLastGclientUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;

end;


procedure TFormDssFeed.Button6Click(Sender: TObject);
begin
  if (tbl_target_client.Active) then tbl_target_client.Close;
  if connTarget.Connected then connTarget.Close;

  try
    connTarget.Connected := true;
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
      exit;
    end;
  end;


  try
    tbl_target_client.Open;
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
      exit;
    end;
  end;
end;

procedure TFormDssFeed.Button7Click(Sender: TObject);
begin
  bkill := not (bKill);
end;

procedure TFormDssFeed.Button8Click(Sender: TObject);
begin
  checkClientsums(dtp1.date, dtp2.Date, cbSourceParam.text);
end;

procedure TFormDssFeed.Button9Click(Sender: TObject);
begin
  UpdateStats;
end;

procedure TFormDssFeed.btnClientDateCopyClick(Sender: TObject);
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean;      // manually run client update
begin // assume source is open and has data
  bKill := false;

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        memo2.lines.add('target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;

  if QClientSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update for client')
  else
  begin
    QClientSource.First;

    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;

    while not (QClientSource.Eof) do
    begin
      if bKill then Break;
      inc(myCount);

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
      svchrel := QclientSource.FieldByName('gc_release').AsString;
      ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
      ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

      if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
      begin
        dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
        try
          jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);

        except;
        end;
        inc(dupeCOunt);
      end
      else
      begin
        dupeStr := '';
      end;

      holdVoucher := ivcnumb;
      holdRelease := ivchrel;

      try
        bFound := FindAndUpdateClient(sbasesys, ivcnumb);
      except on e: exception do
        begin
          memo1.Lines.add(e.Message);
          savelogfile('dss_upd', sbasesys +
            ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
          bFound := false;
        end;
      end;

      // debug:
      if bFound then
        memo1.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
      else
      begin
        errstr := insert_g_client();

        memo1.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
      end;

      if errstr > '' then
      begin
        memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);

        savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      end;
      errstr := '';
      // Break;
      QClientSource.Next;
    end;
  end;

  CheckClientSums(dtp1.date, dtp2.date, cbSourceParam.Text);
  memo1.Lines.add('Dupes:  ' + IntToStr(DupeCount));

  if dupeCOunt > 0 then
  begin
    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', 'Dupes:  ' + IntToStr(DupeCount));
      LogList.Add('Dupes:  ' + IntToStr(DupeCount));

    except;
    end;
  end;

  if bKill then memo1.Lines.add('done and Killed')
  else memo1.Lines.add('done');
end;

procedure TFormDssFeed.CheckSums(dstart, dend: TDate; basesys: string);
var sourceq, targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  temp: string;
begin // assuming connected
  sourceSUM := 0.0;
  TargetSUM := 0.0;
  basesys := trim(Uppercase(basesys));
  try
    try
      sourceq := GetIDACQuery(connSource);
      sourceq.sql.Add(SQL_CheckSumSource);
      sourceq.ParamByName('date1').AsDate := dstart;
      sourceq.ParamByName('date2').AsDate := dend;
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourcesum := 0.0
      else sourcesum := sourceq.Fields[0].AsFloat;

    except on e: exception do
      begin
        sourcesum := 0;
        savelogfile('dss_upd', 'checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        memo1.Lines.add('checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(sourceq);
  end;

  try
    try
      targetq := GetIDACQuery(connTarget);
      targetq.sql.Add(SQL_CheckSumTarget);
      targetq.ParamByName('date1').AsDate := dstart;
      targetq.ParamByName('date2').AsDate := dend;
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      if (targetq.IsEmpty) then targetsum := 0.0
      else targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := 0;
        savelogfile('dss_upd', 'checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        memo1.Lines.add('checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(targetq);
  end;

  temp := '';
  if targetsum = sourcesum then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' VandC OK! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;

    //memo2.Lines.add(temp);


  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' VandC Fail! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;
    memo1.Lines.add(temp);


  end;


  memo2.Lines.add(temp);
end;

procedure TFormDssFeed.CheckClientSums(dStart, dEnd: TDate; baseSys: string);
var sourceq, targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  sourceCOUNT, TargetCOUNT: integer;
  temp: string;
begin // assuming connected
  sourceSUM := 0.0;
  sourceCOUNT := 0;
  TargetSUM := 0.0;
  TargetCOUNT := 0;

  basesys := trim(Uppercase(basesys));
  try
    try // do the sum
      sourceq := GetIDACQuery(connSource);
      sourceq.sql.Add(SQL_CheckSumClientSource);
      sourceq.ParamByName('date1').AsDate := dstart;
      sourceq.ParamByName('date2').AsDate := dend;
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourcesum := 0.0
      else sourcesum := sourceq.Fields[0].AsFloat;

    except on e: exception do
      begin
        sourcesum := 0;
        memo1.Lines.Add('checksum CLIENT source fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'checksum CLIENT source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
    //****************** count
    try
      //sourceq := GetIDACQuery(connSource);
      sourceq.Close;
      sourceq.SQL.Clear;
      sourceq.sql.Add(SQL_CheckCountClientSource);
      sourceq.ParamByName('date1').AsDate := dstart;
      sourceq.ParamByName('date2').AsDate := dend;
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourceCOUNT := 0
      else sourceCOUNT := sourceq.Fields[0].AsInteger;

    except on e: exception do
      begin
        sourcesum := 0;
        memo1.Lines.Add('checkCOUNT CLIENT source fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'checkCOUNT CLIENT source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
   //*******************
  finally
    FreeAndNil(sourceq);
  end;

  try
    try
      targetq := GetIDACQuery(connTarget);
      targetq.sql.Add(SQL_CheckSumClientTarget);
      targetq.ParamByName('date1').AsDate := dstart;
      targetq.ParamByName('date2').AsDate := dend;
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      if (targetq.IsEmpty) then targetsum := 0.0
      else targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := 0;
        memo1.Lines.add('checksum CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'checksum CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;

    try
      targetq.Close;
      targetq.SQL.Clear;
      targetq.sql.Add(SQL_CheckCOUNTClientTarget);
      targetq.ParamByName('date1').AsDate := dstart;
      targetq.ParamByName('date2').AsDate := dend;
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      if (targetq.IsEmpty) then targetCOUNT := 0
      else targetCOUNT := targetq.Fields[0].AsInteger;

    except on e: exception do
      begin
        targetsum := 0;
        memo1.Lines.add('checkCOUNT CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'checkCOUNT CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(targetq);
  end;

  temp := '';
  if (targetsum = sourcesum) and (TargetCount = SourceCount) then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' Client OK! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    stradd(temp, '  sourceCount: ' + IntToStr(sourceCount) + ' targetCount: ' + IntToStr(TargetCount));

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;
  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' Client Fail! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    stradd(temp, ' sourceCount: ' + IntToStr(sourceCount) + ' targetCount: ' + IntToStr(TargetCount));

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;

  end;
  memo2.Lines.add(temp);
end;


procedure TFormDssFeed.btnClientSourceOPenClick(Sender: TObject);
begin
  try
    OpenSourceClient(cbSourceparam.text, dtp1.date, dtp2.date);
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
    end;
  end;
end;

procedure TFormDssFeed.ClientCopyForDates(BaseSys: string; startDT, EndDT: TDate);
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean;
begin // assume source is open and has data
  bKill := false;
  // NEED WORK HERE STILL BRO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo1.Lines.add('ClientCopyForDates connTarget fail: ' + e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        memo1.Lines.add('ClientCopyForDates target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;

  if QClientSource.IsEmpty then
  begin
    savelogfile('dss_upd', 'ClientCopyForDates no data to update for client');
    memo1.Lines.add('ClientCopyForDates no data to update for client');
  end
  else
  begin
    QClientSource.First;

    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;

    while not (QClientSource.Eof) do
    begin
      if bKill then Break;
      inc(myCount);

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
      svchrel := QclientSource.FieldByName('gc_release').AsString;
      ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
      ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

      if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
      begin

        dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
        try
          jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);
        except;
        end;
        inc(dupeCOunt);
      end
      else
      begin
        dupeStr := '';
      end;

      holdVoucher := ivcnumb;
      holdRelease := ivchrel;

      try
        bFound := FindAndUpdateClient(sbasesys, ivcnumb);
      except on e: exception do
        begin
          memo1.Lines.add(e.Message);
          savelogfile('dss_upd', sbasesys +
            ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
          bFound := false;
        end;
      end;

      // debug:
      if bFound then
        memo2.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
      else
      begin
        errstr := insert_g_client();

        memo2.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
      end;

      if errstr > '' then
      begin
        memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);

        savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      end;
      errstr := '';
      // Break;
      QClientSource.Next;
    end;
  end;

  CheckClientSums(startDT, EndDT, BaseSys);
  //memo1.Lines.add('Dupes:  ' + IntToStr(DupeCount));

  if dupeCOunt > 0 then
  begin
    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', 'Dupes:  ' + IntToStr(DupeCount));
      LogList.Add('client Dupes:  ' + IntToStr(DupeCount));
      memo1.Lines.Add('Client Dupes:  ' + IntToStr(DupeCount));
    except;
    end;
  end;

  if bKill then memo2.Lines.add('done and Killed');
 // else memo1.Lines.add('done');
end;

procedure TFormDssFeed.VQCopyForDates(BaseSys: string; startDT, EndDT: TDate);
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo1.Lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        memo1.Lines.Add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    memo1.Lines.add('no data to update')
  else
  begin
    QSource.First;

    while not (QSource.Eof) do
    begin
      if bkill then break;


      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QSource.FieldByName('basesys').AsString;
      svcnumb := QSource.FieldByName('vcnumb').AsString;
      svchrel := QSource.FieldByName('vchrel').AsString;
      ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QSource.FieldByName('vchrel').AsInteger;


      errstr := insertVQ();

      if errstr > '' then memo1.Lines.add(sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

      errstr := '';
      if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

      if errstr > '' then memo1.Lines.add(sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);

      QSource.Next;
    end;
  end;

  CheckSums(startDT, endDT, BaseSys);
  memo2.Lines.add('done');
end;


procedure TFormDssFeed.btnVCCopyDateClick(Sender: TObject);
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update')
  else
  begin
    QSource.First;

    while not (QSource.Eof) do
    begin
      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      sbasesys := QSource.FieldByName('basesys').AsString;
      svcnumb := QSource.FieldByName('vcnumb').AsString;
      svchrel := QSource.FieldByName('vchrel').AsString;
      ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QSource.FieldByName('vchrel').AsInteger;


      errstr := insertVQ();

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

      errstr := '';
      if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);

      QSource.Next;
    end;
  end;

  CheckSums(dtp1.date, dtp2.date, cbSourceParam.Text);
  memo1.Lines.add('done');

end;


procedure TFormDssFeed.EMAIL(sSubject, sBody: string);
var sFILENAME: string;
  MyList: TStringList;
  i: Integer;
begin
  Msg.Clear;


  MyList := TStringList.Create;
  try

    MyList.Text := sBody;
    sFILENAME := GetLogFileName('dss_upd');


    with Msg do
    begin
      Body.Add('DSS Update Log:');
      Body.add('');

      if (fileexists(sFILENAME)) then
        Body.Add('Error Log file attached.');

      Body.add('--------------->');
      Body.add('');

      for I := 0 to MyList.Count - 1 do
        Body.Add(MyList[i]);

      From.Text := 'chester@autoeurope.com';
      Recipients.EMailAddresses := 'chester@autoeurope.com';
      Subject := 'DSS Update: ' + sSubject;
      Priority := TIdMessagePriority(mpNormal);
    end;   

    if (fileexists(sFILENAME)) then
    begin
      try
        TIdAttachmentFile.Create(Msg.MessageParts, sFILENAME);
      except on e: exception do
        begin
      //  showmessage('failed to attach file. Failed e-mail.');
          savelogfile('dss_upd', 'failed to attach file. Failed e-mail. ' + e.message);
          exit;
        end;
      end;
    end;

    try
      SMTP.Connect;
    except on e: exception do
      begin
        savelogfile('dss_upd', 'Failed to send e-mail. -->' + e.message);
        exit;
      end;
    end;

    try
      try
        SMTP.Send(Msg);
      except on e: exception do
        begin
        //showmessage(e.message + ': Failed to send e-mail.');
          savelogfile('dss_upd', e.message + ': Failed to send e-mail.');
        end;
      end;
    finally
      SMTP.Disconnect;
    end;



  finally
    if assigned(MyList) then FreeAndNil(MyList);

  end;

end;


procedure TFormDssFeed.runByParams(IsAnError: Boolean; errorStr: string);
var wasError, err1, err2: Boolean;
  i: Integer;
  bsys, errStr, crap, sFileName: string;
  date1, date2, MyDate: TDate;
  ErrorList: TStringList;
  LeftPos: integer;
begin
  // try crap to get the thing to paint since I call the before activate,
  // the damn thing doesn't paint the form until it's done...
  try
    FormDssFeed.Visible := true;
    LeftPos := FormDssFeed.Left;
    FormDssFeed.Left := -5000;
    FormDssFeed.BringToFront;
    FormDssFeed.repaint;
    Forms.Application.ProcessMessages;
    FormDssFeed.Left := LeftPos;
    FormDssFeed.BringToFront;
    FormDssFeed.repaint;
    Forms.Application.ProcessMessages;
  except;
  end;

  err1 := false;
  err2 := false;

  if IsAnError then
  begin
    savelogfile('dss_upd', 'RunByParams fail --> errorstr --> [' + errorStr + ']');
    setbar1('fail: ' + '[' + errorStr + ']');
    EMAIL('Run by params FAIL: ', 'errorstr --> [' + errorStr + ']');
    exit;
  end
  else
  begin

    for i := 0 to BasesysList.Count - 1 do
    begin
      if bKill then break;
      application.ProcessMessages;

      wasError := false;
      bsys := '';
      date1 := 0;
      date2 := 0;
      MyDate := 0;
      errStr := '';
      bsys := trim(Uppercase(basesyslist[i]));
      memo2.Lines.Add('Update vouch and queue: ' + bsys);
      setbar0('Update: [' + bsys + ']');


      try
        crap := AutoUpdateVQ(bsys);
      except on e: exception do
        begin
          wasError := true;
          savelogfile('dss_upd', 'AutoUpdateVQ(' + bsys + ') FOR LOOP:  Msg:' + crap + ' e.msg: ' + e.message);
        end;
      end;

      if not (wasError) then
      begin
        try
          memo2.Lines.Add('AutoUpdateVQ no errors :: ' + crap);
          setbar1('AutoUpdateVQ no errors :: ' + crap);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> autorun result: ' + crap +
            ' AutoUpdateVQ no errors.');
        except;
        end;
      end
      else
      begin
        err1 := true;
        try
          memo2.Lines.Add('AutoUpdateVQ errors :: ' + crap);
          setbar1('AutoUpdateVQ errors: ' + crap);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) +
            ' --> autorun result: ' + crap + ' AutoUpdateVQ ERRORS!');
        except;
        end;
      end;

      crap := '';
      wasError := false;
      memo2.Lines.Add('Update g client: ' + bsys);
      setbar1('update gclient: ' + crap);

      try
        crap := AutoUpdateGClient(bsys);
      except on e: exception do
        begin
          wasError := true;
          savelogfile('dss_upd', 'AutoUpdateGClient(' + bsys + ') FOR LOOP:  Msg: ' + crap + ' e.msg: ' + e.message);
        end;
      end;

      if not (wasError) then
      begin
        try
          memo2.Lines.Add('AutoUpdate GClient NO errors :: ' + crap);
          setbar1('g_client NO errors: ' + crap);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'NO errors AutoUpdateGClient ' +
            FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> autorun result: ' + crap);
        except;
        end;
      end
      else
      begin
        err2 := true;
        try
          memo2.Lines.Add('AutoUpdate G Client errors :: ' + crap);
          setbar1('gclient errors: ' + crap);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'ERRORS! AutoUpdateGClient ' + FormatDateTime('MM:DD:YYYY HH:mm', now) +
            ' --> autorun result: ' + crap);
        except;
        end;
      end;

    end; // for

    UpdateStats();

  end; // else


  EMAIL('DSS UPDATE log: ', LogList.Text);
  application.Terminate;
end;

procedure TFormDssFeed.FormCreate(Sender: TObject);
var bInit_Err: Boolean;
  sInit_ErrStr, temp: string;
begin
  bInit_err := false;
  sInit_ErrStr := '';
  bKill := false;
  GetSupportingFiles('\\fileserver\UPDATE\ini\dss.ini', 'dss.ini');
  paramlist := TStringList.Create;
  paramlist.LoadFromFile('dss.ini');
  baseSysList := TStringList.create;
  temp := paramlist.Values['host'];


  LogList := TStringList.Create;

  if temp > '!' then smtp.Host := temp
  else smtp.Host := 'po.aemaine.com';

  temp := '';

  try
    temp := paramlist.Values['basesys'];
    BaseSysList.Clear;
    Get_ListFromPipeString(basesyslist, temp);

    cbSourceParam.Items.Clear;
    cbSOurceparam.Items := basesyslist;
    cbSOurceparam.text := '';

    if not IsInList(basesyslist, 'USA') then
    begin
      sInit_ErrStr := 'basesyslist fail';
      bInit_Err := true;
    end;

  except on e: exception do
    begin
      stradd(sInit_ErrStr, '--> err: ' + E.Message);
      bInit_Err := true;
    end;
  end;

  try
    setconn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'target connect fail --> ' + E.Message);
      stradd(sInit_ErrStr, '--> contarget.connect err: ' + E.Message);
      bInit_Err := true;
    end;
  end;

  if (trim(lowercase(paramstr(1))) = '/update') then
  begin
    setbar1('start auto-run..');
    runByParams(bInit_err, sInit_errStr);
    exit;
  end
  else
  begin // manual update
    dtp1.Date := now;
    dtp2.Date := now;
  end;
end;

procedure TFormDssFeed.FormDestroy(Sender: TObject);
begin
  if assigned(paramlist) then FreeAndNil(paramlist);
  if assigned(BaseSysList) then FreeAndNil(BaseSysList);
  if assigned(LogList) then FreeAndNil(LogList);
end;

procedure TFormDssFeed.btnVQOpenClick(Sender: TObject);
begin
  try
    OpenSourceVQ(cbSourceparam.text, dtp1.date, dtp2.date);
  except on e: exception do
    begin
      memo2.lines.add(e.Message);
    end;
  end;
end;

function TFormDssFeed.OpenSourceVQ(InBaseSys: string; date1, date2: TDate): boolean;
begin
  result := false;
  dbgrid1.DataSource := dsSource;
  dbgrid2.DataSource := dsTargetVQ;

  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      caption := '';
      exit;
    end;
  end;

  try
    QSource.SQL.Clear;
    QSource.SQL.Text := SQL_Get_VandC;
    QSource.SQL.Text := StringReplace(QSource.SQL.Text, '[[basesys]]',
      InBaseSys, [rfReplaceAll, rfIgnoreCase]);

    QSource.ParamByName('bdate1').AsDate := date1;
    QSource.ParamByName('bdate2').AsDate := date2;

    memo2.Lines := QSource.SQL;
    memo2.Lines.add('date1: ' + QSource.ParamByName('bdate1').AsString);
    memo2.Lines.add('date2: ' + QSource.ParamByName('bdate2').AsString);

    //exit;
    caption := 'connecting to ' + GetCurrentDB(connSource);
    QSource.Open;
    caption := 'connected to ' + GetCurrentDB(connSource);

    memo2.Lines.add('connected to ' + GetCurrentDB(connSource));
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      caption := '';
      exit;
    end;
  end;

  result := true;
end;

procedure TFormDssFeed.QclientSourceBeforeOpen(DataSet: TDataSet);
begin
  dbgrid1.DataSource := dsClientSource;
end;

procedure TFormDssFeed.QSourceBeforeOpen(DataSet: TDataSet);
begin
  dbgrid1.DataSource := dsSource;
end;

function TFormDssFeed.OpenSourceClient(InBaseSys: string; date1, date2: TDate): boolean;
begin
 //dbgrid1.DataSource := dsClientSource;
 // dbgrid2.DataSource := dsTargetClient;

  result := false;
  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      caption := '';
      exit;
    end;
  end;

  try

    QClientSource.SQL.Clear;
    QClientSource.SQL.Text := SQL_Get_Clients;
    qClientSource.SQL.Text := StringReplace(QClientSource.SQL.Text, '[[basesys]]',
      InBaseSys, [rfReplaceAll, rfIgnoreCase]);
    QCLientSource.ParamByName('date1').AsDate := date1;
    QClientSource.ParamByName('date2').AsDate := date2;

    memo1.Lines := QClientSource.SQL;
    //exit;
    caption := 'connecting to ' + GetCurrentDB(connSource);
    QClientSource.Open;
    caption := 'connected to ' + GetCurrentDB(connSource);


  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      caption := '';
      exit;
    end;
  end;

  result := true;
end;



function TFormDssFeed.CryptIt(Instr: string): string;
var s: string;

  function StripCC(Instr: string): string;
  var i, x: integer;
    temp: string;
  begin
    if (InStr = '') then
    begin
      Result := '';
      exit;
    end
    else
    begin
      temp := '';
      x := length(InStr);

      for i := 1 to x do
      begin
        if IsDigit(Instr[i]) then temp := temp + InStr[i]
        else continue;
      end;
    end;
    Result := temp;
  end;

begin
  try
    s := '';
    s := StripCC(trim(Uppercase(InStr)));
    if trim(s) = '' then s := trim(Uppercase(InStr));
    result := API_Encrypt(true, s);
  except on e: exception do
    begin
      result := '';
      savelogfile('dss_upd', 'encrypt fail --> ' + InStr + ' --> ' + E.Message);
    end;
  end;
end;

function TFormDssFeed.StripIt(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '0'..'9': temp := temp + InStr[i];
      else
        continue;
      end;
    end;
  end;
  Result := temp;
end;

function TFormDssFeed.insert_g_client(): string;
var temp: string;
  i: Integer;
begin
  result := '';

  try
    tbl_target_client.Insert;
  except on E: EXCEPTION do
    begin
      stradd(result, '1 client set insert --> ' + e.message);
   //exit
    end;
  end;

  {   SQL_Get_CLients = 'select 0 gc_key, "UID" gc_uid, trim(upper(clvlast)) gc_lastname, ' +
        'trim(upper(clvfirst)) gc_firstname,  ' +
        'trim(upper(clvinit)) gc_initial, ' +
        'trim(upper(clvsurname)) gc_surname,  ' +
        'trim(upper(clvaddr1)) gc_addr1,  ' +
        'trim(upper(clvaddr2)) gc_addr2, ' +
        'trim(upper(clvprov)) gc_prov,  ' +
        'trim(upper(clvcity)) gc_city, ' +
        'trim(upper(clvpostcode)) gc_postcode,  ' +
        'trim(upper(clvctry)) gc_ctry, ' +
        'trim(upper(clvphone)) gc_phone,  ' +
        'trim(upper(clfax)) gc_fax, ' +
        '"cell" gc_cell,  ' +
        'trim(upper(clemail)) gc_email,  ' +
        '"*PASSW*" gc_password,  ' +
        '"[[basesys]]" gc_system,  ' +
        'vcnumb gc_voucher, ' +
        'vchrel gc_release, ' +
        'trim(upper(vciata)) gc_iata  ' +
        'vchpqbath gc_batchdt  ' +
        '"" gc_lastupdt  ' +
        'from vouch, vchpqueue, vclient  ' +
        'where vcntype = "A"  and  ' +
        '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
        '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
        '(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> "N")) and ' +
        'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
        'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
        ' vchpqbath between :date1 and :date2';

      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('vcnumb').AsString;
      svchrel := QclientSource.FieldByName('vchrel').AsString;
      ivcnumb := QclientSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QclientSource.FieldByName('vchrel').AsInteger;
  }
 // tbl_target_client.FieldByName('gc_system').AsString := trim(Uppercase(QClientSource.FieldByName('gc_system').AsString));


 // temp := '';

  with QClientSource do
  begin
    for i := 0 to fieldcount - 1 do
    begin
      temp := '';
      temp := trim(LowerCase(fields[i].fieldname));
      if (temp = 'gc_phone') or (temp = 'gc_fax') or (temp = 'gc_cell') then
      begin // STRIP ALL PHONE NUMBERS OF NON NUMERIC CHARACTERS
        tbl_target_client.fieldbyname(fields[i].fieldname).AsString :=
          StripIt(trim(UpperCase(fields[i].AsString)));
      end
      else
      begin
        try
          case fields[i].DataType of
            ftString:
              begin // format all strings:
                tbl_target_client.fieldbyname(fields[i].fieldname).AsString := trim(UpperCase(fields[i].AsString));
              end
          else
            begin
              tbl_target_client.fieldbyname(fields[i].fieldname).value := fields[i].value;
            end;
          end; // case
        except on e: exception do
            stradd(result, ' client FOR LOOP:  ' + e.message);
        end;
      end; // else
    end;
  end; // with do

  try
    tbl_target_client.Post;
  except on e: exception do
      stradd(result, ' client_post --> ' + e.message);
  end; //try
  application.ProcessMessages;
end;






end.


  {

client sample:


procedure TForm1.Button3Click(Sender: TObject);
var temp: string;
ass: integer;
begin
  ass:= 0;
  temp := GetCurrent_DB_PARAMSTR(source);

  QSource.First;
  dbgrid1.DataSource := nil;
  dbGrid2.DataSource := nil;

  while not QSource.Eof do
  begin
    try
      inc(ass);
      label1.Caption := 'count: ' + IntTOStr(ass);
      application.ProcessMessages;
      if kill  then break;



      tblTarget.Insert;

    tblTarget.FieldByName('gc_key').AsInteger := 0;

    if Trim(qsource.FieldByName('clemail').AsString) > '!' then
      tblTarget.FieldByName('gc_uid').AsString := Uppercase(Trim(qsource.FieldByName('clemail').AsString))
    else
      tblTarget.FieldByName('gc_uid').AsString := GetGUidString();

    tblTarget.FieldByName('gc_lastname').AsString := Uppercase(Trim(qsource.FieldByName('clvlast').AsString));
    tblTarget.FieldByName('gc_firstname').AsString := Uppercase(Trim(qsource.FieldByName('clvfirst').AsString));
    tblTarget.FieldByName('gc_initial').AsString := Uppercase(Trim(qsource.FieldByName('clvinit').AsString));
    tblTarget.FieldByName('gc_surname').AsString := Uppercase(Trim(qsource.FieldByName('clvsurname').AsString));
    tblTarget.FieldByName('gc_addr1').AsString := Uppercase(Trim(qsource.FieldByName('clvaddr1').AsString));
    tblTarget.FieldByName('gc_addr2').AsString := Uppercase(Trim(qsource.FieldByName('clvaddr2').AsString));
    tblTarget.FieldByName('gc_addr3').AsString := '';
    tblTarget.FieldByName('gc_prov').AsString := Uppercase(Trim(qsource.FieldByName('clvprov').AsString));
    tblTarget.FieldByName('gc_city').AsString:= Uppercase(Trim(qsource.FieldByName('clvcity').AsString));
    tblTarget.FieldByName('gc_postcode').AsString := Uppercase(Trim(qsource.FieldByName('clvpostcode').AsString));
    tblTarget.FieldByName('gc_ctry').AsString := Uppercase(Trim(qsource.FieldByName('clvctry').AsString));
    tblTarget.FieldByName('gc_phone').AsString := Uppercase(Trim(qsource.FieldByName('clvphone').AsString));
    tblTarget.FieldByName('gc_fax').AsString := Uppercase(Trim(qsource.FieldByName('clfax').AsString));
    tblTarget.FieldByName('gc_cell').AsString := '';
    tblTarget.FieldByName('gc_email').AsString := Uppercase(Trim(qsource.FieldByName('clemail').AsString));
    tblTarget.FieldByName('gc_password').AsString := '**PASSW**';

    tblTarget.FieldByName('gc_system').AsString := Uppercase(Trim(temp));
   // tblTarget.FieldByName('gc_system').AsString := Uppercase(Trim('ARCH'));

    tblTarget.FieldByName('gc_voucher').AsInteger := qsource.FieldByName('vcnumb').AsInteger;
    tblTarget.FieldByName('gc_release').AsInteger := qsource.FieldByName('vchrel').AsInteger;
    tblTarget.FieldByName('gc_iata').AsString := qsource.FieldByName('vciata').AsString;

      tblTarget.Post;

     except on e:exception do
     begin
       memo1.Lines.Add(qsource.FieldByName('vcnumb').AsString+'-'+ qsource.FieldByName('vchrel').AsString
       + ' --> '+ e.message);
       tblTarget.Cancel;
     end;
     end;


    qsource.Next;
  end;
}

