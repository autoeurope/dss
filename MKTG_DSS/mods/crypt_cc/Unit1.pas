unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, IfxCustomDataSet, IfxQuery,
  IfxConnection, apicryptunit1, jmclasses;

type
  TForm1 = class(TForm)
    conn: TIfxConnection;
    Q: TIfxQuery;
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Memo1: TMemo;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    QRpBiz: TIfxQuery;
    DataSource2: TDataSource;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Edit2: TEdit;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
  private
    function StripCC(Instr: string): string;
    procedure vouchandqueue;
    procedure RPBIZ();
    procedure undo();
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

  kill: boolean;

implementation

{$R *.dfm}


function TForm1.StripCC(Instr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);
                 // '0'..'9': Result := True;
    for i := 1 to x do
    begin
      if IsDigit(Instr[i]) then temp := temp + InStr[i]
      else continue;
    end;
  end;
  Result := temp;
end;



procedure TForm1.vouchandqueue;
var i: integer;
s: String;
begin
 // if Q.FieldByName('vccrcard').asString > '!' then
 // begin
    i := 0;
    i := pos('BNK TRNS', Q.FieldByName('cryptcc').asString);

    if i > 0 then
    begin
       try
        Q.Edit;
        Q.FieldbyName('cryptcc').asstring := trim(uppercase(Q.FieldByName('vccrcard').asString));
        Q.Post;
      except on e: exception do
        begin
          memo1.lines.add('voucher: ' + Q.FieldbyName('vcnumb').asstring + '-' +
            Q.FieldbyName('vchrel').asstring + '-' + Q.FieldbyName('basesys').asstring + 'BNK E: ' + e.message);

         // q.Cancel;
        end;
      end;
    end
    else
    begin

      try
        s:='';
        s:= StripCC(trim(Uppercase(Q.FieldByName('vccrcard').asString)));
        if trim(s) = '' then  s:= trim(Uppercase(Q.FieldByName('vccrcard').asString));

        Q.Edit;
        Q.FieldbyName('cryptcc').asstring := API_Encrypt(true, s);
        Q.Post;
      except on e: exception do
        begin
          memo1.lines.add('voucher: ' + Q.FieldbyName('vcnumb').asstring + '-' +
            Q.FieldbyName('vchrel').asstring + '-' + Q.FieldbyName('basesys').asstring + '2 E: ' + e.message);

         // q.Cancel;
        end;
      end;
    end;

 // end;
end;

procedure TForm1.undo();
begin

  if Q.FieldByName('cryptcc').asString > '!' then
  begin
    try
      Q.Edit;
      //Q.FieldbyName('cryptcc').asstring := API_Encrypt(true, trim(Q.FieldByName('vccrcard').asString) );
      Q.FieldByName('vccrcard').asString := API_Decrypt(true, Q.FieldByName('cryptcc').asString);


      Q.Post;
    except on e: exception do
      begin
        memo1.lines.add('voucher: ' + Q.FieldbyName('vcnumb').asstring + '-' +
          Q.FieldbyName('vchrel').asstring + '-' + Q.FieldbyName('basesys').asstring + ' E: ' + e.message);

        q.Cancel;
      end;
    end;
  end;
end;



procedure TForm1.RPBIZ();
begin
 // if trim(QRPBIZ.FieldbyName('cryptcc').asstring) > '!' then exit;
  exit;
 // if QRPBIZ.FieldByName('ccnum').asString > '!' then
 // begin
  try
    QRPBIZ.Edit;
    QRPBIZ.FieldbyName('cryptcc').asstring := API_Encrypt(true, trim(QRPBIZ.FieldByName('ccnum').asString));
    QRPBIZ.Post;
  except on e: exception do
    begin
      memo1.lines.add('voucher: ' + QRPBIZ.FieldbyName('voucher').asstring + '-' +
        QRPBIZ.FieldbyName('release').asstring + ' E: ' + e.message);

       // QRPBIZ.Cancel;
    end;
  end;
 // end;
end;




procedure TForm1.Button10Click(Sender: TObject);
begin
  showmessage(StripCC(edit2.Text));
end;

procedure TForm1.Button11Click(Sender: TObject);
begin
memo1.Lines.add( API_Decrypt(true, edit2.text) );
end;

procedure TForm1.Button12Click(Sender: TObject);
begin
memo1.Lines.add( API_encrypt(true, edit2.text) );
end;

procedure TForm1.Button1Click(Sender: TObject);
var theCount: integer;
begin
  kill := false;

  Q.First;
  thecount := 0;

  dbgrid1.DataSource := nil;
  while not Q.Eof do
  begin
    if kill then break;
    inc(thecount);
    label1.Caption := 'count: ' + IntToStr(theCount);
    application.ProcessMessages;
    vouchandqueue;
   //  undo();
    ///if thecount > 100 then break;
    q.Next;
  end;
  dbgrid1.DataSource := datasource1;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  kill := true;
  application.ProcessMessages;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin // encrypt
// showmessage('stubba');
//  vouchandqueue;
 //   memo1.Lines.add(API_Encrypt(true, edit1.text));

  memo1.lines.add(API_Encrypt(true, trim(Q.FieldByName('vccrcard').asString)));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin // Q.decrypt button
 // showmessage('stubba');
  memo1.Lines.add(API_Decrypt(true, {edit1.text} Q.FieldByName('cryptcc').asString));
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  try
    dbgrid1.DataSource := datasource1;
    conn.Connected := true;
    Q.Active := true;
  except on e: Exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  try
    if not conn.Connected then conn.Connected := true;
    dbgrid1.DataSource := datasource2;

    QRPBiz.Active := true;
  except on e: Exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
var theCount: integer;
begin
  kill := false;
  QRPBIZ.First;
  thecount := 0;

  dbgrid1.DataSource := nil;
  while not QRPBIZ.Eof do
  begin
    if kill then break;
    inc(thecount);
    label1.Caption := 'count: ' + IntToStr(theCount);
    application.ProcessMessages;
   // vouchandqueue;
    RPBIZ();
    QRPBIZ.Next;
  end;
  dbgrid1.DataSource := datasource2;

end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  RPBIZ();
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
  memo1.Lines.add(API_DEcrypt(true, QRPBIZ.FieldByName('cryptcc').asString));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  kill := false;
end;

end.

