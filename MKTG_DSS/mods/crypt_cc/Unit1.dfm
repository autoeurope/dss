object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 583
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    505
    583)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 152
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Button1: TButton
    Left = 8
    Top = 42
    Width = 58
    Height = 25
    Caption = 'go'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 73
    Width = 58
    Height = 25
    Caption = 'kill'
    TabOrder = 1
    OnClick = Button2Click
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 120
    Width = 489
    Height = 104
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Memo1: TMemo
    Left = 8
    Top = 230
    Width = 489
    Height = 130
    Anchors = [akLeft, akTop, akRight]
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
  end
  object Button3: TButton
    Left = 128
    Top = 27
    Width = 55
    Height = 25
    Caption = 'encrypt'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 128
    Top = 58
    Width = 55
    Height = 25
    Caption = 'decrypt'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 8
    Top = 8
    Width = 58
    Height = 25
    Caption = 'open'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 336
    Top = 8
    Width = 89
    Height = 25
    Caption = 'RPbiz OPEN'
    TabOrder = 7
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 336
    Top = 39
    Width = 89
    Height = 25
    Caption = 'rp Biz Go'
    TabOrder = 8
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 336
    Top = 70
    Width = 89
    Height = 25
    Caption = 'encrypt RPBIZ'
    TabOrder = 9
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 431
    Top = 70
    Width = 66
    Height = 25
    Caption = 'DEC RPBIZ'
    TabOrder = 10
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 128
    Top = 89
    Width = 55
    Height = 25
    Caption = 'strip'
    TabOrder = 11
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 64
    Top = 392
    Width = 99
    Height = 25
    Caption = 'Manual Decrypt'
    TabOrder = 12
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 64
    Top = 423
    Width = 99
    Height = 25
    Caption = 'Manual Encrypt'
    TabOrder = 13
    OnClick = Button12Click
  end
  object Edit2: TEdit
    Left = 169
    Top = 396
    Width = 328
    Height = 21
    TabOrder = 14
    Text = 'Edit2'
  end
  object conn: TIfxConnection
    DesignConnection = True
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    Left = 88
    Top = 8
  end
  object Q: TIfxQuery
    Connection = conn
    SQL.Strings = (
      
        'Select basesys, vcnumb, vchrel, cryptcc, vccrcard From vouchandq' +
        'ueue'
      'where (length(trim(vccrcard)) > 0 and length(trim(cryptcc)) = 0)')
    Left = 88
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = Q
    Left = 88
    Top = 72
  end
  object QRpBiz: TIfxQuery
    Connection = conn
    SQL.Strings = (
      
        'select lname, fname, ccnum, cryptcc, voucher, release from repea' +
        'tbusiness'
      
        'where length(ccnum) > 0 and (cryptcc is null OR length(cryptcc)=' +
        '0 )')
    Left = 288
    Top = 24
  end
  object DataSource2: TDataSource
    DataSet = QRpBiz
    Left = 288
    Top = 56
  end
end
