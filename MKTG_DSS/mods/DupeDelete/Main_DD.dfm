object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 334
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    703
    334)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 290
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 39
    Width = 313
    Height = 120
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 165
    Width = 687
    Height = 132
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 151
    Top = 8
    Width = 128
    Height = 25
    Caption = 'delete g_client Dupes'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 327
    Top = 39
    Width = 368
    Height = 120
    Anchors = [akLeft, akTop, akRight]
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
  end
  object Button2: TButton
    Left = 8
    Top = 8
    Width = 43
    Height = 25
    Caption = 'kill'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 303
    Width = 75
    Height = 25
    Caption = 'Open VandQ'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 89
    Top = 303
    Width = 112
    Height = 25
    Caption = 'delete VandQ Dupes'
    TabOrder = 6
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 64
    Top = 8
    Width = 81
    Height = 25
    Caption = 'open g_client'
    TabOrder = 7
    OnClick = Button5Click
  end
  object btnOpenGCB: TButton
    Left = 384
    Top = 8
    Width = 121
    Height = 25
    Caption = 'OpenGC BatchUp'
    TabOrder = 8
    OnClick = btnOpenGCBClick
  end
  object Button6: TButton
    Left = 511
    Top = 8
    Width = 130
    Height = 25
    Caption = 'update GC_BatchDates'
    TabOrder = 9
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 511
    Top = 301
    Width = 105
    Height = 25
    Caption = 'strip Phone'
    TabOrder = 10
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 430
    Top = 303
    Width = 75
    Height = 25
    Caption = 'open'
    TabOrder = 11
    OnClick = Button8Click
  end
  object IfxConnection1: TIfxConnection
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    AfterConnect = IfxConnection1AfterConnect
    Left = 216
    Top = 88
  end
  object QGCMaster: TIfxQuery
    Connection = IfxConnection1
    SQL.Strings = (
      'Select gc_system, gc_voucher, gc_release, count(*) From g_client'
      'group by 1,2,3 having count(*) >= 2')
    Left = 104
    Top = 48
  end
  object DataSource1: TDataSource
    DataSet = QGCMaster
    Left = 368
    Top = 232
  end
  object QGCSlave: TIfxQuery
    Connection = IfxConnection1
    DataSource = DataSource1
    RequestLive = True
    SQL.Strings = (
      'Select * From g_client'
      'where gc_system =:gc_system and gc_voucher = :gc_voucher and'
      'gc_release = :gc_release')
    Left = 144
    Top = 48
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'gc_system'
        ParamType = ptInput
        Size = 5
      end
      item
        DataType = ftInteger
        Name = 'gc_voucher'
        ParamType = ptInput
        Size = 4
      end
      item
        DataType = ftInteger
        Name = 'gc_release'
        ParamType = ptInput
        Size = 4
      end>
  end
  object DataSource2: TDataSource
    DataSet = QGCSlave
    Left = 472
    Top = 168
  end
  object QVQMaster: TIfxQuery
    Connection = IfxConnection1
    SQL.Strings = (
      'select basesys, vcnumb, vchrel, count(*) thecount '
      'from vouchandqueue'
      'group by 1,2,3 having count(*) > 1')
    Left = 24
    Top = 264
  end
  object QVQSlave: TIfxQuery
    Connection = IfxConnection1
    DataSource = DataSource1
    RequestLive = True
    SQL.Strings = (
      'select * from vouchandqueue'
      'where basesys =:basesys and'
      'vcnumb =:vcnumb and'
      'vchrel =:vchrel')
    Left = 56
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vcnumb'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vchrel'
        ParamType = ptInput
      end>
  end
  object QmasterUpBatch: TIfxQuery
    Connection = IfxConnection1
    SQL.Strings = (
      'Select * From g_client'
      'where gc_batchdt is null'
      '')
    Left = 424
    Top = 64
  end
  object QmasterUpBatchSlave: TIfxQuery
    Connection = IfxConnection1
    DataSource = DataSource1
    SQL.Strings = (
      'Select basesys, vcnumb, vchrel, vchpqbath from vouchandqueue'
      'where basesys =:gc_system and '
      'vcnumb=:gc_voucher and vchrel=:gc_release')
    Left = 464
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'gc_system'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'gc_voucher'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'gc_release'
        ParamType = ptInput
      end>
  end
  object qStripPhone: TIfxQuery
    Connection = IfxConnection1
    SQL.Strings = (
      '--Select * From g_client'
      '--where gc_phone is not null and '
      '--length(trim(gc_phone)) > 0'
      ''
      'Select * From g_client'
      'where gc_phone is not null and '
      'length(trim(gc_phone)) > 0'
      'and ('
      
        '( gc_phone[1] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[1] > '#39'!'#39' ) )OR'
      
        '( gc_phone[2] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[2] > '#39'!'#39' ) )OR'
      
        '( gc_phone[3] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[3] > '#39'!'#39' ) )OR'
      
        '( gc_phone[4] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[4] > '#39'!'#39' ) )OR'
      
        '( gc_phone[5] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[5] > '#39'!'#39' ) )OR'
      
        '( gc_phone[6] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[6] > '#39'!'#39' ) )OR'
      
        '( gc_phone[7] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[7] > '#39'!'#39' ) )OR'
      
        '( gc_phone[8] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[8] > '#39'!'#39' ) )OR'
      
        '( gc_phone[9] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') an' +
        'd (gc_phone[9] > '#39'!'#39' ) )OR'
      
        '( gc_phone[10] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[10] > '#39'!'#39' ) )OR'
      
        '( gc_phone[11] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[11] > '#39'!'#39' ) )OR'
      
        '( gc_phone[12] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[12] > '#39'!'#39' ) )OR'
      
        '( gc_phone[13] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[13] > '#39'!'#39' ) )OR'
      
        '( gc_phone[14] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[14] > '#39'!'#39' ) )OR'
      
        '( gc_phone[15] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[15] > '#39'!'#39' ) )OR'
      
        '( gc_phone[16] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[16] > '#39'!'#39' ) )OR'
      
        '( gc_phone[17] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[17] > '#39'!'#39' ) )OR'
      
        '( gc_phone[18] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[18] > '#39'!'#39' ) )OR'
      
        '( gc_phone[19] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[19] > '#39'!'#39' ) )OR'
      
        '( gc_phone[20] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[20] > '#39'!'#39' ) )OR'
      
        '( gc_phone[21] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[21] > '#39'!'#39' ) )OR'
      
        '( gc_phone[22] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[22] > '#39'!'#39' ) )OR'
      
        '( gc_phone[23] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[23] > '#39'!'#39' ) )OR'
      
        '( gc_phone[24] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[24] > '#39'!'#39' ) )OR'
      
        '( gc_phone[25] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[25] > '#39'!'#39' ) )OR'
      
        '( gc_phone[26] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[26] > '#39'!'#39' ) )OR'
      
        '( gc_phone[27] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[27] > '#39'!'#39' ) )OR'
      
        '( gc_phone[28] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[28] > '#39'!'#39' ) )OR'
      
        '( gc_phone[29] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[29] > '#39'!'#39' ) )OR'
      
        '( gc_phone[30] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[30] > '#39'!'#39' ) )OR'
      
        '( gc_phone[31] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[31] > '#39'!'#39' ) )OR'
      
        '( gc_phone[32] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[32] > '#39'!'#39' ) )OR'
      
        '( gc_phone[33] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[33] > '#39'!'#39' ) )OR'
      
        '( gc_phone[34] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[34] > '#39'!'#39' ) )OR'
      
        '( gc_phone[35] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[35] > '#39'!'#39' ) )OR'
      
        '( gc_phone[36] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[36] > '#39'!'#39' ) )OR'
      
        '( gc_phone[37] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[37] > '#39'!'#39' ) )OR'
      
        '( gc_phone[38] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[38] > '#39'!'#39' ) )OR'
      
        '( gc_phone[39] not in('#39'1'#39','#39'2'#39','#39'3'#39','#39'4'#39','#39'5'#39','#39'6'#39','#39'7'#39','#39'8'#39','#39'9'#39','#39'0'#39') a' +
        'nd (gc_phone[39] > '#39'!'#39' ) )'
      ''
      ')')
    Left = 408
    Top = 232
  end
end
