object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Copy Client to G_Client'
  ClientHeight = 492
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 112
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Button1: TButton
    Left = 56
    Top = 8
    Width = 49
    Height = 25
    Caption = 'open'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 167
    Width = 433
    Height = 195
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Memo1: TMemo
    Left = 111
    Top = 8
    Width = 330
    Height = 153
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button2: TButton
    Left = 56
    Top = 39
    Width = 49
    Height = 25
    Caption = 'kill'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 56
    Top = 70
    Width = 49
    Height = 25
    Caption = 'go'
    TabOrder = 4
    OnClick = Button3Click
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 368
    Width = 433
    Height = 120
    DataSource = DataSource2
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object MemoSQL: TMemo
    Left = 447
    Top = 8
    Width = 513
    Height = 480
    Lines.Strings = (
      'select  '
      #9'clvlast,'
      '    '#9'clvfirst,'
      '    '#9'clvinit,'
      '    '#9'clvsurname,'
      '        clvaddr1,'
      '        clvaddr2,    '#9
      '    '#9'clvprov,    '
      '    '#9'clvcity,'
      '    '#9'clvpostcode,    '#9
      '        clvctry,    '#9
      '        clvphone,'
      '    '#9'clfax,      '
      '    '#9'clemail,    '#9
      '    '#9'"TEST" system,'
      '    '#9' vcnumb,'
      #9' vchrel,'
      #9' vciata, vchpqbath'
      'from vouch, vchpqueue, vclient'
      'where vcntype = "A"  and'
      '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ('
      
        '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> '#39'N'#39 +
        ') and'
      
        '(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> '#39'N'#39 +
        ')) and'
      'vcclientid = clvchnum and'
      'VCNUMB = VCHPQVCH and'
      'VCHREL = VCHPQVCHLN ')
    TabOrder = 6
  end
  object source: TIfxConnection
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=vouchers_pre2003@online_f50aus')
    Left = 16
    Top = 8
  end
  object target: TIfxConnection
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    Left = 248
    Top = 8
  end
  object QSource: TIfxQuery
    Connection = source
    SQL.Strings = (
      'select  '
      #9'clvlast gc_lastname,'
      '    '#9'clvfirst gc_firstname,'
      '    '#9'clvinit gc_initial,'
      '    '#9'clvsurname gc_surname,'
      '        clvaddr1 gc_addr1,'
      '        clvaddr2 gc_addr2,    '#9
      '    '#9'clvprov gc_prov,    '
      '    '#9'clvcity gc_city,'
      '    '#9'clvpostcode gc_clvpostcode,    '#9
      '        clvctry gc_clvctry,    '#9
      '        clvphone gc_phone,'
      '    '#9'clfax gc_fax,      '
      '    '#9'clemail gc_email,    '#9
      '    '#9'"TEST" gc_system,'
      '    '#9' clvchnum gc_voucher'
      'from vclient'
      ''
      'where'
      '   (length(trim(clvlast))  > 0 and clvlast is not null)'#9'and'
      '   (length(trim(clvfirst)) > 0 and clvfirst is not null) and'
      '   (length(trim(clvaddr1)) > 0 and clvaddr1 is not null) and'
      '   (length(trim(clvprov))  > 0 and clvprov is not null) and'
      '   (length(trim(clvcity))  > 0 and clvcity is not null) and'
      
        '   (length(trim(clvpostcode))  > 0 and clvpostcode is not null) ' +
        'and'
      '   (length(trim(clvctry))  > 0 and clvctry is not null) and'
      '   (length(trim(clemail)) > 0 and clemail is not null) ')
    Left = 16
    Top = 40
  end
  object tblTarget: TIfxTable
    Connection = target
    TableName = 'g_client'
    Left = 248
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = QSource
    Left = 16
    Top = 72
  end
  object DataSource2: TDataSource
    DataSet = tblTarget
    Left = 248
    Top = 72
  end
  object DSCalc1: TDSCalc
    Left = 112
    Top = 232
  end
end
