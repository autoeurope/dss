unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, IfxTable, IfxCustomDataSet, IfxQuery,
  IfxConnection, aecommonfunc, jmclasses, DSCalc
  ;

type
  TForm1 = class(TForm)
    source: TIfxConnection;
    target: TIfxConnection;
    QSource: TIfxQuery;
    tblTarget: TIfxTable;
    Button1: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    DBGrid2: TDBGrid;
    DataSource2: TDataSource;
    MemoSQL: TMemo;
    Label1: TLabel;
    DSCalc1: TDSCalc;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    kill: boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;



implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  try
    kill := false;
    memo1.Clear;
    source.Connected := true;
    Qsource.SQL.Clear;
    QSource.SQL.Text := memoSQL.text;
    memo1.Text := QSource.SQL.Text;

    QSource.Open;
    tblTarget.open;
  except on e:exception do
  begin
    showmessage(e.message);
  end;
  end;
   showmessage('database: ' + GetCurrentDB(source) + ' :param: ' + GetCurrent_DB_PARAMSTR(source) );
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
 kill := true;
 application.ProcessMessages;
end;

procedure TForm1.Button3Click(Sender: TObject);
var temp: string;
ass: integer;
begin
  ass:= 0;
  temp := GetCurrent_DB_PARAMSTR(source);

  QSource.First;

  dbgrid1.DataSource := nil;
  dbGrid2.DataSource := nil;

  while not QSource.Eof do
  begin
    try
      inc(ass);
      label1.Caption := 'count: ' + IntTOStr(ass);
      application.ProcessMessages;
      if kill  then break;

      

      tblTarget.Insert;

     {   drop table g_client;

create table "informix".g_client
  (
    gc_key serial not null ,
    gc_uid varchar(200) not null ,
    gc_lastname char(60) not null ,
    gc_firstname varchar(60) not null ,
    gc_initial char(2),
    gc_surname char(8),
    gc_addr1 varchar(60) not null ,
    gc_addr2 varchar(60),
    gc_addr3 varchar(60),
    gc_prov char(10) not null ,
    gc_city varchar(50) not null ,
    gc_postcode char(20) not null ,
    gc_ctry char(4) not null ,
    gc_phone char(40),
    gc_fax char(40),
    gc_cell char(40),
    gc_email varchar(200),
    gc_password varchar(60),
    gc_system char(4),
    gc_voucher integer,
    gc_release integer not null ,
    gc_iata char(9) not null
  )lock mode row;
revoke all on "informix".g_client from "public";
      }

    tblTarget.FieldByName('gc_key').AsInteger := 0;

    if Trim(qsource.FieldByName('clemail').AsString) > '!' then
      tblTarget.FieldByName('gc_uid').AsString := Uppercase(Trim(qsource.FieldByName('clemail').AsString))
    else
      tblTarget.FieldByName('gc_uid').AsString := GetGUidString();

    tblTarget.FieldByName('gc_lastname').AsString := Uppercase(Trim(qsource.FieldByName('clvlast').AsString));
    tblTarget.FieldByName('gc_firstname').AsString := Uppercase(Trim(qsource.FieldByName('clvfirst').AsString));
    tblTarget.FieldByName('gc_initial').AsString := Uppercase(Trim(qsource.FieldByName('clvinit').AsString));
    tblTarget.FieldByName('gc_surname').AsString := Uppercase(Trim(qsource.FieldByName('clvsurname').AsString));
    tblTarget.FieldByName('gc_addr1').AsString := Uppercase(Trim(qsource.FieldByName('clvaddr1').AsString));
    tblTarget.FieldByName('gc_addr2').AsString := Uppercase(Trim(qsource.FieldByName('clvaddr2').AsString));
    tblTarget.FieldByName('gc_addr3').AsString := '';
    tblTarget.FieldByName('gc_prov').AsString := Uppercase(Trim(qsource.FieldByName('clvprov').AsString));
    tblTarget.FieldByName('gc_city').AsString:= Uppercase(Trim(qsource.FieldByName('clvcity').AsString));
    tblTarget.FieldByName('gc_postcode').AsString := Uppercase(Trim(qsource.FieldByName('clvpostcode').AsString));
    tblTarget.FieldByName('gc_ctry').AsString := Uppercase(Trim(qsource.FieldByName('clvctry').AsString));
    tblTarget.FieldByName('gc_phone').AsString := Uppercase(Trim(qsource.FieldByName('clvphone').AsString));
    tblTarget.FieldByName('gc_fax').AsString := Uppercase(Trim(qsource.FieldByName('clfax').AsString));
    tblTarget.FieldByName('gc_cell').AsString := '';
    tblTarget.FieldByName('gc_email').AsString := Uppercase(Trim(qsource.FieldByName('clemail').AsString));
    tblTarget.FieldByName('gc_password').AsString := '**PASSW**';

    tblTarget.FieldByName('gc_system').AsString := Uppercase(Trim(temp));
   // tblTarget.FieldByName('gc_system').AsString := Uppercase(Trim('ARCH'));

    tblTarget.FieldByName('gc_voucher').AsInteger := qsource.FieldByName('vcnumb').AsInteger;
    tblTarget.FieldByName('gc_release').AsInteger := qsource.FieldByName('vchrel').AsInteger;
    tblTarget.FieldByName('gc_iata').AsString := qsource.FieldByName('vciata').AsString;

      {
    select
	clvlast,
    	clvfirst,
    	clvinit,
    	clvsurname,
        clvaddr1,
        clvaddr2,    	
    	clvprov,    
    	clvcity,
    	clvpostcode,    	
        clvctry,    	
        clvphone,
    	clfax,      
    	clemail,    	
    	"TEST" system,
    	 vcnumb,
	 vchrel,
	 vciata, vchpqbath
from vouch, vchpqueue, vclient
where vcntype = "A"  and
-- VCHPQBATH BETWEEN Date("12/31/1998") and Date("12/31/2002") and
(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and (
(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N')) and
vcclientid = clvchnum and
VCNUMB = VCHPQVCH and
VCHREL = VCHPQVCHLN












      }




      tblTarget.Post;

     except on e:exception do
     begin
       memo1.Lines.Add(qsource.FieldByName('vcnumb').AsString+'-'+ qsource.FieldByName('vchrel').AsString
       + ' --> '+ e.message);
       tblTarget.Cancel;
     end;
     end;


    qsource.Next;
  end;

  dbgrid1.DataSource := DataSource1;
  dbGrid2.DataSource := DataSource2;
end;

end.
