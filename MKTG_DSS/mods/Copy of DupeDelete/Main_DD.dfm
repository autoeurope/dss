object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 346
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    703
    346)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 216
    Top = 8
    Width = 273
    Height = 13
    Caption = 'Label1'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 39
    Width = 313
    Height = 120
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 165
    Width = 687
    Height = 132
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 57
    Top = 8
    Width = 119
    Height = 25
    Caption = 'delete g_client Dupes'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 327
    Top = 39
    Width = 368
    Height = 120
    Anchors = [akLeft, akTop, akRight]
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    ExplicitWidth = 350
  end
  object Button2: TButton
    Left = 8
    Top = 8
    Width = 43
    Height = 25
    Caption = 'kill'
    TabOrder = 4
    OnClick = Button2Click
  end
  object IfxConnection1: TIfxConnection
    Connected = True
    Params.Strings = (
      'INFORMIXSERVER=online_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@online_f50aus')
    Left = 400
    Top = 80
  end
  object QGCMaster: TIfxQuery
    Active = True
    Connection = IfxConnection1
    SQL.Strings = (
      'Select gc_system, gc_voucher, gc_release, count(*) From g_client'
      'group by 1,2,3 having count(*) >= 2')
    Left = 432
    Top = 80
  end
  object DataSource1: TDataSource
    DataSet = QGCMaster
    Left = 432
    Top = 112
  end
  object QGCSlave: TIfxQuery
    Active = True
    Connection = IfxConnection1
    DataSource = DataSource1
    RequestLive = True
    SQL.Strings = (
      'Select * From g_client'
      'where gc_system =:gc_system and gc_voucher = :gc_voucher and'
      'gc_release = :gc_release')
    Left = 472
    Top = 80
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'gc_system'
        ParamType = ptInput
        Size = 5
      end
      item
        DataType = ftInteger
        Name = 'gc_voucher'
        ParamType = ptInput
        Size = 4
      end
      item
        DataType = ftInteger
        Name = 'gc_release'
        ParamType = ptInput
        Size = 4
      end>
  end
  object DataSource2: TDataSource
    DataSet = QGCSlave
    Left = 472
    Top = 112
  end
end
