// find dupes in vouchandqueue

select basesys, vcnumb, vchrel, count(*) thecount 
from vouchandqueue
group by 1,2,3 having count(*) > 1
------------------------------------
output

select * from vouchandqueue
where basesys = "USA" and

(vcnumb = 336947 and vchrel = 3) OR
(vcnumb = 361633 and vchrel = 3) OR
(vcnumb = 367120 and vchrel = 7) OR
(vcnumb = 395964 and vchrel = 1) OR
(vcnumb = 775323 and vchrel = 5) OR
(vcnumb = 786085 and vchrel = 3) OR
(vcnumb = 795289 and vchrel = 3) OR
(vcnumb = 804657 and vchrel = 3) OR
(vcnumb = 839625 and vchrel = 1) OR
(vcnumb = 846993 and vchrel = 5) OR
(vcnumb = 855954 and vchrel = 1) OR
(vcnumb = 857289 and vchrel = 1) OR
(vcnumb = 863992 and vchrel = 3) OR
(vcnumb = 867631 and vchrel = 3) OR
(vcnumb = 875521 and vchrel = 1) OR
(vcnumb = 876145 and vchrel = 1) OR
(vcnumb = 878382 and vchrel = 1) OR
(vcnumb = 1216251 and vchrel = 1) OR
(vcnumb = 1294212 and vchrel = 1) OR
(vcnumb = 1355338 and vchrel = 1) OR
(vcnumb = 1853256 and vchrel = 1) OR
(vcnumb = 1891875 and vchrel = 1) OR
(vcnumb = 1949592 and vchrel = 1) OR
(vcnumb = 1955355 and vchrel = 1) OR
(vcnumb = 1958353 and vchrel = 1) OR
(vcnumb = 1968088 and vchrel = 1) OR
(vcnumb = 2005691 and vchrel = 1) OR
(vcnumb = 2016052 and vchrel = 1) OR
(vcnumb = 2101882 and vchrel = 1) OR
(vcnumb = 2126226 and vchrel = 1) OR
(vcnumb = 2149129 and vchrel = 1) OR
(vcnumb = 2153590 and vchrel = 1) OR
(vcnumb = 2153594 and vchrel = 1) OR
(vcnumb = 2157575 and vchrel = 1) OR
(vcnumb = 2160825 and vchrel = 1) OR
(vcnumb = 2173018 and vchrel = 1) OR
(vcnumb = 2177147 and vchrel = 1) OR
(vcnumb = 2189146 and vchrel = 1) OR
(vcnumb = 2191013 and vchrel = 1) OR
(vcnumb = 2200918 and vchrel = 1) OR
(vcnumb = 2256599 and vchrel = 1) OR
(vcnumb = 2262517 and vchrel = 1) 

order by vcnumb, vchrel










//************************************************

possible index variations:

create unique index sysvchrel on vouchandqueue (basesys, vcnumb, vchrel);



// stats update by the book:

update statistics medium for table vouchandqueue distributions only; 
update statistics high for table vouchandqueue(basesys); 
update statistics high for table vouchandqueue(vcnumb);
update statistics high for table vouchandqueue(vchrel);



create index "informix".asdf1 on "informix".repeatbusiness (lname,
    fname,ccnum) using btree ;
create index "informix".bdt_idx on "informix".repeatbusiness (batchdate)
    using btree ;
create index "informix".name_idx on "informix".repeatbusiness
    (lname,fname) using btree ;


//*************************************************************************


-- drop un-unique and re-create unique index
drop index "informix".sysvchrel;

create unique index  "informix".sysvchrel on "informix".vouchandqueue
    (basesys,vcnumb,vchrel) using btree ;











