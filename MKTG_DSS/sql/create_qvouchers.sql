create table "informix".q_vouchers
  (
    basesys char(4) not null ,
    clientid varchar(100),
    cryptcc varchar(50),
    vcnumb integer not null ,
    vchrel integer not null ,
    voit1 smallint default 0,
    voit2 smallint default 0,
    voit3 smallint default 0,
    voit1ret decimal(10,2) default 0,
    voit2ret decimal(10,2) default 0,
    voit3ret decimal(10,2) default 0,
    voit4ret decimal(10,2) default 0,
    voit1whl decimal(10,2) default 0,
    vopsubt char(2),
    vopstype1 char(1),
    voit2whl decimal(10,2) default 0,
    voit3whl decimal(10,2) default 0,
    voit4whl decimal(10,2) default 0,
    voptax1 decimal(10,2)  default 0,
    voptax2 decimal(10,2)  default 0,
    vcntype char(1),
    vciata char(9),
    voptype char(3),
    vccrcardid smallint,
    vccrcard char(20),
    vcccmonth char(4),
    vcrid smallint,
    vccomtaken char(1),
    vccom1 decimal(10,2) default 0,
    vccom2 decimal(10,2) default 0,
    vcdisaddamount decimal(10,2) default 0,
    affiliate1 char(10),
    affiliate2 char(10),
    vodddtestat char(1),
    vopcurrency char(3),
    vopcurrate decimal(8,4),
    vcccauthcode char(10),
    vopid char(20),
    vopid2 char(20),
    vopctry1 char(2),
    vopctry2 char(2),
    vopprov1 char(5),
    vopprov2 char(5),
    vopcity1 char(20),
    vopcity2 char(20),
    voploc1 char(2),
    voploc2 char(2),
    voprd1 char(20),
    voiamt5 smallint default 0,
    vcfpover1 char(1),
    voxtp1 integer default 0,
    voxtp1w decimal(10,2) default 0,
    voxtp2w decimal(10,2) default 0,
    voxtp3w decimal(10,2) default 0,
    voxtp4w decimal(10,2) default 0,
    vopper1 decimal(10,2) default 0,
    vopper2 decimal(10,2) default 0,
    vchdate date,
    voptxper1 decimal(10,2) default 0,
    voptxper2 decimal(10,2) default 0,
    vccashdeposi money(10,2),
    vopdate1 date,
    vopdate2 date,
    voptime1 datetime hour to minute,
    voptime2 datetime hour to minute,
    voptime3 datetime hour to minute,
    vopper3 decimal(10,2) default 0,
    voit1sw char(1),
    vopcomt1 char(30),
    vcvpromo1 char(10),
    vcvpromo2 char(10),
    vcvpromo3 char(10),
    vccconfirm char(20),
    vcclientid integer,
    vcvendorattn char(18),
    vccomments varchar(100),
    vccomments1 varchar(100),
    vcspecrequest varchar(100),
    vcarrvair char(2),
    vcarrvflight char(10),
    vceta char(10),
    vcprepay char(2),
    vcresoffice char(1),
    vcst1 char(1),
    vcst2 char(1),
    vcst3 char(1),
    vcstdte1 date,
    vcstdte2 date,
    vcstdte3 date,
    vord1 decimal(10,2) default 0,
    vord2 decimal(10,2) default 0,
    vord3 decimal(10,2) default 0,
    vord4 decimal(10,2) default 0,
    vord5 decimal(10,2) default 0,
    vord6 decimal(10,2) default 0,
    vord7 decimal(10,2) default 0,
    vord8 decimal(10,2) default 0,
    vord9 decimal(10,2) default 0,
    vord10 decimal(10,2) default 0,
    vowd1 decimal(10,2)
        default 0,
    vowd2 decimal(10,2)
        default 0,
    vowd3 decimal(10,2)
        default 0,
    vowd4 decimal(10,2)
        default 0,
    vowd5 decimal(10,2)
        default 0,
    vopwd6 decimal(10,2)
        default 0,
    vopwd7 decimal(10,2)
        default 0,
    vopwd8 decimal(10,2)
        default 0,
    vopwd9 decimal(10,2)
        default 0,
    vopwd10 decimal(10,2)
        default 0,
    vopdatecreate date,
    vopdatemodif date,
    vopamt1 decimal(10,2)
        default 0,
    vopamt2 decimal(10,2)
        default 0,
    vopamt3 decimal(10,2)
        default 0,
    vopamt4 decimal(10,2)
        default 0,
    vopamt5 decimal(10,2)
        default 0,
    vopamt6 decimal(10,2)
        default 0,
    vopamt7 decimal(10,2)
        default 0,
    vopamt8 decimal(10,2)
        default 0,
    vopamt9 decimal(10,2)
        default 0,
    origcurr char(3),
    voiamt1 smallint
        default 0,
    voiamt2 smallint
        default 0,
    voiamt3 smallint
        default 0,
    voiamt4 smallint
        default 0,
    voprcode char(15),
    voprt1 char(1),
    voconsort char(10),
    voconsort2 char(10),
    voconsort3 char(10),
    vcconsortsub smallint,
    vcconsortsub2 smallint,
    vcconsortsub3 smallint,
    vorecord integer,
    voxtp1t char(1),
    voxtp2 integer
        default 0,
    voxtp2t char(1),
    vocusttype char(2),
    vccomover1 char(1),
    homecurr char(3),
    homectry char(2),
    altresnum1 char(10)

  );
revoke all on "informix".q_vouchers from "public";

create unique index "informix".qsysvchrel on "informix".q_vouchers
    (basesys,vcnumb,vchrel) using btree ;

create index "informix".qsyspickup on "informix".q_vouchers
    (basesys,vopdate1) using btree;

create index "informix".qsysvhcdate on "informix".q_vouchers
    (basesys,vchdate) using btree;




