drop table g_client;

create table g_client
  (
    gc_key serial not null ,
    gc_uid varchar(100),
    gc_lastname char(60),
    gc_firstname varchar(60),
    gc_initial char(2),
    gc_surname char(8),
    gc_addr1 varchar(60),
    gc_addr2 varchar(60),
    gc_prov char(10),
    gc_city varchar(50),
    gc_postcode char(20),
    gc_ctry char(4),
    gc_phone char(40),
    gc_fax char(40),
    gc_cell char(40),
    gc_email varchar(250),
    gc_password varchar(60),
    gc_system char(4),		-- below can be dropped later
    gc_voucher integer,
    gc_release integer,
    gc_iata char(9)
   	
  );
revoke all on "informix".g_client from "public";
