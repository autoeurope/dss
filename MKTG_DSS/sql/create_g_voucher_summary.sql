              size = 146 }
create table "informix".g_voucher_summary
  (
    vs_basesys char(4) not null ,
    vs_voucher integer not null ,
    vs_release integer not null ,
    vs_active_history char(1) not null ,
    vs_p_or_q char(1) not null ,
    vs_payment_type integer,
    vs_fp_pp char(2) not null ,
    vs_plus_basic char(1) not null ,
    vs_rid integer,
    vs_auth_code char(10),
    vs_op_confirmation char(20),
    vs_product_type char(10),
    vs_product_type_tag char(10),
    vs_business_source char(10),
    vs_operator_code varchar(20) not null ,
    vs_pu_ctry char(2) not null ,
    vs_do_ctry char(2) not null ,
    vs_pu_city varchar(50),
    vs_do_city varchar(50),
    vs_pu_loc char(2),
    vs_do_loc char(2),
    vs_voucher_create_dt date not null ,
    vs_paid_dt date not null ,
    vs_updated_dt date not null ,
    vs_pu_dt date not null ,
    vs_do_dt date not null ,
    vs_pu_time datetime year to second not null ,
    vs_do_time datetime year to second not null ,
    vs_days_between_create_pu decimal(10,2),
    vs_duration decimal(10,2)
        default 0.00,
    vs_sipp char(20),
    vs_op_rate_code char(15),
    vs_sipp_type varchar(30),
    vs_sipp_description varchar(100),
    vs_num_passengers integer,
    vs_comm_tot decimal(10,2)
        default 0.00,
    vs_comm_due decimal(10,2)
        default 0.00,
    vs_comm_percent decimal(10,2)
        default 0.00,
    vs_home_curr char(3),
    vs_currency_operator char(3),
    vs_exchange_rt decimal(8,4),
    vs_vat_tax_rt decimal(10,2),
    vs_subtype char(2),
    vs_home_ctry char(2),
    vs_base_rate_retail decimal(10,2)
        default 0.00,
    vs_discount_retail decimal(10,2)
        default 0.00,
    vs_insurance_tot_retail decimal(10,2)
        default 0.00,
    vs_tax_tot_retail decimal(10,2)
        default 0.00,
    vs_pu_fee_retail decimal(10,2)
        default 0.00,
    vs_do_fee_retail decimal(10,2)
        default 0.00,
    vs_xtra_fee_retail decimal(10,2)
        default 0.00,
    vs_base_rate_wholesale decimal(10,2)
        default 0.00,
    vs_insurance_tot_wholesale decimal(10,2)
        default 0.00,
    vs_tax_tot_wholesale decimal(10,2)
        default 0.00,
    vs_pu_fee_wholesale decimal(10,2)
        default 0.00,
    vs_do_fee_wholesale decimal(10,2)
        default 0.00,
    vs_retail_total decimal(10,2)
        default 0.00,
    vs_wholesale_total decimal(10,2)
        default 0.00,
    vs_operator_due decimal(10,2)
        default 0.00,
    vs_operator_due_curr decimal(10,2)
        default 0.00,
    vs_profit decimal(10,2)
        default 0.00,
    vs_deposit_amt decimal(10,2)
        default 0.00,
    vs_waived_amt decimal(10,2)
        default 0.00,
    vs_deposit_after_waive decimal(10,2)
        default 0.00,
    vs_due_at_pu decimal(10,2)
        default 0.00,
    vs_deferred_amt decimal(10,2)
        default 0.00,
    vs_gross_revenue decimal(10,2)
        default 0.00,
    vs_operator_cost decimal(10,2)
        default 0.00,
    vs_count_new integer,
    vs_count_cxl integer,
    vs_count_change integer,
    vs_count_voucher integer,
    vs_client_age integer,
    vs_iata char(9) not null ,
    vs_direct_or_ta char(2) not null ,
    vs_consortium char(10),
    vs_comments varchar(100),
    vs_insert_dt datetime year to day
        default current year to day,
    vs_update_dt datetime year to day
        default current year to day,
    vs_upl_to_listmgr_dt date,
    vs_key serial not null
  );
revoke all on "informix".g_voucher_summary from "public" as "informix";


create index "informix".idx_sysbatchctrycityoperetc on "informix"
    .g_voucher_summary (vs_basesys,vs_paid_dt,vs_pu_ctry,vs_pu_city,
    vs_operator_code,vs_direct_or_ta,vs_consortium,vs_active_history,
    vs_sipp) using btree ;
create unique index "informix".ix_avs_basesys_vouch on "informix"
    .g_voucher_summary (vs_basesys,vs_voucher,vs_release) using
    btree ;
create index "informix".ix_batchdate on "informix".g_voucher_summary
    (vs_paid_dt) using btree ;
