-- index creation for rpbus:

-- vouchandqueue:

create index "informix".batchctrycity on "informix".vouchandqueue(vchpqbath,vopctry1,vopcity1) using btree ;
create index "informix".batchdate on "informix".vouchandqueue(vchpqbath) using btree ;
create index "informix".batchiata on "informix".vouchandqueue(vchpqbath,basesys,vciata) using btree ;
create index "informix".batchoperctrycityloc on "informix".vouchandqueue(vchpqbath,vopid,vopctry1,vopcity1,voploc1) using btree ;
create index "informix".batchsyspickup on "informix".vouchandqueue(vchpqbath,basesys,vopdate1) using btree ;
create index "informix".batchsysvchrel on "informix".vouchandqueue(vchpqbath,basesys,vcnumb,vchrel) using btree ;
create index "informix".batchvcntype on "informix".vouchandqueue(vchpqbath,basesys,vcntype) using btree ;
create index "informix".clientid on "informix".vouchandqueue (clientid) using btree ;
create index "informix".clientidsysvchrel on "informix".vouchandqueue(clientid,basesys,vcnumb,vchrel) using btree ;
create unique index "informix".sysvchrel on "informix".vouchandqueue(basesys,vcnumb,vchrel) using btree ;
create index "informix".batchsysconsort on 
	"informix".vouchandqueue(vchpqbath,basesys,voconsort) using btree ;


-- batch, basesys, country, city



-- g_client:

create index "informix".idx_gc_key on "informix".g_client (gc_key) using btree;
create index "informix".idx_gc_uid on "informix".g_client (gc_uid) using btree;
create index "informix".idx_gc_email on "informix".g_client (gc_email) using btree;
create index "informix".idx_gc_sysemail on "informix".g_client (gc_system, gc_email) using btree;
create index "informix".idx_gc_lnamefname on "informix".g_client (gc_lastname, gc_firstname) using btree;
--create index "informix".idx_gc_sysvouchrel on "informix".g_client (gc_system, gc_voucher, gc_release) using btree;
create index "informix".idx_gc_lnamefnameemail on "informix".g_client (gc_lastname,gc_firstname, gc_email) using btree ;
create index "informix".idx_gc_iata on "informix".g_client (gc_iata) using btree;

--drop index "informix".idx_gc_sysvouchrel;

create unique index "informix".idx_gc_sysvouchrel 
	on "informix".g_client(gc_system,gc_voucher,gc_release) using btree ;





-- csfiles
-- cscomments
-- cscontact
-- indexes created on DSS 9/18/2007:

create unique index "informix".csf_sysresreltype 
	on "informix".csfiles (cs_basesys, cs_res, 
	cs_rel, cs_filetype) using btree;

create index "informix".csc_sysresreltype 
	on "informix".cscomments (cscmnt_basesys, cscmntresnum, 
	cscmtresrel,cscmntfiletype) using btree ;

create unique index "informix".cscontact_sysresreltype on 
	"informix".cscontact(csclt_basesys, csclt_vouch, 
		csclt_release, csclt_filetype) using btree ;


