9/18/2007: Customer Service Tables Archived:

sql to unload and copy data to data store:
modify dates for next run
modify constants to reflect appropriate system of origin.
after gui supports the dss, purdge corresponding data from source systems
~ to this point, the following systems have been copied to the DataStore thru
12/31/2003: USA, CANA, HOHO, KEM, EURO, UK

//****************************************************************************

-- original usa unload:

unload to cs_uk.unl (
Select "UK" cs_basesys, * From csfiles
where cs_daterecvd <= date("12/31/2003")
);

get more later example:
unload to cs_THEALIAS.unl (
Select "THE_ALIAS" cs_basesys, * From csfiles
where cs_daterecvd between date("01/01/2004") and date("12/31/2004")
);




//***************************************************************************



-- get cscomments where relevent to csfiles unloaded above:

unload to cscmnt_uk.unl (
select "UK" cscmnt_baseys, 
cscmtname,
cscmtdatefax, 
cscmtseq,            
cscmtdate,
cscmtridnum, 
cscmntresnum,
cscmtresrel, 
cscmtcomment,
cscmtfaxexists,
cscmtdocexists, 
cscmtdbtag,
cscmntfiletype,
cscmntfileextention  
from cscomments, csfiles where 
cs_daterecvd <= date("12/31/2003")
and cscmntresnum = cs_res
and cscmtresrel = cs_rel
);

//***************************************************************************

-- get cscontact where relevent to csfiles unloaded above:

unload to cscontact_uk.unl (
select "UK" csclt_basesys,
csclt_vouch, 
csclt_release, 
csclt_lastname, 
csclt_firstname, 
csclt_initial, 
csclt_addr1,
csclt_addr2, 
csclt_addr3,
csclt_city, 
csclt_zip,
csclt_ctry, 
csclt_email, 
csclt_faxnum,
csclt_phonenum, 
csclt_other2, 
csclt_other, 
csclt_prov, 
csclt_filetype 
from cscontact, csfiles
where 
cs_daterecvd <= date("12/31/2003")
and csclt_vouch = cs_res
and csclt_release = cs_rel
);

//****************************************************************************

-- csfilehistory:
-- ~ to be dopne later since file history did not start until 2006.

unload to csfilehistory_usa.unl (
select "USA" csfs_basesys,
csfs_csfilenum,
csfs_insertdtt, 
csfs_filestat,
csfs_who, 
batch_date, 
csfs_filetype, 
csfs_reason,
csfs_csduedate,
csfs_csref,
csfs_rqstd_refund
from csfilehistory, csfiles
where 
cs_daterecvd <= date("12/31/2002")
and CS_FILENUM = csfs_csfilenum
)


-- load
load from cscmnt_usa.unl insert into cscomments;
load from cs_usa.unl insert into csfiles;
load from cscontact_usa.unl insert into cscontact;

load from cscmnt_cana.unl insert into cscomments;
load from cs_cana.unl insert into csfiles;
load from cscontact_cana.unl insert into cscontact;

load from cscmnt_kem.unl insert into cscomments;
load from cs_kem.unl insert into csfiles;
load from cscontact_kem.unl insert into cscontact;

load from cscmnt_hoho.unl insert into cscomments;
load from cs_hoho.unl insert into csfiles;
load from cscontact_hoho.unl insert into cscontact;


load from cscmnt_uk.unl insert into cscomments;
load from cs_uk.unl insert into csfiles;
load from cscontact_uk.unl insert into cscontact;

load from cscmnt_euro.unl insert into cscomments;
load from cs_euro.unl insert into csfiles;
load from cscontact_euro.unl insert into cscontact;










 