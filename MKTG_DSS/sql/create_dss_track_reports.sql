create table "informix".dss_track_reports
  (
    dtr_key serial not null,
    dtr_user_name varchar(50) not null,
    dtr_client_pc varchar(50) not null, 
    dtr_run_date datetime year to second default current year to second,
    dtr_system char(4) not null,
    dtr_start_pd_date date not null,
    dtr_end_pd_dt date not null,
    dtr_futures_only char(1) not null,
    dtr_report_name varchar(25) not null,
    dtr_country char(10) not null,
    dtr_city varchar(50) not null,
    dtr_other_flags varchar(100),
    dtr_app_name varchar(50)  
  )lock mode row;
revoke all on "informix".dss_track_reports from "public";


create unique index "informix".dtr_key on 
	"informix".dss_track_reports(dtr_key)using btree;

create index "informix".idx_usrname on 
	"informix".dss_track_reports (dtr_user_name) using btree ;

create index "informix".idx_rundate on 
	"informix".dss_track_reports (dtr_run_date) using btree ;

create index "informix".idx_rundateusr on 
	"informix".dss_track_reports 
	(dtr_run_date, dtr_user_name) using btree ;

-- insert into dss_track_reports 
-- values(0,"CHESTER","XP-CHESTER",current year to second,"ALL",
-- date("3/1/2007"),date("6/14/2007"),
-- "n","Frequency","*","*","min times booked:[5]","Marketing DSS");

