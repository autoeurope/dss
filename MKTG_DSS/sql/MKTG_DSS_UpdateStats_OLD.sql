-- update stats script for marketing dss (rpbus)
-- ---------------------------------------------

-- distributions on tables:
update statistics medium for table vouchandqueue distributions only; 
update statistics medium for table g_client distributions only;
update statistics medium for table dss_track_reports distributions only;
update statistics medium for table cc_ref distributions only;
update statistics medium for table csfiles distributions only;
update statistics medium for table cscontact distributions only;
update statistics medium for table cscomments distributions only;
update statistics medium for table q_vouchers distributions only;

-- add g_vendor Jan 13, 2008
update statistics medium for table g_vendor distributions only;

update statistics medium for table g_voucher_summary distributions only;
update statistics high for table g_voucher_summary(vs_basesys);
update statistics high for table g_voucher_summary(vs_voucher);
update statistics medium for table g_voucher_summary(vs_release);


-- columns head of index:

update statistics high for table vouchandqueue(vchpqbath);
update statistics high for table vouchandqueue(basesys);
update statistics high for table vouchandqueue(clientid);
update statistics high for table cc_ref(cc_id);
update statistics high for table q_vouchers(basesys);

-- add g_vendor Jan 13, 2008
update statistics high for table g_vendor(gv_basesys);
update statistics high for table g_vendor(gv_iata);




-- track reports table:

update statistics high for table dss_track_reports(dtr_key);
update statistics high for table dss_track_reports(dtr_user_name);
update statistics high for table dss_track_reports(dtr_run_date);


-- columns that are in indexes but not at head of index
-- use high because performance guide suggest
update statistics high for table vouchandqueue(vcnumb); -- used in two indexes same[i] = high
update statistics high for table vouchandqueue(vchrel); -- used in two 

-- cs archive
update statistics high for table csfiles(cs_basesys);
update statistics high for table csfiles(cs_res);
update statistics high for table csfiles(cs_rel);
update statistics high for table csfiles(cs_filestat);
update statistics high for table csfiles(cs_filetype);

update statistics high for table cscontact(csclt_basesys);
update statistics high for table cscontact(csclt_vouch);
update statistics high for table cscontact(csclt_release);
update statistics high for table cscontact(csclt_filetype);

update statistics high for table cscomments(cscmnt_basesys);
update statistics high for table cscomments(cscmntresnum);
update statistics high for table cscomments(cscmtresrel);
update statistics high for table cscomments(cscmntfiletype);
-- end cs archive




-- rest of keys
-- update statistics low for table vouchandqueue(vcst1); -- index dropped 4/12/2007 as all are paid status
update statistics low for table vouchandqueue(vcntype);
update statistics low for table vouchandqueue(vciata);
update statistics low for table vouchandqueue(vopid);
update statistics low for table vouchandqueue(vopctry1);
update statistics low for table vouchandqueue(vopcity1);
update statistics low for table vouchandqueue(voploc1);
update statistics low for table vouchandqueue(vopdate1);
update statistics low for table vouchandqueue(vopdate2);
update statistics low for table vouchandqueue(voconsort);

update statistics low for table q_vouchers(vopdate1);
update statistics low for table q_vouchers(vcnumb);
update statistics low for table q_vouchers(vchrel);
update statistics low for table q_vouchers(vchdate);


-- columns head of index:
update statistics high for table g_client(gc_key);
update statistics high for table g_client(gc_phone);
update statistics high for table g_client(gc_uid);
update statistics high for table g_client(gc_email);
update statistics high for table g_client(gc_system);
update statistics high for table g_client(gc_lastname);
update statistics high for table g_client(gc_iata);
update statistics high for table g_client(gc_batchdt);
update statistics high for table g_client(gc_lastupdt);

--  all columns that are in indexes but not at head of index
update statistics low for table g_client(gc_firstname);
update statistics low for table g_client(gc_voucher);
update statistics low for table g_client(gc_release);

-- dss_updatestatus
-- update statistics high for table dss_updatestatus(stat_basesys);
-- update statistics high for table dss_updatestatus(stat_tablename);
-- update statistics high for table dss_updatestatus(stat_lastupdate);

