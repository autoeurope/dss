-- update stats script for marketing dss (rpbus)
-- ---------------------------------------------
update statistics medium for table dss_track_reports distributions only;

update statistics medium for table csfiles distributions only;
update statistics medium for table cscontact distributions only;
update statistics medium for table cscomments distributions only;
update statistics medium for table q_vouchers distributions only;

update statistics medium for table cc_ref distributions only;
update statistics high for table cc_ref(cc_id);

update statistics high for table q_vouchers(basesys);


update statistics medium for table g_vendor distributions only;
update statistics high for table g_vendor(gv_basesys);
update statistics high for table g_vendor(gv_iata);

update statistics high for table dss_track_reports(dtr_key);
update statistics high for table dss_track_reports(dtr_user_name);
update statistics high for table dss_track_reports(dtr_run_date);


update statistics medium for table vouchandqueue distributions only;
update statistics high for table vouchandqueue(basesys);
update statistics low for table vouchandqueue(vchpqbath);
update statistics high for table vouchandqueue(vciata);
update statistics high for table vouchandqueue(vopctry1);
update statistics low for table vouchandqueue(vopcity1);
update statistics low for table vouchandqueue(vopid);
update statistics low for table vouchandqueue(voprd1);
update statistics low for table vouchandqueue(vcntype);
update statistics low for table vouchandqueue(voconsort);
update statistics high for table vouchandqueue(vopdate1);
update statistics high for table vouchandqueue(vcnumb); 
update statistics low for table vouchandqueue(vchrel);  


update statistics medium for table g_client distributions only;
update statistics high for table g_client(gc_key);
update statistics high for table g_client(gc_lastname);
update statistics low for table g_client(gc_firstname);
update statistics high for table g_client(gc_system);
update statistics high for table g_client(gc_email);
update statistics high for table g_client(gc_voucher);


update statistics medium for table g_voucher_summary distributions only;
update statistics high for table g_voucher_summary(vs_basesys);
update statistics high for table g_voucher_summary(vs_paid_dt);
update statistics high for table g_voucher_summary(vs_voucher_create_dt);
update statistics high for table g_voucher_summary(vs_voucher);
update statistics low for table g_voucher_summary(vs_release);
update statistics low for table g_voucher_summary(vs_pu_ctry);
update statistics low for table g_voucher_summary(vs_pu_city);
update statistics low for table g_voucher_summary(vs_operator_code);
update statistics low for table g_voucher_summary(vs_direct_or_ta);
update statistics low for table g_voucher_summary(vs_consortium);
update statistics low for table g_voucher_summary(vs_active_history);
update statistics low for table g_voucher_summary(vs_sipp);


-- cs archive
update statistics high for table csfiles(cs_basesys);
update statistics high for table csfiles(cs_res);
update statistics high for table csfiles(cs_rel);
update statistics high for table csfiles(cs_filestat);
update statistics high for table csfiles(cs_filetype);

update statistics high for table cscontact(csclt_basesys);
update statistics high for table cscontact(csclt_vouch);
update statistics high for table cscontact(csclt_release);
update statistics high for table cscontact(csclt_filetype);

update statistics high for table cscomments(cscmnt_basesys);
update statistics high for table cscomments(cscmntresnum);
update statistics high for table cscomments(cscmtresrel);
update statistics high for table cscomments(cscmntfiletype);
-- end cs archive

update statistics low for table q_vouchers(vopdate1);
update statistics low for table q_vouchers(vcnumb);
update statistics low for table q_vouchers(vchrel);
update statistics low for table q_vouchers(vchdate);
