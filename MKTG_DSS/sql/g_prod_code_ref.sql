drop table g_prod_code_ref;

create table "informix".g_prod_code_ref
  (
    pcr_basesys char(4)
        default '*' not null ,
    pcr_basediv char(4)
        default '*' not null ,
    pcr_chaos_subtype char(2)
        default '?',
    pcr_chaos_stype1 char(1)
        default '?',
    pcr_description varchar(25) not null ,
    pcr_product_type char(10)
        default 'UNKNOWN',
    pcr_product_subtype char(10)
        default 'UNKNOWN',
    pcr_source_market char(10)
        default 'UNKNOWN',
    pcr_old_new_code char(1)
        default 'n',
    pcr_sort_order integer
        default 1,
    pcr_date_changed date
        default  '12/31/1950',
    pcr_who_changed varchar(25)
        default 'UNKNOWN',
    pcr_key serial not null
  ) lock mode row;
revoke all on "informix".g_prod_code_ref from "public" as "informix";

create unique index "informix".ix_ptsssysdoubletype on "informix"
    .g_prod_code_ref (pcr_basesys,pcr_chaos_subtype,pcr_chaos_stype1,
    pcr_old_new_code) using btree ;

load from jonload.unl insert into g_prod_code_ref;

