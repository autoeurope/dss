-- a big issue is knowing if VP has finished for the day - we can depend on
-- 1 either using a date that is 3-4 days ago, or checking the queue or vouch for a status of some sort

-- Should I trim(upper(allstringfields)) in this SQL OR on the other side?
 
-- voucher/queue table

unload to KEM_VC.unl (

SELECT 
"KEM" basesys, "" clientid, "" cryptcc, 
vcnumb, vchrel, voit1, voit2,
voit3,  voit1ret,  voit2ret, voit3ret, voit4ret, voit1whl,
trim(upper(vopsubt)) vopsubt, 
trim(upper(vopstype1)) vopstype1, 
voit2whl, voit3whl, voit4whl, voptax1,
voptax2, 
trim(upper(vcntype)) vcntype, 
trim(upper(vciata)) vciata, 
trim(upper(voptype)) voptype, 
vccrcardid, 
trim(upper(vccrcard)) vccrcard,
trim(upper(vcccmonth)) vcccmonth, 
vcrid, 
trim(upper(vccomtaken)) vccomtaken,
vccom1, vccom2, vcdisaddamount, 
trim(upper(affiliate1)) affiliate1,
trim(upper(affiliate2)) affiliate2, 
trim(upper(vodddtestat)) vodddtestat, 
trim(upper(vopcurrency)) vopcurrency, 
vopcurrate, 
vcccauthcode,
trim(upper(vopid)) vopid,
trim(upper(vopid2)) vopid2, 
trim(upper(vopctry1)) vopctry1, 
trim(upper(vopctry2)) vopctry2, 
trim(upper(vopprov1)) vopprov1, 
trim(upper(vopprov2)) vopprov2, 
trim(upper(vopcity1)) vopcity1, 
trim(upper(vopcity2)) vopcity2,
trim(upper(voploc1)) voploc1, 
trim(upper(voploc2)) voploc2, 
trim(upper(voprd1)) voprd1, 
voiamt5,
trim(upper(vcfpover1)) vcfpover1, 
voxtp1, voxtp1w, voxtp2w,
voxtp3w, voxtp4w, vopper1, vopper2, vchdate, voptxper1, voptxper2,
vccashdeposi, vopdate1, vopdate2, voptime1, voptime2,
voptime3, vopper3,
trim(upper(voit1sw)) voit1sw, 
trim(upper(vopcomt1)) vopcomt1, 
trim(upper(vcvpromo1)) vcvpromo1, 
trim(upper(vcvpromo2)) vcvpromo2,
trim(upper(vcvpromo3)) vcvpromo3, 
trim(upper(vccconfirm)) vccconfirm, 
vcclientid,
trim(upper(vcvendorattn)) vcvendorattn, 
trim(upper(vccomments)) vccomments,
trim(upper(vccomments1)) vccomments1, 
trim(upper(vcspecrequest)) vcspecrequest, 
trim(upper(vcarrvair)) vcarrvair, 
trim(upper(vcarrvflight)) vcarrvflight, 
trim(upper(vceta)) vceta, 
trim(upper(vcprepay)) vcprepay,
trim(upper(vcresoffice)) vcresoffice, 
trim(upper(vcst1)) vcst1, 
trim(upper(vcst2)) vcst2, 
trim(upper(vcst3)) vcst3, 
vcstdte1, vcstdte2, vcstdte3,
vord1, vord2, vord3, vord4, vord5, vord6, vord7, vord8, vord9,
vord10, vowd1, vowd2, vowd3, vowd4, vowd5, vopwd6, vopwd7, vopwd8,
vopwd9, vopwd10, vopdatecreate, vopdatemodif, vopamt1, vopamt2,
vopamt3, vopamt4, vopamt5, vopamt6, vopamt7, vopamt8, vopamt9, 
trim(upper(origcurr)) origcurr,
voiamt1, voiamt2, voiamt3, voiamt4, 
trim(upper(voprcode)) voprcode, 
trim(upper(voprt1)) voprt1,
trim(upper(voconsort)) voconsort, 
trim(upper(voconsort2)) voconsort2, 
trim(upper(voconsort3)) voconsort3, 
vcconsortsub, vcconsortsub2,
vcconsortsub3, vorecord, 
trim(upper(voxtp1t)) voxtp1t, 
voxtp2, 
trim(upper(voxtp2t)) voxtp2t,
trim(upper(vocusttype)) vocusttype,
trim(upper(vccomover1)) vccomover1, 
trim(upper(homecurr)) homecurr, 
trim(upper(homectry)) homectry, 
trim(upper(vouch.altresnum1)) altresnum1, 
vchpqbath,
trim(upper(vchpqtype)) vchpqtype, 
vchpqvch, vchpqvchln, vchpqseq, 
trim(upper(vchpstat)) vchpstat, 
trim(upper(vchpmanneed)) vchpmanneed, 
trim(upper(vchpoper)) vchpoper,
trim(upper(vchptranctry)) vchptranctry, 
trim(upper(vchpcity)) vchpcity, 
trim(upper(vchplocation)) vchplocation, 
vchplprintdate, 
trim(upper(vchpqcccredsale)) vchpqcccredsale,
trim(upper(vchpqnsw1)) vchpqnsw1, 
trim(upper(vchpqnsw2)) vchpqnsw2, 
trim(upper(vchpqnsw3)) vchpqnsw3
FROM VOUCH, VCHPQUEUE WHERE
VCHPQBATH = :Indate
AND VCNUMB = VCHPQVCH
AND VCHREL = VCHPQVCHLN
AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" 
and  (VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null)
and 
((vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N'))

);


-- ftp to f50aus - over writing file named KEM_VC.unl if it exists

-- if records exist for this batch date in f50aus.vouchandqueue then delete them (or brute force delete every time)

delete from vouchandqueue where basesys = "KEM" vchpqbath = :InDate;

load from KEM_VC.unl insert into vouchandqueu;












