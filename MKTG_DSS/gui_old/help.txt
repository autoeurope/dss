Step 1: 
~ click the "Step One" tab. 
    These are the primary filters (Required) for all reports in this application.
~ select a Database or use "ALL" to retrieve data from all systems.
~ select start and end "processed date" range

Step 2:
~ Click the "E-Mail Lists" tab.
Here, there are different ways to retrieve e-mail addresses for Direct Clients.
Select a sub tab according to your requirements.
	 
Available E-Mail List Queries:

Frequency: "Booking frequency by country / city / number bookings":  
retrieves a list of e-mail address and total number of bookings for each e-mail address

~ follow above steps, type in country code, city name and minimum number of vouchers for that country and city then click "OK". 

Hint: use wildcards for county and city if needed

Example:  get e-mails for people whom traveled to PARIS 5 or more times in the last 5 years  

"Future PU": 
Example: get e-mails for future pick ups in Italy
Use the same procedure as "Frequency"

"Last Minute"
Example: get a list of e-mails where clients paid and picked up within the same week
Use the same procedure as "Frequency"

Step 3:
After queries complete, the data is displayed in the "Data" tab.
There are currently two types of output: XML and Comma Separated Text.

Select "Data" tab
Click appropriate output button


HINT: Watch the status bar 

General Information:

The Marketing Data Warehouse contains all paid vouchers and client information for all systems since the beginning of chaos. Currently, the data collected is up to and including March 31, 2007 process dates. Future developments will include a nightly uploads of data for the prior business day(s). After implementation, one can expect data to be current through the prior day's business.

Generally, data warehouses contain very large amounts of data compared to production (point of sale) databases. This fact is true for this warehouse, as well as this warehouse also resides on a system with lesser amounts of resources such as memory, therefore, data retrieval times are expected to be much longer.

Typical expectations for retrieving data from production systems are measured in millisecond to minutes, where Data warehouses will be minutes to hours. 
