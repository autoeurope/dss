unit main_mkt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  aecommonfunc,
  jmclasses,
  shellapi,
  dscalc,
  jxml,
  math,
  StdCtrls, ComCtrls, ExtCtrls, DB,
  IfxConnection, Menus, Grids, DBGrids,
  IfxCustomDataSet,
  IfxQuery,
  CheckLst,
  IdAttachmentFile, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdBaseComponent, IdMessage, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache,
  ppDB, ppProd, ppReport, ppComm, ppRelatv, ppDBPipe, ppVar, kbmMemTable;

const

  MTD_SQL =
    'SELECT {+index(vouchandqueue sysbatch)} basesys, VCNUMB,VCHREL,VORD4,VOPAMT4,VCCASHDEPOSI,VOPSUBT, ' +
    'VOPDATE1,VOPCURRENCY, ALTRESNUM1,VCIATA,VOPCTRY1,VOPCITY1,VOPID, ' +
    'A.homectry,vcntype,voptype,vcstdte2,vchpqbath,vccrcardid,vccrcard, ' +
    'vcccmonth,vccconfirm,vcclientid, vcvendorattn, vccomments,vccomments1,vcarrvair, ' +
    'vcarrvflight,vceta,vcprepay,vcccauthcode,vchdate,vcrid,vcvpromo1,vcvpromo2, ' +
    'vcvpromo3,vcst1,vcst2,vcst3,vcstdte1,vcstdte3,vopid2,vopctry2,vopprov1,vopprov2, ' +
    'vopcity2,voploc1,voploc2,voprd1,vopcurrate,vord1,vord2,vord3,vord5,vord6, ' +
    'vord7,vord8,vord9,vord10,vowd1,vowd2,vowd3,vowd4,vowd5,vopwd6,vopwd7,vopwd8, ' +
    'vopwd9,vopwd10,vopdate2,voptime1,voptime2,vopcomt1,voptax2,vopper1,vopper2, ' +
    'vopper3,voptxper1,voptxper2,vopdatecreate,vopdatemodif,vopstype1,vopamt1,vopamt2, ' +
    'vopamt3,voiamt1,voiamt2,voiamt3,voiamt4,voiamt5,voprcode,voconsort,vorecord, ' +
    'voit1,voit1ret,voit1whl,voit2,voit2ret,voit2whl,voit3,voit3ret,voit3whl, ' +
    'voit4ret,voit4whl,voxtp1,voxtp1w,voxtp1t,voxtp2,voxtp2w,voxtp2t,vccom1, ' +
    'vccom2,vcdisaddamount,vcconsortsub,vccomtaken,vcfpover1,vccomover1, ' +
    'A.homecurr,affiliate1,affiliate2,voit1sw,vopamt5,vopamt6, ' +
    'vopamt7,vopamt8,vopamt9,origcurr,vchpqtype,vchpqvch,vchpqvchln,vchpqseq,vchpstat, ' +
    'vchpqnsw1,vchpqnsw2,vchpqnsw3,VOPTAX1 ' +
    'FROM vouchandqueue A WHERE ' +
    'VCHPQBATH BETWEEN :date1 and :date2 ';

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    sb: TStatusBar;
    tabStep1: TTabSheet;
    tabDCEMAIL: TTabSheet;
    conn: TIfxConnection;
    Q1: TIfxQuery;
    DataSource1: TDataSource;
    tabData: TTabSheet;
    DBGrid1: TDBGrid;
    tabhelp: TTabSheet;
    tabForbiddenZone: TTabSheet;
    Memo1: TMemo;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    cbDB: TComboBox;
    Label3: TLabel;
    dtpStart: TDateTimePicker;
    Label11: TLabel;
    dtpEnd: TDateTimePicker;
    PageControl2: TPageControl;
    TabFreq: TTabSheet;
    TabFuturePU: TTabSheet;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label5: TLabel;
    btnFreqCTRYCITY: TButton;
    editNumber: TEdit;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    btnFPU_OK: TButton;
    dtp_FPU_Start: TDateTimePicker;
    dtp_FPU_END: TDateTimePicker;
    Panel3: TPanel;
    lblSYSTEM: TLabel;
    lblDates: TLabel;
    Panel1: TPanel;
    Panel4: TPanel;
    tabLastMinute: TTabSheet;
    GroupBox3: TGroupBox;
    btnLastMin: TButton;
    EditLMDays: TEdit;
    Label18: TLabel;
    Button2: TButton;
    Image1: TImage;
    Panel2: TPanel;
    btnCSV: TButton;
    Button3: TButton;
    Label22: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    Memo2: TMemo;
    SheetAge: TTabSheet;
    GroupBox5: TGroupBox;
    btnAGE: TButton;
    editAGELow: TEdit;
    editAGEHigh: TEdit;
    Label28: TLabel;
    sheetPayType: TTabSheet;
    GroupBox6: TGroupBox;
    Label32: TLabel;
    btnPayType: TButton;
    clbPayType: TCheckListBox;
    sheetduration: TTabSheet;
    Label33: TLabel;
    EditCountry: TEdit;
    Label34: TLabel;
    EditCity: TEdit;
    Label35: TLabel;
    GroupBox7: TGroupBox;
    Label7: TLabel;
    btnDurationGO: TButton;
    editDurationLOW: TEdit;
    EditDurationHIGH: TEdit;
    sheetCarType: TTabSheet;
    Label17: TLabel;
    Button13: TButton;
    Button14: TButton;
    lblshit: TLabel;
    cbFuturesOnly: TCheckBox;
    blbFUtures: TLabel;
    Label6: TLabel;
    sheetGender: TTabSheet;
    lblConsort: TLabel;
    EditToMail: TEdit;
    cbemail: TCheckBox;
    Msg: TIdMessage;
    SMTP: TIdSMTP;
    EditFrom: TEdit;
    Label9: TLabel;
    btnEMailSend: TButton;
    rgGender: TRadioGroup;
    btnGenderOK: TButton;
    Label14: TLabel;
    Label15: TLabel;
    Label20: TLabel;
    GroupBox9: TGroupBox;
    Label24: TLabel;
    dtpTrackReportsStart: TDateTimePicker;
    Label27: TLabel;
    dtpTrackReportsEnd: TDateTimePicker;
    btnTrack: TButton;
    pipeTrack: TppDBPipeline;
    reportTrack: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppDBText1: TppDBText;
    ppLabel1: TppLabel;
    ppDBText2: TppDBText;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppDBText3: TppDBText;
    ppLabel4: TppLabel;
    ppDBText4: TppDBText;
    ppLabel5: TppLabel;
    ppDBText5: TppDBText;
    ppLabel6: TppLabel;
    ppDBText6: TppDBText;
    ppLabel7: TppLabel;
    ppDBText7: TppDBText;
    ppDBText8: TppDBText;
    ppLabel8: TppLabel;
    ppLabel9: TppLabel;
    ppDBText9: TppDBText;
    ppLabel10: TppLabel;
    ppDBText10: TppDBText;
    ppLabel11: TppLabel;
    ppDBText11: TppDBText;
    ppLabel12: TppLabel;
    ppDBText12: TppDBText;
    ppLabel13: TppLabel;
    ppDBText13: TppDBText;
    ppLine1: TppLine;
    ppTitleBand1: TppTitleBand;
    ppLabel14: TppLabel;
    lblDate: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppGroup1: TppGroup;
    ppGroupHeaderBand1: TppGroupHeaderBand;
    ppGroupFooterBand1: TppGroupFooterBand;
    ppDBText14: TppDBText;
    ppLine2: TppLine;
    ppDBCalc1: TppDBCalc;
    cbSumOnly: TCheckBox;
    asdf: TppLabel;
    ppLine3: TppLine;
    ppLine4: TppLine;
    ppSummaryBand1: TppSummaryBand;
    ppDBCalc3: TppDBCalc;
    lblLast: TppLabel;
    Button1: TButton;
    mem: TkbmMemTable;
    dsc: TDSCalc;
    jonLbl: TLabel;
    GroupBox8: TGroupBox;
    Label10: TLabel;
    Label16: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label30: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    btnCarTypeGO: TButton;
    clbpos1: TCheckListBox;
    clbpos2: TCheckListBox;
    clbpos4: TCheckListBox;
    clbpos3: TCheckListBox;
    EdotPos5to8: TEdit;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    rgCarTypeOutputType: TRadioGroup;
    sheetReportsOther: TTabSheet;
    PageControl3: TPageControl;
    SheetTopTen: TTabSheet;
    Panel5: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure connect1Click(Sender: TObject);
    procedure disconnect1Click(Sender: TObject);
    procedure btnFreqCTRYCITYClick(Sender: TObject);
    procedure tabStep1Hide(Sender: TObject);
    procedure btnCSVClick(Sender: TObject);
    procedure btnFPU_OKClick(Sender: TObject);
    procedure btnLastMinClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btnAGEClick(Sender: TObject);
    procedure btnPayTypeClick(Sender: TObject);
    procedure sheetPayTypeShow(Sender: TObject);
    procedure btnDurationGOClick(Sender: TObject);
    procedure sheetCarTypeShow(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure btnCarTypeGOClick(Sender: TObject);
    procedure btnGenderOKClick(Sender: TObject);
    procedure cbDBExit(Sender: TObject);
    procedure btnEMailSendClick(Sender: TObject);
    procedure btnTrackClick(Sender: TObject);
    procedure reportTrackBeforePrint(Sender: TObject);
    procedure ppDetailBand1BeforeGenerate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure tabDataShow(Sender: TObject);
    procedure JScroll(DataSet: TDataSet);
    procedure memAfterScroll(DataSet: TDataSet);
    procedure Q1AfterScroll(DataSet: TDataSet);
    procedure Q1BeforeOpen(DataSet: TDataSet);
    procedure EditCountryExit(Sender: TObject);
    procedure EditCityExit(Sender: TObject);
  private
    LatestRunDate: TDateTime;
    procedure addToRunLog(sender: TButton); // add June 6, 2007 per IK - they all needed this yesterday, let's see how much the actuall use it
    procedure FillDBS();
    procedure OpenQ();
    function InitQ(): boolean;
    procedure UpdateLabels;
    procedure OpenFile(InFileName: string);
    function GetXMLHeaderAttribs(InReportName: string): string;
    function GetHeader(InReportName: string): string;
    procedure FocusCtry();
    procedure FocusCity();
    function GetDB_arg(): string;
    function ThrottleDB(): boolean;
    function GetConsortArg(): string;
    function GetClientArg(): string;
    procedure EMAIL(sFILENAME: string);
    procedure OpenQAndFillMem(InGlobFName: string);
    procedure SetMem(InGLobFName: string);
    procedure DoOutPut(sender: TObject; O_Type, InFileName: string);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  startTime, endTime: DWORD;
  GlobFName, globEMLFileName: string;
  xxxxx: Integer;

implementation

{$R *.dfm}

procedure TForm1.DoOutPut(sender: TObject; O_Type, InFileName: string);
var sFILENAME: string;
begin
  globEMLFileName := '';
  O_Type := trim(upperCase(O_Type));
  xxxxx := 0;

  if Q1.IsEmpty then
  begin
    showmessage('no data');
    exit;
  end;

  if not ((O_Type = 'CSV') or (O_Type = 'XML') or (O_Type = 'HTML')) then
  begin
    showmessage('invalid output type.');
    exit;
  end;

  if (trim(InFileName) = '') then InFileName := GetGUIDString();

  try
    Q1.DisableControls;
    mem.DisableControls;
    sFILENAME := SetDirectoryForTSUser();

    if (O_Type = 'CSV') then
    begin
      SAdd(sFILENAME, 'CSV_' + InFileName + '.csv');
    end
    else if (O_Type = 'XML') then
    begin
      SAdd(sFILENAME, 'XML_' + InFileName + '.xml');
    end
    else // O_Type == 'HTML'
    begin
      SAdd(sFILENAME, 'HTML_' + InFileName + '.html');
    end;

    sb.Panels[2].Text := 'Please wait..Generating ' + O_Type;
    application.ProcessMessages;

    try
      if fileexists(sFILENAME) then deletefile(sFilename);
    except;
    end;

    globEMLFileName := sFILENAME;

    try
      if (O_Type = 'CSV') then
        SaveCSV(DataSource1, sFileName, GetHeader(InFileName), ',')
      else if (O_Type = 'XML') then
        SaveXMLToFile(datasource1, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1)
      else // O_Type == 'HTML'
        SaveDataSetToHtmlFile(datasource1, 'AE_WHSE_RESULTS', InFileName, sFILENAME, q1.FieldCount - 1);
    except on e: exception do
      begin
        showmessage('error saving ' + O_Type + ' --> ' + E.Message);
      end;
    end;

    sb.Panels[2].Text := 'Done generating ' + O_Type + '.';
    application.ProcessMessages;

    if cbEmail.Checked then EMail(sFILENAME)
    else OpenFile(sFILENAME);

    btnEMailSend.Enabled := true;
  finally
    Q1.EnableControls;
    mem.EnableControls;

  end;

end;

procedure TForm1.addToRunLog(sender: TButton);
var temp, xxx, theSQL,
  usr, pc, basesys, futuresOnly,
    reportName, ctry, city,
    otherFlags, appname: string;
  x: integer;
  startDate, endDate: TDate;
  MyQuery: TIfxQuery;
begin
  usr := '';
  pc := '';
  basesys := '';
  futuresOnly := '';
  reportName := '';
  ctry := '';
  city := '';
  otherFlags := '';
  appName := '';
  theSQL := '';
  usr := GetTheUserName();
  pc := GetMachineName();
  // rundate plugged by infx
  basesys := cbdb.text;

  if cbFuturesOnly.Checked then futuresOnly := 'y'
  else futuresOnly := 'n';

  reportname := GlobFName;
  ctry := EditCountry.Text;
  city := EditCity.Text;
  appName := application.Title;
  startDate := dtpStart.date;
  endDate := dtpEnd.Date;

  temp := 'DB: ' + basesys + '|' +
    'PD_dates: ' + dateToStr(dtpStart.date) + '-' + dateToStr(dtpEnd.Date) + '|';

  if cbFuturesOnly.Checked then
    stradd(temp, 'for Future PU only|');

  stradd(temp, 'country: [' + ctry + ']');
  stradd(temp, 'city: [' + city + ']');

  ////case sender of
  if sender = btnFreqCTRYCITY then
  begin
    stradd(temp, '" BY_FREQUENCY" (min times booked): [' + editNumber.Text + ']');
    stradd(otherFlags, 'min times booked:[' + editNumber.Text + ']');
  end
  else if sender = btnFPU_OK then
  begin
    stradd(temp, 'PU Dates: [' + datetostr(dtp_FPU_start.date) + '-' + datetostr(dtp_FPU_END.date) + ']');
    stradd(otherFlags, 'PU Dates: [' + datetostr(dtp_FPU_start.date) + '-' + datetostr(dtp_FPU_END.date) + ']');

  end
  else if sender = btnLastMin then
  begin
    stradd(temp, 'LastMinute: days between pd and pu [' + EditLMDays.Text + ']');
    stradd(otherFlags, 'days between pd and pu: [' + EditLMDays.Text + ']');
  end
  else if sender = btnAGE then
  begin
    stradd(temp, 'between ages of: [' + editagelow.Text + ' and ' + editagehigh.text + ']');
    stradd(otherFlags, 'between ages of: [' + editagelow.Text + ' and ' + editagehigh.text + ']');
  end
  else if sender = btnPayType then
  begin
    xxx := '';
    stradd(xxx, ' by pay types: ['); //']' + ' " BY_FREQUENCY"'); end;

    for x := 0 to CLBPayType.Items.Count - 1 do
    begin
      if CLBPayType.Checked[x] then
      begin
        case x of
          0: {AMEX} stradd(xxx, 'amex, ');
          1: {VISA} stradd(xxx, 'visa, ');
          2: {MC} stradd(xxx, 'mastercard, ');
          3: {Discover} stradd(xxx, 'discover, ');
          4: {Diners} stradd(xxx, 'diners, ');
          5: {cc bu} stradd(xxx, 'cc bu, ');
          6: {DP} stradd(xxx, 'deferred paymnt, ');
          7: {Check} stradd(xxx, 'check, ');
          8: {Waive Deposit} stradd(xxx, 'waive deposit, ');
          9: {waive total} stradd(xxx, 'waive total, ');
          10: {Other} stradd(xxx, 'other, ');
          11: {Switch} stradd(xxx, 'switch, ');
          12: {TIAS} stradd(xxx, 'tias, ');
          13: {TT} stradd(xxx, 'tt, ');
          14: {ExcVch} stradd(xxx, 'excvch, ');
          15: {MCO} stradd(xxx, 'mco, ');
          16: {EFT Bal DUe} stradd(xxx, 'EFT bal due, ');
          17: {EFT} stradd(xxx, 'eft, ');
        else ;
        end;
      end; //case
    end; // for

    stradd(xxx, ' ]');
    stradd(temp, xxx);
    stradd(otherFlags, xxx);
  end
  else if sender = btnDurationGO then
  begin
    stradd(temp, 'Duration between: [' + editDUrationLow.Text + ' and ' + EditDUrationHigh.Text + ']');
    stradd(otherFlags, 'Duration between: [' + editDUrationLow.Text + ' and ' + EditDUrationHigh.Text + ']');
  end
  else if sender = btnCarTypeGO then
  begin
    stradd(temp, 'By Car Type (sipp)');
    stradd(otherflags, '*** multi SIPP ***');
  end
  else if sender = btnGenderOK then
  begin
    if rgGender.ItemIndex = 0 then
    begin
      stradd(temp, 'by gender - Male');
      stradd(otherFlags, 'Male');
    end
    else
    begin
      stradd(temp, 'by gender - Female');
      stradd(otherFlags, 'Female');
    end;
  end
  else
    exit;

  try // log file::
    jSaveFile('\\fileserver\chaoslog\', 'dss_run_log', '.txt',
      usr + '|machine: ' + pc +
      '|' + Application.Name + '|' + temp + '| PC date: ' + DateTImeTOStr(now));
  except;
  end;

  if not conn.Connected then
  begin
    try connect1Click(nil);
    except exit;
    end;
  end;

  theSQL := 'insert into dss_track_reports values(0,"' + usr + '",' +
    '"' + pc + '",' +
    'current year to second,' +
    '"' + basesys + '",' +
    'date("' + dateToStr(startDate) + '"),' +
    'date("' + dateToStr(endDate) + '"),' +
    '"' + futuresOnly + '",' +
    '"' + reportName + '",' +
    '"' + ctry + '",' +
    '"' + city + '",' +
    '"' + otherFlags + '",' +
    '"' + appName + '");';
  try
    try
      MyQuery := GetIDACQuery(conn);
      MyQuery.sql.Add(theSql);
      MyQuery.ExecSQL;
    except on e: exception do
      begin
        jSaveFile('\\fileserver\chaoslog\', 'dss_err_log', '.txt',
          DateTImeTOStr(now) + '--> failed insert on dss_track_reports: --> ' + e.message);
      end;
    end;
  finally
    MyQuery.active := false;
    MyQuery.free;
    MyQuery := nil;
  end;
end;

function TForm1.GetConsortArg(): string;
var s: string;
begin
  result := '';
  s := trim(uppercase(paramstr(2))); // s/b consort code
  if s = '' then exit
  else result := ' and voconsort = "' + s + '" ';
end;

function TForm1.GetClientArg(): string;
var s: string;
begin // dtag does not use iata "99" for direct client
  result := '';
  s := trim(uppercase(paramstr(2))); // s/b consort code
  if s = 'DTAG' then
    result := ' and length(gc_email) > 0 '
  else
    result := ' and substr(gc_iata,1,2) = "99" and length(gc_email) > 0 ';
end;

function TForm1.ThrottleDB(): Boolean;
var MyList: TStringList;
begin
  result := false;
  try
    try
      MyList := TStringList.Create;
      MyList.Text := cbDb.Items.Text;
      if IsInList(MyList, cbDB.Text) then result := true;
    except;
    end;
  finally
    if Assigned(MyList) then FreeAndNil(MyList);
  end;
end;

procedure TForm1.FocusCtry();
begin
  try
    pagecontrol1.ActivePage := tabStep1;
    EditCountry.SetFocus;
  except;
  end;
end;

procedure TForm1.FocusCity();
begin
  try
    pagecontrol1.ActivePage := tabStep1;
    EditCity.SetFocus;
  except;
  end;
end;

procedure TForm1.btnPayTypeClick(Sender: TObject);
var theSQL: string;
  i: integer;

  function GetPTSQLSTR(): string;
  var x: Integer; sql: string; beerTime: boolean;
  begin
    result := '';
    beerTime := false;
    sql := ' and vccrcardid in(';

    for x := 0 to CLBPayType.Items.Count - 1 do
    begin
      if CLBPayType.Checked[x] then
      begin
        beerTime := true;
        case x of
          0: {AMEX} stradd(sql, '0,');
          1: {VISA} stradd(sql, '1,');
          2: {MC} stradd(sql, '2,');
          3: {Discover} stradd(sql, '3,');
          4: {Diners} stradd(sql, '4,');
          5: {cc bu} stradd(sql, '12,');
          6: {DP} stradd(sql, '8,');
          7: {Check} stradd(sql, '9,');
          8: {Waive Deposit} stradd(sql, '10,');
          9: {waive total} stradd(sql, '11,');
          10: {Other} stradd(sql, '5,');
          11: {Switch} stradd(sql, '6,');
          12: {TIAS} stradd(sql, '21,');
          13: {TT} stradd(sql, '22,');
          14: {ExcVch} stradd(sql, '23,');
          15: {MCO} stradd(sql, '24,');
          16: {EFT Bal DUe} stradd(sql, '25,');
          17: {EFT Bal DUe} stradd(sql, '20,');
        else ;
        end;
      end; //case
    end; // for

    if beerTime then
    begin
      sql := copy(sql, 1, length(sql) - 1);
      stradd(sql, ')');
    end
    else sql := '';
    result := sql;
  end;

begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'ByPayType';
  addtorunlog(btnPayType);

  theSQL := 'Select gc_system DB_system, cc_name, gc_email client_email, vopdate1 PU_DT,  ';
  theSQL := theSQL + ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client, outer cc_ref where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg()); // constrict to DTAG or other constortium ( parastr(2) )


  if cbFuturesOnly.Checked then
    stradd(theSQL, 'and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" '); // invalid if not updated after feed.
                                         // must update to "H" for lower release numbers

  stradd(theSQL, 'and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');
  stradd(theSQL, ' and cc_id = vccrcardid '); // join cc_ref table

  stradd(theSQL, GetPTSQLSTR());
  q1.SQL.Text := theSQL;

  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);

  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  OpenQ();
end;


procedure TForm1.Button10Click(Sender: TObject);
begin
  SetCLB(clbpos3, false);
end;

procedure TForm1.Button11Click(Sender: TObject);
begin
  SetCLB(clbpos4, true);
end;

procedure TForm1.Button12Click(Sender: TObject);
begin
  SetCLB(clbpos4, false);
end;

procedure TForm1.Button13Click(Sender: TObject);
begin
  SetCLB(clbpaytype, true);
end;

procedure TForm1.Button14Click(Sender: TObject);
begin
  SetCLB(clbpaytype, false);
end;

procedure TForm1.Button1Click(Sender: TObject);
var TempFN, sFILENAME: string;
begin
  DoOutPut(Button1, 'HTML', GlobFName);
end;

procedure TForm1.btnTrackClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  LatestRunDate := 0;

  initQ();
  theSQL := 'Select * From dss_track_reports ';
  stradd(theSQL, ' where date(dtr_run_Date) between :date1 and :date2 ');
  stradd(theSQL, ' order by dtr_user_name, dtr_run_date, dtr_report_name');
  q1.SQL.Text := theSQL;

  q1.ParamByName('date1').AsDateTime := trunc(dtpTrackReportsStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpTrackReportsEnd.DateTime);

  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);

  reportTrack.Reset;

  if cbSumOnly.Checked then
  begin
    ppHeaderBand1.Visible := false;
    ppDetailBand1.Visible := false;
    ppLine2.Visible := false;
  end
  else
  begin
    ppHeaderBand1.Visible := true;
    ppDetailBand1.Visible := true;
    ppLine2.Visible := true;
  end;

  OpenQ();
  reportTrack.Print;
end;

procedure TForm1.btnGenderOKClick(Sender: TObject);
var theSQL: string;
  i: integer;

  function GetGenderArg(): string;
  begin // "gender" switch not needed per IK - Use Salutation (stored in surname) instead 5/15/2007
    result := ' and ( (length(gc_surname) > 0 and gc_surname is not null) ';

    case rgGender.ItemIndex of
      0: begin result := result + ' and gc_surname in("MR.") ) '; end; // male
      1: begin result := result + ' and gc_surname in("MRS.", "MS.") ) '; end; // female
    else result := '';
    end;
  end;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  if rgGender.ItemIndex = -1 then
  begin
    showmessage('Please select a gender.');
    exit;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'ByGender';
  addtorunlog(btngenderOK);

  theSQL := 'Select gc_system DB_system, trim(upper(gc_surname)) Gender, trim(gc_lastname) || ", " || trim(gc_firstname) c_name, vcnumb || "-" || vchrel voucher, gc_email client_email, vopdate1 PU_DT  ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());
  stradd(theSQL, GetGenderArg());

  if cbFuturesOnly.Checked then
    stradd(theSQL, ' and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" '); // invalid if not updated after feed.
                                         // must update to "H" for lower release numbers

  stradd(theSQL, ' and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');
  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);

  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  OpenQ();
end;

procedure TForm1.btnEMailSendClick(Sender: TObject);
begin
  try
    if fileexists(globEMLFileName) then EMAIL(globEMLFileName)
    else showmessage('The File [' + globEMLFileName + ' does not exist.');
  except;
  end;
end;

procedure TForm1.btnLastMinClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  try
    StrTOInt(EditLMDays.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try EditLMDays.SetFocus; except; end;
      exit;
    end;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'DaysBetweenPDPU';
  addToRunLog(btnLastMin);

  theSQL := 'Select gc_system DB_system, gc_email client_email, vchpqbath PD_DT, vopdate1 PU_DT, (vopdate1-date(vchpqbath)) days_between, ';
  theSQL := theSQL + ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ';

  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());

  if cbFuturesOnly.Checked then
    stradd(theSQL, 'and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" '); // invalid if not updated after feed.
                                         // must update to "H" for lower release numbers

  stradd(theSQL, 'and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');

  stradd(theSQL, 'and (vopdate1-date(vchpqbath)) <= ' + EditLMDays.Text);
  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);
  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  OpenQ();
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  PageControl1.ActivePage := tabDCEMAIL;
  application.ProcessMessages;
end;

function TForm1.GetXMLHeaderAttribs(InReportName: string): string;
begin
  result := '';
  if trim(InReportName) = '' then exit;
  InReportName := trim(uppercase(InReportName));

  try
    XAdd(result, ' DataBases="' + trim(uppercase(cbDB.Text)) + '"');
    XAdd(result, ' StartProcessDate="' + FormatDateTime('MMM_DD_YYYY', dtpStart.Date) + '"');
    XAdd(result, ' EndProcessDate="' + FormatDateTime('MMM_DD_YYYY', dtpEND.Date) + '"');

    if (InReportName = 'CTRYCITYFREQ') then
    begin
     // country           city          Freq
     // EditDCEMcountry   EditDCEMCity  editNumber
      XAdd(result, ' Country="' + trim(uppercase(EditCountry.Text)) + '"');
      XAdd(result, ' City="' + trim(uppercase(EditCity.Text)) + '"');
      XAdd(result, ' Frequency="' + trim(uppercase(editNumber.text)) + '"');
    end
    else if (InReportName = 'DAYSBETWEENPDPU') then
    begin
     // country           city          days
     // EditLMCountry     EditLMCity    EditLMDays
      XAdd(result, ' Country="' + trim(uppercase(EditCountry.Text)) + '"');
      XAdd(result, ' City="' + trim(uppercase(EditCity.text)) + '"');
      XAdd(result, ' DaysBetween="' + trim(uppercase(EditLMDays.Text)) + '"');
    end
    else if (InReportName = 'FUTUREPU') then
    begin
     // country           city          pick up st      pick up end
     // EditFPUCTRY       EditFPUCITY   dtp_FPU_Start   dtp_FPU_END
      XAdd(result, ' Country="' + trim(uppercase(EditCountry.Text)) + '"');
      XAdd(result, ' City="' + trim(uppercase(EditCITY.Text)) + '"');
      XAdd(result, ' PickUpStart="' + FormatDateTime('MMM_DD_YYYY', dtp_FPU_Start.Date) + '"');
      XAdd(result, ' PickUpEnd="' + FormatDateTime('MMM_DD_YYYY', dtp_FPU_END.Date) + '"');
    end;
  except result := '';
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  DoOutPut(Button3, 'XML', GlobFName);
end;

procedure TForm1.btnCarTypeGOClick(Sender: TObject);
var theSql: string;

  function GetInStatement(sender: TCheckListBox; pos: Integer): string;
  var x: Integer;
    sql: string;
    DoItUp: boolean;
  begin
    result := '';
    DoItUp := false;
    sql := ' and voprd1[' + IntToStr(pos) + '] in(';

    for x := 0 to sender.Items.Count - 1 do
    begin
      if sender.Checked[x] then
      begin
        DoItUp := true;
        stradd(sql, '"' + sender.items[x] + '",');
      end;
    end;

    if DoItUp then
    begin
      sql := copy(sql, 1, length(sql) - 1);
      stradd(sql, ')');
    end
    else sql := '';
    result := sql;
  end;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end
  else if (rgCarTypeOutputType.ItemIndex = -1) then
  begin
    showmessage('Select an "output". See the Radio Group on the "car type" tab.');
    exit;
  end;

  initQ();
  theSQL := '';

  if (rgCarTypeOutputType.ItemIndex = 0) then // standard e-mail list output
  begin
    GlobFName := 'ByCarType';
    theSQL := 'Select gc_system DB_system, voprd1 sipp, gc_email client_email, vopdate1 PU_DT,  ';
    SAdd(theSQL, ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ');
    if paramstr(2) > '' then SAdd(theSQL, ', voconsort consortium ');
    SAdd(theSQL, ' From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ');
  end
  else // specialty car output - added for MAX 8/27/2007
  begin // either all releases or CXLS only
         // see exceptions to SQL divided by rgCarTypeOutputType.ItemIndex
    {
    output required:
    1. Voucher #
    2. Confirmation #
    3. Date Booked
    4. Date of Service
    5. Time of Service
    6. Date Cxl'd
    7. Time Cxl'd
    8. Retail USD
    9. Cost Currency

    msg back 8/27:
    I understand what you need for output, but what should the query be?
    for example:
    1: Get all Cxld specialty car bookings --
    2: get all specialty car bookings and include cxld bookings
    Also, if option 2, do you need to see all releases like the sales journal, or just the latest release?

    Maximilian Brockmann [27/Aug/07 10:59 AM]
    Point 1 and 2, yes
    If possible regarding option 2 we would need to see all releases
    }
    theSQL := MTD_SQL;


    if (rgCarTypeOutputType.ItemIndex = 1) then // all releases
      GlobFName := 'ByCarType_Spec_SIPP_ALL'
    else
      GlobFName := 'ByCarType_Spec_SIPP_CXLS'; // cxls only
  end; // - else special MAX report

  addtorunlog(btncartypego);

  if trim(EditCountry.Text) <> '*' then
    sadd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    sadd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  SAdd(theSQL, GetDB_arg());

  if (rgCarTypeOutputType.ItemIndex = 0) then // only needed for e-mail list
    SAdd(theSQL, GetClientArg());

  SAdd(theSQL, GetConsortArg()); // restrict to a consortium "DTAG" for example

  if cbFuturesOnly.Checked then
    sadd(theSQL, 'and vopdate1 >= current ');

  if (rgCarTypeOutputType.ItemIndex = 0) then // join and "active" only needed for e-mail list
  begin
    sadd(theSQL, ' and vcntype = "A" ');
    sadd(theSQL, 'and gc_system = basesys and gc_voucher = vcnumb ');
    sadd(theSQL, ' and gc_release = vchrel ');
  end;

  sadd(theSql, GetInStatement(clbpos1, 1));
  sadd(theSql, GetInStatement(clbpos2, 2));
  sadd(theSql, GetInStatement(clbpos3, 3));
  sadd(theSql, GetInStatement(clbpos4, 4));

  if (rgCarTypeOutputType.ItemIndex = 2) then // filter cxls only
  begin
    sadd(theSQL, 'and mod(vchrel, 2) = 0 ');
    sadd(theSQL, 'AND vchrel = (select max(vchrel) from vouchandqueue where basesys = A.basesys and vcnumb = A.vcnumb)');
  end;

  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);
  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);

  if (rgCarTypeOutputType.ItemIndex = 0) then OpenQ()
  else OpenQAndFillMem(GlobFName);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  SetCLB(clbpos1, true);
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  SetCLB(clbpos1, false);
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  SetCLB(clbpos2, false);
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  SetCLB(clbpos2, true);
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
  SetCLB(clbpos3, true);
end;

procedure TForm1.btnFPU_OKClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'ByPUDate';
  addToRunLog(btnFPU_OK);

  theSQL := 'Select gc_system DB_system, gc_email client_email, vopctry1 PU_ctry, vopcity1 PU_City, vopdate1 PU_DT,  ';
  theSQL := theSQL + ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());

  stradd(theSQL, ' and vopdate1 between :date3 and :date4 ');

  if cbFuturesOnly.Checked then
    stradd(theSQL, 'and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');
  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);
  q1.ParamByName('date3').AsDateTime := trunc(dtp_FPU_Start.DateTime);
  q1.ParamByName('date4').AsDateTime := trunc(dtp_FPU_END.DateTime);
  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  memo1.Lines.Add('PU Start: ' + q1.Params[2].asstring);
  memo1.Lines.Add('PU End: ' + q1.Params[3].asstring);
  OpenQ();
end;

function TForm1.GetHeader(InReportName: string): string;
var MyList: TStringList;
  sPROCDT, sDB: string;
begin
  result := '';
  if trim(InReportName) = '' then exit;
  InReportName := trim(uppercase(InReportName));
  MyList := TStringList.Create;

  try
    try
      sPROCDT := 'Process Dates: ' + FormatDateTime('MMM DD YYYY', dtpStart.Date) + '  to  ' +
        FormatDateTime('MMM DD YYYY', dtpEND.Date);
      sDB := 'Database: "' + trim(uppercase(cbDB.Text)) + '"';

      if (InReportName = 'CTRYCITYFREQ') then
      begin
     // country           city          Freq
     // EditDCEMcountry   EditDCEMCity  editNumber
        MyList.Add('Client E-Mail list by Country City and Number of bookings');
        MyList.Add(sDB);
        MyList.Add(sPROCDT);
        MyList.Add('Country: "' + trim(uppercase(EditCountry.Text)) + '"');
        MyList.Add('City: "' + trim(uppercase(EditCity.Text)) + '"');
        MyList.Add('Frequency: "' + trim(uppercase(editNumber.text)) + '"');
      end
      else if (InReportName = 'DAYSBETWEENPDPU') then
      begin
     // country           city          days
     // EditLMCountry     EditLMCity    EditLMDays
        MyList.Add('Last Minutes by Country City and Days Between Process and PU Date');
        MyList.Add(sDB);
        MyList.Add(sPROCDT);
        MyList.add('Country: "' + trim(uppercase(EditCountry.Text)) + '"');
        MyList.Add('City: "' + trim(uppercase(EditCity.text)) + '"');
        MyList.Add('Days Between: "' + trim(uppercase(EditLMDays.Text)) + '"');
      end
      else if (InReportName = 'FUTUREPU') then
      begin
     // country           city          pick up st      pick up end
     // EditFPUCTRY       EditFPUCITY   dtp_FPU_Start   dtp_FPU_END
        MyList.Add('Future Bookings by Country City and PickUp Date Range');
        MyList.Add(sDB);
        MyList.Add(sPROCDT);
        MyList.add('Country: "' + trim(uppercase(EditCountry.Text)) + '"');
        MyList.Add('City: "' + trim(uppercase(EditCITY.Text)) + '"');
        MyList.Add('Pick Up Start: "' + FormatDateTime('MMM DD YYYY', dtp_FPU_Start.Date) + '"');
        MyList.Add('Pick Up End: "' + FormatDateTime('MMM DD YYYY', dtp_FPU_END.Date) + '"');
      end;
    except result := '';
    end;
    result := MyList.Text;
  finally
    if assigned(MyList) then FreeAndNil(MyList);
  end;
end;

procedure TForm1.btnAGEClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  try
    StrTOInt(editAGELow.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try editAGELow.SetFocus; except; end;
      exit;
    end;
  end;

  try
    StrTOInt(editAGEHigh.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try editAGEHigh.SetFocus; except; end;
      exit;
    end;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'ByAGE';
  addtorunlog(btnage);

  theSQL := 'Select gc_system DB_system, voiamt4 age, gc_email client_email, vopdate1 PU_DT, ';
  theSQL := theSQL + ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());

  if cbFuturesOnly.Checked then
    stradd(theSQL, 'and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" '); // invalid if not updated after feed.
                                         // must update to "H" for lower release numbers
  stradd(theSQL, 'and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');

 // voiamt4 smallint = client Age
  stradd(theSQL, 'and voiamt4 between ' + editAGELow.text + ' and ' + editAGEHigh.text + ' ');
  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);
  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  OpenQ();
end;

procedure TForm1.btnCSVClick(Sender: TObject);
begin
  DoOutPut(BtnCSV, 'CSV', GlobFName);
end;

procedure TForm1.btnDurationGOClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  if trim(EditCountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  try
    StrTOInt(editDurationLow.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try editDurationLow.SetFocus; except; end;
      exit;
    end;
  end;

  try
    StrTOInt(editDurationHigh.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try editDurationHigh.SetFocus; except; end;
      exit;
    end;
  end;

  initQ();
  theSQL := '';
  GlobFName := 'ByDuration';
  addtorunlog(btnDurationGO);

  theSQL := 'Select gc_system DB_system, (vopdate2-vopdate1) duration, gc_email client_email, vopdate1 PU_DT,  ';
  theSQL := theSQL + ' trim(gc_lastname) || ", " || trim(gc_firstname) c_name,  vcnumb || "-" || vchrel voucher ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + 'From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(EditCountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(EditCountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());

  if cbFuturesOnly.Checked then
    stradd(theSQL, ' and vopdate1 >= current ');

  // join
  stradd(theSQL, ' and vcntype = "A" '); // invalid if not updated after feed.
                                         // must update to "H" for lower release numbers

  stradd(theSQL, ' and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');
  stradd(theSQL, ' and vopdate2-vopdate1 between ' + editdurationlow.Text + ' and ' + editdurationhigh.text);


  q1.SQL.Text := theSQL;

  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);

  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);

  OpenQ();
end;

procedure TForm1.OpenFile(InFIleName: string);
var buttonSelected: INteger;
begin
  if FileExists(inFILENAME) then
  begin
    buttonSelected := MessageDlg('Would you like to open the file?',
      mtConfirmation, mbYesNoCancel, 0);

    if buttonSelected = mrYes then
    begin
      try
        ShellExecute(handle,
          'Open',
          PChar(inFILENAME),
          nil,
          nil,
          SW_SHOWNORMAL);
      except on e: exception do
        begin
          showmessage(e.message);
        end;
      end;
    end;
  end
  else
  begin
    showmessage(inFILENAME + ' not found.');
  end;
end;

function TForm1.InitQ: boolean;
begin
  result := false;
  if not conn.Connected then
  begin
    try
      connect1Click(nil);
    except on e: exception do
      begin
        showmessage('please connect first.');
        exit;
      end;
    end;
  end;

  try
    q1.Close;
    q1.SQL.Clear;
    datasource1.DataSet := Q1; // this is set to InMem - in ByCarType Speciatly (Max's Requested report)
    result := true;
  except;
  end;
  memo1.Clear;
end;

procedure TForm1.JScroll(DataSet: TDataSet);
begin
  inc(xxxxx);
  sb.Panels[2].Text := 'processing: ' + IntToStr(xxxxx);
  application.ProcessMessages;
end;

procedure TForm1.memAfterScroll(DataSet: TDataSet);
begin
  JScroll(DataSet);
end;

procedure TForm1.OpenQ();
var DREC: TDurationRec;
begin
  btnEmailSend.Enabled := false;
  xxxxx := 0;

  try
    starttime := GetTickCount();
    OpenQuery(Q1); // handle the cursor
    endtime := GetTickCount();
    DREC := DwordToDuration(ENDtime - Starttime);

    if drec.hours > 0 then
      sb.panels[1].text := 'Hrs: ' + IntToStr(drec.hours) + ' Min: ' + IntToStr(drec.minutes) +
        ' Sec: ' + IntToStr(drec.seconds)
    else
      sb.panels[1].text := 'Min: ' + IntToStr(drec.minutes) + ' Sec: ' + IntToStr(drec.seconds);

    sb.Panels[2].Text := 'get data = OK';

    if not (Q1.IsEmpty) then
    begin
      pagecontrol1.ActivePage := tabdata;
      form1.FormStyle := fsStayOnTop;
      application.processmessages;
      form1.FormStyle := fsNormal;
    end
    else
      showmessage('no data returned.');

    application.processmessages;
  except on e: exception do
      showmessage('Failed to open. Err -> ' + E.Message);
  end;
end;

procedure TForm1.SetMem(InGlobFName: string);
begin
  // close, Clear Field Defs, add field defs by name
  InGlobFName := trim(uppercase(InGlobFName));
  try
    Mem.EmptyTable;
  except;
  end;

  try
    mem.Close;
    mem.FieldDefs.Clear;

    if (InGlobFName = 'BYCARTYPE_SPEC_SIPP_CXLS') or (InGlobFName = 'BYCARTYPE_SPEC_SIPP_ALL') then
    begin
      mem.FieldDefs.Add('basesys', ftString, 5, true);
      mem.FieldDefs.Add('voucher', ftString, 20, false);
      mem.FieldDefs.Add('confirmation', ftString, 40, false);
      mem.FieldDefs.Add('date_booked', ftDate, 0, false);
      mem.FieldDefs.Add('date_of_svc', ftDate, 0, false);
      mem.FieldDefs.Add('time_of_svc', ftString, 25, true);
      mem.FieldDefs.Add('retail_total', ftFloat, 0, true);
      mem.FieldDefs.Add('opdue_curr', ftFloat, 0, true);
      mem.FieldDefs.Add('curr_type', ftString, 5, false);
      mem.FieldDefs.Add('curr_exch_rate', ftFloat, 0, false);
      mem.FieldDefs.Add('sipp', ftString, 10, false);
      mem.FieldDefs.Add('date_of_transaction', ftDate, 0, false);

      // adding more 9/18 - they keep asking for "just one more field", so add a lot....
      mem.FieldDefs.Add('commission', ftFloat, 0, false);
      mem.FieldDefs.Add('profit', ftFloat, 0, false);
      mem.FieldDefs.Add('duration', ftFloat, 0, false);
      mem.FieldDefs.Add('iata', ftstring, 9, false);
      mem.FieldDefs.Add('country_pu', ftstring, 3, false); //vopctry1
      mem.FieldDefs.Add('city_pu', ftstring, 20, false); // vopcity1
      mem.FieldDefs.Add('loc_pu', ftstring, 2, false); // voploc1
      mem.FieldDefs.Add('operator', ftstring, 20, false); // vopid


      if paramstr(2) > '' then
        mem.FieldDefs.Add('consortium', ftString, 10, false);


    end
    else
    begin
      // add more stuff later
    end;

    mem.CreateTable; // required
    mem.Open;
  except on e: Exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

procedure Tform1.OpenQAndFillMem(InGlobFName: string);
var DREC: TDurationRec;

  procedure plug(InError: string);
  begin
    memo1.Lines.add(InError);
    mem.fields[0].AsString := 'sys?';
    mem.fields[1].AsString := 'voucher?';
    mem.fields[2].AsString := InError;
    mem.fields[3].AsDateTime := trunc(strtodatetime('11/27/1942')); // birth of Jimi Hendrix
    mem.fields[4].AsDateTime := trunc(strtodatetime('09/18/1970')); // death of Jimi Hendrix
    mem.fields[5].AsString := '00:00:00';
    mem.fields[6].AsFloat := 0.0;
    mem.fields[7].AsFloat := 0.0;
    mem.fields[8].AsString := 'curr?';
    mem.fields[9].AsFloat := 0.0;
    mem.fields[10].AsString := 'sipp?';
    mem.fields[11].AsDateTime := trunc(strtodatetime('09/18/1970')); // death of Jimi Hendrix

    // new crap 9/17
    mem.fields[12].AsFloat := 0.00;
    mem.fields[13].AsFloat := 0.00;
    mem.fields[14].asFloat := 0.00;
    mem.fields[15].AsString := '?? IATA ??';
    mem.fields[16].AsString := '??';
    mem.fields[17].AsString := 'city?';
    mem.fields[18].AsString := '??';
    mem.fields[19].AsString := '?oper?';

    if paramstr(2) > '' then try mem.fields[20].AsString := 'consort?'; except; end;
  end;
begin
  btnEMAILSend.Enabled := false;
  InGlobFName := trim(uppercase(InGlobFName));
  Q1.DisableControls;
  mem.DisableControls;
  xxxxx := 0;
  try
    try
      starttime := GetTickCount();
      OpenQuery(Q1); // handle the cursor
      Q1.First;
      SetMem(InGLobFName);

      while not Q1.eof do
      begin // fill the mem table
        if (InGlobFName = 'BYCARTYPE_SPEC_SIPP_CXLS') or (InGlobFName = 'BYCARTYPE_SPEC_SIPP_ALL') then
        begin
          try
            mem.Insert;
            if not DSC.Execute then
            begin
              plug(dsc.ErrorMessage);
            end
            else
            begin
              mem.fields[0].AsString := Q1.FieldByName('basesys').AsString;
              mem.fields[1].AsString := Q1.FieldByName('vcnumb').AsString + '-' + Q1.FieldByName('vchrel').AsString;
              mem.fields[2].AsString := Q1.FieldByName('vccconfirm').AsString;
              mem.fields[3].AsDateTime := trunc(Q1.FieldByName('vchdate').AsDateTime);
              mem.fields[4].AsDateTime := trunc(Q1.FieldByName('vopdate1').AsDateTime);
              mem.fields[5].AsString := FormatDateTime('HH:MM:SS', frac(Q1.FieldByName('voptime1').AsDateTime));
              mem.fields[6].AsFloat := RoundTo(dsc.RetailTot,-2);
              mem.fields[7].AsFloat := RoundTo(dsc.OperatorDueCurr,-2);
              mem.fields[8].AsString := Q1.FieldByName('vopcurrency').AsString;
              mem.fields[9].AsFloat := RoundTo(dsc.ExchangeRate,-3);
              mem.fields[10].AsString := Q1.FieldByName('voprd1').AsString; // sipp
              mem.fields[11].AsDateTime := trunc(Q1.FieldByName('vchpqbath').AsDateTime);
              // new crap 9/17 RoundTo(1.234, -2)
              mem.fields[12].AsFloat := RoundTo(dsc.Comm,-2);
              mem.fields[13].AsFloat := RoundTo(dsc.ProfitAComm,-2);
              mem.fields[14].asFloat := RoundTo(dsc.NumberDays,-2);
              mem.fields[15].AsString := dsc.Iata;
              mem.fields[16].AsString := dsc.CountryPU;
              mem.fields[17].AsString := dsc.CityPU;
              mem.fields[18].AsString := dsc.LocPU;
              mem.fields[19].AsString := dsc.OperatorPU;

              if paramstr(2) > '' then
              try mem.fields[20].AsString := Q1.FieldByName('voconsort').AsString; except; end;

            end;
            mem.Post;
          except on e: exception do
              plug(e.Message);
          end;
        end;
        Q1.Next;
      end;

      endtime := GetTickCount();
      DREC := DwordToDuration(ENDtime - Starttime);

      sb.panels[1].text := 'Min: ' + IntToStr(drec.minutes) +
        ' Sec: ' + IntToStr(drec.seconds);

      sb.Panels[2].Text := 'data retrieved OK';

      if not (mem.IsEmpty) then
      begin
        datasource1.DataSet := mem;
        Mem.SortOn('basesys;voucher;', mem.SortOptions);
        pagecontrol1.ActivePage := tabdata;
        form1.FormStyle := fsStayOnTop;
        application.processmessages;
        form1.FormStyle := fsNormal;
      end
      else
        showmessage('no data returned.');

      application.processmessages;
    except on e: exception do
        showmessage('Failed to open. Err -> ' + E.Message);
    end;
  finally
    Q1.EnableControls;
    mem.EnableControls;
  end;
end;


procedure TForm1.ppDetailBand1BeforeGenerate(Sender: TObject);
var temp: TDateTime;
begin
  temp := q1.FieldByName('dtr_run_date').AsDateTime;
  if temp >= LatestRunDate then
  begin
    LatestRunDate := temp;

    lblLast.Caption := 'Last run was ' +
      q1.FieldByName('dtr_user_name').AsString + ' on ' +
      FormatDateTime('mm/dd/yyyy', LatestRunDate) + ' at ' +
      FormatDateTime('HH:MM AM/PM', LatestRunDate);
  end;
end;

procedure TForm1.Q1AfterScroll(DataSet: TDataSet);
begin
  JScroll(DataSet);
end;

procedure TForm1.Q1BeforeOpen(DataSet: TDataSet);
begin
  sb.Panels[2].Text := 'getting data. please wait.';
  application.ProcessMessages;
end;

procedure TForm1.reportTrackBeforePrint(Sender: TObject);
begin
  lblDate.Caption := FormatDateTime('MM/DD/YYYY', dtpTrackReportsStart.dateTime) + ' to ' +
    FormatDateTime('MM/DD/YYYY', dtpTrackReportsEnd.dateTime);
end;

procedure TForm1.sheetCarTypeShow(Sender: TObject);
begin
  if Form1.Height < 432 then Form1.Height := 432;
  if form1.Width < 449 then form1.Width := 449;
  application.ProcessMessages;
end;

procedure TForm1.sheetPayTypeShow(Sender: TObject);
begin
  if Form1.Height < 344 then Form1.Height := 344;
  application.ProcessMessages;
end;

procedure TForm1.tabDataShow(Sender: TObject);
begin
  if datasource1.DataSet = Q1 then
  begin
    jonlbl.caption := 'Query';
  end
  else if datasource1.DataSet = mem then
  begin
    jonlbl.caption := 'InMem';
  end;
end;

procedure TForm1.tabStep1Hide(Sender: TObject);
begin
  UpdateLabels;
end;

function Tform1.GetDB_arg(): string;
begin
  result := '';
  // base system USA, CANA, ALL etc
  if (trim(uppercase(paramstr(1))) = 'AUS') or (trim(uppercase(paramstr(1))) = 'NEWZ') then
  begin
    if trim(uppercase(cbDB.text)) <> 'ALL' then
      result := ' and basesys = "' + trim(uppercase(cbdb.text)) + '" '
    else
      result := ' and basesys in ("AUS","NEWZ") ';
  end
  else if (trim(uppercase(paramstr(1))) = 'UK') or (trim(uppercase(paramstr(1))) = 'EURO') then
  begin
    if trim(uppercase(cbDB.text)) <> 'ALL' then
      result := ' and basesys = "' + trim(uppercase(cbdb.text)) + '" '
    else
      result := ' and basesys in ("EURO","UK") ';
  end
  else
  begin
    if trim(uppercase(cbDB.text)) <> 'ALL' then
      result := ' and basesys = "' + trim(uppercase(cbdb.text)) + '" ';
  end;
end;

procedure TForm1.btnFreqCTRYCITYClick(Sender: TObject);
var theSQL: string;
  i: integer;
begin
  if trim(Editcountry.text) = '' then
  begin
    showmessage('enter a Country Code.');
    FocusCtry();
    exit;
  end
  else if trim(EditCity.Text) = '' then
  begin
    showmessage('enter a city.');
    FocusCity();
    exit;
  end;

  try
    StrTOInt(editNumber.text);
  except on e: exception do
    begin
      showmessage(e.Message);
      try editNumber.SetFocus; except; end;
      exit;
    end;
  end;

  GlobFName := 'Frequency';
  addToRunLog(btnFreqCTRYCITY);
  initQ();
  theSQL := '';

  theSQL := 'Select gc_system DB_system, gc_email client_email, count(*) rentals ';
  if paramstr(2) > '' then theSQL := theSQL + ', voconsort consortium ';
  theSQL := theSQL + ' From vouchandqueue, g_client where vchpqbath between :date1 and :date2 ';

  if trim(Editcountry.Text) <> '*' then
    stradd(theSQL, ' and vopctry1 = "' + trim(UpperCase(Editcountry.Text)) + '" ');

  if trim(EditCity.Text) <> '*' then
    stradd(theSQL, ' and vopcity1 = "' + trim(UpperCase(EditCity.Text)) + '" ');

  stradd(theSQL, GetDB_arg());
  stradd(theSQL, GetClientArg());
  stradd(theSQL, GetConsortArg());
  // join
  stradd(theSQL, ' and vcntype = "A" and gc_system = basesys and gc_voucher = vcnumb ');
  stradd(theSQL, ' and gc_release = vchrel ');

  if paramstr(2) > '' then stradd(theSQL, 'group by 1,2, 4  having count(*) >= ' + editNumber.Text)
  else stradd(theSQL, 'group by 1,2  having count(*) >= ' + editNumber.Text);

  q1.SQL.Text := theSQL;
  q1.ParamByName('date1').AsDateTime := trunc(dtpStart.DateTime);
  q1.ParamByName('date2').AsDateTime := trunc(dtpEnd.DateTime);
  memo1.Lines.Text := theSQL;
  memo1.Lines.Add(q1.Params[0].asstring);
  memo1.Lines.Add(q1.Params[1].asstring);
  OpenQ();
end;

procedure TForm1.cbDBExit(Sender: TObject);
begin
  if not ThrottleDB() then
  begin
    cbDB.Text := '';
    showmessage('Please Select From what is in the List.');
    cbdb.SetFocus;
    cbdb.DroppedDown := true;
  end;
end;

procedure TForm1.connect1Click(Sender: TObject);
begin
  if conn.Connected then try conn.Close; except; end;

  try
    Set_Idac_Connection(conn, 'WHSE');
    conn.Open;
    sb.Panels[0].Text := 'OK: ' + GetCurrentDB(conn);
  except on e: exception do
    begin
      showmessage('Failed to connect: ' + e.message);
    end;
  end;
end;

procedure TForm1.disconnect1Click(Sender: TObject);
begin
  try
    conn.close;
    sb.Panels[0].Text := 'disconnected';
  except on e: exception do
    begin
      showmessage('Failed to connect: ' + e.message);
    end;
  end;
end;

procedure TForm1.EditCityExit(Sender: TObject);
begin
  EditCity.Text := trim(UpperCase(EditCity.Text));
end;

procedure TForm1.EditCountryExit(Sender: TObject);
begin
  EditCountry.Text := trim(UpperCase(EditCOuntry.Text));
end;

procedure TForm1.EMAIL(sFILENAME: string);
var errstr: string;
begin
  if (editToMail.Text = '') then
  begin
    showmessage('Please fill in the "To" field.');
    editToMail.SetFocus;
    exit;
  end
  else if (EditFrom.Text = '') then
  begin
    showmessage('Please fill in the "From" field.');
    EditFrom.SetFocus;
    exit;
  end;

  errStr := '';
  if not (IsValidEMailAddr(editToMail.Text, errStr)) then
  begin
    showmessage('Invalid "to" --> ' + errStr);
    EditToMail.SetFocus;
    exit;
  end;

  errStr := '';
  if not (IsValidEMailAddr(editFrom.Text, errStr)) then
  begin
    showmessage('Invalid "from" --> ' + errStr);
    EditFrom.SetFocus;
    exit;
  end;

  if not (fileexists(sFILENAME)) then
  begin
    showmessage('Can not find ' + sFILENAME + ' = E-mail Failure.');
    exit;
  end;

  Msg.Clear;
  try
    TIdAttachmentFile.Create(Msg.MessageParts, sFILENAME);
  except on e: exception do
    begin
      showmessage('Failed to attach file.');
      exit;
    end;
  end;

  with Msg do
  begin
    Body.Add('Attached is your Report.');
    From.Text := trim(EditFrom.text);
    Recipients.EMailAddresses := trim(editTomail.text);
    Subject := 'AE Data Store reports';
    Priority := TIdMessagePriority(mpNormal);
  end;

  try
    SMTP.Connect;
  except on e: exception do
    begin
      showmessage('failed to connect to server. --> ' + e.Message);
      exit;
    end;
  end;

  try
    try
      SMTP.Send(Msg);
      showmessage('e-mail Sent.');
    except on e: exception do
      begin
        showmessage(e.message + ': Failed to send e-mail.');
      end;
    end;
  finally
    SMTP.Disconnect;
  end;
end;

procedure TForm1.UpdateLabels;
begin
  lblDates.Caption := 'Dates: ' + DateTOStr(dtpStart.date) + '  to  ' + DateTOStr(dtpEnd.date); // FormatDateTime('MM-DD-YYYY', dtpStart.dateTime );
  lblSYSTEM.caption := 'DB: "' + cbDB.text + '"';
  lblshit.caption := 'Ctry: "' + EditCountry.text + '" ' + 'City: "' + EditCity.Text + '"';
  if cbFuturesOnly.checked then blbFUtures.caption := 'Future PU Only'
  else blbFUtures.caption := 'not(Future PU Only)';
end;

procedure TForm1.FillDBS();
var MyStringList: TStringList;
  I: Integer;
  temp: string;
begin
  if cbdb.Items.Count > 0 then cbDb.Items.Clear;
  cbDB.Items.Add('ALL');

  if (trim(uppercase(paramstr(1))) = 'AUS') or (trim(uppercase(paramstr(1))) = 'NEWZ') then
  begin
    cbDB.Items.Add('AUS');
    cbDB.Items.Add('NEWZ');
  end
  else if (trim(uppercase(paramstr(1))) = 'UK') or (trim(uppercase(paramstr(1))) = 'EURO') then
  begin
    cbDB.Items.Add('EURO');
    cbDB.Items.Add('UK');
  end
  else
  begin
    try
      try
        MyStringList := TStringList.Create;
        Get_DBList(MyStringList);

        for I := 0 to MyStringList.Count - 1 do
        begin
          temp := UpperCase(MyStringList[i]);
          if not (
            (temp = 'TEST') or
            (temp = 'TESTGLOBAL') or
            (temp = 'GLOBAL') or
            (temp = 'HIST') or
            (temp = 'EAV') or
            (temp = 'QUAL') or
            (temp = 'MKTG') or
            (temp = 'ARCH') or
            (temp = 'WHSE') or
            (temp = 'DEVLANGDATA') or
            (temp = 'ACCT') or
            (temp = 'AUSACCT') or (temp = 'POPTART') or
            (temp = 'WEBDATA')
            ) then cbDb.Items.add(temp);
        end;
        cbDB.Sorted := true;
      except on e: exception do
        begin
          showmessage('Fill List of Databases Err --> ' + e.Message);
        end;
      end;
    finally
      MyStringList.Free;
    end;
  end; // not aus
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  GetSupportingFiles('\\fileserver\UPDATE\idac.ini', 'c:\ae\idac.ini');

  dtpStart.Date := now - 30;
  dtpEnd.Date := now;
  dtp_FPU_Start.Date := now;
  dtp_FPU_END.Date := now + 14;
  dtpTrackReportsStart.Date := now - 30;
  dtpTrackReportsEnd.Date := now;
  sb.Panels[0].Text := 'disconnected';
  globEMLFileName := '';
  UpdateLabels;
  FIllDBS(); // locks aus, UK, EURO
  GlobFName := '';
  height := constraints.minHeight;
  width := constraints.minwidth;

  if not (GetTheUserName() = 'CHESTER') then
  begin
    tabForbiddenZone.TabVisible := false;
    sheetReportsOther.TabVisible := false;
    jonLbl.Visible := false;
  end;

  pagecontrol2.ActivePage := TabFreq;
  pagecontrol1.ActivePage := tabStep1;
  // to lock dow for DTAG or any other consortium for that matter:
  // MarketingReps.exe DB ConsortCode
  // MarketingReps.exe UK DTAG --> should only show records for voconsort "DTAG"
  // for AUS : lock to AUS and NEWZ: "MarketingReps.exe AUS" or "MarketingReps.exe NEWZ"
  if paramstr(2) <> '' then
    lblConsort.Caption := 'Consortium Code: ' + paramstr(2);
end;



end.

