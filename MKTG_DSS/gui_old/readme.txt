Top level filters:

Source Database / ALL or single
Batch date range
~future PU only switch
Country OR * (ALL)
City OR *  

Lower level filters (top level applicable):

** words surrounded in "%" specify Input Variables **

booking frequency: 
input:    	>= %integer% 
output: 	source system - client Email - # Rentals

Pick Up Date:
input: 		PU Date Range
output:		source system, client e-mail, PU Ctry, PU City, PU Date, Client Name,
		voucher-release

Last Minute:
input:		<= %integer% days between PD and PU Dates
output:		source system, client e-mail, paid Date, PU date, days between PD&PU,
		client name, voucher-release

AGE:
input:		age between %integer% and %integer%
output:		source system, age, client e-mail, pu date, client name, voucher-release

Pay Type:
input:		check mark select box per pay type
output:		source system, pay Type Id, client e-mail, PU date, client name, voucher-release


Duration:	
input:		Duration between %integer% and %integer%
output:		source system, duration, client e-mail, PU date, client name, voucher-release

Car Type:
input:		check mark select box for each of fist 4 positions of SIPP
output:		source system, SIPP, client e-mail, PU date, client name, voucher-release

Gender:
input:		Male / Female switch
output:		source system, Gender (Mr. Mrs. Ms.), client e-mail, client name, 
		voucher-release, PU Date


Output Types Supported: CSV OR XML File
					






