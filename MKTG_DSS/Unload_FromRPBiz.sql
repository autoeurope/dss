unload to AIR_2006 (

select "AIR" dbKey , 
	clvlast lname, 
	clvfirst fname, 
	clemail email,
        vcnumb voucher, 
	vchrel release, 
	vccrcard ccnum, 
	vcccmonth expdate,
        vopcity1 pucity, 
        vopid operator, 
        vchpqbath batchdate, 
	vopdate1 pudt,
        vopdate2 dropdt, 
	voprd1 sipp

from vouch, vchpqueue, vclient
where vchpqbath between Date('01/01/2006') and date('09/30/2006')
and vcnumb = vchpqvch
and vchrel = vchpqvchln
and vcntype = 'A'
and clvchnum = vcclientID
and vchpqtype <> 'p'
and vchpqtype <> 'm'
and  vopsubt <> 'T'
and VOPSTYPE1 = 'A'     
and (
(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N'))
);


unload to HOT_2006 (

select "HOT" dbKey , 
	clvlast lname, 
	clvfirst fname, 
	clemail email,
        vcnumb voucher, 
	vchrel release, 
	vccrcard ccnum, 
	vcccmonth expdate,
        vopcity1 pucity, 
        vopid operator, 
        vchpqbath batchdate, 
	vopdate1 pudt,
        vopdate2 dropdt, 
	voprd1 sipp

from vouch, vchpqueue, vclient
where vchpqbath between Date('01/01/2006') and date('09/30/2006')
and vcnumb = vchpqvch
and vchrel = vchpqvchln
and vcntype = 'A'
and clvchnum = vcclientID
and vchpqtype <> 'p'
and vchpqtype <> 'm'
and  vopsubt <> 'T'
and (VOPSTYPE1 <> 'A' and ( length(vopstype1) = 0) OR vopstype1 is null  )    
and (
(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N'))
);



//*************************** HOHO


unload to HOHO (

select "HOHO" dbKey , 
	clvlast lname, 
	clvfirst fname, 
	clemail email,
        vcnumb voucher, 
	vchrel release, 
	vccrcard ccnum, 
	vcccmonth expdate,
        vopcity1 pucity, 
        vopid operator, 
        vchpqbath batchdate, 
	vopdate1 pudt,
        vopdate2 dropdt, 
	voprd1 sipp

from vouch, vchpqueue, vclient
where vchpqbath between Date('01/01/2001') and date('12/31/2005')
and vcnumb = vchpqvch
and vchrel = vchpqvchln
and vcntype = 'A'
and clvchnum = vcclientID
and vchpqtype <> 'p'
and vchpqtype <> 'm'
and  vopsubt <> 'T'
    
and (
(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N'))
);


//***
unload to LAC (

select "LAC" dbKey , 
	clvlast lname, 
	clvfirst fname, 
	clemail email,
        vcnumb voucher, 
	vchrel release, 
	vccrcard ccnum, 
	vcccmonth expdate,
        vopcity1 pucity, 
        vopid operator, 
        vchpqbath batchdate, 
	vopdate1 pudt,
        vopdate2 dropdt, 
	voprd1 sipp

from vouch, vchpqueue, vclient
where vchpqbath between Date('01/01/2001') and date('12/31/2005')
and vcnumb = vchpqvch
and vchrel = vchpqvchln
and vcntype = 'A'
and clvchnum = vcclientID
and vchpqtype <> 'p'
and vchpqtype <> 'm'
and  vopsubt <> 'T'
    
and (
(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> 'N') and
(vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> 'N'))
);









