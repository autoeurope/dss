--create index "informix".idx_gc_email on "informix".g_client (gc_email)
  --  using btree ;
--create index "informix".idx_gc_firstname on "informix".g_client
  --  (gc_firstname) using btree ;
--create index "informix".idx_gc_lastfirst on "informix".g_client
  --  (gc_lastname,gc_firstname) using btree ;
--create index "informix".idx_gc_lastname on "informix".g_client
  --  (gc_lastname) using btree ;
--create index "informix".idx_gc_phone on "informix".g_client (gc_phone)
  --  using btree ;


--create index "informix".idx_gckey on 
--	"informix".g_client (gc_key)using btree ;

-- distributions on tables:
update statistics medium for table g_client distributions only;

-- for first field in each index:

update statistics high for table g_client(gc_phone); 
update statistics high for table g_client(gc_email); 
update statistics high for table g_client(gc_firstname); 
update statistics high for table g_client(gc_lastname);
update statistics high for table g_client(gc_key);