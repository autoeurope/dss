object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Get Product type etc Mod'
  ClientHeight = 310
  ClientWidth = 582
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    582
    310)
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 361
    Top = 279
    Width = 213
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'get_prod_desc_type_subType_source'
    TabOrder = 0
    OnClick = Button1Click
  end
  object DBGrid1: TDBGrid
    Left = 319
    Top = 8
    Width = 255
    Height = 120
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 134
    Width = 304
    Height = 139
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Fixedsys'
    Font.Style = []
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object Button2: TButton
    Left = 8
    Top = 279
    Width = 57
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'connect'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 71
    Top = 279
    Width = 42
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'open'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 119
    Top = 279
    Width = 51
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'loop'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 176
    Top = 279
    Width = 42
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'kill'
    TabOrder = 6
    OnClick = Button5Click
  end
  object memoSQL: TMemo
    Left = 8
    Top = 8
    Width = 305
    Height = 120
    Lines.Strings = (
      'Select basesys, vcnumb, vchrel, vopsubt, vopstype1 '
      'From vouchandqueue'
      'where basesys in ("EURO", "HOT")'
      'and vchpqbath >= date("01/01/2007")'
      ''
      ''
      '--Select distinct basesys, vopsubt, vopstype1 From vouchandqueue')
    TabOrder = 7
    WordWrap = False
  end
  object memoerr: TMemo
    Left = 319
    Top = 134
    Width = 256
    Height = 139
    Anchors = [akTop, akRight, akBottom]
    Lines.Strings = (
      'memoerr')
    ScrollBars = ssBoth
    TabOrder = 8
  end
  object DataSource1: TDataSource
    DataSet = sourceQ
    Left = 24
    Top = 200
  end
  object IfxConnection1: TIfxConnection
    Params.Strings = (
      'INFORMIXSERVER=ids_p570c'
      'WIN32HOST=p570c.aemaine.com'
      'WIN32PROTOCOL=onsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@ids_p570c')
    Left = 24
    Top = 136
  end
  object sourceQ: TIfxQuery
    Connection = IfxConnection1
    SQL.Strings = (
      
        'Select basesys, vcnumb, vchrel, vopsubt, vopstype1 From vouchand' +
        'queue'
      'where basesys = "EURO"')
    Left = 24
    Top = 168
  end
end
