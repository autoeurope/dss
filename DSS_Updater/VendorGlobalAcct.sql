dan's method:


This is it.  Works about 99% of the time.  Why it fails the other 1%, I don't know.
 
select vendor_vname, b.opctvendor
from g_opctr a, g_opctrinfotag b, g_operator c, apvenmast
where oper_code = vopid
 and opctoplink = opserial
 and opctryctry = vopctry1
 and opctprvid = vopprov1
 and opctrit_id = opctserial
 and ((opctrit_id = opctserial and opctrit_basesys = "KEM"
      or
     ((opctrit_id = opctserial and opctrit_basesys = "*" ) and not exists
      (select opctrit_id from g_opctrinfotag where
       opctrit_id = opctserial  and opctrit_basesys =  "KEM") )  )
 and ((vendor_group = vgroup and vendor = b.opctvendor)
 or (vendor_group = "MSTR" and vendor = b.opctvendor))



join from from vouch - get the global acct vendor number

Select opctvendor from  g_operator, g_opctr, g_opcity, g_oploc1, g_oploc1addr
where opserial=opctoplink and opctserial=opctctrylink and opcityserial=oplloccityid and oplactive = "y" and 
oplserial=opladkey1 
and oplserial=:voxtp1


SAF: example

Select opctvendor, oplserial, * from  g_operator, g_opctr, g_opcity, g_oploc1, g_oploc1addr
where opserial=opctoplink and opctserial=opctctrylink and opcityserial=oplloccityid and oplactive = "y" and 
oplserial=opladkey1 
and oplserial in(28718, 31166) -- vouch.voxtp1=oplserial

voucher    voxtp1

1048         28718
1088         31166

//*********************************************************

SELECT VCNUMB, VCHREL,
VOIT1RET,VOIT2RET,VOIT3RET,VOIT4RET,VOWD2, VOWD4,VOIT1WHL,VOIT2WHL,VOIT3WHL,VOIT4WHL,VOPWD6,VOPWD7,
VOPTAX2,VCNTYPE,VCIATA,VCCRCARD,VCRID,VOPAMT4,VCCOMTAKEN,VOPCURRENCY,VCCCMONTH,VCCLIENTID,
VCCOMMENTS,VCCCAUTHCODE,VCSTDTE1,VOPID,VOPID2,VOPCTRY1,VOPCTRY2,VOPCITY1,VOPCITY2,VOPLOC1,
VOPLOC2,VOPRD1,VOIAMT4,VCVENDORATTN,VOIAMT5,VOPAMT2,VORD6,VORD7,VORD8,VOPTAX1,VCPREPAY,
VCFPOVER1,VOXTP1W,VOXTP2W,VOXTP3W,VOXTP4W,VOPCURRATE,VORD3, VORD4,VOPPER1,VCDISADDAMOUNT,VCCRCARDID,
VCCASHDEPOSI,VOPDATE2,VOPDATE1,VOPTIME2,VOPTIME1,ALTRESNUM1, VOPPER3, VOPAMT5, VOPAMT6, VOIT1SW,
VOPAMT3, vccom2, voxtp1 
from vouch where vcnumb =:voucher and vchrel=:ver
