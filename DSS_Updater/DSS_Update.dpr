program DSS_Update;

uses
  Forms,
  main_update in 'main_update.pas' {FormDssFeed},
  DMUpdate in 'DMUpdate.pas' {UpdateDM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TUpdateDM, UpdateDM);
  Application.CreateForm(TFormDssFeed, FormDssFeed);
  Application.Run;
end.
