-- for each insert into vouchandqueue, insert a corresponding vendor record

drop table g_vendor;

create table g_vendor
  (
    gv_basesys char(4) not null ,
    gv_iata char(9) not null ,
    gv_class char(3),
    gv_name char(30),
    gv_legal_name char(30),
    gv_contact_name char(30),
    gv_addr1 char(30),
    gv_addr2 char(30),
    gv_addr3 char(30),
    gv_addr4 char(30),
    gv_city char(18),
    gv_prov char(3),
    gv_postal char(10),
    gv_country varchar(30),
    gv_phone_prefix char(6),
    gv_phone_num char(15),
    gv_fax_prefix char(6),
    gv_fax_num char(15),
    gv_email varchar(255),
    gv_consortium char(10)
  )extent size 35000 next size 3200;

revoke all on "informix".g_vendor from "public" as "informix";

create unique index "informix".sys_iata on "informix".g_vendor
    (gv_basesys,gv_iata) using btree ;
