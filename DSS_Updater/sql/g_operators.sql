create table g_operators_booked
(
    gob_basesys char(4) not null ,
    gob_operator_code varchar(20) not null
    
);

revoke all on g_operators_booked from "public";

create unique index ix_gob_sys_oper on 
g_operators_booked (gob_basesys,gob_operator_code);

insert into g_operators_booked 
Select distinct vs_basesys, vs_operator_code From g_voucher_summary
