create table g_voucher_summary
 (
    vs_basesys char(4) not null,
    vs_voucher integer not null,
    vs_release integer not null, -- not really relevent
    vs_active_inactive char(1) not null,
    vs_P_or_Q char(1) not null,
    vs_iata char(9) not null,
    vs_FP_PP char(2) not null,
    vs_plus_basic char(1) not null,
    vs_rid integer,
    vs_comments varchar(100),
    vs_auth_code char(10),
    vs_voucher_create_dt date not null,     -- vchdate 
    vs_operator_code varchar(20) not null,  -- vopid
    vs_pu_ctry char(2) not null, 
    vs_do_ctry char(2) not null,
    vs_pu_city varchar(50),
    vs_do_city varchar(50),
    vs_pu_loc char(2),
    vs_do_loc char(2),
    vs_sipp varchar(20),
    vs_sipp_type varchar(30), -- operator car group
    vs_sipp_description varchar(100),
    vs_updated_dt date not null, -- vcstdte1
    vs_pu_dt date not null,
    vs_do_dt date not null,
    vs_pu_time datetime year to second not null,
    vs_do_time datetime year to second not null,
    vs_client_age integer,    
    vs_num_passengers integer, 
    vs_duration decimal(10,2) default 0.0,	   
    vs_comm_tot decimal(10,2) default 0.0,
    vs_comm_due decimal(10,2) default 0.0,
    vs_exchange_rt decimal(8,4),
    vs_currency_operator char(3),
    vs_subtype char(2), 	-- vopsubt
    vs_home_curr char(3), 	--  vouch.homecurr
    vs_home_ctry char(2),      -- vouch.homectry

    vs_base_rate_retail decimal(10,2) default 0.0, -- vord4 or baserate
    vs_discount_retail decimal(10,2) default 0.0,
    vs_insurance_tot_retail decimal(10,2) default 0.0,
    vs_tax_tot_retail decimal(10,2) default 0.0,
    vs_pu_fee_retail decimal(10,2) default 0.0,
    vs_do_fee_retail decimal(10,2) default 0.0,
    vs_xtra_fee_retail decimal(10,2) default 0.0,

    vs_base_rate_wholesale decimal(10,2) default 0.0,
    vs_insurance_tot_wholesale decimal(10,2) default 0.0,
    vs_tax_tot_wholesale decimal(10,2) default 0.0,
    vs_pu_fee_wholesale decimal(10,2) default 0.0,
    vs_do_fee_wholesale decimal(10,2) default 0.0,
    
    vs_retail_total decimal(10,2) default 0.0,
    vs_wholesale_total decimal(10,2) default 0.0,
    vs_operator_due decimal(10,2) default 0.0,
    vs_operator_due_curr decimal(10,2) default 0.0,
    vs_profit decimal(10,2) default 0.0,
    vs_deposit_amt decimal(10,2) default 0.0,
    vs_waive_deposit decimal(10,2) default 0.0,   
    vs_deposit_after_waive decimal(10,2) default 0.0,
    vs_due_at_pu decimal(10,2) default 0.0,
    vs_cost decimal(10,2) default 0.0,    
    vs_deferred decimal(10,2) default 0.0, 

    vs_bank_deposit decimal(10,2) default 0.0,  -- vopamt1        
    
  --  vs_client_fname varchar(30),
   -- vs_client_lname varchar(30),
 --   vs_client_init char(1),
 --   vs_client_gender char(1),
 --   vs_client_addr1 varchar(60),
 --   vs_client_addr2 varchar(60),
 --   vs_client_city varchar(30),
 --   vs_client_prov char(10),
 --   vs_client_postal char(20),
 --   vs_client_phone varchar(40),
 --   vs_client_fax varchar(40),    	
 --   vs_client_email varchar(255), 
    vs_ta_email varchar(255),
        
    vs_comment varchar(100),
    vs_insert_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,
    vs_update_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,
    vs_upl_to_listmgr_dt date,
    vs_key serial not null    
  );

revoke all on g_voucher_summary from "public";


create unique index ix_avs_basesys_vouch on g_voucher_summary (vs_basesys, vs_voucher) using btree;
create index ix_basesys_clemail on g_voucher_client_summary (vs_basesys, vs_client_email) using btree; 
create index ix_basesys_tamail on g_voucher_client_summary (vs_basesys, vs_ta_email) using btree;






