create table "informix".csfiles
  (
    cs_basesys char(4) not null ,
    cs_refundamt decimal(16),
    cs_ranum char(50),
    cs_daterecvd date,
    cs_accesscount integer,
    cs_gendesc char(600),
    cs_filenum integer not null ,
    cs_res integer not null ,
    cs_rel integer not null ,
    cs_rid smallint not null ,
    cs_date_o date,
    cs_date_b date,
    cs_date_c date,
    cs_who_o char(50),
    cs_who_c char(50),
    cs_filestat char(2),
    cs_ctrycode char(2),
    cs_reasoncode char(2),
    cs_batchcount integer,
    cs_desc char(700),
    cs_clientid integer,
    cs_dbtag char(3),
    cs_reason char(30),
    cs_officetag char(10),
    cs_filetype char(3),
    cs_rqstd_refund decimal(16),
    cs_duedate datetime year to day
        default current year to day,
    cs_subtype char(3) default 'n/a',
    cs_subtype2 char(1),
    cs_ref varchar(25)
  );
revoke all on "informix".csfiles from "public" as "informix";


create index "informix".csf_stattype on "informix".csfiles (cs_basesys,cs_filestat,cs_filetype,cs_res,cs_rel) using btree ;

create unique index "informix".csf_sysresreltype on "informix".csfiles (cs_basesys,cs_res,cs_rel,cs_filetype) using btree;


--update statistics medium for table csfiles distributions only;
--update statistics high for table csfiles(cs_basesys);
--update statistics high for table csfiles(cs_res);
--update statistics low for table csfiles(cs_rel);
--update statistics low for table csfiles(cs_filetype);






-------------------------------------------------------------------------

create table "informix".cscomments
  (
    cscmnt_basesys char(4) not null ,
    cscmtname char(20),
    cscmtdatefax date,
    cscmtseq integer not null ,
    cscmtdate date not null ,
    cscmtridnum smallint not null ,
    cscmntresnum integer not null ,
    cscmtresrel integer not null ,
    cscmtcomment char(5000),
    cscmtfaxexists char(1),
    cscmtdocexists char(1),
    cscmtdbtag char(3),
    cscmntfiletype char(3),
    cscmntfileextention char(4)
  );
revoke all on "informix".cscomments from "public" as "informix";


create index "informix".csc_sysresreltype on "informix".cscomments (cscmnt_basesys,cscmntresnum,cscmtresrel,cscmntfiletype) using btree ;

create unique index "informix".csc_sys_serial on 
	"informix".cscomments (cscmnt_basesys, cscmtseq) using btree;


--update statistics medium for table cscomments distributions only;
--update statistics high for table cscomments(cscmnt_basesys);
--update statistics high for table cscomments(cscmntresnum);
--update statistics high for table cscomments(cscmtseq);
--update statistics low for table cscomments(cscmtresrel);
--update statistics low for table cscomments(cscmntfiletype);


--------------------------------------------------------------------------

create table "informix".csfilehistory
  (
    csfs_basesys char(4) not null ,
    csfs_csfilenum integer not null ,
    csfs_insertdtt datetime year to second default current year to second,
    csfs_filestat char(2) not null ,
    csfs_who char(20) not null ,
    batch_date date,
    csfs_filetype char(3) default 'CS',
    csfs_reason char(30),
    csfs_csduedate datetime year to day,
    csfs_csref varchar(25),
    csfs_rqstd_refund decimal(16)
  );
revoke all on "informix".csfilehistory from "public" as "informix";


create index "informix".csfileh_ixdt on "informix".csfilehistory
    (csfs_basesys,csfs_insertdtt) using btree ;

create index "informix".csfileh_ixfnumtype on "informix".csfilehistory
    (csfs_basesys,csfs_csfilenum,csfs_filetype) using btree ;



--update statistics medium for table csfilehistory distributions only;
--update statistics high for table csfilehistory(csfs_basesys);
--update statistics low for table csfilehistory(csfs_insertdtt);
--update statistics low for table csfilehistory(csfs_csfilenum);
--update statistics low for table csfilehistory(csfs_filetype);


---------------------------------------------------------------------------

create table "informix".cscontact
  (
    csclt_basesys char(4) not null ,
    csclt_vouch integer not null ,
    csclt_release integer not null ,
    csclt_lastname char(30),
    csclt_firstname char(30),
    csclt_initial char(1),
    csclt_addr1 char(60),
    csclt_addr2 char(60),
    csclt_addr3 char(60),
    csclt_city char(30),
    csclt_zip char(30),
    csclt_ctry char(4),
    csclt_email char(200),
    csclt_faxnum char(60),
    csclt_phonenum char(60),
    csclt_other2 char(60),
    csclt_other text,
    csclt_prov char(10),
    csclt_filetype char(3)
        default 'CS'
  );
revoke all on "informix".cscontact from "public" as "informix";


create unique index "informix".cscontact_sysresreltype on "informix"
    .cscontact (csclt_basesys,csclt_vouch,csclt_release,csclt_filetype)
    using btree ;

--update statistics medium for table cscontact distributions only;
--update statistics high for table cscontact(csclt_basesys);
--update statistics high for table cscontact(csclt_vouch);
--update statistics low for table cscontact(csclt_release);
--update statistics low for table cscontact(csclt_filetype);






