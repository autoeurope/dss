unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, DB, IfxCustomDataSet, IfxQuery,
  IfxConnection, StdCtrls, aecommonfunc_II;

type
  TForm1 = class(TForm)
    open: TButton;
    dtpBatch: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    conn: TIfxConnection;
    connGLobal: TIfxConnection;
    qSource: TIfxQuery;
    qGetVendor: TIfxQuery;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    thebar: TStatusBar;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    Qsummary: TIfxQuery;
    DataSource3: TDataSource;
    cbSystem: TComboBox;
    Button1: TButton;
    dtpBatchEnd: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure openClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    bkill: boolean;
    { Private declarations }
  public
    function GetOperatorVendor(VouchVOXTP1: Integer; InSys: string): string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{
create index "informix".sysbatchdtiata on "informix".vouchandqueue
    (basesys,vchpqbath,vciata) using btree ;

create index "informix".syspddtctrycityopersippahdtaconsort on
    "informix".vouchandqueue (basesys,vchpqbath,vopctry1,vopcity1,
    vopid,voprd1,vcntype,vciata,voconsort) using btree ;

create index "informix".syspickup on "informix".vouchandqueue
    (basesys,vopdate1) using btree ;

create unique index "informix".sysvchrel on "informix".vouchandqueue
    (basesys,vcnumb,vchrel) using btree ;
}


procedure TForm1.Button2Click(Sender: TObject);
var vendor: string;
begin
  if qSource.fieldbyname('VOXTP1').IsNull then exit;

  vendor := '';
  vendor := GetOperatorVendor(qSource.fieldbyname('VOXTP1').asInteger, qSource.fieldbyname('basesys').asstring);

  if vendor <> '' then
  begin
    try
      qsummary.Edit;
      qsummary.FieldByName('vs_opvend').AsString := vendor;
      qsummary.Post;
    except on e: exception do
      begin
        qsummary.Cancel;
        memo1.Lines.Add('failed edit --> ' + e.Message);
      end;
    end;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var i: Integer;
begin
  bkill := false;
  qsource.First;
  i := 0;

  while not qsource.Eof do
  begin
    if bkill then break;

    inc(i);
    if i >= 100 then
    begin
      application.ProcessMessages;
      i := 0;
    end;

    button2.Click();
    qsource.Next;
  end;

  if bkill then memo1.Lines.add('done - killed')
  else memo1.Lines.add('done');
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  bkill := true;
  application.ProcessMessages;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  bkill := false;
end;

function TForm1.GetOperatorVendor(VouchVOXTP1: Integer; InSys: string): string;
var theSQL: string;
  reccount: Integer;
begin
  result := '';
  InSys := trim(uppercase(InSys));
  if InSys = 'USA' then InSys := 'US';

  if not connGLobal.Connected then
  begin
    showmessage('global not connected');
    exit;
  end;

  theSQL := 'Select g_opctrinfotag.opctvendor, opctrit_basesys, oplactive ' +
    'from  g_operator, g_opctr, g_opcity, g_oploc1, g_oploc1addr, g_opctrinfotag ' +
    'where ' +
    'opserial=opctoplink and opctserial=opctctrylink and opcityserial=oplloccityid ' +
    'and oplserial=opladkey1 ' +
    'and oplserial = :voxtp1 ' +
    'and ((opctrit_id = opctserial and opctrit_basesys = :sys) ' +
    'or ' +
    '((opctrit_id = opctserial and opctrit_basesys = "*" ) and not exists ' +
    '(select opctrit_id from g_opctrinfotag where ' +
    'opctrit_id = opctserial  and opctrit_basesys =  :sys ))) ' +
    'order by 2 DESC, 3 DESC; ';

    {
      Select g_opctrinfotag.opctvendor, opctrit_basesys, oplactive, *
      from  g_operator, g_opctr, g_opcity, g_oploc1, g_oploc1addr, g_opctrinfotag
      where
      opserial=opctoplink and opctserial=opctctrylink and opcityserial=oplloccityid
      and oplserial=opladkey1
      and oplserial = 40991
      and ((opctrit_id = opctserial and opctrit_basesys = "US")
      or
      ((opctrit_id = opctserial and opctrit_basesys = "*" ) and not exists
      (select opctrit_id from g_opctrinfotag where
      opctrit_id = opctserial  and opctrit_basesys =  "US")))
      order by 2 DESC, 3 DESC;
    }

  try
    qGetVendor.close;
    qGetVendor.SQL.Clear;
    qGetVendor.sql.Add(theSQL);
    qGetVendor.ParamByName('voxtp1').AsInteger := VouchVOXTP1;
    qGetVendor.ParamByName('sys').AsString := InSys;
    qGetVendor.Active := true;
    if (qGetVendor.IsEmpty) then exit
    else
    begin
      reccount := qGetVendor.RecordCount;
      if reccount > 1 then
      begin
        memo1.Lines.add(IntToStr(reccount) + ' results for ' + intToStr(VouchVOXTP1) + ' ' + insys);
      end;

      qGetVendor.First;
      result := qGetVendor.Fields[0].AsString;
    end;
  except on e: exception do
    begin
      result := '';
      memo1.Lines.add('GetOperatorVendor fail voxtp1 [' + IntToStr(VouchVOXTP1) +
        '] system [ ' + InSys + '] --> ' + E.Message);
    end;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin // open button - open queries
  try
    qSummary.close;
    qSource.Close;
  except on e: exception do
    begin
      showmessage('close fail --> ' + e.Message);
      exit;
    end;
  end;
  {
  Select basesys, voxtp1, vcnumb, vchrel, vchpqbath From vouchandqueue
where basesys = :basesys and vchpqbath between :date1 and :date2
  }
  qsource.SQL.Clear;

  if (cbSystem.Text = 'ALL') then
  begin
    qsource.SQL.add('Select basesys, voxtp1, vcnumb, vchrel, vchpqbath From vouchandqueue');
    qsource.SQL.add('where basesys is not null and vchpqbath between :date1 and :date2');
  end
  else
  begin
    qsource.SQL.add('Select basesys, voxtp1, vcnumb, vchrel, vchpqbath From vouchandqueue ');
    qsource.SQL.add('where basesys = :basesys and vchpqbath between :date1 and :date2');
  end;

  try

    if (cbSystem.Text <> 'ALL') then
      qSource.ParamByName('basesys').AsString := cbSystem.Text;

    qSource.ParamByName('date1').AsDate := trunc(dtpBatch.Date);
    qSource.ParamByName('date2').AsDate := trunc(dtpBatchEnd.date);

    memo1.Lines.add('**** source SQL start ****');
    memo1.Lines.add(qSource.sql.text);

    if (cbSystem.Text <> 'ALL') then
      memo1.Lines.add('basesys [' + qSource.ParamByName('basesys').AsString + ']');

    memo1.Lines.add('date1 [' + qSource.ParamByName('date1').AsString + ']');
    memo1.Lines.add('date2 [' + qSource.ParamByName('date2').AsString + ']');
    memo1.Lines.add('**** source SQL end ****');

    qsource.Open;
  except on e: exception do
    begin
      showmessage('qSource.Open fail --> ' + e.Message);
      exit;
    end;
  end;

  try
    qSummary.Open;
  except on e: exception do
    begin
      showmessage('qSummary.Open fail --> ' + e.Message);
      exit;
    end;
  end;
end;

procedure TForm1.openClick(Sender: TObject);
begin // connect button - connect to systems
  try
    conn.Close;
    connGlobal.Close;
    set_idac_connection(conn, 'WHSE');
    set_idac_connection(connGLobal, 'GLOBAL');
    conn.Open;
    connglobal.Open;
    thebar.Panels[1].Text := 'target: ' + GetCurrentDB(conn) + '@' + GetCurrentHost(conn);
    thebar.Panels[0].Text := 'source: ' + GetCurrentDB(connGlobal) + '@' + GetCurrentHost(connGlobal);
  except on e: exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

end.

