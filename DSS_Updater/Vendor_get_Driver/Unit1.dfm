object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'update aewhse.g_voucher_summary.vs_opvend'
  ClientHeight = 428
  ClientWidth = 684
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    684
    428)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 13
    Width = 34
    Height = 13
    Caption = 'system'
  end
  object Label2: TLabel
    Left = 104
    Top = 13
    Width = 40
    Height = 13
    Caption = 'batch dt'
  end
  object Label3: TLabel
    Left = 344
    Top = 246
    Width = 100
    Height = 13
    Caption = 'g_voucher_summary'
  end
  object Label4: TLabel
    Left = 344
    Top = 93
    Width = 77
    Height = 13
    Caption = 'vocuhandqueue'
  end
  object Label5: TLabel
    Left = 8
    Top = 246
    Width = 65
    Height = 13
    Caption = 'q get_vendor'
  end
  object open: TButton
    Left = 207
    Top = 32
    Width = 58
    Height = 21
    Caption = 'connect'
    TabOrder = 0
    OnClick = openClick
  end
  object dtpBatch: TDateTimePicker
    Left = 103
    Top = 32
    Width = 97
    Height = 21
    Date = 40478.584770000000000000
    Time = 40478.584770000000000000
    TabOrder = 1
  end
  object thebar: TStatusBar
    Left = 0
    Top = 409
    Width = 684
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
  end
  object DBGrid1: TDBGrid
    Left = 344
    Top = 112
    Width = 328
    Height = 128
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 265
    Width = 321
    Height = 125
    Anchors = [akLeft, akTop, akBottom]
    DataSource = DataSource2
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 344
    Top = 265
    Width = 328
    Height = 125
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource3
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cbSystem: TComboBox
    Left = 8
    Top = 32
    Width = 89
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = 'ALL'
    Items.Strings = (
      'USA'
      'CANA'
      'KEM'
      'HOHO'
      'LAC'
      'SAF'
      'EURO'
      'UK'
      'HOT'
      'AUS'
      'NEWZ'
      'ALL')
  end
  object Button1: TButton
    Left = 207
    Top = 59
    Width = 58
    Height = 21
    Caption = 'open'
    TabOrder = 7
    OnClick = Button1Click
  end
  object dtpBatchEnd: TDateTimePicker
    Left = 104
    Top = 59
    Width = 97
    Height = 21
    Date = 40478.584770000000000000
    Time = 40478.584770000000000000
    TabOrder = 8
  end
  object Memo1: TMemo
    Left = 8
    Top = 112
    Width = 321
    Height = 128
    Lines.Strings = (
      'Memo1')
    TabOrder = 9
  end
  object Button2: TButton
    Left = 271
    Top = 32
    Width = 75
    Height = 23
    Caption = 'update 1'
    TabOrder = 10
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 271
    Top = 59
    Width = 75
    Height = 21
    Caption = 'update loop'
    TabOrder = 11
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 78
    Width = 34
    Height = 20
    Caption = 'kill'
    TabOrder = 12
    OnClick = Button4Click
  end
  object conn: TIfxConnection
    DesignConnection = True
    IsolationLevel = ilDirtyRead
    Left = 352
    Top = 8
  end
  object connGLobal: TIfxConnection
    DesignConnection = True
    IsolationLevel = ilDirtyRead
    Left = 448
    Top = 8
  end
  object qSource: TIfxQuery
    Connection = conn
    SQL.Strings = (
      
        'Select basesys, voxtp1, vcnumb, vchrel, vchpqbath From vouchandq' +
        'ueue'
      'where basesys = :basesys and vchpqbath between :date1 and :date2')
    Left = 352
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'date1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'date2'
        ParamType = ptInput
      end>
  end
  object qGetVendor: TIfxQuery
    Connection = connGLobal
    Left = 448
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = qSource
    Left = 352
    Top = 72
  end
  object DataSource2: TDataSource
    DataSet = qGetVendor
    Left = 448
    Top = 72
  end
  object Qsummary: TIfxQuery
    Connection = conn
    DataSource = DataSource1
    RequestLive = True
    SQL.Strings = (
      'select * from g_voucher_summary where'
      'vs_basesys = :basesys '
      'and vs_voucher = :vcnumb'
      'and vs_release = :vchrel'
      '')
    Left = 384
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vcnumb'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vchrel'
        ParamType = ptInput
      end>
  end
  object DataSource3: TDataSource
    DataSet = Qsummary
    Left = 384
    Top = 72
  end
end
