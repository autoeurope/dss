unit main_update;
{
select gc_system, gc_voucher, gc_surname, gc_email, * from vouchandqueue a, outer(g_client c)
where
c.gc_system = a.basesys and
c.gc_voucher = a.vcnumb and
c.gc_release = a.vchrel
and
not exists(
Select * From
g_voucher_summary b
where
b.vs_basesys = a.basesys and
b.vs_voucher = a.vcnumb and
b.vs_release = a.vchrel
)

// Aug 27 2008: Add fields for fraud tracking:
alter table vouchandqueue add( voxtp3t char(1) );

// you may want to do this once in a while, or at least whenever the
// product code table us updated in global ...

-- unload the table on aeglobal:
unload to g_prod.unl select * from g_prod_code_ref;
-- ftp the file to f50aus then do this:
unload to g_prodcrap.unl select * from g_prod_code_ref; -- save original
delete from g_prod_code_ref where 1=1;                  -- empty original
load from g_prod.unl insert into g_prod_code_ref;       -- load new values

01/01/2011 added IATA and dcmail updates
}

interface

uses
//standard modules
  Windows
  , Messages
  , SysUtils
  , Variants
  , Classes
  , Graphics
  , Controls
  , Forms
  , Dialogs
  , StdCtrls
  , DB
  , Grids
  , DBGrids
  , ComCtrls
  , DateUtils
  , ExtCtrls
  , DMUpdate

  //AE components
  , kbmMemTable
  , SippDescribe
  , DSCalc
  , AECommonFunc_II

  //Indy
  , IdComponent
  , IdTCPConnection
  , IdTCPClient
  , IdExplicitTLSClientServerBase
  , IdMessageClient
  , IdSMTPBase
  , IdSMTP
  , IdBaseComponent
  , IdMessage
  , IdAttachmentFile

  //Informix
  , IfxTable
  , IfxCustomDataSet
  , IfxQuery
  , IfxConnection;

type
  BizDateRec = record
    totalBizDaysBackDt,
      churnedDaysBackDt: TDate;
  end;

type
  TFormDssFeed = class(TForm)
    lblSource: TLabel;
    Memo1: TMemo;
    DBGrid1: TDBGrid;
    dtpStart: TDateTimePicker;
    DBGrid2: TDBGrid;
    dtpEnd: TDateTimePicker;
    lblBatchStart: TLabel;
    lblBatchEnd: TLabel;
    cbsourceparam: TComboBox;
    Memo2: TMemo;
    sbar: TStatusBar;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    Edit1: TEdit;
    btnFind: TButton;
    btnRefill: TButton;
    Edit2: TEdit;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    DBGrid7: TDBGrid;
    lblQClientUpdateDriver: TLabel;
    lblQClientSource: TLabel;
    lblTblTargetClient: TLabel;
    gbCS: TGroupBox;
    rgCSTasks: TRadioGroup;
    btnCSHistoryTasks: TButton;
    gbIATAUpdates: TGroupBox;
    rgIATATasks: TRadioGroup;
    btnIATATasks: TButton;
    gbDCMail: TGroupBox;
    rgdcMailTasks: TRadioGroup;
    btnDCMailTasks: TButton;
    gbDevelopment: TGroupBox;
    btnDevTasks: TButton;
    rgDevTasks: TRadioGroup;
    editTEST: TEdit;
    EditTEST2: TEdit;
    gbSummary: TGroupBox;
    rgSummaryTasks: TRadioGroup;
    btnSummaryTasks: TButton;
    gbClient: TGroupBox;
    rgClientTasks: TRadioGroup;
    btnClientTasks: TButton;
    gbVQ: TGroupBox;
    rgVQTasks: TRadioGroup;
    btnVQTasks: TButton;
    gbPQ: TGroupBox;
    rgPQTasks: TRadioGroup;
    Label2: TLabel;
    btnPQTasks: TButton;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure btnPQTasksClick(Sender: TObject);
    procedure btnVQTasksClick(Sender: TObject);
    procedure btnClientTasksClick(Sender: TObject);
    procedure btnSummaryTasksClick(Sender: TObject);
    procedure btnDevTasksClick(Sender: TObject);
    procedure btnDCMailTasksClick(Sender: TObject);
    procedure btnIATATasksClick(Sender: TObject);
    procedure btnCSHistoryTasksClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnRefillClick(Sender: TObject);

  private

    //development tools
    procedure Kill;
    procedure testRun;

    // cs methods:
    function GetLastCSUpdate(baseSYS: string): TDate;
    function AutoUpdateCS(sender, basesystem: string): string;
    function AutoUpdateCSFileHistory(sender, basesystem: string): string;
    function GetLastCSHistoryUpdate(baseSYS: string): TDate;
    
    procedure LastCSHistdate;
    procedure LastCSDate;
    procedure runCSByParams(IsAnError: Boolean; errorStr: string);

    // IATA refreshing
    function GetLatestUpdatedBatchForSystem(basesys: string): TDate; // bring dss more current than 5 days

    procedure refresh_rc_operators();
    procedure LoopUpdateIATAS();

    // new eml funcs starting 1/03/2011
    procedure GetDCMailDATA;
    procedure SetDCMDate;
    procedure AutoUpdtDCM;

    //client methods
    function GetLastGClientUpdate(baseSYS: string): TDate;
    function AutoUpdateGClient(basesystem: string): string;

    procedure GetLastClientUpdate;

    //PQ methods
    function AutoUpdateVQ(basesystem: string): string;

    procedure GetDatesForUpdate;

  public
    BzDateRec: BizDateRec; // global dates for churn args
    emlTO: String;
    paramList,BaseSysList,logList: TStringList;
    g_source: string;
    bKill: boolean;

    //utility methods
    function IsADC(InSystem, InIATA: string): boolean;
    function GetBizDates(WhseConn: TIfxConnection): BizDateRec;

    procedure SetBar0(InStr: string);
    procedure SetBar1(InStr: string);     
    procedure CS_Hist_CheckSums(dstart, dend: TDate; basesys: string);
    procedure runByParams(IsAnError: Boolean; errorStr: string);
  end;

var
  FormDssFeed: TFormDssFeed;

implementation

{$R *.dfm}


function TFormDssFeed.IsADC(InSystem, InIATA: string): boolean;
begin
  result := false;
  if UpdateDM.MemIATAWhiteList.Locate('basesys;vendor', VarArrayOf([InSystem, InIATA]), [loCaseInsensitive]) then
    result := true;
end;

function TFormDssFeed.GetLastCSUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(basesyslist, baseSYS)) then
  begin
    savelogfile('dss_upd', 'GetLastCSUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    UpdateDM.SetConn(UpdateDM.connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastCCUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'Select max(cs_date_c) From csfiles where cs_basesys = "' + baseSYS + '"';

  try
    try
      MyQuery := GetIDACQuery(UpdateDM.connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else result := trunc(MyQuery.Fields[0].AsDateTime);

    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLastCSUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;

procedure TFormDssFeed.SetBar0(InStr: string);
begin
  sbar.Panels[0].text := InStr;
end;

procedure TFormDssFeed.SetBar1(InStr: string);
begin
  sbar.Panels[1].text := InStr;
end;

function TFormDssFeed.AutoUpdateVQ(basesystem: string): string;
var ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';
  ErrorList := TStringList.create;

  try
    try
      MyDate := 0;
      date1 := 0;
      date2 := 0;
      MyDate := UpdateDM.GetLastVQUpdate(basesystem); // sets target connection active
                                             // also validates baseSystem
      if MyDate = 0 then
      begin
        ErrorList.add('AutoUpdateVQ: No date returned from GetLastVCUpdate(' + basesystem + ') *** aborted ***');
        result := errorList.Text;
        exit;
      end
      else
      begin
        date1 := Mydate + 1;

        //  date2 := now - daysback;
        // can update thru this date - i.e. latest batch completed by VP Department
        date2 := GetLatestUpdatedBatchForSystem(basesystem);

        if (date2 = 0) then
        begin
          ErrorList.add('AutoUpdateVQ: failed to get "update thru" date for system [' + basesystem + ']');
          result := errorlist.Text;
          exit;
        end;

       // if trunc(date2) <= trunc(date1) then date2 := date1;

      //  if (trunc(date2) >= now - daysback) then
      //  begin
      //    result := 'no need to update ' + basesystem + ' last update: ' + DateTOStr(Mydate);
      //    exit;
      //  end;

        if (trunc(date2) < trunc(date1)) then
        begin
          result := 'AutoUpdateVQ: No need to update system [' + basesystem + '] last updated: [' + DateTOStr(Date1) + ']';
          exit;
        end;
      end; // MyDate <> 0

      // should have valid values for basesystem, date1 and date2 here

      try // close and open source for feed
        UpdateDM.OpenSourceVQ(baseSystem, date1, date2);
      except on e: exception do
        begin
          ErrorList.add('system: ' + baseSystem + datetostr(date1) + ' to ' + datetostr(date2) + ' --> source connect fail --> ' + E.Message);
          exit;
        end;
      end;

      try
        memo2.Lines.add('debug system: ' + baseSystem + ' ' + datetostr(date1) + ' to ' + datetostr(date2));
      except;
      end;

      memo1.Clear;
      // this func adds errors to the memo -
      UpdateDM.VQCopyForDates(baseSystem, date1, date2);

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdateVQ: General exception --> ' + e.Message)
      end;
    end;

    if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
    end;
  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

function TFormDssFeed.AutoUpdateGClient(basesystem: string): string;
var ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';
  ErrorList := TStringList.create;
  MyDate := 0;
  date1 := 0;
  date2 := 0;
  // here
  try
    try
      MyDate := GetLastGClientUpdate(basesystem); // sets target connection active
                                                  // also validates baseSystem
      if MyDate = 0 then
      begin
        ErrorList.add('AutoUpdateGClient(): No date returned from GetLastGClientUpdate(' + basesystem + ') *** aborted ***');
        result := errorList.Text;
        exit;
      end
      else
      begin
        //date1 := Mydate + 1;
        date1 := MyDate - 14; // change 7/26/2011 - using OpenSourceClientForSync instead

        // date2 := now - daysback; // get date to update to - using vchbatch in the future
        // 11/01/2010 new method:
        // can update thru this date - i.e. latest batch completed by VP Department
        date2 := GetLatestUpdatedBatchForSystem(basesystem);

        if (date2 = 0) then
        begin
          ErrorList.add('AutoUpdateGClient(): failed to get "update thru" date from GetLatestUpdatedBatchForSystem(' + basesystem + ')');
          result := errorlist.Text;
          exit;
        end;

        if (trunc(date2) < trunc(date1)) then
        begin
          ErrorList.add('AutoUpdateGClient(): No need to update system [' + basesystem + '] Last updated: [' + DateTOStr(MyDate) + ']');
          exit;
        end;
      end; // MyDate <> 0

           // should have valid values for basesystem, date1 and date2 here
      try // close and open client Source.
        //  OpenSourceClient(baseSystem, date1, date2, 'P'); // comment out 7/26/2011
        UpdateDM.OpenSourceClientForSync(basesystem, date1, date2); // only get where not updated ... changed  7/26/2011
        // OpenSourceClientForSync(InBaseSys: string; date1, date2: TDate): boolean;

      except on e: exception do
        begin
          ErrorList.add('system: ' + baseSystem + datetostr(date1) + ' to ' + datetostr(date2) + ' --> source connect fail --> ' + E.Message);
          exit;
        end;
      end;

      memo1.Clear;

      // this func adds errors to the memo -
      UpdateDM.ClientCopyForDates(baseSystem, date1, date2, 'P');

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdate g_client: General exception --> ' + e.Message);
      end;
    end;

      if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;


  finally

     if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
    end;

    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

function TFormDssFeed.GetLatestUpdatedBatchForSystem(basesys: string): TDate;
var theSQL: string; // should return most current date that DSS can update TO
  MyQuery: TIfxQuery;
  myDate: TDate;
begin
  result := 0;
  MyDate := 0;
  baseSys := trim(Uppercase(BaseSys));

  if not (isInList(basesyslist, basesys)) then
  begin
    savelogfile('dss_upd', 'GetLatestUpdatedBatchForSystem --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  if UpdateDM.connSource.Connected then UpdateDM.connSource.Close;

  try
    Set_IDAC_Connection(UpdateDM.connSource, BaseSys);
    UpdateDM.connSource.Open;
    caption := 'connected to ' + GetCurrentDB(UpdateDM.connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      caption := '';
      UpdateDM.connSOurce.Close;
      exit;
    end;
  end;

  MyDate := now - 60; // if vchbatchstatus = 'U' then it's been updated already

  if basesys = 'HOHO' then // HOHO does not run updates, so get the date from the queue
    thesql := 'Select max(vchpqbath) thedate From vchpqueue where vchpqbath > :date1 and vchpqnsw3 = "N"; '
  else
    theSQL := 'Select max(vchbtchdate) thedate From vchbatch where vchbtchdate > :date1 and vchbtchstatus = "U";';

  try
    try
      MyQuery := GetIDACQuery(UpdateDM.connSource);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := trunc(MyDate);

      try
        memo1.Lines.Add(theSQL);
        memo1.lines.add('date1: ' + DateToStr(trunc(mydate)));
      except;
      end;

      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then
      begin
        memo1.Lines.add('empty query for GetLatestUpdatedBatchForSystem [' + basesys + ']');
        exit;
      end
      else
      begin
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLatestUpdatedBatchForSystem fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
    UpdateDM.connsource.Close;
  end;
end;

procedure TFormDssFeed.btnCSHistoryTasksClick(Sender: TObject);
begin
  case rgCSTasks.ItemIndex of
    0: LastCSHistdate;
    1:
    begin
      UpdateDM.OpenSourceCS(cbSourceparam.text, dtpStart.date, dtpEnd.date);
      UpdateDM.OpenTargetCS();
    end;
    2:
    begin
      UpdateDM.OpenSourceCS_HIST(cbSourceparam.text, dtpStart.date, dtpEnd.date);
      UpdateDM.OpenTargetCS_HIST();
    end;
    3:
    begin
      memo2.Lines.add('autoupdatecsHIST: ' + AutoUpdateCSFileHistory('btnUpdateCSHIst', cbSourceParam.Text));
      memo2.Lines.add('autoupdatecsHIST done');
    end;
    4:
    begin
      memo2.Lines.add('autoupdatecs: ' + AutoUpdateCS('btnUpdateCSForSys', cbsourceparam.Text));
      memo2.Lines.add('autoupdatecs done');
    end;
    5: LastCSDate;
  end;
end;

procedure TFormDssFeed.btnDCMailTasksClick(Sender: TObject);
begin
  case rgdcMailTasks.ItemIndex of
    0: UpdateDM.GetDCmailDates;
    1: GetDCMailDATA;
    2: UpdateDM.Update_dcemail();
    3: SetDCMDate;
    4: AutoUpdtDCM;
  end;
end;

procedure TFormDssFeed.btnDevTasksClick(Sender: TObject);
var
  strErr:string;
  bolSuccess:boolean;
begin
  case rgDevTasks.ItemIndex of
    0: UpdateDM.EMAIL('Test', memo1.Lines.text);
    1: UpdateDM.Initsdq;
    2: Kill;
  end;
end;

procedure TFormDssFeed.btnFindClick(Sender: TObject);
begin // "find" system, iata button in center of form
  if IsADC(edit2.Text, edit1.text) then showmessage('is a DC')
  else showmessage('ia a TA');
end;

procedure TFormDssFeed.btnRefillClick(Sender: TObject);
begin
  UpdateDM.FillWhiteList();
end;

procedure TFormDssFeed.btnSummaryTasksClick(Sender: TObject);
begin
  case rgSummaryTasks.ItemIndex of
    0: UpdateDM.OpenSourceTarget_Summary();
    1: UpdateDM.UpdateSummary();
    2: UpdateDM.loopUpdateSummary;
  end;
end;

procedure TFormDssFeed.AutoUpdtDCM;
var basesystem: string;
  sl: TStringList;
begin
  sl := TStringList.Create;
  sl.Clear;
  basesystem := trim(uppercase(cbSOurceParam.Text));

  if basesystem = '' then
  begin
    showmessage('select a system');
    exit;
  end;

  if MessageDlg('auto update dc_Email?', mtWarning, mbOKCancel, 0) = mrCancel then exit;

  sl.text := UpdateDM.AutoUpdate_dcemail(basesystem);
  memo2.Lines.add('auto update result:');
  memo2.Lines.add(sl.Text);
  if assigned(sl) then freeandNil(sl);
end;

procedure TFormDssFeed.GetDCMailDATA;
begin
  dbgrid1.DataSource := UpdateDM.ds_dc_email;
  try
    UpdateDM.Get_dcMail_data(dtpStart.date, dtpEnd.Date, cbSourceParam.Text);
  except on e: Exception do
    begin
      showmessage('update g_dc_email: system: ' + cbSourceParam.Text + datetostr(dtpStart.date) + ' to ' +
        datetostr(dtpEnd.date) + ' --> Get_dcMail_data fail --> ' + E.Message);
      exit;
    end;
  end;

  if (not (UpdateDM.q_dc_email.Active)) or (UpdateDM.q_dc_email.IsEmpty) then
  begin
    showmessage('update g_dc_email: q_dc_email - no data');
  end;
end;

function TFormDssFeed.AutoUpdateCS(sender, basesystem: string): string;
var ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';

  try
    ErrorList := TStringList.create;
    try
      MyDate := 0;
      basesystem := trim(uppercase(basesystem));
      MyDate := GetLastCSUpdate(basesystem);
      date1 := 0;
      date2 := 0;

      if (sender = 'btnUpdateCSForSys') then
      begin
        date1 := trunc(dtpStart.Date);
        date2 := trunc(dtpEnd.date);
      end
      else date2 := MyDate + 90; // assumes up to date, bring in 90 days at a time otherwise

      if (MyDate = 0) and (not (sender = 'btnUpdateCSForSys')) then
      begin
        ErrorList.add('No CS date returned *** aborted ** GetLastCSUpdate(' + basesystem + ')');
        result := errorList.Text;
        exit;
      end
      else
      begin
        if not (sender = 'btnUpdateCSForSys') then date1 := Mydate + 1;
      end; // MyDate <> 0

      dtpStart.date := trunc(date1);

      if not (sender = 'btnUpdateCSForSys') then dtpEnd.date := date2;

      try
        if (sender = 'btnUpdateCSForSys') then
        begin
          if MessageDlg('update CS for dates: ' + datetostr(dtpStart.date) + ' to ' +
            datetostr(dtpEnd.date) + ' system [' + cbSourceparam.Text + '] ?', mtConfirmation, mbOKCancel, 0) = mrCancel then
          begin
            showmessage('cancelled CS update');
            result := errorList.Text;
            exit;
          end
          else UpdateDM.OpenSourceCS(cbSourceparam.text, dtpStart.date, dtpEnd.Date);
        end
        else UpdateDM.OpenSourceCS(basesystem, date1, date2);
      // open target not needed - should be open already
      except on e: exception do
        begin
          memo1.lines.add('auto update cs OpenSOurceCS error --> [' + e.Message + ']');
          raise(exception.Create('auto update cs OpenSOurceCS error --> ' + e.Message + ']'));
        end;
      end;

      memo2.Lines.add('debug CS system: [' + baseSystem + '] > [' + datetostr(date1) + ']');
      memo1.Clear;

      // cs_checksums() is called in CopyCS();
      if (sender = 'btnUpdateCSForSys') then UpdateDM.CopyCS(cbSourceparam.text, date1, dtpEnd.Date)
      else UpdateDM.CopyCS(baseSystem, date1, date2);

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdateCS: General exception --> [' + e.Message + ']');
        raise(exception.Create('AutoUpdateCS: General exception --> ' + e.Message + ']'));
      end;
    end;

    if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
    end;
  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

function TFormDssFeed.GetLastCSHistoryUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(basesyslist, baseSYS)) then
  begin
    savelogfile('dss_upd', 'GetLastCSHistoryUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    UpdateDM.SetConn(UpdateDM.connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastCSHIstoryUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'Select max(csfs_insertdtt) From csfilehistory where csfs_basesys = "' + baseSYS + '"';

  try
    try
      MyQuery := GetIDACQuery(UpdateDM.connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else result := trunc(MyQuery.Fields[0].AsDateTime);

    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLastCSHIstoryUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;

function TFormDssFeed.AutoUpdateCSFileHistory(sender, basesystem: string): string;
var ErrorList: TStringList;
  date1, date2, MyDate: TDate;
begin
  result := '';

  try
    ErrorList := TStringList.create;
    try
      MyDate := 0;
      MyDate := GetLastCSHistoryUpdate(basesystem);
      date1 := 0;
      date2 := 0;

      if (sender = 'btnUpdateCSHIst') then
      begin
        date1 := trunc(dtpStart.Date);
        date2 := trunc(dtpEnd.date);
      end
      else date2 := MyDate + 90; // assumes up to date, bring in 90 days at a time otherwise

      if (MyDate = 0) and (not (sender = 'btnUpdateCSHist')) then
      begin
        ErrorList.add('No CS HIST date returned *** aborted ** GetLastCSHISTUpdate(' + basesystem + ')');
        result := errorList.Text;
        exit;
      end
      else
      begin
        if not (sender = 'btnUpdateCSHIst') then date1 := Mydate + 1;
      end; // MyDate <> 0

      dtpStart.date := trunc(date1);

      if not (sender = 'btnUpdateCSHist') then dtpEnd.date := date2;

      try
        if (sender = 'btnUpdateCSHist') then
        begin
          if MessageDlg('update CS History for dates: ' + datetostr(dtpStart.date) + ' to ' +
            datetostr(dtpEnd.date) + ' system [' + cbSourceparam.Text + '] ?', mtConfirmation, mbOKCancel, 0) = mrCancel then
          begin
            showmessage('cancelled CS History update');
            result := errorList.Text;
            exit;
          end //
          else UpdateDM.OpenSourceCS_HIST(cbSourceparam.text, dtpStart.date, dtpEnd.Date);
        end
        else UpdateDM.OpenSourceCS_HIST(basesystem, date1, date2);

       // a redundant jic
        UpdateDM.OpenTargetCS_Hist();

      except on e: exception do
        begin
          memo1.lines.add('auto update cs OpenSourceORTarget error --> [' + e.Message + ']');
          raise(exception.Create('auto update cs OpenSourceORTarget error --> ' + e.Message + ']'));
          exit;
        end;
      end;

      memo2.Lines.add('debug CS system: [' + baseSystem + '] > [' + datetostr(date1) + ']');
      memo1.Clear;

      // cs_Histchecksums() is called in CopyCS();
      if ((sender = 'btnUpdateCSHist')) then UpdateDM.CopyCS_HIST(cbSourceparam.text, date1, dtpEnd.Date)
      else UpdateDM.CopyCS_HIST(baseSystem, date1, date2);

    except on e: exception do
      begin
        memo1.Lines.add('AutoUpdateCS: General exception --> [' + e.Message + ']');
        raise(exception.Create('AutoUpdateCS: General exception --> ' + e.Message + ']'));
      end;
    end;

    if memo1.Text > '' then
    begin
      errorList.Add(memo1.text);
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
    end;
  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

procedure TFormDssFeed.CS_Hist_CheckSums(dstart, dend: TDate; basesys: string);
var sourceq, targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  temp, theSQL: string;
begin // assuming connected
  sourceSUM := 0.0;
  TargetSUM := 0.0;
  basesys := trim(Uppercase(basesys));

  theSQL := 'Select sum(csfs_csfilenum) From csfilehistory where ' +
    'csfs_insertdtt between :date1 and :date2';

  try
    try
      sourceq := GetIDACQuery(UpdateDM.connSource);
      sourceq.sql.Add(theSQL);
      sourceq.ParamByName('date1').AsDate := trunc(dstart);
      sourceq.ParamByName('date2').AsDate := trunc(dend);
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourcesum := 0.0
      else sourcesum := sourceq.Fields[0].AsFloat;
    except on e: exception do
      begin
        sourcesum := 0;
        savelogfile('dss_upd', 'CS_Hist_checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        memo1.Lines.add('CS_HIST_checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(sourceq);
  end;

  theSQL := 'Select sum(csfs_csfilenum) From csfilehistory where ' +
    'csfs_basesys = "' + basesys + '" and csfs_insertdtt between :date1 and :date2';

  try
    try
      targetq := GetIDACQuery(UpdateDM.connTarget);
      targetq.sql.Add(theSQL);
      targetq.ParamByName('date1').AsDate := trunc(dstart);
      targetq.ParamByName('date2').AsDate := trunc(dend);
      targetq.Active := true;

      if (targetq.IsEmpty) then targetsum := 0.0
      else targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := 0;
        savelogfile('dss_upd', 'cs_hist_checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        memo1.Lines.add('cs_hist_checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(targetq);
  end;

  temp := '';
  if (targetsum = sourcesum) then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' CS History checksum OK! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;
  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' CS History checksum Fail! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      LogList.Add(temp);
    except;
    end;
    memo1.Lines.add(temp);
  end;
  memo2.Lines.add(temp);
end;

procedure TFormDssFeed.runCSByParams(IsAnError: Boolean; errorStr: string);
var wasError, err1, err2: Boolean;
  i, LeftPos: Integer;
  bsys, errStr, strMsg, sFileName: string;
  date1, date2, MyDate: TDate;
  ErrorList: TStringList;
begin
  // try crap to get the thing to paint since I call the before activate,
  // the damn thing doesn't paint the form until it's done...
  try
   Visible := true;
    LeftPos :=Left;
   Left := -5000;
   BringToFront;
   repaint;
    Forms.Application.ProcessMessages;
   Left := LeftPos;
   BringToFront;
   repaint;
    Forms.Application.ProcessMessages;
  except;
  end;

  err1 := false;
  err2 := false;

  if IsAnError then
  begin
    savelogfile('dss_upd', 'RunCSByParams fail --> errorstr --> [' + errorStr + ']');
    setbar1('fail: ' + '[' + errorStr + ']');
    UpdateDM.EMAIL('Run CS by params FAIL: ', 'errorstr --> [' + errorStr + ']');
    exit;
  end
  else
  begin
    try
      UpdateDM.OpenTargetCS()
    except on e: exception do
      begin
        savelogfile('dss_upd', 'AutoUpdateCS' + bsys + 'failed to open target --> ' + e.message);
        exit;
      end;
    end;

    for i := 0 to BasesysList.Count - 1 do
    begin
      if bKill then break;

      application.ProcessMessages;
      wasError := false;
      bsys := '';
      date1 := 0;
      date2 := 0;
      MyDate := 0;
      errStr := '';
      bsys := trim(Uppercase(basesyslist[i]));
      memo2.Lines.Add('Update CS: ' + bsys);
      setbar0('Update CS: [' + bsys + ']');

      try
        strMsg := AutoUpdateCS('', bsys); // here
      except on e: exception do
        begin
          wasError := true;
          savelogfile('dss_upd', 'AutoUpdateCS' + bsys + ') FOR LOOP:  Msg:' + strMsg + ' e.msg: ' + e.message);
        end;
      end;

      if not (wasError) then
      begin
        try
          memo2.Lines.Add('AutoUpdateCS no errors :: ' + strMsg);
          setbar1('AutoUpdateCS no errors :: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> run CS by Params result: ' + strMsg +
            ' AutoUpdateCS no errors.');
        except;
        end;
      end
      else
      begin
        err1 := true;
        try
          memo2.Lines.Add('AutoUpdateCS errors :: ' + strMsg);
          setbar1('AutoUpdateCS errors: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) +
            ' --> run CS by params result: ' + strMsg + ' AutoUpdateCS ERRORS!');
        except;
        end;
      end;
    end; // for

    try UpdateDM.tblcscontactsTarget.Close; except; end;
    try UpdateDM.tblcscmntsTarget.Close; except; end;
    try UpdateDM.tblcsfilesTarget.Close; except; end;
    try UpdateDM.qcsContactsSource.Close; except; end;
    try UpdateDM.QcscommentsSource.Close; except; end;
    try UpdateDm.QcsfilesSource.Close; except; end;

    application.processmessages;
  end; // else

  application.Terminate;
end;

procedure TFormDssFeed.btnIATATasksClick(Sender: TObject);
begin
  case rgIATATasks.ItemIndex of
    0: UpdateDM.UpdateIATAS(cbsourceparam.Text);
    1: LoopUpdateIATAS();
    2: refresh_rc_operators();
  end;
end;

procedure TFormDssFeed.LastCSDate;
var MyDate: TDate;
begin
  MyDate := 0;
  MyDate := GetLastCSUpdate(cbSourceParam.text);

  if MyDate = 0 then
  begin
    memo2.lines.add('Err --> no cs date returned');
  end
  else
  begin
    memo2.lines.add('Last ' + cbSourceParam.text + ' cs date processed: ' + DateToStr(MyDate));

    dtpStart.Date := Mydate + 1;
    dtpEnd.Date := MyDate + 90;

    if trunc(dtpEnd.Date) <= trunc(dtpStart.date) then dtpEnd.Date := dtpStart.Date;
    memo2.lines.add('run from ' + datetostr(dtpStart.date) + ' to ' + datetostr(dtpEnd.date));
  end;
end;

procedure TFormDssFeed.LastCSHistdate;
var MyDate: TDate;
begin
  MyDate := 0;
  MyDate := GetLastCSHistoryUpdate(cbsourceparam.text);

  if MyDate = 0 then
  begin
    memo2.lines.add('Err --> no cs history date returned');
  end
  else
  begin
    memo2.lines.add('Last [' + cbSourceParam.text + '] cs history date processed: ' + DateToStr(MyDate));

    dtpStart.Date := Mydate + 1;
    dtpEnd.Date := now - 5;

    if trunc(dtpEnd.Date) <= trunc(dtpStart.date) then dtpEnd.Date := dtpStart.Date;

    memo2.lines.add('run cs history from ' + datetostr(dtpStart.date) + ' to ' + datetostr(dtpEnd.date));
  end;
end;

procedure TFormDssFeed.btnPQTasksClick(Sender: TObject);
begin
  case rgPQTasks.ItemIndex of
    0: UpdateDM.OpenSourceVQ(cbSourceparam.text, dtpStart.date, dtpEnd.date);
    1: UpdateDM.FIXROPENTARGET;
    2: UpdateDM.FixOnePQRecord;
  end;
end;

procedure TFormDssFeed.SetDCMDate;
var baseSystem: string;
  date2: TDate;
begin
  baseSystem := '';
  date2 := 0;
  baseSystem := trim(uppercase(cbsourceparam.Text));
  date2 := trunc(dtpEnd.Date);

  if trim(baseSystem) = '' then
  begin
    showmessage('select a system');
    exit;
  end;

  if UpdateDM.SetLatestDCEMLUpdate(baseSystem, date2) then
  begin
    try
      memo2.Lines.Add('last dcmail upd set ok for ' + basesystem + ' to ' + datetostr(date2));
    except;
    end;
  end
  else
  begin
    try
      memo2.Lines.Add('FAILED last dcmail upd set for ' + basesystem + ' to ' +
        datetostr(date2));
    except;
    end;
  end;
end;

procedure TFormDssFeed.GetDatesForUpdate;
var MyDate, date1, date2: TDate;
begin
  Mydate := 0;
  MyDate := UpdateDM.GetLastVQUpdate(cbsourceparam.Text); // this returns the latest date updated for a system

  if MyDate = 0 then // error
  begin
    showmessage('GetDatesForUpdate: No date returned from GetLastVCUpdate for system [' + cbsourceparam.Text + ']' +
      #10#13 + '*** aborted *** ');
    exit;
  end
  else
  begin
    date1 := Mydate + 1; // update from this date

    // can update thru this date - i.e. latest batch completed by VP Department
    date2 := GetLatestUpdatedBatchForSystem(cbsourceparam.Text);

    if (date2 = 0) then
    begin
      showmessage('GetDatesForUpdate: failed to get "update thru" date w/ GetLatestUpdatedBatchForSystem() for system [' + cbSourceParam.Text + ']');
      exit;
    end;

    if (trunc(date2) < trunc(date1)) then
    begin
      showmessage('no need to update VQ for system [' + cbsourceparam.Text + '] last updated: [' + DateTOStr(Date1) + ']');
      exit;
    end;
  end; // MyDate <> 0

  try
    showmessage('Last VQ Update for [' + cbsourceparam.Text + ']: [' + dateToStr(MyDate) + #10#13 +
      'Suggested update range [' + DateToStr(date1) + '] to [' + DateToStr(date2) + '] (last updated Batch)');
  except on e: exception do
    begin
      showmessage('Last VQ Update for [' + cbsourceparam.Text + '] FAILED DATE CONVERSION --> ' + e.Message);
    end;
  end;
end;

procedure TFormDssFeed.testRun;
var
  strResult: string;
  IsErr: boolean;
begin // test button

  if cbSourceParam.text = '' then
  begin
    memo2.lines.add('select a source');
    cbSOurceParam.DroppedDown := true;
    exit;
  end;

  IsErr := false;
  try
    strResult := AutoUpdateVQ(cbSourceParam.text);
  except on e: exception do
    begin
      IsErr := true;
      memo2.Lines.Add('result: ' + strResult + ':: AutoUpdateVQ(' + cbSourceParam.text + ') -->' + e.message);
    end;
  end;

  if not (IsErr) then memo2.Lines.Add('result: ' + strResult + ' AutoUpdateVQ no errors.')
  else memo2.Lines.Add('result: ' + strResult + ' AutoUpdateVQ YES errors.');

  IsErr := false;
  strResult := '';
  try
    strResult := AutoUpdateGClient(cbSourceParam.text);
  except on e: exception do
    begin
      IsErr := true;
      memo2.Lines.Add('AutoUpdateGClient(' + cbSourceParam.text + ') --> Msg: ' +
        strResult + ' e.msg: ' + e.message);
    end;
  end;
end;

procedure TFormDssFeed.btnVQTasksClick(Sender: TObject);
begin
  case rgVQTasks.ItemIndex of
    0: GetDatesForUpdate;
    1: UpdateDM.OpenSourceVQ(cbSourceparam.text, dtpStart.date, dtpEnd.date);
    2: UpdateDM.VQTargetOpen;
    3: UpdateDM.VCCopyDate;
    4: UpdateDM.VQchecksums(dtpStart.date, dtpEnd.Date, cbSourceParam.text);
    5: UpdateDM.VQCopyOneRecord;
  end;
end;

procedure TFormDssFeed.GetLastClientUpdate;
var MyDate, date1, date2: TDate; // get last client update
begin
  MyDate := 0;
  date1 := 0;
  date2 := 0;
  MyDate := GetLastGClientUpdate(cbSourceParam.text);

  if MyDate = 0 then // error
  begin
    memo2.lines.add('GetLastClientUpdate: No date returned from call to GetLastGClientpdate for system [' + cbsourceparam.Text + ']');
    exit;
  end
  else
  begin
    memo2.lines.add('Last g_client date processed: ' + DateToStr(MyDate));
    date1 := MyDate + 1;
    dtpStart.date := date1; // update from this date

    // can update thru this date - i.e. latest batch completed by VP Department
    date2 := GetLatestUpdatedBatchForSystem(cbsourceparam.Text);

    if (date2 = 0) then
    begin
      memo2.Lines.add('GetLastClientUpdate: failed to get "update thru" date from call to GetLatestUpdatedBatchForSystem() - system [' + cbSourceParam.Text + ']');
      exit;
    end;

    if (trunc(date2) < trunc(date1)) then
    begin
      memo2.Lines.add('no need to update g_client for system [' + cbsourceparam.Text + '] last updated: [' + DateTOStr(Date1) + ']');
      exit;
    end;

    dtpEnd.Date := date2;
  end; // MyDate <> 0

  try
    memo2.Lines.add('Last g_client update for [' + cbsourceparam.Text + ']: [' +
      dateToStr(MyDate) + '  Suggested update range [' + DateToStr(date1) +
      '] to [' + DateToStr(date2) + '] (last updated Batch)');
  except on e: exception do
    begin
      memo2.Lines.add('Last g_client update for [' + cbsourceparam.Text + '] FAILED DATE CONVERSION --> ' + e.Message);
    end;
  end;
end;

function TFormDssFeed.GetLastGClientUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(basesyslist, baseSYS)) then
  begin
    memo1.Lines.add('GetLastGCClientUpdate --> invalid basesys: [' + baseSYS + ']');
    savelogfile('dss_upd', 'GetLastGCClientUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    UpdateDM.SetConn(UpdateDM.connTarget);
  except on e: exception do
    begin
      memo1.Lines.add('GetLastGCCLientUpdate --> connect failure --> ' + E.Message);
      savelogfile('dss_upd', 'GetLastGCCLientUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'select max(gc_batchdt) from g_client where ' +
    'gc_system = "' + baseSYS + '" and gc_batchdt > :date1';

  try
    try
      MyQuery := GetIDACQuery(UpdateDM.connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := now - 90;
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        memo1.Lines.add('GetLastGclientUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'GetLastGclientUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end;

procedure TFormDssFeed.Kill;
begin
  bkill := not (bKill);
end;

procedure TFormDssFeed.btnClientTasksClick(Sender: TObject);
begin
  case rgClientTasks.ItemIndex of
    0: GetLastClientUpdate;
    1: UpdateDM.OpenClientTarget;
    2: UpdateDM.OpenSourceClientForSync(cbSourceparam.text, dtpStart.date, dtpEnd.date);
    3: UpdateDM.ClientDateCopy;
    4: UpdateDM.checkClientsums(dtpStart.date, dtpEnd.Date, cbSourceParam.text);
    5: UpdateDM.CopyOneClient;
  end;
end;

procedure TFormDssFeed.LoopUpdateIATAS();
var i: Integer; // takes about 20-25 min for all systems
begin
  for i := 0 to cbsourceparam.Items.Count - 1 do
  begin
    try
      UpdateDM.UpdateIATAS(cbsourceparam.Items[i]);
    except on e: exception do
      begin
        savelogfile('dss_upd', e.Message);
      end;
    end;
  end;
end;

procedure TFormDssFeed.runByParams(IsAnError: Boolean; errorStr: string);
var wasError, err1, err2: Boolean;
  i, theDay, LeftPos: Integer;
  bsys, errStr, strMsg, sFileName: string;
  date1, date2, MyDate: TDate;
  ErrorList: TStringList;
begin
  // try crap to get the thing to paint since I call the before activate,
  // the damn thing doesn't paint the form until it's done...
  try
    Visible := true;
    LeftPos :=Left;
    Left := -5000;
    BringToFront;
    repaint;
    Forms.Application.ProcessMessages;
    Left := LeftPos;
    BringToFront;
    repaint;
    Forms.Application.ProcessMessages;
  except;
  end;

  err1 := false;
  err2 := false;
  errorList := TStringList.Create;
  errorList.Clear;
  dbgrid4.DataSource := nil;

  if IsAnError then
  begin
    savelogfile('dss_upd', 'RunByParams fail --> errorstr --> [' + errorStr + ']');
    setbar1('fail: ' + '[' + errorStr + ']');
    UpdateDM.EMAIL('Run by params FAIL: ', 'errorstr --> [' + errorStr + ']');
    exit;
  end
  else
  begin
    for i := 0 to BasesysList.Count - 1 do
    begin
      if bKill then break;

      application.ProcessMessages;
      wasError := false;
      bsys := '';
      date1 := 0;
      date2 := 0;
      MyDate := 0;
      errStr := '';
      bsys := trim(Uppercase(basesyslist[i]));
      memo2.Lines.Add('Update vouch and queue: ' + bsys);
      setbar0('Update: [' + bsys + ']');

      try
        strMsg := AutoUpdateVQ(bsys); //first bit of actual work
      except on e: exception do
        begin
          wasError := true;
          savelogfile('dss_upd', 'AutoUpdateVQ(' + bsys + ') FOR LOOP:  Msg:' + strMsg + ' e.msg: ' + e.message);
        end;
      end;

      if not (wasError) then
      begin
        try
          memo2.Lines.Add('AutoUpdateVQ no errors :: ' + strMsg);
          setbar1('AutoUpdateVQ no errors :: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> autorun result: ' + strMsg +
            ' AutoUpdateVQ no errors.');
        except;
        end;
      end
      else
      begin
        err1 := true;
        try
          memo2.Lines.Add('AutoUpdateVQ errors :: ' + strMsg);
          setbar1('AutoUpdateVQ errors: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', FormatDateTime('MMM:DD:YYYY HH:mm', now) +
            ' --> autorun result: ' + strMsg + ' AutoUpdateVQ ERRORS!');
        except;
        end;
      end;

      strMsg := '';
      wasError := false;
      memo2.Lines.Add('Update g client: ' + bsys);
      setbar1('update gclient: ' + strMsg);

      try
        strMsg := AutoUpdateGClient(bsys);   //second bit of actual work
      except on e: exception do
        begin
          wasError := true;
          savelogfile('dss_upd', 'AutoUpdateGClient(' + bsys + ') FOR LOOP:  Msg: ' + strMsg + ' e.msg: ' + e.message);
        end;
      end;

      if not (wasError) then
      begin
        try
          memo2.Lines.Add('AutoUpdate GClient NO errors :: ' + strMsg);
          setbar1('g_client NO errors: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'NO errors AutoUpdateGClient ' +
            FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> autorun result: ' + strMsg);
        except;
        end;
      end
      else
      begin
        err2 := true;
        try
          memo2.Lines.Add('AutoUpdate G Client errors :: ' + strMsg);
          setbar1('gclient errors: ' + strMsg);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'ERRORS! AutoUpdateGClient ' + FormatDateTime('MM:DD:YYYY HH:mm', now) +
            ' --> autorun result: ' + strMsg);
        except;
        end;
      end;
    end; // for "BasesysList"

    strMsg := '';
    strMsg := UpdateDM.UpdateSummary(); //third bit of actual work

    if strMsg > '' then // error
      savelogfile('dss_upd', 'updateSummary  Msg: ' + strMsg)
    else
      jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'NO errors UpdateSummary ' +
        FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> autorun result: ' + strMsg);

    application.ProcessMessages;
    // autoUpdate_dcemail() requires that voucher_summary be populated
    for i := 0 to BasesysList.Count - 1 do
    begin
      if bKill then break;
      application.ProcessMessages;
      bsys := '';
      bsys := trim(Uppercase(basesyslist[i]));

      ErrorList.Clear;
      if bsys <> 'HOHO' then
      begin
        errorList.Text := UpdateDM.AutoUpdate_dcemail(bsys);  //fourth bit of actual work

        if trim(ErrorList.text) > '!' then
        begin // was an error
          memo2.Lines.Add('AutoUpdate_dcEmail errors: ' + errorList.text);
          jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'ERRORS! AutoUpdate_dcmail ' + FormatDateTime('MM:DD:YYYY HH:mm', now) +
            errorList.Text);
        end;
      end;
    end; // for loop #2 dc_email


    strMsg := '';

    try
      strMsg := UpdateDM.SummaryCheckSum(); //fifth bit of actual work

      memo2.Lines.Add(strMsg);
      jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'NO errors summarychecksum ' +
        FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> result: ' + strMsg);
      loglist.add('OK summarychecksum ' + FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> result: ' + strMsg);
    except on e: exception do
      begin
        memo2.Lines.Add(strMsg);
        setbar1('summary checksum errors: ' + strMsg);
        loglist.add('errors summarychecksum ' + FormatDateTime('MMM:DD:YYYY HH:mm', now) + ' --> result: ' + strMsg);
        jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', 'ERRORS! summarychecksum ' + FormatDateTime('MM:DD:YYYY HH:mm', now) +
          ' --> result: ' + strMsg);
      end;
    end;

    // add 10/29/2010 -- iata refreshing
    theday := 0;
    theDay := dayoftheweek(now);
    if (theDay = 3) then LoopUpdateIATAS() // just update on Wednesdays - takes an xtra hour or more
                                            // the Update runs Mon-Friday - not on weekends
                                            //sixth bit of actual work
    // add 02/23/2011
    else if (theDay = 4) then refresh_rc_operators();  //seventh bit of actual work


    try UpdateDM.Tbl_Target_VQ.Close; except; end;
    try UpdateDM.QSource.Close; except; end;
    try UpdateDM.Tbl_Target_Summary.Close; except; end;
    try UpdateDM.QSummarySource.Close; except; end;
    try UpdateDM.tbl_target_client.Close; except; end;
    try UpdateDM.QclientSource.Close; except; end;
    try UpdateDM.qClientUpdateDriver.Close except; end;
    try UpdateDM.connsource.Close; except; end;

    application.processmessages;
  end; // else

  UpdateDM.EMAIL('DSS UPDATE log: ', LogList.Text);
  if assigned(errorList) then FreeAndNil(errorList);
  application.Terminate;
end;

procedure TFormDssFeed.FormCreate(Sender: TObject);
var bInit_Err: Boolean;
  sInit_ErrStr, temp: string;
begin
  bInit_err := false;
  sInit_ErrStr := '';
  emlTO:='';
  bKill := false;
  GetSupportingFiles('\\fileserver\UPDATE\ini\dss.ini', 'dss.ini');
  paramlist := TStringList.Create;

  BzDateRec.churnedDaysBackDt := 0;
  BzDateRec.totalBizDaysBackDt := 0;

  paramlist.LoadFromFile('dss.ini');
  baseSysList := TStringList.create;
  temp := paramlist.Values['host'];
  emlTO := paramlist.Values['emlTO'];
  // showmessage(emlTo);

  LogList := TStringList.Create;

  if temp > '!' then UpdateDM.smtp.Host := temp
  else UpdateDM.smtp.Host := 'mx2.aemaine.com';

  if emlTO = '' then emlTO := 'chester@autoeurope.com';

  temp := '';

  try
    temp := paramlist.Values['basesys'];
    BaseSysList.Clear;
    Get_ListFromPipeString(basesyslist, temp);

    cbSourceParam.Items.Clear;
    cbSOurceparam.Items := basesyslist;
    cbSOurceparam.text := '';

    if not IsInList(basesyslist, 'USA') then
    begin
      sInit_ErrStr := 'basesyslist fail';
      bInit_Err := true;
    end;
  except on e: exception do
    begin
      stradd(sInit_ErrStr, '--> err: ' + E.Message);
      bInit_Err := true;
    end;
  end;

// daysback replaced 11/02/2010 - will now find actual last 'updated date' by system

//  try
//    daysback := strtoint(paramlist.Values['daysback']);
//  except on e: exception do
//    begin
//      daysback := 5;
//      savelogfile('dss_upd', 'days back get fail. daysback set to 5 --> ' + E.Message);
//    end;
//  end;

  //try  // remove 10/12/2010
    //Set_IATA_WhiteList(); // sets DC or TA
  //except;
  //end;

  try // changed to this method 10/11/2010
    UpdateDM.FillWhiteList();
  except on e: exception do
    begin
      stradd(sInit_ErrStr, '--> fill white list err: ' + E.Message);
      bInit_Err := true;
    end;
  end;

  try
    set_idac_connection(UpdateDM.connTarget, 'WHSE');
    UpdateDM.setconn(UpdateDM.connTarget);
    sbar.Panels[0].text := GetCurrentDB(UpdateDM.connTarget) + ' on ' + GetCurrentHost(UpdateDM.connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'target connect fail --> ' + E.Message);
      stradd(sInit_ErrStr, '--> contarget.connect err: ' + E.Message);
      bInit_Err := true;
    end;
  end;

  if (trim(lowercase(paramstr(1))) = '/update') then
  begin
    setbar1('start auto-run..');
    runByParams(bInit_err, sInit_errStr);
    exit;
  end
  else
  begin // manual update
    dtpStart.Date := now;
    dtpEnd.Date := now;
  end;
end;

procedure TFormDssFeed.FormDestroy(Sender: TObject);
begin
  if assigned(paramlist) then FreeAndNil(paramlist);
  if assigned(BaseSysList) then FreeAndNil(BaseSysList);
  if assigned(LogList) then FreeAndNil(LogList);
end;

procedure TFormDssFeed.refresh_rc_operators();
var i, count, stuffcount, projected_total: Integer;
  SourceQuery, targetQuery: TIfxQuery;
  theSQL, baseSystem, insertSQL: string;
  StartTime, EndTime: DWORD;
begin // re-load all active operators into whse - may have to resort to bringing inactive operators in later

// added this to runbyparams() - once a week on thursday.
// this is the operator update routine from the dead rate cache program.
// if this table is not updated once in a while, laura will complain about shit (operator master vs operator)
// not lining up in her special reports.

// to fix it quick manually:
// Run rate_cache.exe
// click 'target open' under systems tab
// run "Refresh Operators" in Operators tab

// memo2 -> Logs
// memo1 -> errors

  try
    UpdateDM.SetConn(UpdateDM.connTarget); // UpdateDM.connTarget is connected to WHSE OnCreate() using set_idac_connection
    SetBar0('UpdateDM.connTarget connected to: ' + GetCurrentDB(UpdateDM.connTarget));
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastDCEMLpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  if not UpdateDM.connGLobal.Connected then
  begin
    try
      set_idac_connection(UpdateDM.connGLobal, 'GLOBAL');
      UpdateDM.connglobal.Open;
      SetBar1('connglobal connected to: ' + GetCurrentDB(UpdateDM.connGlobal));
    except
      exit;
    end;
  end;

  if not (UpdateDM.connglobal.Connected) then exit;
  if not (UpdateDM.connTarget.Connected) then exit;

  try
    SourceQuery := GetIdacquery(UpdateDM.connglobal); // source system = AEGLOBAL
    targetQuery := GetIdacquery(UpdateDM.connTarget); // whse
    targetQuery.SQL.Clear;
    count := 0;

    try // delete all operators for a system in the WHSE:
      StartTime := GetTickCount();
      targetQuery.SQL.Clear;
      targetQuery.SQL.Add('delete from rc_operators where 1=1');
      count := targetQuery.ExecSQL;
      EndTime := GetTickCount();
      memo2.Lines.add(IntToStr(count) + ' rc_operators deleted from rc_operators in ' + FloatToStr(EndTIme - StartTime) + ' ms');
    except on e: exception do
      begin
        savelogfile('dss_upd', 'failed to delete operators from rc_operators [' + e.message + '] -- cxld.');
        targetQuery.Cancel;
        exit;
      end;
    end;

    // now re-fill operators:
    SourceQuery.Close;

    theSQL := ' select ' +
      'opbasesys, oper_code, opmaster, oper_name, opctryctry, opctprvid,  oplytdcancel, ' +
      'opcityrteprvid, opcitycity, opcityrtecity, oplrteloccity, oplloc, oplname,opladdr1,  ' +
      'opladdr2,opladdr3,opladdr4, oplctry, opltelephone, oplfax, oplzip, oplloctype,  ' +
      'opladtype, oplloccityid,  opcitykeycity, oplserial, opctserial, opctctrylink,  ' +
      'opladserial, opcityserial,opladkey1, oplswitches, oplpudotype, opproductid,  ' +
      'opeastern, opother2, optype1, opserial, opctoplink, opctryppdsw, ' +
      'opctcurid,opctcurid2,opctmsws,opctgsmi1,opctgsmi2,opcttxrte, opctactive,opcttxinc,  ' +
      'opctatfax, opctvendor, oploag, oplbrcode, op_inact2, opctr_inact2, opcity_inact2, oploc_inact2  ' +
      'from g_oploc1, g_operator, g_opctr, g_opcity, outer g_oploc1addr  ' +

    'where g_opctr.opctoplink = g_operator.opserial  ' +
    // --and (g_operator.opbasesys = "US" or g_operator.opbasesys = "*")
    'and g_opcity.opctctrylink = g_opctr.opctserial and g_oploc1.oplloccityid = g_opcity.opcityserial  ' +
      'and g_opctr.opctryctry  between  " "  and  "zz" and g_opctr.opctprvid between  " "  and  "zzzzz" ' +
      'and g_opcity.opcitycity between  " "  and  "zzzzzzzzzzzzzzzzzzzz" ' +
      'and g_operator.oper_code  between  " "  and  "zzzzzzzzzz" ' +
      'and g_oploc1addr.opladkey1 = g_oploc1.oplserial and g_oploc1addr.opladtype in ("P", "R") ' + // pickup return
      'and (upper(g_oploc1.oplactive) = "Y" or g_oploc1.oplactive is null) ' +
      'and (upper(g_oploc1.oplpudotype) in ("D","P") or g_oploc1.oplpudotype is null ' +
      'or g_oploc1.oplpudotype =  "  ") and (g_operator.optype1 in("S","R")  or g_operator.optype1 is null) ';

    // memo1.Text := theSQL;
    SourceQuery.SQL.Clear;
    SourceQuery.SQL.add(theSQL);

    try
      memo2.Lines.add('get operators opening query.');
      application.ProcessMessages;
      StartTime := GetTickCount();
      SourceQuery.Open;
      EndTime := GetTickCount();
      memo2.Lines.add('get operators time: ' + FloatToStr(EndTIme - StartTime) + ' ms');
      application.ProcessMessages;

      startTime := GetTIckCOunt();

      memo2.Lines.add('calling dataset.recordcount ' + formatdateTime('mm/dd/yyyy:nn:ss' ,now));
      application.ProcessMessages;
      projected_total := SourceQuery.RecordCount;
      EndTime := GetTickCount();
      memo2.Lines.add('get operators recordcount ' + IntToStr(projected_total) + ' time: ' + FloatToStr(EndTIme - StartTime) + ' ms');
    except on e: exception do
      begin
        savelogfile('dss_upd', 'Get Operators: failed to open query --> ' + e.message);
        exit;
      end;
    end;

    count := 0;
    SourceQuery.First;
    StartTIme := gettickcount();

    insertSQL := 'insert into rc_operators (' +
      'rc_basesystem,rc_oploc1_cityid,rc_oploc1_location,rc_oploc1_oplserial,' +
      'rc_oploc1_oplytdcancel,rc_oploc1_ratecity,rc_oploc1_locationtype,' +
      'rc_oploc1_switches,rc_oploc1_pudotype,rc_operator_productid,' +
      'rc_operator_oper_code,rc_operator_master,rc_operator_oper_name,' +
      'rc_operator_easternok,rc_operator_other2,rc_operator_reg_special,' +
      'rc_operator_opserial,rc_opctr_opctoplink,rc_opctr_country,' +
      'rc_opctr_prov,rc_opctr_payment_type,rc_opctr_currency_id,' +
      'rc_opctr_currency_id2,rc_opctr_msws,rc_opctr_gsmi1,rc_opctr_gsmi2,' +
      'rc_opctr_taxrate,rc_opctr_active,rc_opctr_tax_included,' +
      'rc_opctr_atfax,rc_opctr_vendor,rc_opctr_serial,rc_opcity_country_link,' +
      'rc_opcity_city,rc_opcity_rate_prov,rc_opcity_rate_city,' +
      'rc_opcity_is_key_city,rc_opcity_serial,rc_oploc1addr_key,' +
      'rc_oploc1addr_address_type,rc_oploc1addr_op_name,' +
      'rc_oploc1addr_addr1,rc_oploc1addr_addr2,rc_oploc1addr_addr3,' +
      'rc_oploc1addr_addr4,rc_oploc1addr_ctry,rc_oploc1addr_phone,' +
      'rc_oploc1addr_fax,rc_oploc1addr_zip,rc_oploc1addr_serial,' +
      'rc_oploc1addr_oag,rc_oploc1addr_branch_code,rc_op_inact2,' +
      'rc_oploc_inact2,rc_opctr_inact2, rc_opcity_inact2)' +
      'VALUES(' +
      ':rc_basesystem,:rc_oploc1_cityid,:rc_oploc1_location,:rc_oploc1_oplserial,' +
      ':rc_oploc1_oplytdcancel,:rc_oploc1_ratecity,:rc_oploc1_locationtype,' +
      ':rc_oploc1_switches,:rc_oploc1_pudotype,:rc_operator_productid,:rc_operator_oper_code,' +
      ':rc_operator_master,:rc_operator_oper_name,:rc_operator_easternok,' +
      ':rc_operator_other2,:rc_operator_reg_special,:rc_operator_opserial,' +
      ':rc_opctr_opctoplink,:rc_opctr_country,:rc_opctr_prov,:rc_opctr_payment_type,' +
      ':rc_opctr_currency_id,:rc_opctr_currency_id2,:rc_opctr_msws,:rc_opctr_gsmi1,' +
      ':rc_opctr_gsmi2,:rc_opctr_taxrate,:rc_opctr_active,:rc_opctr_tax_included,' +
      ':rc_opctr_atfax,:rc_opctr_vendor,:rc_opctr_serial,:rc_opcity_country_link,' +
      ':rc_opcity_city,:rc_opcity_rate_prov,:rc_opcity_rate_city,:rc_opcity_is_key_city,' +
      ':rc_opcity_serial,:rc_oploc1addr_key,:rc_oploc1addr_address_type,' +
      ':rc_oploc1addr_op_name,:rc_oploc1addr_addr1,:rc_oploc1addr_addr2,' +
      ':rc_oploc1addr_addr3,:rc_oploc1addr_addr4,:rc_oploc1addr_ctry,' +
      ':rc_oploc1addr_phone,:rc_oploc1addr_fax,:rc_oploc1addr_zip,:rc_oploc1addr_serial,' +
      ':rc_oploc1addr_oag,:rc_oploc1addr_branch_code,:rc_op_inact2,:rc_oploc_inact2,' +
      ':rc_opctr_inact2,:rc_opcity_inact2);';

    targetQuery.Close;
    targetQuery.SQL.Clear;
    targetQuery.SQL.Add(insertSQL);

    while not SourceQuery.Eof do
    begin
      try
        if (bkill) then break;

        targetquery.Close;
        targetQuery.ParamByName('rc_basesystem').AsString := SourceQuery.FieldByName('opbasesys').AsString; // clbsystems.Items[i];
        targetQuery.ParamByName('rc_oploc1_cityid').AsInteger := SourceQuery.FieldByName('oplloccityid').AsInteger;
        targetQuery.ParamByName('rc_oploc1_location').asstring := SourceQuery.FieldByName('oplloc').asstring;
        targetQuery.ParamByName('rc_oploc1_oplserial').asInteger := SourceQuery.FieldByName('oplserial').asInteger;
        targetQuery.ParamByName('rc_oploc1_oplytdcancel').asInteger := SourceQuery.FieldByName('oplytdcancel').asInteger; //new 32 bit int will link to op hours
        targetQuery.ParamByName('rc_oploc1_ratecity').AsString := SourceQuery.FieldByName('oplrteloccity').AsString;
        targetQuery.ParamByName('rc_oploc1_locationtype').AsString := SourceQuery.FieldByName('oplloctype').AsString;
        targetQuery.ParamByName('rc_oploc1_switches').AsString := SourceQuery.FieldByName('oplswitches').AsString;
        targetQuery.ParamByName('rc_oploc1_pudotype').AsString := SourceQuery.FieldByName('oplpudotype').AsString;
        targetQuery.ParamByName('rc_operator_productid').AsInteger := SourceQuery.FieldByName('opproductid').AsInteger;
        targetQuery.ParamByName('rc_operator_oper_code').AsString := SourceQuery.FieldByName('oper_code').asstring;
        targetQuery.ParamByName('rc_operator_master').asstring := SourceQuery.FieldByName('opmaster').AsString;
        targetQuery.ParamByName('rc_operator_oper_name').asstring := SourceQuery.FieldByName('oper_name').AsString;
        targetQuery.ParamByName('rc_operator_easternok').AsString := SourceQuery.FieldByName('opeastern').AsString;
        targetQuery.ParamByName('rc_operator_other2').AsString := SourceQuery.FieldByName('opother2').AsString;
        targetQuery.ParamByName('rc_operator_reg_special').AsString := SourceQuery.FieldByName('optype1').AsString;
        targetQuery.ParamByName('rc_operator_opserial').asinteger := SourceQuery.FieldByName('opserial').asinteger;
        targetQuery.ParamByName('rc_opctr_opctoplink').asinteger := SourceQuery.FieldByName('opctoplink').asinteger;
        targetQuery.ParamByName('rc_opctr_country').AsString := SourceQuery.FieldByName('opctryctry').AsString;
        targetQuery.ParamByName('rc_opctr_prov').AsString := SourceQuery.FieldByName('opctprvid').AsString;
        targetQuery.ParamByName('rc_opctr_payment_type').AsString := SourceQuery.FieldByName('opctryppdsw').AsString;
        targetQuery.ParamByName('rc_opctr_currency_id').AsString := SourceQuery.FieldByName('opctcurid').AsString;
        targetQuery.ParamByName('rc_opctr_currency_id2').AsString := SourceQuery.FieldByName('opctcurid2').AsString;
        targetQuery.ParamByName('rc_opctr_msws').AsString := SourceQuery.FieldByName('opctmsws').AsString;
        targetQuery.ParamByName('rc_opctr_gsmi1').AsInteger := SourceQuery.FieldByName('opctgsmi1').AsInteger;
        targetQuery.ParamByName('rc_opctr_gsmi2').AsInteger := SourceQuery.FieldByName('opctgsmi2').AsInteger;
        targetQuery.ParamByName('rc_opctr_taxrate').AsFloat := SourceQuery.FieldByName('opcttxrte').AsFloat;
        targetQuery.ParamByName('rc_opctr_active').AsString := SourceQuery.FieldByName('opctactive').AsString;
        targetQuery.ParamByName('rc_opctr_tax_included').AsString := SourceQuery.FieldByName('opcttxinc').AsString;
        targetQuery.ParamByName('rc_opctr_atfax').AsString := SourceQuery.FieldByName('opctatfax').AsString;
        targetQuery.ParamByName('rc_opctr_vendor').asstring := SourceQuery.FieldByName('opctvendor').AsString;
        targetQuery.ParamByName('rc_opctr_serial').AsInteger := SourceQuery.FieldByName('opctserial').AsInteger;
        targetQuery.ParamByName('rc_opcity_country_link').AsInteger := SourceQuery.FieldByName('opctctrylink').AsInteger;
        targetQuery.ParamByName('rc_opcity_city').AsString := SourceQuery.FieldByName('opcitycity').asstring;
        targetQuery.ParamByName('rc_opcity_rate_prov').AsString := SourceQuery.FieldByName('opcityrteprvid').asstring;
        targetQuery.ParamByName('rc_opcity_rate_city').AsString := SourceQuery.FieldByName('opcityrtecity').asstring;
        targetQuery.ParamByName('rc_opcity_is_key_city').AsString := SourceQuery.FieldByName('opcitykeycity').asstring;
        targetQuery.ParamByName('rc_opcity_serial').AsInteger := SourceQuery.FieldByName('opcityserial').AsInteger;
        targetQuery.ParamByName('rc_oploc1addr_key').AsInteger := SourceQuery.FieldByName('opladkey1').AsInteger;
        targetQuery.ParamByName('rc_oploc1addr_address_type').AsString := SourceQuery.FieldByName('opladtype').AsString;
        targetQuery.ParamByName('rc_oploc1addr_op_name').AsString := SourceQuery.FieldByName('oplname').AsString;
        targetQuery.ParamByName('rc_oploc1addr_addr1').AsString := SourceQuery.FieldByName('opladdr1').AsString;
        targetQuery.ParamByName('rc_oploc1addr_addr2').AsString := SourceQuery.FieldByName('opladdr2').AsString;
        targetQuery.ParamByName('rc_oploc1addr_addr3').AsString := SourceQuery.FieldByName('opladdr3').AsString;
        targetQuery.ParamByName('rc_oploc1addr_addr4').AsString := SourceQuery.FieldByName('opladdr4').AsString;
        targetQuery.ParamByName('rc_oploc1addr_ctry').AsString := SourceQuery.FieldByName('oplctry').AsString;
        targetQuery.ParamByName('rc_oploc1addr_phone').AsString := SourceQuery.FieldByName('opltelephone').AsString;
        targetQuery.ParamByName('rc_oploc1addr_fax').AsString := SourceQuery.FieldByName('oplfax').AsString;
        targetQuery.ParamByName('rc_oploc1addr_zip').AsString := SourceQuery.FieldByName('oplzip').AsString;
        targetQuery.ParamByName('rc_oploc1addr_serial').AsInteger := SourceQuery.FieldByName('opladserial').AsInteger;
        targetQuery.ParamByName('rc_oploc1addr_oag').AsString := SourceQuery.FieldByName('oploag').AsString;
        targetQuery.ParamByName('rc_oploc1addr_branch_code').AsString := SourceQuery.FieldByName('oplbrcode').AsString;
        targetQuery.ParamByName('rc_op_inact2').AsString := SourceQuery.FieldByName('op_inact2').AsString; //rc_op_inact2
        targetQuery.ParamByName('rc_oploc_inact2').AsString := SourceQuery.FieldByName('oploc_inact2').AsString; //rc_oploc_inact2
        targetQuery.ParamByName('rc_opctr_inact2').AsString := SourceQuery.FieldByName('opctr_inact2').AsString; //rc_opctr_inact2
        targetQuery.ParamByName('rc_opcity_inact2').AsString := SourceQuery.FieldByName('opcity_inact2').AsString; //rc_opcity_inact2

        i := 0;
        i := targetQuery.ExecSQL;
        {
         for later:
         targetQuery.ParamByName('rc_oploc1addr_lat').AsFloat :=  MyQuery.FieldByName('opllat').AsFloat;
         targetQuery.ParamByName('rc_oploc1addr_long').AsFloat := MyQuery.FieldByName('opllong').AsFloat;

         Keep in mind lat lon is in a tag table named g_oploc1addr_other now.
         the links: g_oploc1addr_other.oplao_link = g_oploc1addr.opladserial and g_oploc1addr_other.oplao_key1 = 'GEO1'

         indexes for g_oploc1addr_other:

         create index "informix".oplao_floatkey on "informix".g_oploc1addr_other
                (oplao_link,oplao_fl5,oplao_fl6) using btree ;
         create unique index "informix".oplao_pkey2 on "informix".g_oploc1addr_other
                (oplao_link,oplao_key1,oplao_dfrom,oplao_dthru) using btree
        }
      except on e: exception do
        begin
          targetQuery.Cancel;
          savelogfile('dss_upd', 'insert Operators: failed to insert [' + e.message + ']');
        end;
      end;

      SourceQuery.Next;

      if (i = 1) then inc(count)
      else inc(stuffcount);

      try
          //lbl_rc_op_updt.Caption := 'updates: ' +InttoStr(count) + ' errors: ' + IntToStr(stuffcount);
      except;
      end;

      application.ProcessMessages;

    end; // while

    EndTime := GetTickCount();

    memo2.Lines.add('[' + IntToStr(count) + '] operators added to whse. in [' + FloatToStr(EndTIme - StartTime) + '] ms with ['
        + IntTOStr(stuffcount) + '] errors.');

  finally
    SourceQuery.Close;
    targetQuery.Close;
    if assigned(SourceQuery) then FreeAndNil(SourceQuery);
    if assigned(targetQuery) then FreeAndNil(targetQuery);
  end;
end;

function TFormDSSFeed.GetBizDates(WhseConn: TIfxConnection): BizDateRec;
var MyQuery: TIfxQuery;
  myBDRec: BizDateRec;
  // using lost as rule for date range for all biz and range where customer has not come back.
  // typically, the rule is "if booked in last 5 years, but not within the last year, the customer is churned (lost)"
  // assuming WhseConn is connected to the aewhse DB
begin
  myBDRec.churnedDaysBackDt := now() - 365; // 1 year ago
  myBDRec.totalBizDaysBackDt := now() - 1825; // 5 years ago
  result := myBDRec;

  if (not (UpdateDM.ConnTarget.connected)) then exit;

  try
    MyQuery := GetIDACQuery(UpdateDM.ConnTarget);
    try
      MyQuery.SQL.Clear;
      MyQuery.SQL.Add('select emr_daysback_bktotals, emr_daysback_churn from g_dcemail_rules');
      MyQuery.SQL.Add('where emr_rulename = "lost"');
      MyQuery.Open;
      myBDRec.totalBizDaysBackDt := now() - trunc(MyQuery.Fields[0].AsInteger);
      myBDRec.churnedDaysBackDt := now() - trunc(MyQuery.Fields[1].AsInteger);
      result := myBDRec;
    except on e: exception do
      begin
        myBDRec.churnedDaysBackDt := now() - 365;
        myBDRec.totalBizDaysBackDt := now() - 1825;
        result := myBDRec;
      end;
    end;
  finally
    if assigned(MyQuery) then
    begin
      MyQuery.Close;
      FreeAndNil(MyQuery);
    end;
  end;
end;


end.


