object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    554
    441)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 18
    Top = 80
    Width = 111
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 18
    Top = 99
    Width = 151
    Height = 13
    Caption = 'Label2'
  end
  object Memo1: TMemo
    Left = 230
    Top = 8
    Width = 316
    Height = 105
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 35
    Width = 49
    Height = 25
    Caption = 'open'
    TabOrder = 1
    OnClick = Button1Click
  end
  object DateTimePicker1: TDateTimePicker
    Left = 8
    Top = 8
    Width = 105
    Height = 21
    Date = 40352.579972280090000000
    Time = 40352.579972280090000000
    TabOrder = 2
  end
  object DateTimePicker2: TDateTimePicker
    Left = 119
    Top = 8
    Width = 105
    Height = 21
    Date = 40352.580024502310000000
    Time = 40352.580024502310000000
    TabOrder = 3
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 136
    Width = 538
    Height = 120
    Anchors = [akLeft, akTop, akRight]
    DataSource = DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 262
    Width = 538
    Height = 171
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource2
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button2: TButton
    Left = 149
    Top = 35
    Width = 75
    Height = 25
    Caption = 'update'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 175
    Top = 75
    Width = 49
    Height = 25
    Caption = 'kill'
    TabOrder = 7
    OnClick = Button3Click
  end
  object IfxConnection1: TIfxConnection
    Connected = True
    Params.Strings = (
      'INFORMIXSERVER=ids_p570c'
      'WIN32HOST=p570c.aemaine.com'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@ids_p570c')
    IsolationLevel = ilDirtyRead
    Left = 96
    Top = 32
  end
  object source: TIfxQuery
    BufferChunks = 64
    Connection = IfxConnection1
    SQL.Strings = (
      'Select basesys, vcnumb, vchrel, '
      'vcvpromo1, vcvpromo2, vcvpromo3'
      ''
      'From vouchandqueue'
      'where '
      
        'basesys in ('#39'USA'#39', '#39'CANA'#39', '#39'KEM'#39', '#39'HOHO'#39', '#39'SAF'#39', '#39'LAC'#39', '#39'AUS'#39', '#39 +
        'NEWZ'#39', '#39'UK'#39', '#39'EURO'#39', '#39'HOT'#39') and'
      'vchpqbath between :date1 and :date2 and'
      '('
      '(vcvpromo1 is not null and length(vcvpromo1) > 0) OR'
      '(vcvpromo2 is not null and length(vcvpromo2) > 0) OR'
      '(vcvpromo3 is not null and length(vcvpromo3) > 0) '
      ')')
    Left = 96
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'date1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'date2'
        ParamType = ptInput
      end>
  end
  object target: TIfxQuery
    BufferChunks = 64
    Connection = IfxConnection1
    UpdateMode = upWhereKeyOnly
    DataSource = DataSource1
    RequestLive = True
    SQL.Strings = (
      'select * from g_voucher_summary where'
      'vs_basesys = :basesys and'
      'vs_voucher = :vcnumb and'
      'vs_release = :vchrel')
    Left = 128
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vcnumb'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vchrel'
        ParamType = ptInput
      end>
  end
  object DataSource1: TDataSource
    DataSet = source
    Left = 96
    Top = 96
  end
  object DataSource2: TDataSource
    DataSet = target
    Left = 128
    Top = 96
  end
end
