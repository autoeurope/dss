unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, IfxCustomDataSet, IfxQuery, IfxConnection,
  ComCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    IfxConnection1: TIfxConnection;
    source: TIfxQuery;
    target: TIfxQuery;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  bkill : boolean;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin



  try
    target.Close;
    source.Close;
    IfxConnection1.Close;
    application.ProcessMessages;
    source.ParamByName('date1').AsDate :=  DateTimePicker1.date;
    source.ParamByName('date2').AsDate :=  DateTimePicker2.date;
    IfxConnection1.Open;
     source.open;
    target.Open;
    application.ProcessMessages;

    label1.caption := IntTOStr(source.recordcount);

  except on e:exception do
  begin
    showmessage(e.Message);
  end;
  end;


end;

procedure TForm1.Button2Click(Sender: TObject);
var i : INteger;
begin
  source.First;
  i:=0;
  bkill := false;

  while not source.Eof do
  begin
    if bkill then break ;

    inc(i);
    label2.Caption := IntToStr(i);
    application.ProcessMessages;

  ///  if I > 10 then break;    

    try
    target.Edit;
    target.FieldByName('vs_promo1').asstring := source.FieldByName('vcvpromo1').AsString;
    target.FieldByName('vs_promo2').asstring := source.FieldByName('vcvpromo2').AsString;
    target.FieldByName('vs_promo3').asstring := source.FieldByName('vcvpromo3').AsString;
    target.Post;
    except on e:exception do
    begin
      memo1.Lines.add(source.FieldByName('vcnumb').AsString + ' error --> ' + e.Message);
    end;
    end;

    source.Next;
  end;



end;

procedure TForm1.Button3Click(Sender: TObject);
begin
bkill := true;
end;

end.
