unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, IfxTable, DB, StdCtrls, IfxCustomDataSet, IfxQuery,
  IfxConnection;

type
  TForm1 = class(TForm)
    targetconn: TIfxConnection;
    sourceconn: TIfxConnection;
    qsource: TIfxQuery;
    go: TButton;
    kill: TButton;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    tbltarget: TIfxTable;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Memo1: TMemo;
    Label1: TLabel;
    procedure goClick(Sender: TObject);
    procedure killClick(Sender: TObject);
  private
    { Private declarations }
  public
    bkill : boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.goClick(Sender: TObject);
var i,x: Integer;
begin
  qsource.First;
  bkill := false;
  x:= 0;

  while not qsource.Eof do
  begin
     application.ProcessMessages;
    try
      tblTarget.Insert;

      with qsource do
      begin

      if bkill then break;

      for i := 0 to fieldcount - 1 do
      begin
        try
            { if (fields[i].fieldname = 'filter' ) then
              begin    // do this if the fields are different names:
                  tblTO.fieldbyname('filterx').value := fields[i].value;

               end
              else
            }
          tblTarget.fieldbyname(fields[i].fieldname).value := fields[i].value;
        except on e1: exception do
            memo1.lines.add('e1: ' + e1.message);
        end;
      end;

      end;

     tblTarget.Post;
     inc(x);

     try
     label1.Caption := IntToStr(x);
      except; end;

    except on e:exception do begin

       tblTarget.Cancel;
       memo1.lines.add('e: ' + e.message);

    end;
    end;







   qsource.Next;
  end;


end;

procedure TForm1.killClick(Sender: TObject);
begin
 bkill := not(bkill);
end;

end.
