object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 545
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 72
    Top = 64
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object go: TButton
    Left = 56
    Top = 8
    Width = 75
    Height = 25
    Caption = 'go'
    TabOrder = 0
    OnClick = goClick
  end
  object kill: TButton
    Left = 433
    Top = 8
    Width = 75
    Height = 25
    Caption = 'kill'
    TabOrder = 1
    OnClick = killClick
  end
  object DBGrid1: TDBGrid
    Left = 296
    Top = 120
    Width = 332
    Height = 173
    DataSource = DataSource2
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 120
    Width = 282
    Height = 173
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Memo1: TMemo
    Left = 8
    Top = 299
    Width = 618
    Height = 238
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
  end
  object targetconn: TIfxConnection
    DesignConnection = True
    Params.Strings = (
      'INFORMIXSERVER=ids_f50aus'
      'WIN32HOST=f50aus'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=rpbus@ids_f50aus')
    Left = 152
    Top = 8
  end
  object sourceconn: TIfxConnection
    DesignConnection = True
    Params.Strings = (
      'INFORMIXSERVER=ids_p650'
      'WIN32HOST=p650'
      'WIN32PROTOCOL=olsoctcp'
      'WIN32SERVICE=sqlexec'
      'WIN32USER=informix'
      'WIN32PASS=portland'
      'DATABASE=nzreserve@ids_p650')
    Left = 8
    Top = 8
  end
  object qsource: TIfxQuery
    BufferChunks = 256
    Connection = sourceconn
    SQL.Strings = (
      'select "NEWZ" gv_basesys,'
      '    trim(upper(apvenmast.vendor)) gv_iata,'
      '    trim(upper(ven_class)) gv_class,'
      '    trim(upper(vendor_vname)) gv_name,'
      '    trim(upper(legal_name)) gv_legal_name,'
      '    trim(upper(vendor_contct)) gv_contact_name,'
      '    trim(upper(addr1)) gv_addr1,'
      '    trim(upper(addr2)) gv_addr2,'
      '    trim(upper(addr3)) gv_addr3,'
      '    trim(upper(addr4)) gv_addr4,'
      '    trim(upper(city_addr5)) gv_city,'
      '    trim(upper(county)) gv_prov, -- county is used for state'
      '    trim(upper(postal_code)) gv_postal,'
      '    trim(upper(country)) gv_country,'
      '    trim(upper(phone_prefix)) gv_phone_prefix,'
      '    trim(upper(phone_num)) gv_phone_num,'
      '    trim(upper(fax_prefix)) gv_fax_prefix,'
      '    trim(upper(fax_num)) gv_fax_num,'
      '    trim(upper(agemail2)) gv_email,'
      '    trim(upper(agconsortium)) gv_consortium'
      '    from apvenmast, agfile, outer(apvenaddr) '
      '    where apvenmast.vendor_group = "DRIV" and'
      '    inumb = apvenmast.vendor'
      '    and apvenaddr.vendor = apvenmast.vendor '
      '    and apvenaddr.vendor_group = apvenmast.vendor_group')
    Left = 8
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = qsource
    Left = 8
    Top = 72
  end
  object DataSource2: TDataSource
    DataSet = tbltarget
    Left = 184
    Top = 40
  end
  object tbltarget: TIfxTable
    BufferChunks = 256
    Connection = targetconn
    TableName = 'g_vendor'
    Left = 184
    Top = 8
  end
end
