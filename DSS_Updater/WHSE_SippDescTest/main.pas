unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DB, Grids,
  DBGrids, IfxCustomDataSet, IfxQuery, IfxConnection,
  jmclasses, aecommonfunc, SippDescribe;

type
  TForm1 = class(TForm)
    conn: TIfxConnection;
    q: TIfxQuery;
    DBGrid1: TDBGrid;
    ds: TDataSource;
    StatusBar1: TStatusBar;
    Splitter1: TSplitter;
    Panel1: TPanel;
    EditParam: TEdit;
    connect: TButton;
    memo: TMemo;
    memoSQL: TMemo;
    Button1: TButton;
    SIPPDesc1: TSIPPDesc;
    connGLobal: TIfxConnection;
    cbUSetestglobal: TCheckBox;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    DBGrid2: TDBGrid;
    Splitter2: TSplitter;
    dsassFace: TDataSource;
    procedure connectClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    bKILL: boolean;
  public
    procedure SetBar(i: Integer; instr: string);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.SetBar(i: Integer; instr: string);
begin
  statusbar1.Panels[i].Text := instr;
end;



procedure TForm1.Button1Click(Sender: TObject);
var
  temp, errstr, def1, def2: string;
  assCount: integer;
begin
  bKILL := false;
  if not connglobal.Connected then
  begin
    showmessage('need to connect 1');
    exit;
  end;

  SIPPDesc1.Connection := connGLobal;


  errstr := '';
  if SIPPDesc1.Initialize(errstr) then
    setbar(1, 'init OK')
  else
  begin

    exit;
  end;


 ////showmessage('['+ errstr + ']');

  SIPPDesc1.Language := 'ENG';

 // if checkbox1.Checked then
 //   SIPPDESC1.Logging := true
 // else
 //   SIPPDESC1.Logging := false;

  asscount := 0;
  memo.Clear;

  while not (Q.Eof) do
  begin
    if bkill then break;

    inc(asscount);
    setbar(0, intTOsTR(ASSCOUNT));

    temp := '?';
    if q.FieldByName('vs_product_type_tag').AsString = 'CHAUFFEUR' then temp := 'chf'
    else if q.FieldByName('vs_product_type').AsString = 'CAR' then temp := 'car'
    else if q.FieldByName('vs_product_type').AsString = 'AIR' then temp := 'air'
    else if q.FieldByName('vs_product_type').AsString = 'HOTEL' then temp := 'hot'
    else if q.FieldByName('vs_product_type').AsString = 'CRUISE' then temp := 'cru';



      // TAlgType = (mtCached, mtCar_HC_OLD, mtHotelAIR_HC_Old);
      if (temp = 'hot') or (temp = 'air') then
      begin
        SIPPDesc1.MethodType := mtHotelAIR_HC_Old;
      end
      else
      begin
        SIPPDesc1.MethodType := mtCar_HC_OLD;
      end;


    SIPPDesc1.Product := temp;
    SIPPDesc1.SIPP := q.FieldByName('vs_sipp').AsString;

    if SIPPDesc1.Execute then
      def1 := sippdesc1.SippDescription_Short
    else
    begin
      def1 := sippdesc1.SippDescription_Short + ' : err : ' + sippdesc1.ErrorMessage;
    end;


    SIPPDesc1.MethodType := mtCached;

    if SIPPDesc1.Execute then
      def2 := sippdesc1.SippDescription_Short
    else
    begin
      def2 := sippdesc1.SippDescription_Short + ' : err : ' + sippdesc1.ErrorMessage;
    end;

    memo.lines.add(sippdesc1.SIPP + ':');
    memo.lines.add('old: ' + def1 + ' ms: ' + IntToStr(SIPPDesc1.Execution_Milliseconds));
    memo.lines.add('new: ' + def2 + ' ms: ' + IntToStr(SIPPDesc1.Execution_Milliseconds));
    memo.lines.add('------------');

    if asscount > 500 then break;

    q.Next;

  end;





end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  try
    q.Close;
    q.SQL.Clear;
    q.SQL.Text := memosql.Text;
    setbar(0, 'opening q...');
    application.ProcessMessages;
    q.Open;
    setbar(0, 'success');
  except on e: exception do
    begin
      showmessage(e.Message);
    end;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  bkill := not (bkill);
end;

procedure TForm1.Button4Click(Sender: TObject);
var errStr: String;
begin
   errStr := '';
   Button4 .Caption := 're-init';

   SIPPDesc1.ValidDate := now;

   if not  SIPPDesc1.Initialize(errStr) then
   begin
    memo.Lines.add('Init fail ->' + errStr);
   end
   else
    memo.Lines.add('Init OK');

   try
    dsassFace.DataSet := sippdesc1.FMEM;
   except;
   end;

end;

procedure TForm1.connectClick(Sender: TObject);
begin
  setbar(0, '');
  try
    q.Close;
    conn.Close;
    connGLobal.Close;


    Set_IDAC_Connection(conn, trim(Uppercase(EditParam.text)));
    conn.Connected := true;

    if cbUseTestGlobal.Checked then Set_IDAC_Connection(connGlobal, 'TESTGLOBAL')
    else Set_IDAC_Connection(connGlobal, 'GLOBAL');


    conn.Connected := true;
    connglobal.Connected := true;


    //setbar(0, 'opening q...');
    application.ProcessMessages;
    //q.Open;
    setbar(1, 'success: ' + Get_Old_BDE_AliasFromIDAC(conn) + ' on ' + GetCurrentHost(conn) + ' | ' +
      Get_Old_BDE_AliasFromIDAC(connglobal) + ' on ' + GetCurrentHost(connglobal));

    setbar(0, 'totally, yo');
  except on e: exception do
    begin
      showmessage(e.Message);
    end;
  end;


end;

end.

