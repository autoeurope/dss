unit DMUpdate;

interface

uses
  Windows
  , SysUtils
  , Forms
  , Classes
  , Controls
  , Graphics
  , Dialogs
  , IfxQuery
  , DB      
  , Variants

  //AE modules
  , AeCommonfunc_II
  , SippDescribe 
  , DSCalc

  //KBM
  , kbmMemTable

  //Informix
  , IfxConnection
  , IfxCustomDataSet
  , IfxTable

  //Indy
  , IdComponent
  , IdTCPConnection
  , IdTCPClient
  , IdExplicitTLSClientServerBase
  , IdMessageClient
  , IdSMTPBase
  , IdSMTP
  , IdBaseComponent
  , IdMessage
  , IdAttachmentFile;

const
  SQL_GET_VANDC =
    'SELECT "[[basesys]]" basesys, "uid" clientid, "" cryptcc, vcnumb, vchrel, voit1, voit2, voit3, ' +
    'voit1ret, voit2ret, voit3ret, voit4ret, voit1whl, vopsubt, vopstype1, voit2whl, voit3whl, ' +
    'voit4whl, voptax1, voptax2, vcntype, vciata, voptype, vccrcardid, vccrcard, vcccmonth, ' +
    'vcrid, vccomtaken, vccom1, vccom2, vcdisaddamount, affiliate1, affiliate2, vodddtestat, ' +
    'vopcurrency, vopcurrate, vcccauthcode, vopid, vopid2, vopctry1, vopctry2, vopprov1, ' +
    'vopprov2, vopcity1, vopcity2, voploc1, voploc2, voprd1, voiamt5, vcfpover1, voxtp1, voxtp1w, ' +
    'voxtp2w, voxtp3w, voxtp4w, vopper1, vopper2, vchdate, voptxper1, voptxper2, vccashdeposi, ' +
    'vopdate1, vopdate2, voptime1, voptime2, voptime3, vopper3, voit1sw, vopcomt1, vcvpromo1, ' +
    'vcvpromo2, vcvpromo3, vccconfirm, vcclientid, vcvendorattn, vccomments, vccomments1, ' +
    'vcspecrequest, vcarrvair, vcarrvflight, vceta, vcprepay, vcresoffice, vcst1, vcst2, ' +
    'vcst3, vcstdte1, vcstdte2, vcstdte3, vord1, vord2, vord3, vord4, vord5, vord6, vord7, vord8, ' +
    'vord9, vord10, vowd1, vowd2, vowd3, vowd4, vowd5, vopwd6, vopwd7, vopwd8, vopwd9, vopwd10, ' +
    'vopdatecreate, vopdatemodif, vopamt1, vopamt2, vopamt3, vopamt4, vopamt5, vopamt6, vopamt7, ' +
    'vopamt8, vopamt9, origcurr, voiamt1, voiamt2, voiamt3, voiamt4, voprcode, voprt1, voconsort, ' +
    'voconsort2, voconsort3, vcconsortsub, vcconsortsub2, vcconsortsub3, vorecord, voxtp1t, voxtp2, ' +
    'voxtp2t, vocusttype, vccomover1, vouch.homecurr, vouch.homectry, vouch.altresnum1, vchpqbath, ' +
    'vchpqtype, vchpqvch, vchpqvchln, vchpqseq, vchpstat, vchpmanneed, vchpoper, vchptranctry, vchpcity, ' +
    'vchplocation, vchplprintdate, vchpqcccredsale, vchpqnsw1, vchpqnsw2, vchpqnsw3, ' +
    'voxtp3t, ' + // added 8/27/2008 for DH
      // --  new client and agfile fields needed for summary:
  'clvlast lastname, ' +
    'clvfirst firstname,  ' +
    'clvinit initial,  ' +
    'clvaddr1 client_addr1, ' +
    'clvaddr2 client_addr2, ' +
    'clvcity client_city, ' +
    'clvprov client_prov, ' +
    'clvpostcode client_postal, ' +
    'clvphone client_phone, ' +
    'clvsurname crap, ' +
    'upper(clvctry) client_ctry, ' +
    'lower(trim(clemail)) clemail, ' +
    'lower(trim(agemail2)) agemail2 ' +
    'FROM VOUCH, VCHPQUEUE, ' +
    // join for summary:
  'outer (agfile, vclient) ' +
    'WHERE (VCHPQBATH between :bdate1 and :bdate2) AND VCNUMB = VCHPQVCH  AND VCHREL = VCHPQVCHLN ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ' +
    '( (vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) ' + // change to OR 7/16/2007
    // join args:
  'and inumb = vciata and  vcclientid = clvchnum';

  SQL_CHECKSUMTARGET = 'Select sum(  vord4-vopamt2+voit1ret+voit2ret+voit3ret+voit4ret+vord6+vord8+voptax1+vord3) ' +
    ' From vouchandqueue where basesys = :basesys and vchpqbath between :date1 and :date2';

  SQL_CHECKSUMSOURCE = 'SELECT sum(vord4-vopamt2+voit1ret+voit2ret+voit3ret+voit4ret+vord6+vord8+voptax1+vord3) ' +
    ' FROM VOUCH, VCHPQUEUE WHERE VCHPQBATH BETWEEN :date1 and :date2 AND VCNUMB = VCHPQVCH ' +
    ' AND VCHREL = VCHPQVCHLN AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" ' +
    ' and  (VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ' +
    '((vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N"))';


    // old changed 7/21/2011
 { SQL_CheckSumClientSource = 'select sum(vcnumb+vchrel) from ' +
    'vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' +
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2';  }
  {
  SQL_CHECKCOUNTCLIENTSOURCE = 'select count(*) from ' +
    'vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' +
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2';
 }
{  SQL_CheckSumClientTarget = 'Select sum(  gc_voucher+ gc_release ) ' +
    ' From g_client where gc_system = :basesys and gc_batchdt between :date1 and :date2'; }
 {
  SQL_CHECKCOUNTCLIENTTARGET = 'Select count(*) ' +
    ' From g_client where gc_system = :basesys and gc_batchdt between :date1 and :date2';
 }
  {SQL_Get_Clients = 'select 0 gc_key, "UID" gc_uid, trim(upper(clvlast)) gc_lastname, ' +
    'trim(upper(clvfirst)) gc_firstname,  ' +
    'trim(upper(clvinit)) gc_initial, ' +
    'trim(upper(clvsurname)) gc_surname,  ' +
    'trim(upper(clvaddr1)) gc_addr1,  ' +
    'trim(upper(clvaddr2)) gc_addr2, ' +
    'trim(upper(clvprov)) gc_prov,  ' +
    'trim(upper(clvcity)) gc_city, ' +
    'trim(upper(clvpostcode)) gc_postcode,  ' +
    'trim(upper(clvctry)) gc_ctry, ' +
    'trim(upper(clvphone)) gc_phone,  ' +
    'trim(upper(clfax)) gc_fax, ' +
    '"cell" gc_cell,  ' +
    'trim(upper(clemail)) gc_email,  ' +
    '"*PASSW*" gc_password,  ' +
    '"[[basesys]]" gc_system,  ' +
    'vcnumb gc_voucher, ' +
    'vchrel gc_release, ' +
    'trim(upper(vciata)) gc_iata,  ' +
    'vchpqbath gc_batchdt,  ' +
    '"12/31/1899" gc_lastupdt  ' +
    'from vouch, vchpqueue, vclient  ' +
    'where vcntype = "A"  and  ' +
    '(VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( ' +
    '(vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and ' +
    '(vchpstat3 is null OR length(vchpstat3) = 0 or vchpstat3 <> "N")) and ' + // or change 7/16/2007 as above
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    'AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and ' +
    ' vchpqbath between :date1 and :date2 order by vcnumb, vchrel';
    // order by is necessary to elimiate dupes - the vchpqueue join brings back multiples
    }

    // re-do this 7/21/2011 becaus of sync issues - add qClientUpdate to sync against actual vouchers in the whse ..
    SQL_GET_CLIENTS = 'select 0 gc_key, "UID" gc_uid, trim(upper(clvlast)) gc_lastname, ' +
    'trim(upper(clvfirst)) gc_firstname,  ' +
    'trim(upper(clvinit)) gc_initial, ' +
    'trim(upper(clvsurname)) gc_surname,  ' +
    'trim(upper(clvaddr1)) gc_addr1,  ' +
    'trim(upper(clvaddr2)) gc_addr2, ' +
    'trim(upper(clvprov)) gc_prov,  ' +
    'trim(upper(clvcity)) gc_city, ' +
    'trim(upper(clvpostcode)) gc_postcode,  ' +
    'trim(upper(clvctry)) gc_ctry, ' +
    'trim(upper(clvphone)) gc_phone,  ' +
    'trim(upper(clfax)) gc_fax, ' +
    '"cell" gc_cell,  ' +
    'trim(upper(clemail)) gc_email,  ' +
    '"*PASSW*" gc_password,  ' +
    '"[[basesys]]" gc_system,  ' +
    'vcnumb gc_voucher, ' +
    'vchrel gc_release, ' +
    'trim(upper(vciata)) gc_iata,  ' +
    'vchpqbath gc_batchdt,  ' +
    '"12/31/1899" gc_lastupdt  ' +
    'from vouch, vchpqueue, vclient  ' +
    'where clvchnum =:vcnumb and ' +
    'vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN  ' +
    ' order by vcnumb, vchrel';

type
  TUpdateDM = class(TDataModule)
    connGlobal: TIfxConnection;
    dsmtcache: TDataSource;
    sd1: TSIPPDesc;
    connSource: TIfxConnection;
    memIATAWhiteList: TkbmMemTable;
    dsIWHiteList: TDataSource;
    qcsCsFileHIstorySource: TIfxQuery;
    dshistsource: TDataSource;
    dscsHistTarget: TDataSource;
    tblcsfilehistoryTarget: TIfxTable;
    connTarget: TIfxConnection;
    tblcsfilesTarget: TIfxTable;
    tblcscontactsTarget: TIfxTable;
    qcsContactsSource: TIfxQuery;
    dscsfilesSource: TDataSource;
    QcscommentsSource: TIfxQuery;
    QcsfilesSource: TIfxQuery;
    tblcscmntsTarget: TIfxTable;
    QSummarySource: TIfxQuery;
    dsTarget_summary: TDataSource;
    Tbl_Target_Summary: TIfxTable;
    dsSUmmarySOurce: TDataSource;
    dsc: TDSCalc;
    dscEdit: TDSCalc;
    dsQVandC_FIXREC: TDataSource;
    QSummaryFIXREC: TIfxQuery;
    QVandQ_FIXREC: TIfxQuery;
    dsSource: TDataSource;
    QSource: TIfxQuery;
    dsSummaryFIXREC: TDataSource;
    Tbl_Target_VQ: TIfxTable;
    dsTargetVQ: TDataSource;
    qsourceVENDOR: TIfxQuery;
    dsSourceVENDOR: TDataSource;
    tbltargetVENDOR: TIfxTable;
    dsTargetVENDOR: TDataSource;
    ds_dc_email: TDataSource;
    q_dc_email: TIfxQuery;
    memdcmailUpdt: TkbmMemTable;
    memdcmailUpdteml_system: TStringField;
    memdcmailUpdteml_email: TStringField;
    memdcmailUpdteml_pu_ctry: TStringField;
    memdcmailUpdteml_booking_count: TIntegerField;
    memdcmailUpdteml_profit: TFloatField;
    memdcmailUpdteml_cust_class: TStringField;
    memdcmailUpdteml_churn_class: TStringField;
    memdcmailUpdteml_is_multi_ctry: TStringField;
    ds_memdcmailUpdt: TDataSource;
    QgetDCMAILUpdateInfo: TIfxQuery;
    QclientSource: TIfxQuery;
    qClientUpdateDriver: TIfxQuery;
    dsTargetClient: TDataSource;
    tbl_target_client: TIfxTable;
    dsClientUpdateDriver: TDataSource;
    dscscommentsSource: TDataSource;
    dscsContactsSource: TDataSource;
    dsClientSource: TDataSource;
    Msg: TIdMessage;
    SMTP: TIdSMTP;
    procedure DataModuleDestroy(Sender: TObject);
    procedure tbl_target_clientBeforeOpen(DataSet: TDataSet);
    procedure Tbl_Target_SummaryBeforeOpen(DataSet: TDataSet);
    procedure Tbl_Target_VQBeforeOpen(DataSet: TDataSet);
    procedure QSourceBeforeOpen(DataSet: TDataSet);
  private     
    //BzDateRec: BizDateRec;
    { Private declarations }
  public
    procedure Initsdq;
    procedure FillWhiteList();
    procedure CopyCS_HIST(BaseSys: string; startDT, EndDt: TDate);
    procedure InsertCS_HIST();
    procedure SetConn(InConn: TIfxCOnnection);
    procedure InsertUpdateCSContact();
    procedure InsertCSComments();
    procedure InsertUpdateCSFiles();
    procedure CopyCS(BaseSys: string; startDT, EndDt: TDate);
    procedure CS_CheckSums(dstart, dend: TDate; basesys: string);
    procedure loopUpdateSummary;   
    procedure UpdateOperatorList(sys, Oper: string);
    procedure FixOnePQRecord;
    procedure FIXROPENTARGET;
    procedure VQCopyOneRecord;
    procedure VQCopyForDates(BaseSys: string; startDT, EndDT: TDate);
    procedure VCCopyDate;
    procedure VQTargetOpen;
    procedure VQCheckSums(dstart, dend: TDate; basesys: string);
    procedure UpdateStats;
    procedure UpdateIATAS(InSystem: string);
    procedure Update_dcemail();
    procedure Get_dcMail_data(date1, date2: TDate; basesystem: string);
    procedure getDCEmlUpdateInfo(InEmailAddr, InSystem: string);
    procedure SetDCEmlUpdateInfo();
    procedure DeleteObsoleteDCMAILRecords(sSystem, sEmlAddr: String);
    procedure GetDCmailDates;
    procedure CopyOneClient;
    procedure ClientDateCopy;
    procedure ClientCopyForDates(BaseSys: string; startDT, EndDT: TDate; Ctype: string);
    procedure OpenClientTarget;
    procedure CheckClientSums(dStart, dEnd: TDate; baseSys: string);
    procedure EMAIL(sSubject, sBody: string);

    function GetOperatorVendor(VouchVOXTP1: Integer; InSys: string): string;
    function SippDescExecute(product, sipp, lang:string; methodtype:TAlgType):boolean;
    function OpenSourceCS_HIST(InBaseSys: string; date1, date2: TDate): boolean;
    function OpenTargetCS(): boolean;
    function OpenTargetCS_HIST(): boolean;
    function OpenSourceCS(InBaseSys: string; date1, date2: TDate): boolean;
    function CS_record_exists(TableName, InBaseSys, FileType: string; res, rel, commentsequence: integer): boolean;
    function UpdateSummary(): string;
    function Insert_voucher_summary(): string;
    function OpenSourceTarget_Summary(): boolean;
    function Get_Product_Code_Rec(basesys, vopsubt, vopstype1: string): jProdRec;
    function Edit_voucher_summary(): string;
    function Update_VCNTYPE_Summary(bsys: string; vch, rel: Integer): string;
    function EditVQ(): string;
    function insertVQ(): string;
    function OpenSourceVQ(InBaseSys: string; date1, date2: TDate): boolean;
    function Update_VCNTYPE(bsys: string; vch, rel: Integer): string;
    function SummaryCheckSum(): string;
    function AutoUpdate_dcemail(basesystem: string): string;
    function GetLastDCEMLUpdate(baseSYS: string): TDate;
    function GetLastVQUpdate(baseSYS: string): TDate;
    //function GetBizDates(WhseConn: TIfxConnection): FormDSSFeed.BizDateRec;
    function SetLatestDCEMLUpdate(InbaseSYS: string; InDate: TDate): boolean;
    function FindAndUpdateClient(basesys: string; voucher: Integer): boolean;
    function OpenSourceClientForSync(InBaseSys: string; date1, date2: TDate): boolean;
    function insert_g_client(): string;
    function StripIt(InStr: string): string;
    { Public declarations }
  end;

var
  UpdateDM: TUpdateDM;

implementation

{$R *.dfm}

uses main_update;

procedure TUpdateDM.DataModuleDestroy(Sender: TObject);
begin
  dsmtcache.DataSet := nil;
end;

function TUpdateDM.GetOperatorVendor(VouchVOXTP1: Integer; InSys: string): string;
var theSQL: string;
  reccount: Integer;
  q: TIfxQuery;
begin
  result := '';
  InSys := trim(uppercase(InSys));
  if InSys = 'USA' then InSys := 'US';

  if not connGLobal.Connected then
  begin
    try
      set_idac_connection(connGLobal, 'GLOBAL');
      connglobal.Open;
    except
      exit;
    end;
  end;

  if not (connglobal.Connected) then exit;

  theSQL := 'Select g_opctrinfotag.opctvendor, opctrit_basesys, oplactive ' +
    'from  g_operator, g_opctr, g_opcity, g_oploc1, g_oploc1addr, g_opctrinfotag ' +
    'where ' +
    'opserial=opctoplink and opctserial=opctctrylink and opcityserial=oplloccityid ' +
    'and oplserial=opladkey1 ' +
    'and oplserial = :voxtp1 ' +
    'and ((opctrit_id = opctserial and opctrit_basesys = :sys) ' +
    'or ' +
    '((opctrit_id = opctserial and opctrit_basesys = "*" ) and not exists ' +
    '(select opctrit_id from g_opctrinfotag where ' +
    'opctrit_id = opctserial  and opctrit_basesys =  :sys ))) ' +
    'order by 2 DESC, 3 DESC; ';

  try
    q := GetIDACQuery(connGlobal);
    try
      q.sql.clear;
      q.sql.Add(theSQL);
      q.ParamByName('voxtp1').AsInteger := VouchVOXTP1;
      q.ParamByName('sys').AsString := InSys;
      q.Active := true;
      if (q.IsEmpty) then exit
      else
      begin
        q.First;
        result := q.Fields[0].AsString;
      end;
    except on e: exception do
      begin
        savelogfile('dss_upd', 'GetOperatorVendor fail --> ' + E.Message);
      end;
    end;
  finally
    if assigned(q) then FreeAndNil(q);
  end;
end;

function TUpdateDM.SippDescExecute(product, sipp, lang:string; methodtype:TAlgType):boolean;
begin
  sd1.Product := product;
  sd1.SIPP := sipp;
  sd1.Language := lang;
  sd1.MethodType := methodtype;
  result := sd1.Execute;
end;

procedure TUpdateDM.Initsdq;
var
  errstr:string;
begin
  try
    try
      connGlobal.Close;
      set_idac_connection(connGLobal, 'GLOBAL');
      connglobal.Open
    except
      exit;
    end;

    if not connglobal.Connected then
    begin
      exit;
    end;

    SD1.Connection := connGLobal;
    sd1.Language := 'ENG';
    sd1.MethodType := mtCached;
    sd1.Product := 'car';
    sd1.ValidDate := trunc(now);

    if SD1.Initialize(errstr) then
      FormDssFeed.memo1.Lines.add('init sd1 OK')
    else
    begin
      FormDssFeed.memo2.Lines.add('sd1 init err --> ' + errstr);
      exit;
    end;

    FormDssFeed.dbgrid3.DataSource := dsmtcache;
    dsmtcache.DataSet := sd1.FMEM;
  finally
    connglobal.Close;
  end;
end;

procedure TUpdateDM.FillWhiteList();
var MyQ: TifxQuery;
  theSQL, bsys: string;
  i, x: integer;
begin
  FormDssFeed.dbgrid4.DataSource := nil;

  try // finally
    MyQ := GetIdacQuery(connSource);
    try
      MemIATAWhiteList.EmptyTable;
    except on e: exception do
      begin
        raise(exception.create('failed to empty TA/DC list -->' + e.Message));
        exit;
      end;
    end;

    for i := 0 to FormDssFeed.BasesysList.Count - 1 do
    begin
      if FormDssFeed.bKill then break;
      bsys := trim(Uppercase(FormDssFeed.basesyslist[i]));
                             // add spaces so other system flags don't get truncated
      theSQL := 'Select "' + bsys + '  " basesys, vendor from apvenmast where vendor_group is not null and charge_code = "D"';

      myQ.Close;
      myQ.SQL.Clear;
      connSource.Close;
      FormDssFeed.memo1.Lines.add(bsys + ' white list add');

      try
        Set_IDAC_Connection(connSource, bsys);
        connSource.Open;
        FormDssFeed.caption := 'connected to ' + GetCurrentDB(connSource);
        FormDssFeed.color := GetDBColor(bsys);
      except on e: exception do
        begin
          raise(exception.Create(e.Message));
          FormDssFeed.caption := '';
          continue;
        end;
      end;

      if not connSource.connected then
      begin
        raise(exception.Create('FillWhiteList - not connected to ' + bsys));
        continue;
      end;

      application.ProcessMessages;
      try
        FormDssFeed.memo1.Lines.add(bsys + ' white list open');
        MyQ.SQL.add(theSQL);
        MyQ.Open;
      except on e: exception do
        begin
          raise(exception.create('FillWhiteList -> ' + E.Message));
        end;
      end;

      if MyQ.IsEmpty then
      begin
        raise(exception.create('FillWhiteList -> Empty dataset for [' + bsys + ']'));
        continue;
      end;

      FormDssFeed.memo1.Lines.add(bsys + ' white list load');
      try
        if MemIATAWhiteList.IsEmpty then
          MemIATAWhiteList.LoadFromDataSet(MyQ, [mtcpoStructure, mtcpoProperties, mtcpoLookup, mtcpoCalculated])
        else
          MemIATAWhiteList.LoadFromDataSet(MyQ, [mtcpoCalculated, mtcpoAppend]);
      except on e: exception do
        begin
          raise(exception.create('failed to fill TA/DC list -->' + e.Message));
          exit;
        end;
      end;
    end; // for loop

    FormDssFeed.memo1.Lines.add(bsys + ' white list to uppercase');
    MemIATAWhiteList.first;
    x := 0;
    x := memIataWhiteList.RecordCount;

    try
      while not MemIATAWhiteList.eof do
      begin
        inc(i);
        try FormDssFeed.SetBar1('upper: ' + IntToStr(i) + ' of ' + inttostr(x)); except; end;
        MemIATAWhiteList.edit;
        MemIATAWhiteList.fields[0].AsString := trim(uppercase(MemIATAWhiteList.fields[0].AsString));
        MemIATAWhiteList.fields[1].AsString := trim(uppercase(MemIATAWhiteList.fields[1].AsString));
        MemIATAWhiteList.post;
        MemIATAWhiteList.next;
        application.ProcessMessages;
      end;
    except on e: exception do
      begin
        MemIATAWhiteList.Cancel;
        raise(exception.create('failed to fill TA/DC list -->' + e.Message));
      end;
    end;

    memIATAWHITELIST.SortOn('basesys;vendor;', memIATAWHITELIST.SortOptions);
  finally
    if assigned(MyQ) then FreeAndNil(MyQ);
    connSource.connected := false;
    FormDssFeed.color := clSilver;
    try FormDssFeed.SetBar1('fill white list disconnected'); except; end;
    FormDssFeed.dbgrid4.DataSource := dsIWHiteList;
  end;
end;

function TUpdateDM.OpenSourceCS_HIST(InBaseSys: string; date1, date2: TDate): boolean;
var Sys: string;
begin
  result := false;
  FormDssFeed.dbgrid1.DataSource := dshistsource;
  FormDssFeed.dbgrid3.DataSource := dscsHistTarget;

  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    Sys := GetCurrentDB(connSource);
    FormDssFeed.caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDssFeed.caption := '';
      exit;
    end;
  end;

  try
    qcsCsFileHIstorySource.Close;

    qcsCsFileHIstorySource.SQL.Clear; // may need index on csfiles if this is slow
    qcsCsFileHIstorySource.SQL.Text := 'Select "' + inbasesys + '" csfs_basesys, * From csfilehistory ' +
      'where csfs_insertdtt between :date1 and :date2';

    qcsCsFileHIstorySource.ParamByName('date1').AsDate := trunc(date1);
    qcsCsFileHIstorySource.ParamByName('date2').AsDate := trunc(date2);

    FormDssFeed.memo2.Lines := qcsCsFileHIstorySource.SQL;
    FormDssFeed.memo2.Lines.add('date1: ' + qcsCsFileHIstorySource.ParamByName('date1').AsString);
    FormDssFeed.memo2.Lines.add('date2: ' + qcsCsFileHIstorySource.ParamByName('date2').AsString);
    FormDssFeed.caption := 'connecting to csfilehistory on ' + Sys;
    qcsCsFileHIstorySource.Open;

    FormDssFeed.caption := 'connected to ' + Sys;
    FormDssFeed.memo2.Lines.add('connected to ' + Sys);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDssFeed.caption := '';
      exit;
    end;
  end;
  result := true;
end;

procedure TUpdateDM.CopyCS_HIST(BaseSys: string; startDT, EndDt: TDate);
var sbasesys, errstr: string;
begin // assume source is open and has data
  if not (connTarget.Connected) then
  begin
    FormDssFeed.memo1.Lines.add('cs hist --> conn target not connected');
    raise(exception.Create('cs hist --> conn target not connected'));
    exit;
  end;

  if not (qcsCsFileHistorySource.active) then
  begin
    FormDssFeed.memo1.Lines.add('cs history --> a source query is not active');
    raise(exception.Create('cs history --> a source query is not active'));
    exit;
  end;

  if qcsCsFileHistorySource.IsEmpty then exit;
  qcsCsFileHistorySource.First;

  while not (qcsCsFileHistorySource.Eof) do
  begin
    if FormDssFeed.bkill then break;

    sbasesys := '';
    errstr := '';
    sbasesys := qcsCsFileHistorySource.FieldByName('csfs_basesys').AsString;

    try
      InsertCS_Hist(); // need to double check
    except on e: exception do
      begin
        FormDssFeed.memo1.Lines.add('CopyCS_hist InsertCS_Hist --> ' + e.Message + ']');
        raise(exception.Create('CopyCS_hist InsertCS_Hist --> [' + e.Message + ']'));
        break;
      end;
    end;
    qcsCsFileHistorySource.Next;
  end;

  FormDssFeed.CS_Hist_CheckSums(startDT, endDT, BaseSys);
  FormDssFeed.memo2.Lines.add('done cs history upload');
end;

procedure TUpdateDM.InsertCS_HIST();
var
  i: Integer;
begin
  if not (connTarget.Connected) then
  begin
    raise(exception.Create('InsertCS_Hist --> conn target not connected'));
    exit;
  end;

  if not tblcsfilehistoryTarget.Active then
  begin
    try
      tblcsfilehistoryTarget.Active := true;
    except on e: exception do
      begin
        raise(exception.Create('InsertCS_Hist --> can not activate tblcsfilehistoryTarget --> ' + e.Message));
        exit;
      end;
    end;
  end;

  if not tblcsfilehistoryTarget.Active then
  begin
    raise(exception.Create('InsertCS_Hist tblcsfilehistoryTarget not active.'));
    exit;
  end;

  if not qcsCsFileHIstorySource.Active then
  begin
    exit;
  end;

  try
    tblcsfilehistoryTarget.Insert;

    for i := 0 to qcsCsFileHIstorySource.fieldcount - 1 do
      tblcsfilehistoryTarget.fieldbyname(qcsCsFileHIstorySource.fields[i].fieldname).value :=
        qcsCsFileHIstorySource.fields[i].value;

    tblcsfilehistoryTarget.Post;
  except on e: exception do
    begin
      tblcsfilehistoryTarget.cancel;
      raise(exception.Create('InsertCS HIST 34 --> ' + e.Message));
    end;
  end;
end;

function TUpdateDM.OpenTargetCS(): boolean;
begin
  result := false;
  try
    SetConn(connTarget);
    tblcsfilesTarget.Open;
    tblcscmntsTarget.Open;
    tblcscontactsTarget.Open;
    FormDssFeed.caption := 'connected to: ' + GetCurrentDB(connTarget);
    FormDssFeed.SetBar0('connected to: ' + GetCurrentDB(connTarget));
  except on e: exception do
    begin
      FormDssFeed.memo1.Lines.add('OpenTargetCS connect fail --> ' + E.Message);
      savelogfile('dss_upd', 'OpenTargetCS connect fail --> ' + E.Message);
      raise(exception.Create('OpenTargetCS connect fail --> ' + e.Message));
      exit;
    end;
  end;

  if not ((tblcsfilesTarget.Active) or (tblcscmntsTarget.Active) or
    (tblcscontactsTarget.Active)) then
  begin
    savelogfile('dss_upd', 'failed to open CS tables.');
    raise(exception.Create('failed to open CS.'));
  end;
end;

function TUpdateDM.OpenTargetCS_HIST(): boolean;
begin
  result := false;

  try
    SetConn(connTarget);
    tblcsfilehistoryTarget.Open;
    FormDssFeed.caption := 'connected to: ' + GetCurrentDB(connTarget);
    FormDssFeed.SetBar0('connected to: ' + GetCurrentDB(connTarget));
  except on e: exception do
    begin
      FormDssFeed.memo1.Lines.add('OpenTargetCS_HIST connect fail --> ' + E.Message);
      savelogfile('dss_upd', 'OpenTargetCS_HIST connect fail --> ' + E.Message);
      raise(exception.Create('OpenTargetCS_HIST connect fail --> ' + e.Message));
      exit;
    end;
  end;

  if not (tblcsfilehistoryTarget.Active) then
  begin
    savelogfile('dss_upd', 'failed to open CS History target table.');
    raise(exception.Create('failed to open CS History target table.'));
  end;
end;

procedure TUpdateDM.SetConn(InConn: TIfxCOnnection);
begin
  if not InConn.Connected then
  begin // "setconn(connTarget)" is called Form.onCreate, therefore, should set fine in the case
         // where the db was moved etc
    try
      InConn.Connected := true;
    except on e: exception do
      begin
        raise(Exception.Create('setConn --> ' + e.Message));
      end;
    end;
  end;
end;

procedure TUpdateDM.InsertUpdateCSContact();
var i: Integer;
  insertORupdate, logParams: string;
begin
  if not (connTarget.Connected) then
  begin
    raise(exception.Create('InsertUpdateCSContact --> conn target not connected'));
    exit;
  end;

  if not tblcscontactsTarget.Active then
  begin
    try
      tblcscontactsTarget.Active := true;
    except on e: exception do
      begin
        raise(exception.Create('InsertUpdateCSContact --> can not activate tblcscontactsTarget --> ' + e.Message));
        exit;
      end;
    end;
  end;

  if not tblcscontactsTarget.Active then
  begin
    raise(exception.Create('InsertUpdateCSContact TblcscontactsTarget Not Active.'));
    exit;
  end;

  if not qcsContactsSource.Active then
  begin
    raise(exception.Create('InsertUpdateCSContact qcsContactsSource Not Active.'));
    exit;
  end;

  if qcsContactsSource.IsEmpty then
  begin
    exit;
  end;

  try // for cscontact, I expect commentsequence to be set to zero
    logParams := qcsContactsSource.FieldByName('csclt_basesys').AsString + ' ' +
      qcsContactsSource.FieldByName('csclt_filetype').AsString + ' ' +
      qcsContactsSource.FieldByName('csclt_vouch').AsString + '-' +
      qcsContactsSource.FieldByName('csclt_release').AsString;

    if CS_record_exists('cscontact',
      qcsContactsSource.FieldByName('csclt_basesys').AsString,
      qcsContactsSource.FieldByName('csclt_filetype').AsString,
      qcsContactsSource.FieldByName('csclt_vouch').AsInteger,
      qcsContactsSource.FieldByName('csclt_release').AsInteger,
      0)
      then InsertORUpdate := 'UPDATE'
    else
      InsertORUpdate := 'INSERT';
  except on e: Exception do
    begin
      raise(exception.Create('InsertUpdateCSContact --> ' + e.message));
      exit;
    end;
  end;

  try
    if InsertORUpdate = 'INSERT' then
    begin
      tblcscontactsTarget.insert;
      FormDssFeed.memo2.Lines.Add('insert: ' + logParams);
    end
    else if InsertORUpdate = 'UPDATE' then
    begin
      tblcscontactsTarget.edit;
      FormDssFeed.memo2.Lines.Add('edit: ' + logParams);
    end
    else
    begin
      raise(exception.Create('InsertUpdateCSContact --> [' + InsertORUpdate + '] not a valid update/insert type'));
      exit;
    end;

    for i := 0 to qcsContactsSource.fieldcount - 1 do
      tblcscontactsTarget.fieldbyname(qcsContactsSource.fields[i].fieldname).value := qcsContactsSource.fields[i].value;

    tblcscontactsTarget.Post;
  except on e: exception do
    begin
      tblcscontactsTarget.cancel;
      raise(exception.Create('InsertUpdateCSFiles 10 --> ' + e.Message));
    end;
  end;
end;  

function TUpdateDM.OpenSourceCS(InBaseSys: string; date1, date2: TDate): boolean;
var strSys: string;
begin
  result := false;
  FormDssFeed.dbgrid1.DataSource := dscsfilesSource;
  FormDssFeed.dbgrid3.DataSource := dscscommentsSource;
  FormDssFeed.dbgrid2.DataSource := dscsContactsSource;

  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    strSys := GetCurrentDB(connSource);
    FormDssFeed.caption := 'connected to ' + strSys;
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDssFeed.caption := '';
      exit;
    end;
  end;

  try
    qcsContactsSource.Close;
    QcscommentsSource.Close;
    QcsfilesSource.Close;

    QcsfilesSource.SQL.Clear; // may need index on csfiles if this is slow
    QcsfilesSource.SQL.Text := 'Select "' + inbasesys + '" cs_basesys, * From csfiles where cs_filestat = "C" and cs_date_c between :date1 and :date2';

    QcsfilesSource.ParamByName('date1').AsDate := trunc(date1);
    QcsfilesSource.ParamByName('date2').AsDate := trunc(date2);

    FormDssFeed.memo2.Lines := QcsfilesSource.SQL;
    FormDssFeed.memo2.Lines.add('date1: ' + QcsfilesSource.ParamByName('date1').AsString);
    FormDssFeed.memo2.Lines.add('date2: ' + QcsfilesSource.ParamByName('date2').AsString);
    FormDssFeed.caption := 'connecting to csfiles on ' + strSys;
    QcsfilesSource.Open;

    FormDssFeed.caption := 'connecting to cscomments on ' + strSys;
    QcscommentsSource.SQL.Clear;
    QcscommentsSource.SQL.text := 'Select "' + inbasesys + '" cscmnt_basesys, * From cscomments where cscmntresnum = :cs_res and ' +
      'cscmtresrel = :cs_rel and cscmntfiletype = :cs_filetype';
    QcscommentsSource.Open;

    FormDssFeed.caption := 'connecting to cscontacts on ' + strSys;
    qcsContactsSource.SQL.Clear;
    qcsContactsSource.SQL.Text := 'Select "' + inbasesys + '" csclt_basesys, * From cscontact where csclt_vouch = :cs_res and ' +
      'csclt_release = :cs_rel and csclt_filetype = :cs_filetype';
    qcsContactsSource.Open;

    FormDssFeed.caption := 'connected to ' + strSys;
    FormDssFeed.memo2.Lines.add('connected to ' + strSys);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDssFeed.caption := '';
      exit;
    end;
  end;
  result := true;
end; 

function TUpdateDM.CS_record_exists(TableName, InBaseSys, FileType: string; res, rel, commentsequence: integer): boolean;
var MyQuery: TIfxQuery;
  theSQL, sres, srel, sseq: string;
begin
  // for csfiles, I expect commentsequence to be set to zero
  // for cscomments, res and rel should be set to zero and commentsequence > 0 and type = ''
  // for cscontact, I expect commentsequence to be set to zero
  result := false;

  if not (connTarget.Connected) then
  begin
    raise(exception.Create('CS_record_exists --> conn target not connected'));
    exit;
  end;

  try
    sres := IntTOStr(res);
    srel := IntToStr(rel);
    sseq := IntToStr(commentsequence);
  except on e: exception do
    begin
      raise(exception.Create('CS_record_exists --> ' + e.Message));
      exit;
    end;
  end;

  InBaseSys := trim(Uppercase(InBaseSys));
  TableName := trim(Uppercase(TableName));
  FileType := trim(Uppercase(FileType));

  theSQL := '';
  if TableName = 'CSFILES' then // unique index: cs_basesys,cs_res,cs_rel,cs_filetype
    theSQL := 'Select cs_res from csfiles where cs_basesys = "' +
      InBaseSys + '" and cs_res = ' + sres + ' and cs_rel = ' + srel +
      ' and cs_filetype = "' + FileType + '"'

  else if TableName = 'CSCOMMENTS' then // unique index: (cscmnt_basesys,cscmtseq)
    theSQL := 'Select cscmnt_basesys from cscomments where cscmnt_basesys = "' +
      InBaseSys + '" and cscmtseq = ' + sseq

  else if TableName = 'CSCONTACT' then // unique index: (csclt_basesys,csclt_vouch,csclt_release,csclt_filetype)
    theSQL := 'Select csclt_basesys from cscontact where csclt_basesys = "' +
      InBaseSys + '" and csclt_vouch = ' + sres + ' and csclt_release = ' + srel +
      ' and csclt_filetype = "' + FileType + '"'
  else
  begin
    raise(exception.Create('CS_record_exists --> invalid table name.'));
    exit;
  end;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      FormDssFeed.memo2.Lines.add('cs record exists sql [' + theSQL + ']');
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then
      begin
        FormDssFeed.memo2.Lines.add('no record found for cs sys[' + InBaseSys + '] ftype[' + FileType + '] res[' + sres + '-' + srel + '] cmntseq[' + sseq + ']');
        exit;
      end
      else
      begin
        FormDssFeed.memo2.Lines.add('record found OK for sys[' + InBaseSys + '] ftype[' + FileType + '] res[' + sres + '-' + srel + '] cmntseq[' + sseq + ']');
        result := true;
      end;
    except on e: exception do
      begin
        result := false;
        raise(exception.Create('CS_record_exists fail OPEN --> sys: ' + InBaseSys + ' --> ' + E.Message));
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;   

procedure TUpdateDM.InsertCSComments();
var
  i: Integer;
  insertORupdate: string;
begin
  if not (connTarget.Connected) then
  begin
    raise(exception.Create('InsertCSComments --> conn target not connected'));
    exit;
  end;

  if not tblcscmntsTarget.Active then
  begin
    try
      tblcscmntsTarget.Active := true;
    except on e: exception do
      begin
        raise(exception.Create('InsertCSComments --> can not activate tblcscmntsTarget --> ' + e.Message));
        exit;
      end;
    end;
  end;

  if not tblcscmntsTarget.Active then
  begin
    raise(exception.Create('InsertCSComments tblcscmntsTarget not active.'));
    exit;
  end;

  if not QcscommentsSource.Active then
  begin
    exit;
  end;

  QcscommentsSource.First;
  while not QcscommentsSource.EOF do
  begin
    // for cscomments, res and rel should be set to zero and commentsequence > 0 and type = ''
    if CS_record_exists('cscomments',
      QcscommentsSource.FieldByName('cscmnt_basesys').AsString,
      '', // filetype
      0, 0, // res, release
      QcscommentsSource.FieldByName('cscmtseq').AsInteger)
      then
    begin
      FormDssFeed.memo2.Lines.Add('comment record exists. SKIPPED -> ');
      QcscommentsSource.Next;
      continue;
    end;

    try
      tblcscmntsTarget.Insert;

      for i := 0 to QcscommentsSource.fieldcount - 1 do
        tblcscmntsTarget.fieldbyname(QcscommentsSource.fields[i].fieldname).value := QcscommentsSource.fields[i].value;

      tblcscmntsTarget.Post;
    except on e: exception do
      begin
        tblcscmntsTarget.cancel;
        raise(exception.Create('InsertCSComments 10 --> ' + e.Message));
        break;
      end;
    end;
    QcscommentsSource.Next;
  end; // while
end;   

procedure TUpdateDM.InsertUpdateCSFiles();
var i: Integer;
  insertORupdate, asdf: string;
begin
  if not (connTarget.Connected) then
  begin
    raise(exception.Create('InsertUpdateCSFiles --> conn target not connected'));
    exit;
  end;

  if not tblcsfilesTarget.Active then
  begin
    try
      tblcsfilesTarget.Active := true;
    except on e: exception do
      begin
        raise(exception.Create('InsertUpdateCSFiles --> can not activate tblcsfilestarget --> ' + e.Message));
        exit;
      end;
    end;
  end;

  if not QcsfilesSource.Active then
  begin
    raise(exception.Create('InsertUpdateCSFiles --> QcsfilesSource not active '));
    exit;
  end;

  if QcsfilesSource.IsEmpty then exit;

  try
    // for csfiles, I expect commentsequence to be set to zero
    asdf := '';
    asdf := QcsfilesSource.FieldByName('cs_basesys').AsString + ' ' +
      QcsfilesSource.FieldByName('cs_filetype').AsString + ' ' +
      QcsfilesSource.FieldByName('cs_res').AsString + '-' +
      QcsfilesSource.FieldByName('cs_rel').AsString;

    if CS_record_exists('csfiles',
      QcsfilesSource.FieldByName('cs_basesys').AsString,
      QcsfilesSource.FieldByName('cs_filetype').AsString,
      QcsfilesSource.FieldByName('cs_res').AsInteger,
      QcsfilesSource.FieldByName('cs_rel').AsInteger,
      0)
      then InsertORUpdate := 'UPDATE'
    else
      InsertORUpdate := 'INSERT';
  except on e: Exception do
    begin
      raise(exception.Create('InsertUpdateCSFiles --> ' + e.message));
      exit;
    end;
  end;

  try
    if InsertORUpdate = 'INSERT' then
    begin
      tblcsfilesTarget.insert;
      FormDssFeed.memo2.Lines.Add('insert: ' + asdf);
    end
    else if InsertORUpdate = 'UPDATE' then
    begin
      tblcsfilesTarget.edit;
      FormDssFeed.memo2.Lines.Add('edit: ' + asdf);
    end
    else
    begin
      raise(exception.Create('InsertUpdateCSFiles --> [' + InsertORUpdate + '] not a valid update/insert type'));
      exit;
    end;

    for i := 0 to QcsfilesSource.fieldcount - 1 do
      tblcsfilesTarget.fieldbyname(QcsfilesSource.fields[i].fieldname).value := QcsfilesSource.fields[i].value;

    tblcsfilesTarget.Post;
  except on e: exception do
    begin
      tblcsfilesTarget.cancel;
      raise(exception.Create('InsertUpdateCSFiles 10 --> ' + e.Message));
    end;
  end;
end;

procedure TUpdateDM.CopyCS(BaseSys: string; startDT, EndDt: TDate);
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data
  if not (connTarget.Connected) then
  begin
    FormDssFeed.memo1.Lines.add('loop cs --> conn target not connected');
    raise(exception.Create('loop cs --> conn target not connected'));
    exit;
  end;

  if not ((QcsfilesSource.active) and (QcscommentsSource.Active) and (qcsContactsSource.Active))
    then
  begin
    FormDssFeed.memo1.Lines.add('loop cs --> a source query is not active');
    raise(exception.Create('loop cs --> a source query is not active'));
    exit;
  end;

  if QcsfilesSource.IsEmpty then exit;
  QcsfilesSource.First;

  while not (QcsfilesSource.Eof) do
  begin
    if FormDssFeed.bkill then break;

    sbasesys := '';
    svcnumb := '';
    svchrel := '';
    ivchrel := 0;
    errstr := '';
    sbasesys := QcsfilesSource.FieldByName('cs_basesys').AsString;
    svcnumb := QcsfilesSource.FieldByName('cs_res').AsString;
    svchrel := QcsfilesSource.FieldByName('cs_rel').AsString;
    ivcnumb := QcsfilesSource.FieldByName('cs_res').AsInteger;
    ivchrel := QcsfilesSource.FieldByName('cs_rel').AsInteger;

    try
      InsertCSComments(); // need to double check
    except on e: exception do
      begin
        FormDssFeed.memo1.Lines.add('CopyCS InsertCSComments xx --> ' + e.Message + ']');
        raise(exception.Create('CopyCS InsertCSComments xx --> [' + e.Message + ']'));
        break;
      end;
    end;

    try
      InsertUpdateCSContact();
    except on e: exception do
      begin
        FormDssFeed.memo1.Lines.add('CopyCS InsertCSContact XXX --> ' + e.Message);
        raise(exception.Create('CopyCS InsertCSContact XXX --> ' + e.Message));
        break;
      end;
    end;

    try
      InsertUpdateCSFiles();
    except on e: exception do
      begin
        FormDssFeed.memo1.Lines.add('CopyCS InsertUpdateCSFiles x --> ' + e.Message);
        raise(exception.Create('CopyCS InsertUpdateCSFiles x --> ' + e.Message));
        break;
      end;
    end;
    QcsfilesSource.Next;
  end;

  CS_CheckSums(startDT, endDT, BaseSys);
  FormDssFeed.memo2.Lines.add('done cs');
end;

procedure TUpdateDM.CS_CheckSums(dstart, dend: TDate; basesys: string);
var sourceq, targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  temp, theSQL: string;
begin // assuming connected
  sourceSUM := 0.0;
  TargetSUM := 0.0;
  basesys := trim(Uppercase(basesys));
  theSQL := 'Select sum(cs_res+cs_rel+cs_rid) From csfiles where cs_filestat = "C" and cs_date_c between :date1 and :date2';

  try
    try
      sourceq := GetIDACQuery(connSource);
      sourceq.sql.Add(theSQL);
      sourceq.ParamByName('date1').AsDate := trunc(dstart);
      sourceq.ParamByName('date2').AsDate := trunc(dend);
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourcesum := 0.0
      else sourcesum := sourceq.Fields[0].AsFloat;
    except on e: exception do
      begin
        sourcesum := 0;
        savelogfile('dss_upd', 'CS_checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        FormDssFeed.memo1.Lines.add('CS_checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(sourceq);
  end;

  theSQL := 'Select sum(cs_res+cs_rel+cs_rid) From csfiles where cs_basesys = :basesys and ' +
    'cs_filestat = "C" and cs_date_c between :date1 and :date2';

  try
    try
      targetq := GetIDACQuery(connTarget);
      targetq.sql.Add(theSQL);
      targetq.ParamByName('date1').AsDate := trunc(dstart);
      targetq.ParamByName('date2').AsDate := trunc(dend);
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      if (targetq.IsEmpty) then targetsum := 0.0
      else targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := 0;
        savelogfile('dss_upd', 'cs_checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        FormDssFeed.memo1.Lines.add('cs_checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(targetq);
  end;

  temp := '';
  if targetsum = sourcesum then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' CS checksum OK! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDssFeed.LogList.Add(temp);
    except;
    end;
  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' CS checksum Fail! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDssFeed.LogList.Add(temp);
    except;
    end;
    FormDssFeed.memo1.Lines.add(temp);
  end;
  FormDssFeed.memo2.Lines.add(temp);
end; 

procedure TUpdateDM.loopUpdateSummary;
var
  Issomething: Boolean;
  i: Integer; // one time to get all data for the first rev
begin
  FormDssFeed.memo2.Lines.add('loopUpdateSummary stub');
  exit;

  isSomething := false;
  i := 0;

  while not issomething do
  begin
    try
      QSummarySource.Active := false;
      Tbl_Target_Summary.Active := false;
    except;
    end;

    if (FormDssFeed.bkill) then break;

    try
      OpenSourceTarget_Summary();
    except on e: exception do
      begin
        FormDssFeed.memo2.Lines.add(e.message);
        savelogfile('dss_upd', 'Update Summary 1 --> ' + E.Message);
      end;
    end;

    if QSummarySource.IsEmpty then
    begin
      isSomething := true;
      break;
    end
    else
    begin
      inc(i);
      FormDssFeed.SetBar0('updating summary loop: ' + IntToStr(i));
      UpdateSummary();
      application.ProcessMessages;
    end;
  end; // while
end;     

function TUpdateDM.UpdateSummary(): string;
var temp: string;
  theCount: integer;
  errorList: TStringList;
begin
  result := '';
  errorList := TStringList.Create;

  try
    if not (connTarget.Connected) or (not QSummarySource.Active) or (not Tbl_Target_Summary.Active) then
    begin
      try
        OpenSourceTarget_Summary();
      except on e: exception do
        begin
          errorList.add('Update Summary 1 --> ' + E.Message);
          FormDssFeed.memo2.Lines.add(e.message);
          savelogfile('dss_upd', 'Update Summary 1 --> ' + E.Message);
          FormDssFeed.dbgrid1.DataSource := dsSUmmarySource;
          FormDssFeed.dbgrid2.DataSource := dsTarget_summary;
          exit;
        end;
      end;
    end;

    QSummarySource.First;
    theCount := 0;
    application.ProcessMessages;

    while not QSummarySource.Eof do
    begin
      temp := '';
      temp := Insert_voucher_summary();

      if temp <> 'OK' then
      begin
        errorList.Add('update summary err --> ' + temp);
        FormDssFeed.memo2.Lines.add('update summary err --> ' + temp);
        savelogfile('dss_upd', 'Update Summary 2--> ' + temp);
      end;

      inc(theCount);
      try FormDssFeed.SetBar1('updating summary: ' + IntToStr(theCOunt)); except; end;

      if (FormDssFeed.bkill) or (theCount > 350000) then break;
      QSummarySource.Next;
      application.ProcessMessages;
    end; // while

    try FormDssFeed.SetBar1('done updating summary: ' + IntToStr(theCOunt)); except; end;
  finally
    result := errorlist.Text;
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

function TUpdateDM.Insert_voucher_summary(): string;
//remember to edit Edit_voucher_summary if editing this - copy and paste action unfortunately
var sbasesys, temp, voucherType, strResult, vendor: string;
  holdRelease, holdVoucher: Integer;
  DaysPlus, grev, duration, daysBetween: Real;
  IsEvenRel, IsNewVoucher, SippExe: Boolean;
  MyProdRec: jProdRec;

  procedure resetrec();
  begin
    MyProdRec.description := '';
    MyProdRec.prod_subtype := '';
    MyProdRec.prod_type := '';
    MyProdRec.source_market := '';
    MyProdRec.system := '';
  end;
begin // assuming on the record needed for update
  result := 'OK';

  if not (Tbl_Target_Summary.Active) then
  begin
    try
      Tbl_Target_Summary.Active := true;
    except on e: exception do
      begin
        result := 'Insert_voucher_summary -> ' + e.Message;
        exit;
      end;
    end;
  end;

  if not dsc.Execute then
  begin
    result := result + ':: dsc exe fail --> ' + dsc.ErrorMessage;
    exit;
  end;

  holdRelease := 0;
  holdVoucher := 0;
  sbasesys := '';

  Tbl_Target_Summary.insert;

  sbasesys := trim(UpperCase(QSummarySource.FieldByName('basesys').AsString));
  Tbl_Target_Summary.FieldByName('vs_basesys').asString := sbasesys;

  holdVoucher := dsc.Voucher;
  Tbl_Target_Summary.FieldByName('vs_voucher').AsInteger := holdVoucher;

  holdRelease := dsc.VouchRel;
  Tbl_Target_Summary.FieldByName('vs_release').AsInteger := holdRelease;

  Tbl_Target_Summary.FieldByName('vs_active_history').AsString := trim(uppercase(dsc.VouchType)); // vcntype

  Tbl_Target_Summary.FieldByName('vs_p_or_q').AsString :=
    trim(uppercase(QSummarySource.FieldByName('vcst1').AsString));

  if QSummarySource.FieldByName('vchpqbath').IsNull then
    Tbl_Target_Summary.FieldByName('vs_paid_dt').AsDateTime := StrToDate('12/31/1899')
  else
    Tbl_Target_Summary.FieldByName('vs_paid_dt').AsDateTime :=
      QSummarySource.FieldByName('vchpqbath').AsDateTime;

  if QSummarySource.FieldByName('vccrcardid').IsNull then
    Tbl_Target_Summary.FieldByName('vs_payment_type').AsInteger := -1
  else
    Tbl_Target_Summary.FieldByName('vs_payment_type').AsInteger :=
      QSummarySource.FieldByName('vccrcardid').AsInteger;

  if dsc.IsFullPay then
    Tbl_Target_Summary.FieldByName('vs_fp_pp').AsString := 'FP'
  else
    Tbl_Target_Summary.FieldByName('vs_fp_pp').AsString := 'PP';

  if dsc.IsPlusRate then
    Tbl_Target_Summary.FieldByName('vs_plus_basic').AsString := 'P'
  else
    Tbl_Target_Summary.FieldByName('vs_plus_basic').AsString := 'B';

  Tbl_Target_Summary.FieldByName('vs_rid').AsInteger := dsc.Rid;

  Tbl_Target_Summary.FieldByName('vs_comments').AsString := trim(dsc.Comments1);

  Tbl_Target_Summary.FieldByName('vs_auth_code').AsString := trim(dsc.AuthCode);

  Tbl_Target_Summary.FieldByName('vs_op_confirmation').AsString :=
    trim(QSummarySource.FieldByName('vccconfirm').AsString);

  resetrec();
  MyProdRec := Get_Product_Code_Rec(sbasesys,
    QSummarySource.FieldByName('vopsubt').AsString,
    QSummarySource.FieldByName('vopstype1').AsString);

  Tbl_Target_Summary.FieldByName('vs_product_type').AsString := trim(uppercase(MyProdRec.prod_type));
  Tbl_Target_Summary.FieldByName('vs_product_type_tag').AsString := trim(uppercase(MyProdRec.prod_subtype));
  Tbl_Target_Summary.FieldByName('vs_business_source').AsString := trim(uppercase(MyProdRec.source_market));

  if QSummarySource.FieldByName('vchdate').IsNull then
    Tbl_Target_Summary.FieldByName('vs_voucher_create_dt').AsDateTime := StrToDate('12/31/1899')
  else
    Tbl_Target_Summary.FieldByName('vs_voucher_create_dt').AsDateTime :=
      trunc(QSummarySource.FieldByName('vchdate').AsDateTime);

  Tbl_Target_Summary.FieldByName('vs_operator_code').AsString := trim(uppercase(dsc.OperatorPU));
  Tbl_Target_Summary.FieldByName('vs_pu_ctry').AsString := trim(uppercase(dsc.CountryPU));
  Tbl_Target_Summary.FieldByName('vs_do_ctry').AsString := trim(uppercase(dsc.CountryDO));
  Tbl_Target_Summary.FieldByName('vs_pu_city').AsString := trim(uppercase(dsc.CityPU));
  Tbl_Target_Summary.FieldByName('vs_do_city').AsString := trim(uppercase(dsc.CityDO));
  Tbl_Target_Summary.FieldByName('vs_pu_loc').AsString := trim(uppercase(dsc.LocPU));
  Tbl_Target_Summary.FieldByName('vs_do_loc').AsString := trim(uppercase(dsc.LocDO));

  try
    Tbl_Target_Summary.FieldByName('vs_pu_dt').AsDateTime := trunc(StrToDate(dsc.PickUpDate));
  except
    Tbl_Target_Summary.FieldByName('vs_pu_dt').AsDateTime := StrToDate('12/31/1899');
  end;

  try
    Tbl_Target_Summary.FieldByName('vs_do_dt').AsDateTime := trunc(StrToDate(dsc.DropOffDate));
  except
    Tbl_Target_Summary.FieldByName('vs_do_dt').AsDateTime := StrToDate('12/31/1899');
  end;

  try
    Tbl_Target_Summary.FieldByName('vs_pu_time').AsDateTime :=
      trunc(StrToDate(dsc.PickUpDate)) + frac(strtodatetime(dsc.PickUpTime));
  except
    Tbl_Target_Summary.FieldByName('vs_pu_time').AsDateTime := 0;
  end;

  try
    Tbl_Target_Summary.FieldByName('vs_do_time').AsDateTime :=
      trunc(StrToDate(dsc.DropOffDate)) + frac(strtodatetime(dsc.DropOffTime));
  except
    Tbl_Target_Summary.FieldByName('vs_do_time').AsDateTime := 0;
  end;

  Tbl_Target_Summary.FieldByName('vs_sipp').AsString := trim(UpperCase(dsc.SIPP));

  Tbl_Target_Summary.FieldByName('vs_op_rate_code').AsString :=
    trim(UpperCase(QSummarySource.FieldByName('voprcode').AsString));

  Tbl_Target_Summary.FieldByName('vs_sipp_type').AsString := trim(UpperCase(dsc.CAR_GROUP));

  // description may need changing later if we update our SIPP Standards
  //sd1.Product := 'car';
  //sd1.SIPP := dsc.SIPP;
  //sd1.Language := 'ENG';

  if sbasesys = 'HOT' then
    SippExe := SippDescExecute('car', dscEdit.Sipp, 'ENG', mtHotelAIR_HC_Old)
    //sd1.MethodType := mtHotelAIR_HC_Old
  else
    SippExe := SippDescExecute('car', dscEdit.Sipp, 'ENG', mtCar_HC_OLD);
    //sd1.MethodType := mtCar_HC_OLD;

  try
    if SippExe then
    begin
      Tbl_Target_Summary.FieldByName('vs_sipp_description').AsString :=sd1.SippDescription_Short
    end
    else
      Tbl_Target_Summary.FieldByName('vs_sipp_description').AsString :=sd1.ErrorMessage;
  except on e: exception do
    begin
      Tbl_Target_Summary.FieldByName('vs_sipp_description').AsString := e.Message;
    end;
  end;

  if QSummarySource.FieldByName('vcstdte1').IsNull then
    Tbl_Target_Summary.FieldByName('vs_updated_dt').AsDateTime := StrToDate('12/31/1899')
  else
    Tbl_Target_Summary.FieldByName('vs_updated_dt').AsDateTime :=
      trunc(QSummarySource.FieldByName('vcstdte1').AsDateTime);

  try
    Tbl_Target_Summary.FieldByName('vs_num_passengers').AsInteger := StrTOInt(dsc.PAXNUM);
  except
    Tbl_Target_Summary.FieldByName('vs_num_passengers').AsInteger := 0;
  end;

  IsEvenRel := false;
  if ((QSummarySource.FieldByName('vchrel').asinteger mod 2) = 0) then IsEvenRel := true;

  duration := 0;
  try
    duration := dsc.NumberDays;
    if IsEvenRel then duration := duration * -1;
    Tbl_Target_Summary.FieldByName('vs_duration').AsFloat := duration;
  except
    Tbl_Target_Summary.FieldByName('vs_duration').AsFloat := 0;
  end;

  try
    daysBetween := 0;
    daysBetween := trunc(QSummarySource.FieldByName('vopdate1').asdatetime) -
      trunc(QSummarySource.FieldByName('vchdate').asdatetime);

    if IsEvenRel then daysBetween := daysBetween * -1;

    Tbl_Target_Summary.FieldByName('vs_days_between_create_pu').AsFloat := daysBetween;
  except
    Tbl_Target_Summary.FieldByName('vs_days_between_create_pu').AsFloat := 0.00;
  end;

  try // add for michelle 3/19/2009
    if IsEvenRel then
      Tbl_Target_Summary.FieldByName('vs_count_logical').AsInteger := -1
    else
      Tbl_Target_Summary.FieldByName('vs_count_logical').AsInteger := 1;
  except
    Tbl_Target_Summary.FieldByName('vs_count_logical').AsInteger := 0;
  end;

  Tbl_Target_Summary.FieldByName('vs_comm_tot').AsFloat := dsc.Comm;
  Tbl_Target_Summary.FieldByName('vs_comm_due').AsFloat := dsc.CommDue;

  if QSummarySource.FieldByName('vccom1').IsNull then
    Tbl_Target_Summary.FieldByName('vs_comm_percent').AsFloat := 0
  else
    Tbl_Target_Summary.FieldByName('vs_comm_percent').AsFloat :=
      QSummarySource.FieldByName('vccom1').asFloat;

  Tbl_Target_Summary.FieldByName('vs_home_curr').AsString :=
    QSummarySource.FieldByName('homecurr').AsString;

  Tbl_Target_Summary.FieldByName('vs_currency_operator').AsString := dsc.Currency;

  Tbl_Target_Summary.FieldByName('vs_exchange_rt').AsFloat := dsc.ExchangeRate;

  if QSummarySource.FieldByName('voptxper1').IsNull then
    Tbl_Target_Summary.FieldByName('vs_vat_tax_rt').AsFloat := 0.0
  else
    Tbl_Target_Summary.FieldByName('vs_vat_tax_rt').AsFloat :=
      QSummarySource.FieldByName('voptxper1').asFloat;

  Tbl_Target_Summary.FieldByName('vs_subtype').AsString :=
    QSummarySource.FieldByName('vopsubt').AsString;

  Tbl_Target_Summary.FieldByName('vs_home_ctry').AsString :=
    QSummarySource.FieldByName('homectry').AsString;

  Tbl_Target_Summary.FieldByName('vs_base_rate_retail').AsFloat := dsc.BaseRet;
  Tbl_Target_Summary.FieldByName('vs_discount_retail').AsFloat := dsc.DiscOldAndNewTotal;

  Tbl_Target_Summary.FieldByName('vs_insurance_tot_retail').AsFloat :=
    dsc.Ins1Ret + dsc.Ins2Ret + dsc.Ins3Ret + dsc.Ins4Ret;

  Tbl_Target_Summary.FieldByName('vs_tax_tot_retail').asfloat := dsc.TaxRetTotal;
  Tbl_Target_Summary.FieldByName('vs_pu_fee_retail').AsFloat := dsc.PURet;
  Tbl_Target_Summary.FieldByName('vs_do_fee_retail').AsFloat := dsc.DORet;
  Tbl_Target_Summary.FieldByName('vs_xtra_fee_retail').AsFloat := dsc.SpecialFeeRet;
  Tbl_Target_Summary.FieldByName('vs_base_rate_wholesale').AsFloat := dsc.BaseWhl;
  Tbl_Target_Summary.FieldByName('vs_insurance_tot_wholesale').AsFloat :=
    dsc.Ins1Whl + dsc.Ins2Whl + dsc.Ins3Whl + dsc.Ins4Whl;

  Tbl_Target_Summary.FieldByName('vs_tax_tot_wholesale').AsFloat := dsc.TaxWhlTotal;
  Tbl_Target_Summary.FieldByName('vs_pu_fee_wholesale').AsFloat := dsc.PUWhl;
  Tbl_Target_Summary.FieldByName('vs_do_fee_wholesale').AsFloat := dsc.DOWhl;
  Tbl_Target_Summary.FieldByName('vs_retail_total').AsFloat := dsc.RetailTot;
  Tbl_Target_Summary.FieldByName('vs_wholesale_total').AsFloat := dsc.WholeSaleTot;
  Tbl_Target_Summary.FieldByName('vs_operator_due').AsFloat := dsc.OperatorDue;
  Tbl_Target_Summary.FieldByName('vs_operator_due_curr').AsFloat := dsc.OperatorDueCURR;
  Tbl_Target_Summary.FieldByName('vs_profit').AsFloat := dsc.ProfitAComm;
  Tbl_Target_Summary.FieldByName('vs_deposit_amt').AsFloat := dsc.DepAmt;
  Tbl_Target_Summary.FieldByName('vs_waived_amt').AsFloat := dsc.WDAmount;
  Tbl_Target_Summary.FieldByName('vs_deposit_after_waive').AsFloat := dsc.DepAmt - dsc.WDAmount;
  Tbl_Target_Summary.FieldByName('vs_due_at_pu').AsFloat := dsc.DueAtPU;
  Tbl_Target_Summary.FieldByName('vs_deferred_amt').AsFloat := dsc.DeferredAmount;

  // gross revenue = everything less Delivery - in this add up all but delivery fee
  grev := 0;
  try
    grev := QSummarySource.FieldByName('vord4').AsFloat + // base rate +
      (dsc.Ins1Ret + dsc.Ins2Ret + dsc.Ins3Ret + dsc.Ins4Ret) + // insuranceTotal +
      dsc.PURet + // pu fee +
      dsc.SpecialFeeRet + // xtra fee +
      dsc.TaxRetTotal; // retail tax total
    Tbl_Target_Summary.FieldByName('vs_gross_revenue').AsFloat := grev;
  except
    Tbl_Target_Summary.FieldByName('vs_gross_revenue').AsFloat := 0.0;
  end;

  try
    Tbl_Target_Summary.FieldByName('vs_operator_cost').AsFloat :=
      grev - dsc.DiscOldAndNewTotal - dsc.Comm - dsc.ProfitAComm;
  except
    Tbl_Target_Summary.FieldByName('vs_operator_cost').AsFloat := 0.0;
  end;

  // add this for IK counting changes 4/13/2009
  IsNewVoucher := false;
                                                // this is actually ' if >= '
  if not (trunc(QSummarySource.fieldbyname('vchpqbath').asfloat) < 39918) then // 39918 = Wednesday, April 15, 2009 00:00:00
  begin
    if (uppercase(QSummarySource.fieldbyname('vchpqtype').asstring) = 'N') and
      (QSummarySource.fieldbyname('vchpqvchln').asinteger = 1) then isnewvoucher := true
  end
  else if (uppercase(QSummarySource.fieldbyname('vchpqtype').asstring) = 'N') then
  begin
    isnewvoucher := true;
  end;
  // end add this for IK counting changes 4/13/2009

  if (isNewVoucher) then
  begin
    Tbl_Target_Summary.FieldByName('vs_count_new').AsInteger := 1;
    Tbl_Target_Summary.FieldByName('vs_count_cxl').AsInteger := 0;
    Tbl_Target_Summary.FieldByName('vs_count_change').AsInteger := 0;
    Tbl_Target_Summary.FieldByName('vs_count_voucher').AsInteger := 1;
  end
  else // not new "N"
  begin
    Tbl_Target_Summary.FieldByName('vs_count_new').AsInteger := 0;
    Tbl_Target_Summary.FieldByName('vs_count_cxl').AsInteger := 0;
    Tbl_Target_Summary.FieldByName('vs_count_change').AsInteger := 0;
    Tbl_Target_Summary.FieldByName('vs_count_voucher').AsInteger := 0;

    if (uppercase(trim(QSummarySource.FieldByName('vchpqtype').asstring)) = 'D') then
    begin
      Tbl_Target_Summary.FieldByName('vs_count_cxl').AsInteger := 1;
      Tbl_Target_Summary.FieldByName('vs_count_voucher').AsInteger := -1;
    end
    else
    begin // must be a change
      Tbl_Target_Summary.FieldByName('vs_count_change').AsInteger := 1;
    end;
  end;

  Tbl_Target_Summary.FieldByName('vs_client_age').AsInteger :=
    QSummarySource.FieldByName('voiamt4').AsInteger;

  Tbl_Target_Summary.FieldByName('vs_iata').AsString := trim(UpperCase(dsc.Iata));

  temp := '';
  temp := copy(trim(QSummarySource.FieldByName('vciata').AsString), 1, 2);

 // depricated 10/11/2010
 // if (uppercase(temp) = '99') then
 //   Tbl_Target_Summary.FieldByName('vs_direct_or_ta').AsString := 'DC' // "DC"
 // else if isInList(IataWhiteList, trim(UpperCase(dsc.Iata))) then
 //   Tbl_Target_Summary.FieldByName('vs_direct_or_ta').AsString := 'DC' // "DC" by being in the white list table
 // else
 //   Tbl_Target_Summary.FieldByName('vs_direct_or_ta').AsString := 'TA';

  // new method 10/12/2010
  if FormDSSFeed.IsADC(trim(Uppercase(sbasesys)), trim(UpperCase(dsc.Iata))) then
    Tbl_Target_Summary.FieldByName('vs_direct_or_ta').AsString := 'DC'
  else
    Tbl_Target_Summary.FieldByName('vs_direct_or_ta').AsString := 'TA';


  Tbl_Target_Summary.FieldByName('vs_consortium').AsString :=
    trim(UpperCase(QSummarySource.FieldByName('voconsort').AsString));

  // these have defaults
  // vs_insert_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,
  // vs_update_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,

  // plug date, assuming no update has been done yet
  // when updating lyris, use this date, then set it after done.
  Tbl_Target_Summary.FieldByName('vs_upl_to_listmgr_dt').AsDateTime := strToDate('12/31/1899');

  // add for DonH 8/27/2008 ..
  Tbl_Target_Summary.FieldByName('fraud_status').AsString := QSummarySource.FieldByName('voxtp3t').AsString;

  // add for aus 06/23/2010
  Tbl_Target_Summary.FieldByName('vs_promo1').AsString := QSummarySource.FieldByName('vcvpromo1').AsString;
  Tbl_Target_Summary.FieldByName('vs_promo2').AsString := QSummarySource.FieldByName('vcvpromo2').AsString;
  Tbl_Target_Summary.FieldByName('vs_promo3').AsString := QSummarySource.FieldByName('vcvpromo3').AsString;

  // add the Vendor plug here
  // function GetOperatorVendor(VouchVOXTP1: Integer; InSys: string): string;
  vendor := '';
  if not (QSummarySource.fieldbyname('VOXTP1').IsNull) then
  begin
    vendor :=GetOperatorVendor(QSummarySource.fieldbyname('VOXTP1').asInteger, Trim(Uppercase(sbasesys)));

    if vendor <> '' then
      Tbl_Target_Summary.FieldByName('vs_opvend').AsString := vendor;
  end;

  try
    Tbl_Target_Summary.post;
  except on e: exception do
    begin
      Tbl_Target_Summary.cancel;
      result := result + ':: post err --> ' + e.message;
      exit;
    end;
  end;

  // add back in when doing regular updating
  // not now for full load

  if (holdRelease > 1) then
  begin // set all lower releases to "H" for this voucher
    strResult := Update_VCNTYPE_Summary(sbasesys, holdVoucher, holdRelease);
    try
      if strResult <> 'OK' then
        savelogfile('dss_upd', sbasesys +
          ' voucher: ' + IntToStr(holdVoucher) + '-' + IntToStr(holdRelease) + ' Update_VCNTYPE() --> ' + strResult);
    except;
    end;
  end;

  // update the system operator list here. This list is used to populate Operator ComboBox's and the like
  // instead of using "DISTINCT" on millions of rows which is super slow
  try
    UpdateOperatorList(sbasesys, trim(uppercase(dsc.OperatorPU)));
  except;
  end;
end;    

function TUpdateDM.OpenSourceTarget_Summary(): boolean;
begin // this needs to be updated to summary not Q stuff !!
  result := false;
  FormDSSFeed.dbgrid1.DataSource := dsSUmmarySOurce;
  FormDSSFeed.dbgrid2.DataSource := dsTarget_summary;

  try
    SetConn(connTarget);
    FormDSSFeed.caption := 'connected to: ' + GetCurrentDB(connTarget);
    FormDSSFeed.SetBar0('connected to: ' + GetCurrentDB(connTarget));
  except on e: exception do
    begin
      FormDSSFeed.memo1.Lines.add('OpenSource_Summary connect fail --> ' + E.Message);
      raise(exception.Create('OpenSource Summary connect fail --> ' + e.Message));
      savelogfile('dss_upd', 'OpenSource_Summary connect fail --> ' + E.Message);
      exit;
    end;
  end;

  if not (Tbl_Target_Summary.Active) then
  begin
    try
      Tbl_Target_Summary.Active := true;
    except on e: exception do
      begin
        raise(exception.Create('failed to open summary target table. -->' + e.Message));
        savelogfile('dss_upd', 'failed to open summary target table. --> ' + E.Message);
        exit;
      end;
    end;
  end;

  try
{

select  * from vouchandqueue  a
where
a.vchpqbath >= :InDate and
not exists(
Select b.vs_basesys,b.vs_voucher,b.vs_release From
g_voucher_summary b
where
b.vs_basesys = a.basesys and
b.vs_voucher = a.vcnumb and
b.vs_release = a.vchrel
)
}
    try
      if QSummarySource.Active then QSummarySource.Close;
    except;
    end;

    QSummarySource.ParamByName('InDate').AsDate := now() - 30;
    FormDSSFeed.memo2.Lines := QSummarySource.SQL;
    FormDSSFeed.memo2.Lines.Add('date: ' + QSummarySource.ParamByName('InDate').AsString);
    FormDSSFeed.caption := 'connecting to ' + GetCurrentDB(connTarget);
    QSummarySource.Open;
    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connTarget);
    FormDSSFeed.memo2.Lines.add('connected to ' + GetCurrentDB(connTarget));
  except on e: exception do
    begin
      raise(exception.Create('failed to open summary source query. --> ' + e.Message));
      savelogfile('dss_upd', 'failed to open summary source query. --> ' + E.Message);
      FormDSSFeed.caption := '';
      exit;
    end;
  end;
  result := true;
end;   

function TUpdateDM.Get_Product_Code_Rec(basesys, vopsubt, vopstype1: string): jProdRec;
var // assuming connGLobal is connected
  sys, subtype, subtype1, theStr: string;

  procedure unset();
  begin
    result.system := '';
    result.source_market := '';
    result.prod_type := '';
    result.prod_subtype := '';
    result.description := '';
  end;

begin
  sys := '';
  theStr := '';
  subtype := '';
  subtype1 := '';
  unset();
  sys := trim(Uppercase(basesys));
  subtype := vopsubt;
  subtype1 := vopstype1;

  try
    get_prod_desc_type_subtype_source(
      sys,
      subtype,
      subtype1,
      connTarget, // points to global normally, whse in this case
      result);
  except on e: exception do
    begin
      theStr := 'sys: [' + sys + '] vopsubt [' + subtype + '] vopstype1: [' + subtype1 + ']';
      stradd(theStr, 'err: ' + e.Message);
      result.description := theStr;
      result.prod_subtype := '?';
      result.prod_type := '?';
      result.source_market := '?';
      savelogfile('dss_upd', theStr);
    end;
  end;
end;

function TUpdateDM.Edit_voucher_summary(): string;
// remember to edit Insert_voucher_summary if you edit this - cut n paste / duplicate code
var sbasesys, temp, voucherType, vendor: string;
  holdRelease, holdVoucher: Integer;
  DaysPlus, grev, duration, daysBetween: Real;
  IsEvenRel, IsNewVoucher, SippExe: Boolean;
  MyProdRec: jProdRec;

  procedure resetrec();
  begin
    MyProdRec.description := '';
    MyProdRec.prod_subtype := '';
    MyProdRec.prod_type := '';
    MyProdRec.source_market := '';
    MyProdRec.system := '';
  end;

begin
  result := 'OK';
  // assuming on the record needed for update
  // assuming QSummaryFIXREC is open already

  if not dscEdit.Execute then
  begin
    result := result + ':: dscedit exe fail --> ' + dscEdit.ErrorMessage;
    exit;
  end;

  holdRelease := 0;
  holdVoucher := 0;
  sbasesys := '';

  QSummaryFIXREC.Edit;

  sbasesys := trim(UpperCase(QVandQ_FIXREC.FieldByName('basesys').AsString));

  QSummaryFIXREC.FieldByName('vs_basesys').asString := sbasesys;

  holdVoucher := dscEdit.Voucher;
  QSummaryFIXREC.FieldByName('vs_voucher').AsInteger := holdVoucher;

  holdRelease := dscEdit.VouchRel;
  QSummaryFIXREC.FieldByName('vs_release').AsInteger := holdRelease;

  QSummaryFIXREC.FieldByName('vs_active_history').AsString := trim(uppercase(dscEdit.VouchType)); // vcntype

  QSummaryFIXREC.FieldByName('vs_p_or_q').AsString :=
    trim(uppercase(QVandQ_FIXREC.FieldByName('vcst1').AsString));

  if QVandQ_FIXREC.FieldByName('vchpqbath').IsNull then
    QSummaryFIXREC.FieldByName('vs_paid_dt').AsDateTime := StrToDate('12/31/1899')
  else
    QSummaryFIXREC.FieldByName('vs_paid_dt').AsDateTime :=
      QVandQ_FIXREC.FieldByName('vchpqbath').AsDateTime;

  if QVandQ_FIXREC.FieldByName('vccrcardid').IsNull then
    QSummaryFIXREC.FieldByName('vs_payment_type').AsInteger := -1
  else
    QSummaryFIXREC.FieldByName('vs_payment_type').AsInteger :=
      QVandQ_FIXREC.FieldByName('vccrcardid').AsInteger;

  if dscEdit.IsFullPay then
    QSummaryFIXREC.FieldByName('vs_fp_pp').AsString := 'FP'
  else
    QSummaryFIXREC.FieldByName('vs_fp_pp').AsString := 'PP';

  if dscEdit.IsPlusRate then
    QSummaryFIXREC.FieldByName('vs_plus_basic').AsString := 'P'
  else
    QSummaryFIXREC.FieldByName('vs_plus_basic').AsString := 'B';

  QSummaryFIXREC.FieldByName('vs_rid').AsInteger := dscEdit.Rid;

  QSummaryFIXREC.FieldByName('vs_comments').AsString := trim(dscEdit.Comments1);

  QSummaryFIXREC.FieldByName('vs_auth_code').AsString := trim(dscEdit.AuthCode);

  QSummaryFIXREC.FieldByName('vs_op_confirmation').AsString :=
    trim(QVandQ_FIXREC.FieldByName('vccconfirm').AsString);

  resetrec();
  MyProdRec := Get_Product_Code_Rec(sbasesys,
    QVandQ_FIXREC.FieldByName('vopsubt').AsString,
    QVandQ_FIXREC.FieldByName('vopstype1').AsString);

  QSummaryFIXREC.FieldByName('vs_product_type').AsString := trim(uppercase(MyProdRec.prod_type));
  QSummaryFIXREC.FieldByName('vs_product_type_tag').AsString := trim(uppercase(MyProdRec.prod_subtype));
  QSummaryFIXREC.FieldByName('vs_business_source').AsString := trim(uppercase(MyProdRec.source_market));

  if QVandQ_FIXREC.FieldByName('vchdate').IsNull then
    QSummaryFIXREC.FieldByName('vs_voucher_create_dt').AsDateTime := StrToDate('12/31/1899')
  else
    QSummaryFIXREC.FieldByName('vs_voucher_create_dt').AsDateTime :=
      trunc(QVandQ_FIXREC.FieldByName('vchdate').AsDateTime);

  QSummaryFIXREC.FieldByName('vs_operator_code').AsString := trim(uppercase(dscEdit.OperatorPU));
  QSummaryFIXREC.FieldByName('vs_pu_ctry').AsString := trim(uppercase(dscEdit.CountryPU));
  QSummaryFIXREC.FieldByName('vs_do_ctry').AsString := trim(uppercase(dscEdit.CountryDO));
  QSummaryFIXREC.FieldByName('vs_pu_city').AsString := trim(uppercase(dscEdit.CityPU));
  QSummaryFIXREC.FieldByName('vs_do_city').AsString := trim(uppercase(dscEdit.CityDO));
  QSummaryFIXREC.FieldByName('vs_pu_loc').AsString := trim(uppercase(dscEdit.LocPU));
  QSummaryFIXREC.FieldByName('vs_do_loc').AsString := trim(uppercase(dscEdit.LocDO));

  try
    QSummaryFIXREC.FieldByName('vs_pu_dt').AsDateTime := trunc(StrToDate(dscEdit.PickUpDate));
  except
    QSummaryFIXREC.FieldByName('vs_pu_dt').AsDateTime := StrToDate('12/31/1899');
  end;

  try
    QSummaryFIXREC.FieldByName('vs_do_dt').AsDateTime := trunc(StrToDate(dscEdit.DropOffDate));
  except
    QSummaryFIXREC.FieldByName('vs_do_dt').AsDateTime := StrToDate('12/31/1899');
  end;

  try
    QSummaryFIXREC.FieldByName('vs_pu_time').AsDateTime :=
      trunc(StrToDate(dscEdit.PickUpDate)) + frac(strtodatetime(dscEdit.PickUpTime));
  except
    QSummaryFIXREC.FieldByName('vs_pu_time').AsDateTime := 0;
  end;

  try
    QSummaryFIXREC.FieldByName('vs_do_time').AsDateTime :=
      trunc(StrToDate(dscEdit.DropOffDate)) + frac(strtodatetime(dscEdit.DropOffTime));
  except
    QSummaryFIXREC.FieldByName('vs_do_time').AsDateTime := 0;
  end;

  QSummaryFIXREC.FieldByName('vs_sipp').AsString := trim(UpperCase(dscEdit.SIPP));

  QSummaryFIXREC.FieldByName('vs_op_rate_code').AsString :=
    trim(UpperCase(QVandQ_FIXREC.FieldByName('voprcode').AsString));

  QSummaryFIXREC.FieldByName('vs_sipp_type').AsString := trim(UpperCase(dscEdit.CAR_GROUP));

  // description may need changing later if we update our SIPP Standards

  //sd1.Product := 'car';
  //sd1.SIPP := dscEdit.SIPP;
  //sd1.Language := 'ENG';

  if sbasesys = 'HOT' then
    SippExe := SippDescExecute('car', dscEdit.Sipp, 'ENG', mtHotelAIR_HC_Old)
    //sd1.MethodType := mtHotelAIR_HC_Old
  else
    SippExe := SippDescExecute('car', dscEdit.Sipp, 'ENG', mtCar_HC_OLD);
    //sd1.MethodType := mtCar_HC_OLD;

  try
    if SippExe then
    begin
      QSummaryFIXREC.FieldByName('vs_sipp_description').AsString := sd1.SippDescription_Short
    end
    else
      QSummaryFIXREC.FieldByName('vs_sipp_description').AsString := sd1.ErrorMessage;
  except on e: exception do
    begin
      QSummaryFIXREC.FieldByName('vs_sipp_description').AsString := e.Message;
    end;
  end;

  if QVandQ_FIXREC.FieldByName('vcstdte1').IsNull then
    QSummaryFIXREC.FieldByName('vs_updated_dt').AsDateTime := StrToDate('12/31/1899')
  else
    QSummaryFIXREC.FieldByName('vs_updated_dt').AsDateTime :=
      trunc(QVandQ_FIXREC.FieldByName('vcstdte1').AsDateTime);

  try
    QSummaryFIXREC.FieldByName('vs_num_passengers').AsInteger := StrTOInt(dscEdit.PAXNUM);
  except
    QSummaryFIXREC.FieldByName('vs_num_passengers').AsInteger := 0;
  end;

  IsEvenRel := false;
  if ((QVandQ_FIXREC.FieldByName('vchrel').asinteger mod 2) = 0) then IsEvenRel := true;

  duration := 0;
  try
    duration := dscEdit.NumberDays;
    if IsEvenRel then duration := duration * -1;
    QSummaryFIXREC.FieldByName('vs_duration').AsFloat := duration;
  except
    QSummaryFIXREC.FieldByName('vs_duration').AsFloat := 0;
  end;

  try
    daysBetween := 0;
    daysBetween := trunc(QVandQ_FIXREC.FieldByName('vopdate1').asdatetime) -
      trunc(QVandQ_FIXREC.FieldByName('vchdate').asdatetime);

    if IsEvenRel then daysBetween := daysBetween * -1;

    QSummaryFIXREC.FieldByName('vs_days_between_create_pu').AsFloat := daysBetween;
  except
    QSummaryFIXREC.FieldByName('vs_days_between_create_pu').AsFloat := 0.00;
  end;

  try // add for michelle 3/19/2009
    if IsEvenRel then
      QSummaryFIXREC.FieldByName('vs_count_logical').AsInteger := -1
    else
      QSummaryFIXREC.FieldByName('vs_count_logical').AsInteger := 1;
  except
    QSummaryFIXREC.FieldByName('vs_count_logical').AsInteger := 0;
  end;

  QSummaryFIXREC.FieldByName('vs_comm_tot').AsFloat := dscEdit.Comm;
  QSummaryFIXREC.FieldByName('vs_comm_due').AsFloat := dscEdit.CommDue;

  if QVandQ_FIXREC.FieldByName('vccom1').IsNull then
    QSummaryFIXREC.FieldByName('vs_comm_percent').AsFloat := 0
  else
    QSummaryFIXREC.FieldByName('vs_comm_percent').AsFloat :=
      QVandQ_FIXREC.FieldByName('vccom1').asFloat;

  QSummaryFIXREC.FieldByName('vs_home_curr').AsString :=
    QVandQ_FIXREC.FieldByName('homecurr').AsString;

  QSummaryFIXREC.FieldByName('vs_currency_operator').AsString := dscEdit.Currency;

  QSummaryFIXREC.FieldByName('vs_exchange_rt').AsFloat := dscEdit.ExchangeRate;

  if QVandQ_FIXREC.FieldByName('voptxper1').IsNull then
    QSummaryFIXREC.FieldByName('vs_vat_tax_rt').AsFloat := 0.0
  else
    QSummaryFIXREC.FieldByName('vs_vat_tax_rt').AsFloat :=
      QVandQ_FIXREC.FieldByName('voptxper1').asFloat;

  QSummaryFIXREC.FieldByName('vs_subtype').AsString :=
    QVandQ_FIXREC.FieldByName('vopsubt').AsString;

  QSummaryFIXREC.FieldByName('vs_home_ctry').AsString :=
    QVandQ_FIXREC.FieldByName('homectry').AsString;

  QSummaryFIXREC.FieldByName('vs_base_rate_retail').AsFloat := dscEdit.BaseRet;
  QSummaryFIXREC.FieldByName('vs_discount_retail').AsFloat := dscEdit.DiscOldAndNewTotal;

  QSummaryFIXREC.FieldByName('vs_insurance_tot_retail').AsFloat :=
    dscEdit.Ins1Ret + dscEdit.Ins2Ret + dscEdit.Ins3Ret + dscEdit.Ins4Ret;

  QSummaryFIXREC.FieldByName('vs_tax_tot_retail').asfloat := dscEdit.TaxRetTotal;
  QSummaryFIXREC.FieldByName('vs_pu_fee_retail').AsFloat := dscEdit.PURet;
  QSummaryFIXREC.FieldByName('vs_do_fee_retail').AsFloat := dscEdit.DORet;
  QSummaryFIXREC.FieldByName('vs_xtra_fee_retail').AsFloat := dscEdit.SpecialFeeRet;
  QSummaryFIXREC.FieldByName('vs_base_rate_wholesale').AsFloat := dscEdit.BaseWhl;
  QSummaryFIXREC.FieldByName('vs_insurance_tot_wholesale').AsFloat :=
    dscEdit.Ins1Whl + dscEdit.Ins2Whl + dscEdit.Ins3Whl + dscEdit.Ins4Whl;

  QSummaryFIXREC.FieldByName('vs_tax_tot_wholesale').AsFloat := dscEdit.TaxWhlTotal;
  QSummaryFIXREC.FieldByName('vs_pu_fee_wholesale').AsFloat := dscEdit.PUWhl;
  QSummaryFIXREC.FieldByName('vs_do_fee_wholesale').AsFloat := dscEdit.DOWhl;
  QSummaryFIXREC.FieldByName('vs_retail_total').AsFloat := dscEdit.RetailTot;
  QSummaryFIXREC.FieldByName('vs_wholesale_total').AsFloat := dscEdit.WholeSaleTot;
  QSummaryFIXREC.FieldByName('vs_operator_due').AsFloat := dscEdit.OperatorDue;
  QSummaryFIXREC.FieldByName('vs_operator_due_curr').AsFloat := dscEdit.OperatorDueCURR;
  QSummaryFIXREC.FieldByName('vs_profit').AsFloat := dscEdit.ProfitAComm;
  QSummaryFIXREC.FieldByName('vs_deposit_amt').AsFloat := dscEdit.DepAmt;
  QSummaryFIXREC.FieldByName('vs_waived_amt').AsFloat := dscEdit.WDAmount;
  QSummaryFIXREC.FieldByName('vs_deposit_after_waive').AsFloat := dscEdit.DepAmt - dscEdit.WDAmount;
  QSummaryFIXREC.FieldByName('vs_due_at_pu').AsFloat := dscEdit.DueAtPU;
  QSummaryFIXREC.FieldByName('vs_deferred_amt').AsFloat := dscEdit.DeferredAmount;

  // gross revenue = everything less Delivery - in this add up all but delivery fee
  grev := 0;
  try
    grev := QVandQ_FIXREC.FieldByName('vord4').AsFloat + // base rate +
      (dscEdit.Ins1Ret + dscEdit.Ins2Ret + dscEdit.Ins3Ret + dscEdit.Ins4Ret) + // insuranceTotal +
      dscEdit.PURet + // pu fee +
      dscEdit.SpecialFeeRet + // xtra fee +
      dscEdit.TaxRetTotal; // retail tax total
    QSummaryFIXREC.FieldByName('vs_gross_revenue').AsFloat := grev;
  except
    QSummaryFIXREC.FieldByName('vs_gross_revenue').AsFloat := 0.0;
  end;

  try
    QSummaryFIXREC.FieldByName('vs_operator_cost').AsFloat :=
      grev - dscEdit.DiscOldAndNewTotal - dscEdit.Comm - dscEdit.ProfitAComm;
  except
    QSummaryFIXREC.FieldByName('vs_operator_cost').AsFloat := 0.0;
  end;

  // add this for IK counting changes 4/13/2009
  IsNewVoucher := false;

  if not (trunc(QVandQ_FIXREC.fieldbyname('vchpqbath').asfloat) < 39918) then // 39918 = Wednesday, April 15, 2009 00:00:00
  begin
    if (uppercase(QVandQ_FIXREC.fieldbyname('vchpqtype').asstring) = 'N') and
      (QVandQ_FIXREC.fieldbyname('vchpqvchln').asinteger = 1) then isnewvoucher := true
  end
  else if (uppercase(QVandQ_FIXREC.fieldbyname('vchpqtype').asstring) = 'N') then
  begin
    isnewvoucher := true;
  end;
  // end add this for IK counting changes 4/13/2009

  if (isNewVoucher) then
  begin
    QSummaryFIXREC.FieldByName('vs_count_new').AsInteger := 1;
    QSummaryFIXREC.FieldByName('vs_count_cxl').AsInteger := 0;
    QSummaryFIXREC.FieldByName('vs_count_change').AsInteger := 0;
    QSummaryFIXREC.FieldByName('vs_count_voucher').AsInteger := 1;
  end
  else // not new "N"
  begin
    QSummaryFIXREC.FieldByName('vs_count_new').AsInteger := 0;
    QSummaryFIXREC.FieldByName('vs_count_cxl').AsInteger := 0;
    QSummaryFIXREC.FieldByName('vs_count_change').AsInteger := 0;
    QSummaryFIXREC.FieldByName('vs_count_voucher').AsInteger := 0;

    if (uppercase(trim(QVandQ_FIXREC.FieldByName('vchpqtype').asstring)) = 'D') then
    begin
      QSummaryFIXREC.FieldByName('vs_count_cxl').AsInteger := 1;
      QSummaryFIXREC.FieldByName('vs_count_voucher').AsInteger := -1;
    end
    else
    begin // must be a change
      QSummaryFIXREC.FieldByName('vs_count_change').AsInteger := 1;
    end;
  end;

  QSummaryFIXREC.FieldByName('vs_client_age').AsInteger :=
    QVandQ_FIXREC.FieldByName('voiamt4').AsInteger;

  QSummaryFIXREC.FieldByName('vs_iata').AsString := trim(UpperCase(dscEdit.Iata));

  temp := '';
  temp := copy(trim(QVandQ_FIXREC.FieldByName('vciata').AsString), 1, 2);

  // new method 10/12/2010
  if FormDSSFeed.IsADC(trim(Uppercase(sbasesys)), trim(UpperCase(dscEdit.Iata))) then
    QSummaryFIXREC.FieldByName('vs_direct_or_ta').AsString := 'DC'
  else
    QSummaryFIXREC.FieldByName('vs_direct_or_ta').AsString := 'TA';

  QSummaryFIXREC.FieldByName('vs_consortium').AsString :=
    trim(UpperCase(QVandQ_FIXREC.FieldByName('voconsort').AsString));

  // these have defaults
  // vs_insert_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,
  // vs_update_dt DATETIME YEAR TO DAY DEFAULT CURRENT YEAR TO DAY,

  // plug date, assuming no update has been done yet
  // when updating lyris, use this date, then set it after done.
  QSummaryFIXREC.FieldByName('vs_upl_to_listmgr_dt').AsDateTime := strToDate('12/31/1899');

  // add for DonH 8/27/2008 ..
  QSummaryFIXREC.FieldByName('fraud_status').AsString := QVandQ_FIXREC.FieldByName('voxtp3t').AsString;

  // add for aus 06/23/2010
  QSummaryFIXREC.FieldByName('vs_promo1').AsString := QVandQ_FIXREC.FieldByName('vcvpromo1').AsString;
  QSummaryFIXREC.FieldByName('vs_promo2').AsString := QVandQ_FIXREC.FieldByName('vcvpromo2').AsString;
  QSummaryFIXREC.FieldByName('vs_promo3').AsString := QVandQ_FIXREC.FieldByName('vcvpromo3').AsString;

  // Vendor plug for acct i.e. operator 'vendor' number, not IATA
  vendor := '';
  if not (QVandQ_FIXREC.fieldbyname('VOXTP1').IsNull) then
  begin
    vendor := GetOperatorVendor(QVandQ_FIXREC.fieldbyname('VOXTP1').asInteger, Trim(Uppercase(sbasesys)));

    if vendor <> '' then
      QSummaryFIXREC.FieldByName('vs_opvend').AsString := vendor;
  end;

  try
    QSummaryFIXREC.post;
  except on e: exception do
    begin
      QSummaryFIXREC.cancel;
      result := result + ':: post err --> ' + e.message;
      exit;
    end;
  end;

  // add back in when doing regular updating
  // not now for full load
  {
  if (holdRelease > 1) then
  begin // set all lower releases to "H" for this voucher
    ass := '';
    ass := Update_VCNTYPE_Summary(sbasesys, holdVoucher, holdRelease);
    try
      if ass <> 'OK' then
        savelogfile('dss_upd', sbasesys +
          ' voucher: ' + IntToStr(holdVoucher) + '-' + IntToStr(holdRelease) + ' Update_VCNTYPE() --> ' + ass);
    except;
    end;
  end;
   }
end; ////// edit summary

function TUpdateDm.Update_VCNTYPE_Summary(bsys: string; vch, rel: Integer): string;
var MyQuery: TIfxQuery; // update vcntype to History when new vouchers come in
  theSQL, v, r: string;
  i: integer;
begin
  result := 'OK';

  theSQL := 'update g_voucher_summary set vs_active_history = "H" where vs_basesys = :sbasesys and ' +
    'vs_voucher = :InVCNUMB and vs_release < :Invchrel';

  bsys := trim(Uppercase(bsys));
  if bsys = '' then
  begin
    result := 'Update_VCNTYPE_Summary --> no basesys';
    exit;
  end;

  v := '';
  r := '';
  i := 0;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      r := IntToStr(rel);
      v := IntTOStr(vch);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('sbasesys').AsString := bsys;
      MyQuery.ParamByName('InVCNUMB').AsInteger := vch;
      MyQuery.ParamByName('Invchrel').AsInteger := rel;
      i := MyQuery.ExecSQL;
    except on e: exception do
      begin
        result := 'Update_VCNTYPE_Summary ' + bsys + ' voucher: ' + IntTostr(vch) + '-' + IntToStr(rel) + ' --> ' + E.Message;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;

procedure TUpdateDM.UpdateOperatorList(sys, Oper: string);
var MYQuery: TIfxQuery;
  DoesExist: Boolean;
  theSQL: string;
  i: Integer;
begin
  sys := trim(Uppercase(sys));
  Oper := trim(Uppercase(Oper));

  if (Oper = '') or (sys = '') then
  begin
    savelogfile('dss_upd', 'UpdateOperatorList sys or oper blank --> [' + sys + '] oper [' + oper + ']');
    exit;
  end;

  try
    try
      MyQuery := GetIDACQuery(connTarget);

      theSQL := 'select gob_basesys from g_operators_booked where gob_basesys = "'
        + sys + '" and gob_operator_code = "' + Oper + '"';

      MyQuery.sql.Add(theSQL);
      FormDSSFeed.memo2.Lines.add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then
      begin
        MyQuery.active := false;
        MyQuery.SQL.Clear;
        theSQL := '';

        theSQL := 'INSERT into g_operators_booked ' +
          '(gob_basesys, gob_operator_code) ' +
          ' VALUES ' +
          '("' + sys + '", "' + Oper + '" )';

        MyQuery.SQL.Add(theSQL);
        i := 0;
        i := MyQuery.ExecSQL;
        FormDSSFeed.memo2.Lines.add(IntToStr(i) + ' insert to g_operators_booked for [' + sys + '] and [' + oper + ']');
      end
      else
      begin
        FormDSSFeed.memo2.Lines.add('record already exists in g_opers_b for [' + sys + '] and [' + oper + ']');
      end;
    except on e: exception do
      begin
        savelogfile('dss_upd', 'UpdateOperatorList --> sys [' + sys + '] oper [' + oper + '] --> ' + E.Message);
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end; 

procedure TUpdateDM.FixOnePQRecord;
var errstr, sbasesys, svcnumb, svchrel, tmp: string;
  ivcnumb, ivchrel: INteger;
begin
  tmp := 'Update vouchAndQueue / g_voucher_summary for selected record?';
  if MessageDlg(tmp, mtWarning, mbOKCancel, 0) = mrCancel then exit;

  if (not Qsource.Active) or (Qsource.IsEmpty) or
    (not QVandQ_FIXREC.Active) or (not QSummaryFIXREC.active) then
  begin
    showmessage('you must activate qsource, QVandQ_FIXREC and QSummaryFIXREC before editing.');
    exit;
  end;

  // first update vouchandqueue
  sbasesys := '';
  svcnumb := '';
  svchrel := '';
  ivchrel := 0;
  errstr := '';
  sbasesys := QSource.FieldByName('basesys').AsString;
  svcnumb := QSource.FieldByName('vcnumb').AsString;
  svchrel := QSource.FieldByName('vchrel').AsString;
  ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
  ivchrel := QSource.FieldByName('vchrel').AsInteger;

  errstr := '';
  errstr := EditVQ();

  if errstr > '' then FormDSSfeed.memo1.Lines.add(sbasesys +
      ' voucher: ' + svcnumb + '-' + svchrel + ' EditVQ() --> ' + errstr);

  errstr := '';

  //not completed:
  errstr := Edit_voucher_summary();
  if errstr > '' then FormDSSfeed.memo1.Lines.add(sbasesys +
      ' voucher: ' + svcnumb + '-' + svchrel + ' EditVQ() --> ' + errstr);
end; 

procedure TUpdateDm.FIXROPENTARGET;
begin
  QSummaryFIXREC.Close; // datasource linked to dsQVandC_FIXREC
  QVandQ_FIXREC.Close; // datasource linked to dsSource
 connTarget.Close;

  if not QSource.Active then
  begin
    showmessage('Target is dependent on source.' + #10#13 +
      'Select a system and date range, then click "fix rec open source" first.'
      );
    exit;
  end;

  try
   SetConn(connTarget);
  except on e: exception do
    begin
      showmessage('FIX rec Open Target - connect failure --> ' + E.Message);
      exit;
    end;
  end;

  try
    QVandQ_FIXREC.Open; // datasource linked to dsSource
    QSummaryFIXREC.Open; // datasource linked to dsQVandC_FIXREC
  except on e: exception do
    begin
      showmessage(e.Message);
      exit;
    end;
  end;
  FormDSSfeed.dbgrid3.DataSource :=dsQVandC_FIXREC; // top right grid
  FormDSSfeed.dbgrid2.DataSource := dsSummaryFIXREC; // bottom right grid
end; 

function tuPDATEdm.EditVQ(): string;
// if you edit this, remember to edit insertVQ() - this was a cut and paste
var temp: string;
  i: Integer;
begin
  result := '';
  // at this point, the user should have selected a source DB, batch start and end dates,
  // opened source dataset, and has the pointer on the record to be updated.
  // QVandQ_FIXREC is slave to Qsource.
  // QSummaryFIXREC is slave to QVandQ_FIXREC

  try
    QVandQ_FIXREC.Edit;
  except;
  end;

  QVandQ_FIXREC.FieldByName('basesys').AsString :=
    trim(Uppercase(QSource.FieldByName('basesys').AsString));

  QVandQ_FIXREC.FieldByName('clientid').AsString := '';
  QVandQ_FIXREC.FieldByName('cryptcc').AsString := '';

  temp := '';
  with QSource do
  begin
    for i := 0 to fieldcount - 1 do
    begin
      temp := '';
      temp := trim(LowerCase(fields[i].fieldname));

      if (temp = 'basesys') or
        (temp = 'clientid') or
        (temp = 'vccrcard') or
        (temp = 'cryptcc') or
        (temp = 'lastname') or // continue due to these new fields not existing in Vouchandqueue
        (temp = 'firstname') or // these are brought in for the summary table.
        (temp = 'initial') or
        (temp = 'client_addr1') or
        (temp = 'client_addr2') or
        (temp = 'client_city') or
        (temp = 'client_prov') or
        (temp = 'client_postal') or
        (temp = 'client_phone') or
        (temp = 'crap') or
        (temp = 'client_ctry') or
        (temp = 'clemail') or
        (temp = 'agemail2') or
        (temp = 'vchpqseq') // add to handle err --> EditVQ() post error --> -232 SQL error: A SERIAL column (vchpqseq) may not be updated.
         then continue
      else
      begin
        try
          case fields[i].DataType of
            ftString:
              begin // format all strings:
                QVandQ_FIXREC.fieldbyname(fields[i].fieldname).AsString := trim(UpperCase(fields[i].AsString));
              end
          else
            begin
              QVandQ_FIXREC.fieldbyname(fields[i].fieldname).value := fields[i].value;
            end;
          end; // case
        except on e: exception do
            stradd(result, ' EditVQ() FOR LOOP error:  ' + e.message);
        end;
      end; // else
    end;
  end; // with do

  try
    QVandQ_FIXREC.Post;
  except on e: exception do
      stradd(result, 'EditVQ() post error --> ' + e.message);
  end; //try

  application.ProcessMessages;
end;

function TUpdateDM.insertVQ(): string;
var temp: string; // if you edit this, remember to edit EditVQ() - that was a cut and paste
  i: Integer;
begin
  result := '';

  try
    Tbl_Target_VQ.Insert;
  except;
  end;

  Tbl_Target_VQ.FieldByName('basesys').AsString :=
    trim(Uppercase(QSource.FieldByName('basesys').AsString));

  Tbl_Target_VQ.FieldByName('clientid').AsString := ''; // maybe we have client ID in vouch later?

  temp := '';

  // remove addition of credit card info per Steve Grant

  Tbl_Target_VQ.FieldByName('cryptcc').AsString := '';

  temp := '';

  with QSource do
  begin
    for i := 0 to fieldcount - 1 do
    begin
      temp := '';
      temp := trim(LowerCase(fields[i].fieldname));
      if (temp = 'basesys') or
        (temp = 'clientid') or
        (temp = 'vccrcard') or
        (temp = 'cryptcc') or
        (temp = 'lastname') or // these new fields do not exist in vouchAndQueue
        (temp = 'firstname') or // ~ brought in for the summary table.
        (temp = 'initial') or
        (temp = 'client_addr1') or
        (temp = 'client_addr2') or
        (temp = 'client_city') or
        (temp = 'client_prov') or
        (temp = 'client_postal') or
        (temp = 'client_phone') or
        (temp = 'crap') or
        (temp = 'client_ctry') or
        (temp = 'clemail') or
        (temp = 'agemail2') then continue
      else
      begin
        try
          case fields[i].DataType of
            ftString:
              begin // format all strings:
                Tbl_Target_VQ.fieldbyname(fields[i].fieldname).AsString := trim(UpperCase(fields[i].AsString));
              end
          else
            begin
              Tbl_Target_VQ.fieldbyname(fields[i].fieldname).value := fields[i].value;
            end;
          end; // case
        except on e: exception do
            stradd(result, ' FOR LOOP:  ' + e.message);
        end;
      end; // else
    end;
  end; // with do

  try
    Tbl_Target_VQ.Post;
  except on e: exception do
      stradd(result, 'post --> ' + e.message);
  end; //try
  application.ProcessMessages;
end;    

procedure TUpdateDM.VQCopyOneRecord;
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update')
  else
  begin
    sbasesys := '';
    svcnumb := '';
    svchrel := '';
    ivchrel := 0;
    errstr := '';

    sbasesys := QSource.FieldByName('basesys').AsString;
    svcnumb := QSource.FieldByName('vcnumb').AsString;
    svchrel := QSource.FieldByName('vchrel').AsString;
    ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
    ivchrel := QSource.FieldByName('vchrel').AsInteger;

    errstr := insertVQ();

    if errstr > '' then savelogfile('dss_upd', sbasesys +
        ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

    errstr := '';
    if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

    if errstr > '' then savelogfile('dss_upd', sbasesys +
        ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);
  end;
end;

procedure TUpdateDM.VQCopyForDates(BaseSys: string; startDT, EndDT: TDate);
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data
  if not (connTarget.Connected) then
  begin
    try
     connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.Add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    FormDSSFeed.memo1.Lines.add('no data to update')
  else
  begin
    QSource.First;

    while not (QSource.Eof) do
    begin
      if FormDSSFeed.bkill then break;

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';
      sbasesys := QSource.FieldByName('basesys').AsString;
      svcnumb := QSource.FieldByName('vcnumb').AsString;
      svchrel := QSource.FieldByName('vchrel').AsString;
      ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QSource.FieldByName('vchrel').AsInteger;

      errstr := insertVQ();

      if errstr > '' then FormDSSFeed.memo1.Lines.add(sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

      errstr := '';
      if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

      if errstr > '' then FormDSSFeed.memo1.Lines.add(sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);

      QSource.Next;
    end;
  end;
  VQCheckSums(startDT, endDT, BaseSys);
  FormDSSFeed.memo2.Lines.add('done');
end; 

procedure TUpdateDM.VCCopyDate;
var sbasesys, svcnumb, svchrel, errstr: string;
  ivchrel, ivcnumb: integer;
begin // assume source is open and has data

  if not (connTarget.Connected) then
  begin
    try
     connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (Tbl_Target_VQ.Active) then
  begin
    try
      Tbl_Target_VQ.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if QSource.IsEmpty then
    savelogfile('dss_upd', 'No data to update')
  else
  begin
    QSource.First;

    while not (QSource.Eof) do
    begin
      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';
      sbasesys := QSource.FieldByName('basesys').AsString;
      svcnumb := QSource.FieldByName('vcnumb').AsString;
      svchrel := QSource.FieldByName('vchrel').AsString;
      ivcnumb := QSource.FieldByName('vcnumb').AsInteger;
      ivchrel := QSource.FieldByName('vchrel').AsInteger;

      errstr := insertVQ();

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' InsertVQ() --> ' + errstr);

      errstr := '';
      if ivchrel > 1 then errstr := Update_VCNTYPE(sbasesys, ivcnumb, ivchrel);

      if errstr > '' then savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' Update_VCNTYPE() --> ' + errstr);

      QSource.Next;
    end;
  end;

  VQCheckSums(FormDSSFeed.dtpStart.date, FormDSSFeed.dtpEnd.date, FormDSSFeed.cbSourceParam.Text);
  FormDSSFeed.memo1.Lines.add('done');
end;

function TUpdateDM.OpenSourceVQ(InBaseSys: string; date1, date2: TDate): boolean;
begin
  result := false;
  FormDSSFeed.dbgrid1.DataSource :=dsSource;
  FormDSSFeed.dbgrid2.DataSource := dsTargetVQ;
  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDSSFeed.caption := '';
      exit;
    end;
  end;

  try
    QSource.SQL.Clear;
    QSource.SQL.Text := SQL_GET_VANDC;

    QSource.SQL.Text := StringReplace(QSource.SQL.Text, '[[basesys]]',
      InBaseSys, [rfReplaceAll, rfIgnoreCase]);

    QSource.ParamByName('bdate1').AsDate := date1;
    QSource.ParamByName('bdate2').AsDate := date2;

    FormDSSFeed.memo2.Lines := QSource.SQL;
    FormDSSFeed.memo2.Lines.add('date1: ' + QSource.ParamByName('bdate1').AsString);
    FormDSSFeed.memo2.Lines.add('date2: ' + QSource.ParamByName('bdate2').AsString);
    FormDSSFeed.caption := 'connecting to ' + GetCurrentDB(connSource);

    QSource.Open;

    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connSource);
    FormDSSFeed.memo2.Lines.add('connected to ' + GetCurrentDB(connSource));
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDSSFeed.caption := '';
      exit;
    end;
  end;
  result := true;
end;  

procedure TUpdateDM.VQTargetOpen;
begin
  if (Tbl_Target_VQ.Active) then Tbl_Target_VQ.Close;
  if connTarget.Connected then connTarget.Close;

  try
    connTarget.Connected := true;
  except on e: exception do
    begin
      FormDSSFeed.memo2.lines.add(e.Message);
      exit;
    end;
  end;

  try
    Tbl_Target_VQ.Open;
  except on e: exception do
    begin
      FormDSSFeed.memo2.lines.add(e.Message);
      exit;
    end;
  end;
end;   

function TUpdateDM.Update_VCNTYPE(bsys: string; vch, rel: Integer): string;
var MyQuery: TIfxQuery; // update vcntype to History when new vouchers come in
  theSQL, v, r: string;
  i: integer;
begin
  result := '';

  theSQL := 'update vouchandqueue set vcntype = "H" where basesys = :sbasesys and ' +
    'vcnumb = :InVCNUMB and vchrel < :Invchrel';

  bsys := trim(Uppercase(bsys));
  if bsys = '' then
  begin
    result := 'no basesys';
    exit;
  end;

  v := '';
  r := '';
  i := 0;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      r := IntToStr(rel);
      v := IntTOStr(vch);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('sbasesys').AsString := bsys;
      MyQuery.ParamByName('InVCNUMB').AsInteger := vch;
      MyQuery.ParamByName('Invchrel').AsInteger := rel;
      i := MyQuery.ExecSQL;
    except on e: exception do
      begin
        result := bsys + ' voucher: ' + IntTostr(vch) + '-' + IntToStr(rel) + ' --> ' + E.Message;
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
end;

procedure TUpdateDM.VQCheckSums(dstart, dend: TDate; basesys: string);
var sourceq, targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  temp: string;
begin // assuming connected
  sourceSUM := 0.0;
  TargetSUM := 0.0;
  basesys := trim(Uppercase(basesys));
  try
    try
      sourceq := GetIDACQuery(connSource);
      sourceq.sql.Add(SQL_CHECKSUMSOURCE);
      sourceq.ParamByName('date1').AsDate := dstart;
      sourceq.ParamByName('date2').AsDate := dend;
      sourceq.Active := true;

      if (sourceq.IsEmpty) then sourcesum := 0.0
      else sourcesum := sourceq.Fields[0].AsFloat;

    except on e: exception do
      begin
        sourcesum := 0;
        savelogfile('dss_upd', 'checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        FormDSSFeed.memo1.Lines.add('checksum source fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(sourceq);
  end;

  try
    try
      targetq := GetIDACQuery(connTarget);
      targetq.sql.Add(SQL_CHECKSUMTARGET);
      targetq.ParamByName('date1').AsDate := dstart;
      targetq.ParamByName('date2').AsDate := dend;
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      if (targetq.IsEmpty) then targetsum := 0.0
      else targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := 0;
        savelogfile('dss_upd', 'checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        FormDSSFeed.memo1.Lines.add('checksum target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(targetq);
  end;

  temp := '';
  if targetsum = sourcesum then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' VandC OK! : ' + 'source: ' +
     FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDSSFeed.LogList.Add(temp);
    except;
    end;

  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' VandC Fail! : ' + 'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' target: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDSSFeed.LogList.Add(temp);
    except;
    end;
    FormDSSFeed.memo1.Lines.add(temp);
  end;
  FormDSSFeed.memo2.Lines.add(temp);
end; 

function TUPdateDM.SummaryCheckSum(): string;
var errorList: TStringList;
  MyQuery: TIfxQuery;
  sourceTot, TargetTot: largeint;
  temp: string;
begin
  // assume conntarget is open already
  result := '';
  if not connTarget.Connected then
  try
   conntarget.Connected := true;
  except;
  end;

  try
    errorList := TStringList.Create;
    try
      MyQuery := GetIdacQuery(connTarget);
      sourceTot := 0;
      TargetTot := 0;

      MyQuery.SQL.Clear;
      MyQuery.SQL.Text := 'select sum(vs_voucher) from g_voucher_summary where vs_paid_dt >= :InDate;';
      MyQuery.Params[0].AsDate := trunc(now() - 30);
      MyQuery.Open;
      targetTot := MyQuery.Fields[0].AsInteger;

      MyQuery.Close;
      MyQuery.SQL.Clear;
      MyQuery.SQL.Text := 'Select sum(vcnumb) From vouchandqueue where vchpqbath >= :InDate;';
      MyQuery.Params[0].AsDate := trunc(now() - 30);
      MyQuery.Open;
      sourceTot := MyQuery.Fields[0].AsInteger;

      if sourcetot = targettot then temp := 'OK'
      else
      begin
        temp := 'fail';
        raise(exception.Create('summary checksum does not match. Look for missing data in summary.'));
      end;

      result := 'summary check sum ' + temp + ' source: ' + IntToStr(sourcetot) + ' target: ' + IntToStr(targettot);
    except on e: exception do
      begin
        raise(exception.Create('summary checksum err --> ' + e.Message));
      end;
    end;
  finally
    if assigned(errorList) then freeandNil(errorList);
    if assigned(MyQuery) then freeandNil(MyQuery);
  end;
end;   

procedure TUpdateDM.UpdateStats;
var intResult: integer;

  procedure asdf();
  begin
    application.ProcessMessages;
    intResult := 0;
  end;

  procedure sav(InStr: string);
  begin
    FormDSSFeed.memo2.Lines.Add(Instr);
    FormDSSFeed.SetBar1(InStr);
    jSaveFile('c:\clog\', 'dss_AutoRunLog', '.txt', InStr);
  end;

begin
  // don't need this - backup including update stats runs at 8AM every day
  // this feed runs before that
  exit;
  asdf();

  try
    setconn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'target connect fail Update Stats --> ' + E.Message);
    end;
  end;

  try
    sav('update statistics start.');
    if FormDSSFeed.bkill then exit;

    // g_vendor:
    intResult :=connTarget.execute('update statistics medium for table g_vendor distributions only;');
    sav('update statistics medium for table g_vendor distributions only; --> ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_vendor(gv_basesys);');
    sav('update statistics high for table g_vendor(gv_basesys); --> ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_vendor(gv_iata);');
    sav('update statistics high for table g_vendor(gv_iata); --> ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    // vouchandqueue
    intResult :=connTarget.execute('update statistics medium for table vouchandqueue distributions only;');
    sav('update statistics medium for table vouchandqueue distributions only; --> ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table vouchandqueue(basesys);');
    sav('update statistics high for table vouchandqueue(basesys); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(vchpqbath);');
    sav('update statistics low for table vouchandqueue(vchpqbath);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table vouchandqueue(vciata);  ');
    sav('update statistics high for table vouchandqueue(vciata);  ' + IntToStr(intResult));
    asdf();

    intResult :=connTarget.execute('update statistics high for table vouchandqueue(vopctry1); ');
    sav('update statistics high for table vouchandqueue(vopctry1); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(vopcity1); ');
    sav('update statistics low for table vouchandqueue(vopcity1); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(vopid); ');
    sav('update statistics low for table vouchandqueue(vopid); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(voprd1); ');
    sav('update statistics low for table vouchandqueue(voprd1); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(vcntype); ');
    FormDSSFeed.memo2.Lines.Add('update statistics low for table vouchandqueue(vcntype); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(voconsort); ');
    FormDSSFeed.memo2.Lines.Add('update statistics low for table vouchandqueue(voconsort); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table vouchandqueue(vopdate1); ');
    sav('update statistics high for table vouchandqueue(vopdate1); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table vouchandqueue(vcnumb); ');
    sav('update statistics high for table vouchandqueue(vcnumb); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table vouchandqueue(vchrel); ');
    sav('update statistics low for table vouchandqueue(vchrel); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    // g_client:
    intResult :=connTarget.execute('update statistics medium for table g_client distributions only;');
    sav('update statistics medium for table g_client distributions only; ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_client(gc_key); ');
    sav('update statistics high for table g_client(gc_key); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_client(gc_lastname); ');
    sav('update statistics high for table g_client(gc_lastname); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_client(gc_firstname); ');
    sav('update statistics low for table g_client(gc_firstname); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_client(gc_email); ');
    sav('update statistics high for table g_client(gc_email); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_client(gc_system); ');
    sav('update statistics high for table g_client(gc_system); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_client(gc_voucher); ');
    sav('update statistics high for table g_client(gc_voucher); ' + IntToStr(intResult));
    asdf();

    // report tracker:
    intResult :=connTarget.execute('update statistics high for table dss_track_reports(dtr_key); ');
    sav('update statistics high for table dss_track_reports(dtr_key); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table dss_track_reports(dtr_user_name); ');
    sav('update statistics high for table dss_track_reports(dtr_user_name); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table dss_track_reports(dtr_run_date);  ');
    sav('update statistics high for table dss_track_reports(dtr_run_date);  ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;

    // voucher summary:
    intResult :=connTarget.execute('update statistics medium for table g_voucher_summary distributions only;');
    sav('update statistics medium for table g_voucher_summary distributions only; ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_voucher_summary(vs_basesys);');
    sav('update statistics high for table g_voucher_summary(vs_basesys); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_voucher_summary(vs_voucher);');
    sav('update statistics high for table g_voucher_summary(vs_voucher); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_release);');
    sav('update statistics low for table g_voucher_summary(vs_release); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_voucher_summary(vs_paid_dt);');
    sav('update statistics high for table g_voucher_summary(vs_paid_dt); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics high for table g_voucher_summary(vs_voucher_create_dt);');
    sav('update statistics high for table g_voucher_summary(vs_voucher_create_dt); ' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_pu_ctry);');
    sav('update statistics low for table g_voucher_summary(vs_pu_ctry);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_pu_city);');
    sav('update statistics low for table g_voucher_summary(vs_pu_city);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_operator_code);');
    sav('update statistics low for table g_voucher_summary(vs_operator_code);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_direct_or_ta);');
    sav('update statistics low for table g_voucher_summary(vs_direct_or_ta);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_consortium);');
    sav('update statistics low for table g_voucher_summary(vs_consortium);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult :=connTarget.execute('update statistics low for table g_voucher_summary(vs_active_history);');
    sav('update statistics low for table g_voucher_summary(vs_active_history);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    intResult := connTarget.execute('update statistics low for table g_voucher_summary(vs_sipp);');
    sav('update statistics low for table g_voucher_summary(vs_sipp);' + IntToStr(intResult));
    asdf();
    if FormDSSFeed.bkill then exit;
    sav('update statistics done');
    asdf();
  except on e: Exception do
    begin
      savelogfile('dss_upd', 'Err update statistics result --> [' + IntToStr(intResult) + '] ' + e.Message);
      FormDSSFeed.memo2.Lines.Add('Err update statistics result --> [' + IntToStr(intResult) + '] ' + e.Message);
    end;
  end;
end;

procedure TUpdateDM.UpdateIATAS(InSystem: string);
var i, x, z, rowsEffected: Integer;
  deleteQ: TIfxQuery;
  theSQL: string;
begin
  InSystem := trim(Uppercase(InSystem));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InSystem);
    connSource.Open;
    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connSource);
    FormDSSFeed.loglist.add('Update vendors connected to ' + GetCurrentDB(connSource));
    FormDSSFeed.memo2.Lines.Add('Update vendors connected to ' + GetCurrentDB(connSource));
  except on e: exception do
    begin
      raise(exception.Create('UpdateIATAS failed to connect to [' + InSystem + '] --> ' + e.Message));
      FormDSSFeed.caption := '';
      exit;
    end;
  end;

  // open target
  if (TblTargetVendor.active) then TblTargetVendor.Close;
  if connTarget.Connected then connTarget.Close;

  try
    connTarget.Connected := true;
    TblTargetVendor.Active := true;
    FormDSSFeed.logList.add('UpdateIATAS Connected to target OK');
    FormDSSFeed.memo2.Lines.Add('UpdateIATAS Connected to target OK');
  except on e: exception do
    begin
      raise(exception.Create('UpdateIATAS failed to connect to target --> ' + e.Message));
      exit;
    end;
  end;

 // delete target vendors for the system:
  rowsEffected := 0;
  try
    DeleteQ := GetIDACQuery(connTarget);
    try
      DeleteQ.sql.clear;
      DeleteQ.sql.Add('delete from g_vendor where gv_basesys = "' + InSystem + '";');
      rowsEffected := DeleteQ.ExecSQL;

      FormDSSFeed.logList.add('deleted ' + IntToStr(rowsEffected) + ' vendors from g_vendor OK for ' + InSystem);
      FormDSSFeed.memo2.Lines.Add('deleted ' + IntToStr(rowsEffected) + ' vendors from g_vendor OK for ' + InSystem);
    except on e: exception do
      begin
        raise(exception.Create('UpdateIATAS: failed to delete vendors on target table --> ' + e.Message));
        exit;
      end;
    end;
  finally
    DeleteQ.active := false;
    DeleteQ.free;
    DeleteQ := nil;
  end;

  if (InSystem = 'AUS') or (InSystem = 'NEWZ') then
  begin // join apvenaddr here but not other systems
    theSQL := 'select "' + InSystem + '" gv_basesys, ' +
      'trim(upper(apvenmast.vendor)) gv_iata, ' +
      'trim(upper(ven_class)) gv_class, ' +
      'trim(upper(vendor_vname)) gv_name, ' +
      'trim(upper(legal_name)) gv_legal_name, ' +
      'trim(upper(vendor_contct)) gv_contact_name, ' +
      'trim(upper(addr1)) gv_addr1, ' +
      'trim(upper(addr2)) gv_addr2, ' +
      'trim(upper(addr3)) gv_addr3, ' +
      'trim(upper(addr4)) gv_addr4, ' +
      'trim(upper(city_addr5)) gv_city, ' +
      'trim(upper(county)) gv_prov, ' + // -- county is used for state
      'trim(upper(postal_code)) gv_postal, ' +
      'trim(upper(country)) gv_country, ' +
      'trim(upper(phone_prefix)) gv_phone_prefix, ' +
      'trim(upper(phone_num)) gv_phone_num, ' +
      'trim(upper(fax_prefix)) gv_fax_prefix, ' +
      'trim(upper(fax_num)) gv_fax_num, ' +
      'trim(upper(agemail2)) gv_email, ' +
      'trim(upper(agconsortium)) gv_consortium, ' +
      'trim(upper(charge_code)) gv_isdc ' +
      'from apvenmast, agfile, outer(apvenaddr) ' +
      'where apvenmast.vendor_group = "DRIV" and ' +
      'inumb = apvenmast.vendor ' +
      'and apvenaddr.vendor = apvenmast.vendor ' +
      'and apvenaddr.vendor_group = apvenmast.vendor_group;';
  end
  else
  begin
    theSQL := 'select "' + InSystem + '" gv_basesys, ' +
      'trim(upper(apvenmast.vendor)) gv_iata, ' +
      'trim(upper(ven_class)) gv_class, ' +
      'trim(upper(vendor_vname)) gv_name,  ' +
      'trim(upper(legal_name)) gv_legal_name,  ' +
      'trim(upper(vendor_contct)) gv_contact_name, ' +
      'trim(upper(addr1)) gv_addr1, ' +
      'trim(upper(addr2)) gv_addr2, ' +
      'trim(upper(addr3)) gv_addr3, ' +
      'trim(upper(addr4)) gv_addr4,  ' +
      'trim(upper(city_addr5)) gv_city,  ' +
      'trim(upper(state_prov)) gv_prov,  ' + // diff between this and aus/nz
      'trim(upper(postal_code)) gv_postal, ' +
      'trim(upper(country)) gv_country,  ' +
      'trim(upper(phone_prefix)) gv_phone_prefix,  ' +
      'trim(upper(phone_num)) gv_phone_num,  ' +
      'trim(upper(fax_prefix)) gv_fax_prefix,  ' +
      'trim(upper(fax_num)) gv_fax_num,  ' +
      'trim(upper(agemail2)) gv_email, ' +
      'trim(upper(agconsortium)) gv_consortium, ' +
      'trim(upper(charge_code)) gv_isdc ' +
      'from apvenmast, agfile  ' +
      'where apvenmast.vendor_group = "MSTR"  ' +
      'and inumb = apvenmast.vendor;  ';
  end;

  // now get and refresh the vendors
  try
    qsourceVENDOR.Close;
    qsourceVENDOR.SQL.Clear;
    qsourceVENDOR.SQL.Add(theSQL);
    qsourceVENDOR.Open;
    FormDSSFeed.logList.add('UpdateIATAS opened OK for ' + InSystem);
    FormDSSFeed.memo2.Lines.Add('UpdateIATAS opened OK for ' + InSystem);
  except on e: exception do
    begin
      raise(exception.Create('UpdateIATAS failed to open source for ' + InSystem + '  --> ' + e.Message));
      exit;
    end;
  end;

  z := 0;
  // save some time if autoamted process
  if not (trim(lowercase(paramstr(1))) = '/update') then z := qsourceVENDOR.RecordCount;

  qsourceVENDOR.First;
  FormDSSFeed.bkill := false;
  x := 0;

  while not qsourceVENDOR.Eof do
  begin
    application.ProcessMessages;
    try
      tblTargetVendor.Insert;

      with qsourceVENDOR do
      begin
        if FormDSSFeed.bkill then break;

        for i := 0 to fieldcount - 1 do
        begin
          try
            tblTargetVENDOR.fieldbyname(fields[i].fieldname).value := fields[i].value;
          except on e: exception do
            begin
              FormDSSFeed.memo2.lines.add('update fields err: ' + e.message);
              savelogfile('dss_upd', 'update fields err: ' + e.Message);
            end;
          end;
        end;
      end;

      tblTargetVENDOR.Post;
      inc(x);

      try
        FormDSSFeed.gbIATAUpdates.Caption := 'updates: ' + IntToStr(x) + ' of ' + INtToStr(z);
      except;
      end;

    except on e: exception do
      begin
        tblTargetVENDOR.Cancel;
        FormDSSFeed.memo2.Lines.add('UpdateIATAS failed post ' + InSystem + '  --> ' + e.Message);
        savelogfile('dss_upd', 'UpdateIATAS failed post ' + InSystem + '  --> ' + e.Message);
      end;
    end;

    qsourceVENDOR.Next;
  end; // while

  FormDSSFeed.logList.add('UpdateIATAS done for ' + InSystem);
  FormDSSFeed.memo2.Lines.Add('UpdateIATAS done for ' + InSystem);

  try
    qsourceVENDOR.Close;
    tblTargetVENDOR.Close;
    connTarget.Close;
    connSource.Close;
  except;
  end;
end; // update vendors (iatas)

procedure TUpdateDM.Update_dcemail();
var cnt: Integer;
  eml, errStr, sys: string;
begin
  application.ProcessMessages;

  if (not (q_dc_email.Active)) or (q_dc_email.IsEmpty) then
  begin
    FormDSSFeed.memo1.lines.Add('update g_dc_email: q_dc_email - no data');
    savelogfile('dss_dcmail_log', 'update g_dc_email - no data.');
    exit;
  end;

  q_dc_email.First;
  cnt := 0;

  while not q_dc_email.Eof do
  begin
    inc(cnt);

    eml := '';
    errStr := '';
    sys := '';
    eml := trim(uppercase(q_dc_email.FieldByName('gc_email').AsString));
    sys := trim(uppercase(q_dc_email.FieldByName('vs_basesys').AsString));

    application.ProcessMessages;
    if FormDSSFeed.bkill then break;

    if IsValidEMailAddr(eml, errStr) then
    begin
      // get valid dataset for system and emladdr and put into mem table
      getDCEmlUpdateInfo(eml, sys);
      SetDCEmlUpdateInfo(); // update/insert for everything in mem
      DeleteObsoleteDCMAILRecords(sys, eml); // remove records for that eml where not in mem
    end
    else // not a valid e-mail addr
    begin
      FormDSSFeed.memo2.Lines.add('skipped: ' + errStr);
    end;

    // if cnt>=5 then  kill := true;
    q_dc_email.Next;
  end; // while
end;

procedure TUpdateDM.Get_dcMail_data(date1, date2: TDate; basesystem: string);
begin // tested - works well
  try // get data for g_dc_email update
    date1 := trunc(date1);
    date2 := trunc(date2);
    basesystem := trim(uppercase(basesystem));

    q_dc_email.Close;
    q_dc_email.Connection := connTarget;
    q_dc_email.SQL.Clear;

    q_dc_email.SQL.Add('Select distinct vs_basesys, gc_email From g_voucher_summary, g_client');
    q_dc_email.SQL.Add('where vs_basesys = :basessystem');
    q_dc_email.SQL.Add('and vs_paid_dt between :date1 and :date2');
    q_dc_email.SQL.Add('and vs_direct_or_ta = "DC"');
    q_dc_email.SQL.Add('and vs_active_history = "A"');
    q_dc_email.SQL.Add('and (length(gc_email) > 0 and gc_email is not null)  ');
    q_dc_email.SQL.Add('and gc_system = vs_basesys');
    q_dc_email.SQL.Add('and gc_voucher = vs_voucher');
    q_dc_email.SQL.Add('and');
    q_dc_email.SQL.Add('vs_iata NOT in('); //   -- opt out iatas
    q_dc_email.SQL.Add('Select gmoi_iata From g_mailer_optout_iata');
    q_dc_email.SQL.Add('where gmoi_basesys = "*" and gmoi_active = "A"');
    q_dc_email.SQL.Add(') and');
    q_dc_email.SQL.Add('gc_email not in ( '); //-- opt out emails
    q_dc_email.SQL.Add('Select mailer_addr From g_mailer_optout');
    q_dc_email.SQL.Add('where mailer_basesys = "*" and mailer_active = "A")');

    q_dc_email.ParamByName('basessystem').AsString := basesystem;
    q_dc_email.ParamByName('date1').AsDateTime := trunc(date1);
    q_dc_email.ParamByName('date2').AsDateTime := trunc(date2);

    FormDSSFeed.memo2.Lines.add('sweep update sql');
    FormDSSFeed.memo2.Lines.add(q_dc_email.SQL.Text);
    FormDSSFeed.memo2.Lines.add(q_dc_email.ParamByName('basessystem').AsString);
    FormDSSFeed.memo2.Lines.add(q_dc_email.ParamByName('date1').AsString);
    FormDSSFeed.memo2.Lines.add(q_dc_email.ParamByName('date2').AsString);




    q_dc_email.open;

    FormDSSFeed.memo2.Lines.add('data retreived OK for [' + basesystem + ' dates [' +
      q_dc_email.ParamByName('date1').AsString + ' - ' + q_dc_email.ParamByName('date2').AsString + ']');

    try
      savelogfile('dss_dcmail_log', '< -------Get_dcMail_data()------- >');
      savelogfile('dss_dcmail_log', 'data retreived OK for [' + basesystem + ' dates [' +
        q_dc_email.ParamByName('date1').AsString + ' - ' + q_dc_email.ParamByName('date2').AsString + '] records returned [' +
        inttostr(q_dc_email.RecordCount) + ']');
      savelogfile('dss_dcmail_log', 'the SQL: ' + q_dc_email.SQL.text);
      savelogfile('dss_dcmail_log', '<-- end sql -->');


    except;
    end;

  except on e: exception do
    begin
      raise(exception.Create('update g_dc_email: system: ' + baseSystem + datetostr(date1) + ' to ' + datetostr(date2) + ' --> source connect fail --> ' + E.Message));
        //  exit;
    end;
  end;
end;       

function TUpdateDM.AutoUpdate_dcemail(basesystem: string): string;
var
  ErrorList: TStringList; // main loop for updating dc_email table
  date1, date2, MyDate, fiveYearsAgo, oneYearAgo: TDate;
  errStr, eml: string;
  cnt: integer;
begin
  result := '';
  basesystem := trim(uppercase(basesystem));
  if (basesystem = 'HOHO') then exit;
  ErrorList := TStringList.create;
  FormDSSFeed.bkill := false;
  application.ProcessMessages;

  try
    try
      MyDate := 0;
      date1 := 0;
      date2 := 0;
      MyDate := GetLastDCEMLUpdate(basesystem); // sets target connection active if not
                                                 // also validates baseSystem
      if MyDate = 0 then
      begin
        ErrorList.add('AutoUpdate_dcemail: No date returned from GetLastDCEMLUpdate(' + basesystem + ') *** aborted ***');
        result := errorList.Text;
        exit;
      end
      else
      begin
        date1 := Mydate + 1;
       // update thru this date - i.e. latest batch completed by VP Department
        date2 := GetLastVQUpdate(basesystem);

        if (date2 = 0) then
        begin
          ErrorList.add('AutoUpdate_dcmail: failed to get "update thru" date for system [' + basesystem + ']');
          result := errorlist.Text;
          exit;
        end;

        if (trunc(date2) < trunc(date1)) then
        begin
          result := 'AutoUpdate_dcmail: No need to update dcemail for [' + basesystem + '] last updated: [' + DateTOStr(Date1) + ']';
          exit;
        end;
      end; // MyDate <> 0

      try
        FormDSSFeed.memo2.Lines.add('dcemail updating: ' + baseSystem + ' FROM: ' + datetostr(date1) + ' to ' + datetostr(date2));
        FormDSSFeed.memo2.Lines.add('opening query q_dc_email..');
        savelogfile('dss_dcmail_log', 'dcemail updating: ' + baseSystem + ' FROM: ' + datetostr(date1) + ' to ' + datetostr(date2));
      except;
      end;

      FormDSSFeed.BzDateRec := FormDSSFeed.GetBizDates(connTarget); // load current dates extracted from g_dcemail_rules table
      fiveYearsAgo := FormDSSFeed.BzDateRec.totalBizDaysBackDt;
      oneYearAgo := FormDSSFeed.BzDateRec.churnedDaysBackDt;
      FormDSSFeed.memo2.Lines.add('1 years ago: ' + DateToStr(OneYearAgo));
      FormDSSFeed.memo2.Lines.add('5 years ago: ' + DateToStr(fiveYearsAgo));

      FormDSSFeed.dbgrid1.DataSource := ds_dc_email;
      try
        Get_dcMail_data(date1, date2, basesystem);
      except on e: Exception do
        begin
          ErrorList.add('update g_dc_email: system: ' + baseSystem + datetostr(date1) + ' to ' +
            datetostr(date2) + ' --> Get_dcMail_data fail --> ' + E.Message);

          savelogfile('dss_dcmail_log', 'update g_dc_email: system: ' + baseSystem + datetostr(date1) + ' to ' +
            datetostr(date2) + ' --> Get_dcMail_data fail --> ' + E.Message);

          exit;
        end;
      end;


      application.ProcessMessages;

      if (not (q_dc_email.Active)) or (q_dc_email.IsEmpty) then
      begin
        ErrorList.Add('update g_dc_email: q_dc_email - no data');
        savelogfile('dss_dcmail_log', 'update g_dc_email: q_dc_email - no data');

        exit;
      end;

      Update_dcemail(); // run through the dataset & update g_dcemail table

      FormDSSFeed.memo2.Lines.add('update done.');

      if SetLatestDCEMLUpdate(baseSystem, trunc(date2)) then
      begin
        try
          FormDSSFeed.memo2.Lines.Add('last dcmail upd set ok for ' + basesystem + ' to ' + datetostr(date2));
        except;
        end;
      end
      else
      begin
        try
          FormDSSFeed.memo2.Lines.Add('FAILED last dcmail upd set for ' + basesystem + ' to ' +
            datetostr(date2));
        except;
        end;
      end;
    except on e: exception do
      begin
        ErrorList.add('AutoUpdate_dcemail: General exception --> ' + e.Message);
        savelogfile('dss_dcmail_log', 'AutoUpdate_dcemail: General exception --> ' + e.Message);
      end;
    end;

    if errorList.Text > '' then
    begin
      savelogfile('dss_upd', errorList.Text);
    end;
  finally
    if assigned(ErrorList) then FreeandNil(ErrorList);
  end;
end;

procedure TUpdateDM.getDCEmlUpdateInfo(InEmailAddr, InSystem: string);
var
  theSQL, isMultyCountry, cust_class, churn_class: string;
  bizBackDt, churnBackDt: TDate;
  i: Integer;
  theStart, theend: DWORD;
begin // get info needed to update g_dcemail from g_voucher_summary, g_client
      // put it into a memtable
  if not connTarget.Connected then
  begin
    FormDSSFeed.memo1.Lines.add('getDCEmlUpdateInfo not connected to WHSE)');
    savelogfile('dss_dcmail_log', 'getDCEmlUpdateInfo not connected to WHSE ' + InEmailAddr + ' ' + inSystem);
    exit;
  end;

  theStart := 0;
  theEnd := 0;
  theStart := GetTickCount();

  try
    if not (memdcmailUpdt.active) then memdcmailUpdt.active := true;
    memdcmailUpdt.emptytable;
  except on e: Exception do
    begin
      FormDSSFeed.memo1.lines.add('mem error -->' + e.message);
      savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: mem empty error --> ' + e.message);
      exit;
    end;
  end;

  try FormDSSFeed.dbgrid3.dataSource := ds_memdcmailUpdt; except; end;

  InEmailAddr := trim(uppercase(InEmailAddr));
  InSystem := trim(uppercase(InSystem));

  bizBackDt := 0;
  churnBackDt := 0;
  // called Conn.AfterConnect - why call GetBizDates() every time..once only
  bizBackDt := FormDSSFeed.BzDateRec.totalBizDaysBackDt;
  churnBackDt := FormDSSFeed.BzDateRec.churnedDaysBackDt;

  theSQL :=
    'Select vs_basesys as eml_system, gc_email eml_email, vs_pu_ctry eml_pu_ctry, ' +
    'count(*) as eml_booking_count, sum(vs_profit) eml_profit ' +
    'From g_voucher_summary, g_client ' +
    'where vs_basesys = :basesys ' +
  //-- must be last 5 years - later use rules table for this
  'and vs_paid_dt between :fiveYearsAgo and current ' +
    'and vs_direct_or_ta = "DC" ' +
    'and vs_active_history = "A" ' +
    'and gc_email = :inEmail ' +
    'and gc_system = vs_basesys ' +
    'and gc_voucher = vs_voucher   ' +
    'group by 1,2,3';

  try
    QgetDCMAILUpdateInfo.Close;
    QgetDCMAILUpdateInfo.SQL.Clear;
    QgetDCMAILUpdateInfo.SQL.Add(theSQL);
    QgetDCMAILUpdateInfo.ParamByName('basesys').AsString := InSystem;
    QgetDCMAILUpdateInfo.ParamByName('fiveYearsAgo').AsDateTime := trunc(bizBackDt);
    QgetDCMAILUpdateInfo.ParamByName('inEmail').AsString := InEmailAddr;
  except on e: exception do
    begin
      FormDSSFeed.memo1.Lines.Add('close,clear,set error --> ' + e.Message);
      savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: close, clear, set error --> ' + e.message);
      try QgetDCMAILUpdateInfo.Close; except; end;
      exit;
    end;
  end;

  try
    QgetDCMAILUpdateInfo.Open;
  except on e: exception do
    begin
      FormDSSFeed.memo1.Lines.Add('open error --> ' + e.Message);
      savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: open error --> ' + e.message);
      exit;
    end;
  end;


    // if there is nothing returned, they have cancelled the booking most likely.
  // this is assuming the active record was inserted into g_dcemail before

  if QgetDCMAILUpdateInfo.IsEmpty then exit;


  // here, we have eml_system, eml_email, eml_pu_ctry, eml_booking_count and eml_profit
  // 1 to N records. Need to add eml_cust_class, eml_churn_class, eml_is_multi_ctry, eml_dt_updated

  isMultyCountry := 'N';
  if QgetDCMAILUpdateInfo.Recordcount > 1 then isMultyCountry := 'Y';

  QgetDCMAILUpdateInfo.first;
  while not (QgetDCMAILUpdateInfo.eof) do
  begin
    try
      memdcmailUpdt.insert;
      memdcmailUpdteml_system.asstring := QgetDCMAILUpdateInfo.fieldbyname('eml_system').asstring;
      memdcmailUpdteml_email.asstring := QgetDCMAILUpdateInfo.fieldbyname('eml_email').asstring;
      memdcmailUpdteml_pu_ctry.AsString := QgetDCMAILUpdateInfo.fieldbyname('eml_pu_ctry').asstring;
      memdcmailUpdteml_booking_count.asInteger := QgetDCMAILUpdateInfo.fieldbyname('eml_booking_count').asInteger;
      memdcmailUpdteml_profit.AsFloat := QgetDCMAILUpdateInfo.fieldbyname('eml_profit').asFloat;
      memdcmailUpdteml_cust_class.asString := '??????????';
      memdcmailUpdteml_churn_class.asString := '??????????';
      memdcmailUpdteml_is_multi_ctry.asString := isMultyCountry;
      memdcmailUpdt.post;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('getDCEmlUpdateInfo mem insert err --> [' + e.Message + ']');
        savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: mem insert error --> ' + e.message);
        memdcmailUpdt.Cancel;
      end;
    end;
    QgetDCMAILUpdateInfo.next;
  end;

  // now set mememl_cust_class.asString and mememl_churn_class.asString;
  i := 0;
  memdcmailUpdt.first;
  while not (memdcmailUpdt.eof) do
  begin
    i := i + memdcmailUpdteml_booking_count.asInteger;
    memdcmailUpdt.next;
  end;

  cust_class := '??????????';
  try
    QgetDCMAILUpdateInfo.Close;
    QgetDCMAILUpdateInfo.SQL.Clear;
    QgetDCMAILUpdateInfo.SQL.add('select emr_rulename from g_dcemail_rules where ');
    QgetDCMAILUpdateInfo.SQL.add('emr_bookings_min <= :asdf and emr_bookings_max >= :asdf');
    QgetDCMAILUpdateInfo.ParamByName('asdf').AsInteger := i;
    QgetDCMAILUpdateInfo.open;
    cust_class := QgetDCMAILUpdateInfo.Fields[0].AsString;
  except on e: exception do
    begin
      FormDSSFeed.memo1.Lines.Add('get cust_class error --> ' + e.Message);
      savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: get cust class error --> ' + e.message);
      cust_class := '??????????';
      QgetDCMAILUpdateInfo.Close;
    end;
  end;

  // now determine is the customer is lost or active
  churn_class := '??????????';
  try
    QgetDCMAILUpdateInfo.Close;
    QgetDCMAILUpdateInfo.SQL.Clear;
    QgetDCMAILUpdateInfo.SQL.add('Select gc_email');
    QgetDCMAILUpdateInfo.SQL.add('From g_voucher_summary, g_client');
    QgetDCMAILUpdateInfo.SQL.add('where vs_basesys = :basesys');
    QgetDCMAILUpdateInfo.SQL.add('and vs_paid_dt between :oneYearAgo and current');
    QgetDCMAILUpdateInfo.SQL.add('and vs_direct_or_ta = "DC"');
    QgetDCMAILUpdateInfo.SQL.add('and vs_active_history = "A" ');
    QgetDCMAILUpdateInfo.SQL.add('and gc_email = :inEmail ');
    QgetDCMAILUpdateInfo.SQL.add('and gc_system = vs_basesys ');
    QgetDCMAILUpdateInfo.SQL.add('and gc_voucher = vs_voucher ');

    QgetDCMAILUpdateInfo.ParamByName('basesys').AsString := InSystem;
    QgetDCMAILUpdateInfo.ParamByName('oneYearAgo').AsDateTime := trunc(churnBackDt);
    QgetDCMAILUpdateInfo.ParamByName('inEmail').AsString := InEmailAddr;
    QgetDCMAILUpdateInfo.open;

    application.ProcessMessages;

    if QgetDCMAILUpdateInfo.IsEmpty then churn_class := 'lost'
    else churn_class := 'active';

  except on e: exception do
    begin
      FormDSSFeed.memo1.Lines.Add('get churn error --> ' + e.Message);
      savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: get churn error --> ' + e.message);
      churn_class := '??????????';
      QgetDCMAILUpdateInfo.Close;
    end;
  end;

  // set values in mem
  memdcmailUpdt.first;
  while not (memdcmailUpdt.eof) do
  begin
    if churn_class = '??????????' then break;

    try
      memdcmailUpdt.edit;
      memdcmailUpdteml_cust_class.asString := cust_class;
      memdcmailUpdteml_churn_class.asString := churn_class;
      memdcmailUpdt.Post;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('mem edit err --> [' + e.Message + ']');
        savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo: mem edit error --> ' + e.message);
        memdcmailUpdt.Cancel;
      end;
    end;
    memdcmailUpdt.next;
  end;

  theEnd := GetTickCount();
  FormDSSFeed.memo2.Lines.Add('getDCEmlUpdateInfo time: ' + IntToStr(theEnd - theStart) + ' ms');
 // savelogfile('dss_dcmail_log', 'getDCRmlUpdateInfo time: ' + e.message);
  QgetDCMAILUpdateInfo.Close;
end;    

procedure TUpdateDM.SetDCEmlUpdateInfo();
var theStart, theend: DWORD;
  theSQL, tmp: string;
  MyQuery: TIfxQuery;
  theDateTime: TDateTime;
begin // updates or inserts a single record by system, pickUpCtry and emlAddress
  if not connTarget.Connected then
  begin
    FormDSSFeed.memo1.Lines.add('SetDCEmlUpdateINfo connect to aewhse first)');
    savelogfile('dss_dcmail_log', 'SetDCEmlUpdateINfo connect to aewhse first)');
    exit;
  end;

  try
    if (not (memdcmailUpdt.active)) or (memdcmailUpdt.IsEmpty) then
    begin
      savelogfile('dss_dcmail_log', 'SetDCEmlUpdateINfo mem empty or not active)');
      exit;
    end;
  except
    exit;
  end;

  tmp := '';
  theDateTime := now();
  theStart := 0;
  theEnd := 0;
  theStart := GetTickCount();

  theSQL := 'select * From g_dcemail where ' +
    'eml_email = :email and eml_system = :sys and eml_pu_ctry = :ctry';

  try
    MyQuery := GetIDACQuery(connTarget);
    MyQuery.RequestLive := true;
    try
      MyQuery.Close;
      MyQuery.SQL.Clear;
      MyQuery.SQL.Add(theSQL);
      memdcmailUpdt.First;

      while not memdcmailUpdt.Eof do
      begin
        try
          MyQuery.Close;
          MyQuery.Params[0].AsString := memdcmailUpdteml_email.AsString;
          MyQuery.Params[1].AsString := memdcmailUpdteml_system.AsString;
          MyQuery.Params[2].AsString := memdcmailUpdteml_pu_ctry.AsString;
          MyQuery.Open;

          if MyQuery.IsEmpty then
          begin
            tmp := 'insert';
            MyQuery.Insert;
          end
          else
          begin
            tmp := 'edit';
            MyQuery.Edit;
          end;

          MyQuery.FieldByName('eml_system').AsString := trim(uppercase(memdcmailUpdteml_system.AsString));
          MyQuery.FieldByName('eml_email').AsString := trim(uppercase(memdcmailUpdteml_email.AsString));
          MyQuery.FieldByName('eml_pu_ctry').AsString := trim(uppercase(memdcmailUpdteml_pu_ctry.AsString));
          MyQuery.FieldByName('eml_booking_count').AsInteger := memdcmailUpdteml_booking_count.AsInteger;
          MyQuery.FieldByName('eml_profit').AsFloat := memdcmailUpdteml_profit.AsFloat;
          MyQuery.FieldByName('eml_cust_class').AsString := trim(lowercase(memdcmailUpdteml_cust_class.AsString));
          MyQuery.FieldByName('eml_churn_class').AsString := trim(lowercase(memdcmailUpdteml_churn_class.AsString));
          MyQuery.FieldByName('eml_is_multi_ctry').AsString := trim(uppercase(memdcmailUpdteml_is_multi_ctry.AsString));
          MyQuery.FieldByName('eml_dt_updated').AsDateTime := theDateTime;
          myQuery.Post;

        except on e: exception do
          begin
            MyQuery.Cancel;

            try
              savelogfile('dss_dcmail_log', 'SetDCEmlUpdateInfo() FAIL: [' + tmp + '] ' +
                trim(uppercase(memdcmailUpdteml_system.AsString)) + ' ' +
                trim(uppercase(memdcmailUpdteml_email.AsString)) + ' ' +
                trim(uppercase(memdcmailUpdteml_pu_ctry.AsString)) + ' --> ' + e.Message
                );
            except;
            end;

            FormDSSFeed.memo1.Lines.add('SetDCEmlUpdateInfo() ' + tmp + ' error --> [' + e.Message + ']');
          end;
        end;
        memdcmailUpdt.Next;
      end; // while
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('SetDCEmlUpdateInfo() generic error --> [' + e.Message + ']');
      end;
    end;
  finally
    if assigned(MyQuery) then
    begin
      MyQuery.Close;
      FreeAndNil(MyQuery);
    end;
  end;
  theEnd := GetTickCount();
  FormDSSFeed.memo2.Lines.Add('Total ' + tmp + ' time: ' + IntToStr(theEnd - theStart) + ' ms');
end;    

procedure TupdateDM.DeleteObsoleteDCMAILRecords(sSystem, sEmlAddr: String);
var InStatement, theSQL, sys, eml, deleteSQL: string;
  i: Integer;
  MyQuery, deleteQuery: TIfxQuery;
  theStart, theend: DWORD;

  function IsInMem(): boolean;
  begin
    result := true;

    try
      if (not (memdcmailUpdt.active)) or (memdcmailUpdt.IsEmpty) then
      begin
       FormDSSFeed.memo2.Lines.add('mem empty [' + sys + '] [' + eml + '] DeleteObsoleteDCMAILRecords.IsInMem()');
       result := false;
       exit;
      end;
    except
      exit;
    end;

    try
      if memdcmailUpdt.Locate('eml_system;eml_email;eml_pu_ctry',
        VarArrayOf([MyQuery.Fields[0].AsString,
        MyQuery.Fields[1].AsString,
          MyQuery.Fields[2].AsString]),
          [loCaseInsensitive]) then result := true
      else
        result := false;
    except;
    end;
  end;


begin
  // get a list from g_dcemail for a system & email
  // traverse list & delete records in g_dcemail that are not in the memTable
  // these have expired if they are not in the mem table, & should be removed
  // mem table holds current qualified list for last 5 years.

  application.ProcessMessages;

  if not connTarget.Connected then
  begin
    FormDSSFeed.memo1.Lines.add('DeleteObsoleteRecords() connect to aewhse first)');
    savelogfile('dss_dcmail_log', 'DeleteObsoleteDCMAILRecords() connect to WHSE first');
    exit;
  end;

  sSystem := trim(uppercase(sSystem));
  sEmlAddr := trim(uppercase(sEmlAddr));

  //try
  //  if (not (memdcmailUpdt.active)) or (memdcmailUpdt.IsEmpty) then exit;
  //except
  //  exit;
  //end;

  sys := '';
  eml := '';
  theStart := 0;
  theEnd := 0;
  theStart := GetTickCount();
  memdcmailUpdt.First;
  //sys := memdcmailUpdteml_system.AsString;
  //eml := memdcmailUpdteml_email.AsString;
  sys := sSystem;
  eml := sEmlAddr;

  theSQL := 'select eml_system, eml_email, eml_pu_ctry From g_dcemail where eml_email = :eml ' +
    '  and eml_system = :sys ';

  deleteSQL := '';

  try
    MyQuery := GetIDACQuery(connTarget);
    DeleteQuery := GetIDACQuery(connTarget);

    try
      MyQuery.Close;
      MyQuery.SQL.Clear;
      MyQuery.SQL.Add(theSQL);
      MyQuery.ParamByName('sys').asstring := sys;
      MyQuery.ParamByName('eml').AsString := eml;
      MyQuery.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('DeleteObsoleteRecords() error --> [' + e.Message + ']');
        savelogfile('dss_dcmail_log', 'DeleteObsoleteRecords() error --> [' + e.Message + ']');
      end;
    end;

    MyQuery.First;
    while not MyQuery.Eof do
    begin
      if (not IsInMem()) then
      begin
        deleteSQL := 'DELETE from g_dcemail where eml_email = "' + MyQuery.Fields[1].AsString + '" ' +
          ' and eml_system = "' + MyQuery.Fields[0].AsString + '" and eml_pu_ctry = "' +
          MyQuery.Fields[2].AsString + '";';

        DeleteQuery.Close;
        DeleteQuery.SQL.Clear;
        DeleteQuery.SQL.Add(deleteSQL);

        try
          i := 0;
          i := DeleteQuery.ExecSQL;
          FormDSSFeed.memo2.Lines.add('DELETED [' + IntTOStr(i) + '] obsolete record from g_dcemail.');
        except on e: exception do
          begin
            deleteQuery.Cancel;
            savelogfile('dss_dcmail_log', 'error deleting ' + e.Message);
            savelogfile('dss_dcmail_log', 'deleteSQL: ' + deleteSQL);
            FormDSSFeed.memo1.Lines.Add('error deleting ' + e.Message);
            FormDSSFeed.memo1.Lines.add('deleteSQL: ' + deleteSQL);
          end;
        end;
      end;
      MyQuery.Next;
    end;
  finally
    if assigned(MyQuery) then
    begin
      MyQuery.Close;
      FreeAndNil(MyQuery);
    end;

    if assigned(DeleteQuery) then
    begin
      DeleteQuery.Close;
      FreeAndNil(DeleteQuery);
    end;
  end;

  theEnd := GetTickCount();
  FormDSSFeed.memo1.Lines.Add('Total DELETE time: ' + IntToStr(theEnd - theStart) + ' ms');
end;      

function TUpdateDM.GetLastDCEMLUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(FormDSSFeed.basesyslist, baseSYS)) then
  begin
    savelogfile('dss_upd', 'GetLastDCEMLUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    SetConn(connTarget); // conntarget is connected to WHSE OnCreate() using set_idac_connection
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastDCEMLpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'select lu_last_update from last_update where ' +
    'lu_basesys = "' + baseSYS + '" and lu_appname = "dc_mail"';

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLastDCEMLUpdate err. system ' + baseSYS + ' --> ' + e.Message);
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end; 

function TUpdateDM.GetLastVQUpdate(baseSYS: string): TDate;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
begin
  result := 0;
  baseSYS := trim(UpperCase(baseSYS));

  if not (isInList(FormDSSFeed.basesyslist, baseSYS)) then
  begin
    savelogfile('dss_upd', 'GetLastVCUpdate --> invalid basesys: [' + baseSYS + ']');
    exit;
  end;

  try
    SetConn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'GetLastVCUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  theSQL := 'select max(vchpqbath) from vouchandqueue where ' +
    'basesys = "' + baseSYS + '" and vchpqbath > :date1';

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := now - 90;
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        result := trunc(MyQuery.Fields[0].AsDateTime);
      end;
    except on e: exception do
      begin
        result := 0;
        savelogfile('dss_upd', 'GetLastUpdate fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end;

procedure TUpdateDM.GetDCmailDates;
var MyDate, date1, date2, fiveYearsAgo, oneYearAgo: TDate;
  basesystem: string;
begin
  if trim(FormDSSFeed.cbsourceparam.text) = '' then
  begin
    showmessage('select a system');
    exit;
  end;

  basesystem := trim(uppercase(FormDSSFeed.cbsourceparam.text));

  if (basesystem = 'HOHO') then
  begin
    FormDSSFeed.memo2.Lines.Add('g_dcemail update for system [' + basesystem + '] not supported.');
    exit;
  end;

  MyDate := 0;
  date1 := 0;
  date2 := 0;
  MyDate := GetLastDCEMLUpdate(basesystem); // sets target connection active if not
                                                 // also validates baseSystem
  if MyDate = 0 then
  begin
    FormDSSFeed.memo2.lines.add('AutoUpdate_dcemail: No date returned from GetLastDCEMLUpdate(' + basesystem + ') *** aborted ***');
    exit;
  end
  else
  begin
    date1 := Mydate + 1;
    // update thru this date - i.e. latest batch completed by VP Department
    date2 := GetLastVQUpdate(basesystem);

    if (date2 = 0) then
    begin
      FormDSSFeed.memo2.lines.add('AutoUpdate_dcmail: failed to get "update thru" date for system [' + basesystem + ']');
      exit;
    end;

    if (trunc(date2) < trunc(date1)) then
    begin
      FormDSSFeed.memo2.lines.add('AutoUpdate_dcmail: No need to update dcemail for [' + basesystem + '] last updated: [' + DateTOStr(Date1) + ']');
      exit;
    end;
  end; // MyDate <> 0

  try
    FormDSSFeed.memo2.Lines.add('dcemail dates: ' + baseSystem + ' FROM: ' + datetostr(date1) + ' to ' + datetostr(date2));
    FormDSSFeed.memo2.Lines.add('opening query q_dc_email..');
  except;
  end;

  // not sure if the date time pickers will be used in the test zone, but set them anyway
  // date1 and 2 are used to grab the dataset to be updated TO g_dcemail
  // fiveyearsago and oneyearago are used to Process that data within g_dcemail
  FormDSSFeed.dtpStart.Date := date1;
  FormDSSFeed.dtpEnd.date := date2;

  // load current dates extracted from g_dcemail_rules table to be used for processing later
  FormDSSFeed.BzDateRec := FormDSSFeed.GetBizDates(connTarget);
  fiveYearsAgo := FormDSSFeed.BzDateRec.totalBizDaysBackDt;
  oneYearAgo := FormDSSFeed.BzDateRec.churnedDaysBackDt;
  FormDSSFeed.memo2.Lines.add('1 years ago: ' + DateToStr(OneYearAgo));
  FormDSSFeed.memo2.Lines.add('5 years ago: ' + DateToStr(fiveYearsAgo));
end;

function TUpdateDM.SetLatestDCEMLUpdate(InbaseSYS: string; InDate: TDate): boolean;
var theSQL: string; // return Zero if fail
  MyQuery: TIfxQuery;
  i: Integer;
begin
  result := false;
  InBaseSYS := trim(UpperCase(InBaseSYS));

  if not (isInList(FormDSSFeed.basesyslist, InBaseSYS)) then
  begin
    savelogfile('dss_upd', 'SetLatestDCEMLUpdate --> invalid basesys: [' + InBaseSYS + ']');
    exit;
  end;

  try
    SetConn(connTarget);
  except on e: exception do
    begin
      savelogfile('dss_upd', 'SetLatestDCEMLUpdate --> connect failure --> ' + E.Message);
      exit;
    end;
  end;

  i := 0;
  theSQL := 'update last_update set lu_last_update = :date1, ' +
    'lu_last_update_time = :date1 where lu_basesys = :basesys and lu_appname = "dc_mail"';

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.sql.Add(theSQL);
      MyQuery.ParamByName('date1').AsDate := trunc(InDate);
      MyQuery.ParamByName('basesys').AsString := InBaseSys;

      i := MyQuery.execSQL;

      if i = 1 then
      begin
        result := true;
        FormDSSFeed.memo2.Lines.add('last_update [dc_email] updated to ' + datetostr(indate) + ' for ' + InBaseSys);
      end;
    except on e: exception do
      begin
        result := false;
        savelogfile('dss_upd', 'SetLastDCEMLUpdate err --> sys: ' + InBaseSys + ' --> ' + E.Message);
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end; 

function TUpdateDM.FindAndUpdateClient(basesys: string; voucher: Integer): boolean;
var theSQL, temp: string;
  MyQuery: TIfxQuery;
  i: integer;
begin
  result := false;
  basesys := trim(uppercase(basesys));

  try
    theSql := 'select * From g_client where gc_system = "' + basesys + '" and gc_voucher = ' +
      IntToStr(voucher);
  except;
  end;

  try
    try
      MyQuery := GetIDACQuery(connTarget);
      MyQuery.RequestLive := true;
      MyQuery.sql.Add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then exit
      else
      begin
        // check and handle IF status != "Y" ?
        MyQuery.Edit;

        with QClientSource do
        begin
          for i := 0 to fieldcount - 1 do
          begin
            temp := '';
            temp := trim(LowerCase(fields[i].fieldname));

            if (temp = 'gc_key') or (temp = 'gc_uid') then continue
            else if (temp = 'gc_phone') or (temp = 'gc_fax') or (temp = 'gc_cell') then
            begin // STRIP ALL PHONE NUMBERS OF NON NUMERIC CHARACTERS
              MyQuery.fieldbyname(fields[i].fieldname).AsString :=
                StripIt(trim(UpperCase(fields[i].AsString)));
            end
            else
            begin
              try
                case fields[i].DataType of
                  ftString:
                    begin // format all strings:
                      MyQuery.fieldbyname(fields[i].fieldname).AsString :=
                        trim(UpperCase(fields[i].AsString));
                    end
                else
                  begin
                    MyQuery.fieldbyname(fields[i].fieldname).value := fields[i].value;
                  end;
                end; // case
              except on e: exception do
                begin
                  raise(exception.Create('FindAndUpdateClient FOR LOOP:  ' + e.message));
                  savelogfile('dss_upd', 'FindAndUpdateClient FOR LOOP:  ' + e.message);
                end;
              end;
            end; // else
          end;
        end; // with do

        MyQuery.Post;
        result := true;
      end;
    except on e: exception do
      begin
        result := false;
        savelogfile('dss_upd', 'FindAndUpdateClient --> sys: ' +
          basesys + ' voucher: ' + IntTOStr(voucher) + ' --> ' + E.Message);
        raise(exception.Create('FindAndUpdateClient --> sys: ' +
          basesys + ' voucher: ' + IntTOStr(voucher) + ' --> ' + E.Message));
      end;
    end;
  finally
    FreeAndNil(MyQuery);
  end;
end;

procedure TUpdateDM.CopyOneClient;
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean; // manually run client update
begin // assume source is open and has data
  FormDSSFeed.bKill := false;

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add('target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;

  if QClientSource.IsEmpty then
    savelogfile('dss_upd', 'no data to update for client')
  else
  begin
     QClientSource.last;
    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;
    inc(myCount);
    sbasesys := '';
    svcnumb := '';
    svchrel := '';
    ivchrel := 0;
    errstr := '';
    sbasesys := QclientSource.FieldByName('gc_system').AsString;
    svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
    svchrel := QclientSource.FieldByName('gc_release').AsString;
    ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
    ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

    if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
    begin
      dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
      try
        jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);
      except;
      end;
      inc(dupeCOunt);
    end
    else
    begin
      dupeStr := '';
    end;

    holdVoucher := ivcnumb;
    holdRelease := ivchrel;

    try
      bFound := FindAndUpdateClient(sbasesys, ivcnumb);
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add(e.Message);
        savelogfile('dss_upd', sbasesys +
          ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
        bFound := false;
      end;
    end;

    // debug:
    if bFound then
      FormDSSFeed.memo1.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
    else
    begin
      errstr := insert_g_client();
      FormDSSFeed.memo1.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
    end;

    if errstr > '' then
    begin
      FormDSSFeed.memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
    end;
    errstr := '';
  end;
end;

procedure TUpdateDM.ClientDateCopy;
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean; //  manually run client update
begin //  assume source is open and has data
  FormDSSFeed.bKill := false;

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add(e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo2.lines.add('target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;


  if (not qClientUpdateDriver.Active) or (qClientUpdateDriver.IsEmpty) then
  begin

    if (not qClientUpdateDriver.Active) then
      FormDSSFeed.memo2.lines.add('ClientDateCopy qClientUpdateDriver not active.')
    else
      FormDSSFeed.memo2.lines.add('ClientDateCopy qClientUpdateDriver empty.') ;

     exit;
  end;

  if (not QclientSource.Active) Then
  begin
     FormDSSFeed.memo2.lines.add('ClientDateCopy QclientSource not active.') ;
     exit;
  end;

  if qClientUpdateDriver.IsEmpty then
    savelogfile('dss_upd', 'no data to update for client')
  else
  begin
    qClientUpdateDriver.First;

    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;

    while not (qClientUpdateDriver.Eof) do
    begin
      if FormDSSFeed.bKill then Break;
      inc(myCount);

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      QclientSource.Last; // take the last record since they are all the same except batch date
      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
      svchrel := QclientSource.FieldByName('gc_release').AsString;
      ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
      ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

      if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
      begin
        dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
        try
          jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);
        except;
        end;
        inc(dupeCOunt);
      end
      else
      begin
        dupeStr := '';
      end;

      holdVoucher := ivcnumb;
      holdRelease := ivchrel;

      // here bro 7/21/2011

      try
        bFound := FindAndUpdateClient(sbasesys, ivcnumb);
      except on e: exception do
        begin
          FormDSSFeed.memo1.Lines.add(e.Message);
          savelogfile('dss_upd', sbasesys +
            ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
          bFound := false;
        end;
      end;

      // debug:
      if bFound then
        FormDSSFeed.memo1.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
      else
      begin
        errstr := insert_g_client();

        FormDSSFeed.memo1.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
      end;

      if errstr > '' then
      begin
        FormDSSFeed.memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
        savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      end;
      errstr := '';

      //QClientSource.Next;
      qClientUpdateDriver.Next;
    end;
  end;

  CheckClientSums(FormDSSFeed.dtpStart.date, FormDSSFeed.dtpEnd.date, FormDSSFeed.cbSourceParam.Text);
  FormDSSFeed.memo1.Lines.add('Dupes:  ' + IntToStr(DupeCount));

  if dupeCOunt > 0 then
  begin
    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', 'Dupes:  ' + IntToStr(DupeCount));
      FormDSSFeed.LogList.Add('Dupes:  ' + IntToStr(DupeCount));
    except;
    end;
  end;

  if FormDSSFeed.bKill then FormDSSFeed.memo1.Lines.add('done and Killed')
  else FormDSSFeed.memo1.Lines.add('done');
end;



procedure TupdateDM.ClientCopyForDates(BaseSys: string; startDT, EndDT: TDate; Ctype: string);
var sbasesys, svcnumb, svchrel, errstr, dupestr: string;
  ivchrel, ivcnumb, myCount, holdVoucher, HoldRelease, dupeCOunt: integer;
  bFound: boolean;
begin // assume source is open and has data
  FormDSSFeed.bKill := false;
  CType := trim(UpperCase(CType));

  if not (connTarget.Connected) then
  begin
    try
      connTarget.Connected := true;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('ClientCopyForDates connTarget fail: ' + e.Message);
        exit;
      end;
    end;
  end;

  if not (tbl_target_client.Active) then
  begin
    try
      tbl_target_client.Open;
    except on e: exception do
      begin
        FormDSSFeed.memo1.Lines.add('ClientCopyForDates target client open err --> ' + e.Message);
        exit;
      end;
    end;
  end;

  // new 7/21/2011
  if (not qClientUpdateDriver.Active) or (qClientUpdateDriver.IsEmpty) then
  begin
    if (not qClientUpdateDriver.Active) then
    begin
     FormDSSFeed.memo2.lines.add('ClientDateCopy qClientUpdateDriver not active.');
     savelogfile('dss_upd', 'ClientDateCopy qClientUpdateDriver not active.');
    end
    else
    begin
      FormDSSFeed.memo2.lines.add('ClientDateCopy qClientUpdateDriver empty.');
      savelogfile('dss_upd', 'ClientDateCopy qClientUpdateDriver empty.');
    end;

     exit;
  end;

  if (not QclientSource.Active) Then
  begin
     FormDSSFeed.memo2.lines.add('ClientDateCopy QclientSource not active.');
     savelogfile('dss_upd', 'ClientDateCopy QclientSource not active.');
     exit;
  end;

  // if QClientSource.IsEmpty then
  if qClientUpdateDriver.IsEmpty then
  begin
    savelogfile('dss_upd', 'ClientCopyForDates no data to update for client');
    FormDSSFeed.memo1.Lines.add('ClientCopyForDates no data to update for client');
  end
  else
  begin
   // QClientSource.First;  // 07/21/2011
   qClientUpdateDriver.First;


    MyCount := 0;
    HoldVoucher := 0;
    HoldRelease := 0;
    dupestr := '';
    DupeCount := 0;

    // while not (QClientSource.Eof) do // 7/21/2011
    while not (qClientUpdateDriver.Eof) do
    begin
      if FormDSSFeed.bKill then Break;
      inc(myCount);

      sbasesys := '';
      svcnumb := '';
      svchrel := '';
      ivchrel := 0;
      errstr := '';

      QclientSource.Last; // take the last record since they are all the same except batch date
      sbasesys := QclientSource.FieldByName('gc_system').AsString;
      svcnumb := QclientSource.FieldByName('gc_voucher').AsString;
      svchrel := QclientSource.FieldByName('gc_release').AsString;
      ivcnumb := QclientSource.FieldByName('gc_voucher').AsInteger;
      ivchrel := QclientSource.FieldByName('gc_release').AsInteger;

      if (holdVoucher = ivcnumb) and (holdRelease = ivchrel) then
      begin
        dupeStr := 'DUPE! ' + svcnumb + '-' + svchrel;
        try
          jSaveFile('c:\clog\', 'dss_dupes', '.txt', sbasesys + '|' + svcnumb + '-' + svchrel);
        except;
        end;
        inc(dupeCOunt);
      end
      else
      begin
        dupeStr := '';
      end;

      holdVoucher := ivcnumb;
      holdRelease := ivchrel;

      try
        bFound := FindAndUpdateClient(sbasesys, ivcnumb);
      except on e: exception do
        begin
          FormDSSFeed.memo1.Lines.add(e.Message);
          savelogfile('dss_upd', sbasesys +
            ' voucher: ' + svcnumb + '-' + svchrel + ' FindAndUpdateClient--> ' + e.Message);
          bFound := false;
        end;
      end;

      // debug:
      if bFound then
        FormDSSFeed.memo2.Lines.add(dupestr + 'OK FindAndUpdate - UPDATED OK: ' + sBasesys + ' ' + svcnumb + '-' + svchrel)
      else
      begin
        errstr := insert_g_client();
        FormDSSFeed.memo2.Lines.add(dupestr + 'INSERT: ' + sBasesys + ' ' + svcnumb + '-' + svchrel);
      end;

      if errstr > '' then
      begin
        FormDSSFeed.memo1.Lines.add(sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
        savelogfile('dss_upd', sbasesys + ' voucher: ' + svcnumb + '-' + svchrel + ' Insert Client() --> ' + errstr);
      end;
      errstr := '';
      // QClientSource.Next; // 07/21/2011
      qClientUpdateDriver.Next;
    end;
  end;

  if CType = 'P' then CheckClientSums(startDT, EndDT, BaseSys)
  else if CType = 'Q' then ;

  if dupeCount > 0 then
  begin
    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', 'Dupes:  ' + IntToStr(DupeCount));
      FormDSSFeed.LogList.Add('client Dupes:  ' + IntToStr(DupeCount));
      FormDSSFeed.memo1.Lines.Add('Client Dupes:  ' + IntToStr(DupeCount));
    except;
    end;
  end;

  if FormDSSFeed.bKill then FormDSSFeed.memo2.Lines.add('done and Killed');
end;

function TUpdateDM.OpenSourceClientForSync(InBaseSys: string; date1, date2: TDate): boolean;
var sourceDriverSQL: String;
begin
  result := false;
  InbaseSys := trim(Uppercase(InBaseSys));
  if connSource.Connected then connSource.Close;

  try
    Set_IDAC_Connection(connSource, InBaseSys);
    connSource.Open;
    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDSSFeed.caption := '';
      exit;
    end;
  end;

  try
   sourceDriverSQL := 'Select unique vcnumb From vouchandqueue where basesys = "'+InBasesys + '" and ' +
      ' vchpqbath between :date1 and :date2 ' +
     ' and vcnumb not in ( '+
       ' select vcnumb from vouchandqueue, g_client WHERE  '+
       ' basesys in("'+ InBaseSys +'") and  '+
       ' (  vchpqbath BETWEEN :date1 and :date2 ) '+
      '  and gc_system = basesys and gc_voucher = vcnumb  '+
      '  ) ';

   // gets a list of vouchers in vouchAndQueue that need client records ..
   qClientUpdateDriver.Close;
   qClientUpdateDriver.sql.Clear;
   qClientUpdateDriver.SQL.add(sourceDriverSQL);

   FormDSSFeed.memo1.Lines.add('sourceDriverSQL:');
   FormDSSFeed.memo1.Lines.add(sourceDriverSQL);

   try
    FormDSSFeed.memo1.Lines.add('date1: '+datetostr(trunc(date1)));
    FormDSSFeed.memo1.Lines.add('date2: '+datetostr(trunc(date2)));
   except;
   end;

   qClientUpdateDriver.parambyname('date1').AsDate := trunc(date1);
   qClientUpdateDriver.parambyname('date2').AsDate := trunc(date2);
   qClientUpdateDriver.open;

   QClientSource.SQL.Clear; // detail for qClientUpdateDriver - datasource property points to qClientUpdateDriver.datasource

    QClientSource.SQL.Text := SQL_GET_CLIENTS;

    qClientSource.SQL.Text := StringReplace(QClientSource.SQL.Text, '[[basesys]]',
      InBaseSys, [rfReplaceAll, rfIgnoreCase]);

    FormDSSFeed.memo1.Lines := QClientSource.SQL;

    FormDSSFeed.caption := 'connecting to ' + GetCurrentDB(connSource);
    QClientSource.Open;
    FormDSSFeed.caption := 'connected to ' + GetCurrentDB(connSource);
  except on e: exception do
    begin
      raise(exception.Create(e.Message));
      FormDSSFeed.caption := '';
      exit;
    end;
  end;
  result := true;
end;   

function TUpdateDM.insert_g_client(): string;
var temp: string;
  i: Integer;
begin
  result := '';
  try
    tbl_target_client.Insert;
  except on E: EXCEPTION do
    begin
      stradd(result, '1 client set insert --> ' + e.message);
    end;
  end;

  with QClientSource do
  begin
    for i := 0 to fieldcount - 1 do
    begin
      temp := '';
      temp := trim(LowerCase(fields[i].fieldname));
      if (temp = 'gc_phone') or (temp = 'gc_fax') or (temp = 'gc_cell') then
      begin // STRIP ALL PHONE NUMBERS OF NON NUMERIC CHARACTERS
        tbl_target_client.fieldbyname(fields[i].fieldname).AsString :=
          StripIt(trim(UpperCase(fields[i].AsString)));
      end
      else
      begin
        try
          case fields[i].DataType of
            ftString:
              begin // format all strings:
                tbl_target_client.fieldbyname(fields[i].fieldname).AsString := trim(UpperCase(fields[i].AsString));
              end
          else
            begin
              tbl_target_client.fieldbyname(fields[i].fieldname).value := fields[i].value;
            end;
          end; // case
        except on e: exception do
            stradd(result, ' client FOR LOOP:  ' + e.message);
        end;
      end; // else
    end;
  end; // with do

  try
    tbl_target_client.Post;
  except on e: exception do
      stradd(result, ' client_post --> ' + e.message);
  end; //try
  application.ProcessMessages;
end;

function TUpdateDM.StripIt(InStr: string): string;
var i, x: integer;
  temp: string;
begin
  if (InStr = '') then
  begin
    Result := '';
    exit;
  end
  else
  begin
    temp := '';
    x := length(InStr);

    for i := 1 to x do
    begin
      case InStr[i] of
        '0'..'9': temp := temp + InStr[i];
      else
        continue;
      end;
    end;
  end;
  Result := temp;
end;  

procedure TUpdateDM.OpenClientTarget;
begin
  if (tbl_target_client.Active) then tbl_target_client.Close;
  if connTarget.Connected then connTarget.Close;

  try
    connTarget.Connected := true;
  except on e: exception do
    begin
      FormDSSFeed.memo2.lines.add(e.Message);
      exit;
    end;
  end;

  try
    tbl_target_client.Open;
  except on e: exception do
    begin
      FormDSSFeed.memo2.lines.add(e.Message);
      exit;
    end;
  end;
end;

procedure TUpdateDM.CheckClientSums(dStart, dEnd: TDate; baseSys: string);
var targetq: TIfxQuery;
  sourceSUM, TargetSUM: real;
  sourceCOUNT, TargetCOUNT: integer;
  temp, theSQL: string;
begin // assuming connected
  sourceSUM := 0.0;
  sourceCOUNT := 0;
  TargetSUM := 0.0;
  TargetCOUNT := 0;

  basesys := trim(Uppercase(basesys));


  theSQL :=
  'Select  count(*) From vouchandqueue where basesys = :basesys and  '+
  ' vchpqbath between :date1 and :date2  '+
  ' and vcnumb not in (   '+
  ' select vcnumb from vouchandqueue, g_client WHERE   '+
  'basesys in(:basesys) and  '+
  '(  vchpqbath BETWEEN :date1 and :date2 )  '+
  ' and gc_system = basesys and gc_voucher = vcnumb   '+
  ') ';

  try
    try
      targetq := GetIDACQuery(connTarget);
      //targetq.sql.Add(SQL_CheckSumClientTarget);
      targetq.sql.Add(theSQL);


      targetq.ParamByName('date1').AsDate := trunc(dstart);
      targetq.ParamByName('date2').AsDate := trunc(dend);
      targetq.ParamByName('basesys').AsString := basesys;
      targetq.Active := true;

      targetsum := -1;
      targetSum := targetq.Fields[0].AsFloat;

    except on e: exception do
      begin
        targetsum := -1;
        FormDSSFeed.memo1.Lines.add('checksum CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        savelogfile('dss_upd', 'checksum CLIENT target fail --> sys: ' + basesys + ' --> ' + E.Message);
        exit;
      end;
    end;

  finally
    FreeAndNil(targetq);
  end;

  temp := '';

  if targetsum = 0 then
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' Client OK! : ' + /// 'source: ' + // FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' missing records: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    stradd(temp, ':: targetCount: ' + IntToStr(TargetCount));

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDSSFeed.LogList.Add(temp);
    except;
    end;
  end
  else
  begin
    temp := basesys + ' ' + datetostr(dstart) + ' to ' + datetostr(dend) + ' Client Fail! : ' + //'source: ' + FOrmatFLoat('###,###,###,##0.00', sourcesum) +
      ' missing records: ' + FOrmatFLoat('###,###,###,##0.00', targetsum);

    stradd(temp, ':: targetCount: ' + IntToStr(TargetCount));

    try
      jSaveFile('c:\clog\', 'dss_record', '.txt', temp);
      FormDSSFeed.LogList.Add(temp);
    except;
    end;
  end;
  FormDSSFeed.memo2.Lines.add(temp);
end;

procedure TUpdateDM.EMAIL(sSubject, sBody: string);
var sFILENAME: string;
  MyList: TStringList;
  i: Integer;
begin
  Msg.Clear;
  MyList := TStringList.Create;
  try
    MyList.Text := sBody;
    sFILENAME := GetLogFileName('dss_upd');

    with Msg do
    begin
      Body.Add('DSS Update Log:');
      Body.add('');

      if (fileexists(sFILENAME)) then
        Body.Add('Error Log file attached.');

      Body.add('--------------->');
      Body.add('');

      for I := 0 to MyList.Count - 1 do
        Body.Add(MyList[i]);

      From.Text := 'chester@autoeurope.com';
      Recipients.EMailAddresses := FormDSSFeed.emlTO;


      Subject := 'DSS Update: ' + sSubject;
      Priority := TIdMessagePriority(mpNormal);
    end;

    if (fileexists(sFILENAME)) then
    begin
      try
        TIdAttachmentFile.Create(Msg.MessageParts, sFILENAME);
      except on e: exception do
        begin
          savelogfile('dss_upd', 'failed to attach file. Failed e-mail. ' + e.message);
          exit;
        end;
      end;
    end;

    try
      SMTP.Connect;
    except on e: exception do
      begin
        savelogfile('dss_upd', 'Failed to send e-mail. -->' + e.message);
        exit;
      end;
    end;

    try
      try
        SMTP.Send(Msg);
      except on e: exception do
        begin
          savelogfile('dss_upd', e.message + ': Failed to send e-mail.');
        end;
      end;
    finally
      SMTP.Disconnect;
    end;
  finally
    if assigned(MyList) then FreeAndNil(MyList);
  end;
end; 

procedure TUpdateDM.tbl_target_clientBeforeOpen(DataSet: TDataSet);
begin
  FormDSSFeed.dbgrid2.DataSource := dsTargetClient;
end;

procedure TUpdateDM.Tbl_Target_SummaryBeforeOpen(DataSet: TDataSet);
begin
  FormDSSFeed.dbgrid2.DataSource := dsTarget_summary;
end;

procedure TUpdateDM.Tbl_Target_VQBeforeOpen(DataSet: TDataSet);
begin
  FormDSSFeed.dbgrid2.DataSource := dsTargetVQ; //dsTarget_Q_Vouchers
end;  

procedure TUpdateDM.QSourceBeforeOpen(DataSet: TDataSet);
begin
  FormDSSFeed.dbgrid1.DataSource := dsSource;
end;

end.
