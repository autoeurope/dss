object UpdateDM: TUpdateDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 762
  Width = 1282
  object connGlobal: TIfxConnection
    DesignConnection = True
    IsolationLevel = ilDirtyRead
    Left = 32
    Top = 30
  end
  object dsmtcache: TDataSource
    Left = 17
    Top = 706
  end
  object sd1: TSIPPDesc
    ValidDate = 39427.000000000000000000
    Language = 'ENG'
    Product = 'car'
    Connection = connGlobal
    Execution_Milliseconds = 0
    Left = 351
    Top = 30
  end
  object connSource: TIfxConnection
    DesignConnection = True
    Left = 108
    Top = 30
  end
  object memIATAWhiteList: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.51'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 739
    Top = 30
  end
  object dsIWHiteList: TDataSource
    DataSet = memIATAWhiteList
    Left = 86
    Top = 706
  end
  object qcsCsFileHIstorySource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    Left = 45
    Top = 383
  end
  object dshistsource: TDataSource
    DataSet = qcsCsFileHIstorySource
    Left = 172
    Top = 706
  end
  object dscsHistTarget: TDataSource
    DataSet = tblcsfilehistoryTarget
    Left = 361
    Top = 706
  end
  object tblcsfilehistoryTarget: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    TableName = 'csfilehistory'
    Left = 61
    Top = 191
  end
  object connTarget: TIfxConnection
    DesignConnection = True
    Params.Strings = (
      '')
    Left = 177
    Top = 30
  end
  object tblcsfilesTarget: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    TableName = 'csfiles'
    Left = 192
    Top = 191
  end
  object tblcscontactsTarget: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    TableName = 'cscontact'
    Left = 314
    Top = 191
  end
  object qcsContactsSource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    DataSource = dscsfilesSource
    Left = 174
    Top = 383
  end
  object dscsfilesSource: TDataSource
    DataSet = QcsfilesSource
    Left = 266
    Top = 706
  end
  object QcscommentsSource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    DataSource = dscsfilesSource
    Left = 313
    Top = 383
  end
  object QcsfilesSource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    SQL.Strings = (
      'Select * From csfiles where '
      'cs_filestat = '#39'C'#39' and cs_date_c between '
      ':date1 and :date2')
    Left = 419
    Top = 383
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'date1'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'date2'
        ParamType = ptInput
      end>
  end
  object tblcscmntsTarget: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    TableName = 'cscomments'
    Left = 438
    Top = 191
  end
  object QSummarySource: TIfxQuery
    BufferChunks = 128
    Connection = connTarget
    SQL.Strings = (
      'select  * from vouchandqueue  a'
      'where '
      ''
      'a.vchpqbath >= :InDate and'
      ''
      'not exists('
      'Select b.vs_basesys,b.vs_voucher,b.vs_release From'
      'g_voucher_summary b'
      'where'
      'b.vs_basesys = a.basesys and'
      'b.vs_voucher = a.vcnumb and'
      'b.vs_release = a.vchrel'
      ')')
    Left = 519
    Top = 383
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'InDate'
        ParamType = ptInput
      end>
  end
  object dsTarget_summary: TDataSource
    DataSet = Tbl_Target_Summary
    Left = 606
    Top = 706
  end
  object Tbl_Target_Summary: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    BeforeOpen = Tbl_Target_SummaryBeforeOpen
    TableName = 'g_voucher_summary'
    Left = 569
    Top = 191
  end
  object dsSUmmarySOurce: TDataSource
    DataSet = QSummarySource
    Left = 721
    Top = 706
  end
  object dsc: TDSCalc
    DataSource = dsSUmmarySOurce
    Left = 552
    Top = 30
  end
  object dscEdit: TDSCalc
    DataSource = dsQVandC_FIXREC
    Left = 603
    Top = 30
  end
  object dsQVandC_FIXREC: TDataSource
    DataSet = QVandQ_FIXREC
    Left = 480
    Top = 706
  end
  object QSummaryFIXREC: TIfxQuery
    Connection = connTarget
    DataSource = dsQVandC_FIXREC
    RequestLive = True
    SQL.Strings = (
      'select * From g_voucher_summary where'
      'vs_basesys = :basesys'
      'and vs_voucher = :vcnumb'
      'and vs_release = :vchrel')
    Left = 711
    Top = 191
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vcnumb'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vchrel'
        ParamType = ptInput
      end>
  end
  object QVandQ_FIXREC: TIfxQuery
    Connection = connTarget
    DataSource = dsSource
    RequestLive = True
    SQL.Strings = (
      'select * from vouchandqueue'
      'where'
      'basesys = :basesys'
      'and vcnumb =:vcnumb'
      'and vchrel =:vchrel')
    Left = 623
    Top = 383
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basesys'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vcnumb'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'vchrel'
        ParamType = ptInput
      end>
  end
  object dsSource: TDataSource
    DataSet = QSource
    Left = 806
    Top = 706
  end
  object QSource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    BeforeOpen = QSourceBeforeOpen
    Left = 704
    Top = 383
  end
  object dsSummaryFIXREC: TDataSource
    DataSet = QSummaryFIXREC
    Left = 891
    Top = 706
  end
  object Tbl_Target_VQ: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    BeforeOpen = Tbl_Target_VQBeforeOpen
    IndexName = 'sysvchrel'
    TableName = 'vouchandqueue'
    Left = 823
    Top = 191
  end
  object dsTargetVQ: TDataSource
    DataSet = Tbl_Target_VQ
    Left = 991
    Top = 706
  end
  object qsourceVENDOR: TIfxQuery
    BufferChunks = 256
    Connection = connSource
    SQL.Strings = (
      'select "NEWZ" gv_basesys,'
      '    trim(upper(apvenmast.vendor)) gv_iata,'
      '    trim(upper(ven_class)) gv_class,'
      '    trim(upper(vendor_vname)) gv_name,'
      '    trim(upper(legal_name)) gv_legal_name,'
      '    trim(upper(vendor_contct)) gv_contact_name,'
      '    trim(upper(addr1)) gv_addr1,'
      '    trim(upper(addr2)) gv_addr2,'
      '    trim(upper(addr3)) gv_addr3,'
      '    trim(upper(addr4)) gv_addr4,'
      '    trim(upper(city_addr5)) gv_city,'
      '    trim(upper(county)) gv_prov, -- county is used for state'
      '    trim(upper(postal_code)) gv_postal,'
      '    trim(upper(country)) gv_country,'
      '    trim(upper(phone_prefix)) gv_phone_prefix,'
      '    trim(upper(phone_num)) gv_phone_num,'
      '    trim(upper(fax_prefix)) gv_fax_prefix,'
      '    trim(upper(fax_num)) gv_fax_num,'
      '    trim(upper(agemail2)) gv_email,'
      '    trim(upper(agconsortium)) gv_consortium'
      '    from apvenmast, agfile, outer(apvenaddr) '
      '    where apvenmast.vendor_group = "DRIV" and'
      '    inumb = apvenmast.vendor'
      '    and apvenaddr.vendor = apvenmast.vendor '
      '    and apvenaddr.vendor_group = apvenmast.vendor_group')
    Left = 782
    Top = 383
  end
  object dsSourceVENDOR: TDataSource
    DataSet = qsourceVENDOR
    Left = 937
    Top = 594
  end
  object tbltargetVENDOR: TIfxTable
    BufferChunks = 256
    Connection = connTarget
    TableName = 'g_vendor'
    Left = 918
    Top = 191
  end
  object dsTargetVENDOR: TDataSource
    DataSet = tbltargetVENDOR
    Left = 828
    Top = 594
  end
  object ds_dc_email: TDataSource
    DataSet = q_dc_email
    Left = 727
    Top = 594
  end
  object q_dc_email: TIfxQuery
    BufferChunks = 128
    Connection = connTarget
    Left = 864
    Top = 383
  end
  object memdcmailUpdt: TkbmMemTable
    Active = True
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'eml_system'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'eml_email'
        DataType = ftString
        Size = 250
      end
      item
        Name = 'eml_pu_ctry'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'eml_booking_count'
        DataType = ftInteger
      end
      item
        Name = 'eml_profit'
        DataType = ftFloat
      end
      item
        Name = 'eml_cust_class'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'eml_churn_class'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'eml_is_multi_ctry'
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.51'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 849
    Top = 30
    object memdcmailUpdteml_system: TStringField
      DisplayWidth = 11
      FieldName = 'eml_system'
      Size = 4
    end
    object memdcmailUpdteml_email: TStringField
      DisplayWidth = 34
      FieldName = 'eml_email'
      Size = 250
    end
    object memdcmailUpdteml_pu_ctry: TStringField
      DisplayWidth = 12
      FieldName = 'eml_pu_ctry'
      Size = 2
    end
    object memdcmailUpdteml_booking_count: TIntegerField
      DisplayWidth = 19
      FieldName = 'eml_booking_count'
    end
    object memdcmailUpdteml_profit: TFloatField
      DisplayWidth = 12
      FieldName = 'eml_profit'
    end
    object memdcmailUpdteml_cust_class: TStringField
      DisplayWidth = 14
      FieldName = 'eml_cust_class'
      Size = 10
    end
    object memdcmailUpdteml_churn_class: TStringField
      DisplayWidth = 16
      FieldName = 'eml_churn_class'
      Size = 10
    end
    object memdcmailUpdteml_is_multi_ctry: TStringField
      DisplayWidth = 17
      FieldName = 'eml_is_multi_ctry'
      Size = 1
    end
  end
  object ds_memdcmailUpdt: TDataSource
    DataSet = memdcmailUpdt
    Left = 624
    Top = 594
  end
  object QgetDCMAILUpdateInfo: TIfxQuery
    Connection = connTarget
    Left = 967
    Top = 383
  end
  object QclientSource: TIfxQuery
    BufferChunks = 128
    Connection = connSource
    DataSource = dsClientUpdateDriver
    Left = 1085
    Top = 383
  end
  object qClientUpdateDriver: TIfxQuery
    BufferChunks = 128
    Connection = connTarget
    UpdateMode = upWhereKeyOnly
    Left = 1192
    Top = 383
  end
  object dsTargetClient: TDataSource
    DataSet = tbl_target_client
    Left = 512
    Top = 594
  end
  object tbl_target_client: TIfxTable
    BufferChunks = 128
    Connection = connTarget
    BeforeOpen = tbl_target_clientBeforeOpen
    TableName = 'g_client'
    Left = 1025
    Top = 191
  end
  object dsClientUpdateDriver: TDataSource
    DataSet = qClientUpdateDriver
    Left = 407
    Top = 594
  end
  object dscscommentsSource: TDataSource
    DataSet = QcscommentsSource
    Left = 50
    Top = 594
  end
  object dscsContactsSource: TDataSource
    DataSet = qcsContactsSource
    Left = 176
    Top = 594
  end
  object dsClientSource: TDataSource
    DataSet = QclientSource
    Left = 283
    Top = 594
  end
  object Msg: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 1202
    Top = 30
  end
  object SMTP: TIdSMTP
    UseEhlo = False
    Host = 'mx2.aemaine.com'
    SASLMechanisms = <>
    Left = 1153
    Top = 30
  end
end
