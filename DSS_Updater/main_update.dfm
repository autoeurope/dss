object FormDssFeed: TFormDssFeed
  Left = 0
  Top = 0
  Caption = 'DSS Feed'
  ClientHeight = 834
  ClientWidth = 1333
  Color = clMedGray
  Constraints.MinWidth = 785
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    1333
    834)
  PixelsPerInch = 120
  TextHeight = 17
  object lblSource: TLabel
    Left = 10
    Top = 18
    Width = 46
    Height = 17
    Caption = 'source:'
  end
  object lblBatchStart: TLabel
    Left = 145
    Top = 18
    Width = 74
    Height = 17
    Caption = 'Batch Start:'
  end
  object lblBatchEnd: TLabel
    Left = 294
    Top = 18
    Width = 68
    Height = 17
    Caption = 'Batch End:'
  end
  object lblQClientUpdateDriver: TLabel
    Left = 10
    Top = 647
    Width = 122
    Height = 17
    Caption = 'qClientUpdateDriver'
  end
  object lblQClientSource: TLabel
    Left = 479
    Top = 647
    Width = 84
    Height = 17
    Caption = 'QclientSource'
  end
  object lblTblTargetClient: TLabel
    Left = 928
    Top = 650
    Width = 99
    Height = 17
    Caption = 'tbl_target_client'
  end
  object Memo1: TMemo
    Left = 971
    Top = 147
    Width = 351
    Height = 216
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object DBGrid1: TDBGrid
    Left = 10
    Top = 365
    Width = 461
    Height = 274
    DataSource = UpdateDM.dsSource
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dtpStart: TDateTimePicker
    Left = 145
    Top = 35
    Width = 139
    Height = 25
    Date = 39196.577854745370000000
    Time = 39196.577854745370000000
    TabOrder = 2
  end
  object DBGrid2: TDBGrid
    Left = 711
    Top = 511
    Width = 611
    Height = 131
    Anchors = [akLeft, akTop, akRight]
    DataSource = UpdateDM.dsTargetVQ
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dtpEnd: TDateTimePicker
    Left = 292
    Top = 35
    Width = 134
    Height = 25
    Date = 39196.577854745370000000
    Time = 39196.577854745370000000
    TabOrder = 4
  end
  object cbsourceparam: TComboBox
    Left = 10
    Top = 35
    Width = 127
    Height = 25
    DropDownCount = 15
    ItemHeight = 17
    TabOrder = 5
    Text = 'cbsourceparam'
  end
  object Memo2: TMemo
    Left = 9
    Top = 264
    Width = 959
    Height = 99
    Anchors = [akLeft, akTop, akRight]
    ScrollBars = ssVertical
    TabOrder = 6
  end
  object sbar: TStatusBar
    Left = 0
    Top = 815
    Width = 1333
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object DBGrid3: TDBGrid
    Left = 711
    Top = 365
    Width = 612
    Height = 138
    Anchors = [akLeft, akTop, akRight]
    DataSource = UpdateDM.dsmtcache
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid4: TDBGrid
    Left = 479
    Top = 405
    Width = 225
    Height = 234
    DataSource = UpdateDM.dsIWHiteList
    TabOrder = 9
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Edit1: TEdit
    Left = 539
    Top = 365
    Width = 73
    Height = 25
    TabOrder = 10
    Text = '72001273'
  end
  object btnFind: TButton
    Left = 620
    Top = 365
    Width = 39
    Height = 33
    Caption = 'find'
    TabOrder = 11
    OnClick = btnFindClick
  end
  object btnRefill: TButton
    Left = 662
    Top = 365
    Width = 39
    Height = 33
    Caption = 're-fill'
    TabOrder = 12
    OnClick = btnRefillClick
  end
  object Edit2: TEdit
    Left = 480
    Top = 365
    Width = 51
    Height = 25
    TabOrder = 13
    Text = 'USA'
  end
  object DBGrid5: TDBGrid
    Left = 479
    Top = 670
    Width = 442
    Height = 139
    Anchors = [akLeft, akTop, akBottom]
    DataSource = UpdateDM.dsClientSource
    TabOrder = 14
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid6: TDBGrid
    Left = 10
    Top = 670
    Width = 461
    Height = 139
    Anchors = [akLeft, akTop, akBottom]
    DataSource = UpdateDM.dsClientUpdateDriver
    TabOrder = 15
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid7: TDBGrid
    Left = 928
    Top = 670
    Width = 394
    Height = 139
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = UpdateDM.dsTargetClient
    TabOrder = 16
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object gbCS: TGroupBox
    Left = 365
    Top = 63
    Width = 160
    Height = 195
    Caption = 'C/S History'
    TabOrder = 17
    DesignSize = (
      160
      195)
    object rgCSTasks: TRadioGroup
      Left = 4
      Top = 46
      Width = 151
      Height = 142
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Last csH date'
        'CS O source/targ'
        'Open csH S/T'
        'Update csH for sys'
        'Update CS for sys'
        'Last CS date')
      TabOrder = 0
    end
    object btnCSHistoryTasks: TButton
      Left = 81
      Top = 22
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnCSHistoryTasksClick
    end
  end
  object gbIATAUpdates: TGroupBox
    Left = 527
    Top = 2
    Width = 192
    Height = 120
    Caption = 'IATA updates'
    TabOrder = 18
    DesignSize = (
      192
      120)
    object rgIATATasks: TRadioGroup
      Left = 7
      Top = 43
      Width = 178
      Height = 70
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'IATA Update 1 sys'
        'loop update IATAs'
        'update rc_operators')
      TabOrder = 0
    end
    object btnIATATasks: TButton
      Left = 110
      Top = 19
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnIATATasksClick
    end
  end
  object gbDCMail: TGroupBox
    Left = 721
    Top = 2
    Width = 247
    Height = 145
    Caption = 'dcMail updates'
    TabOrder = 19
    DesignSize = (
      247
      145)
    object rgdcMailTasks: TRadioGroup
      Left = 6
      Top = 39
      Width = 235
      Height = 98
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'DCMail Get Dates'
        'Get DCMail data'
        'Update DCMail data'
        'set DCMail update date'
        'Auto Update DCMail')
      TabOrder = 0
    end
    object btnDCMailTasks: TButton
      Left = 166
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnDCMailTasksClick
    end
  end
  object gbDevelopment: TGroupBox
    Left = 721
    Top = 147
    Width = 247
    Height = 111
    Caption = 'Development'
    TabOrder = 20
    DesignSize = (
      247
      111)
    object btnDevTasks: TButton
      Left = 165
      Top = 19
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 0
      OnClick = btnDevTasksClick
    end
    object rgDevTasks: TRadioGroup
      Left = 7
      Top = 16
      Width = 119
      Height = 87
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Test Mail'
        'Init SippDesc'
        'Kill')
      TabOrder = 1
    end
    object editTEST: TEdit
      Left = 144
      Top = 47
      Width = 96
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 2
      Text = '27230'
    end
    object EditTEST2: TEdit
      Left = 144
      Top = 71
      Width = 96
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 3
      Text = 'EURO'
    end
  end
  object gbSummary: TGroupBox
    Left = 526
    Top = 125
    Width = 192
    Height = 133
    Caption = 'Summary'
    TabOrder = 21
    DesignSize = (
      192
      133)
    object rgSummaryTasks: TRadioGroup
      Left = 7
      Top = 45
      Width = 178
      Height = 82
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Summary Open SandT'
        'Summary Update'
        'Loop Update Summary')
      TabOrder = 0
    end
    object btnSummaryTasks: TButton
      Left = 110
      Top = 24
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnSummaryTasksClick
    end
  end
  object gbClient: TGroupBox
    Left = 163
    Top = 63
    Width = 199
    Height = 195
    Caption = 'Client'
    TabOrder = 22
    DesignSize = (
      199
      195)
    object rgClientTasks: TRadioGroup
      Left = 7
      Top = 44
      Width = 184
      Height = 142
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Get last client update'
        'Open client target'
        'Open client source sync'
        'Client copy date'
        'Client check sum'
        'Copy one client')
      TabOrder = 0
    end
    object btnClientTasks: TButton
      Left = 112
      Top = 22
      Width = 79
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnClientTasksClick
    end
  end
  object gbVQ: TGroupBox
    Left = 10
    Top = 63
    Width = 147
    Height = 195
    Caption = 'VQ'
    TabOrder = 23
    DesignSize = (
      147
      195)
    object rgVQTasks: TRadioGroup
      Left = 7
      Top = 52
      Width = 133
      Height = 136
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Last VQ date'
        'VQ source open'
        'VQ target open'
        'VC copy date'
        'Check sum'
        'Copy one VQ')
      TabOrder = 0
    end
    object btnVQTasks: TButton
      Left = 65
      Top = 25
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnVQTasksClick
    end
  end
  object gbPQ: TGroupBox
    Left = 971
    Top = 2
    Width = 351
    Height = 139
    TabOrder = 24
    DesignSize = (
      351
      139)
    object Label2: TLabel
      Left = 6
      Top = 19
      Width = 333
      Height = 39
      AutoSize = False
      Caption = 
        'use this section to update fix/update vouchandqueue & g_voucher_' +
        'summary records:'
      WordWrap = True
    end
    object rgPQTasks: TRadioGroup
      Left = 6
      Top = 61
      Width = 335
      Height = 70
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Tasks'
      Items.Strings = (
        'Update record open source'
        'UR open target'
        'update selected record')
      TabOrder = 0
    end
    object btnPQTasks: TButton
      Left = 267
      Top = 39
      Width = 75
      Height = 25
      Caption = 'Do Task'
      TabOrder = 1
      OnClick = btnPQTasksClick
    end
  end
end
