unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, IfxConnection, IFXQuery, aecommonfunc_II;

type
  TForm1 = class(TForm)
    Memo2: TMemo;
    Button1: TButton;
    conntarget: TIfxConnection;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure UpdateOperatorList(sys, Oper: String);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
begin
   UpdateOperatorList('AUS', 'BUTTCRACK');
end;

procedure TForm1.UpdateOperatorList(sys, Oper: String);
var MYQuery: TIfxQuery;
    DoesExist: Boolean;
    theSQL: String;
    i: Integer;
begin // the call:  UpdateOperatorList(sbasesys, trim(uppercase(dsc.OperatorPU)));

    try
    try
      MyQuery := GetIDACQuery(connTarget);
      i:=0;

      theSQL := 'select gob_basesys from g_operators_booked where gob_basesys = "'
        + sys + '" and gob_operator_code = "' + Oper + '"'  ;

      MyQuery.sql.Add( theSQL );
      memo2.Lines.add(theSQL);
      MyQuery.Active := true;

      if (MyQuery.IsEmpty) then
      begin
        MyQuery.active := false;
        MyQuery.SQL.Clear;
        theSQL := '';

        theSQL := 'INSERT into g_operators_booked ' +
                  '(gob_basesys, gob_operator_code) ' +
                  ' VALUES ' +
                  '("'+ sys + '", "'+ Oper + '" )';

        MyQuery.SQL.Add(theSQL);

        i := MyQuery.ExecSQL;
        memo2.Lines.add(IntToStr(i) + ' record inserted to g_operators_booked for sys ['+ sys + '] oper [' + oper + ']') ;
      end
      else
      begin
        memo2.Lines.add('record exists in g_operators_booked for sys ['+ sys + '] oper [' + oper + ']') ;
      end;
    except on e: exception do
      begin
        showmessage(' --> UpdateOperatorList  --> ['+ sys + '] oper [' + oper + '] --> '+ E.Message);
      end;
    end;
  finally
    if assigned(MyQuery) then FreeAndNil(MyQuery);
  end;
 end;
end.
