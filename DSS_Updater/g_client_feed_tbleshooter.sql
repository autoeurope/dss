// g_client troubleshooting

select 0 gc_key, "UID" gc_uid, trim(upper(clvlast)) gc_lastname, 
    trim(upper(clvfirst)) gc_firstname, 
    trim(upper(clvinit)) gc_initial, 
    trim(upper(clvsurname)) gc_surname,  
    trim(upper(clvaddr1)) gc_addr1,
    trim(upper(clvaddr2)) gc_addr2,
    trim(upper(clvprov)) gc_prov,
    trim(upper(clvcity)) gc_city,
    trim(upper(clvpostcode)) gc_postcode,
    trim(upper(clvctry)) gc_ctry,
    trim(upper(clvphone)) gc_phone,
    trim(upper(clfax)) gc_fax,
    "cell" gc_cell,
    trim(upper(clemail)) gc_email,
    "*PASSW*" gc_password,
    "HOHO" gc_system,
    vcnumb gc_voucher,
    vchrel gc_release,
    trim(upper(vciata)) gc_iata,
    vchpqbath gc_batchdt,
    "12/31/1899" gc_lastupdt
    from vouch, vchpqueue, vclient
    where vcntype = "A"  and
    (VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( 
    (vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and
    (vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> "N")) and
    vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN 
    AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and
     vchpqbath between date("04/01/2007") and date("04/30/2007") 



select vcnumb, vchrel, count(*)
    from vouch, vchpqueue, vclient
    where vcntype = "A"  and
    (VOPSUBT <> "T" OR length(VOPSUBT) = 0 OR VOPSUBT is null) and ( 
    (vchpqnsw1 is null OR length(vchpqnsw1) = 0 and vchpqnsw1 <> "N") and
    (vchpstat3 is null OR length(vchpstat3) = 0 and vchpstat3 <> "N")) and
    vcclientid = clvchnum and VCNUMB = VCHPQVCH and VCHREL = VCHPQVCHLN 
    AND VCHPQTYPE <> "m" AND VCHPQTYPE <> "p" and
     vchpqbath between date("04/01/2007") and date("04/30/2007") 
group by 1,2
having count(*) > 1